import 'dart:math';

import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){
  group("方法测试", (){
    test ("测试取年两位",(){
      final String result = VgDateTimeUtils.getTwoDigitsYear(2021);
      print("返回结果$result");
      expect(result, "21");
    });

    test("测试时间格式化",(){
      final String result = VgDateTimeUtils.getFormatTimeStr(1610441430);
      expect(result, "1/12 16:50");

      // final String now = VgDateTimeUtils.getFormatTimeStr(1610441430);
      // expect(now, "");
    });

    test("测试截断手机号",(){


      final String normalPhone = "13470016025";
      final String subPhone = "1347001602";
      final String morePhone = "134700160255";

      expect(globalSubPhone(normalPhone), "13470016025");

      expect(globalSubPhone(subPhone), "1347001602");

      expect(globalSubPhone(morePhone), "13470016025");

    });
  });
}
import 'package:e_reception_flutter/common_widgets/common_name_and_nick_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_red_num_widget/common_red_num_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets("测试一个组件", (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home:Scaffold (
         body: Container(
           child: CommonNameAndNickWidget(
             name: "name",
             nick: "nick",
           ),
         ),
        ),
    ),
    );
    final titleFinder = find.text("name");
    expect(titleFinder, findsOneWidget);
  });

}

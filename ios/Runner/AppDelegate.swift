import UIKit
import Flutter
import AppTrackingTransparency
// import flutter_downloader


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
//    GeneratedPluginRegistrant.register(with: self)

//     FlutterDownloaderPlugin.setPluginRegistrantCallback({ registry in
//         FlutterDownloaderPlugin.register(with: registry.registrar(forPlugin: "vn.hunghd.flutter_downloader")!)
//         GeneratedPluginRegistrant.register(with: registry)
//     })
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }


    override func applicationDidBecomeActive(_ application: UIApplication) {
        requestIDFA()
    }

    func requestIDFA() {
        if #available(iOS 14, *) {

            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                ATTrackingManager.requestTrackingAuthorization(completionHandler: {     status in
                    // Tracking authorization completed. Start loading ads here.
                    // loadAd()
                    print(status.rawValue)
                })
            }
        } else {
            // Fallback on earlier versions
        }
    }
    private func registerPlugins(registry: FlutterPluginRegistry) {
//        if (!registry.hasPlugin("FlutterDownloaderPlugin")) {
//           FlutterDownloaderPlugin.register(with: registry.registrar(forPlugin: "FlutterDownloaderPlugin")!)
//        }
    }
}
import 'dart:convert';

import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:vg_base/vg_sp_lib.dart';

class CacheUtil {


  static void putCache(String key, Map cacheBean) {
    if (cacheBean == null) {
      return;
    }
    print('存入缓存$key');
    SharePreferenceUtil.putString(
        key + UserRepository.getInstance().getCacheKeySuffix(),
        json.encode(cacheBean));
  }

  static Future getCache(String key) {
    return SharePreferenceUtil.getString(key+UserRepository.getInstance().getCacheKeySuffix())
        .then((value){
          print('$key 缓存不为空');
          if(value!=null){
           return json.decode(value);
          }else{
            return null;
          }
    } );
  }
}

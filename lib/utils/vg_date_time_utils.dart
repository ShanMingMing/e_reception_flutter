import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:vg_base/vg_log_lib.dart';

class VgDateTimeUtils {
  ///秒转分钟
  static String getSecondsToMinuteStr(int duration) {
    if (duration == null || duration <= 0) {
      return null;
    }
    StringBuffer stringBuffer = StringBuffer();
    int minute;
    try {
      minute = (duration / 60).floor();
    } catch (e) {
      minute = 0;
    }
    if (minute == null || minute <= 0) {
      stringBuffer.write("00");
    } else if (minute >= 0 && minute <= 9) {
      stringBuffer.write("0$minute");
    } else {
      stringBuffer.write("$minute");
    }

    int remainder = duration % 60;
    if (remainder == null || remainder <= 0 || remainder >= 60) {
      stringBuffer.write(":00");
    } else if (remainder >= 0 && remainder <= 9) {
      stringBuffer.write(":0$remainder");
    } else {
      stringBuffer.write(":$remainder");
    }
    return stringBuffer.toString();
  }

  ///格式化时间戳
  ///zhAndTimeSplit: 今天/昨天 与后面间隔字符
  ///yearOrMonthOrDaySplit: 年月日 直接间隔字符
  ///isShowCompleteYear: 是否显示完整的年 2020（默认false）
  static String getFormatTimeStr(
    int timestamp, {
    String zhAndTimeSplit = "",
    String yearOrMonthOrDaySplit,
    bool isShowCompleteYear = false,
    bool isShowCompleteMonth = false,
    bool isShowCompleteDay = false,
    bool isShowCompleteHour = false,
    bool isShowCompleteMinute = false,
  }) {
    if (timestamp == null || timestamp <= 0) {
      return null;
    }

    final int nowTimeStamp = DateTime.now().millisecondsSinceEpoch;
    final int fomartTimeStamp = timestamp * 1000;
    final DateTime nowDateTime =
        DateTime.fromMillisecondsSinceEpoch(nowTimeStamp);
    final DateTime fomartDateTime =
        DateTime.fromMillisecondsSinceEpoch(fomartTimeStamp);

    if (fomartTimeStamp == null || fomartTimeStamp <= 0) {
      return null;
    }
    if (fomartDateTime.isAfter(nowDateTime)) {
      return "${isShowCompleteYear ? fomartDateTime.year : getTwoDigitsYear(fomartDateTime.year)}"
          "${yearOrMonthOrDaySplit ?? "/"}"
          "${isShowCompleteMonth ? VgToolUtils.twoDigits(fomartDateTime.month) : fomartDateTime.month}"
          "${yearOrMonthOrDaySplit ?? "/"}"
          "${isShowCompleteDay ? VgToolUtils.twoDigits(fomartDateTime.day) : fomartDateTime.day}"
          " "
          "${isShowCompleteHour ? VgToolUtils.twoDigits(fomartDateTime.hour) : fomartDateTime.hour}"
          ":"
          "${_motifyZeroMinuteTwoDigits(isShowCompleteMinute ? VgToolUtils.twoDigits(fomartDateTime.minute) : fomartDateTime.minute)}";
    }

    ///今天
    if (nowDateTime.year == fomartDateTime.year &&
        nowDateTime.month == fomartDateTime.month &&
        nowDateTime.day == fomartDateTime.day) {
      return "今天"
          "$zhAndTimeSplit"
          "${isShowCompleteHour ? VgToolUtils.twoDigits(fomartDateTime.hour) : fomartDateTime.hour}"
          ":"
          "${_motifyZeroMinuteTwoDigits(isShowCompleteMinute ? VgToolUtils.twoDigits(fomartDateTime.minute) : fomartDateTime.minute)}";
    }

    ///昨天
    if (isYesterdayByMillis(fomartTimeStamp, nowTimeStamp)) {
      return "昨天"
          "${isShowCompleteHour ? VgToolUtils.twoDigits(fomartDateTime.hour) : fomartDateTime.hour}"
          ":"
          "${_motifyZeroMinuteTwoDigits(isShowCompleteMinute ? VgToolUtils.twoDigits(fomartDateTime.minute) : fomartDateTime.minute)}";
    }

    ///今年
    if (yearIsEqualByMillis(fomartTimeStamp, nowTimeStamp)) {
      return "${isShowCompleteMonth ? VgToolUtils.twoDigits(fomartDateTime.month) : fomartDateTime.month}"
          "${yearOrMonthOrDaySplit ?? "/"}"
          "${isShowCompleteDay ? VgToolUtils.twoDigits(fomartDateTime.day) : fomartDateTime.day}"
          " "
          "${isShowCompleteHour ? VgToolUtils.twoDigits(fomartDateTime.hour) : fomartDateTime.hour}"
          ":"
          "${_motifyZeroMinuteTwoDigits(isShowCompleteMinute ? VgToolUtils.twoDigits(fomartDateTime.minute) : fomartDateTime.minute)}";
    }

    ///年月日 时分
    return "${isShowCompleteYear ? fomartDateTime.year : getTwoDigitsYear(fomartDateTime.year)}"
        "${yearOrMonthOrDaySplit ?? "/"}"
        "${isShowCompleteMonth ? VgToolUtils.twoDigits(fomartDateTime.month) : fomartDateTime.month}"
        "${yearOrMonthOrDaySplit ?? "/"}"
        "${isShowCompleteDay ? VgToolUtils.twoDigits(fomartDateTime.day) : fomartDateTime.day}"
        " "
        "${isShowCompleteHour ? VgToolUtils.twoDigits(fomartDateTime.hour) : fomartDateTime.hour}"
        ":"
        "${_motifyZeroMinuteTwoDigits(isShowCompleteMinute ? VgToolUtils.twoDigits(fomartDateTime.minute) : fomartDateTime.minute)}";
  }

  static String _motifyZeroMinuteTwoDigits(dynamic minute) {
    int value;
    if (minute is String) {
      value = int.parse(minute);
    }
    if (minute is int) {
      value = minute;
    }
    if (value < 10 && value >= 0) {
      return "0$value";
    }
    return "$value";
  }

  // static String _getTodayStr(
  //     {int nowTimeStamp,
  //     int fomartTimeStamp,
  //     DateTime nowDateTime,
  //     DateTime formatDateTime}) {
  //   //相差值
  //   final int diffMill = nowTimeStamp - fomartTimeStamp;
  //   final int diffSecond = (diffMill / 1000).floor();
  //   //时间超前
  //   if (diffSecond < 0) {
  //     return null;
  //   }
  //
  //   //刚刚（1分钟）
  //   if (diffSecond < 1 * 60) {
  //     return "刚刚";
  //   }
  //   //59分钟（1小时以内）
  //   if (diffSecond < 1 * 60 * 60) {
  //     return "${(diffSecond / (1 * 60 * 60).floor())}分钟前";
  //   }
  //   //23小时（1天内）
  //   if (diffSecond < 1 * 60 * 60 * 24) {
  //     return "${(diffSecond / (1 * 60 * 60 * 24))}小时前";
  //   }
  // }

  /// is yesterday by millis.
  /// 是否是昨天.
  static bool isYesterdayByMillis(int millis, int locMillis) {
    return isYesterday(DateTime.fromMillisecondsSinceEpoch(millis),
        DateTime.fromMillisecondsSinceEpoch(locMillis));
  }

  /// is yesterday by dateTime.
  /// 是否是昨天.
  static bool isYesterday(DateTime dateTime, DateTime locDateTime) {
    final DateTime lastDayLocDateTime = locDateTime.subtract(Duration(days: 1));
    if(dateTime.year == lastDayLocDateTime.year && dateTime.month == lastDayLocDateTime.month && dateTime.day == lastDayLocDateTime.day){
      return true;
    }
    return false;
    // //同年 同月 同天+1
    // if (yearIsEqual(dateTime, locDateTime) &&
    //     locDateTime.month == dateTime.month &&
    //     locDateTime.day == dateTime.day + 1) {
    //   return true;
    // }
    // //同年 上一月
    // if (yearIsEqual(dateTime, locDateTime) && locDateTime.month == dateTime.month + 1) {
    //
    // }
    // return ((locDateTime.year - dateTime.year == 1) &&
    //     dateTime.month == 12 &&
    //     locDateTime.month == 1 &&
    //     dateTime.day == 31 &&
    //     locDateTime.day == 1);
  }

  /// year is equal.
  /// 是否同年.
  static bool yearIsEqual(DateTime dateTime, DateTime locDateTime) {
    return dateTime.year == locDateTime.year;
  }

  /// year is equal.
  /// 是否同年.
  static bool yearIsEqualByMillis(int millis, int locMillis) {
    return yearIsEqual(DateTime.fromMillisecondsSinceEpoch(millis),
        DateTime.fromMillisecondsSinceEpoch(locMillis));
  }

  static String getTwoDigitsYear(int year) {
    if (year == null || year < 0) {
      return null;
    }
    String yearStr = year.toString();
    final int yearLength = yearStr.length ?? 0;
    if (yearLength <= 2) {
      return null;
    }
    String twoYear = yearStr.substring(yearLength - 2, yearLength);
    return twoYear;
  }

  ///是否是今天 根据时间戳
  static bool isToday(int timeStamp) {
    if (timeStamp == null || timeStamp <= 0) {
      return false;
    }
    final DateTime nowDateTime = DateTime.now();
    final DateTime fomartDateTime =
        DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
    if (fomartDateTime.year == nowDateTime.year &&
        fomartDateTime.month == nowDateTime.month &&
        fomartDateTime.day == nowDateTime.day) {
      return true;
    }
    return false;
  }

  ///是否是同一天
  static bool isEqualDay(int timeStamp, int timeStamp1) {
    if (timeStamp == null || timeStamp1 == null) {
      return false;
    }
    final DateTime oneDateTime =
        DateTime.fromMillisecondsSinceEpoch(timeStamp * 1000);
    final DateTime twoDateTime =
        DateTime.fromMillisecondsSinceEpoch(timeStamp1 * 1000);
    if (oneDateTime.year == twoDateTime.year &&
        oneDateTime.month == twoDateTime.month &&
        oneDateTime.day == twoDateTime.day) {
      return true;
    }
    return false;
  }

  //位置1大于位置2 true;
  static bool isBig(int timeStamp, int timeStamp1) {
    return timeStamp > timeStamp1;
  }

  //位置1小于位置2 true;
  static bool isSmall(int timeStamp, int timStamp1) {
    return timeStamp < timStamp1;
  }

  //String转秒
  static int getStringToIntMillisecond(String date){
    if(date==null){
      return null;
    }
    DateTime _strTime = DateTime.parse(date);
    double _intendTime = _strTime.millisecondsSinceEpoch/1000;
    return _intendTime.toInt();
  }

  //获取当前时区与东八区的时间偏移量
  static int getOffsetInSeconds(){
    // 获取当前时间
    DateTime now = DateTime.now();
    // 获取当前时间的UTC偏移量
    Duration localOffset = now.timeZoneOffset;
    // 东八区的UTC偏移量为8小时
    Duration utcPlus8Offset = Duration(hours: 8);
    // 计算当前时区与东八区的偏移量（秒）
    int offsetInSeconds = localOffset.inSeconds - utcPlus8Offset.inSeconds;
    return offsetInSeconds;
  }

  //获取当前时区下，东八区对应的当前日期0点的时间戳
  static String getCurrentTimeStamp(){
    final DateTime nowDateTime = DateTime.now();
    var currentTimeByWeekHours =
        DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day);
    int millisecondsSinceEpoch = ((currentTimeByWeekHours?.millisecondsSinceEpoch ?? 0)/1000).floor();
    LogUtils.d("millisecondsSinceEpoch：$millisecondsSinceEpoch");
    int offsetInSeconds = VgDateTimeUtils.getOffsetInSeconds();
    LogUtils.d("offsetInSeconds：$offsetInSeconds");
    LogUtils.d("millisecondsSinceEpoch+offsetInSeconds：${millisecondsSinceEpoch + offsetInSeconds}");
    final String sDateTime = (millisecondsSinceEpoch + offsetInSeconds).toString();
    return sDateTime;
  }
}

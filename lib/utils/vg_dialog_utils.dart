import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/vg_widgets/vg_dialog_widget/vg_custom_show_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_log_lib.dart';

class VgDialogUtils {

  ///通用底部弹窗
  static Future<T> showCommonBottomDialog<T>(
      {@required BuildContext context,
      @required Widget child,
      bool isDismissible = true,
      Color barrierColor = ColorConstants.BOTTOM_SHEET_BARRIER_COLOR,
      bool enableDrag = true}) {
    if (child == null || context == null) {
      return null;
    }
    LogUtils.d("\n"
        "--------------------------------------------------------"
        "\n\n"
        "  路由：底部弹窗页面名称：--------- ${child?.runtimeType?.toString() ?? "未知"}"
        "\n\n"
        "-------------------------------------------------------");
    return showModalBottomSheet<T>(
        context: context,
        isScrollControlled: true,
        isDismissible: isDismissible,
        enableDrag: enableDrag,
        barrierColor: barrierColor,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return child ?? Container();
        });
  }

  ///默认弹窗
  static Future<T> showCommonDialog<T>(
      {@required BuildContext context,
      @required Widget child,
      bool barrierDismissible = true,
      bool useRootNavigator = true,
        Color barrierColor = ColorConstants.BOTTOM_SHEET_BARRIER_COLOR,
      RouteSettings routeSettings,
      bool needSafeArea = true,}) {
    LogUtils.d("\n"
        "--------------------------------------------------------"
        "\n\n"
        "  路由：普通弹窗页面名称：--------- ${child?.runtimeType?.toString() ?? "未知"}"
        "\n\n"
        "-------------------------------------------------------");
    return VgCustomShowDialog<T>(
        context: context,
        barrierColor:barrierColor,
        barrierDismissible: barrierDismissible,
        useRootNavigator: useRootNavigator,
        routeSettings: routeSettings,
        needSafeArea: needSafeArea,
        builder: (BuildContext context) {
          return child ?? Container();
        });
  }
}

import 'package:e_reception_flutter/net/server_api.dart';

/// privacyPolicyBoardAndUserAgreement.html 隐私协议和用户协议一起的
const String PRIVACY_AND_USER_H5 =
   ServerApi.BASE_URL + "privacyPolicyBoardAndUserAgreement.html";

/// privacyPolicyBoard.html 纯隐私协议
const String PRIVACY_H5 = "https://etpic.we17.com/privacy/privacyPolicyBoard.html";

/// userAgreement_TSD.html 纯用户协议
const String USER_H5 =ServerApi.BASE_URL + "userAgreement_TSD.html";

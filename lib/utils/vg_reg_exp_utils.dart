
///正则工具类
class VgRegExpUtils{

  ///手机号验证
  static bool isPhone(String str) {
    return RegExp(r"1[0-9]\d{9}$")
        .hasMatch(str);
  }

  ///邮箱验证
  static bool isEmail(String str) {
    return RegExp(
        r"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")
        .hasMatch(str);
  }
}
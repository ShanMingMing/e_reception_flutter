import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_adapter.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:vg_base/vg_string_util_lib.dart';

const String _SUPER_ADMIN = "99";
const String _COMMON_ADMIN = "90";

enum RoleType {
  superAdmin, //99
  commonAdmin, //90
  commonUser,
}

extension RoleTypeExtension on RoleType {
  static RoleType getRoleTypeByStr(String roleId) {
    if (StringUtils.isEmpty(roleId)) {
      return RoleType.commonUser;
    }
    switch (roleId) {
      case _SUPER_ADMIN:
        return RoleType.superAdmin;
      case _COMMON_ADMIN:
        return RoleType.commonAdmin;
    }
    return RoleType.commonUser;
  }
}

class VgRoleUtils {
  ///是否超级管理
  static bool isSuperAdmin(String roleid) {
    return roleid == _SUPER_ADMIN;
  }

  ///是否超级管理
  static bool isCommonAdmin(String roleid) {
    return roleid == _COMMON_ADMIN;
  }

  static bool isCommonUser(String roleid){
    return !(isSuperAdmin(roleid) || isCommonAdmin(roleid));
  }

  ///获取当前权限
  static RoleType getCurrentRole() {
    String currentCompanyId = UserRepository.getInstance().companyId;
    List<UserComListBean> allCompanyList =
        UserRepository.getInstance().companyList;
    if (StringUtils.isEmpty(currentCompanyId) ||
        allCompanyList == null ||
        allCompanyList.isEmpty) {
      return null;
    }
    String currentRoleId;
    for (UserComListBean item in allCompanyList) {
      if (item?.companyid == currentCompanyId) {
        currentRoleId = item?.roleid;
        break;
      }
    }
    if (currentRoleId == null || currentRoleId.isEmpty) {
      return null;
    }
    return RoleTypeExtension.getRoleTypeByStr(currentRoleId);
  }

  ///是否有编辑权限 公司详情
  static bool isHasEditForCompanyDetail(String userId, String roleId) {
    final RoleType currentRoleType = getCurrentRole();
    final String currentUserId = UserRepository.getInstance().userData?.user?.userid;
    //是本人的话 直接可以编辑
    if (StringUtils.isNotEmpty(currentUserId) && userId == currentUserId) {
      return true;
    }
    if (currentRoleType == null) {
      return false;
    }
    //如果是超级管理员可以编辑所有人
    if (currentRoleType == RoleType.superAdmin) {
      return true;
    }
    //普通管理员编辑普通人
    if (currentRoleType == RoleType.commonAdmin) {
      if(VgRoleUtils.isCommonUser(roleId)){
        return true;
      }
    }
    return false;
  }

  ///是否有删除权限 公司详情
  static bool isHasDeleteForCompanyDetail(String userId, String roleId) {
    final RoleType currentRoleType = getCurrentRole();
    final String currentUserId = UserRepository.getInstance().userData?.user?.userid;
    if (currentRoleType == null) {
      return false;
    }
    //如果是超级管理员可以删除所有人，不能删除自己
    if (currentRoleType == RoleType.superAdmin) {
      //是否自己
      if (StringUtils.isNotEmpty(currentUserId) && userId != currentUserId) {
        return true;
      } else {
        return false;
      }
    }
    //普通管理员只能删除普通人
    if (currentRoleType == RoleType.commonAdmin) {
      if (StringUtils.isNotEmpty(roleId) &&
          RoleTypeExtension.getRoleTypeByStr(roleId) == RoleType.commonUser) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  ///判断编辑是否展示底部修改权限开关
  static bool isEditUserShowModifityRole(String userId,){
    final String currentUserId = UserRepository.getInstance().userData?.user?.userid;
    final RoleType currentRoleType = getCurrentRole();
    //是本人的话 不可以编辑权限
    if (StringUtils.isNotEmpty(currentUserId) && userId == currentUserId) {
      return false;
    }
    ///如果是超级管理员
    if(currentRoleType == RoleType.superAdmin){
      return true;
    }
    return false;
  }
}

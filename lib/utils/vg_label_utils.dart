import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class VgLabelUtils {
  ///超级管理员标签
  static Widget getRoleLabel(String roleid) {
    if (StringUtils.isEmpty(roleid)) {
      return null;
    }
    if (VgRoleUtils.isSuperAdmin(roleid)) {
      return Container(
        height: 16,
        padding: const EdgeInsets.symmetric(horizontal: 4),
        margin: const EdgeInsets.symmetric(horizontal: 4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2),
            color: ThemeRepository.getInstance()
                .getMinorYellowColor_FFB714()
                .withOpacity(0.1)),
        child: Center(
          child: Text(
            "超级管理员",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:
                    ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
                fontSize: 10,
                height: 1.2),
          ),
        ),
      );
    }
    if (VgRoleUtils.isCommonAdmin(roleid)) {
      return Container(
        height: 16,
        padding: const EdgeInsets.symmetric(horizontal: 4),
        margin: const EdgeInsets.symmetric(horizontal: 4),

        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2),
            color: ThemeRepository.getInstance()
                .getPrimaryColor_1890FF()
                .withOpacity(0.1)),
        child: Center(
          child: Text(
            "管理员",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                fontSize: 10,
                height: 1.2),
          ),
        ),
      );
    }
    return null;
  }

  ///当前登录
  ///00-不是
  ///01-当前登录
  static Widget getCurrentLoginDeviceLabel(String loginFlg){
    if(loginFlg != "01"){
      return null;
    }
    return Container(
      height: 16,
      padding: const EdgeInsets.symmetric(horizontal: 4),
      margin: const EdgeInsets.symmetric(horizontal: 4),

      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2),
          color: ThemeRepository.getInstance()
              .getPrimaryColor_1890FF()
              .withOpacity(0.1)),
      child: Center(
        child: Text(
          "当前设备登录",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              fontSize: 10,
              height: 1.2),
        ),
      ),
    );
  }
}

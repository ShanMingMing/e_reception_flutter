//刷脸界面返回数据更新
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';

class MonitoringIndexPageClass{}

class MonitoringMediaLibraryIndexClass{}

class MonitoringAiPosterIndexClass{}

//主页接口数据刷新小部件监听
class UpdateHomePageEventMonitor{}
class HomePageInterfaceEventMonitor{
  final bool showLoad;
  HomePageInterfaceEventMonitor({this.showLoad});

}

//我的主界面意见反馈接口更新
class SubmitProposalInterfaceEventMonitor{}

//搜索区县列表数据
class SearchCityNameListEventMonitor{
  final List<Map<String,ChooseCityListItemBean>> searchCityNameList;
  SearchCityNameListEventMonitor(this.searchCityNameList);
}

class ArtificialAttendanceUpdateEvent{
  final String headFaceUrl;
  final String fuid;
  ArtificialAttendanceUpdateEvent({this.fuid, this.headFaceUrl});

}

//监听企业主页数字条
class CompanyNumBarEventMonitor{
  final bool delete;
  CompanyNumBarEventMonitor({this.delete});
}
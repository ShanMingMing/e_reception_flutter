import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:flutter/widgets.dart';
import 'package:umeng_common_sdk/umeng_common_sdk.dart';

class AppAnalysis extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) async {
    if (!ConstantRepository.of().isAppStore) {
      return;
    }

    if (route?.settings?.name != null) {
      UmengCommonSdk.onPageStart(route.settings.name);
    }

    if (previousRoute?.settings?.name != null) {
      UmengCommonSdk.onPageEnd(previousRoute.settings.name);
    }
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) async {
    if (!ConstantRepository.of().isAppStore) {
      return;
    }

    if (previousRoute?.settings?.name != null) {
      UmengCommonSdk.onPageStart(previousRoute.settings.name);
    }

    if (route?.settings?.name != null) {
      UmengCommonSdk.onPageEnd(route.settings.name);
    }
  }

  @override
  void didReplace({Route<dynamic> newRoute, Route<dynamic> oldRoute}) async {
    if (!ConstantRepository.of().isAppStore) {
      return;
    }

    if (newRoute?.settings?.name != null) {
      UmengCommonSdk.onPageStart(newRoute.settings.name);
    }

    if (oldRoute?.settings?.name != null) {
      UmengCommonSdk.onPageEnd(oldRoute.settings.name);
    }
  }
}

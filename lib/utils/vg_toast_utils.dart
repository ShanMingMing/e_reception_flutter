import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_widget_lib.dart';

///项目内吐司类
class VgToastUtils{
  static toast(BuildContext context,String msg){
    if(context == null){
      return;
    }
    if(msg == null || msg == ""){
      return;
    }
    ToastUtils.toast(context: context,msg: msg);
  }
}
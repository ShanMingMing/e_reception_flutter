import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';

class AppOverallDistinguish{
  static bool comefromAiOrWeStudy(){
    //comefrom：公司来源  00AI前台  01一起学
    if(UserRepository.getInstance()?.userData?.companyInfo?.comefrom == "00"){
      return true;
    }else{
      return false;
    }
  }

  static bool enableFaceRecognition(){
    ////启用人脸识别 00启用 01关闭
    if(UserRepository.getInstance()?.userData?.companyInfo?.enableFaceRecognition == "00"
        && UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition()){
      return true;
    }else{
      return false;
    }
  }
}
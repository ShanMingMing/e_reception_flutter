import 'dart:convert';

import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/company/choose_city/widgets/choose_city_tabs_and_pages_widget.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

const String CHOOSE_PROVINCE_KEY = "province";

const String CHOOSE_CITY_KEY = "city";

const String CHOOSE_DISTRICT_KEY = "district";

class VgLocationUtils {
  ///获取省码
  static String getProvinceCode(String code) {
    if (code == null || code == "" || code.length != 6) {
      return null;
    }
    return code.substring(0, 2) + "0000";
  }

  ///获取城市码
  static String getCityCode(String code) {
    if (code == null || code == "" || code.length != 6) {
      return null;
    }
    return code.substring(0, 4) + "00";
  }

  ///获取地区码
  static String getDistrictCode(String code) {
    if (code == null || code == "" || code.length != 6) {
      return null;
    }
    return code.substring(0, 6);
  }

  ///获取缓存的经纬度
  static Future<String> getCacheLatLngString()async{
    String latLng = await SharePreferenceUtil.getString("latlng");
    LatLngBean bean;
    if(StringUtils.isNotEmpty(latLng)){
      Map map = json.decode(latLng);
      bean = LatLngBean.fromMap(map);
    }else{
      bean =  new LatLngBean();
    }
    return bean.latitude.toString() + "," + bean.longitude.toString();
  }

  ///Location定位数据转gps
  static void transLocationToLatLngAndCache(
      Map<String, Object> locationMap) async{
    String latLng = await SharePreferenceUtil.getString("latlng");
    LatLngBean latLngBean = new LatLngBean();
    //无定位数据，并且当前缓存gps也无，则默认gps为北京
    if(locationMap == null && StringUtils.isEmpty(latLng)){
      SharePreferenceUtil.putString("latlng", json.encode(latLngBean));
      return;
    }
    if(locationMap.containsKey("latitude") && locationMap.containsKey("longitude")){
      latLngBean = LatLngBean.fromMap(locationMap);
      SharePreferenceUtil.putString("latlng", json.encode(latLngBean));
      return;
    }
    SharePreferenceUtil.putString("latlng", json.encode(latLngBean));
    print("latlng:" + json.encode(latLngBean));
  }


  ///Location定位数据转选择城市所需的Map
  static Map<String, ChooseCityListItemBean> transLocationToMap(
      Map<String, Object> locationMap) {
    if (locationMap == null || locationMap["adCode"] == "") {
      return null;
    }

    String provinceCode = getProvinceCode(locationMap["adCode"]);
    String cityCode = getCityCode(locationMap["adCode"]);
    String districtCode = getDistrictCode(locationMap["adCode"]);
    if (StringUtils.isEmpty(provinceCode) ||
        StringUtils.isEmpty(cityCode) ||
        StringUtils.isEmpty(districtCode)) {
      return null;
    }
    print(provinceCode + " | " + cityCode + " | " + districtCode);
    return {
      CHOOSE_PROVINCE_KEY:
      ChooseCityListItemBean(provinceCode, locationMap["province"]),
      CHOOSE_CITY_KEY: ChooseCityListItemBean(cityCode, locationMap["city"]),
      CHOOSE_DISTRICT_KEY:
      ChooseCityListItemBean(districtCode, locationMap["district"])
    };
  }

  static Map<String, ChooseCityListItemBean> transStringToMap(
      CompanyInfoBean companyInfoBean) {
    if (companyInfoBean == null) {
      return null;
    }
    Map<String, Object> locationMap = Map();
    locationMap.putIfAbsent("province", () => companyInfoBean?.addrProvince);
    locationMap.putIfAbsent("city", () => companyInfoBean?.addrCity);
    locationMap.putIfAbsent("district", () => companyInfoBean?.addrDistrict);
    locationMap.putIfAbsent("adCode", () => companyInfoBean?.addrCode);
    return transLocationToMap(locationMap);
  }

  ///根据Map编码
  //{
  // 	"province":"北京市",
  // 	"city":"北京市",
  // 	"district":"大兴区",
  // 	"code":"xxxxxx"
  // }
  static String encodeLocationByMap(Map<String, ChooseCityListItemBean> map) {
    if (map == null || map.isEmpty) {
      return null;
    }
    SimpleLocationBean bean = SimpleLocationBean();
    if (map.containsKey(CHOOSE_PROVINCE_KEY)) {
      bean.province = map[CHOOSE_PROVINCE_KEY]?.sname;
      bean.code = map[CHOOSE_PROVINCE_KEY]?.sid;
    }
    if (map.containsKey(CHOOSE_CITY_KEY)) {
      bean.city = map[CHOOSE_CITY_KEY]?.sname;
      bean.code = map[CHOOSE_CITY_KEY]?.sid;
    }
    if (map.containsKey(CHOOSE_DISTRICT_KEY)) {
      bean.district = map[CHOOSE_DISTRICT_KEY]?.sname;
      bean.code = map[CHOOSE_DISTRICT_KEY]?.sid;
    }
    return jsonEncode(bean);
  }
}

///简单地理Bean
///
/// province : "北京市"
/// city : "北京市"
/// district : "大兴区"
/// code : "xxxxxx"

class SimpleLocationBean {
  String province;
  String city;
  String district;
  String code;

  static SimpleLocationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SimpleLocationBean bean = SimpleLocationBean();
    bean.province = map['province'];
    bean.city = map['city'];
    bean.district = map['district'];
    bean.code = map['code'];
    return bean;
  }

  Map toJson() => {
    "province": province,
    "city": city,
    "district": district,
    "code": code,
  };
}

///经纬度
class LatLngBean{
  double latitude = 39.928353;
  double longitude = 116.416357;

  static LatLngBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LatLngBean bean = LatLngBean();
    if (map['latitude'] is String) {
      bean.latitude = double.parse(map['latitude']);
      bean.longitude = double.parse(map['longitude']);
    } else {
      bean.latitude = map['latitude'];
      bean.longitude = map['longitude'];
    }
    return bean;
  }

  Map toJson() => {
    "latitude": latitude,
    "longitude": longitude,
  };

}

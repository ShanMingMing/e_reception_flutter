import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_widget_lib.dart';

class VgHudUtils{

  ///显示hud
  static void show(BuildContext context,[String msg]){
    if(context == null){
      return;
    }
    if (msg == null || msg.isEmpty) {
      HudUtils.showLoading(context,true);
    } else {
      HudUtils.showLoading(context, true,
          builder: HudUtils.customMsgHudBuilder(msg));
    }
  }

  ///隐藏hud
  static void hide(BuildContext context){
    if(context == null){
      return;
    }
    HudUtils.showLoading(context, false);
  }
}
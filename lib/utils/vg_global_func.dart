
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';

///空串返回null
 String showOriginEmptyStr(String str){
  if(str == null || str.isEmpty){
    return null;
  }
  return str;
}

///获取底部导航栏距离
double getNavHeightDistance(BuildContext context){
  return 20 + NAV_HEIGHT + ScreenUtils.getBottomBarH(context);
}

///截断手机号多位问题
///bug: android 编辑框快速输入显示不正确问题
String globalSubPhone(String phone){
   if(phone == null || phone.isEmpty){
     return null;
   }
   if(phone.length > DEFAULT_PHONE_LENGTH_LIMIT){
     return phone.substring(0,DEFAULT_PHONE_LENGTH_LIMIT);
   }
   return phone;
}
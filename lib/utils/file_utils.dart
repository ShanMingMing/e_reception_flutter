import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';

class FileUtils {
  static Future<String> createFileFromString(String base64Str) async {
    Uint8List bytes = base64.decode(base64Str);

    Directory rootPath = await getApplicationSupportDirectory();
    Directory tempDirectory = new Directory('${rootPath.path}/imageTemp');
    if (!tempDirectory.existsSync()) {
      tempDirectory.createSync();
    }
    String dir = tempDirectory.path;
    File file = File("$dir/" + DateTime
        .now()
        .millisecondsSinceEpoch
        .toString() + ".png");
    await file.writeAsBytes(bytes);
    return file.path;
  }

  static void getImageInfo(String path, Function(String) getSize){
    FileImage clipImage = FileImage(File(path));
    ImageInfoBean imageInfoBean = new ImageInfoBean();
    clipImage.resolve(new ImageConfiguration())
        .addListener(new ImageStreamListener((ImageInfo info, bool _){
      imageInfoBean.folderWidth = info.image.width;
      imageInfoBean.folderHeight = info.image.height;
      print("folderWidth:" + info.image.width.toString());
      print("folderHeight:" + info.image.height.toString());
      getSize.call(imageInfoBean.getSize());
    }));
  }

  ///删除临时文件夹文件
  static void safeDeleteTemporaryFiles() async {
    Directory rootPath = await getApplicationSupportDirectory();
    Directory tempDirectory = new Directory('${rootPath.path}/imageTemp');

    if (tempDirectory.existsSync()) {
      tempDirectory.deleteSync(recursive: true);
      // List<FileSystemEntity> files = tempDirectory.listSync();
      // if (files != null && files.length > 0) {
      //   files.forEach((file) {
      //     file.delete();
      //   });
      // }
      print("y临时文件夹清空完成");
    }
  }

  static Future<String> createFileFromUnit8List(Uint8List picBytes) async {
    Directory rootPath = await getApplicationSupportDirectory();
    Directory tempDirectory = new Directory('${rootPath.path}/imageTemp');
    if (!tempDirectory.existsSync()) {
      tempDirectory.createSync();
    }
    String dir = tempDirectory.path;
    File file = File("$dir/" + DateTime
        .now()
        .millisecondsSinceEpoch
        .toString() + ".png");
    await file.writeAsBytes(picBytes);
    return file.path;
  }

  static Future<String> createFileFromUnit8ListInDirectory(String directory, Uint8List picBytes) async {
    Directory tempDirectory = new Directory(directory);
    if (!tempDirectory.existsSync()) {
      tempDirectory.createSync();
    }
    String dir = tempDirectory.path;
    File file = File("$dir/" + DateTime
        .now()
        .millisecondsSinceEpoch
        .toString() + ".png");
    await file.writeAsBytes(picBytes);
    return file.path;
  }
}




class ImageInfoBean{
  String imagePath;
  //文件内存大小
  int folderStorage;
  int folderWidth;
  int folderHeight;

  String getSize() {
    if (folderWidth == null || folderHeight == null) {
      return null;
    }
    return "$folderWidth*$folderHeight";
  }
}
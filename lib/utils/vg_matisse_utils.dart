import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:ali_vod_video/ali_vod_video.dart';
import 'package:ali_vod_video/upload_callback.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/video_upload_success_bean.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/utils/vg_oss_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

class VgMatisseUploadUtils{
  
  ///上传一个图片
  static void uploadSingleImage(String path,VgBaseCallback<String> callback,{bool isFace, bool isNoCompress, ValueChanged<int> onGetNewFileLength}){
    if(StringUtils.isEmpty(path)){
      callback?.onError("路径获取失败");
      return;
    }
    if(VgStringUtils.isNetUrl(path)){
      callback?.onSuccess(path);
      return;
    }
    VgOssUtils.uploadSingleImage(path: path, isFace: isFace, isNoCompress:isNoCompress,callback: BaseCallback(
          onSuccess: (net){
            print("上传单个图片成功：${net}");
            if(net == null || net == ""){
              callback?.onError(null);
              return;
            }
            callback?.onSuccess(net);
          },
          onError: (msg){
            callback?.onError(msg);
          },
        ),
        onGetNewFileLength: onGetNewFileLength
    );
  }

  static Future<String> uploadSingleImageForFuture(String path,{bool isFace, bool isNoCompress, ValueChanged<int> onGetNewFileLength}){
    print("uploadSingleImageForFuture:${path}");
    if(StringUtils.isEmpty(path)){
      return Future.error("路径获取失败");
    }
    if(VgStringUtils.isNetUrl(path)){
      return Future.value(path);
    }
    Completer<String> completer = Completer();
    VgOssUtils.uploadSingleImage(path: path,isFace: isFace, isNoCompress:isNoCompress,callback: BaseCallback(
        onSuccess: (net){
          if(net == null || net == ""){
            completer.completeError("数据空");
            return;
          }
         completer.complete(net);
        },
        onError: (msg){
          print("上传失败：222222" + msg);
          completer.completeError(msg);
        }
    ),
      onGetNewFileLength: onGetNewFileLength
    );
    return completer.future;
  }

  static Future<VideoUploadSuccessBean> uploadSingleVideoForFuture(String path,{bool isNoCompress, bool needProgress, OnProgress onProgress}){
    if(StringUtils.isEmpty(path)){
      return Future.error("路径获取失败");
    }
    if(VgStringUtils.isNetUrl(path)){
      return Future.value(new VideoUploadSuccessBean(path, "","", "", "", ""));
    }
    Completer<VideoUploadSuccessBean> completer = Completer();
    // VgOssUtils.uploadSingleVideo(path:path,callback: BaseCallback(
    //     onSuccess: (net){
    //       if(net == null || net == ""){
    //         completer.completeError("数据空");
    //         return;
    //       }
    //       completer.complete(net);
    //     },
    //     onError: (msg){
    //       completer.completeError(msg);
    //     }
    // ));
    String cateId = ("http://et.lemontry.com/" ==ServerApi.BASE_URL)?"2061":"2165";
    print("VgMatisseUploadUtils:" + cateId);
    String userData = '{"MessageCallback":{"CallbackURL":"'+ServerApi.BASE_URL+'/aliyunvod/transcodingCallback","CallbackType":"http"}, "Extend":"01"}';
    String id=DateTime.now().toString();
    new AliVodVideo().uploadVideo(id, cateId, ConstantRepository.of().getTemplateGroupId(),
        ConstantRepository.of().getDefinition(), path, userData, UploadCallback(
        onSuccess: (net, fileSize,coverUrl, videoId, sdVideo, sdStorage){
          if(net == null || net == ""){
            completer.completeError("数据空");
            print("VgMatisseUploadUtils:数据空");
            return;
          }
          print("VgMatisseUploadUtils:net:" + net);
          completer.complete(new VideoUploadSuccessBean(net, fileSize,coverUrl, videoId, sdVideo, sdStorage));
        },
        onError: (msg){
          print("VgMatisseUploadUtils:onError:" + msg);
          completer.completeError(msg);
        },
          onProgress: (progress, id, {String msg}){
            onProgress?.call(progress, id);
          },
    ), needProgress: needProgress);
    return completer.future;
  }


  static String getVideoPath(){
    return (Random().nextInt(8999) + 1000).toString() +
            "_" +
            DateUtil.formatDate(DateTime.now(), format: "yyyyMMddHHmmss") +
            ".mp4";
  }

}


import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';

class FlutterGlobalMonitoringUtils {
  static ValueNotifier<String> dataNotifier = ValueNotifier(null);
  static void init(){
    dataNotifier.addListener(() {
      if(dataNotifier?.value=="610"){
          UserRepository.getInstance().clearUserInfo();
          RouterUtils.popUntil(AppMain.context, AppMain.ROUTER,
              rootNavigator: false);
          // VgToastUtils.toast(AppMain.context, "您已被删除或被降级为普通员工");
      }
    });
  }
}




import 'dart:async';

import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/widgets/create_user_edit_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

class SelectUtil {
  ///显示下方弹出的列表弹窗，
  ///传入参数有标题，
  ///列表应该选中的列表文字，不传这一项就默认选中第0项
  ///点击右侧确定按钮的函数
  static Future<dynamic> showListSelectDialog<T>(
      {@required BuildContext context,
        String title,
        String subTitle,
        String confirmText,
        List<String> textList,
        String positionStr,
        Function onSelect,
        Color barrierColor,
        double textHeight,
        String rightText,
        double rightTextOffsetX,
        Color bgColor,
        Color titleColor,
        Color itemColor,
      }) {
    return showModalBottomSheet<T>(
        context: context,
        barrierColor: barrierColor ?? ColorConstants.BOTTOM_SHEET_BARRIER_COLOR,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return SelectPage(
            strList: textList,
            positionStr: positionStr,
            title: title,
            subTitle: subTitle,
            confirmText: confirmText,
            rightText: rightText,
            textHeight: textHeight,
            rightTextOffsetX: rightTextOffsetX,
            onSelect: onSelect,
            bgColor: bgColor,
            titleColor: titleColor,
            itemColor: itemColor,
          );
        });
  }
}

class SelectPage extends StatefulWidget {
  List<String> strList = [];

  String positionStr = "";

  int initPosition = 0;

  String title;

  String subTitle;

  String confirmText;

  String rightText;

  double rightTextOffsetX;

  double textHeight;

  Function onSelect;

  Color bgColor;
  Color titleColor;
  Color itemColor;

  SelectPage(
      {Key key,
        this.strList,
        this.positionStr,
        this.title,
        this.subTitle,
        this.confirmText,
        this.onSelect,
        this.rightText,
        this.rightTextOffsetX,
        this.textHeight,
        this.bgColor,
        this.titleColor,
        this.itemColor,
      })
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SelectState(strList, positionStr, title, onSelect, bgColor, titleColor, itemColor);
  }
}

class SelectState extends BaseState<SelectPage> {
  String positionStr = ""; //被选中的item文字

  String clickItem; //当前选定item文字

  List<String> strList = []; //资源列表

  int initPosition = 0; //初始item下标

  String title; //列表标题

  Function _onSelect; //点击确定的函数

  int selectPosition = 0; //被选中的下标

  FixedExtentScrollController proviceCotroller;

  Color bgColor;
  Color titleColor;
  Color itemColor;

  SelectState(this.strList, this.positionStr, this.title, this._onSelect,
      this.bgColor, this.titleColor, this.itemColor);

  @override
  void initState() {
    super.initState();
    initPosition = (positionStr == null || positionStr == "")
        ? 0
        : (strList.indexOf(positionStr) == -1
        ? 0
        : strList.indexOf(positionStr));
    selectPosition = initPosition;
    clickItem = strList[initPosition];
    proviceCotroller =
    new FixedExtentScrollController(initialItem: initPosition ?? 0);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 288,
      decoration: new BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
          color: bgColor??ThemeRepository.getInstance().getCardBgColor_21263C()),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 18, left: 18, right: 18),
            child: Row(
              children: <Widget>[
                GestureDetector(
                  child: Text(
                    "取消",
                    style: TextStyle(color: Color(0XFFB0B3BF), fontSize: 16),
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: titleColor??ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7(),
                        fontSize: 17,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                GestureDetector(
                  child: Text(
                    widget.confirmText ?? "确认",
                    style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getPrimaryColor_1890FF(),
                        fontSize: 16),
                  ),
                  onTap: () {
                    RouterUtils.pop(context, result: clickItem);
                    if (_onSelect != null) {
                      _onSelect(clickItem);
                    }
                  },
                )
              ],
            ),
          ),
          Container(
            height: 28,
            padding: EdgeInsets.only(top: 2),
            child: (StringUtils.isNotEmpty(widget.subTitle))
                ? Text(widget.subTitle,
                style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextMainColor_D0E0F7(),
                    fontSize: 12))
                : SizedBox(),
          ),
          Expanded(
              flex: 1,
              child: Stack(
                children: [
                  Container(
                    child: ScrollConfiguration(
                      behavior: MyBehavior(),
                      child: CupertinoPicker(
                        backgroundColor: bgColor??ThemeRepository.getInstance()
                            .getCardBgColor_21263C(),
                        diameterRatio: 1,
                        scrollController: proviceCotroller,
                        itemExtent: 31,
                        onSelectedItemChanged: (position) {
                          setState(() {
                            selectPosition = position;
                            clickItem = strList[position];
                          });
                        },
                        children: createEachItem(strList, selectPosition),
                      ),
                    ),
                  ),
                  if (widget.rightText != null)
                    Center(
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: widget.rightTextOffsetX ?? 100),
                        child: Text(
                          widget.rightText ?? '',
                          style: TextStyle(
                              fontSize: 19.0,
                              color: ThemeRepository.getInstance()
                                  .getTextMainColor_D0E0F7()),
                        ),
                      ),
                    )
                ],
              )),
          Padding(
            padding: EdgeInsets.only(bottom: 46.5),
          )
        ],
      ),
    );
  }

  List<Widget> createEachItem(List<String> data, int selectPosition) {
    List<Widget> target = [];

    for (String item in data) {
      target.add(Container(
        margin: EdgeInsets.only(top: 2.5, bottom: 2.5),
        child: Text(
          item ?? '',
          style: TextStyle(
              fontSize: 19.0,
              height: widget.textHeight ?? 1.3,
              color: itemColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
        ),
      ));
    }
    return target;
  }
}

import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class AddressAboutUtils{

  //经纬度拼接 E”表示东经 N”表示北纬
  static String getLongitudeAndLatitude(String longitude,String latitude){
    if(StringUtils.isNotEmpty(longitude)&&StringUtils.isNotEmpty(latitude)){
      return "${latitude?.toString()}°N,${longitude?.toString()}°E";
    }
    return "";
  }

  //刷脸地址省市区拼接
  static String getAddressSplicing(CompanyAddressListInfoBean itemBean){
    if(itemBean==null)return "";
    String addressStr = itemBean?.address;
    if(itemBean?.addrProvince == itemBean?.addrCity){
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrDistrict}${itemBean?.address}";
    }else{
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrCity}${itemBean?.addrDistrict}${itemBean?.address}";
    }
    return addressStr;
  }


  static String getBranchSplicing(CompanyBranchListInfoBean itemBean){
    if(itemBean==null)return "";
    String addressStr = itemBean?.address;
    if(itemBean?.addrProvince == itemBean?.addrCity){
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrDistrict}·${itemBean?.address}";
    }else{
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrCity}${itemBean?.addrDistrict}·${itemBean?.address}";
    }
    return addressStr;
  }

  static String getAddressStr() {
    CompanyInfoBean data = UserRepository.getInstance().userData?.companyInfo;
    String province = data?.addrProvince ?? "";
    String city = data?.addrCity ?? "";
    String district = data?.addrDistrict ?? "";
    return province == city ? "$province$district" : "$province$city$district";
  }

  static CompanyAddressListInfoBean getCompanyAddressListInfoBeanNew(){
    CompanyAddressListInfoBean itemBean = CompanyAddressListInfoBean();
    CompanyInfoBean data = UserRepository.getInstance().userData?.companyInfo;
    itemBean.address = data?.address;
    itemBean.addrProvince = data?.addrProvince ?? "";
    itemBean.addrCity = data?.addrCity ?? "";
    itemBean.addrDistrict = data?.addrDistrict ?? "";
    itemBean.gps = data.gps;
    return itemBean;
  }

  // static double clipGpsToDouble(String value) {
  //   //  判空 如果是空 返回
  //   //  判断value是否包含圈，不包含，直接return;
  //   //  包含，通过符号，截取到符号前的字符 reutrn;
  //   return  double.parse(xxx);
  // }
}
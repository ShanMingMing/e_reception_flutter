import 'package:flutter/cupertino.dart';

class VgColors{
  static const Color INPUT_BG_COLOR = Color(0xFF5E687C);
  static const Color DEFAULT_FIELD_COLOR = Color(0xFF808388);
}
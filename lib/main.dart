import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/generated/l10n.dart';
import 'package:e_reception_flutter/main_delegate.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/show_progress_notifier.dart';
import 'package:e_reception_flutter/utils/app_analysis.dart';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_inherited_widget/vg_inherited_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vg_base/vg_log_lib.dart';

import 'singleton/user_repository/user_repository.dart';
import 'ui/app_main.dart';
import 'utils/localization_delegate.dart';
import 'package:timezone/data/latest.dart' as tz;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///启动配置
  MainDelegate.getInstance().initState();

  ///加载用户
  await UserRepository.getInstance().loadCacheUserInfo();
  // runApp(MyApp());
  UserRepository.getInstance()
      .loginService(
      callback: VgBaseCallback(onSuccess: (val) {
        LogUtils.d("自动登录-----成功");
      }, onError: (msg) {
        LogUtils.d("自动登录------失败");
        LogUtils.e("失败原因------$msg");
      }), isBackIndexPage: false, isColdLogin: true)
      .autoLogin();
  tz.initializeTimeZones();
  runApp(MyApp());
  // ConstantRepository.of().initBugly(() => runApp(MyApp()));
  // ///加载组件
  // DoKit.runApp(
  //     app: DoKitApp(MyApp()),
  //     // 是否在release包内使用，默认release包会禁用
  //     useInRelease: false,
  //     releaseAction: () {
  //       // release模式下执行该函数，一些用到runZone之类实现的可以放到这里，该值为空则会直接调用系统的runApp(MyApp())，
  //       runApp(MyApp());
  //     });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUISetting.getStyleByPlatform(),
      child: VgInheritedWidget<UserDataBean>(
        initData: UserRepository.getInstance().userData,
        child: MaterialApp(
          title: 'AI前台',
          theme: ThemeData(
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily: "PingFang",
          ),
          builder: (BuildContext context, Widget child) {
            return MediaQuery(
              child: child,
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            );
          },
          // 设置语言
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            CupertinoLocalizationsDelegate(),
          ],
          // 讲zh设置为第一项,没有适配语言时，英语为首选项
          supportedLocales: S.delegate.supportedLocales,
          routes: {AppMain.ROUTER: (BuildContext context) => AppMain()},
          initialRoute: AppMain.ROUTER,
          //友盟监听
          navigatorObservers: [AppAnalysis()],
        ),
      ),
    );
  }
}

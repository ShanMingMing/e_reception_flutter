import 'package:e_reception_flutter/constants/account_constant.dart';
import 'package:e_reception_flutter/singleton/amap_repository/constant/amap_net_api.dart';
import 'package:e_reception_flutter/singleton/amap_repository/vo/amap_administratice_area_query_vo.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';


///此项控制行政区信息中返回行政区边界坐标点
enum AmapMoreType{
    all,
    base
}

///高德Web api接口
///文档：https://lbs.amap.com/api/webservice/guide/api/district
///
class AmapService{

    ///行政区域查询
    void administrativeAreaQuery({String keywords,String subdistrict,AmapMoreType extensions,BaseCallback callback}){
      Map<String,dynamic> map = {
        "key": AccountConstant.AMAP_WEB_KEY,
        "keywords":keywords ?? "",
        "subdistrict": subdistrict ?? "",
        "extensions": _getAmapMoreTypeStr(extensions) ?? "",
      };
       HttpUtils.get(url: AmapNetApi.ADMINISTRATIVE_AREA_QUERY_API,query: map).then((resp){
          AmapAdministraticeAreaQueryVo areaQueryVo = AmapAdministraticeAreaQueryVo.fromMap(resp?.data);
          if(areaQueryVo == null || areaQueryVo?.status != "1"){
            callback?.onError("查询城市失败");
            return;
          }
          callback?.onSuccess(areaQueryVo);
       });
    }

    String _getAmapMoreTypeStr(AmapMoreType extensions){
      if(extensions == null){
        return null;
      }
      return extensions.toString().split(".")[1];
    }
}
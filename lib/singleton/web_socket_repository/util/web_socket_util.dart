import 'dart:io';

import 'package:device_info/device_info.dart';

class WebSocketUtil {

  ///获取设备号
  static String mDeviceId;

  static Future<void> setDeviceId() async {
    mDeviceId = await getUniqueId();
    return null;
  }

  static Future<String> getUniqueId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      // print("ios唯一设备码："+iosDeviceInfo.identifierForVendor);
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      // print("android唯一设备码："+androidDeviceInfo.androidId);
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }
}

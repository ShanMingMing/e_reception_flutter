import 'dart:async';

import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

///自动注销用户流监听
mixin UserStreamMixin<T extends StatefulWidget> on BaseState<T>{

  StreamSubscription _streamSubscription;
  @override
  void initState() {
    super.initState();
  }

  ///添加监听
  addUserStreamListen(ValueChanged<UserDataBean> onListen){
    _streamSubscription = UserRepository.getInstance().userStream?.listen((UserDataBean userBean) {
      if(onListen != null){
        onListen(userBean);
      }
    });
  }


  @override
  void dispose() {
    _streamSubscription?.cancel();
    super.dispose();
  }
}
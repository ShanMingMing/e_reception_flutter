import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:flutter/widgets.dart';

import 'bean/user_response_bean.dart';
import 'service/user_login_service.dart';

///方便暴露常用接口(避免跳到UserRepository类中)
///
///tips:
///     1. UserBean获取方式为地址传值 修改后会影响系统级UserBean
///     2. userStream: 完整暴露出来，请正确使用避免造成问题
abstract class UserAdapter {
  ///登录服务接口
  ///callback: 自定义回调处理方式（设置缓存及通知所有更新都在内部处理）
  ///isBackIndexPage: 登录成功后是否需要回到首页
  ///isColdLogin: 是否冷启动登录
  UserLoginService loginService(
      {VgBaseCallback<UserDataBean> callback, bool isBackIndexPage = true, bool isColdLogin = false});

  ///通过InheritedWidget获取UserBean
  ///优点isListen == true 自动调取 didChangeDependencies() ->->-> build(_) 进行更新
  ///调用案例：
  /// @override
  /// void didChangeDependencies() {
  ///   _user = UserRepository.getInstance().of(context);
  ///   super.didChangeDependencies();
  /// }
  UserDataBean of(BuildContext context, {bool isListen = true});

  ///获取当前authId
  String get authId;

  ///获取最后一次浏览的shid
  String get lastshid;

  ///终端数量
  int get tercnt;

  void setLastShid(String shid);

  ///是否是智能家居行业
  bool isSmartHomeCompany();

  ///公司ID
  String get companyId;

  ///判断是否是登录状态
  bool isLogined();

  ///清除用户信息及缓存并通知
  Future<void> clearUserInfo();

  ///加载用户信息缓存
  Future<UserDataBean> loadCacheUserInfo();

  ///销毁
  void dispose();

  ///获取用户信息Stream
  Stream<UserDataBean> get userStream;

  ///直接获取用户信息（不要直接操作修改）
  ///修改需要走登录接口
  UserDataBean get userData;

  ///获取公司列表
  List<UserComListBean> get companyList;

  ///获取手机号
  String getPhone() {
    return userData?.comUser?.phone ?? '';
  }

  ///获取当前企业类型//公司类型 00民营企业 01培训机构 02政府单位 03 大中小学
  String getType() {
    return userData?.companyInfo?.type ?? '';
  }

  ///获取当前企业id
  String getCompanyId() {
    return userData?.companyInfo?.companyid ?? '';
  }

  ///获取当前企业logo
  String getCompanyLogo() {
    return userData?.companyInfo?.companylogo ?? '';
  }

  ///当前身份id
  String getCurrentRoleId() {
    return userData?.comUser?.roleid ?? '';
  }

  ///特殊后缀
  String getCacheKeySuffix() {
    return getPhone() + getCompanyId() + getCurrentRoleId();
  }
  
  ///获取首页终端以及打卡信息缓存key
  String getHomePunchAndTerminalInfoCacheKey(){
    return NetApi.HOME_PUNCH_AND_TERMINAL_INFO + (getCacheKeySuffix()??"");
  }
  
  ///获取首页企业信息缓存key
  String getHomeCompanyBaseInfoCacheKey(){
    return NetApi.HOME_COMPANY_BASE_INFO + (getCacheKeySuffix()??"");
  }

  ///获取首页企业信息与打卡信息缓存key
  String getHomeCompanyBaseInfoAndPunchInfoCacheKey(){
    return NetApi.HOME_COMPANY_BASE_INFO + (getCacheKeySuffix()??"");
  }

  ///获取首页终端信息缓存key
  String getHomeTerminalInfoCacheKey(){
    return NetApi.HOME_PUNCH_AND_TERMINAL_INFO + (getCacheKeySuffix()??"");
  }

  ///更新当前身份
  void updateCurrentRoleId(String roleId) {
    if (roleId == null) {
      return;
    }
    if (userData == null || userData.comUser == null) {
      return;
    }
    userData.comUser.roleid = roleId;
  }

  ///当前为企业
  bool isCompany() {
    return userData?.companyInfo?.isCompanyLike() ?? false;
  }

  ///当前为机构
  bool isOrganization() {
    return userData?.companyInfo?.isSchoolLike() ?? false;
  }
}

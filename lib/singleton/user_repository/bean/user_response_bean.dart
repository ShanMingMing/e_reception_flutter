import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"companyId":"cd191d29404e4a5dbb6ff2eb8bad48e9","companyInfo":{"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"be97b4557acc49a782c2e52b53bb77a1","createdate":1611712498,"updatetime":1611712498,"updateuid":"be97b4557acc49a782c2e52b53bb77a1","groupList":[{"groupName":"员工","groupid":"eadc02553f2a40509f05e407125f15e9","visitor":"01","recorded":"00"},{"groupName":"访客","groupid":"35fe36fc21004cfca1bccb7989fea7e1","visitor":"00","recorded":"01"}]},"comUser":{"fuid":"dfc25c6825c04278bc0ebe9ea7674497","type":"01","number":"","name":"王凡语","nick":"","phone":"18611999116","napicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","putpicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","createdate":1611886269,"createuid":"be97b4557acc49a782c2e52b53bb77a1","groupid":"eadc02553f2a40509f05e407125f15e9","status":"01","userid":"be97b4557acc49a782c2e52b53bb77a1","roleid":"99","city":null,"projectid":null,"departmentid":null,"delflg":"00"},"osUser":{"userid":"be97b4557acc49a782c2e52b53bb77a1","nick":"王凡语","loginphone":"18611999116","napicurl":null,"createdate":1611712498,"createuid":"be97b4557acc49a782c2e52b53bb77a1","lasttime":1615447044,"intro":null,"lastLoginCompanyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","delflg":"00"},"userComList":[{"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","roleid":"99","type":"00","companynick":"湛腾科技"},{"companyid":"5141eea68aa34926a5a2618c9e262b35","roleid":"99","type":"00","companynick":"0223新企业"}],"authId":"9e591f296d7a22399311e8bb1203afe9"}
/// extra : null

class UserResponseBean {
  bool success;
  String code;
  String msg;
  UserDataBean data;
  dynamic extra;

  static UserResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserResponseBean userResponseBeanBean = UserResponseBean();
    userResponseBeanBean.success = map['success'];
    userResponseBeanBean.code = map['code'];
    userResponseBeanBean.msg = map['msg'];
    userResponseBeanBean.data = UserDataBean.fromMap(map['data']);
    userResponseBeanBean.extra = map['extra'];
    return userResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// companyId : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// companyInfo : {"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"be97b4557acc49a782c2e52b53bb77a1","createdate":1611712498,"updatetime":1611712498,"updateuid":"be97b4557acc49a782c2e52b53bb77a1","groupList":[{"groupName":"员工","groupid":"eadc02553f2a40509f05e407125f15e9","visitor":"01","recorded":"00"},{"groupName":"访客","groupid":"35fe36fc21004cfca1bccb7989fea7e1","visitor":"00","recorded":"01"}]}
/// comUser : {"fuid":"dfc25c6825c04278bc0ebe9ea7674497","type":"01","number":"","name":"王凡语","nick":"","phone":"18611999116","napicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","putpicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","createdate":1611886269,"createuid":"be97b4557acc49a782c2e52b53bb77a1","groupid":"eadc02553f2a40509f05e407125f15e9","status":"01","userid":"be97b4557acc49a782c2e52b53bb77a1","roleid":"99","city":null,"projectid":null,"departmentid":null,"delflg":"00"}
/// osUser : {"userid":"be97b4557acc49a782c2e52b53bb77a1","nick":"王凡语","loginphone":"18611999116","napicurl":null,"createdate":1611712498,"createuid":"be97b4557acc49a782c2e52b53bb77a1","lasttime":1615447044,"intro":null,"lastLoginCompanyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","delflg":"00"}
/// userComList : [{"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","roleid":"99","type":"00","companynick":"湛腾科技"},{"companyid":"5141eea68aa34926a5a2618c9e262b35","roleid":"99","type":"00","companynick":"0223新企业"}]
/// authId : "9e591f296d7a22399311e8bb1203afe9"

class UserDataBean {
  String companyId;
  bool outFactoryFlg;
  CompanyInfoBean companyInfo;
  AllAndUnReadCntBean allAndUnReadCnt;
  ComUserBean comUser;
  OsUserBean user;
  ComLogoBean comLogo;
  List<UserComListBean> companyList;
  String authId;
  //最后一次浏览的shid
  String lastshid;
  int tercnt;
  //是否冷启动登录
  bool isColdLogin;


  static UserDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserDataBean dataBean = UserDataBean();
    dataBean.companyId = map['companyId'];
    dataBean.outFactoryFlg = map['outFactoryFlg'];
    dataBean.companyInfo = CompanyInfoBean.fromMap(map['companyInfo']);
    dataBean.comUser = ComUserBean.fromMap(map['comUser']);
    dataBean.allAndUnReadCnt = AllAndUnReadCntBean.fromMap(map['allAndUnReadCnt']);
    dataBean.user = OsUserBean.fromMap(map['osUser']);
    dataBean.comLogo = ComLogoBean.fromMap(map['comLogo']);
    dataBean.companyList = List()..addAll(
      (map['userComList'] as List ?? []).map((o) => UserComListBean.fromMap(o))
    );
    dataBean.authId = map['authId'];
    dataBean.lastshid = map['lastshid'];
    dataBean.tercnt = map['tercnt'];
    return dataBean;
  }

  Map toJson() => {
    "outFactoryFlg": outFactoryFlg,
    "companyId": companyId,
    "companyInfo": companyInfo,
    "comUser": comUser,
    "osUser": user,
    "comLogo": comLogo,
    "userComList": companyList,
    "authId": authId,
    "lastshid": lastshid,
    "tercnt": tercnt,
  };
}

/// allCnt : 17
/// unreadCnt : 17

class AllAndUnReadCntBean {
  int allCnt;
  int unreadCnt;

  static AllAndUnReadCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AllAndUnReadCntBean allAndUnReadCntBean = AllAndUnReadCntBean();
    allAndUnReadCntBean.allCnt = map['allCnt'];
    allAndUnReadCntBean.unreadCnt = map['unreadCnt'];
    return allAndUnReadCntBean;
  }

  Map toJson() => {
    "allCnt": allCnt,
    "unreadCnt": unreadCnt,
  };
}

/// companyid : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// roleid : "99"
/// type : "00"
/// companynick : "湛腾科技"

class UserComListBean {
  String companyid;
  String roleid;
  String type;
  String companynick;
  String companyname;
  String isimport;

  static UserComListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserComListBean userComListBean = UserComListBean();
    userComListBean.companyid = map['companyid'];
    userComListBean.roleid = map['roleid'];
    userComListBean.type = map['type'];
    userComListBean.companynick = map['companynick'];
    userComListBean.companyname = map['companyname'];
    userComListBean.isimport = map['isimport'];
    return userComListBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "roleid": roleid,
    "type": type,
    "companynick": companynick,
    "companyname": companyname,
    "isimport": isimport,
  };
}

/// companyid : "8899713cd2714b369e922207d71658d3"
/// logo : "http://etpic.we17.com/test/20210907140952_1418.jpg"
/// squareOpaqueLogo : "http://etpic.we17.com/test/20210907140952_1418.jpg"
/// squareTransparentLogo : "http://etpic.we17.com/test/20210907141006_9455.png"
/// oblongPortraitLogo : "-1"
/// oblongTransverseLogo : "-1"
/// createuid : null
/// createdate : null
/// updatetime : 1630995057
/// updateuid : "c7774a6c37f84ae58261acac27ff71db"
/// delflg : "00"
/// bjtime : "2021-09-07T14:09:52"
class ComLogoBean {
  String companyid;
  String logo;
  String squareOpaqueLogo;
  String squareTransparentLogo;
  String oblongPortraitLogo;
  String oblongTransverseLogo;
  dynamic createuid;
  dynamic createdate;
  int updatetime;
  String updateuid;
  String delflg;
  String bjtime;

  static ComLogoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComLogoBean comLogoBean = ComLogoBean();
    comLogoBean.companyid = map['companyid'];
    comLogoBean.logo = map['logo'];
    comLogoBean.squareOpaqueLogo = map['squareOpaqueLogo'];
    comLogoBean.squareTransparentLogo = map['squareTransparentLogo'];
    comLogoBean.oblongPortraitLogo = map['oblongPortraitLogo'];
    comLogoBean.oblongTransverseLogo = map['oblongTransverseLogo'];
    comLogoBean.createuid = map['createuid'];
    comLogoBean.createdate = map['createdate'];
    comLogoBean.updatetime = map['updatetime'];
    comLogoBean.updateuid = map['updateuid'];
    comLogoBean.delflg = map['delflg'];
    comLogoBean.bjtime = map['bjtime'];
    return comLogoBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "logo": logo,
    "squareOpaqueLogo": squareOpaqueLogo,
    "squareTransparentLogo": squareTransparentLogo,
    "oblongPortraitLogo": oblongPortraitLogo,
    "oblongTransverseLogo": oblongTransverseLogo,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "bjtime": bjtime,
  };
}

/// userid : "be97b4557acc49a782c2e52b53bb77a1"
/// nick : "王凡语"
/// loginphone : "18611999116"
/// napicurl : null
/// createdate : 1611712498
/// createuid : "be97b4557acc49a782c2e52b53bb77a1"
/// lasttime : 1615447044
/// intro : null
/// lastLoginCompanyid : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// delflg : "00"

class OsUserBean {
  String userid;
  String nick;
  String loginphone;
  dynamic napicurl;
  int createdate;
  String createuid;
  int lasttime;
  dynamic intro;
  String lastLoginCompanyid;
  String delflg;
  String changeflg;
  //hangeflg  切换企业 00未切换过 01切换过',

  static OsUserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    OsUserBean osUserBean = OsUserBean();
    osUserBean.userid = map['userid'];
    osUserBean.nick = map['nick'];
    osUserBean.loginphone = map['loginphone'];
    osUserBean.napicurl = map['napicurl'];
    osUserBean.createdate = map['createdate'];
    osUserBean.createuid = map['createuid'];
    osUserBean.lasttime = map['lasttime'];
    osUserBean.intro = map['intro'];
    osUserBean.lastLoginCompanyid = map['lastLoginCompanyid'];
    osUserBean.delflg = map['delflg'];
    osUserBean.changeflg = map['changeflg'];
    return osUserBean;
  }

  Map toJson() => {
    "userid": userid,
    "nick": nick,
    "loginphone": loginphone,
    "napicurl": napicurl,
    "createdate": createdate,
    "createuid": createuid,
    "lasttime": lasttime,
    "intro": intro,
    "lastLoginCompanyid": lastLoginCompanyid,
    "delflg": delflg,
    "changeflg": changeflg,
  };

  @override
  String toString() {
    return 'OsUserBean{userid: $userid, nick: $nick, loginphone: $loginphone, napicurl: $napicurl, createdate: $createdate, createuid: $createuid, lasttime: $lasttime, intro: $intro, lastLoginCompanyid: $lastLoginCompanyid, delflg: $delflg}';
  }
}

/// fuid : "dfc25c6825c04278bc0ebe9ea7674497"
/// type : "01"
/// number : ""
/// name : "王凡语"
/// nick : ""
/// phone : "18611999116"
/// napicurl : "http://etpic.we17.com/test/20210129101109_2814.jpg"
/// putpicurl : "http://etpic.we17.com/test/20210129101109_2814.jpg"
/// companyid : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// createdate : 1611886269
/// createuid : "be97b4557acc49a782c2e52b53bb77a1"
/// groupid : "eadc02553f2a40509f05e407125f15e9"
/// status : "01"
/// userid : "be97b4557acc49a782c2e52b53bb77a1"
/// roleid : "99"
/// city : null
/// projectid : null
/// departmentid : null
/// delflg : "00"

class ComUserBean {
  String fuid;
  String type;
  String number;
  String name;
  String nick;
  String phone;
  String napicurl;
  String putpicurl;
  String companyid;
  int createdate;
  String createuid;
  String groupid;
  String status;
  String userid;
  String roleid;
  dynamic city;
  dynamic projectid;
  dynamic departmentid;
  String delflg;
  int punchDayCnt;

  bool isNotSale(){
    return "02" != delflg;
  }

  bool isSale(){
    return !isNotSale();
  }

  static ComUserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComUserBean comUserBean = ComUserBean();
    comUserBean.fuid = map['fuid'];
    comUserBean.type = map['type'];
    comUserBean.number = map['number'];
    comUserBean.name = map['name'];
    comUserBean.nick = map['nick'];
    comUserBean.phone = map['phone'];
    comUserBean.napicurl = map['napicurl'];
    comUserBean.putpicurl = map['putpicurl'];
    comUserBean.companyid = map['companyid'];
    comUserBean.createdate = map['createdate'];
    comUserBean.createuid = map['createuid'];
    comUserBean.groupid = map['groupid'];
    comUserBean.status = map['status'];
    comUserBean.userid = map['userid'];
    comUserBean.roleid = map['roleid'];
    comUserBean.city = map['city'];
    comUserBean.projectid = map['projectid'];
    comUserBean.departmentid = map['departmentid'];
    comUserBean.delflg = map['delflg'];
    comUserBean.punchDayCnt = map['punchDayCnt'];
    return comUserBean;
  }

  Map toJson() => {
    "fuid": fuid,
    "type": type,
    "number": number,
    "name": name,
    "nick": nick,
    "phone": phone,
    "napicurl": napicurl,
    "putpicurl": putpicurl,
    "companyid": companyid,
    "createdate": createdate,
    "createuid": createuid,
    "groupid": groupid,
    "status": status,
    "userid": userid,
    "roleid": roleid,
    "city": city,
    "projectid": projectid,
    "departmentid": departmentid,
    "delflg": delflg,
    "punchDayCnt" : punchDayCnt,
  };

  @override
  String toString() {
    return 'ComUserBean{fuid: $fuid, type: $type, number: $number, name: $name, nick: $nick, phone: $phone, napicurl: $napicurl, putpicurl: $putpicurl, companyid: $companyid, createdate: $createdate, createuid: $createuid, groupid: $groupid, status: $status, userid: $userid, roleid: $roleid, city: $city, projectid: $projectid, departmentid: $departmentid, delflg: $delflg ,punchDayCnt: $punchDayCnt}';
  }
}

/// companyid : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// status : "00"
/// companyname : "北京湛腾世纪科技有限公司"
/// companynick : "湛腾科技"
/// companylogo : null
/// type : "00"
/// city : "{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}"
/// gps : null
/// address : null
/// createuid : "be97b4557acc49a782c2e52b53bb77a1"
/// createdate : 1611712498
/// updatetime : 1611712498
/// updateuid : "be97b4557acc49a782c2e52b53bb77a1"
/// groupList : [{"groupName":"员工","groupid":"eadc02553f2a40509f05e407125f15e9","visitor":"01","recorded":"00"},{"groupName":"访客","groupid":"35fe36fc21004cfca1bccb7989fea7e1","visitor":"00","recorded":"01"}]

class CompanyInfoBean {
  String companyid;
  String status;//00审核中 01审核过了 02审核被拒绝 03注销
  String companyname;
  String companynick;
  dynamic companylogo;
  String type;
  String registionedition;
  dynamic gps;
  dynamic addrProvince;
  dynamic addrCity;
  dynamic addrDistrict;
  dynamic addrCode;
  String enableFaceRecognition;//启用人脸识别 00启用 01关闭
  String comefrom;//公司来源  00AI前台  01一起学
  dynamic address;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;
  List<GroupListBean> groupList;
  String appedition;//00基础版、01海报版、02人脸版、03智慧版
  //00 无 01智能家居
  String trade;
  //播放切换时间
  int cpsecond;

  //企业被注销or审核被拒绝需要强制退出
  bool isNeedForceLogout(){
    return ("02" == status) || ("03" == status);
  }


  //设备详情是否展示背景音乐布局
  //基础版 和 人脸版的企业，终端详情顶部增加背景音乐横条
  bool isShowMusicLayout(){
    return ("00" == appedition) || ("02" == appedition);
  }

  bool isShowFaceRecognition(){
    if("00" == status){
      //审核中的企业类型判断
      if(StringUtils.isEmpty(registionedition)){
        return false;
      }
      if(("00" == registionedition) || ("01" == registionedition)){
        return false;
      }
      return true;
    }else{
      if(StringUtils.isEmpty(appedition)){
        return false;
      }
      if(("00" == appedition) || ("01" == appedition)){
        return false;
      }
      return true;
    }
  }

  bool isShowEndDay(){
    if("00" == status){
      //审核中的企业类型判断
      if(StringUtils.isEmpty(registionedition)){
        return false;
      }
      if(("01" == registionedition) || ("03" == registionedition)){
        return true;
      }
      return false;
    }else{
      if(StringUtils.isEmpty(appedition)){
        return false;
      }
      if(("01" == appedition) || ("03" == appedition)){
        return true;
      }
      return false;
    }
  }

  static CompanyInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyInfoBean companyInfoBean = CompanyInfoBean();
    companyInfoBean.companyid = map['companyid'];
    companyInfoBean.status = map['status'];
    companyInfoBean.companyname = map['companyname'];
    companyInfoBean.companynick = map['companynick'];
    companyInfoBean.companylogo = map['companylogo'];
    companyInfoBean.type = map['type'];
    companyInfoBean.registionedition = map['registionedition'];
    companyInfoBean.gps = map['gps'];
    companyInfoBean.addrProvince = map['addrProvince'];
    companyInfoBean.addrCity = map['addrCity'];
    companyInfoBean.addrDistrict = map['addrDistrict'];
    companyInfoBean.addrCode = map['addrCode'];
    companyInfoBean.enableFaceRecognition = map['enableFaceRecognition'];
    companyInfoBean.comefrom = map['comefrom'];
    companyInfoBean.address = map['address'];
    companyInfoBean.createuid = map['createuid'];
    companyInfoBean.createdate = map['createdate'];
    companyInfoBean.updatetime = map['updatetime'];
    companyInfoBean.cpsecond = map['cpsecond'];
    companyInfoBean.updateuid = map['updateuid'];
    companyInfoBean.appedition = map['appedition'];
    companyInfoBean.trade = map['trade'];
    companyInfoBean.groupList = List()..addAll(
      (map['groupList'] as List ?? []).map((o) => GroupListBean.fromMap(o))
    );
    return companyInfoBean;
  }

  ///从企业主页更新数据
   CompanyInfoBean update(Map<String, dynamic> map) {
    if (map == null) return null;
    companyid = map['companyid'];
    status = map['status'];
    companyname = map['companyname'];
    companynick = map['companynick'];
    companylogo = map['companylogo'];
    type = map['type'];
    registionedition = map['registionedition'];
    gps = map['gps'];
    addrProvince = map['addrProvince'];
    addrCity = map['addrCity'];
    addrDistrict = map['addrDistrict'];
    addrCode = map['addrCode'];
    // enableFaceRecognition = map['enableFaceRecognition'];
    address = map['address'];
    createuid = map['createuid'];
    createdate = map['createdate'];
    updatetime = map['updatetime'];
    updateuid = map['updateuid'];
    cpsecond = map['cpsecond'];
    return this;
  }

  Map toJson() => {
    "companyid": companyid,
    "status": status,
    "companyname": companyname,
    "companynick": companynick,
    "companylogo": companylogo,
    "type": type,
    "registionedition" : registionedition,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "addrCode": addrCode,
    "enableFaceRecognition": enableFaceRecognition,
    "comefrom": comefrom,
    "address": address,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "groupList": groupList,
    "appedition": appedition,
    "trade": trade,
    "cpsecond": cpsecond,
  };

  @override
  String toString() {
    return 'CompanyInfoBean{companyid: $companyid, status: $status, companyname: $companyname,enableFaceRecognition:$enableFaceRecognition ,companynick: $companynick, companylogo: $companylogo, type: $type, gps: $gps, address: $address, createuid: $createuid, createdate: $createdate, updatetime: $updatetime, cpsecond: $cpsecond, updateuid: $updateuid, groupList: $groupList}';
  }


  /// 企业、单位
  /// S01机构 S02国有企业 S03民营企业 S06外资企业 S04单位 S05学校
  bool isCompanyLike(){
    return type == "S02"||type=="S03"||type=="S06"||type =="S05"||type =="S04";
  }

  ///学校、培训机构
  bool isSchoolLike(){
    return type == "S01"||type=="S05";
  }

  ///地址信息
  void setAddressInfo({String address, String code, String gps, String province, String city, String district}){
    if(StringUtils.isNotEmpty(address)) this.address = address;
    if(StringUtils.isNotEmpty(code)) this.addrCode = code;
    if(StringUtils.isNotEmpty(gps)) this.gps = gps;
    if(StringUtils.isNotEmpty(province)) this.addrProvince = province;
    if(StringUtils.isNotEmpty(city)) this.addrCity = city;
    if(StringUtils.isNotEmpty(district)) this.addrDistrict = district;
  }
}

/// groupName : "员工"
/// groupid : "eadc02553f2a40509f05e407125f15e9"
/// visitor : "01"
/// recorded : "00"

class GroupListBean {
  String groupName;
  String groupid;
  String visitor;
  String recorded;
  //04员工 01访客 02学员 03家长 99普通
  String groupType;

  static GroupListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GroupListBean groupListBean = GroupListBean();
    groupListBean.groupName = map['groupName'];
    groupListBean.groupid = map['groupid'];
    groupListBean.visitor = map['visitor'];
    groupListBean.recorded = map['recorded'];
    groupListBean.groupType = map['groupType'];
    return groupListBean;
  }

  Map toJson() => {
    "groupName": groupName,
    "groupid": groupid,
    "visitor": visitor,
    "recorded": recorded,
    "groupType": groupType,
  };

  bool isStaff(){
    return groupType == "04";
  }

  bool isStudent(){
    return groupType == "02";
  }

  @override
  String toString() {
    return 'GroupListBean{groupName: $groupName, groupid: $groupid, visitor: $visitor, recorded: $recorded, groupType: $groupType}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GroupListBean &&
          runtimeType == other.runtimeType &&
          groupid == other.groupid;

  @override
  int get hashCode => groupid.hashCode;

  isParent() {
    return groupType == "03";
  }
}
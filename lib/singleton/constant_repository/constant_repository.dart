import 'package:e_reception_flutter/constants/branch/branch_appstore_constant.dart';
import 'package:e_reception_flutter/constants/branch/branch_test_constant.dart';
import 'package:e_reception_flutter/constants/branch/main_constant.dart';
import 'package:e_reception_flutter/net/server_api.dart';

///常量测试服/正式服区分变量引用
class ConstantRepository{

  static ConstantRepository _instance;

  ConstantRepository._();

  static ConstantRepository _getInstance() {
    if (_instance == null) {
      _instance = new ConstantRepository._();
    }
    return _instance;
  }
  
  static MainConstant of(){
    if(ConstantRepository._getInstance().constant == null){
      ConstantRepository._getInstance().init();
    }
    return ConstantRepository._getInstance().constant;
  }

  ///局部成员

  //开关变量
  MainConstant _mainConstant;

  MainConstant get constant => _mainConstant;

  init(){
    assert(ServerApi.CURRENT_TYPE != null ,"当前版本类型无法指定");
    if(ServerApi.CURRENT_TYPE == VersionType.appstore){
      _mainConstant = BranchAppstoreConstant();
    }else if(ServerApi.CURRENT_TYPE == VersionType.test){
      _mainConstant = BranchTestConstant();
    }else{
      throw NullThrownError();
    }
  }

  dispose(){
    _mainConstant = null;
    _instance = null;
  }
}
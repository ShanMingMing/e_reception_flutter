import 'package:flutter/foundation.dart';
import 'package:sharesdk_plugin/sharesdk_plugin.dart';

class ShareParamsBean {
  String title;
  String text;
  final dynamic images;
  final String imageUrlAndroid;
  final String imagePathAndroid;
  final String url;
  final String titleUrlAndroid;
  final String musicUrlAndroid;
  final String videoUrlAndroid;
  final String filePath;
  final SSDKContentType contentType;

  ShareParamsBean(
      {this.title,
      this.text,
      this.images,
      this.imageUrlAndroid,
      this.imagePathAndroid,
      this.url,
      this.titleUrlAndroid,
      this.musicUrlAndroid,
      this.videoUrlAndroid,
      this.filePath,
      @required this.contentType});
}

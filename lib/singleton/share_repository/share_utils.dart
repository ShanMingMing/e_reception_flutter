import 'dart:io';
import 'package:sharesdk_plugin/sharesdk_plugin.dart';


class ShareUtils {
  ///是否安装微信
  static Future<bool> isInstalledWx() async {
    var result = await SharesdkPlugin.isClientInstalled(
        ShareSDKPlatforms.wechatTimeline);
    if (Platform.isAndroid && result is Map && result['state'] == "installed") {
      return true;
    } else if (Platform.isIOS && result is bool && result) {
      return true;
    }
    return false;
  }

  ///是否安装微信朋友圈
  static Future<bool> isInstalledWxTimeLine() async {
    var result = await SharesdkPlugin.isClientInstalled(
        ShareSDKPlatforms.wechatTimeline);
    if (Platform.isAndroid && result is Map && result['state'] == "installed") {
      return true;
    } else if (Platform.isIOS && result is bool && result) {
      return true;
    }
    return false;
  }

  ///是否安装了qq
  static Future<bool> isInstallQQ() async {
    var result = await SharesdkPlugin.isClientInstalled(
        ShareSDKPlatforms.qq);
    if (Platform.isAndroid && result is Map && result['state'] == "installed") {
      return true;
    } else if (Platform.isIOS && result is bool && result) {
      return true;
    }
    return false;
  }

  ///是否安装了qq空间
  static Future<bool> isInstallQzone() async {
    var result = await SharesdkPlugin.isClientInstalled(
        ShareSDKPlatforms.qZone);
    if (Platform.isAndroid && result is Map && result['state'] == "installed") {
      return true;
    } else if (Platform.isIOS && result is bool && result) {
      return true;
    }
    return false;
  }
}


import 'area_data.dart';

class LocationData {
  List<LocalProvinceBean> list;

  static LocationData fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocationData locationDataBean = LocationData();
    locationDataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => LocalProvinceBean.fromMap(o))
    );
    return locationDataBean;
  }

  Map toJson() => {
    "list": list,
  };
}



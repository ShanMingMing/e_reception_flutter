/// sid : "110000"
/// sname : "北京市"
/// city : [{"sid":"110100","sname":"北京市","site":[{"sid":"110101","sname":"东城区","la":39.917544,"lo":116.418757},{"sid":"110102","sname":"西城区","la":39.915309,"lo":116.366794},{"sid":"110105","sname":"朝阳区","la":39.921489,"lo":116.486409},{"sid":"110106","sname":"丰台区","la":39.863642,"lo":116.286968},{"sid":"110107","sname":"石景山区","la":39.914601,"lo":116.195445},{"sid":"110108","sname":"海淀区","la":39.956074,"lo":116.310316},{"sid":"110109","sname":"门头沟区","la":39.937183,"lo":116.105381},{"sid":"110111","sname":"房山区","la":39.735535,"lo":116.139157},{"sid":"110112","sname":"通州区","la":39.902486,"lo":116.658603},{"sid":"110113","sname":"顺义区","la":40.128936,"lo":116.653525},{"sid":"110114","sname":"昌平区","la":40.218085,"lo":116.235906},{"sid":"110115","sname":"大兴区","la":39.728908,"lo":116.338033},{"sid":"110116","sname":"怀柔区","la":40.324272,"lo":116.637122},{"sid":"110117","sname":"平谷区","la":40.144783,"lo":117.112335},{"sid":"110118","sname":"密云区","la":40.377362,"lo":116.843352},{"sid":"110119","sname":"延庆区","la":40.465325,"lo":115.985006}],"la":39.904989,"lo":116.405285}]
/// la : 39.904989
/// lo : 116.405285

class LocalProvinceBean {
  String sid;
  String sname;
  List<LocalCityBean> city;
  double la;
  double lo;


  LocalProvinceBean({this.sid, this.sname, this.city, this.la, this.lo});

  static LocalProvinceBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocalProvinceBean locationDataBean = LocalProvinceBean();
    locationDataBean.sid = map['sid'];
    locationDataBean.sname = map['sname'];
    locationDataBean.city = List()..addAll(
      (map['city'] as List ?? []).map((o) => LocalCityBean.fromMap(o))
    );
    locationDataBean.la = map['la'];
    locationDataBean.lo = map['lo'];
    return locationDataBean;
  }

  Map toJson() => {
    "sid": sid,
    "sname": sname,
    "city": city,
    "la": la,
    "lo": lo,
  };
}

/// sid : "110100"
/// sname : "北京市"
/// site : [{"sid":"110101","sname":"东城区","la":39.917544,"lo":116.418757},{"sid":"110102","sname":"西城区","la":39.915309,"lo":116.366794},{"sid":"110105","sname":"朝阳区","la":39.921489,"lo":116.486409},{"sid":"110106","sname":"丰台区","la":39.863642,"lo":116.286968},{"sid":"110107","sname":"石景山区","la":39.914601,"lo":116.195445},{"sid":"110108","sname":"海淀区","la":39.956074,"lo":116.310316},{"sid":"110109","sname":"门头沟区","la":39.937183,"lo":116.105381},{"sid":"110111","sname":"房山区","la":39.735535,"lo":116.139157},{"sid":"110112","sname":"通州区","la":39.902486,"lo":116.658603},{"sid":"110113","sname":"顺义区","la":40.128936,"lo":116.653525},{"sid":"110114","sname":"昌平区","la":40.218085,"lo":116.235906},{"sid":"110115","sname":"大兴区","la":39.728908,"lo":116.338033},{"sid":"110116","sname":"怀柔区","la":40.324272,"lo":116.637122},{"sid":"110117","sname":"平谷区","la":40.144783,"lo":117.112335},{"sid":"110118","sname":"密云区","la":40.377362,"lo":116.843352},{"sid":"110119","sname":"延庆区","la":40.465325,"lo":115.985006}]
/// la : 39.904989
/// lo : 116.405285

class LocalCityBean {
  String sid;
  String sname;
  List<LocalSiteBean> site;
  double la;
  double lo;


  LocalCityBean({this.sid, this.sname, this.site, this.la, this.lo});

  static LocalCityBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocalCityBean cityBean = LocalCityBean();
    cityBean.sid = map['sid'];
    cityBean.sname = map['sname'];
    cityBean.site = List()..addAll(
      (map['site'] as List ?? []).map((o) => LocalSiteBean.fromMap(o))
    );
    cityBean.la = map['la'];
    cityBean.lo = map['lo'];
    return cityBean;
  }

  Map toJson() => {
    "sid": sid,
    "sname": sname,
    "site": site,
    "la": la,
    "lo": lo,
  };
}

/// sid : "110101"
/// sname : "东城区"
/// la : 39.917544
/// lo : 116.418757

class LocalSiteBean {
  String sid;
  String sname;
  double la;
  double lo;

  static LocalSiteBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocalSiteBean siteBean = LocalSiteBean();
    siteBean.sid = map['sid'];
    siteBean.sname = map['sname'];
    siteBean.la = map['la'];
    siteBean.lo = map['lo'];
    return siteBean;
  }

  Map toJson() => {
    "sid": sid,
    "sname": sname,
    "la": la,
    "lo": lo,
  };
}
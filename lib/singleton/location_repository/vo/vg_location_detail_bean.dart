// import 'package:flutter/widgets.dart';
// import 'package:vg_base/vg_string_util_lib.dart';
//
// ///地理详情等级
// enum LocationDetailLevelType{
//   province,
//   city,
//   district,
// }
//
// // extension LocationDetailLevelTypeExtension on String{
// //
// //   ///字符转地理详情等级
// //   LocationDetailLevelType transToLocationDetailLevelType(){
// //     if(this == null || this.isEmpty){
// //       return null;
// //     }
// //     for(final LocationDetailLevelType item in LocationDetailLevelType.values){
// //       if(item == null){
// //         continue;
// //       }
// //       if(item.toString().split(".")[1] == this){
// //         return item;
// //       }
// //     }
// //     return null;
// //   }
// // }
//
// class VgLocationDetailBean {
//   VgLocationDetailBean({
//     @required this.level,
//     @required this.address,
//     @required this.la,
//     @required this.lo,
//     @required this.altitude,
//     @required this.bearing,
//     @required this.country,
//     @required this.province,
//     @required this.city,
//     @required this.cityCode,
//     @required this.adCode,
//     @required this.district,
//     @required this.poiName,
//     @required this.street,
//     @required this.streetNumber,
//     @required this.aoiName,
//     @required this.accuracy,
//     @required this.speed,
//   });
//
//   ///字符转地理详情等级
//   static LocationDetailLevelType transToLocationDetailLevelType(String level){
//     if(level == null || level.isEmpty){
//       return null;
//     }
//     for(final LocationDetailLevelType item in LocationDetailLevelType.values){
//       if(item == null){
//         continue;
//       }
//       if(item.toString().split(".")[1] == level){
//         return item;
//       }
//     }
//     return null;
//   }
//
//   ///当前等级（手动标记）
//   LocationDetailLevelType level;
//
//   /// 地址全称
//   String address;
//
//   /// 经度
//   String la;
//
//   /// 纬度
//   String lo;
//
//   /// 海拔
//   double altitude;
//
//   /// 设备朝向/移动方向
//   double bearing;
//
//   /// 国家
//   String country;
//
//   /// 省份
//   String province;
//
//   /// 城市
//   String city;
//
//   /// 城市编号
//   String cityCode;
//
//   /// 邮编
//   String adCode;
//
//   /// 区域
//   String district;
//
//   /// poi名称
//   String poiName;
//
//   /// 街道
//   String street;
//
//   /// 街道号
//   String streetNumber;
//
//   /// aoi名称
//   String aoiName;
//
//   /// 精度
//   double accuracy;
//
//   /// 速度
//   double speed;
//
//   ///获取省-城市-区
//   ///showToLevel 显示到等级
//   ///split 分割线
//   ///emptyStr 空显示
//   String getLocalNamesStr({LocationDetailLevelType showToLevel,String split,String emptyStr}){
//     List<String> strList = List();
//     //todo 待封装
//     return null;
//   }
//
//   ///得到省+城市拼串 直辖市显示一个（北京市北京市 -> 北京市）
//   String getProvinceAndCityStr(){
//     StringBuffer buffer = StringBuffer();
//     if(StringUtils.isNotEmpty(province)){
//       buffer.write(province);
//     }
//     if(StringUtils.isEmpty(city) || buffer.toString() == city){
//       return buffer?.toString();
//     }
//     buffer.write(city);
//     return buffer.toString();
//   }
//
//   @override
//   String toString() {
//     return 'Location{\nlevel: ${level?.toString()}\naddress: $address, \nlatLng: $la, $lo, \naltitude: $altitude, \nbearing: $bearing, \ncountry: $country, \nprovince: $province, \ncity: $city, \ncityCode: $cityCode, \nadCode: $adCode, \ndistrict: $district, \npoiName: $poiName, \nstreet: $street, \nstreetNumber: $streetNumber, \naoiName: $aoiName, \naccuracy: $accuracy\n}';
//   }
//
// }

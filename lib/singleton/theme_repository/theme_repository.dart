import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:flutter/material.dart';

///主题控制
class ThemeRepository{

  static ThemeRepository _instance;

  ThemeRepository._(){
    _init();
  }

  static ThemeRepository getInstance() {
    if (_instance == null) {
      _instance = new ThemeRepository._();
    }
    return _instance;
  }

  ///App全局主题
  ThemeData _appGlobalTheme;
  ThemeData get appGlobalTheme => _appGlobalTheme ?? _defaultThemeData();

  ///初始化
  _init() {
    if (_appGlobalTheme == null) {
      _appGlobalTheme = _customGlobalThemeData();
    }
  }

  ///默认主题
  ThemeData _defaultThemeData(){
    return ThemeData();
  }

  ///自定义主题
  ThemeData _customGlobalThemeData(){
      const Color primaryColor = ColorConstants.MAIN_COLOR;
      final ThemeData base = new ThemeData.light();
      return base.copyWith(
        primaryColor: primaryColor,
        // buttonColor: primaryColor,
        // indicatorColor: Colors.white,
        // accentColor: const Color(0xFF13B9FD),
        // canvasColor: const Color(0xFF202124),
        // scaffoldBackgroundColor: const Color(0xFF202124),
        // backgroundColor: const Color(0xFF202124),
        // errorColor: const Color(0xFFB00020),
        // buttonTheme: const ButtonThemeData(
        //   textTheme: ButtonTextTheme.primary,
        // ),
        // textTheme: _buildTextTheme(base.textTheme),
        // primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
        // accentTextTheme: _buildTextTheme(base.accentTextTheme),
      );
  }


  ///获取主题色
  ///#FF1890FF
  Color getPrimaryColor_1890FF(){
    if(_appGlobalTheme == null){
      _init();
    }
    return _appGlobalTheme?.primaryColor;
  }

  ///背景/分隔条
  ///#FF191E31
  Color getBgOrSplitColor_191E31(){
    return ColorConstants.BG_OR_SPLIT_COLOR;
  }

  ///主卡片背景getPrimaryColor_1890FF
  ///#FF21263C
  Color getCardBgColor_21263C(){
    return ColorConstants.CARD_BG_COLOR;
  }

  ///辅色红色
  ///#FFF95355
  Color getMinorRedColor_F95355(){
    return ColorConstants.MINOR_RED_COLOR;
  }

  ///辅色黄色
  ///#FFFFB714
  Color getMinorYellowColor_FFB714(){
    return ColorConstants.MINOR_YELLOW_COLOR;
  }

  ///默认灰色
  ///#FFF95355
  Color getDefaultGrayColor_FF5E687C(){
    return ColorConstants.MINOR_Gray_COLOR;
  }

  ///主要文字颜色
  ///#FFD0E0F7
  Color getTextMainColor_D0E0F7(){
    return TextColorConstants.TEXT_MAIN_COLOR;
  }

  ///次要文字灰色
  ///#FF808388
  Color getTextMinorGreyColor_808388(){
    return TextColorConstants.TEXT_MINOR_GREY_COLOR;
  }

  ///输入框内提示文字
  ///#FF3A3F50
  Color getTextEditHintColor_3A3F50(){
    return Color(0xFF3A3F50);
  }

  ///输入框内提示文字
  ///#FF5e687c
  Color getTextEditHintColor(){
    return Color(0xff5e687c);
  }

  ///#FFBFC2CC
  Color getTextBFC2CC_BFC2CC(){
    return Color(0xFFBFC2CC);
  }

  ///线条颜色
  ///#FF3A3F50
  Color getLineColor_3A3F50(){
    return ColorConstants.LINE_COLOR;
  }

  ///#FF00C6C4
  Color getHintGreenColor_00C6C4(){
    return Color(0xFF00C6C4);
  }

  ///#FF5E687C
  Color getHintGreenColor_5E687C(){
    return Color(0xFF5E687C);
  }

  ///#FFD0E0F7
  Color getTextColor_D0E0F7(){
    return Color(0xFFD0E0F7);
  }

  ///#FF808388
  Color getSmUnSelectedColor_808388(){
    return TextColorConstants.TEXT_MINOR_GREY_COLOR;
  }

  ///#FF1890FF
  Color getSmSelectedColor_1890FF(){
    return ColorConstants.MAIN_COLOR;
  }


}
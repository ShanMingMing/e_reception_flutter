import 'dart:ui';

///颜色常量
class ColorConstants {

  ///主色
  ///#FF1890FF
  static const Color MAIN_COLOR = Color(0xFF1890FF);

  ///背景/分割条
  ///#FF191E31
  static const Color BG_OR_SPLIT_COLOR = Color(0xFF191E31);

  ///主卡片背景
  ///#FF21263C
  static const Color CARD_BG_COLOR = Color(0xFF21263C);

  ///辅助红色
  ///#FFF95355
  static const Color MINOR_RED_COLOR = Color(0xFFF95355);

  ///辅助黄色
  ///#FFFFB714
  static const Color MINOR_YELLOW_COLOR = Color(0xFFFFB714);

  ///m默认灰色
  ///#FF5E687C
  static const Color MINOR_Gray_COLOR = Color(0xFF5E687C);

  ///线条颜色
  ///#FF3A3F50
  static const Color LINE_COLOR = Color(0xFF3A3F50);

  ///底部弹窗遮罩颜色
  ///#99000000
  static const Color BOTTOM_SHEET_BARRIER_COLOR =  Color(0x99000000);

  ///导航颜色
  static const Color NAV_COLOR = Color(0x993D4051);

  ///导航颜色
  static const Color NAV_COLOR_WHITE = Color(0xFFFFFFFF);

}

///文字颜色常量
class TextColorConstants{

  ///主要文字
  ///#FFD0E0F7
  static const Color TEXT_MAIN_COLOR = Color(0xFFD0E0F7);

  ///次要灰色文字
  ///#FF808388
  static const Color TEXT_MINOR_GREY_COLOR = Color(0xFF808388);

  ///输入框内提示文字
  static const Color TEXT_EDIT_HINT_COLOR = Color(0xFF5E687C);
}
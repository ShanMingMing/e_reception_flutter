import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class CustomAnnotatedRegion extends StatelessWidget {
  final Brightness navigationBarIconBrightness;
  final Color navigationBarColor;
  final Widget child;

  CustomAnnotatedRegion(this.child, {this.navigationBarColor, this.navigationBarIconBrightness});

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarBrightness: Brightness.dark,
          systemNavigationBarIconBrightness:navigationBarIconBrightness??Brightness.dark,
          systemNavigationBarColor: navigationBarColor ??
              ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
      child: child,
    );
  }
}

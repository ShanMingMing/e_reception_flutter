import 'dart:io';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class LightAnnotatedRegion extends StatelessWidget {
  final Widget child;

  LightAnnotatedRegion({this.child});

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isIOS?
      SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.light)
          :SystemUISetting.light,
      child: child,
    );
  }
}

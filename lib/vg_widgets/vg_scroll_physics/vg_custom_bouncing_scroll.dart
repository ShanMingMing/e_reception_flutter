
import 'package:flutter/material.dart';


///禁掉上回弹效果
class VgBanPullBouncingScroll extends BouncingScrollPhysics {

  /// Creates scroll physics that bounce back from the edge.

  const VgBanPullBouncingScroll({ScrollPhysics parent}) : super(parent: parent);

  @override

  VgBanPullBouncingScroll applyTo(ScrollPhysics ancestor) {

    return VgBanPullBouncingScroll(parent: buildParent(ancestor));

  }

// 重构弹性范围，只有当上滑的时候才有弹性，下拉去除

  @override

  double applyBoundaryConditions(ScrollMetrics position, double value) {

    if (value < position.pixels &&

        position.pixels <= position.minScrollExtent) // underscroll

      return value - position.pixels;

    if (value < position.minScrollExtent &&

        position.minScrollExtent < position.pixels) // hit top edge

      return value - position.minScrollExtent;

    return 0.0;

  }

}

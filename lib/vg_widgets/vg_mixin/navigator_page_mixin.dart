import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

import 'navigator_page_observer.dart';

mixin NavigatorPageMixin<T extends StatefulWidget> on BaseState<T>{

  NavigatorPageObserver _observer;

  ///添加页面切换等监听
  void addPageListener({VoidCallback onPush,VoidCallback onPop}){
    _observer = NavigatorPageObserver(context,onPop: onPop,onPush: onPush)..addObserver();
  }

  @override
  void dispose() {
    _observer?.removeObserver();
    super.dispose();
  }

}
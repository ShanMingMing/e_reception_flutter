import 'dart:ui';

import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_screen_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

class VgTopBarWidget extends StatelessWidget{

  final Color backgroundColor;
  final bool isShowBack; //是否显示返回图标
  final Color backImgColor; //返回图标颜色
  final bool isShowGrayLine; //是否显示灰线
  final Widget rightWidget; //右组件
  final String title; //标题字符串
  final Widget centerWidget; //中间组件
  final Color titleColor; //标题颜色
  final bool isFontWeight; //中间字粗细
  final double titleFontSize; //标题大小
  final double rightPadding; //右填充
  final VoidCallback navAfterFunciton; //退出前的操作(完成后退出)
  final VoidCallback navFunction; //点击返回键回调
  final bool isHideCenterWidget; //是否隐藏中间组件
  final bool isHideRightWidget; // 是否隐藏右组件
  final Widget rightSecondWidget; // 右侧第二个组件
  final bool isShowSecondRightWdiget; // 是否显示从右数、右侧第二个组件
  final Widget backRightWidget; // 左侧退出按钮右侧控件
  final bool isShowBackRightWdiget; // 是否显示左侧退出按钮第二个控件

  const VgTopBarWidget(
      {Key key,
        this.backgroundColor = Colors.transparent,
        this.isShowBack = true,
        this.rightWidget,
        this.title = "",
        this.centerWidget,
        this.titleColor = const Color(0xFFD0E0F7),
        this.isFontWeight = true,
        this.rightPadding = 15,
        this.navAfterFunciton,
        this.isHideCenterWidget = false,
        this.isHideRightWidget = false,
        this.isShowGrayLine = false,
        this.titleFontSize = 17,
        this.rightSecondWidget,
        this.isShowSecondRightWdiget = false,
        this.backRightWidget,
        this.isShowBackRightWdiget = false,
        this.backImgColor = const Color(0xFF5E687C),
        this.navFunction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      padding: EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)),
      child: Container(
        height: 44,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(),
            Positioned(
              left: 0,
              child: Offstage(
                offstage: !isShowBack,
                child: ClickAnimateWidget(
                  child: Container(
                    width: 40,
                    height: 44,
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.only(left: 15.6),
                    child: Image.asset(
                      "images/top_bar_back_ico.png",
                      width: 9,
                      color: backImgColor,
                    ),
                  ),
                  scale: 1.4,
                  onClick: () {
                    if (navFunction != null) {
                      navFunction();
                      return;
                    }
                    if (navAfterFunciton != null) {
                      navAfterFunciton();
                    }
                    // VgNavigatorUtils.pop(context);
                    FocusScope.of(context).requestFocus(FocusNode());
                    Navigator.of(context).maybePop();
                  },
                ),
              ),
            ),
            Positioned(
              left: 40,
              child: Offstage(
                offstage: !isShowBackRightWdiget,
                child: backRightWidget,
              ),
            ),
            AnimatedOpacity(
              duration: DEFAULT_ANIM_DURATION,
              opacity: isHideCenterWidget ? 0: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                child: centerWidget == null
                    ? Text(
                  title ?? "",
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: titleColor,
                      fontSize: titleFontSize,
                      fontWeight: isFontWeight
                          ? FontWeight.w600
                          : FontWeight.normal),
                )
                    : centerWidget,
              ),
            ),
            Positioned(
              right: 44,
              child: Offstage(
                offstage: !isShowSecondRightWdiget,
                child: rightSecondWidget,
              ),
            ),
            Positioned(
              right: 0,
              child: Offstage(
                  offstage: isHideRightWidget,
                  child: Padding(
                    padding: EdgeInsets.only(right: rightPadding),
                    child: rightWidget == null ? Container() : rightWidget,
                  )),
            ),
            Positioned(
              bottom: 0,
              child: Offstage(
                offstage: !isShowGrayLine,
                child: Container(
                  height: 0.5,
                  width: ScreenUtils.screenW(context),
                  color: Color(0xFFE5E5E5),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
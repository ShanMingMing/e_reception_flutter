import 'dart:io';

import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_constants_lib.dart';

///阿里云网络图质量类型枚举
enum ImageQualityType {
  ///原始
  original,

  ///高
  high,

  ///中上
  middleUp,

  ///中
  middle,

  ///中下
  middleDown,

  ///低
  low,

  ///最低
  lowest,

  ///ep使用
  ep_thump,
}

enum ImageErrorType{
  head,
}

enum ImagePlaceType{
  head,
}

extension ImageQualityTypeExtension on ImageQualityType {
  ///转成字符串
  String toStr() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case ImageQualityType.original:
        return ImageThumbConstant.ORIGINAL;
      case ImageQualityType.high:
        return ImageThumbConstant.HIGH;
      case ImageQualityType.middleUp:
        return ImageThumbConstant.MIDDLE_UP;
      case ImageQualityType.middle:
        return ImageThumbConstant.MIDDLE;
      case ImageQualityType.middleDown:
        return ImageThumbConstant.MIDDLE_DOWN;
      case ImageQualityType.low:
        return ImageThumbConstant.LOW;
      case ImageQualityType.lowest:
        return ImageThumbConstant.LOWEST;
      case ImageQualityType.ep_thump:
        return ImageThumbConstant.HIGH;
    }
    return null;
  }
}

/// 通用网络图片加载组件
///
/// (支持文件和网络图判断)
/// @author: zengxiangxi
/// @createTime: 1/5/21 10:28 AM
/// @specialDemand: extendedImage参数copy
class VgCacheNetWorkImage extends StatelessWidget {
  ///网络图路径
  final String url;

  ///阿里云图片质量类型（默认原图）
  final ImageQualityType imageQualityType;

  final Widget placeWidget;

  final Widget errorWidget;

  final Widget emptyWidget;

  final ImageErrorType defaultErrorType;

  final ImagePlaceType defaultPlaceType;

  ///如果非空，则要求图像具有此宽度。
  ///如果为null，则图像将选择最能保留其固有
  ///长宽比的大小。
  ///强烈建议同时指定[width]和[height]
  ///，或将小部件放置在设置严格的布局约束的上下文中，以便图像加载时不会更改大小。
  ///如果事先不知道确切的图像尺寸，请考虑使用[fit]调整图像的渲染以适合给定的宽度
  ///和高度。
  final double width;

  ///如果不为null，则要求图片具有此高度。
  ///如果为null，则图像将选择最能保留其固有///长宽比的大小。
  ///强烈建议同时指定[width]和[height]
  ///，或将小部件放置在设置严格的布局约束的上下文中，以便图像加载时不会更改大小。
  ///如果事先不知道确切的图像尺寸，请考虑使用[fit]调整图像的渲染以适合给定的宽度
  ///和高度。
  final double height;

  ///如果不为null，则使用[colorBlendMode]将此颜色与每个图像像素混合。
  final Color color;

  ///用于将[color]与该图像组合。
  ///默认值为[BlendMode.srcIn]。就混合模式而言，[color]是
  ///作为源，而此图像是目标。
  ///另请参见：
  /// * [BlendMode]，其中包括每种混合模式的效果说明。
  final BlendMode colorBlendMode;

  ///如何在布局期间分配图像到分配的空间。
  /// 默认值根据其他字段而有所不同。请参见
  /// [paintImage]上的讨论。
  final BoxFit fit;

  // ///如何绘制图像未覆盖的布局边界的任何部分。
  // final ImageRepeat repeat;
  //
  // ///如何绘制图像未覆盖的布局边界的任何部分。
  // final Alignment alignment;
  //
  // ///九片图像的中心切片。
  // ///中心切片内的图像区域将水平和垂直拉伸，以使图像适合其目标。
  // ///在中心切片上方和/或下方的图像区域仅水平拉伸，
  // ///在中心切片左侧和右侧的图像区域仅垂直拉伸。
  // final Rect centerSlice;
  //
  // ///当图像提供者更改时，是继续显示旧图像（true），还是不显示任何
  // ///（false）。
  // final bool gaplessPlayback;
  //
  // ///用于设置图像的[FilterQuality]。
  // ///使用[FilterQuality.low]质量设置通过双线性插值缩放图像，或使用
  // ///与最近邻相对应的[FilterQuality.none]缩 放图像。
  // final FilterQuality filterQuality;
  //
  // ///将背景[color]，[gradient]和[image]填充到和///中以填充为[boxShadow]的形状。
  // ///如果这是[BoxShape.circle]，则将忽略[borderRadius]。
  // ///无法插入[shape]；在具有不同[shape]的两个[BoxDecoration] s
  // ///之间进行动画会导致渲染不连续。
  // ///要在两个形状之间进行插值，请考虑使用[ShapeDecoration]和
  // ///不同的[ShapeBorder] s；特别是[CircleBorder]代替
  // /// [BoxShape.circle]和[RoundedRectangleBorder]代替
  // /// [BoxShape.rectangle]。
  // final BoxShape shape;
  //
  // ///自定义加载状态小部件（如果需要）
  // final LoadStateChanged loadStateChanged;
  //
  // ///在背景[color]，[gradient]或[image]上方绘制边框。
  // ///跟随[shape]和[borderRadius]。
  // ///使用[Border]对象描述不依赖于读取方向的边界。
  // ///使用[BoxBorder]对象描述边框，该边框应根据是从左到右还是从
  // ///到从右到左读取文本来翻转其
  // ///和右边缘。
  // final BoxBorder border;
  //
  // ///如果不为null，则此框的角将由此[BorderRadius]进行圆角处理。
  // ///仅适用于矩形框；如果[shape]不是
  // /// [BoxShape.rectangle]，则忽略。
  // final BorderRadius borderRadius;
  //
  // /// {@macro flutter.clipper.clipBehavior}
  // final Clip clipBehavior;
  //
  // ///是否处于加载状态或失败状态
  // ///默认值是否为假
  // ///但是网络映像是否为真
  // ///最好在映像较大时花点时间设置为真
  // final bool enableLoadState;
  //
  // ///您可以在绘制图像之前绘制任何内容。
  // ///用于[ExtendedRawImage]
  // ///和[ExtendedRenderImage]
  // final BeforePaintImage beforePaintImage;
  //
  // ///您可以在绘制图像后绘制任何内容。
  // ///用于[ExtendedRawImage]
  // ///和[ExtendedRenderImage]
  // final AfterPaintImage afterPaintImage;
  //
  // ///是否在PaintingBinding.instance.imageCache中缓存
  // final bool enableMemoryCache;
  //
  // ///当加载图像失败时，是否清除内存缓存
  // ///如果为true，则图像将在下一次重新加载。
  // final bool clearMemoryCacheIfFailed;
  //
  // ///重试次数(默认3次)
  // final int retries;
  //
  // ///开启缓存（默认开启）
  // final bool cache;
  //
  // ///缩放（默认1.0）
  // final double scale;
  //
  // final int cacheWidth;
  //
  // final int cacheHeight;
  //
  // ///当从树中永久删除图像时，是否清除内存缓存（默认false）
  // final bool clearMemoryCacheWhenDispose;

  VgCacheNetWorkImage(
    this.url, {
    Key key,
    this.imageQualityType,
    this.width,
    this.height,
    this.color,
    this.colorBlendMode,
    this.fit = BoxFit.cover,
    this.placeWidget,
    this.errorWidget,
    this.emptyWidget, this.defaultErrorType, this.defaultPlaceType,
  })  : assert(url != null, "VgCacheNetWorkImage图片路径不能为空"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toCheckNullWidget();
  }

  Widget _toCheckNullWidget() {
    if (url == null || url == "") {
      return (emptyWidget ?? errorWidget) ?? _toErrorWidget();
    }
    if (VgStringUtils.isNetUrl(url)) {
      return _toNetWidget();
    }
    return _toFileWidget();
  }

  Widget _toNetWidget() {
    return ExtendedImage.network(
      _getImageQualityStr(),
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            return placeWidget ?? _toPlaceHolderWidget();
            break;

          ///if you don't want override completed widget
          ///please return null or state.completedWidget
          //return null;
          //return state.completedWidget;
          case LoadState.completed:
            return state.completedWidget;
            break;
          case LoadState.failed:
            return errorWidget ?? _toPlaceHolderWidget();
            break;
        }
        return Container();
      },
    );
  }

  Widget _toFileWidget() {
    return ExtendedImage.file(
      File(url),
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      loadStateChanged: (ExtendedImageState state) {
        switch (state.extendedImageLoadState) {
          case LoadState.loading:
            return placeWidget ?? _toPlaceHolderWidget();
            break;

          ///if you don't want override completed widget
          ///please return null or state.completedWidget
          //return null;
          //return state.completedWidget;
          case LoadState.completed:
            return state.completedWidget;
            break;
          case LoadState.failed:
            return errorWidget ?? _toErrorWidget();
            break;
        }
        return Container();
      },
    );
  }

  ///拼串
  String _getImageQualityStr() {
    if (url == null || url == "") {
      return null;
    }
    if (imageQualityType == null) {
      return url;
    }

    return url + (imageQualityType.toStr() ?? "");
  }

  Widget _toPlaceHolderWidget() {
    if(defaultPlaceType !=null){
      switch(defaultPlaceType){
        case ImagePlaceType.head:
          return _toDefaultHeadWidget();
      }
    }

    return Container(
      color: Color(0xfff2f2f2),
    );
  }
  
  Widget _toErrorWidget(){
    if(defaultPlaceType !=null){
      switch(defaultPlaceType){
        case ImagePlaceType.head:
          return _toDefaultHeadWidget();
      }
    }
    return Container(
      color: Color(0xfff2f2f2),
    ); 
  }

  Widget _toDefaultHeadWidget(){
    return Container(
        width: width,
        child: Image.asset("images/default_head_ico.png",fit: BoxFit.cover,));
  }
}

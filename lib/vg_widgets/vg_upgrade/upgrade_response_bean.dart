/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"version":{"name":"EReception_Android","val":2,"disp":"app_Android","dwurl":"http://etpic.we17.com/E_Reception/E_Reception_android.apk","needupflg":"00","upcontent":"1.优化部分bug；\r\n2.新增编辑用户功能。","vername":"1.0.5"}}
/// extra : null

class UpgradeResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static UpgradeResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpgradeResponseBean upgradeResponseBeanBean = UpgradeResponseBean();
    upgradeResponseBeanBean.success = map['success'];
    upgradeResponseBeanBean.code = map['code'];
    upgradeResponseBeanBean.msg = map['msg'];
    upgradeResponseBeanBean.data = DataBean.fromMap(map['data']);
    upgradeResponseBeanBean.extra = map['extra'];
    return upgradeResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// version : {"name":"EReception_Android","val":2,"disp":"app_Android","dwurl":"http://etpic.we17.com/E_Reception/E_Reception_android.apk","needupflg":"00","upcontent":"1.优化部分bug；\r\n2.新增编辑用户功能。","vername":"1.0.5"}

class DataBean {
  VersionBean version;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.version = VersionBean.fromMap(map['version']);
    return dataBean;
  }

  Map toJson() => {
    "version": version,
  };
}

/// name : "EReception_Android"
/// val : 2
/// disp : "app_Android"
/// dwurl : "http://etpic.we17.com/E_Reception/E_Reception_android.apk"
/// needupflg : "00"
/// upcontent : "1.优化部分bug；\r\n2.新增编辑用户功能。"
/// vername : "1.0.5"

class VersionBean {
  String name;
  int val;
  String disp;
  String dwurl;
  //00 不强制 01强制
  String needupflg;
  String upcontent;
  String vername;

  static VersionBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    VersionBean versionBean = VersionBean();
    versionBean.name = map['name'];
    versionBean.val = map['val'];
    versionBean.disp = map['disp'];
    versionBean.dwurl = map['dwurl'];
    versionBean.needupflg = map['needupflg'];
    versionBean.upcontent = map['upcontent'];
    versionBean.vername = map['vername'];
    return versionBean;
  }

  Map toJson() => {
    "name": name,
    "val": val,
    "disp": disp,
    "dwurl": dwurl,
    "needupflg": needupflg,
    "upcontent": upcontent,
    "vername": vername,
  };

  //00 不强制 01强制
  bool isForce(){
    return needupflg == "01";
  }
}
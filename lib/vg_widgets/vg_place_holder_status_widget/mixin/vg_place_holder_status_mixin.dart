import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import '../vg_place_holder_status_widget.dart';

///状态加载组件混入类
///
/// ps: 数据加载完成需手动 parseData(dynamic data) 后调用 loading(false)表示加载完成
mixin VgPlaceHolderStatusMixin<D, T extends StatefulWidget>
    on BasePagerState<D, T> {
  ///默认加载状态
  bool _isLoadingStatus = true;
  bool _isEmptyStatus = false;
  bool _isErrorStatus = false;

  bool get isLoadingStatus => _isLoadingStatus;

  bool get isEmptyStatus => _isEmptyStatus;

  bool get isErrorStatus => _isErrorStatus;

  set isLoadingStatus(bool isLodingStatus) {
    _isLoadingStatus = isLodingStatus;
  }

  set isEmptyStatus(bool emptyStatus) {
    _isEmptyStatus = emptyStatus;
  }

  set isErrorStatus(bool errorStatus) {
    _isErrorStatus = errorStatus;
  }

  ValueChanged<bool> _emptyCustomSetting;

  ValueChanged<bool> _errorCustomSetting;

  Widget MixinPlaceHolderStatusWidget(
      {@required Widget child,
      Widget loadingWidget,
      Widget emptyWidget,
      Widget errorWidget,
      ValueChanged<bool> emptyCustomSetting,
      ValueChanged<bool> errorCustomSetting,
      VoidCallback emptyOnClick,
      VoidCallback errorOnClick,
      VoidCallback loadingOnClick}) {
    _emptyCustomSetting = emptyCustomSetting;
    _errorCustomSetting = errorCustomSetting;
    return VgPlaceHolderStatusWidget(
      loadingStatus: _isLoadingStatus,
      emptyStatus: _isEmptyStatus,
      errorStatus: _isErrorStatus,
      emptyOnClick: emptyOnClick,
      errorOnClick: errorOnClick,
      loadingOnClick: loadingOnClick,
      loadingCustomWidget: loadingWidget,
      emptyCustomWidget: emptyWidget,
      errorCustomWidget: errorWidget,
      child: child,
    );
  }

  @override
  void showErrorPage(bool show) {
    if (_errorCustomSetting != null) {
      _errorCustomSetting?.call(show);
      return;
    }
    setState(() {
      _isErrorStatus = show;
    });
  }

  @override
  void showEmptyPage(bool show) {
    if (_emptyCustomSetting != null) {
      _emptyCustomSetting?.call(show);
      return;
    }
    setState(() {
      _isEmptyStatus = show;
    });
  }

  @override
  void loading(bool show, {String msg}) {
    setState(() {
      _isLoadingStatus = show;
    });
    super.loading(show, msg: msg);
  }
}

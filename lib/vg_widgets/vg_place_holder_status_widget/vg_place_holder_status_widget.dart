import 'dart:async';
import 'dart:io';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_sms_code_widget/constant/common_sms_code_constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

enum PlaceHolderStatusType{
  loading,
  error,
  empty,
}

///状态加载页
class VgPlaceHolderStatusWidget extends StatefulWidget {
  final bool loadingStatus; //加载状态
  final bool errorStatus; //错误页状态
  final bool emptyStatus; //空白页状态

  final Widget loadingCustomWidget; //加载自定义组件（优先级最高）
  final Widget errorCustomWidget; //错误页自定义显示（优先级最高）
  final Widget emptyCustomWidget; //空白页自定义显示组件（优先级最高）

  final String errorDesc; //错误页底部描述
  final String emptyDesc; //空白页底部描述

  final TextStyle errorDescTextStyle; //错误页底部描述样式
  final TextStyle emptyDescTextStyle; //空白页底部描述样式

  final String errorImgAssetUrl; //错误页本地图片路径
  final String emptyImgAssetUrl; //空白页本地图片路径

  final Widget errorImgWidget; //错误页图片组件
  final Widget emptyImgWidget; //空白页图片组件

  final VoidCallback loadingOnClick; //加载点击
  final VoidCallback errorOnClick; //错误页点击回调
  final VoidCallback emptyOnClick; //空白页点击回调

  final double imgAndTextSpacing; // 图片和文本间距
  final Widget child; //主要显示组件

  final Color backgroundColor; //背景颜色默认透明

  final CustomCallback loadingCustomCallback;
  final CustomCallback emptyCustomCallback;
  final CustomCallback errorCustomCallback;

  const VgPlaceHolderStatusWidget(
      {Key key,
      this.loadingStatus = false,
      this.errorStatus = false,
      this.emptyStatus = false,
      this.loadingCustomWidget,
      this.errorCustomWidget,
      this.emptyCustomWidget,
      this.errorDesc,
      this.emptyDesc,
      this.errorDescTextStyle,
      this.emptyDescTextStyle,
      this.errorImgAssetUrl,
      this.emptyImgAssetUrl,
      this.errorImgWidget,
      this.emptyImgWidget,
      this.loadingOnClick,
      this.errorOnClick,
      this.emptyOnClick,
      this.imgAndTextSpacing = 24,
      this.backgroundColor,
      this.child,
      this.loadingCustomCallback,
      this.emptyCustomCallback,
      this.errorCustomCallback})
      : super(key: key);

  @override
  _VgPlaceHolderStatusWidgetState createState() => _VgPlaceHolderStatusWidgetState();
}

class _VgPlaceHolderStatusWidgetState extends State<VgPlaceHolderStatusWidget> {

  ///计时器
  Timer _timer;

  ///1秒
  Duration oneSecond = const Duration(milliseconds: 1000);

  ///计时数
  int _codeCountDown = CommonSmsCodeConstant.DEFAULT_TIMER_LOAD;

  @override
  void initState() {
    super.initState();
    if( widget.loadingCustomWidget == null){
      _startTimer();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        minHeight: 200
      ),
      child: PlaceHolderStatusWidget(
        loadingStatus: widget.loadingStatus,
        emptyStatus: widget.emptyStatus,
        errorStatus: widget.errorStatus,
        loadingCustomWidget: (_codeCountDown == 15 && widget.loadingCustomWidget==null) ?_defaultErrorCustomWidget() : _defaultLoadingCustomWidget(),
        errorCustomWidget: widget.errorCustomWidget ?? _defaultErrorCustomWidget(),
        emptyCustomWidget: widget.emptyCustomWidget ?? _defaultEmptyCustomWidget(),
        errorDesc: widget.errorDesc,
        emptyDesc: widget.emptyDesc,
        errorDescTextStyle: widget.errorDescTextStyle,
        emptyDescTextStyle: widget.emptyDescTextStyle,
        errorImgAssetUrl: widget.errorImgAssetUrl,
        emptyImgAssetUrl: widget.emptyImgAssetUrl,
        errorImgWidget: widget.errorImgWidget,
        emptyImgWidget: widget.emptyImgWidget,
        loadingOnClick: widget.loadingOnClick,
        errorOnClick: widget.errorOnClick,
        emptyOnClick: widget.emptyOnClick,
        imgAndTextSpacing: widget.imgAndTextSpacing,
        backgroundColor: widget.backgroundColor,
        loadingCustomCallback: widget.loadingCustomCallback,
        emptyCustomCallback: widget.emptyCustomCallback,
        errorCustomCallback: widget.errorCustomCallback,
        child: widget.child,
      ),
    );
  }

  Widget _defaultLoadingCustomWidget() {
    return widget.loadingCustomWidget ??
        Container(
          width: 130,
          height: 130,
          child: FlareActor("assets/loading.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              animation: "loading"),
        );
  }

  Widget _defaultEmptyCustomWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset("images/empty_icon.png",width: 120,gaplessPlayback: true,),
         Text(
                   "暂无内容",
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                     color: VgColors.INPUT_BG_COLOR,
                       fontSize: 13,
                       ),
                 )
      ],
    );
  }

  Widget _defaultErrorCustomWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset("images/empty_icon.png",width: 120,gaplessPlayback: true,),
        Text(
          "网络断线 重新获取",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 13,
          ),
        )
      ],
    );
  }

  ///开启计时器
  void _startTimer(){
    if(_timer?.isActive ?? false){
      return;
    }
    _proccessSetState();
    _timer = Timer.periodic(oneSecond, (timer) {
      _proccessSetState();
    });
  }

  void _proccessSetState(){
    _codeCountDown = _codeCountDown - 1;
    if(_codeCountDown <= 0){
      _codeCountDown = CommonSmsCodeConstant.DEFAULT_TIMER_LOAD;
      if(_timer != null){
        _timer?.cancel();
        _timer = null;
        setState(() {

        });
      }
    }
  }
}

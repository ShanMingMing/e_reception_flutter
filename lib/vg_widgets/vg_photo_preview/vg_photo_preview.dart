
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/delegate/watermark_text_image_delegate.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/delegate/watermark_text_video_delegate.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_log_lib.dart';
import 'choose_image_and_video_list_item_vo.dart';
import 'delegate/custom_fast_to_close_slide_delegate.dart';
import 'delegate/custom_video_cache_manager.dart';
import 'more_btn_slide_delegate.dart';

///图片视频预览
class VgPhotoPreview {
  /// 跳转到图片/视频预览页面
  ///
  /// 详情参考 We17/photo_preview_flutter 库
  ///
  /// @required context：上下文
  /// @required dataSource：数据源（多种构造方法）
  /// callback: Route回调
  /// extendedSlideDelegate: 滑动配置类（自定义需继承 DefaultExtendedSlideDelegate 重写方法）
  /// imageDelegate：图片配置（自定义需继承 DefaultPhotoPreviewImageDelegate 重写方法）
  /// videoDelegate：视频配置（自定义需继承 DefaultPhotoPreviewVideoDelegate 重写方法）
  /// customErrorWidget：自定义错误组件
  /// customClass: 自定义业务逻辑类（类似单例类，可在预览页面任何位置直接引用）
  ///              继承 PhotoPreviewCommonClass
  ///              引用方法 clazz = PhotoPreviewCommonClass.of(context);
  /// transitionDuration：Route加载时间
  /// customPageRoute：自定义转场

  static void single(BuildContext context, String url,
      {ImageQualityType loadingImageQualityType, String loadingCoverUrl, PhotoPreviewType photoPreviewType}) async{
    if (PhotoPreviewToolUtils.isNetUrl(url) && (PhotoPreviewType.video == PhotoPreviewToolUtils.getType(url))) {
      //判断是否已缓存好
      //获取存储路径
      FileInfo cacheFile =
          await CustomVideoCacheManager().getFileFromCache(url);
      if (cacheFile != null && cacheFile.file != null) {
        url = cacheFile.file.path;
      }
    }
    PhotoPreviewPage.navigatorPush(
        context,
        PhotoPreviewDataSource.single(url,
            type:photoPreviewType,
            loadingCoverUrl:
            getImageQualityStr(loadingCoverUrl, loadingImageQualityType) ??
                getImageQualityStr(url, loadingImageQualityType)),
        extendedSlideDelegate: CustomFastToCloseSlideDelegate(),
        videoDelegate: WatermarkTextVideoDelegate()
    );
  }

  static void singleWithWatermarkText(BuildContext context, String url,
      {ImageQualityType loadingImageQualityType, String loadingCoverUrl, dynamic extra,
        VoidCallback onTextTap, PhotoPreviewType type}) async{
    if (PhotoPreviewToolUtils.isNetUrl(url) && PhotoPreviewType.video == type) {
      //判断是否已缓存好
      //获取存储路径
      FileInfo cacheFile =
          await CustomVideoCacheManager().getFileFromCache(url);
      if (cacheFile != null && cacheFile.file != null) {
        url = cacheFile.file.path;
      }
    }

    PhotoPreviewPage.navigatorPush(
        context,
        PhotoPreviewDataSource.single(url,
            loadingCoverUrl:
            getImageQualityStr(loadingCoverUrl, loadingImageQualityType) ??
                getImageQualityStr(url, loadingImageQualityType),
            extra:extra,
            type: type
        ),
        imageDelegate: WatermarkTextImageDelegate(),
        videoDelegate: WatermarkTextVideoDelegate()
    );
  }

  ///拼串
  static String getImageQualityStr(
      String url, ImageQualityType loadingImageQualityType) {
    if (url == null || url == "") {
      return null;
    }
    if (loadingImageQualityType == null) {
      return url;
    }

    return url + (loadingImageQualityType.toStr() ?? "");
  }

  static void listForPage(BuildContext context, List<dynamic> list,
      {String initUrl,
        ValueTransformFunc<String> urlTrasformFunc,
        ValueTransformFunc<dynamic> extraTransformFunc,
        ValueTransformFunc<PhotoPreviewType> typeTransformFunc,
        ValueTransformFunc<String> loadingTransformFunc,
        ValueTransformFunc<dynamic> heroTransformFunc}) {
    if (list == null || list.isEmpty) {
      return;
    }
    PhotoPreviewPage.navigatorPush(
        context,
        PhotoPreviewDataSource.customMap(
          list,
          initialUrl: initUrl,
          extraTransformFunc: extraTransformFunc,
          urlTrasformFunc: urlTrasformFunc,
          typeTransformFunc: typeTransformFunc,
          loadingTransformFunc: loadingTransformFunc,
          heroTransformFunc: heroTransformFunc,
        ),
        imageDelegate: WatermarkTextImageDelegate(),
        videoDelegate: WatermarkTextVideoDelegate()
    );
  }

  static navigatorPushWithMoreBtn(BuildContext context,
      List<ChooseImageAndVideoListItemVo> list, String initialUrl,
      VoidCallback moreBtnCallback,
      {PhotoPreviewCallback callback,
        ExtendedSlideDelegate extendedSlideDelegate,
        DefaultPhotoPreviewImageDelegate imageDelegate,
        DefaultPhotoPreviewVideoDelegate videoDelegate,
        PhotoPreviewCommonClass customClass,
        Widget customErrorWidget,
        Duration transitionDuration,
        CustomPageRoute customPageRoute}) {
    List<PhotoPreviewInfoVo> resultList = _transToPhotoInfoData(list);
    if (resultList == null || resultList.isEmpty) {
      return;
    }
    LogUtils.d("图片/视频预览：\n"+list.toString());
    PhotoPreviewPage.navigatorPush(
        context,
        PhotoPreviewDataSource(
            imgVideoFullList: resultList, initialUrl: initialUrl),
        extendedSlideDelegate: MoreBtnSlideDelegate(moreBtnCallback),
        imageDelegate: imageDelegate,
        videoDelegate: videoDelegate,
        customClass: customClass,
        customErrorWidget: customErrorWidget,
        transitionDuration: transitionDuration,
        customPageRoute: customPageRoute);
  }

  static List<PhotoPreviewInfoVo> _transToPhotoInfoData(
      List<ChooseImageAndVideoListItemVo> list) {
    if (list == null || list.isEmpty) {
      return null;
    }
    List<PhotoPreviewInfoVo> infoList = [];
    list.forEach((element) {
      if (element != null) {
        infoList.add(PhotoPreviewInfoVo(
            url: element.getUrl(),
            loadingCoverUrl: element.getPreviewUrl(),
            type: _getPreviewType(element.type)));
      }
    });
    return infoList;
  }

  static PhotoPreviewType _getPreviewType(MatisseType type) {
    if (type == null) {
      return PhotoPreviewType.unknow;
    }
    if (type == MatisseType.image) {
      return PhotoPreviewType.image;
    }
    if (type == MatisseType.video) {
      return PhotoPreviewType.video;
    }
    return PhotoPreviewType.unknow;
  }
}
import 'dart:convert';
import 'dart:core';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'choose_image_and_video_constant.dart';

///存取图片视频描述类型项
class ChooseImageAndVideoListItemVo {
  String imgUrl; //图片路径
  String imgCompressUrl; //图片压缩路径
  String videoUrl; //视频路径
  String videoCoverUrl; //视频封面路径
  String videoCoverCompressUrl; //视频封面压缩路径
  String linkUrl; //链接路径
  int width; //图片宽度
  int height; //图片高度
  String desc; //描述
  MatisseType type; //类型


  /** 暂存图片和视频的本地路径 */
  String localImagePath;
  String localVideoPath;

  //唯一Id
  String _uniqueId = DateTime.now().millisecondsSinceEpoch.toString();

  ///快速构建图片视频类
  factory ChooseImageAndVideoListItemVo.create(MatisseType type,String url,{String previewUrl,String desc}){
    assert(type != null && type != MatisseType.all);
    ChooseImageAndVideoListItemVo vo = ChooseImageAndVideoListItemVo(type: type,desc: desc);

    if(vo.isImageType()){
      vo.imgUrl = url;
      vo.imgCompressUrl = previewUrl ?? url;
    }else if(vo.isVideoType()){
      vo.videoUrl = url;
      vo.videoCoverUrl = previewUrl;
      vo.videoCoverCompressUrl = previewUrl;
    }

    return vo;
  }

  ChooseImageAndVideoListItemVo(
      {this.imgUrl,
      this.imgCompressUrl,
      this.videoUrl,
      this.linkUrl,
      this.width,
      this.height,
      this.desc,
      this.type,
      this.localImagePath,
      this.localVideoPath,
      this.videoCoverUrl,
      this.videoCoverCompressUrl}) {
    if (type == null) {
      type = MatisseType.image;
    }
  }

  static ChooseImageAndVideoListItemVo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ChooseImageAndVideoListItemVo bean = ChooseImageAndVideoListItemVo();
    bean._uniqueId = map['_uniqueId'];
    bean.imgUrl = map['imgUrl'];
    bean.imgCompressUrl = map['imgCompressUrl'];
    bean.videoUrl = map['videoUrl'];
    bean.videoCoverUrl = map['videoCoverUrl'];
    bean.videoCoverCompressUrl = map['videoCoverCompressUrl'];
    bean.linkUrl = map['linkUrl'];
    bean.width = map['width'];
    bean.height = map['height'];
    bean.desc = map['desc'];
    bean.type = _getMatisseType(map['type']);
    return bean;
  }

  Map toJson() {
    Map map = new Map();
    map['_uniqueId'] = _uniqueId ?? "";
    map["imgUrl"] = imgUrl ?? "";
    map["imgCompressUrl"] = imgCompressUrl ?? "";
    map["videoUrl"] = videoUrl ?? "";
    map["videoCoverUrl"] = videoCoverUrl ?? "";
    map["videoCoverCompressUrl"] = videoCoverCompressUrl ?? "";
    map["linkUrl"] = linkUrl ?? "";
    map["width"] = width ?? 0;
    map["height"] = height ?? 0;
    map["desc"] = desc ?? "";
    map["type"] = _getType();
    return map;
  }


  @override
  String toString() {
    return json.encode(this).toString();
  }


  @override
  int get hashCode => super.hashCode;

  @override
  bool operator ==(other) {
   if(other == null || other is! ChooseImageAndVideoListItemVo){
     return false;
   }
   if(identical(this, other)){
     return true;
   }
   if(StringUtils.isEmpty(this._uniqueId) || StringUtils.isEmpty(other._uniqueId)){
      return false;
    }
   return this._uniqueId == other._uniqueId;
  }

  ///根据type返回视频/图片的路径
  String getUrl(){
    if(isImageType()){
      return imgUrl;
    }
    if(isVideoType()){
      return videoUrl;
    }
    return null;
  }

  ///根据type设置网络路径
  void setUrl(String netUrl){
    if(isImageType()){
      imgUrl = netUrl;
      imgCompressUrl = netUrl + DEFAULT_IMAGE_QUALITY;
      return ;
    }
    if(isVideoType()){
      videoUrl = netUrl;
      return ;
    }
  }
  
  ///根据type设置封面（自动设置缩略图）
  void setVideoCoverUrl(String netUrl){
    videoCoverUrl = netUrl;
    videoCoverCompressUrl = netUrl + DEFAULT_IMAGE_QUALITY;
  }

  ///根据type返回视频/图片的预览封面
  String getPreviewUrl(){
    if(isImageType()){
      return imgCompressUrl;
    }
    if(isVideoType()){
      return videoCoverCompressUrl;
    }
    return null;
  }

  ///是否是图片类型
  bool isImageType() {
    return type == MatisseType.image;
  }

  ///是否是视频类型
  bool isVideoType() {
    return type == MatisseType.video;
  }

  ///判断是否是网络链接
  bool isNetworkUrl({String url, MatisseType type}) {
    String tmpUrl;
    if (!StringUtils.isEmpty(url)) {
      tmpUrl = url;
    } else if (type == MatisseType.image) {
      tmpUrl = imgUrl;
    } else if (type == MatisseType.video) {
      tmpUrl = videoUrl;
    }
    if (StringUtils.isEmpty(tmpUrl)) {
      return false;
    }
    if (tmpUrl.startsWith("http://") || tmpUrl.startsWith("https://")) {
      return true;
    }
    return false;
  }

  ///可能不同发布器不同类型（待处理）
  String _getType() {
    if (isImageType()) {
      return ChooseImageAndVideoConstant.IMAGE_DYNAMIC_TYPE;
    }
    if (isVideoType()) {
      return ChooseImageAndVideoConstant.VIDEO_DYNAMIC_TYPE;
    }
    return ChooseImageAndVideoConstant.TEXT_DYNAMIC_TYPE;
  }

  static MatisseType _getMatisseType(String type) {
    if (StringUtils.isEmpty(type)) {
      return MatisseType.image;
    }
    if (type == ChooseImageAndVideoConstant.VIDEO_DYNAMIC_TYPE) {
      return MatisseType.video;
    }
    return MatisseType.image;
  }


}

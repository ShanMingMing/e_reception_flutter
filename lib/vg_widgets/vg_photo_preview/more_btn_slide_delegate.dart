import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// @author: pengboboer
/// @createDate: 7/21/21
///自定义快速关闭滑动配置
class MoreBtnSlideDelegate extends DefaultExtendedSlideDelegate{

  VoidCallback onClickMoreBtnCallback;


  MoreBtnSlideDelegate(this.onClickMoreBtnCallback);

  @override
  Widget topWidget(bool isSlideStatus) {
    return TopBarWidget(
      isShowGrayLine: false,
      isShowBack: false,
      backgroundColor: Colors.transparent,
      rightPadding: 12,
      rightWidget: ClickAnimateWidget(
        scale: 1.2,
        onClick: onClickMoreBtnCallback ?? (){},
        child: Container(
          width: 40,
          height: 40,
          alignment: Alignment.center,
          child: Image.asset("images/more_white.png", width: 30, height: 30),
        ),
      )
    );
  }

  Widget _build() {
    return Container(
      height: 44,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Positioned(
              right: 0,
              child: ClickAnimateWidget(
                scale: 1.2,
                onClick: onClickMoreBtnCallback ?? (){},
                child: Container(
                  padding: EdgeInsets.only(top: 0, right: 8),
                  child: Image.asset("images/more_white.png", width: 39, height: 39),
                ),
              )
          )],
      ),
    );
  }
}
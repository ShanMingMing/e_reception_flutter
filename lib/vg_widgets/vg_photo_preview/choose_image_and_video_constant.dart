import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChooseImageAndVideoConstant {
  ///默认最大图片数
  static const int DEFAULT_MAX_IMAGE_SIZE = 9;

  ///默认最大视频数
  static const int DEFAULT_MAX_VIDEO_SIZE = 1;

  ///默认交叉轴最多容纳总数
  static const int DEFAULT_CROSS_AXIS_COUNT = 3;

  ///默认单项宽高比
  static const double DEFAULT_CHILD_ASPECT_RATIO = 1.0;

  ///默认交叉轴间距
  static const double DEFAULT_CROSS_AXIS_SPACING = 4;

  ///默认主轴间距
  static const double DEFAULT_MAIN_AXIS_SPACING = 13;

  ///默认是否能混选
  static const bool DEFAULT_IS_CAN_MIXED_SELECTION = false;

  ///默认外容器背景颜色
  static const Color DEFAULT_CONTAINER_COLOR = Colors.white;

  ///默认外容器填充
  static const EdgeInsetsGeometry DEFAULT_CONTAINER_PADDING =
      const EdgeInsets.symmetric(horizontal: 12, vertical: 12);

  ///默认外容器边距
  static const EdgeInsetsGeometry DEFAULT_CONTAINER_MARGIN =
      const EdgeInsets.all(0);

  ///默认内部Grid填充
  static const EdgeInsetsGeometry DEFAULT_GRID_PADDING =
      const EdgeInsets.all(0);

  ///默认描述背景颜色
  static const Color DEFAULT_ITEM_DESC_BG_COLOR = Color(0x80000000);

  ///默认描述背景字体
  static const Color DEFAULT_ITEM_DESC_TEXT_COLOR = Colors.white;

  ///视频显示描述
  static const bool DEFAULT_SHOW_VIDEO_DESC = false;

  ///图片显示描述
  static const bool DEFAULT_SHOW_IMAGE_DESC = true;

  ///默认不支持拖动交换
  static const bool DEFAULT_DRAGGABLE_CHANGE = false;

  ///默认图片裁剪
  static const bool DEFAULT_IMAGE_CLIP = false;

  ///文本-动态类型
  static const String TEXT_DYNAMIC_TYPE = "01";
  ///图片-动态类型
  static const String IMAGE_DYNAMIC_TYPE = "02";
  ///视频-动态类型
  static const String VIDEO_DYNAMIC_TYPE = "03";
}

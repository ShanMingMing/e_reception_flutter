import 'dart:async';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

class CustomVideoCacheManager extends BaseCacheManager {
  static const key = "customVideoCache";

  static CustomVideoCacheManager _instance;

  factory CustomVideoCacheManager() {
    if (_instance == null) {
      _instance = new CustomVideoCacheManager._();
    }
    return _instance;
  }

  CustomVideoCacheManager._()
      : super(
          key,
          maxAgeCacheObject: Duration(days: 365),
          maxNrOfCacheObjects: 1024,
        );

  Future<String> getFilePath() async {
    var directory = await getTemporaryDirectory();
    return p.join(directory.path, key);
  }
}

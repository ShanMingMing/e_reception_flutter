import 'dart:async';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/logo_widget.dart';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/watermark_text_widget.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class WatermarkTextImageDelegate extends DefaultPhotoPreviewImageDelegate{

  StreamController<bool> _isSlideStream = StreamController.broadcast();

  StreamController<double> _picHeightStream;

  @override
  ValueChanged<bool> get isSlidingStatus {
    return (status){
      _isSlideStream?.add(status);
    };
  }

  @override
  SlideEndHandler get slideEndHandler =>  (
      Offset offset, {
        ExtendedImageSlidePageState state,
        ScaleEndDetails details,
      }) {
    //如果放大
    if ((state?.imageGestureState?.gestureDetails?.totalScale ??
        PhotoPreviewConstant.DEFAULT_TOTAL_SCALE) >
        (state?.imageGestureState?.imageGestureConfig?.initialScale ??
            PhotoPreviewConstant.DEFAULT_INIT_SCALE)) {
      return false;
    }
    //向上滑回弹
    if (offset.dy <= 0) {
      return false;
    }
    return offset.dy > 20;
  };


  @override
  Widget imageWidget(PhotoPreviewInfoVo imageInfo, {Widget result}) {
    _picHeightStream = StreamController.broadcast();
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        result,
        StreamBuilder<double>(
          stream: _picHeightStream?.stream,
          builder: (context, snapshot) {
            if(snapshot?.data == null){
              return Container();
            }
            return Container(
              margin: (snapshot.data <=0)?null:EdgeInsets.only(bottom: snapshot?.data??1),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: StreamBuilder<bool>(
                  initialData: false,
                  stream: _isSlideStream?.stream,
                  builder: (context, snapshot) {
                    return AnimatedOpacity(
                      opacity: snapshot?.data == true ? 0: 1,
                      duration: Duration(milliseconds: 50),
                      child: (imageInfo?.extra != null)? WatermarkTextWidget(
                          id: imageInfo?.extra['id'] ?? "",
                          watermark: imageInfo?.extra['watermark'] ?? "",
                          waterflg: imageInfo?.extra['waterflg'] ?? "",
                          title: imageInfo?.extra['title'] ?? "",
                          content: imageInfo?.extra['content'] ?? ""
                      ):SizedBox(),
                    );
                  }
                ),
              ),
            );
          }
        ),
        (imageInfo?.extra != null && ("00" == (imageInfo?.extra['logoflg']??"")) && StringUtils.isNotEmpty(imageInfo?.extra['logo']??""))?
        StreamBuilder<double>(
            stream: _picHeightStream?.stream,
            builder: (context, snapshot) {
              if(snapshot?.data == null){
                return Container();
              }
              print("snapshot.data:" + snapshot.data.toString());
              return Container(
                margin: (snapshot.data <=0)?EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)):EdgeInsets.only(top: snapshot?.data??1),
                child: Align(
                  alignment: Alignment.topRight,
                  child: StreamBuilder<bool>(
                      initialData: false,
                      stream: _isSlideStream?.stream,
                      builder: (context, snapshot) {
                        return AnimatedOpacity(
                          opacity: snapshot?.data == true ? 0: 1,
                          duration: Duration(milliseconds: 50),
                          child: (imageInfo?.extra != null)? LogoWidget(
                              logo: imageInfo?.extra['logo'] ?? "",
                          ):SizedBox(),
                        );
                      }
                  ),
                ),
              );
            }
        )
            :SizedBox(),
      ],
    );
  }

  @override
  void dispose() {
    _isSlideStream?.close();
    super.dispose();
  }
  
  @override
  Function(double width, double height) get afterPaintImage {
    return (width, height){
      print("111width:" + width.toString());
      print("1111height:" + height.toString());
      if(width >= height){
        _picHeightStream?.add(0);
      }else{
        _picHeightStream?.add(((ScreenUtils.screenH(context) - height)/2 - 2));
      }
    };
  }

}

class PicStream {
  final double height;
  final String url;


  PicStream({this.height, this.url});
}
// import 'dart:async';
// import 'dart:io';
//
// import 'package:flutter/cupertino.dart';
// import 'package:open_file/open_file.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:upgrade_flutter/src/bean/apk_config_bean.dart';
// import 'package:upgrade_flutter/src/error/upgrade_error.dart';
// import 'package:upgrade_flutter/src/error/upgrade_error_mixin.dart';
// import 'package:upgrade_flutter/src/http/dio_singleton.dart';
// import 'package:upgrade_flutter/src/upgrade/upgrade_repository.dart';
// import 'package:upgrade_flutter/src/utils/file_util.dart';
// import 'package:upgrade_flutter/upgrade_flutter.dart';
//
// ///下载控制
// class VideoDownloadRepository {
//   static VideoDownloadRepository _instance;
//
//   VideoDownloadRepository._();
//
//   static VideoDownloadRepository getInstance() {
//     if (_instance == null) {
//       _instance = new VideoDownloadRepository._();
//     }
//     return _instance;
//   }
//
//   ///下载状态
//   ValueNotifier<double> _processValueNotifier = ValueNotifier(null);
//
//   ValueNotifier<double> get processValueNotifier => _processValueNotifier;
//
//   ///进度条读满
//   void setProcessComplete() {
//     _processValueNotifier.value = 1.0;
//   }
//
//   ///正在下载
//   bool _mIsDownloading = false;
//
//   CancelToken _mCancelToken;
//
//   ///是否正在下载或完成
//   Future<bool> isDownloadingOrComplete(
//       String apkFolderName, int netAndroidVersionCode) async {
//     bool isDownloadComplete =
//         await isCompleteDownload(apkFolderName, netAndroidVersionCode);
//     return _mIsDownloading || isDownloadComplete;
//   }
//
//   ///是否完成下载
//   Future<bool> isCompleteDownload(
//       String apkFolderName, int netAndroidVersionCode) async {
//     String apkPath = await _getApkPathByApkName(apkFolderName);
//     bool isDownloadComplete = await ApkConfigRepository.getInstance()
//         .isCompleteDownload(apkPath, netAndroidVersionCode);
//     return isDownloadComplete;
//   }
//
//   ///销毁
//   void dispose() {
//     _processValueNotifier?.dispose();
//   }
//
//   ///下载apk
//   void downloadApk(UpgradeInfoBean upgradeInfoBean, String apkFolderName,
//       VoidCallback onInstallApk) async {
//     if (!Platform.isAndroid) {
//       throwUpgradeError(UpgradeErrorType.PlatformError, "平台不支持下载安装包");
//       return;
//     }
//     String apkUpgradeUrl = upgradeInfoBean?.androidApkUrl;
//     if (apkFolderName == null || apkFolderName.isEmpty) {
//       throwUpgradeError(UpgradeErrorType.UnImplementError, "安装包文件名不能为空");
//       return;
//     }
//     if (apkUpgradeUrl == null || apkUpgradeUrl.isEmpty) {
//       throwUpgradeError(UpgradeErrorType.ApkUpgradeUrlError, "安装包网络网络下载错误");
//       return;
//     }
//     if (_mIsDownloading) {
//       print("正在下载");
//       throwUpgradeError(UpgradeErrorType.ApkDownloadingError, "正在下载中");
//       return;
//     }
//     String apkPath = await _getApkPathByApkName(apkFolderName);
//     print("安装包路径： $apkPath");
//     _httpApkDownload(upgradeInfoBean, apkPath, onInstallApk);
//   }
//
//   void _httpApkDownload(UpgradeInfoBean upgradeInfoBean, String savePath,
//       VoidCallback onInstallApk) async {
//     if (savePath == null || savePath.isEmpty) {
//       throwUpgradeError(UpgradeErrorType.ApkSavePathError, "安装包保存路径获取失败");
//       return;
//     }
//     String apkUpgradeUrl = upgradeInfoBean?.androidApkUrl;
//     _processValueNotifier.value = null;
//     _mIsDownloading = true;
//     _mCancelToken = CancelToken();
//     Response response;
//
//     int cacheProgress;
//     try {
//       response = await DioSingleton.of()
//           .download(apkUpgradeUrl, savePath, cancelToken: _mCancelToken,
//               onReceiveProgress: (int count, int total) {
//         if (total == null || total <= 0) {
//           _mIsDownloading = false;
//           throwUpgradeError(UpgradeErrorType.ApkDownloadValuesError, "下载安装包失败");
//           return;
//         }
//         _processValueNotifier.value = (count ?? 0) / total;
//         //避免过频更新导致主线程占用资源限制
//         int progressInt = (_processValueNotifier.value * 100).floor() ?? 0;
//         if (progressInt != cacheProgress && progressInt % 3 == 0) {
//           //同步通知进度
//           cacheProgress = progressInt;
//           NotificationRepository.getInstance()
//               .showProgressNotification(count, total);
//         }
//         //避免计算误差 下载完更新通知进度
//         if (_processValueNotifier.value >= 1) {
//           NotificationRepository.getInstance()
//               .showProgressNotification(count, total);
//           cacheProgress = null;
//         }
//       });
//     } catch (e) {
//       _mIsDownloading = false;
//       //因取消取消通知
//       NotificationRepository.getInstance().cancelAllNotifications();
//       throwUpgradeError(UpgradeErrorType.ApkDownloadCancelOrStopError, "取消下载");
//       return;
//     }
//     _mIsDownloading = false;
//     if (response == null) {
//       throwUpgradeError(UpgradeErrorType.ApkDownloadedEmptyError, "下载安装包失败");
//       return;
//     }
//     if (onInstallApk == null) {
//       throwUpgradeError(UpgradeErrorType.ApkInstallError, "下载完成，安装提示失败");
//       return;
//     }
//     try {
//       if (File(savePath).existsSync()) {
//         //下载完 添加安装包配置文件
//         ApkConfigRepository.getInstance().saveApkConfig(upgradeInfoBean);
//         onInstallApk?.call();
//       }
//     } catch (e) {
//       throwUpgradeError(UpgradeErrorType.ApkDownloadedEmptyError, "安装包下载错误");
//       return;
//     }
//   }
//
//   ///通过Apk名称得到具体路径
//   Future<String> _getApkPathByApkName(String apkFolderName) async {
//     if (apkFolderName == null || apkFolderName.isEmpty) {
//       throwUpgradeError(UpgradeErrorType.UnImplementError, "安装包文件名不能为空");
//       return null;
//     }
//
//     ///手机储存目录
//     String savePath = await FileUtil.getPhoneLocalPath();
//     if (savePath == null || savePath.isEmpty) {
//       throwUpgradeError(
//           UpgradeErrorType.AppStorageDirectoryError, "储存安装包路径获取失败");
//       return null;
//     }
//     return "$savePath/$apkFolderName";
//   }
//
//
//   ///取消下载
//   void cancelDownload() {
//     if (_mCancelToken == null) {
//       throwUpgradeError(
//           UpgradeErrorType.ApkDownloadCancelOrStopError, "取消下载失败");
//       return;
//     }
//     if (_mCancelToken.isCancelled) {
//       return;
//     }
//     _mCancelToken.cancel();
//   }
// }

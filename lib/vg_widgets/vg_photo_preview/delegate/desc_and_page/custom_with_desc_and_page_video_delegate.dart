import 'package:photo_preview/photo_preview_export.dart';

class CustomWithDescAndPageVideoDelegate
    extends DefaultPhotoPreviewVideoDelegate {
  @override
  double get controllerBottomDistance => 30 ;
}

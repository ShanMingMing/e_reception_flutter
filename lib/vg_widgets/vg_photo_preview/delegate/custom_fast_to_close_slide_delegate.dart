import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';

///自定义快速关闭滑动配置
class CustomFastToCloseSlideDelegate extends DefaultExtendedSlideDelegate{


  @override
  SlideEndHandler get slideEndHandler =>  (
      Offset offset, {
        ExtendedImageSlidePageState state,
        ScaleEndDetails details,
      }) {
    //如果放大
    if ((state?.imageGestureState?.gestureDetails?.totalScale ??
        PhotoPreviewConstant.DEFAULT_TOTAL_SCALE) >
        (state?.imageGestureState?.imageGestureConfig?.initialScale ??
            PhotoPreviewConstant.DEFAULT_INIT_SCALE)) {
      return false;
    }
    //向上滑回弹
    if (offset.dy <= 0) {
      return false;
    }
    return offset.dy > 20;
  };
}
import 'package:photo_preview/photo_preview_export.dart';

class CustomWithPageStatusVideoDelegate extends DefaultPhotoPreviewVideoDelegate{
  @override
  double get controllerBottomDistance => 50.0;
}
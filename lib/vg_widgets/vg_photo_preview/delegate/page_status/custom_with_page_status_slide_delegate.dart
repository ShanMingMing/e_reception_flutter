import 'dart:async';

import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';

///带指示器的滑动配置
class CustomWithPageStatusSlideDelegate extends DefaultExtendedSlideDelegate {
  final int imgVideoCount;

  StreamController<int> pageChangeStatusStream = StreamController.broadcast();

  CustomWithPageStatusSlideDelegate(this.imgVideoCount);


  @override
  initState() {}

  @override
  ValueChanged<int> get pageChangeStatus {
    return (int page) {
      if (page < 0 || page >= (imgVideoCount ?? 0)) {
        return;
      }
      pageChangeStatusStream?.add(page);
    };
  }

  @override
  void dispose() {
    pageChangeStatusStream?.close();
    super.dispose();
  }

  @override
  SlideEndHandler get slideEndHandler => (
      Offset offset, {
        ExtendedImageSlidePageState state,
        ScaleEndDetails details,
      }) {
    //如果放大
    if ((state?.imageGestureState?.gestureDetails?.totalScale ??
        PhotoPreviewConstant.DEFAULT_TOTAL_SCALE) >
        (state?.imageGestureState?.imageGestureConfig?.initialScale ??
            PhotoPreviewConstant.DEFAULT_INIT_SCALE)) {
      return false;
    }
    //向上滑回弹
    if (offset.dy <= 0) {
      return false;
    }
    return offset.dy > 20;
  };

  @override
  Widget bottomWidget(bool isSlideStatus) {
    if ((imgVideoCount ?? 0) <= 1) {
      return Container();
    }
    return AnimatedOpacity(
      opacity: isSlideStatus ? 0 : 1,
      duration: Duration(milliseconds: 80),
      child: _toBottomDescAndPageStatusWidget(),
    );
  }

  Widget _toBottomDescAndPageStatusWidget() {
    return Container(
      color: Colors.black.withOpacity(0.4),
      padding: EdgeInsets.only(bottom: ScreenUtils.getBottomBarH(context)),
      child: StreamBuilder<Object>(
          initialData: 0,
          stream: pageChangeStatusStream?.stream,
          builder: (context, snapshot) {
            final currentPage = snapshot?.data ?? 0;
            return Container(
              height: 30,
              alignment: Alignment.center,
              child: _toPageStatusWidget(currentPage),
            );
          }),
    );
  }

  Widget _toPageStatusWidget(int currentPage) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        height: 7,
        child: ListView.separated(
            itemCount: imgVideoCount ?? 0,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return AnimatedContainer(
                width: (currentPage ?? 0) == index ? 15 : 7,
                duration: Duration(milliseconds: 200),
                decoration: ShapeDecoration(
                    shape: StadiumBorder(),
                    color: (currentPage ?? 0) == index
                        ? Colors.white
                        : Colors.white.withOpacity(0.5)),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                width: 9,
              );
            }),
      ),
    );
  }

}

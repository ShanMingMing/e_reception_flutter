import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

mixin VgPullToRefreshMixin<D, T extends StatefulWidget>
    on BasePagerState<D, T> {
  VgPullRefreshWidget vgPullRefreshWidget = VgPullRefreshWidget();

  bool isInitObserve = false;

  Widget bindPullToRefresh(
      {BasePagerViewModel viewModel,
      BasePagerState state,
      Widget child,
      bool enablePullDown,
      bool enablePullUp,
      Widget header,
      Widget footer,
      Function pullDownMethod,
      Function loadMoreMethod}) {
    if(!(isInitObserve ?? false)){
      (vgPullRefreshWidget ??= VgPullRefreshWidget()).toBindInit(viewModel: viewModel,state: state);
      isInitObserve = true;
    }
    return vgPullRefreshWidget.toBind(
        viewModel: viewModel,
        state: state,
        child: child,
        enablePullDown: enablePullDown,
        enablePullUp: enablePullUp,
        header: header,
        footer: footer,
        pullDownMethod: pullDownMethod,
        loadMoreMethod: loadMoreMethod);
  }

  @override
  void dispose() {
    vgPullRefreshWidget = null;
    isInitObserve = null;
    super.dispose();
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

class VgPullRefreshWidget {

  ///todo:尽可能混入VgPullToRefreshMixin进行数据绑定
  ///因静态方法会重复绑定
  static Widget bind<D, T extends BasePagerBean<D>>(
      {BasePagerViewModel<D, T> viewModel,
      BasePagerState<D, dynamic> state,
      Widget child,
      bool enablePullDown,
      bool enablePullUp,
      Widget header,
      Widget footer,
      Function pullDownMethod,
      Function loadMoreMethod,
        Color refreshBgColor,
      }) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: PullToRefreshUtils.bind<D, T>(
          viewModel: viewModel,
          state: state,
          refreshView: child,
          enablePullDown: enablePullDown,
          enablePullUp: enablePullUp,
          header: header ?? _defaultHeader(refreshBgColor: refreshBgColor),
          footer: footer ?? _defaultFooter(),
          pullDownMethod: pullDownMethod,
          loadMoreMethod: loadMoreMethod),
    );
  }

  Widget toBind<D, T extends BasePagerBean<D>>(
      {BasePagerViewModel<D, T> viewModel,
        BasePagerState<D, dynamic> state,
        Widget child,
        bool enablePullDown,
        bool enablePullUp,
        Widget header,
        Widget footer,
        Function pullDownMethod,
        Function loadMoreMethod}) {
    return _toPullToRefresh(viewModel: viewModel,
        state: state,
        child: child,
        enablePullDown: enablePullDown,
        enablePullUp: enablePullUp,
        header: header ?? _defaultHeader(),
        footer: footer ?? _defaultFooter(),
        pullDownMethod: pullDownMethod,
        loadMoreMethod: loadMoreMethod);
  }

  Widget toBindInit({BasePagerViewModel viewModel,BasePagerState state}){
    viewModel.onDataChanged.listen((d) {
      state.setListData(d);
    });
    viewModel.onNoMoreData.listen((noMore) {
      state.noMoreData(noMore);
    });
    viewModel.onRefreshFailed.listen((msg) {
      if (msg != null) {
        state.refreshFailed(msg);
      }
    });
    viewModel.onLoadFailed.listen((msg) {
      if (msg != null) {
        state.loadFailed(msg);
      }
    });
    viewModel.onEndLoadMore.listen((val) {
      if (val != null && val) {
        state.finishLoadMore();
      }
    });
    viewModel.onEndRefresh.listen((val) {
      if (val != null && val) {
        state.finishRefresh();
      }
    });
    viewModel.onShowEmptyPage.listen((show) {
      if (show != null) {
        state.showEmptyPage(show);
      }
    });
    viewModel.onShowErrorPage.listen((show) {
      if (show != null) {
        state.showErrorPage(show);
      }
    });

  }

  Widget _toPullToRefresh<D, T extends BasePagerBean<D>>({BasePagerViewModel<D, T> viewModel,
    BasePagerState<D, dynamic> state,
    Widget child,
    bool enablePullDown,
    bool enablePullUp,
    Widget header,
    Widget footer,
    Function pullDownMethod,
    Function loadMoreMethod}){

    return RefreshConfiguration(
      hideFooterWhenNotFull: true,
      child: PullToRefreshUtils.defaultRefresher(
          controller: state?.refreshController,
          onRefresh: () {
            if (pullDownMethod != null) {
              pullDownMethod();
            } else {
              viewModel.refresh();
            }
            // viewModel.refresh();
          },
          onLoading: () {
            //  viewModel.loadMore();
            if (loadMoreMethod != null) {
              loadMoreMethod();
            } else {
              viewModel.loadMore();
            }
          },
          enablePullDown: enablePullDown,
          enablePullUp: enablePullUp,
          header: header,
          footer: footer,
          child: child),
    );
  }

  static Widget _defaultHeader({Color refreshBgColor}) {
    final Color textColor = ThemeRepository.getInstance().getTextMainColor_D0E0F7();
    return MaterialClassicHeader(
      backgroundColor:refreshBgColor??Color(0xFF011c3a),
      color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
    );
    return CustomHeader(
      builder: (BuildContext context, RefreshStatus mode) {
        Widget header;
        switch (mode) {
          case RefreshStatus.canRefresh:
            header = Text('松手刷新',style: TextStyle(
              color: textColor
            ),);
            break;
          case RefreshStatus.idle:
            header = Text('下拉刷新',style: TextStyle(
                color: textColor
            ),);
            break;
          case RefreshStatus.refreshing:
            header = CupertinoActivityIndicator();
            break;
          case RefreshStatus.failed:
            header = Text('刷新失败',style: TextStyle(
                color: textColor
            ),);
            break;
          case RefreshStatus.completed:
            header = Text('刷新完成',style: TextStyle(
                color: textColor
            ),);
            break;
          default:
            header = Text('');
            break;
        }
        return Container(
          height: 55.0,
          child: Center(child: header),
        );
      },
    );
  }

  static Widget _defaultFooter() {
    final Color textColor = ThemeRepository.getInstance().getTextMainColor_D0E0F7();

    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget footer;
        switch (mode) {
          // case LoadStatus.idle:
          //   footer = Text(
          //     '上拉加载更多数据',
          //     style: TextStyle(color: textColor),
          //   );
          //   break;
          // case LoadStatus.canLoading:
          //   footer = Text(
          //     '松手加载',
          //     style: TextStyle(color: textColor),
          //   );
          //   break;
          case LoadStatus.failed:
            footer = Text(
              '加载失败',
              style: TextStyle(color: textColor),
            );
            break;
          case LoadStatus.loading:
            // footer = CupertinoActivityIndicator();
          footer = Container(width:20,height: 20,child: CircularProgressIndicator(strokeWidth: 1,));
            break;
          case LoadStatus.noMore:
            footer = Text(
              '没有更多数据了',
              style: TextStyle(color: textColor),
            );
            return Container(height: 0,);
            break;
        }
        return Container(
          height: 55.0,
          child: Center(child: footer),
        );
      },
    );
  }

  static Widget emptyFooter() {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget footer = Container();
        return Container(
          height: 0,
          child: Center(child: footer),
        );
      },
    );
  }
}

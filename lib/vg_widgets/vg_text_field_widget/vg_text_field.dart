import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'custom_length_limiting_text_input_formatter.dart';

///自定义编辑框
class VgTextField extends StatelessWidget {
  //------------------------
  ///自定义编辑框字段

  ///是否限制输入输入
  final int maxLimitLength;

  ///中英是否都算一个字符
  final bool zhCountEqEnCount;

  ///超出极限触发回调
  final ValueChanged<int> limitCallback;

  //------------------------

  ///以下是新系统字段
  TextEditingController controller;

  final bool autofocus;

  final int minLines;

  final int maxLines;

  final int maxLength;

  /// 当前项目统一 颜色 默认主题色
  final Color cursorColor;

  ///默认keyboardAppearance: Brightness.light,
  final Brightness keyboardAppearance;

  final TextAlign textAlign;

  final InputDecoration decoration;

  final ValueChanged<String> onSubmitted;

  final ValueChanged<String> onChanged;

  final VoidCallback onEditingComplete;

  final TextStyle style;

  final FocusNode focusNode;

  final bool maxLengthEnforced;

  final bool readOnly;

  final VoidCallback onTap;

  final List<TextInputFormatter> inputFormatters;

  final TextInputType keyboardType;

  final TextInputAction textInputAction;

  final double cursorWidth;

  final Radius cursorRadius;

  /// {@macro flutter.widgets.editableText.scrollPhysics}
  final ScrollPhysics scrollPhysics;

  //------------------------

  ///以下是类内局部变量
  List<TextInputFormatter> _defaultInputFormatters;

  //------------------------

  VgTextField(
      {Key key,
      this.controller,
      this.autofocus = false,
      this.minLines,
      this.maxLines,
      this.cursorColor = ColorConstants.MAIN_COLOR,
      this.keyboardAppearance = Brightness.light,
      this.textAlign = TextAlign.start,
      this.decoration,
      this.onSubmitted,
      this.onChanged,
      this.onEditingComplete,
      this.style,
      this.focusNode,
      this.maxLimitLength,
      this.zhCountEqEnCount,
      this.textInputAction,
      this.maxLength,
      this.maxLengthEnforced = true,
      this.readOnly = false,
      this.onTap,
      this.inputFormatters,
      this.keyboardType,
      this.limitCallback, this.cursorWidth = 2.0,this.cursorRadius, this.scrollPhysics})
      : super(key: key) {
    if (controller == null) {
      controller = TextEditingController();
    }

    ///判断是否启用自定义长度限制
    _isEnableCustomMaxLengthLimitFormatter();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      autofocus: autofocus,
      controller: controller,
      focusNode: focusNode,
      minLines: minLines,
      maxLines: maxLines,
      cursorColor: cursorColor,
      keyboardAppearance: keyboardAppearance,
      textAlign: textAlign,
      onChanged: onChanged,
      onEditingComplete: onEditingComplete,
      decoration: decoration,
      maxLength: maxLength,
      textInputAction:textInputAction,
      maxLengthEnforced: maxLengthEnforced,
      style: style,
      onSubmitted: onSubmitted,
      readOnly: readOnly,
      onTap: onTap,
      keyboardType: keyboardType,
      cursorRadius: cursorRadius,
      cursorWidth: cursorWidth,
      inputFormatters: inputFormatters ?? _defaultInputFormatters,
        scrollPhysics:scrollPhysics,
    );
  }

  ///是否启用最大长度字符限制
  void _isEnableCustomMaxLengthLimitFormatter() {
    if (maxLimitLength == null || maxLimitLength <= 0) {
      return;
    }
    if (inputFormatters != null) {
      inputFormatters?.add(CustomLengthLimitingTextInputFormatter(
          maxLimitLength,
          limitCallback: limitCallback, zhCountEqEnCount: zhCountEqEnCount));
    } else {
      _defaultInputFormatters = [
        CustomLengthLimitingTextInputFormatter(maxLimitLength,
            limitCallback: limitCallback, zhCountEqEnCount: zhCountEqEnCount)
      ];
    }
  }
}

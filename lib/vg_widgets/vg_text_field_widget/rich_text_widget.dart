import 'dart:collection';

import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

///富文本显示组件
class RichTextWidget extends StatelessWidget {
  final LinkedHashMap<String, Color> contentMap;
  final double fontSize;
  final double height;

  final TextOverflow overFlow;

  const RichTextWidget({Key key, this.contentMap, this.fontSize, this.height,this.overFlow})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<InlineSpan> textSpans = [];
    contentMap.forEach((text, color) {
      textSpans.add(OverFlowTextSpan(
          text: text ?? '',
          style: TextStyle(
              fontFamily: "PingFang",
              height: height ?? 1,
              fontSize: fontSize ?? 12,
              color: color ?? Color(0xff5e687c))));
    });
    return RichText(
      text: OverFlowTextSpan(children: textSpans),
      textAlign: TextAlign.left,
      overflow: overFlow??TextOverflow.ellipsis,
    );
  }


}

import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

///自定义编辑框长度限制格式化
class CustomLengthLimitingTextInputFormatter
    extends LengthLimitingTextInputFormatter {
  ///极限时出发回调
  final ValueChanged<int> limitCallback;
  ///是否中英字符都算一个字符 false的话，中文算俩，英文算1
  final bool zhCountEqEnCount;

  CustomLengthLimitingTextInputFormatter(int maxLength,{this.limitCallback, this.zhCountEqEnCount}) : super(maxLength);

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (maxLength != null &&
        maxLength > 0 &&
        !isMaxLimtCharCountForStr(newValue?.text,
            maxLimitCount: maxLength, zhCountEqEnCount: zhCountEqEnCount??false)) {
      // If already at the maximum and tried to enter even more, keep the old
      // value.
      int positon = getMaxLimtCharPositionForStr(newValue?.text,
          maxLimitCount: maxLength, zhCountEqEnCount: zhCountEqEnCount??false);
      if (positon == null || positon <= 0) {
        return newValue;
      }
      String tmp = newValue?.text?.substring(0, positon);
      //超出极限时截断回调
      if(limitCallback != null){
        limitCallback(maxLength);
      }
      return _trunkSubList(newValue, maxLength, tmp);
    }
    return newValue;
  }

  static TextEditingValue _trunkSubList(
      TextEditingValue value, int maxLength, String subStr) {
    final TextSelection newSelection = value.selection.copyWith(
      baseOffset: math.min(value.selection.start, subStr?.length ?? 0),
      extentOffset: math.min(value.selection.end, subStr?.length ?? 0),
    );
    return TextEditingValue(
      text: subStr,
      selection: newSelection,
      composing: TextRange.empty,
    );
  }

  static int getStrCharCount(String content, bool zhCountEqEnCount){
    if(content == null || content == ""){
      return 0;
    }
    int _zhCount = 0;
    int _enCount = 0;
    for(int i = 0; i< content.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        _enCount ++;
      }else{
        _zhCount ++;
      }
    }
    // VgLogUtil.v("检验总数字符个数：${_zhCount*2 + _enCount}");
    if(zhCountEqEnCount??false){
      //中英字符个数都算1
      return _zhCount*1 + _enCount;
    }else{
      return (_zhCount*2 + _enCount);
    }
  }

  ///限制中英文个数大于
  static bool isMaxLimtCharCountForStr(String content,{int maxLimitCount, bool zhCountEqEnCount}){
    if(content == null || content == ""){
      return true;
    }
    if(maxLimitCount == null || maxLimitCount < 0){
      maxLimitCount = -1;
    }

    int _zhCount = 0;
    int _enCount = 0;
    for(int i = 0; i< content.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        _enCount ++;
      }else{
        _zhCount ++;
      }
    }
    // VgLogUtil.v("检验总数字符个数：${_zhCount*2 + _enCount}");
    if(zhCountEqEnCount??false){
      //中英字符个数都算1
      if(maxLimitCount != -1 && (_zhCount*1 + _enCount) > (maxLimitCount *1)){
        return false;
      }
      return true;
    }else{
      if(maxLimitCount != -1 && (_zhCount*2 + _enCount) > (maxLimitCount *2)){
        return false;
      }
      return true;
    }
  }

  ///限制中英文个数位数
  static int getMaxLimtCharPositionForStr(String content,{int maxLimitCount, bool zhCountEqEnCount}){
    final int defaultPosition = -1;
    if(content == null || content == ""){
      return defaultPosition;
    }
    if(maxLimitCount == null || maxLimitCount <= 0){
      return defaultPosition;
    }

    int tmpBytesValue = 0;
    //中文算1还是算2
    int addCount = (zhCountEqEnCount??false)?1:2;
    for(int i = 0; i< content.length ;i++){

      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        tmpBytesValue = tmpBytesValue + 1;
      }else{
        tmpBytesValue = tmpBytesValue + addCount;
      }
      if(tmpBytesValue > maxLimitCount*addCount){
        return i;
      }
    }
    return defaultPosition;

  }
}

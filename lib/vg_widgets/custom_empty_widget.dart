
import 'package:flutter/widgets.dart';

///自定义空白组件
class CustomEmptyWidget extends StatelessWidget {
  final String imgAsset;
  final String  msg;
  final Color textColor;

  const CustomEmptyWidget({Key key, this.imgAsset, this.msg, this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset(
          imgAsset,
          width: 120,
          gaplessPlayback: true,
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          msg??"暂无内容",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: textColor,
            fontSize: 13,
          ),
        )
      ],
    );
  }


}

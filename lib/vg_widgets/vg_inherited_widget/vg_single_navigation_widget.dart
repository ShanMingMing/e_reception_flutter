import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'vg_inherited_widget.dart';

///单页新建导航，内建InheritedWidget组件
class VgSingleNavigatorWidget<T> extends StatelessWidget {
  static const String ROUTER = "VgSingleNavigatorWidget";

  ///初始数据
  final T initData;

  ///更新条件
  final CustomUpdateShouldNotify customUpdateShouldNotify;

  ///网络请求回调
  final VoidCallback refreshHttpCallback;

  ///主页面
  final Widget mainPage;

  const VgSingleNavigatorWidget({
    Key key,
    @required this.mainPage,
    this.initData,
    this.customUpdateShouldNotify,
    this.refreshHttpCallback,
  })  : assert(mainPage != null, "主页面找不到"),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return VgInheritedWidget<T>(
      initData: initData,
      customUpdateShouldNotify: customUpdateShouldNotify,
      refreshHttpCallback: refreshHttpCallback,
      child: Navigator(
        // onPopPage: (Route<dynamic> route, dynamic result){
        //   print(result?.toString());
        //   return route.didPop(result);
        // },
        onGenerateRoute: (RouteSettings settings) {
          return MaterialPageRoute(
              builder: (BuildContext context) => mainPage ?? Container());
        },
      ),
    );
  }

  ///跳转到新导航页面
  static Future<dynamic> navigatorPush<T>(BuildContext context,
      {T initData,
        String routeName,
      CustomUpdateShouldNotify customUpdateShouldNotify,
      VoidCallback refreshHttpCallback,
      Widget mainPage}) {
    return RouterUtils.routeForFutureResult(
      context,
      VgSingleNavigatorWidget<T>(
        mainPage: mainPage,
        initData: initData,
        customUpdateShouldNotify: customUpdateShouldNotify,
        refreshHttpCallback: refreshHttpCallback,
      ),
      routeName: routeName ?? VgSingleNavigatorWidget.ROUTER,
    );
  }
}

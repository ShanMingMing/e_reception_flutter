import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/vg_menu_widget.dart' as vgMenu;
/// @author: pengboboer
/// @createDate: 1/4/21
class MenuDialogUtils {

  static Future<dynamic> showMenuDialog({BuildContext context,
    LongPressStartDetails longPressDetails,Map<String,ValueChanged<String>> itemMap}) {
    assert(itemMap != null, "items is not empty");


    return vgMenu.showMenu(
      elevation: 1,
      color: Color(0xFF303546),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8)
      ),
      useRootNavigator: true,
        captureInheritedThemes: false,
        context: context,
        position: RelativeRect.fromLTRB(
          longPressDetails.globalPosition.dx,
          longPressDetails.globalPosition.dy,
          longPressDetails.globalPosition.dx,
          longPressDetails.globalPosition.dy,
        ),

      items: itemMap.keys.map((String key){
        return vgMenu.PopupMenuItem<String>(
          value: key,
          onTap: itemMap[key],
          child: Container(
            width: 130,
            height: 45,
            color: Color(0xFF303546),
            alignment: Alignment.center,
            child: Text(
              key ??"",
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  fontSize: 14,
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
              ),
            ),
          ),
        );

      }).toList()
    );
  }


}
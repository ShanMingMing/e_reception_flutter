import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 通用名字和昵称组件
///
/// @author: zengxiangxi
/// @createTime: 1/28/21 5:16 PM
/// @specialDemand:
class CommonNameAndNickWidget extends StatelessWidget {
  final String name;

  final String nick;

  const CommonNameAndNickWidget({Key key, @required this.name, this.nick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CommonConstraintMaxWidthWidget(
          maxWidth: ScreenUtils.screenW(context)/3,
          child: Text(
            VgStringUtils.subStringAndAppendSymbol((name ?? '暂无'), 17, symbol: "...") ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 15,
              height: 1.2
            ),
          ),
        ),
        Offstage(
          offstage: StringUtils.isEmpty(name) || StringUtils.isEmpty(nick),
          child: Padding(
            padding: const EdgeInsets.only(left: 2,right: 2),
            child: Text(
              "/",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 15,
                height: 1.2
              ),
            ),
          ),
        ),
        CommonConstraintMaxWidthWidget(
          maxWidth: ScreenUtils.screenW(context) / 3.5,
          child: Text(
            VgStringUtils.subStringAndAppendSymbol((nick ?? ''), 7, symbol: "...") ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 12,
              height: 1.1
            ),
          ),
        ),
      ],
    );
  }
}

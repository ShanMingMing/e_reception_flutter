
import 'package:e_reception_flutter/net/server_api.dart';

class CommonSmsCodeNetApi{

  ///发送验证码
  static const String SEND_VALIDATION_CODE = ServerApi.BASE_URL + "sms/sendValidationCode";
  ///
  static const String CHECK_CODE = ServerApi.BASE_URL + "app/checkCode";
}
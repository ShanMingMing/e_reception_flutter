import 'package:e_reception_flutter/common_widgets/common_sms_code_widget/constant/common_sms_code_constant.dart';
import 'package:e_reception_flutter/common_widgets/common_sms_code_widget/constant/common_sms_code_net_api.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';


class CommonSmsCodeService{
  ///发送验证码
  void serviceSendSmsCode(String phone,BaseCallback callback){
    if(StringUtils.isEmpty(phone)){
      callback?.onError("手机号不能为空");
      return;
    }
    if(phone.length < DEFAULT_PHONE_LENGTH_LIMIT){
      callback?.onError("手机号不正确");
      return;
    }
    Map<String,String> params = {
      "phone": globalSubPhone(phone) ?? "",
    };
    VgHttpUtils.get(CommonSmsCodeNetApi.SEND_VALIDATION_CODE, params:params,callback: callback);
  }

  void checkSmsCode(String phone, String code, BaseCallback callback){
    if(StringUtils.isEmpty(phone)){
      callback?.onError("手机号不能为空");
      return;
    }
    if(phone.length < DEFAULT_PHONE_LENGTH_LIMIT){
      callback?.onError("手机号不正确");
      return;
    }
    if(StringUtils.isEmpty(code)){
      callback?.onError("验证码不能为空");
      return;
    }
    Map<String,String> params = {
      "phone": globalSubPhone(phone) ?? "",
      "code": code ?? "",
    };
    VgHttpUtils.get(CommonSmsCodeNetApi.CHECK_CODE, params:params,callback: callback);
  }
}

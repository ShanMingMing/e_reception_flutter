import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 通用红数字组件
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 2:37 PM
/// @specialDemand:
class CommonRedNumWidget extends StatelessWidget {

  ///显示的红数字数
  final int sum;

  ///最大显示数
  final int maxNum;

  ///是否显示0
  final bool isShowZero;

  const CommonRedNumWidget(
    this.sum, {
    Key key, this.maxNum, this.isShowZero = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(!isShowZero && (sum == null || sum <= 0)){
      return Container();
    }
    return Container(
      height: 18,
      padding: const EdgeInsets.symmetric(horizontal: 5),
      constraints: BoxConstraints(minWidth: 18),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getMinorRedColor_F95355(),
          borderRadius: BorderRadius.circular(9)),
      child: Center(
        child: Text(
          _getLastlyNumStr(),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Colors.white,
            fontSize: 12,
          ),
        ),
      ),
    );
  }

  ///得到最终的数
  String _getLastlyNumStr(){
    if(sum == null || sum < 0){
      return "0";
    }
    if(maxNum != null && maxNum > 0){
      return maxNum > sum ? "$sum" : "$maxNum";
    }
    return "$sum";
  }
}

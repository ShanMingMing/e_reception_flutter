import 'package:e_reception_flutter/constants/constant.dart';
import 'package:flutter/widgets.dart';

/// 通用固定大小确定按钮组件
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 10:14 AM
/// @specialDemand:
class CommonFixedHeightConfirmButtonWidget extends StatefulWidget {
  ///是否有效
  final bool isAlive;

  ///是否开启体育场(默认启用)
  final bool isEnableStadium;

  ///组件固定宽度
  final double width;

  ///组件固定高度
  final double height;

  ///选中背景颜色
  final Color selectedBgColor;

  ///未选中背景颜色
  final Color unSelectBgColor;

  ///内部文字
  final String text;

  ///未选中内部文字样式
  final TextStyle selectedTextStyle;

  ///未选中内部文字样式
  final TextStyle unSelectTextStyle;

  final EdgeInsetsGeometry padding;

  final EdgeInsetsGeometry margin;

  final Widget textRightSelectedWidget;

  final Widget textRightUnSelectWidget;

  final Widget textLeftSelectedWidget;

  final Widget textLeftUnSelectWidget;

  final VoidCallback onTap;

  final VoidCallback onDoubleTap;

  final int disableMillTime;

  final BoxBorder selectedBoxBorder;

  final BoxBorder unSelectBoxBorder;

  final double textSize;

  final BorderRadius radius;




  const CommonFixedHeightConfirmButtonWidget({
    Key key,
    @required this.isAlive,
    @required this.height,
    @required this.selectedBgColor,
    @required this.text,
    @required this.selectedTextStyle,
    this.isEnableStadium = true,
    this.padding,
    this.width,
    this.unSelectBgColor,
    this.unSelectTextStyle,
    this.margin,
    this.textRightSelectedWidget,
    this.textRightUnSelectWidget,
    this.textLeftSelectedWidget,
    this.textLeftUnSelectWidget,
    this.radius,
    this.onTap,
    this.onDoubleTap, this.disableMillTime = 300, this.selectedBoxBorder, this.unSelectBoxBorder, this. textSize,
  })  : assert(isAlive != null),
        assert(height != null && height > 0),
        assert(selectedBgColor != null),
        assert(unSelectBgColor != null),
        assert(text != null),
        assert(selectedTextStyle != null),
        assert(unSelectTextStyle != null),
        super(key: key);

  @override
  _CommonFixedHeightConfirmButtonWidgetState createState() =>
      _CommonFixedHeightConfirmButtonWidgetState();
}

class _CommonFixedHeightConfirmButtonWidgetState
    extends State<CommonFixedHeightConfirmButtonWidget> {
  ///避免多次点击响应
  bool _isDisable = false;

  @override
  Widget build(BuildContext context) {
    Widget child = AnimatedContainer(
      duration: DEFAULT_ANIM_DURATION,
      width: widget.width,
      height: widget.height,
      padding: widget.padding,
      margin: widget.margin,
      decoration: BoxDecoration(
          border: widget.isAlive ? widget.selectedBoxBorder : widget.unSelectBoxBorder,
          color:
              widget.isAlive ? widget.selectedBgColor : widget.unSelectBgColor,
          borderRadius: widget.radius!=null?widget.radius:BorderRadius.circular(
              widget.isEnableStadium ? widget.height / 2 : 0)),
      child: Center(
        child: AnimatedDefaultTextStyle(
          style: widget.isAlive
              ? widget.selectedTextStyle
              : widget.unSelectTextStyle,
          duration: DEFAULT_ANIM_DURATION,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              (widget.isAlive
                      ? widget.textLeftSelectedWidget
                      : widget.textLeftUnSelectWidget) ??
                  Container(),
              Text(
                widget.text ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(height: 1.2,fontSize: widget.textSize??14),
              ),
              (widget.isAlive
                      ? widget.textRightSelectedWidget
                      : widget.textRightUnSelectWidget) ??
                  Container()
            ],
          ),
        ),
      ),
    );
    if (widget.onTap == null && widget.onDoubleTap == null) {
      return child;
    }
    child = GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(_isDisable){
          print("点击操作过于频繁");
          return;
        }
        _delayedClickTimer();
        widget?.onTap?.call();
      },
      // onDoubleTap: (){
      //   if(_isDisable){
      //     print("双击操作过于频繁");
      //     return;
      //   }
      //   _delayedClickTimer();
      //   widget?.onDoubleTap?.call();
      // },
      child: child,
    );
    return child;
  }

  void _delayedClickTimer(){
    if(_isDisable){
      return;
    }
    _isDisable = true;
    Future.delayed(Duration(milliseconds: widget?.disableMillTime),(){
      _isDisable = false;
    });
  }
}

///48*24
// CommonFixedHeightConfirmButtonWidget(
//     isAlive: false,
//     width: 48,
//     height: 24,
//     unSelectBgColor: ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
//     selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
//     unSelectTextStyle: TextStyle(
//       color: VgColors.INPUT_BG_COLOR,
//       fontSize: 12,
//       fontWeight: FontWeight.w600
//     ),
//     selectedTextStyle: TextStyle(
//       color: Colors.white,
//       fontSize: 12,
//       fontWeight: FontWeight.w600
//   ),
//   text: "保存",
// )

// CommonFixedHeightConfirmButtonWidget(
// isAlive: false,
// height: 40,
// margin: const EdgeInsets.symmetric(horizontal: 25),
// unSelectBgColor: Color(0xFF3A3F50),
// selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
// unSelectTextStyle: TextStyle(
// color: VgColors.INPUT_BG_COLOR,
// fontSize: 15,
// ),
// selectedTextStyle: TextStyle(
// color: Colors.white,
// fontSize: 15,
// ),
// text: "提交",
// );

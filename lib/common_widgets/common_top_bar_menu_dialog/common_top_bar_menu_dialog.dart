import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'bean/common_top_bar_menu_list_item_bean.dart';

/// topbar通用弹窗
///
/// @author: zengxiangxi
/// @createTime: 2/4/21 4:34 PM
/// @specialDemand:
class CommonTopBarMenuDialog<T> extends StatelessWidget {
  final List<CommonTopBarMenuListItemBean<T>> menuList;

  final T selectedValue;

  const CommonTopBarMenuDialog({Key key, this.menuList, this.selectedValue})
      : super(key: key);

  ///跳转方法
  ///返回 DateTime
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      List<CommonTopBarMenuListItemBean> menuList, dynamic selectedValue) {
    if (menuList == null || menuList.isEmpty) {
      return null;
    }
    return VgDialogUtils.showCommonDialog(
        context: context,
        child: CommonTopBarMenuDialog(
          menuList: menuList,
          selectedValue: selectedValue,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Material(
          type: MaterialType.transparency,
          child: Align(
              alignment: Alignment.topCenter, child: _toMainListWidget(context))),
    );
  }

  Widget _toMainListWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 44 + ScreenUtils.getStatusBarH(context)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Container(
          width: 160,
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: menuList.map((e) => _toListItemWidget(context,e)).toList(),
          ),
        ),
      ),
    );
  }

  Widget _toListItemWidget(BuildContext context,CommonTopBarMenuListItemBean itemBean) {
    if (itemBean == null) {
      return Container();
    }
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
        itemBean?.onTap?.call(itemBean);
      },
      child: Container(
        height: 45,
        child: Center(
          child: Text(
            itemBean?.text ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: itemBean.value == selectedValue
                    ? ThemeRepository.getInstance().getMinorYellowColor_FFB714()
                    : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 14,
                height: 1.2),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';

class CommonTopBarMenuListItemBean<T>{

  final String text;
  final ValueChanged<CommonTopBarMenuListItemBean<T>> onTap;
  final T value;

  CommonTopBarMenuListItemBean({this.text, this.onTap, this.value});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CommonTopBarMenuListItemBean &&
          runtimeType == other.runtimeType &&
          value == other.value;

  @override
  int get hashCode => value.hashCode;
}
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:flutter/widgets.dart';

///通用单选按钮状态
enum CommonSingleChoiceStatus{
  selected,
  unSelect,
  disable,
}

extension CommonSingleChoiceStatusExtension on CommonSingleChoiceStatus{
  String getAssetUrl(){
    switch(this){
      case CommonSingleChoiceStatus.selected:
        return "images/common_selected_ico.png";
      case CommonSingleChoiceStatus.unSelect:
        return "images/common_unselect_ico.png";
      case CommonSingleChoiceStatus.disable:
        return "images/common_select_disable_ico.png";
    }
    return "images/common_unselect_ico.png";
  }
}

/// 通用单选按钮选择组件
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 10:59 AM
/// @specialDemand:
class CommonSingleChoiceButtonWidget extends StatelessWidget {

  final CommonSingleChoiceStatus status;

  final double width;

  const CommonSingleChoiceButtonWidget(this.status,{Key key, this.width = 20, }) :assert(status != null), super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: DEFAULT_ANIM_DURATION,
      child: Image.asset(
        status?.getAssetUrl() ?? "",
        key: ValueKey(status),
        gaplessPlayback: true,
        width: width,
      ),
    );
  }
}

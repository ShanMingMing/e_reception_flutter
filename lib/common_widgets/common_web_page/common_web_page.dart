import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_log_lib.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// 通用WebView页
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 2:41 PM
/// @specialDemand:
class CommonWebPage extends StatefulWidget {
  final String url;

  final String title;

  ///路由名称
  static const String ROUTER = "CommonWebPage";

  const CommonWebPage({
    Key key,
    this.url,
    this.title,
  }) : super(key: key);

  @override
  _CommonWebPageState createState() => _CommonWebPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, String url, String title) {

    LogUtils.d("打印WebView路径: $url");
    return RouterUtils.routeForFutureResult(
      context,
      CommonWebPage(
        url: url,
        title: title,
      ),
      routeName: CommonWebPage.ROUTER,
    );
  }
}

class _CommonWebPageState extends State<CommonWebPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        VgTopBarWidget(
          isShowBack: true,
          title: widget?.title ?? "",
        ),
        Expanded(
          child: _toWebViewWidget(),
        )
      ],
    );
  }

  Widget _toWebViewWidget(){
    return WebView(
      initialUrl: widget?.url,
      javascriptMode: JavascriptMode.unrestricted,
      onPageFinished: (String url){
        // setState(() {
        //   _isLoading = false;
        //   setState(() {
        //
        //   });
        // });
      },
    );
  }
}


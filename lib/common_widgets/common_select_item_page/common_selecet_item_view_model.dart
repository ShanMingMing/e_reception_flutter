import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'common_select_item_page.dart';

class CommonSelectItemViewModel<D, T extends BasePagerBean<D>>  extends BasePagerViewModel<D,T>{

  final CommonSelectItemPageState state;
  CommonSelectItemViewModel(this.state) : super(state);

  @override
  Map<String, dynamic> getQuery(int page) {
    return state?.widget?.queryMap;
  }

  @override
  String getRequestMethod() {
    return state?.widget?.httpType?.getRequestMethod();
  }

  @override
  String getUrl() {
    return state?.widget?.netUrl ?? "";
  }

  @override
  T parseData(VgHttpResponse resp) {
    T vo = state?.widget?.parseDataFunc(resp);
    loading(false);
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

}
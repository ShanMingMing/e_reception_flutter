import 'package:e_reception_flutter/common_widgets/common_select_item_page/common_selecet_item_view_model.dart';
import 'package:e_reception_flutter/common_widgets/common_select_item_page/widgets/common_select_item_list_item_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

typedef CommonSelectItemParseDataCallback<T> = T Function(VgHttpResponse resp);

typedef CommonSelectItemGetTextCallback<T> = String Function(T itemBean);

/// 通用选择一项的列表页
///
/// @author: zengxiangxi
/// @createTime: 1/25/21 11:22 AM
/// @specialDemand:
class CommonSelectItemPage<D, T extends BasePagerBean<D>>
    extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CommonSelectItemPage";

  ///标题
  final String title;

  ///请求Map
  final Map<String, dynamic> queryMap;

  ///请求类型
  final VgHttpType httpType;

  ///请求网址
  final String netUrl;

  ///解析回调
  final CommonSelectItemParseDataCallback<T> parseDataFunc;

  ///获取文本回调
  final CommonSelectItemGetTextCallback<D> getTextFunc;

  ///已选中项
  final D selectedItem;

  const CommonSelectItemPage(
      {Key key,
      this.queryMap,
      this.httpType,
      this.netUrl,
      this.parseDataFunc,
      this.title,
      this.getTextFunc,
      this.selectedItem})
      : super(key: key);

  @override
  CommonSelectItemPageState createState() => CommonSelectItemPageState<D, T>();

  ///跳转方法
  static Future<D> navigatorPush<D, T extends BasePagerBean<D>>(
    BuildContext context,
    final String title,
    final Map<String, dynamic> queryMap,
    final VgHttpType httpType,
    final String netUrl,
    final CommonSelectItemParseDataCallback<T> parseDataFunc,
    final CommonSelectItemGetTextCallback<D> getTextFunc,
    final D selectedItem,
  ) {
    return RouterUtils.routeForFutureResult(
      context,
      CommonSelectItemPage<D, T>(
        title: title,
        queryMap: queryMap,
        httpType: httpType,
        netUrl: netUrl,
        parseDataFunc: parseDataFunc,
        getTextFunc: getTextFunc,
        selectedItem: selectedItem,
      ),
      routeName: CommonSelectItemPage.ROUTER,
    );
  }
}

class CommonSelectItemPageState<D, T extends BasePagerBean<D>>
    extends BasePagerState<D, CommonSelectItemPage>
    with VgPlaceHolderStatusMixin {
  CommonSelectItemViewModel<D, T> _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CommonSelectItemViewModel(this);
    _viewModel?.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toPlaceStatusWidget(),
        ),
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: widget?.title ?? "",
      isShowBack: true,
    );
  }

  Widget _toPlaceStatusWidget() {
    return MixinPlaceHolderStatusWidget(
      emptyOnClick: () => _viewModel?.refresh(),
      errorOnClick: () => _viewModel?.refresh(),
      child: VgPullRefreshWidget.bind(
          state: this,
          viewModel: _viewModel,
          child: ListView.separated(
              itemCount: data?.length ?? 0,
              padding: const EdgeInsets.all(0),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              itemBuilder: (BuildContext context, int index) {
                return CommonSelectItemListItemWidget<D>(
                  index: index,
                  itemBean: data?.elementAt(index),
                  getTextFunc: widget?.getTextFunc,
                  selectedItem: widget.selectedItem,
                  onTap: (D itemBean) {
                    RouterUtils.pop(context, result: itemBean);
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Container(
                  height: 0.5,
                  color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  margin: const EdgeInsets.only(left: 15, right: 1),
                );
              })),
    );
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/widgets.dart';

import '../common_select_item_page.dart';

///通用选择一项的列表页-列表项
///
/// @author: zengxiangxi
/// @createTime: 1/25/21 11:27 AM
/// @specialDemand:
class CommonSelectItemListItemWidget<T> extends StatelessWidget {

  final int index;

  final T itemBean;

  ///获取文本回调
  final CommonSelectItemGetTextCallback<T> getTextFunc;

  ///已选中项
  final T selectedItem;

  final ValueChanged<T> onTap;

  const CommonSelectItemListItemWidget({Key key, this.index, this.itemBean, this.getTextFunc, this.selectedItem, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
        onTap: (){
          onTap?.call(itemBean);
        },
        child: _toListItemWidget());
  }

  Widget _toListItemWidget() {
    final bool isSelected = selectedItem == itemBean;
    return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: <Widget>[
          Text(
            getTextFunc?.call(itemBean) ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color:isSelected? ThemeRepository.getInstance().getPrimaryColor_1890FF() : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 15,
            ),
          ),
          Spacer(),
          Offstage(
              offstage:  !isSelected,
              child: Image.asset("images/current_ico.png",width: 12.3,)),
        ],
      ),

    );
  }

}

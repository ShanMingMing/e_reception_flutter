import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_preview/photo_preview_export.dart';

class CommonEditCallbackWidget extends StatefulWidget {
  final double height; //自定义高度
  final Color backGroundColor; //自定义背景颜色
  final String customIcon; //自定义图标
  final double iconWidth; //图标大小
  final Color iconColor; //图标颜色
  final bool showIcon;//是否显示图标
  final String hintText; //自定义输入框
  final double fontSize;//字体大小
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final Function onTap;
  final TextInputAction textInputAction;
  final TextEditingController editingController;
  final EdgeInsetsGeometry contentPadding;
   //自动弹起键盘
  final bool  autoFocus;
  const CommonEditCallbackWidget({
    Key key,
    this.height = 50,
    this.backGroundColor = const Color(0xFF21263C),
    this.customIcon = "images/add_ico.png",
    this.iconWidth = 17,
    this.iconColor = const Color(0xFF1890FF),
    this.hintText,
    this.onChanged,
    this.onSubmitted, this.onTap, this.contentPadding, this.fontSize=13, this.textInputAction=TextInputAction.search, this.editingController, this.showIcon, this.autoFocus,
  }) : super(key: key);

  @override
  _CommonEditCallbackWidgetState createState() =>
      _CommonEditCallbackWidgetState();
}

class _CommonEditCallbackWidgetState extends State<CommonEditCallbackWidget> {
  TextEditingController editingController;


  @override
  void initState() {
    super.initState();
    editingController=widget.editingController??TextEditingController();
  }

  @override
  void dispose() {
    editingController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      color: widget.backGroundColor,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 15,
          ),
          GestureDetector(
            onTap: widget?.onTap,
            child: Offstage(
              offstage: !(widget.showIcon??true),
              child: Container(
                width: widget.iconWidth,
                margin: EdgeInsets.only(right: 10),
                child: Image.asset(
                  widget?.customIcon,
                  width: widget.iconWidth,
                  color: widget.iconColor,
                ),
              ),
            ),
          ),
          Expanded(
            child:VgTextField(
              controller: editingController,
              maxLines: 1,
              autofocus: widget.autoFocus??false,
              textInputAction: widget.textInputAction,
              onChanged: widget?.onChanged,
              onSubmitted: widget?.onSubmitted,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: widget.contentPadding,
                counterText: "",
                hintText: widget.hintText ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: widget.fontSize,
                  color:
                  ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  height: 1.3),
            ),
          ),
        ],
      ),
    );
  }
}

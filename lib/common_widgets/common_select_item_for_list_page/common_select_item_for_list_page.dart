import 'package:e_reception_flutter/common_widgets/common_select_item_for_list_page/widgets/common_select_item_for_list_item_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

typedef CommonSelectForListItemGetTextCallback<T> = String Function(T itemBean);

/// 通用选择一项的列表页（传已值列表）
///
/// @author: zengxiangxi
/// @createTime: 1/25/21 11:22 AM
/// @specialDemand:
class CommonSelectItemForListPage<T> extends StatefulWidget {
  final String title;

  ///获取文本回调
  final CommonSelectForListItemGetTextCallback<T> getTextFunc;

  ///已选中项
  final T selectedItem;

  final List<T> list;

  const CommonSelectItemForListPage(
      {Key key, this.title, this.getTextFunc, this.selectedItem, this.list})
      : super(key: key);

  @override
  CommonSelectItemForListPageState createState() =>
      CommonSelectItemForListPageState<T>();

  ///跳转方法
  static Future<T> navigatorPush<T>(BuildContext context,
      {List<T> list,
      CommonSelectForListItemGetTextCallback<T> getTextFunc,
      String title,
      T selectedItem}) {
    return RouterUtils.routeForFutureResult<T>(
      context,
      CommonSelectItemForListPage<T>(
        list: list,
        title: title,
        getTextFunc: getTextFunc,
        selectedItem: selectedItem,
      ),
    );
  }
}

class CommonSelectItemForListPageState<T>
    extends BaseState<CommonSelectItemForListPage<T>> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnnotatedRegion(
       Scaffold(
        backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        body: _toMainColumnWidget(),
      ),
      navigationBarColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toPlaceStatusWidget(),
        ),
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: widget?.title ?? "",
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  Widget _toPlaceStatusWidget() {
    return VgPlaceHolderStatusWidget(
      child: ListView.separated(
          itemCount: widget.list?.length ?? 0,
          padding: const EdgeInsets.all(0),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          itemBuilder: (BuildContext context, int index) {
            return CommonSelectItemForListItemWidget<T>(
              index: index,
              itemBean: widget.list?.elementAt(index),
              getTextFunc: widget?.getTextFunc,
              selectedItem: widget.selectedItem,
              onTap: (T itemBean) {
                print("选择返回值:---${itemBean}");
                RouterUtils.pop(context, result: itemBean);
              },
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return Container(
              height: 0.5,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
              margin: const EdgeInsets.only(left: 15, right: 1),
            );
          }),
    );
  }
}

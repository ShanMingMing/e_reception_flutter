import 'package:flutter/widgets.dart';

/// 通用约束最大宽度组件
///
/// @author: zengxiangxi
/// @createTime: 1/15/21 2:31 PM
/// @specialDemand:
class CommonConstraintMaxWidthWidget extends StatelessWidget {
  final Widget child;

  final double maxWidth;

  const CommonConstraintMaxWidthWidget({
    Key key,
    @required this.maxWidth,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: maxWidth
      ),
      child: child,
    );
  }
}

import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:transformer_page_view/transformer_page_view.dart';

/// 通用层叠翻页swiper组件
///
/// @author: zengxiangxi
/// @createTime: 2/3/21 9:10 PM
/// @specialDemand:
class CommonStackPageWidget extends StatefulWidget {
  @override
  _CommonStackPageWidgetState createState() => _CommonStackPageWidgetState();
}

class _CommonStackPageWidgetState extends State<CommonStackPageWidget>
    with TickerProviderStateMixin {
  final List<Color> _colorsList = [
    Colors.blue,
    Colors.red,
    Colors.grey,
    Colors.purple,
    Colors.amber
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _toMainWidget(),
      ),
    );
  }

  Widget _toMainWidget() {
    return Container(
        alignment: Alignment.center,
        height: 300,
        // width: 300,
        color: Colors.blue[200],
        child: TransformerPageView(
            loop: true,
            transformer: new ScaleAndFadeTransformer(),
            pageController: TransformerPageController(viewportFraction: 0.5),
            itemBuilder: (BuildContext context, int index) {
              return Center(
                child: new Container(
                  width: 240,
                  height: 135,
                  color: _colorsList[index % _colorsList.length],
                  child: new Center(
                    child: new Text(
                      "$index",
                      style: new TextStyle(fontSize: 80.0, color: Colors.white),
                    ),
                  ),
                ),
              );
            },
            itemCount: 3));
  }

  Widget _toMWidget(int index, Color color) {
    return _getItemWidget(index, color);
  }

  Widget _getItemWidget(int index, Color color) {
    return Center(
      child: Container(
        width: 120,
        height: 135,
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
      ),
    );
  }
}

class ScaleAndFadeTransformer extends PageTransformer {
  final double _scale;
  final double _fade;

  ScaleAndFadeTransformer({double fade: 0.3, double scale: 0.8})
      : _fade = fade,
        _scale = scale;

  @override
  Widget transform(Widget item, TransformInfo info) {
    double position = info.position;
    double scaleFactor = (1 - position.abs()) * (1 - _scale);
    double fadeFactor = (1 - position.abs()) * (1 - _fade);
    double opacity = _fade + fadeFactor;
    double scale = _scale + scaleFactor;
    return new Opacity(
      opacity: opacity,
      child: new Transform.scale(
        scale: scale,
        child: item,
      ),
    );
  }
}

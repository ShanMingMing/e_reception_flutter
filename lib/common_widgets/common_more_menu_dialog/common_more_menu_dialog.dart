import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 更多菜单弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 6:42 PM
/// @specialDemand:
class CommonMoreMenuDialog extends StatelessWidget {
  final Map<String, VoidCallback> map;
  final Color bgColor;
  final Color splitColor;
  final Color textColor;
  final Color lineColor;

  const CommonMoreMenuDialog({Key key, this.map,
    this.bgColor, this.splitColor, this.textColor, this.lineColor
  }) : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(
      BuildContext context, Map<String, VoidCallback> map,
      {Color bgColor, Color splitColor, Color textColor, Color lineColor}
      ) {
    if (map == null || map.isEmpty) {
      return null;
    }
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      child: CommonMoreMenuDialog(
        map: map,
        bgColor:bgColor,
        splitColor:splitColor,
        textColor:textColor,
        lineColor:lineColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
      child: Container(
        decoration: BoxDecoration(
            color: bgColor??ThemeRepository.getInstance().getCardBgColor_21263C()),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _toMainColumnWidget(context),
            Container(
              height: 10,
              color: splitColor??ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            ),
            _toCancelButtonWidget(context),
          ],
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    int mapLength = map?.length ?? 0;
    int i = 0;
    return ListView(
      shrinkWrap: true,
      padding: const EdgeInsets.all(0),
      physics: NeverScrollableScrollPhysics(),
      children: map.keys.map((String key) {
        i++;
        return _toListItemWidget(context,key, map[key],isshowLines: i <= (mapLength - 1));
      }).toList(),
    );
  }

  Widget _toListItemWidget(BuildContext context,String title, VoidCallback onTap,{bool isshowLines = false,}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
        if(onTap != null){
          onTap();
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 55,
            child: Center(
              child: Text(
                title ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: textColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 16,
                    height: 1.2),
              ),
            ),
          ),
          Offstage(
            offstage: !isshowLines,
            child: Container(
              height: 0.5,
              color: lineColor??ThemeRepository.getInstance().getLineColor_3A3F50(),
            ),
          )
        ],
      ),
    );
  }

  Widget _toCancelButtonWidget(BuildContext context) {
    final double bottomStatusHeight =  ScreenUtils.getBottomBarH(context);
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 55 + bottomStatusHeight,
        padding: EdgeInsets.only(bottom: bottomStatusHeight),
        child: Center(
          child: Text(
            "取消",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: textColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
        ),
      ),
    );
  }
}

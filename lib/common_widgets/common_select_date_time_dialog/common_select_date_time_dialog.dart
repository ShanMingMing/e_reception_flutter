import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/generated/l10n.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_date_picker/vg_date_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 通用选择日期时间组件弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 3:35 PM
/// @specialDemand:
class CommonSelectDateTimeDialog extends StatelessWidget {
  final VgCupertinoDatePickerMode mode;

  ///返回结果map 返回日期的key
  static const String RESULT_DATE_TIME = "result_date_time";

  final TextStyle textStyle;

  final DateTime initDateTime;

  final DateTime minDateTime;

  final DateTime maxDateTime;

  final int minuteInterval;

  //是否显示清除按钮
  final bool showClear;

  CommonSelectDateTimeDialog(this.mode,
      {Key key,
      this.textStyle,
      this.initDateTime,
      this.minDateTime,
      this.maxDateTime,
      this.showClear = false,
      this.minuteInterval})
      : assert(mode != null),
        super(key: key) {
    _resultDateTime = initDateTime ?? DateTime.now();
  }

  ///返回值记录
  DateTime _resultDateTime;

  ///跳转方法
  ///返回 DateTime
  static Future<Map<String, dynamic>> navigatorPushDialog(
      BuildContext context, VgCupertinoDatePickerMode mode,
      {TextStyle textStyle,
      DateTime initDateTime,
      DateTime minDateTime,
      DateTime maxDateTime,
      int minuteInterval,
      bool showClear}) {
    return VgDialogUtils.showCommonBottomDialog<Map<String, dynamic>>(
      context: context,
      child: CommonSelectDateTimeDialog(
        mode,
        textStyle: textStyle,
        initDateTime: initDateTime,
        minDateTime: minDateTime,
        maxDateTime: maxDateTime,
        minuteInterval: minuteInterval ?? 0,
        showClear: showClear,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS))),
      child: _toMainColumnWidget(context),
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _toConfirmAndButtonWidget(context),
        _toDateTimeWidget(),
        SizedBox(
          height: 15 + ScreenUtils.getBottomBarH(context),
        )
      ],
    );
  }

  Widget _toDateTimeWidget() {
    return Container(
        height: 175,
        child: CupertinoTheme(
          data: CupertinoThemeData(
              textTheme: CupertinoTextThemeData(
                  dateTimePickerTextStyle:
                      textStyle ?? TextStyle(color: Colors.white))),
          child: VgCupertinoDatePicker(
            mode: mode,
            use24hFormat: true,
            initialDateTime: initDateTime,
            minimumDate: minDateTime,
            maximumDate: maxDateTime,
            minuteInterval: minuteInterval,
            onDateTimeChanged: (DateTime newDateTime) {
              _resultDateTime = newDateTime;
            },
          ),
        ));
  }

  Widget _toConfirmAndButtonWidget(BuildContext context) {
    return Container(
      height: 54,
      child: Row(
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => RouterUtils.pop(context),
            child: Container(
              width: 58,
              child: Center(
                child: Text(
                  "取消",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: showClear,
            child: Row(
              children: <Widget>[
                Container(
                  width: 0.5,
                  height: 12,
                  color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                ),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => _processClearPop(context),
                  child: Container(
                    width: 86,
                    child: Center(
                      child: Text(
                        "清除设置",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => _processConfirmPop(context),
            child: Container(
              width: 60,
              child: Center(
                child: Text(
                  "确定",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _processConfirmPop(BuildContext context) {
    RouterUtils.pop(context, result: {RESULT_DATE_TIME: _resultDateTime});
  }

  _processClearPop(BuildContext context) {
    RouterUtils.pop(context, result: {RESULT_DATE_TIME: null});
  }
}

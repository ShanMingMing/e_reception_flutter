import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommonISeeDialog extends StatelessWidget {
  final String content;
  final String title;
  final Color confirmBgColor;
  final Color titleColor;
  final Color contentColor;
  final Color widgetBgColor;

  const CommonISeeDialog({Key key, this.content, this.title, this.confirmBgColor,
    this.titleColor, this.contentColor, this.widgetBgColor,}) : super(key: key);

  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,{String title,String content,Color confirmBgColor,
    Color titleColor, Color contentColor, Color widgetBgColor}) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: CommonISeeDialog(
          content: content,
          confirmBgColor: confirmBgColor,
          titleColor: titleColor,
          contentColor: contentColor,
          widgetBgColor: widgetBgColor,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            color: widgetBgColor??ThemeRepository.getInstance().getCardBgColor_21263C(),
            width: 290,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 126,right: 124,top: 25),
                      child: Container(
                        width: 40,
                        height: 20,
                        child: Text(
                          "提示",
                          style: TextStyle(
                            fontSize: 16,
                            color: titleColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 13,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    content ?? "是否取消吗？",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 5,
                    style: TextStyle(
                      color: contentColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 14,
                    ),
                  ),
                ),
                SizedBox(height: 30,),
                CommonFixedHeightConfirmButtonWidget(
                  isAlive: true,
                  height: 40,
                  radius:BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
                  unSelectBgColor: Color(0xff3a3f50),
                  selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  unSelectTextStyle: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                      fontSize: 14,
                  ),
                  selectedTextStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                  ),
                  text: "我知道了",
                  onTap: (){
                    RouterUtils.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

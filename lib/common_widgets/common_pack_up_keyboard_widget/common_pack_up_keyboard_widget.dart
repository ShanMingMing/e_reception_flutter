import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/widgets.dart';

///外部收起键盘
class CommonPackUpKeyboardWidget extends StatelessWidget {
  final Widget child;

  ///是否开启垂直移除焦点
  final bool isEnableVerical;

  const CommonPackUpKeyboardWidget({Key key, @required this.child, this.isEnableVerical = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onVerticalDragUpdate: (_){
        if(isEnableVerical){
          VgToolUtils.removeAllFocus(context);
        }
      },
      onTap: (){
        VgToolUtils.removeAllFocus(context);
      },
      child: _toNotifationWidget(context),
    );
  }

  Widget _toNotifationWidget(BuildContext context){
    return NotificationListener<ScrollUpdateNotification>(
      child: child,
      onNotification: (ScrollUpdateNotification notification) {
        final FocusScopeNode focusScope = FocusScope.of(context);
        if (notification.dragDetails != null && focusScope.hasFocus) {
          focusScope.unfocus();
        }
        return false;
      },
    );
  }
}

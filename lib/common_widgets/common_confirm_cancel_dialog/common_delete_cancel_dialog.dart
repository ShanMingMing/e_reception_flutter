import 'dart:ffi';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_widget_lib.dart';

/// 通用删除取消弹窗
class CommonDeleteCancelDialog extends StatelessWidget {

  final String content;

  final String cancelText;

  final String confirmText;

  final Color confirmBgColor;

  final Color cancelBgColor;

  final Color titleColor;

  final Color contentColor;

  final Color widgetBgColor;

  final VoidCallback onConfrim;

  final VoidCallback onPop;

  final bool isShowLeftButton;

  final Widget contentWidget;

  const CommonDeleteCancelDialog({Key key, this.content,
    this.cancelText, this.confirmText, this.confirmBgColor, this.cancelBgColor,
    this.titleColor, this.contentColor, this.widgetBgColor,
    this.onConfrim, this.onPop, this.isShowLeftButton = true, this.contentWidget}) : super(key: key);

  static Future<bool> navigatorPushDialog(BuildContext context,{String content,
    String cancelText,String confirmText,Color confirmBgColor,
    Color cancelBgColor, Color titleColor, Color contentColor, Color widgetBgColor,
    Widget contentWidget, bool isShowLeftButton,
    VoidCallback onPop, VoidCallback onConfrim}){
    return VgDialogUtils.showCommonDialog<bool>(context: context, child: CommonDeleteCancelDialog(
      content: content,
      cancelText: cancelText,
      confirmText: confirmText,
      confirmBgColor: confirmBgColor,
      cancelBgColor: cancelBgColor,
      titleColor: titleColor,
      contentColor: contentColor,
      widgetBgColor: widgetBgColor,
      contentWidget: contentWidget,
      onPop: onPop,
      onConfrim: onConfrim,
      isShowLeftButton: isShowLeftButton,
    ),barrierDismissible: false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // behavior: HitTestBehavior.translucent,
      onTap: ()=> RouterUtils.pop(context),
      child: UnconstrainedBox(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){},
          child: Material(
            type: MaterialType.transparency,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                width: 290,
                decoration: BoxDecoration(
                  color: widgetBgColor??Colors.white
                ),
                child: _toMainColumnWidget(context),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 30,),
         Padding(
           padding: const EdgeInsets.symmetric(horizontal: 20),
           child: contentWidget??Text(
                     content ?? "确定删除？",
                     overflow: TextOverflow.ellipsis,
                     maxLines: 5,
                     style: TextStyle(
                       color: contentColor??ThemeRepository.getInstance().getCardBgColor_21263C(),
                       fontSize: 16,
                       fontWeight: FontWeight.w600,
                         ),
                   ),
         ),
        SizedBox(height: 30,),
        _toTwoButtonWidget(context),
        SizedBox(height: 20,),
      ],
    );
  }

  Widget _toTwoButtonWidget(BuildContext context){
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: <Widget>[
          if(isShowLeftButton ?? true)
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(onPop != null){
                  onPop?.call();
                  return ;
                }
                RouterUtils.pop(context,result: false);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: cancelBgColor??Color(0XFFF6F7F9),
                ),
                child: Center(
                  child:  Text(
                    cancelText ?? "取消",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: VgColors.DEFAULT_FIELD_COLOR,
                                fontSize: 14,
                              height: 1.2
                                ),
                          ),
                ),
              ),
            ),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(onConfrim != null){
                  onConfrim?.call();
                  return ;
                }
                RouterUtils.pop(context,result: true);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color:confirmBgColor?? Color(0XFFFF5269),
                ),
                child: Center(
                  child:  Text(
                   confirmText ?? "删除",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                        height: 1.2
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:ffi';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_widget_lib.dart';

/// 通用确认取消弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/14/21 5:31 PM
/// @specialDemand:
class CommonConfirmCancelDialog extends StatelessWidget {

  final String title;
  final String content;

  final String cancelText;

  final String confirmText;

  final Color confirmBgColor;

  final Color cancelBgColor;

  final Color titleColor;

  final Color contentColor;

  final Color widgetBgColor;

  final VoidCallback onConfrim;

  final VoidCallback onPop;

  final bool isShowLeftButton;

  final Widget contentWidget;

  const CommonConfirmCancelDialog({Key key, this.title, this.content,
    this.cancelText, this.confirmText, this.confirmBgColor, this.cancelBgColor,
    this.titleColor, this.contentColor, this.widgetBgColor,
    this.onConfrim, this.onPop, this.isShowLeftButton = true, this.contentWidget}) : super(key: key);

  static Future<bool> navigatorPushDialog(BuildContext context,{String title,
    String content,String cancelText,String confirmText,Color confirmBgColor,
    Color cancelBgColor, Color titleColor, Color contentColor, Color widgetBgColor,
    Widget contentWidget, bool isShowLeftButton,
    VoidCallback onPop, VoidCallback onConfrim}){
    return VgDialogUtils.showCommonDialog<bool>(context: context, child: CommonConfirmCancelDialog(
      title: title,
      content: content,
      cancelText: cancelText,
      confirmText: confirmText,
      confirmBgColor: confirmBgColor,
      cancelBgColor: cancelBgColor,
      titleColor: titleColor,
      contentColor: contentColor,
      widgetBgColor: widgetBgColor,
      contentWidget: contentWidget,
      onPop: onPop,
      onConfrim: onConfrim,
      isShowLeftButton: isShowLeftButton,
    ),barrierDismissible: false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // behavior: HitTestBehavior.translucent,
      onTap: ()=> RouterUtils.pop(context),
      child: UnconstrainedBox(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){},
          child: Material(
            type: MaterialType.transparency,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                width: 290,
                decoration: BoxDecoration(
                  color: widgetBgColor??ThemeRepository.getInstance().getCardBgColor_21263C()
                ),
                child: _toMainColumnWidget(context),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 25,),
         Text(
                   title ?? "提示",
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                       color: titleColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                       fontSize: 16,
                     fontWeight: FontWeight.w600
                       ),
                 ),
        SizedBox(height: 13,),
         Padding(
           padding: const EdgeInsets.symmetric(horizontal: 30),
           child: contentWidget??Text(
                     content ?? "是否取消吗？",
                     overflow: TextOverflow.ellipsis,
                     maxLines: 5,
                     style: TextStyle(
                       color: contentColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                       fontSize: 14,
                         ),
                   ),
         ),
        SizedBox(height: 35,),
        _toTwoButtonWidget(context),
      ],
    );
  }

  Widget _toTwoButtonWidget(BuildContext context){
    return Container(
      height: 40,
      child: Row(
        children: <Widget>[
          if(isShowLeftButton ?? true)
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(onPop != null){
                  onPop?.call();
                  return ;
                }
                RouterUtils.pop(context,result: false);
              },
              child: Container(
                color: cancelBgColor??ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                child: Center(
                  child:  Text(
                    cancelText ?? "取消",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: VgColors.INPUT_BG_COLOR,
                                fontSize: 14,
                              height: 1.2
                                ),
                          ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(onConfrim != null){
                  onConfrim?.call();
                  return ;
                }
                RouterUtils.pop(context,result: true);
              },
              child: Container(
                color:confirmBgColor?? ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                child: Center(
                  child:  Text(
                   confirmText ?? "确定",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                        height: 1.2
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}


///编辑框改变回调
///editText: 编辑框内容
///value: 自定义值
typedef CommonTitleEditEditAndValueChangedCallback = void Function(String editText, dynamic value);

///跳转到另一个页面
///并携带之前参数值
typedef CommonTitleNavigatorPushCallback = Future<dynamic> Function(String editText, dynamic value);

///根据页面返回值自定义解析结果
typedef CommonTitlePopResultDecodeCallback = EditTextAndValue Function(dynamic result);


class EditTextAndValue{
  final String editText;

  final dynamic value;

  EditTextAndValue({this.editText, this.value});

  @override
  String toString() {
    return 'EditTextAndValue{editText: $editText, value: $value}';
  }
}
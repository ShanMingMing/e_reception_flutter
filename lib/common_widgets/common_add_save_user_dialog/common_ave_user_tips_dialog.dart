import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddSaveUserTipsFaceMatching extends StatelessWidget {
  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: AddSaveUserTipsFaceMatching());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            color: Color(0xFF303546),
            width: 290,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 126,right: 124,top: 25),
                      child: Container(
                        width: 40,
                        height: 20,
                        child: Text(
                          "提示",
                          style: TextStyle(fontSize: 16, color: Color(0xFFD0E0F7),),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 13,),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 30,right: 30),
                      child: Container(
                        width: 230,
                        child: Text(
                          "预存人脸照片后，需本人在显示屏前进行人脸识别匹配，匹配成功后即可激活该用户",
                          style: TextStyle(fontSize: 14, color: Color(0xFFD0E0F7),),

                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30,),
                Row(
                  children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 85,right: 85,bottom: 30),
                        child: CommonFixedHeightConfirmButtonWidget(
                          isAlive: true,
                          width: 120,
                          height: 40,
                          unSelectBgColor: ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
                          selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                          unSelectTextStyle: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 14,
                              fontWeight: FontWeight.w600
                          ),
                          selectedTextStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w600
                          ),
                          text: "我知道了",
                          onTap: (){
                            RouterUtils.pop(context);
                          },
                        ),
                      )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

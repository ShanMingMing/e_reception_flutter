import 'package:vg_base/vg_string_util_lib.dart';

class VideoUploadSuccessBean {
  String url;
  String fileSize;
  String coverUrl;
  String videoid;
  String sdVideo;
  String sdStorage;

  String getHdStorage(){
    if(StringUtils.isEmpty(sdStorage)){
      return null;
    }
    return (int.parse(sdStorage) / 1024).floor().toString();
  }

  VideoUploadSuccessBean(this.url, this.fileSize,this.coverUrl, this.videoid, this.sdVideo, this.sdStorage);
}
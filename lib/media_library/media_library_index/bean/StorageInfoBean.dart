/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"palynum":10,"spacesize":500}
/// extra : null

class StorageInfoBean {
  bool success;
  String code;
  String msg;
  StorageDataBean data;
  dynamic extra;

  static StorageInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StorageInfoBean storageInfoBeanBean = StorageInfoBean();
    storageInfoBeanBean.success = map['success'];
    storageInfoBeanBean.code = map['code'];
    storageInfoBeanBean.msg = map['msg'];
    storageInfoBeanBean.data = StorageDataBean.fromMap(map['data']);
    storageInfoBeanBean.extra = map['extra'];
    return storageInfoBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// palynum : 10
/// spacesize : 500

class StorageDataBean {
  int palynum;
  int spacesize;
  int hsnNum;

  static StorageDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StorageDataBean dataBean = StorageDataBean();
    dataBean.palynum = map['palynum'];
    dataBean.spacesize = map['spacesize'];
    dataBean.hsnNum = map['hsnNum'];
    return dataBean;
  }

  Map toJson() => {
    "palynum": palynum,
    "spacesize": spacesize,
    "hsnNum": hsnNum,
  };
}
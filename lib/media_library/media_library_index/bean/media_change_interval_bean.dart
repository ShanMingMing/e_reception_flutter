/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"oldPlayNum":124,"playNum":74,"nextPlayNum":10}
/// extra : null

class MediaChangeIntervalBean {
  bool success;
  String code;
  String msg;
  ChangeIntervalBean data;
  dynamic extra;

  static MediaChangeIntervalBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaChangeIntervalBean playNumBeanBean = MediaChangeIntervalBean();
    playNumBeanBean.success = map['success'];
    playNumBeanBean.code = map['code'];
    playNumBeanBean.msg = map['msg'];
    playNumBeanBean.data = ChangeIntervalBean.fromMap(map['data']);
    playNumBeanBean.extra = map['extra'];
    return playNumBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// oldPlayNum : 124
/// playNum : 74
/// nextPlayNum : 10

class ChangeIntervalBean {
  int cpsecond;
  String splicing;//智能海报拼接地址 00拼接 01不拼接

  static ChangeIntervalBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ChangeIntervalBean dataBean = ChangeIntervalBean();
    dataBean.cpsecond = map['cpsecond'];
    dataBean.splicing = map['splicing'];
    return dataBean;
  }

  Map toJson() => {
    "cpsecond": cpsecond,
    "splicing": splicing,
  };
}
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"comPictureMap":{"picurl":"http://etpic.we17.com/test/20210506200930_6246.mp4","createtime":1620302978,"bjtime":"2021-05-06 20:09:38","picsize":"1080*1920","picstorage":"8440","picname":"20210506200930_6246.mp4","createuserid":"1b14d48f34b143ed90b8fd20e0d47bea","videotime":3,"id":"28ecc101d295485ea4fd1de76af1c87d","type":"02","videopic":"http://etpic.we17.com/test/20210506200935_7020.jpg","createname":"杨先生"},"attList":[{"terminalName":"pad111","terminalPicurl":"http://etpic.we17.com/test/20210429171927_9227.jpg","hsn":"ed5d18608a872ce8","createtime":1620302978,"address":"总部基地1501","startday":"2021-05-06","endday":"2021-05-21"}]}
/// extra : null

class MediaDetailResponseBean {
  bool success;
  String code;
  String msg;
  MediaDetailBean data;
  dynamic extra;

  static MediaDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaDetailResponseBean mediaDetailResponseBeanBean = MediaDetailResponseBean();
    mediaDetailResponseBeanBean.success = map['success'];
    mediaDetailResponseBeanBean.code = map['code'];
    mediaDetailResponseBeanBean.msg = map['msg'];
    mediaDetailResponseBeanBean.data = MediaDetailBean.fromMap(map['data']);
    mediaDetailResponseBeanBean.extra = map['extra'];
    return mediaDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// comPictureMap : {"picurl":"http://etpic.we17.com/test/20210506200930_6246.mp4","createtime":1620302978,"bjtime":"2021-05-06 20:09:38","picsize":"1080*1920","picstorage":"8440","picname":"20210506200930_6246.mp4","createuserid":"1b14d48f34b143ed90b8fd20e0d47bea","videotime":3,"id":"28ecc101d295485ea4fd1de76af1c87d","type":"02","videopic":"http://etpic.we17.com/test/20210506200935_7020.jpg","createname":"杨先生"}
/// attList : [{"terminalName":"pad111","terminalPicurl":"http://etpic.we17.com/test/20210429171927_9227.jpg","hsn":"ed5d18608a872ce8","createtime":1620302978,"address":"总部基地1501","startday":"2021-05-06","endday":"2021-05-21"}]

class MediaDetailBean {
  ComPictureMapBean comPictureMap;
  List<AttListBean> usehsnList;
  List<AttListBean> historyhsnList;
  List<AttListBean> attList;

  static MediaDetailBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaDetailBean dataBean = MediaDetailBean();
    dataBean.comPictureMap = ComPictureMapBean.fromMap(map['comPictureMap']);
    if(map['attList'] != null){
      dataBean.attList = List()..addAll(
          (map['attList'] as List ?? []).map((o) => AttListBean.fromMap(o))
      );
    }
    dataBean.usehsnList = List()..addAll(
      (map['usehsnList'] as List ?? []).map((o) => AttListBean.fromMap(o))
    );
    dataBean.historyhsnList = List()..addAll(
        (map['historyhsnList'] as List ?? []).map((o) => AttListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "comPictureMap": comPictureMap,
    "attList": attList,
    "usehsnList": usehsnList,
    "historyhsnList": historyhsnList,
  };
}

/// terminalName : "pad111"
/// terminalPicurl : "http://etpic.we17.com/test/20210429171927_9227.jpg"
/// hsn : "ed5d18608a872ce8"
/// createtime : 1620302978
/// address : "总部基地1501"
/// startday : "2021-05-06"
/// endday : "2021-05-21"

class AttListBean {
  String terminalName;
  String terminalPicurl;
  String hsn;
  int createtime;
  String address;
  String startday;
  String endday;
  String position;
  String adminflg = "00";//01 管理  00不管理
  int playDay;
  String sideNumber;

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }

  static AttListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AttListBean attListBean = AttListBean();
    attListBean.terminalName = map['terminalName'];
    attListBean.sideNumber = map['sideNumber'];
    attListBean.terminalPicurl = map['terminalPicurl'];
    attListBean.hsn = map['hsn'];
    attListBean.createtime = map['createtime'];
    attListBean.address = map['address'];
    attListBean.startday = map['startday'];
    attListBean.endday = map['endday'];
    attListBean.position = map['position'];
    attListBean.adminflg = map['adminflg'];
    attListBean.playDay = map['playDay'];
    return attListBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "sideNumber": sideNumber,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "createtime": createtime,
    "address": address,
    "startday": startday,
    "endday": endday,
    "position": position,
    "adminflg": adminflg,
    "playDay": playDay,
  };

  bool canAdjust(){
    //超管或者在当前终端下是管理员
    return true;
    // return VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId()) || ("01" == adminflg);
  }

  String getPlayTimeString(){
    //异常数据
    if(StringUtils.isEmpty(startday) || StringUtils.isEmpty(endday)){
      return "暂无播放周期";
    }

    DateTime startDate = DateTime.parse(startday);
    DateTime endDate = DateTime.parse(endday);
    DateTime currentDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //待播放
    if(startDate.millisecondsSinceEpoch > currentDate.millisecondsSinceEpoch){
      int interval = (((startDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
      return interval.toString() + "天后播放";
    }
    //已结束
    if(endDate.millisecondsSinceEpoch < currentDate.millisecondsSinceEpoch){
      int interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
      return "已播放" + interval.toString() + "天";
    }
    //播放中
    //1.永久播放
    if("9999-99-99" == endday || "10007-06-0" == endday){
      return "永久播放";
    }
    //2.已播多少天
    int alreadyPlay = (((currentDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
    int resumePlay = (((endDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
    return "已播" + alreadyPlay.toString() + "天 剩余" + resumePlay.toString() + "天";

  }

  int getAlreadyPlayIntervalNum(){
    int interval = 0;
    if(StringUtils.isNotEmpty(startday)){
      DateTime startDate = DateTime.parse(startday);
      DateTime endDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
      if(endDate.millisecondsSinceEpoch > startDate.millisecondsSinceEpoch){
        interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
      }
    }
    return interval;
  }

  String getAlreadyPlayInterval(){
    String interval = "0天";
    if(StringUtils.isNotEmpty(startday)){
      DateTime startDate = DateTime.parse(startday);
      DateTime endDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
      if(endDate.millisecondsSinceEpoch > startDate.millisecondsSinceEpoch){
        interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1).toString() + "天";
      }
    }
    return interval;
  }

  String getResumePlayInterval(){
    String interval = "剩余0天";
    DateTime startDate;
    if(StringUtils.isNotEmpty(startday)){
      DateTime tempStartDate1 = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
      DateTime tempStartDate2 = DateTime.parse(startday);
      if(tempStartDate1.millisecondsSinceEpoch > tempStartDate2.millisecondsSinceEpoch){
        startDate = tempStartDate1;
      }else{
        startDate = tempStartDate2;
      }
    }

    if(StringUtils.isNotEmpty(endday)){
      if("9999-99-99" != endday && "10007-06-0" != endday){
        DateTime endDate = DateTime.parse(endday);
        if(startDate != null && endDate.millisecondsSinceEpoch > startDate.millisecondsSinceEpoch){
          interval = "剩余" + (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1).toString() + "天";
        }
      }else{
        interval = "永久播放";
      }
    }
    return interval;
  }
}

/// picurl : "http://etpic.we17.com/test/20210506200930_6246.mp4"
/// createtime : 1620302978
/// bjtime : "2021-05-06 20:09:38"
/// picsize : "1080*1920"
/// picstorage : "8440"
/// picname : "20210506200930_6246.mp4"
/// createuserid : "1b14d48f34b143ed90b8fd20e0d47bea"
/// videotime : 3
/// id : "28ecc101d295485ea4fd1de76af1c87d"
/// type : "02"
/// videopic : "http://etpic.we17.com/test/20210506200935_7020.jpg"
/// createname : "杨先生"

class ComPictureMapBean {
  String picurl;
  int createtime;
  String bjtime;
  String picsize;
  String picstorage;
  String picname;
  String createuserid;
  int videotime;
  String id;
  String type;
  String videopic;
  String videoid;
  String createname;
  String title;//字幕
  String subtitle;//字幕标题
  String waterflg;//00没有水印 01有水印
  String logoflg;//01无logo 00有logo

  static ComPictureMapBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComPictureMapBean comPictureMapBean = ComPictureMapBean();
    comPictureMapBean.picurl = map['picurl'];
    comPictureMapBean.createtime = map['createtime'];
    comPictureMapBean.bjtime = map['bjtime'];
    comPictureMapBean.picsize = map['picsize'];
    comPictureMapBean.picstorage = map['picstorage'];
    comPictureMapBean.picname = map['picname'];
    comPictureMapBean.createuserid = map['createuserid'];
    comPictureMapBean.videotime = map['videotime'];
    comPictureMapBean.id = map['id'];
    comPictureMapBean.type = map['type'];
    comPictureMapBean.videopic = map['videopic'];
    comPictureMapBean.videoid = map['videoid'];
    comPictureMapBean.createname = map['createname'];
    comPictureMapBean.title = map['title'];
    comPictureMapBean.subtitle = map['subtitle'];
    comPictureMapBean.waterflg = map['waterflg'];
    comPictureMapBean.logoflg = map['logoflg'];
    return comPictureMapBean;
  }

  Map toJson() => {
    "picurl": picurl,
    "createtime": createtime,
    "bjtime": bjtime,
    "picsize": picsize,
    "picstorage": picstorage,
    "picname": picname,
    "createuserid": createuserid,
    "videotime": videotime,
    "id": id,
    "type": type,
    "videopic": videopic,
    "videoid": videoid,
    "createname": createname,
    "title": title,
    "subtitle": subtitle,
    "waterflg": waterflg,
    "logoflg": logoflg,
  };

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  ///获取可删除的图片地址
  String getToDeletePicUrl(){
    if(isVideo()){
      return videopic;
    }
    return picurl;
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return picurl;
  }

  String getPicStorageStr(){
    if(StringUtils.isEmpty(picstorage)){
      picstorage = "0";
    }
    double size = double.parse(picstorage);
    if(size <= 1024){
      if(size < 10){
        return size.toStringAsPrecision(1) + "KB";
      }else if(size < 100){
        return size.toStringAsPrecision(2) + "KB";
      }else if(size < 1000){
        return size.toStringAsPrecision(3) + "KB";
      }else {
        return size.toStringAsPrecision(4) + "KB";
      }
    }else if(size > 1024 && size <= 1024*1024){
      return (size/1024).toStringAsPrecision(3) + "MB";
    }else{
      return (size/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }
}
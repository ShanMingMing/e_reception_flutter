/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"addressList":["个哈哈哈哈gggg"],"comCompany":{"companyid":"0bda0722a1f845c5bc7d35b5c50aed8f","comefrom":"00","status":"00","companyname":"","companynick":"注册一个企业","companylogo":"","type":"00","city":"{\"province\":\"江苏省\",\"city\":\"南京市\",\"district\":\"江宁区\",\"code\":\"320115\"}","gps":null,"address":null,"addrProvince":null,"addrCity":null,"addrDistrict":null,"addrCode":null,"enableFaceRecognition":"00","createuid":"c6be3a55370f4af0a399d26d8dbe0557","createdate":1619510794,"updatetime":1619510794,"updateuid":"c6be3a55370f4af0a399d26d8dbe0557","testflg":"00","deposit":0.0,"paymoney":0.0,"roundnum":3,"groupList":null,"bjtime":"2021-04-27T16:06:34"}}
/// extra : null

class PosterCreateInfoBean {
  bool success;
  String code;
  String msg;
  CompanyAndAddressBean data;
  dynamic extra;

  static PosterCreateInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PosterCreateInfoBean posterCreateInfoBeanBean = PosterCreateInfoBean();
    posterCreateInfoBeanBean.success = map['success'];
    posterCreateInfoBeanBean.code = map['code'];
    posterCreateInfoBeanBean.msg = map['msg'];
    posterCreateInfoBeanBean.data = CompanyAndAddressBean.fromMap(map['data']);
    posterCreateInfoBeanBean.extra = map['extra'];
    return posterCreateInfoBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// addressList : ["个哈哈哈哈gggg"]
/// comCompany : {"companyid":"0bda0722a1f845c5bc7d35b5c50aed8f","comefrom":"00","status":"00","companyname":"","companynick":"注册一个企业","companylogo":"","type":"00","city":"{\"province\":\"江苏省\",\"city\":\"南京市\",\"district\":\"江宁区\",\"code\":\"320115\"}","gps":null,"address":null,"addrProvince":null,"addrCity":null,"addrDistrict":null,"addrCode":null,"enableFaceRecognition":"00","createuid":"c6be3a55370f4af0a399d26d8dbe0557","createdate":1619510794,"updatetime":1619510794,"updateuid":"c6be3a55370f4af0a399d26d8dbe0557","testflg":"00","deposit":0.0,"paymoney":0.0,"roundnum":3,"groupList":null,"bjtime":"2021-04-27T16:06:34"}

class CompanyAndAddressBean {
  List<AddressItemBean> addressList;
  ComCompanyBean comCompany;
  String loginAddress;

  static CompanyAndAddressBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyAndAddressBean dataBean = CompanyAndAddressBean();
    dataBean.addressList = List()..addAll(
      (map['addressList'] as List ?? []).map((o) => AddressItemBean.fromMap(o))
    );
    dataBean.comCompany = ComCompanyBean.fromMap(map['comCompany']);
    dataBean.loginAddress = map['loginAddress'];
    return dataBean;
  }

  Map toJson() => {
    "addressList": addressList,
    "comCompany": comCompany,
    "loginAddress": loginAddress,
  };
}


/// companyid : "0bda0722a1f845c5bc7d35b5c50aed8f"
/// comefrom : "00"
/// status : "00"
/// companyname : ""
/// companynick : "注册一个企业"
/// companylogo : ""
/// type : "00"
/// city : "{\"province\":\"江苏省\",\"city\":\"南京市\",\"district\":\"江宁区\",\"code\":\"320115\"}"
/// gps : null
/// address : null
/// addrProvince : null
/// addrCity : null
/// addrDistrict : null
/// addrCode : null
/// enableFaceRecognition : "00"
/// createuid : "c6be3a55370f4af0a399d26d8dbe0557"
/// createdate : 1619510794
/// updatetime : 1619510794
/// updateuid : "c6be3a55370f4af0a399d26d8dbe0557"
/// testflg : "00"
/// deposit : 0.0
/// paymoney : 0.0
/// roundnum : 3
/// groupList : null
/// bjtime : "2021-04-27T16:06:34"

class ComCompanyBean {
  String companyid;
  String comefrom;
  String status;
  String companyname;
  String companynick;
  String companylogo;
  String type;
  String city;
  dynamic gps;
  dynamic address;
  dynamic addrProvince;
  dynamic addrCity;
  dynamic addrDistrict;
  dynamic addrCode;
  String enableFaceRecognition;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;
  String testflg;
  double deposit;
  double paymoney;
  int roundnum;
  dynamic groupList;
  String bjtime;
  String splicing;//智能海报拼接地址 00拼接 01不拼接

  static ComCompanyBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComCompanyBean comCompanyBean = ComCompanyBean();
    comCompanyBean.companyid = map['companyid'];
    comCompanyBean.comefrom = map['comefrom'];
    comCompanyBean.status = map['status'];
    comCompanyBean.companyname = map['companyname'];
    comCompanyBean.companynick = map['companynick'];
    comCompanyBean.companylogo = map['companylogo'];
    comCompanyBean.type = map['type'];
    comCompanyBean.city = map['city'];
    comCompanyBean.gps = map['gps'];
    comCompanyBean.address = map['address'];
    comCompanyBean.addrProvince = map['addrProvince'];
    comCompanyBean.addrCity = map['addrCity'];
    comCompanyBean.addrDistrict = map['addrDistrict'];
    comCompanyBean.addrCode = map['addrCode'];
    comCompanyBean.enableFaceRecognition = map['enableFaceRecognition'];
    comCompanyBean.createuid = map['createuid'];
    comCompanyBean.createdate = map['createdate'];
    comCompanyBean.updatetime = map['updatetime'];
    comCompanyBean.updateuid = map['updateuid'];
    comCompanyBean.testflg = map['testflg'];
    comCompanyBean.deposit = map['deposit'];
    comCompanyBean.paymoney = map['paymoney'];
    comCompanyBean.roundnum = map['roundnum'];
    comCompanyBean.groupList = map['groupList'];
    comCompanyBean.bjtime = map['bjtime'];
    comCompanyBean.splicing = map['splicing'];
    return comCompanyBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "comefrom": comefrom,
    "status": status,
    "companyname": companyname,
    "companynick": companynick,
    "companylogo": companylogo,
    "type": type,
    "city": city,
    "gps": gps,
    "address": address,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "addrCode": addrCode,
    "enableFaceRecognition": enableFaceRecognition,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "testflg": testflg,
    "deposit": deposit,
    "paymoney": paymoney,
    "roundnum": roundnum,
    "groupList": groupList,
    "bjtime": bjtime,
    "splicing": splicing,
  };
}


class AddressItemBean{
  String address;
  String cbid;
  String branchname;

  String comefrom;
  String gps;
  String addrProvince;
  String addrCity;
  String delflg;
  String gpsaddress;
  String companyidString;
  String addrCode;
  String bjtime;
  String updateuid;
  String juli;
  String addrDistrict;
  String createuid;
  
  
  static AddressItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AddressItemBean dataBean = AddressItemBean();
    dataBean.address = map['address'];
    dataBean.cbid = map['cbid'];
    dataBean.branchname = map['branchname'];
    dataBean.comefrom = map['comefrom'];
    dataBean.gps = map['gps'];
    dataBean.addrProvince = map['addrProvince'];
    dataBean.addrCity = map['addrCity'];
    dataBean.delflg = map['delflg'];
    dataBean.gpsaddress = map['gpsaddress'];
    dataBean.companyidString = map['companyidString'];
    dataBean.addrCode = map['addrCode'];
    dataBean.bjtime = map['bjtime'];
    dataBean.updateuid = map['updateuid'];
    dataBean.juli = map['juli'];
    dataBean.addrDistrict = map['addrDistrict'];
    dataBean.createuid = map['createuid'];
    return dataBean;
  }
  Map toJson() => {
    "address": address,
    "cbid": cbid,
    "branchname": branchname,
    "comefrom": comefrom,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "delflg": delflg,
    "gpsaddress": gpsaddress,
    "companyidString": companyidString,
    "addrCode": addrCode,
    "bjtime": bjtime,
    "updateuid": updateuid,
    "juli": juli,
    "addrDistrict": addrDistrict,
    "createuid": createuid,
  };
}
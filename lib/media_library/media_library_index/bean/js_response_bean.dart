/// type : "02"
/// position : "00"
/// content : ""
/// scalex : 880
/// scaley : 1026

class JsResponseBean {
  String type;
  String position;
  String content;
  String proportion;
  int scalex;
  int scaley;
  int index;

  static JsResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    JsResponseBean jsResponseBeanBean = JsResponseBean();
    jsResponseBeanBean.type = map['type'];
    jsResponseBeanBean.position = map['position'];
    jsResponseBeanBean.content = map['content'];
    jsResponseBeanBean.proportion = map['proportion'];
    var x = map['scalex'];
    var y = map['scaley'];
    var z = map['index'];
    if(x is int){
      jsResponseBeanBean.scalex = map['scalex'];
    }
    if(y is int){
      jsResponseBeanBean.scaley = map['scaley'];
    }
    if(z is int){
      jsResponseBeanBean.index = map['index'];
    }
    return jsResponseBeanBean;
  }

  Map toJson() => {
    "type": type,
    "position": position,
    "content": content,
    "scalex": scalex,
    "scaley": scaley,
    "index": index,
    "proportion": proportion,
  };
}
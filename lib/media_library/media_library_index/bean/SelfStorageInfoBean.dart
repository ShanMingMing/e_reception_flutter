/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"videoSpacesize":5971,"picSpacesize":917}
/// extra : null

class SelfStorageInfoBean {
  bool success;
  String code;
  String msg;
  SelfStorageDataBean data;
  dynamic extra;

  static SelfStorageInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SelfStorageInfoBean selfStorageInfoBeanBean = SelfStorageInfoBean();
    selfStorageInfoBeanBean.success = map['success'];
    selfStorageInfoBeanBean.code = map['code'];
    selfStorageInfoBeanBean.msg = map['msg'];
    selfStorageInfoBeanBean.data = SelfStorageDataBean.fromMap(map['data']);
    selfStorageInfoBeanBean.extra = map['extra'];
    return selfStorageInfoBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// videoSpacesize : 5971
/// picSpacesize : 917

class SelfStorageDataBean {
  int videoSpacesize;
  int picSpacesize;
  int hsnNum;

  static SelfStorageDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SelfStorageDataBean dataBean = SelfStorageDataBean();
    dataBean.videoSpacesize = map['videoSpacesize'];
    dataBean.picSpacesize = map['picSpacesize'];
    dataBean.hsnNum = map['hsnNum'];
    return dataBean;
  }

  String getStorageStr(){
    int totalSize = (picSpacesize??0) + (videoSpacesize??0);
    if(totalSize == 0){
      return "0MB";
    }
    if(totalSize <= 1024){
      return totalSize.toStringAsPrecision(3) + "KB";
    }else if(totalSize > 1024 && totalSize <= 1024*1024){
      return (totalSize/1024).toStringAsPrecision(3) + "MB";
    }else{
      return (totalSize/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }

  Map toJson() => {
    "videoSpacesize": videoSpacesize,
    "picSpacesize": picSpacesize,
    "hsnNum": hsnNum,
  };
}
/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"picurl":"http://etpic.we17.com/test/20210506200930_6246.mp4","createtime":1620302978,"picsize":"1080*1920","picstorage":"8440","picname":"20210506200930_6246.mp4","videotime":"3","id":"28ecc101d295485ea4fd1de76af1c87d","type":"02","picid":"28ecc101d295485ea4fd1de76af1c87d","videopic":"http://etpic.we17.com/test/20210506200935_7020.jpg"}]}
/// extra : null

class MediaUrgentResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static MediaUrgentResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaUrgentResponseBean mediaUrgentResponseBeanBean = MediaUrgentResponseBean();
    mediaUrgentResponseBeanBean.success = map['success'];
    mediaUrgentResponseBeanBean.code = map['code'];
    mediaUrgentResponseBeanBean.msg = map['msg'];
    mediaUrgentResponseBeanBean.data = DataBean.fromMap(map['data']);
    mediaUrgentResponseBeanBean.extra = map['extra'];
    return mediaUrgentResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"picurl":"http://etpic.we17.com/test/20210506200930_6246.mp4","createtime":1620302978,"picsize":"1080*1920","picstorage":"8440","picname":"20210506200930_6246.mp4","videotime":"3","id":"28ecc101d295485ea4fd1de76af1c87d","type":"02","picid":"28ecc101d295485ea4fd1de76af1c87d","videopic":"http://etpic.we17.com/test/20210506200935_7020.jpg"}]

class DataBean {
  List<ListBean> list;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => ListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

/// picurl : "http://etpic.we17.com/test/20210506200930_6246.mp4"
/// createtime : 1620302978
/// picsize : "1080*1920"
/// picstorage : "8440"
/// picname : "20210506200930_6246.mp4"
/// videotime : "3"
/// id : "28ecc101d295485ea4fd1de76af1c87d"
/// type : "02"
/// picid : "28ecc101d295485ea4fd1de76af1c87d"
/// videopic : "http://etpic.we17.com/test/20210506200935_7020.jpg"

class ListBean {
  String picurl;
  int createtime;
  String picsize;
  String picstorage;
  String picname;
  String videotime;
  String id;
  String type;
  String picid;
  String videopic;

  static ListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ListBean listBean = ListBean();
    listBean.picurl = map['picurl'];
    listBean.createtime = map['createtime'];
    listBean.picsize = map['picsize'];
    listBean.picstorage = map['picstorage'];
    listBean.picname = map['picname'];
    listBean.videotime = map['videotime'];
    listBean.id = map['id'];
    listBean.type = map['type'];
    listBean.picid = map['picid'];
    listBean.videopic = map['videopic'];
    return listBean;
  }

  Map toJson() => {
    "picurl": picurl,
    "createtime": createtime,
    "picsize": picsize,
    "picstorage": picstorage,
    "picname": picname,
    "videotime": videotime,
    "id": id,
    "type": type,
    "picid": picid,
    "videopic": videopic,
  };
}
/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"comPicDiy":{"id":"2777cb065558429abff8c0d60bc5d726","companyid":"0126e97aed8146c785dc9be2d6a4c41c","comPicid":"903e24288e5b4b87ae9fd313ddd4881a","sysPicid":"5cc8b2034c0344598fdbd3579884fac1","diytext":"[{\"type\":\"02\",\"position\":\"00\",\"content\":\"http://etpic.we17.com/test/20210728193626_9882.jpg\",\"scalex\":null,\"scaley\":null},{\"type\":\"01\",\"position\":\"01\",\"content\":\"海威你好该喝喝\",\"scalex\":null,\"scaley\":null}]","updatetime":"2021-07-28 19:39:23","updateuid":"4a3d21386acb4f3d923227515c130470"}}
/// extra : null

class PosterDiyInfoBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static PosterDiyInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PosterDiyInfoBean posterDiyInfoBeanBean = PosterDiyInfoBean();
    posterDiyInfoBeanBean.success = map['success'];
    posterDiyInfoBeanBean.code = map['code'];
    posterDiyInfoBeanBean.msg = map['msg'];
    posterDiyInfoBeanBean.data = DataBean.fromMap(map['data']);
    posterDiyInfoBeanBean.extra = map['extra'];
    return posterDiyInfoBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// comPicDiy : {"id":"2777cb065558429abff8c0d60bc5d726","companyid":"0126e97aed8146c785dc9be2d6a4c41c","comPicid":"903e24288e5b4b87ae9fd313ddd4881a","sysPicid":"5cc8b2034c0344598fdbd3579884fac1","diytext":"[{\"type\":\"02\",\"position\":\"00\",\"content\":\"http://etpic.we17.com/test/20210728193626_9882.jpg\",\"scalex\":null,\"scaley\":null},{\"type\":\"01\",\"position\":\"01\",\"content\":\"海威你好该喝喝\",\"scalex\":null,\"scaley\":null}]","updatetime":"2021-07-28 19:39:23","updateuid":"4a3d21386acb4f3d923227515c130470"}

class DataBean {
  ComPicDiyBean comPicDiy;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.comPicDiy = ComPicDiyBean.fromMap(map['comPicDiy']);
    return dataBean;
  }

  Map toJson() => {
    "comPicDiy": comPicDiy,
  };
}

/// id : "2777cb065558429abff8c0d60bc5d726"
/// companyid : "0126e97aed8146c785dc9be2d6a4c41c"
/// comPicid : "903e24288e5b4b87ae9fd313ddd4881a"
/// sysPicid : "5cc8b2034c0344598fdbd3579884fac1"
/// diytext : "[{\"type\":\"02\",\"position\":\"00\",\"content\":\"http://etpic.we17.com/test/20210728193626_9882.jpg\",\"scalex\":null,\"scaley\":null},{\"type\":\"01\",\"position\":\"01\",\"content\":\"海威你好该喝喝\",\"scalex\":null,\"scaley\":null}]"
/// updatetime : "2021-07-28 19:39:23"
/// updateuid : "4a3d21386acb4f3d923227515c130470"

class ComPicDiyBean {
  String id;
  String companyid;
  String comPicid;
  String sysPicid;
  String diytext;
  String updatetime;
  String updateuid;

  static ComPicDiyBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComPicDiyBean comPicDiyBean = ComPicDiyBean();
    comPicDiyBean.id = map['id'];
    comPicDiyBean.companyid = map['companyid'];
    comPicDiyBean.comPicid = map['comPicid'];
    comPicDiyBean.sysPicid = map['sysPicid'];
    comPicDiyBean.diytext = map['diytext'];
    comPicDiyBean.updatetime = map['updatetime'];
    comPicDiyBean.updateuid = map['updateuid'];
    return comPicDiyBean;
  }

  Map toJson() => {
    "id": id,
    "companyid": companyid,
    "comPicid": comPicid,
    "sysPicid": sysPicid,
    "diytext": diytext,
    "updatetime": updatetime,
    "updateuid": updateuid,
  };
}
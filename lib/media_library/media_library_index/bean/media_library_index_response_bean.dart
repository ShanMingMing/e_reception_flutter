import 'package:e_reception_flutter/media_library/media_library_index/pages/media_library_play_page.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"createtime":1612577651,"picsize":"122*500","playnum":0,"picstorage":"100","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","bjtime":"2021-02-06 10:14:11","picname":"20210120160400_9940.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"6aa1e70c09cd4501ac95e6761fc072b5"}],"total":1,"size":10,"current":1,"orders":[],"searchCount":true,"pages":1}}

class MediaLibraryIndexResponseBean extends BasePagerBean<MediaLibraryIndexListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;

  static MediaLibraryIndexResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaLibraryIndexResponseBean mediaLibraryIndexResponseBeanBean = MediaLibraryIndexResponseBean();
    mediaLibraryIndexResponseBeanBean.success = map['success'];
    mediaLibraryIndexResponseBeanBean.code = map['code'];
    mediaLibraryIndexResponseBeanBean.msg = map['msg'];
    mediaLibraryIndexResponseBeanBean.data = DataBean.fromMap(map['data']);
    return mediaLibraryIndexResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<MediaLibraryIndexListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"createtime":1612577651,"picsize":"122*500","playnum":0,"picstorage":"100","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","bjtime":"2021-02-06 10:14:11","picname":"20210120160400_9940.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"6aa1e70c09cd4501ac95e6761fc072b5"}],"total":1,"size":10,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"createtime":1612577651,"picsize":"122*500","playnum":0,"picstorage":"100","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","bjtime":"2021-02-06 10:14:11","picname":"20210120160400_9940.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"6aa1e70c09cd4501ac95e6761fc072b5"}]
/// total : 1
/// size : 10
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<MediaLibraryIndexListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => MediaLibraryIndexListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

class CycleBean{
  String startday;
  String endday;
  static CycleBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CycleBean cycleBean = CycleBean();
    cycleBean.startday = map['startday'];
    cycleBean.endday = map['endday'];
    return cycleBean;
  }
  Map toJson() => {
    "startday": startday,
    "endday": endday,
  };

}

/// createtime : 1612577651
/// picsize : "122*500"
/// playnum : 0
/// picstorage : "100"
/// videotime : 0
/// playtime : 0
/// type : "01"
/// videopic : " "
/// createname : "王凡语"
/// picurl : "http://etpic.we17.com/test/20210120160400_9940.jpg"
/// bjtime : "2021-02-06 10:14:11"
/// picname : "20210120160400_9940.jpg"
/// createuserid : "be97b4557acc49a782c2e52b53bb77a1"
/// id : "6aa1e70c09cd4501ac95e6761fc072b5"

class MediaLibraryIndexListItemBean {
  int createtime;
  String picsize;
  String splpicsize;
  int playnum;
  String picstorage;
  int videotime;
  int playtime;
  String type;
  String videopic;
  String videoid;
  String createname;
  String picurl;
  String htmlurl;
  String bjtime;
  String picname;
  String createuserid;
  String id;
  String picid;
  int attnum;
  String startday;
  String endday;
  String uploadflg;//00自传 01 推送
  int reday;//有效天数
  int suday;//剩余播放天数 已播天数=suday -1等于0的时候  已播天数=suday  就不用-1了   推送的不显示剩余天数
  int playDay;//已播天数
  String likeflg;//01喜欢 02不喜欢 03不喜欢删除
  String cretype;//00:什么都没有 01:logo 02:名称 03:地址 04:logo+名称 05:logo+地址 06:名称+地址 07:全部都有
  bool selectStatus;
  String logoflg;//00显示 01不显示
  String addressflg;//00显示 01不显示
  String diyflg;//00不可diy 01diy
  String calflg;// 00没有日历 01有日历
  String calx;//日历坐标
  String caly;//日历坐标
  String addressdiy;
  String namediy;
  String logodiy;
  String splpicurl;
  List<CycleBean> cycleList;
  String pictype;//09合成海报
  String pictureurl;

  String getPicSize(){
    if(isHtml()){
      return splpicsize;
    }
    return picsize;
  }

  static MediaLibraryIndexListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MediaLibraryIndexListItemBean recordsBean = MediaLibraryIndexListItemBean();
    recordsBean.createtime = map['createtime'];
    recordsBean.picsize = map['picsize'];
    recordsBean.splpicsize = map['splpicsize'];
    recordsBean.playnum = map['playnum'];
    recordsBean.picstorage = map['picstorage'];
    recordsBean.videotime = map['videotime'];
    recordsBean.playtime = map['playtime'];
    recordsBean.type = map['type'];
    recordsBean.videopic = map['videopic'];
    recordsBean.videoid = map['videoid'];
    recordsBean.createname = map['createname'];
    recordsBean.picurl = map['picurl'];
    recordsBean.htmlurl = map['htmlurl'];
    recordsBean.bjtime = map['bjtime'];
    recordsBean.picname = map['picname'];
    recordsBean.createuserid = map['createuserid'];
    recordsBean.id = map['id'];
    recordsBean.picid = map['picid'];
    recordsBean.startday = map['startday'];
    recordsBean.endday = map['endday'];
    recordsBean.uploadflg = map['uploadflg'];
    recordsBean.reday = map['reday'];
    recordsBean.suday = map['suday'];
    recordsBean.likeflg = map['likeflg'];
    recordsBean.cretype = map['cretype'];
    recordsBean.playDay = map['playDay'];
    recordsBean.logoflg = map['logoflg'];
    recordsBean.addressflg = map['addressflg'];
    recordsBean.addressdiy = map['addressdiy'];
    recordsBean.namediy = map['namediy'];
    recordsBean.logodiy = map['logodiy'];
    recordsBean.splpicurl = map['splpicurl'];
    recordsBean.diyflg = map['diyflg'];
    recordsBean.calflg = map['calflg'];
    recordsBean.calx = map['calx'];
    recordsBean.caly = map['caly'];
    recordsBean.pictype = map['pictype'];
    recordsBean.pictureurl = map['pictureurl'];
    dynamic num=map['attnum'];
    if(num!=null){
      if(num is int){
        recordsBean.attnum = num;
      }
    }
    recordsBean.cycleList = List()..addAll(
        (map['cycleList'] as List ?? []).map((o) => CycleBean.fromMap(o))
    );
    return recordsBean;
  }

  Map toJson() => {
    "createtime": createtime,
    "picsize": picsize,
    "splpicsize": splpicsize,
    "playnum": playnum,
    "picstorage": picstorage,
    "videotime": videotime,
    "playtime": playtime,
    "type": type,
    "videopic": videopic,
    "videoid": videoid,
    "createname": createname,
    "picurl": picurl,
    "htmlurl": htmlurl,
    "bjtime": bjtime,
    "picname": picname,
    "createuserid": createuserid,
    "id": id,
    "picid": picid,
    "attnum":attnum,
    "startday": startday,
    "endday": endday,
    "uploadflg": uploadflg,
    "reday": reday,
    "suday": suday,
    "likeflg": likeflg,
    "cretype": cretype,
    "playDay": playDay,
    "logoflg": logoflg,
    "addressflg": addressflg,
    "diyflg": diyflg,
    "calflg": calflg,
    "calx": calx,
    "caly": caly ,
    "addressdiy": addressdiy ,
    "namediy": namediy ,
    "logodiy": logodiy ,
    "splpicurl": splpicurl ,
    "cycleList": cycleList ,
    "pictype": pictype ,
    "pictureurl": pictureurl ,
  };

  //已播天数
  String getPlayedTimeString(){
    if(cycleList == null || cycleList.length == 0){
      return "暂无播放周期";
    }
    int day = 0;
    cycleList.forEach((element) {
      DateTime startDate = DateTime.parse(element.startday);
      DateTime endDate = DateTime.parse(element.endday);
      int interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
      if(day < interval){
        day = interval;
      }
    });
    return "已播${day}天";
  }

  //待播天数
  String getWaitPlayTimeString(){
    //异常数据
    if(StringUtils.isEmpty(startday) || StringUtils.isEmpty(endday)){
      return "暂无播放周期";
    }
    DateTime startDate = DateTime.parse(startday);
    DateTime currentDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //待播放
    if(startDate.millisecondsSinceEpoch > currentDate.millisecondsSinceEpoch){
      int interval = (((startDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
      return interval.toString() + "天后播放";
    }
    return "暂无播放周期";
  }

  String getPlayTimeString({String pageType}){
    if(StringUtils.isNotEmpty(pageType) && pageType == MediaLibraryPlayPage.TYPE_PLAYED){
      if(pageType == MediaLibraryPlayPage.TYPE_PLAYED){
        return getPlayedTimeString();
      }else if(pageType == MediaLibraryPlayPage.TYPE_WAIT_PLAY){
        return getWaitPlayTimeString();
      }
    }
    //系统推送文件
    if("01" == uploadflg){
      if(playDay == null){
        return "暂无播放周期";
      }
      return "已播${playDay??0}天";
      // return "已播${reday-suday+1}天";
    }
    //异常数据
    if(StringUtils.isEmpty(startday) || StringUtils.isEmpty(endday)){
      return "暂无播放周期";
    }

    DateTime startDate = DateTime.parse(startday);
    DateTime endDate = DateTime.parse(endday);
    DateTime currentDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //待播放
    if(startDate.millisecondsSinceEpoch > currentDate.millisecondsSinceEpoch){
      int interval = (((startDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
      return interval.toString() + "天后播放";
    }
    //已结束
    if(endDate.millisecondsSinceEpoch < currentDate.millisecondsSinceEpoch){
      int interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
      return "已播放" + interval.toString() + "天";
    }
    //播放中
    //1.永久播放
    if("9999-99-99" == endday || "10007-06-0" == endday){
        return "永久播放";
    }
    //2.已播多少天
    int alreadyPlay = (((currentDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
    int resumePlay = (((endDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
    return "已播" + alreadyPlay.toString() + "天 剩余" + resumePlay.toString() + "天";

  }

  String getAlreadyPlayInterval(){
    String interval = "0天";
    if(StringUtils.isNotEmpty(startday)){
      DateTime startDate = DateTime.parse(startday);
      DateTime endDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
      if(endDate.millisecondsSinceEpoch > startDate.millisecondsSinceEpoch){
        interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1).toString() + "天";
      }
    }
    return interval;
  }

  String getResumePlayInterval(){
    String interval = "剩余0天";
    DateTime startDate;
    if(StringUtils.isNotEmpty(startday)){
      DateTime tempStartDate1 = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
      DateTime tempStartDate2 = DateTime.parse(startday);
      if(tempStartDate1.millisecondsSinceEpoch > tempStartDate2.millisecondsSinceEpoch){
        startDate = tempStartDate1;
      }else{
        startDate = tempStartDate2;
      }
    }

    if(StringUtils.isNotEmpty(endday)){
      if("9999-99-99" != endday && "10007-06-0" != endday){
        DateTime endDate = DateTime.parse(endday);
        if(startDate != null && endDate.millisecondsSinceEpoch > startDate.millisecondsSinceEpoch){
          interval = "剩余" + (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1).toString() + "天";
        }
      }else{
        interval = "永久播放";
      }
    }
    return interval;
  }


  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  bool isHtml(){
    if(StringUtils.isEmpty(type) && StringUtils.isNotEmpty(htmlurl)){
      return true;
    }
    if("01" == type && StringUtils.isNotEmpty(htmlurl)){
      return true;
    }
    return false;
  }

  ///是否有终端使用
  bool isUserFolder(){
    // return playnum != null && playnum > 0;
    return false;
  }

  ///获取可删除的图片地址
  String getToDeletePicUrl(){
    if(isVideo()){
      return videopic;
    }
    if("01" == (uploadflg?? "")){
      //是推送海报，不删除图片地址
      return "";
    }
    return getPicUrl();
  }
  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return getPicUrl();
  }

  String getPicUrl(){
    //之前是合成海报，并且自传文件地址不为空，就展示自传地址
    //现在改为只要自传地址不为空，就展示
    // if("09" == pictype && StringUtils.isNotEmpty(splpicurl)){
    if(StringUtils.isNotEmpty(splpicurl)){
      return splpicurl;
    }
    if(StringUtils.isNotEmpty(picurl) && !picurl.endsWith(".html")){
      return picurl;
    }
    return pictureurl;
  }

}
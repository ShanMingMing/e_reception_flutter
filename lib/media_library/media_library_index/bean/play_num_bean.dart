/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"oldPlayNum":124,"playNum":74,"nextPlayNum":10}
/// extra : null

class PlayNumBean {
  bool success;
  String code;
  String msg;
  PicNumBean data;
  dynamic extra;

  static PlayNumBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PlayNumBean playNumBeanBean = PlayNumBean();
    playNumBeanBean.success = map['success'];
    playNumBeanBean.code = map['code'];
    playNumBeanBean.msg = map['msg'];
    playNumBeanBean.data = PicNumBean.fromMap(map['data']);
    playNumBeanBean.extra = map['extra'];
    return playNumBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// oldPlayNum : 124
/// playNum : 74
/// nextPlayNum : 10

class PicNumBean {
  int oldPlayNum;
  int playNum;
  int nextPlayNum;

  static PicNumBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PicNumBean dataBean = PicNumBean();
    dataBean.oldPlayNum = map['oldPlayNum'];
    dataBean.playNum = map['playNum'];
    dataBean.nextPlayNum = map['nextPlayNum'];
    return dataBean;
  }

  int getValue(String type){
    if("已播" == type){
      return oldPlayNum;
    }else if("在播" == type){
      return playNum;
    }else if("排队" == type){
      return nextPlayNum;
    }
    return 0;
  }

  Map toJson() => {
    "oldPlayNum": oldPlayNum,
    "playNum": playNum,
    "nextPlayNum": nextPlayNum,
  };
}
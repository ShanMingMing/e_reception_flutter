import 'dart:convert';

import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/StorageInfoBean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_detail_response_bean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_library_index_response_bean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_change_interval_event.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_urgent_update.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/index/bean/update_terminal_play_list_status_bean.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_oss_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/delegate/custom_video_cache_manager.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'bean/SelfStorageInfoBean.dart';
import 'bean/media_change_interval_bean.dart';
import 'bean/media_detail_response_bean.dart';
import 'bean/play_num_bean.dart';
import 'bean/poster_create_info_bean.dart';
import 'bean/poster_diy_info_bean.dart';
import 'even/media_library_index_refresh_even.dart';
import 'even/update_terminal_play_list_status_event.dart';

class MediaLibraryIndexViewModel extends BasePagerViewModel<
    MediaLibraryIndexListItemBean,
    MediaLibraryIndexResponseBean> {

  static const String DELETE_FOLDER_API =ServerApi.BASE_URL + "app/appDelComPic";

  static const String DELETE_PIC_BY_HSN_API =ServerApi.BASE_URL + "app/appDelComAttPic";

  static const String UPDATE_NAME_FOLDER_API =ServerApi.BASE_URL + "app/appUpdatePicname";

  static const String GET_MEDIA_DETAIL_API =ServerApi.BASE_URL + "app/appGetComPicurlBypicid";

  static const String UPDATE_MEDIA_PLAY_INTERVAL_API =ServerApi.BASE_URL + "app/appUPdatePicDayByhsn";
  ///设置视频图片为紧急通告模式
  static const String SET_PIC_URGENT_API =ServerApi.BASE_URL + "app/appUrgentBypic";
  ///取消设置视频图片为紧急通告模式
  static const String CANCEL_PIC_URGENT_API =ServerApi.BASE_URL + "app/appCancelUrgent";
  ///app查询海报展示所需企业信息和所有人地址信息
  static const String COMPANY_INFO_AND_ADDRESS_API =ServerApi.BASE_URL + "app/appGetCompanyAndAddress";
  ///企业无终端时
  static const String DEFAULT_MEDIA_LIST_API =ServerApi.BASE_URL + "app/appGetCurrencyPic";
  ///添加or修改字幕
  static const String ADD_TEXT_API =ServerApi.BASE_URL + "app/appAddSubtitleByPic";
  ///删除字幕
  static const String DELETE_TEXT_API =ServerApi.BASE_URL + "app/appDelSubtitleByPic";
  ///点击喜欢/不喜欢海报
  static const String LIKE_UNLIKE_POSTER_API =ServerApi.BASE_URL + "app/appPicLikeflg";
  ///不喜欢/删除合成海报
  static const String DELETE_COMPOSE_POSTER_API =ServerApi.BASE_URL + "app/appDelComAttPicCompose";
  ///下载、分享海报
  static const String DOWNLOAD_SHARE_POSTER_API =ServerApi.BASE_URL + "app/appPicThird";
  ///浏览海报
  static const String POSTER_LOOK_API = ServerApi.BASE_URL + "app/appPicLook";
  ///App给视频图片设置水印
  static const String SET_WATER_FLAG_API =ServerApi.BASE_URL + "app/appWaterflgByPic";
  ///批量删除图片
  static const String DELETE_PICS_API =ServerApi.BASE_URL + "app/appPLDelPic";
  ///设置海报是否显示logo
  static const String UPDATE_LOGO_API =ServerApi.BASE_URL + "app/appUpdateLogoflg";
  ///设置海报是否显示地址
  static const String UPDATE_ADDRESS_API =ServerApi.BASE_URL + "app/appUpdateAddressflg";
  ///编辑海报
  static const String DIY_POSTER_API =ServerApi.BASE_URL + "app/appDIYPic";
  ///查询海报diy信息
  static const String POSTER_DIY_INFO_API =ServerApi.BASE_URL + "app/appGetDIYPicBycompicid";
  ///查询云存储空间信息
  static const String GET_STORAGE_API =ServerApi.BASE_URL + "app/appGetSysParam";
  ///查询企业自传文件大小
  static const String GET_SELF_STORAGE_INFO_API =ServerApi.BASE_URL + "app/appGetSpacesize";
  ///App查询播放资源库各类型数目
  static const String GET_PLAY_NUM_API =ServerApi.BASE_URL + "app/appGetPicNum";
  ///App查询资料库播放切换时间
  static const String GET_MEDIA_CHANGE_INTERVAL =ServerApi.BASE_URL + "app/appGetCompanyInfo";
  ///时间切换间隔
  static const String MEDIA_CHANGE_INTERVAL = "media_change_interval";

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<MediaDetailBean> mediaDetailValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  ValueNotifier<CompanyAndAddressBean> companyInfoNotifier;
  ValueNotifier<ComPicDiyBean> diyInfoNotifier;
  ValueNotifier<StorageDataBean> storageInfoNotifier;
  ValueNotifier<SelfStorageDataBean> selfStorageInfoNotifier;

  Function _picFunction;


  static bool EMPTY_FLAG;

  MediaLibraryIndexViewModel(BaseState<StatefulWidget> state, {Function picFunction})
      : super(state){
    totalValueNotifier = ValueNotifier(null);
    mediaDetailValueNotifier = ValueNotifier(null);
    companyInfoNotifier = ValueNotifier(null);
    diyInfoNotifier = ValueNotifier(null);
    storageInfoNotifier = ValueNotifier(null);
    selfStorageInfoNotifier = ValueNotifier(null);
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    _picFunction = picFunction;
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    mediaDetailValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    companyInfoNotifier?.dispose();
    diyInfoNotifier?.dispose();
    storageInfoNotifier?.dispose();
    selfStorageInfoNotifier?.dispose();
    super.onDisposed();

  }

  @override
  bool isNeedCache() {
    return true;
  }

  void getCache(){
    String cacheKey = getUrl() + (UserRepository.getInstance().authId??"");
    ///获取缓存数据
    Future<String> future = SharePreferenceUtil.getString(cacheKey);
    future.then((jsonStr){
      if(!StringUtils.isEmpty(jsonStr)){
        Map map = json.decode(jsonStr);
        MediaLibraryIndexResponseBean vo = MediaLibraryIndexResponseBean.fromMap(map);
        FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
        loading(false);
        if((vo?.data?.page?.total??0) > 0){
          statusTypeValueNotifier?.value = null;
        }else{
          statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
        }
        totalValueNotifier.value = vo?.data?.page?.total;
      }
    });
  }

  ///删除资源
  void deletePicByHsns(BuildContext context, String picid, String hsns){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    VgHudUtils.show(context,"删除中");
    VgHttpUtils.get(DELETE_PIC_BY_HSN_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn":hsns,
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, "删除成功");
          RouterUtils.pop(context);
          refreshMultiPage();
          //通知首页更新
          VgEventBus.global.send(MediaLibraryRefreshEven());
          getUpdateTerminalPlayListStatus(hsns);
        },
        onError: (msg){
          VgHudUtils.hide(context);

          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///查询终端获取资源是否成功
  void getUpdateTerminalPlayListStatus(String hsn){
    String authId = UserRepository.getInstance().authId;
    if(StringUtils.isEmpty(authId)){
      return;
    }
    ///网络请求
    VgHttpUtils.get(NetApi.UPDATE_TERMINAL_PLAY_LIST_STATUS_API,params: {
      "authId":authId ?? "",
      "hsn":hsn ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          UpdateTerminalPlayListStatusBean bean = UpdateTerminalPlayListStatusBean.fromMap(val);
          if(bean != null){
            VgEventBus.global.send(new UpdateTerminalPlayListStatusEvent(bean?.data?.obtainflg??"", hsn));
          }
        },
        onError: (msg){
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///删除视频文件
  void deleteVodVideoFile(String videoId){
    if(StringUtils.isEmpty(videoId)){
      return;
    }
    VgHttpUtils.get(NetApi.DELETE_VOD_VIDEO_FILE,params: {
      "videoid":videoId,
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));

  }

  ///永久删除资源
  void deleteFolder(BuildContext context,String picid, String videoId, String picurl, String videoUrl, {VoidCallback callback}){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    VgHttpUtils.get(DELETE_FOLDER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          //删除服务端图片文件
          VgOssUtils.deleteObject(picurl);
          //删除服务端视频文件
          if(StringUtils.isNotEmpty(videoId)){
            deleteVodVideoFile(videoId);
          }
          VgToastUtils.toast(context, "删除文件成功");
          //删除本地视频缓存
          if(StringUtils.isNotEmpty(videoUrl)){
            CustomVideoCacheManager().removeFile(videoUrl);
          }
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          refreshMultiPage();
          getStorageInfo(context);
          getSelfStorageInfo(context);
          getPicNum(context);
          if(callback != null){
            callback.call();
          }
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///永久删除推送
  void deleteMediaPushPoster(BuildContext context, String id, {VoidCallback callback}){
    if(StringUtils.isEmpty(id)){
      return;
    }
    VgHudUtils.show(context,"删除中");
    String companyid = UserRepository.getInstance().getCompanyId();
    VgHttpUtils.get(NetApi.DELETE_MEDIA_LIBRARY_PUSH_POSTER,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
      "companyid": companyid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgToastUtils.toast(context, "删除文件成功");
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          refreshMultiPage();
          getStorageInfo(context);
          getSelfStorageInfo(context);
          getPicNum(context);
          if(callback != null){
            callback.call();
          }
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///永久删除资源
  void deleteFolderAndPop(BuildContext context,String picid, String videoId, String picurl, String videoUrl){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    VgHttpUtils.get(DELETE_FOLDER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          //删除服务端图片文件
          VgOssUtils.deleteObject(picurl);
          //删除服务端视频文件
          if(StringUtils.isNotEmpty(videoId)){
            deleteVodVideoFile(videoId);
          }
          VgToastUtils.toast(context, "删除文件成功");
          RouterUtils.pop(context);
          //删除本地视频缓存
          if(StringUtils.isNotEmpty(videoUrl)){
            CustomVideoCacheManager().removeFile(videoUrl);
          }
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          refreshMultiPage();
          getStorageInfo(context);
          getSelfStorageInfo(context);
          getPicNum(context);
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///更新文件名
  void updateFolderName(BuildContext context,String picid,String name,VoidCallback onPop, {VoidCallback callback}){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    if(StringUtils.isEmpty(name)){
      VgToastUtils.toast(context, "文件名不能为空");
      return;
    }
    VgHudUtils.show(context,"重命名中");
    VgHttpUtils.get(UPDATE_NAME_FOLDER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
      "picname": name ?? ""
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          onPop?.call();
          VgToastUtils.toast(context, "重命名成功");
          refreshMultiPage();
          if(callback != null){
            callback.call();
          }
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///获取媒体文件详情
  void getMediaDetail(String picid){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    String cacheKey = GET_MEDIA_DETAIL_API + (UserRepository.getInstance().authId?? "")
        + (UserRepository.getInstance().companyId??"") + picid;
    ///获取缓存数据
    Future<String> future = SharePreferenceUtil.getString(cacheKey);
    future.then((jsonStr){
      if(!StringUtils.isEmpty(jsonStr)){
        print("获取缓存文件详情：" + cacheKey + "   " + jsonStr);
        Map map = json.decode(jsonStr);
        MediaDetailResponseBean bean = MediaDetailResponseBean.fromMap(map);
        if(bean != null){
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            mediaDetailValueNotifier?.value = bean?.data;
          }
          loading(false);
        }
        getMediaDetailOnLine(picid, cacheKey, bean);
      }else{
        getMediaDetailOnLine(picid, cacheKey, null);
      }
    });

  }

  void getMediaDetailOnLine(String picid, String cacheKey, MediaDetailResponseBean bean){
    VgHttpUtils.get(GET_MEDIA_DETAIL_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          MediaDetailResponseBean bean =
          MediaDetailResponseBean.fromMap(val);
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            //过滤playday为0的情况
            if(bean != null && bean.data != null
                && bean.data.historyhsnList != null && bean.data.historyhsnList.isNotEmpty){
              List<AttListBean> historyhsnList = new List();
              bean.data.historyhsnList.forEach((element) {
                if(element.playDay > 0){
                  historyhsnList.add(element);
                }
              });
              bean.data.historyhsnList = historyhsnList;
            }
            mediaDetailValueNotifier?.value = bean?.data;
          }
          loading(false);
          if(bean!=null){
            SharePreferenceUtil.putString(cacheKey, json.encode(bean));
            print("缓存文件详情：" + cacheKey + "   " + json.encode(bean));
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
          if(bean == null){
            statusTypeValueNotifier?.value = PlaceHolderStatusType.error;
          }
        }
    ));
  }

  ///更新文件播放周期
  void updateMediaPlayInterval(BuildContext context, String picid, String hsns,
      String startday, String endday,
      VoidCallback onPop){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    VgHudUtils.show(context,"设置中");
    VgHttpUtils.get(UPDATE_MEDIA_PLAY_INTERVAL_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
      "hsns": hsns ?? "",
      "startday": (StringUtils.isEmpty(hsns))?null:(startday ?? ""),
      "endday": (StringUtils.isEmpty(hsns))?null:(endday ?? ""),
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          onPop?.call();
          VgToastUtils.toast(context, "设置成功");
          VgEventBus.global.send(new MediaLibraryRefreshEven());
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///设置视频图片为紧急通告模式
  void setPicUrgent(String picid){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    loading(true, msg: "设置中");
    VgHttpUtils.get(SET_PIC_URGENT_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaUrgentUpdateEvent());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///取消设置视频图片为紧急通告模式
  void cancelSetPicUrgent(String picid){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    loading(true, msg: "取消中");
    VgHttpUtils.get(CANCEL_PIC_URGENT_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "picid": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaUrgentUpdateEvent());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///app查询海报展示所需企业信息和所有人地址信息
  void getCompanyAndAddressInfoWithCache(String gps){
    String cacheKey = COMPANY_INFO_AND_ADDRESS_API + (UserRepository.getInstance().getCacheKeySuffix()??"");
    print("cacheKey:" + cacheKey);
    ///获取缓存数据
    ///获取缓存数据
    Future<String> future = SharePreferenceUtil.getString(cacheKey);
    future.then((jsonStr){
      if(!StringUtils.isEmpty(jsonStr)){
        Map map = json.decode(jsonStr);
        PosterCreateInfoBean bean = PosterCreateInfoBean.fromMap(map);
        if(bean != null){
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            companyInfoNotifier?.value = bean?.data;
          }
        }
      }
    });
    getCompanyAndAddressInfo(gps);
  }

  ///app查询海报展示所需企业信息和所有人地址信息
  void getCompanyAndAddressInfo(String gps){
    String cacheKey = COMPANY_INFO_AND_ADDRESS_API + (UserRepository.getInstance().getCacheKeySuffix()??"");
    VgHttpUtils.get(COMPANY_INFO_AND_ADDRESS_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "gps":gps ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          PosterCreateInfoBean bean =
          PosterCreateInfoBean.fromMap(val);
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            companyInfoNotifier?.value = bean?.data;
          }
          if(bean != null){
            SharePreferenceUtil.putString(cacheKey, json.encode(bean));
          }
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
          statusTypeValueNotifier?.value = PlaceHolderStatusType.error;
        }
    ));
  }

  ///获取海报diy信息
  void getPosterDiyInfo(BuildContext context, String id, String intoflg){
    if(StringUtils.isEmpty(id)){
      return;
    }
    VgHttpUtils.get(POSTER_DIY_INFO_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "02",
    },callback: BaseCallback(
        onSuccess: (val){
          PosterDiyInfoBean bean =
          PosterDiyInfoBean.fromMap(val);
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            diyInfoNotifier?.value = bean?.data?.comPicDiy;
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
          statusTypeValueNotifier?.value = PlaceHolderStatusType.error;
        }
    ));
  }

  ///添加or修改字幕
  void addOrEditText(String picid, String title, String subTitle){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    loading(true, msg: "设置中");
    VgHttpUtils.get(ADD_TEXT_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": picid ?? "",
      "title": title ?? null,
      "subtitle": subTitle ?? null,
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///删除字幕
  void deleteText(String picid){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    loading(true, msg: "删除中");
    VgHttpUtils.get(DELETE_TEXT_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": picid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }


  ///设置水印 00没有水印 01有水印
  void setWaterFlag(String picid, String waterflg){
    if(StringUtils.isEmpty(picid)){
      return;
    }
    loading(true, msg: "请稍后");
    VgHttpUtils.get(SET_WATER_FLAG_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": picid ?? "",
      "waterflg": waterflg ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///批量删除
  void deletePics(BuildContext context, String ids){
    if(StringUtils.isEmpty(ids)){
      return;
    }
    loading(true, msg: "删除中");
    VgHttpUtils.get(DELETE_PICS_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "ids": ids ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
          RouterUtils.pop(context);
          VgEventBus.global.send(new MediaLibraryRefreshEven());
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///设置海报是否显示logo
  ///intoflg 01播放列表 02资源库
  ///logoflg 是否显示logo 00显示 01不显示
  void updateLogo(BuildContext context, String id, String intoflg, String logoflg){
    if(StringUtils.isEmpty(id)){
      return;
    }
    loading(true, msg: "请稍后");
    VgHttpUtils.get(UPDATE_LOGO_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "02",
      "logoflg": logoflg ?? "00",
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
          VgEventBus.global.send(new MediaLibraryRefreshEven());
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///设置海报是否显示地址
  ///intoflg 01播放列表 02资源库
  ///addressflg 是否显示地址 00显示 01不显示
  void updateAddress(BuildContext context, String id, String intoflg, String addressflg){
    if(StringUtils.isEmpty(id)){
      return;
    }
    loading(true, msg: "请稍后");
    VgHttpUtils.get(UPDATE_ADDRESS_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "02",
      "addressflg": addressflg ?? "00",
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
          VgEventBus.global.send(new MediaLibraryRefreshEven());
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }



  ///点击喜欢/不喜欢海报
  ///hsn 资源库传空串
  ///id 视频图片id
  ///intoflg 01播放列表 02资源库
  ///likeflg 01喜欢 02不喜欢 03不喜欢删除
  void likeUnLikePoster(BuildContext context, String hsn, String id, String intoflg, String likeflg){
    VgHttpUtils.get(LIKE_UNLIKE_POSTER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "",
      "likeflg": likeflg ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
          if("03" == likeflg){
            RouterUtils.pop(context);
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  //删除合成海报
  void deleteComposePoster(BuildContext context, String hsn, String id, String intoflg, String splpicurl){
    VgHttpUtils.get(DELETE_COMPOSE_POSTER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "",
      "splpicurl": splpicurl ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
          RouterUtils.pop(context);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///下载、分享海报
  ///hsn 资源库传空串
  ///id 视频图片id
  ///intoflg 01播放列表 02资源库
  ///picflg 01下载 02分享
  ///shareflg 下载传空
  void downloadSharePoster(String hsn, String id, String intoflg, String picflg, String shareflg){
    VgHttpUtils.get(DOWNLOAD_SHARE_POSTER_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "",
      "picflg": picflg ?? "",
      "shareflg": shareflg,
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new MediaLibraryRefreshEven());
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///浏览海报
  ///id 海报id
  ///intoflg 01播放列表 02资源库 03海报模板库
  void posterLook(String id, String intoflg){
    if(StringUtils.isEmpty(id)){
      return;
    }
    VgHttpUtils.get(POSTER_LOOK_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
      "intoflg": intoflg ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }


  ///下载、分享海报
  ///hsn 资源库传空串
  ///id 视频图片id
  ///intoflg 01播放列表 02资源库
  ///picflg 01下载 02分享
  ///shareflg 下载传空
  void diyPoster(String diyJson, String id, String intoflg, String picurl,
      String startday, String endday){
    loading(true, msg: "提交中");
    VgMatisseUploadUtils.uploadSingleImage(
        picurl,
        VgBaseCallback(onSuccess: (String netPic) {
          if (StringUtils.isNotEmpty(netPic)) {
            VgHttpUtils.get(DIY_POSTER_API,params: {
              "authId":UserRepository.getInstance().authId ?? "",
              "startday": startday?? "",
              "endday": endday?? "",
              "diyjson": diyJson ?? "",
              "id": id ?? "",
              "intoflg": intoflg ?? "",
              "picurl": netPic ?? "",
            },callback: BaseCallback(
                onSuccess: (val){
                  VgEventBus.global.send(new MediaLibraryRefreshEven());
                  loading(false);
                },
                onError: (msg){
                  loading(false);
                  VgToastUtils.toast(AppMain.context, msg);
                }
            ));

          } else {
            loading(false);
            VgToastUtils.toast(AppMain.context, "图片上传失败");
          }
        }, onError: (String msg) {
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }


  ///获取存储空间信息
  void getStorageInfo(BuildContext context){
    VgHttpUtils.get(GET_STORAGE_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          StorageInfoBean bean =
          StorageInfoBean.fromMap(val);
          storageInfoNotifier?.value = bean?.data;
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }

  ///获取企业自传文件大小
  void getSelfStorageInfo(BuildContext context){
    VgHttpUtils.get(GET_SELF_STORAGE_INFO_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          SelfStorageInfoBean bean =
          SelfStorageInfoBean.fromMap(val);
          selfStorageInfoNotifier?.value = bean?.data;
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }

  ///获取企业自传文件大小
  void getPicNum(BuildContext context){
    VgHttpUtils.get(GET_PLAY_NUM_API, params: {
      "authId":UserRepository.getInstance().authId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          PlayNumBean bean =
          PlayNumBean.fromMap(val);
          if(_picFunction != null){
            _picFunction.call(bean?.data);
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }

  ///获取企业资料库播放切换时间
  void getMediaChangeInterval(BuildContext context){
    VgHttpUtils.get(GET_MEDIA_CHANGE_INTERVAL, params: {
      "companyid":UserRepository.getInstance().getCompanyId() ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          MediaChangeIntervalBean bean =
          MediaChangeIntervalBean.fromMap(val);
          if(bean != null && bean.success && bean.data != null && bean.data.cpsecond != null){
            VgEventBus.global.send(MediaChangeIntervalEvent(bean.data.cpsecond, bean.data.splicing));
            String tempJson = json.encode(bean.data);
            SharePreferenceUtil.putString(MEDIA_CHANGE_INTERVAL, tempJson);
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }


  ///更新企业资料库播放切换时间
  void updateMediaChangeInterval(BuildContext context, int second){
    VgHttpUtils.get(NetApi.UPDATE_MEDIA_CHANGE_INTERVAL, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "cpsecond":second??15,
    },callback: BaseCallback(
        onSuccess: (val){
          VgToastUtils.toast(AppMain.context, "设置成功");
          getMediaChangeInterval(context);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }

  ///更新是否拼合地址
  void updateSplicingPosterStatus(BuildContext context, String splicing){
    VgHttpUtils.get(NetApi.UPDATE_SPLICING_POSTER_STATUS, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "splicing":splicing??"00",
    },callback: BaseCallback(
        onSuccess: (val){
          VgToastUtils.toast(AppMain.context, "设置成功");
          getMediaChangeInterval(context);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "picname":"",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    if(EMPTY_FLAG??false){
      return DEFAULT_MEDIA_LIST_API;
    }
    return ServerApi.BASE_URL + "app/appGetComPicurlList";

  }

  @override
  MediaLibraryIndexResponseBean parseData(VgHttpResponse resp) {
    MediaLibraryIndexResponseBean vo = MediaLibraryIndexResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

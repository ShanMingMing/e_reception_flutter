
import 'package:e_reception_flutter/media_library/media_library_index/bean/poster_create_info_bean.dart';
import 'package:e_reception_flutter/singleton/share_repository/share_repository.dart';
import 'package:e_reception_flutter/singleton/share_repository/vo/share_param_bean.dart';
import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/file_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sharesdk_plugin/sharesdk_defines.dart';
import 'package:sharesdk_plugin/sharesdk_interface.dart';
import 'package:sharesdk_plugin/sharesdk_map.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_save_util_lib.dart';
import 'package:vg_base/vg_screen_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../media_library_index_view_model.dart';


class SelectAddressListPage extends StatefulWidget {

  static const ROUTER = "SelectAddressListPage";

  final List<AddressItemBean> addressList;
  final String cbid;
  final String gps;

  const SelectAddressListPage({
    Key key, @required this.addressList, this.cbid, this.gps
  }) : assert( addressList != null, "addressList must be not null");


  @override
  State<StatefulWidget> createState() => _SelectAddressListState();

  static Future<AddressItemBean> navigatorPush(BuildContext context,
      List<AddressItemBean> addressList, String cbid, String gps) {
    return RouterUtils.routeForFutureResult(context,
        SelectAddressListPage(addressList: addressList,
          cbid: cbid,
          gps: gps,
        ),
        routeName: SelectAddressListPage.ROUTER);
  }
}

class _SelectAddressListState extends BaseState<SelectAddressListPage> {
  MediaLibraryIndexViewModel _viewModel;
  WebViewController _webViewController;
  String _flag = "00";
  String _downloadOrShare = "00";//00下载，01分享
  @override
  void initState() {
    super.initState();
    // _webViewController?.loadUrl(getUrl(widget?.url));
    // loading(true, msg: "生成中");
    MediaLibraryIndexViewModel.EMPTY_FLAG = false;
    _viewModel = MediaLibraryIndexViewModel(this);
    _viewModel.getCompanyAndAddressInfo(widget?.gps);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.CARD_BG_COLOR,
      body: Column(children: <Widget>[
        _toTopBarWidget(),
        Expanded(child: _toAddressListWidget()),
      ]),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      backgroundColor: ColorConstants.BG_OR_SPLIT_COLOR,
      isShowBack: true,
      title: "切换地址",
      isHideRightWidget: true,
    );
  }

  Widget _toAddressListWidget(){
    return ListView.separated(
        itemCount: widget?.addressList?.length ?? 0,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toItemWidget(widget?.addressList?.elementAt(index));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            height: 0.5,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              color: Color(0xFF303546),
            ),
          );
        });
  }

  ///
  Widget _toItemWidget(AddressItemBean itemBean){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context, result: itemBean);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 12),
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            SizedBox(height: 3),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "${itemBean?.branchname}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 14,
                    color: (widget.cbid == itemBean?.cbid)
                        ?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                        :ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                ),
              ),
            ),
            SizedBox(height: 3),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                _branchAddressStr(itemBean),
                softWrap: true,
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 12,
                  color: ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _branchAddressStr(AddressItemBean itemBean){
    String addressStr = itemBean?.address;
    if(itemBean?.addrProvince == itemBean?.addrCity){
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrDistrict}·${itemBean?.address}";
    }else{
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrCity}${itemBean?.addrDistrict}·${itemBean?.address}";
    }
    return addressStr;
  }
}
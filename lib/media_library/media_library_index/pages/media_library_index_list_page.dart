import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_library_index_response_bean.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/click_back_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/menu_dialog.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import '../media_library_index_page.dart';

/// 媒体资源库列表
///
/// @author: zengxiangxi
/// @createTime: 2/3/21 6:04 PM
/// @specialDemand:
class MediaLibraryIndexListPage extends StatelessWidget {

  final List<MediaLibraryIndexListItemBean> list;

  final ValueChanged<MediaLibraryIndexListItemBean> onTap;

  final ValueChanged<MediaLibraryIndexListItemBean> onDelete;

  final ValueChanged<MediaLibraryIndexListItemBean> onUpateName;

  const MediaLibraryIndexListPage({Key key, this.list, this.onTap, this.onDelete, this.onUpateName}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: ListView.separated(
          itemCount: list?.length ?? 0,
          padding: EdgeInsets.only(bottom: getNavHeightDistance(context)),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return _toListItemWidget(context,list?.elementAt(index));
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: 0,
            );
          }),
    );
  }

  Map<String,ValueChanged> _getMenuMap(BuildContext context,MediaLibraryIndexListItemBean itemBean){
    return {
      "重命名":(_){
        onUpateName?.call(itemBean);
      },
      "删除文件":(_){
        onDelete?.call(itemBean);
      }
    };
  }

  Widget _toListItemWidget(BuildContext context,MediaLibraryIndexListItemBean itemBean) {
    return ClickBackgroundWidget(
      onTap: (){
        onTap?.call(itemBean);
      },
      onLongPress: (detail){
        return MenuDialogUtils.showMenuDialog(context: context,longPressDetails: detail,itemMap: _getMenuMap(context,itemBean));
      },
      child: Container(
        height: 60,
        child: Row(
          children: <Widget>[
            SizedBox(width: 15,),
            _toPicWidget(itemBean),
            SizedBox(width: 10,),
            Expanded(
              child: Container(
                height: 36,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      itemBean?.picname ?? "",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 15,
                          height: 1.2
                      ),
                    ),
                    Text(
                      itemBean?.getPlayTimeString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR,
                          fontSize: 12,height: 1.2
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _toPicWidget(MediaLibraryIndexListItemBean itemBean) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        width: 36,
        height: 36,
        color: Colors.black,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            VgCacheNetWorkImage(itemBean?.getCoverUrl() ?? "",imageQualityType: ImageQualityType.middleDown,),
            Positioned(
              top: 2,
              left: 2,
              child: Offstage(
                offstage: (itemBean?.attnum == null || itemBean.attnum <= 0),
                child: Container(
                  width: 5,
                  height: 5,
                  decoration: BoxDecoration(
                      color: ThemeRepository.getInstance()
                          .getMinorRedColor_F95355(),
                      shape: BoxShape.circle),
                ),
              ),
            ),
            Offstage(
              offstage: !(itemBean?.isVideo() ?? false),
              child: Image.asset(
                "images/video_play_ico.png",
                width: 12,
              ),
            )
          ],
        ),
      ),
    );
  }
}

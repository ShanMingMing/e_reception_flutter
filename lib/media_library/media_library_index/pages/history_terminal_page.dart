
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_detail_response_bean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/media_play_time_page.dart';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/set_play_interval_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/terminal_detail_info_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/new_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/terminal_by_pic_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_list_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/terminal_detail_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/click_back_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import '../media_library_index_view_model.dart';

///历史终端页面
class HistoryTerminalPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "HistoryTerminalPage";
  final List<AttListBean> dataList;

  const HistoryTerminalPage({Key key, this.dataList}):super(key: key);

  @override
  _HistoryTerminalPageState createState() => _HistoryTerminalPageState();

  ///跳转方法
  static Future<String> navigatorPush(
      BuildContext context, List<AttListBean> dataList) {
    return RouterUtils.routeForFutureResult(
      context, HistoryTerminalPage(
      dataList: dataList,
    ),
      routeName: HistoryTerminalPage.ROUTER,
    );
  }
}

class _HistoryTerminalPageState extends BaseState<HistoryTerminalPage>{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: CommonPackUpKeyboardWidget(
        child: Column(
          children: [
            _toTopBarWidget(),
            Expanded(child: _terminalListWidget(widget?.dataList)),
          ],
        ),
      ),
    );
  }

  ///标题栏
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "历史播放终端·${widget?.dataList?.length??0}",
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }


  ///终端列表
  Widget _terminalListWidget(List<AttListBean> attList) {
    return ListView.separated(
        itemCount: attList?.length ?? 0,
        padding: EdgeInsets.only(bottom: getNavHeightDistance(context)),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(context, attList, index);
        },
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 0,
          );
        });
  }

  Widget _toListItemWidget(
      BuildContext context, List<AttListBean> attList, int index) {
    AttListBean itemBean = attList[index];
    return ClickBackgroundWidget(
      onTap: () {
        TerminalDetailInfoPage.navigatorPush(context, itemBean?.hsn, router: HistoryTerminalPage.ROUTER);
      },
      child: Container(
        height: 64,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            _toPicWidget(itemBean),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Container(
                height: 38,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Visibility(
                          visible: (StringUtils.isEmpty(itemBean?.terminalName) && StringUtils.isEmpty(itemBean?.sideNumber)&& StringUtils.isNotEmpty(itemBean?.hsn)),
                          child: Text(
                            // "硬件ID：",
                            "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              // fontWeight: FontWeight.w600
                            ),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            itemBean?.getTerminalName(index) ?? "终端显示屏${(index ?? 0) + 1}",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: ThemeRepository.getInstance()
                                    .getTextMainColor_D0E0F7(),
                                fontSize: 15,
                                height: 1.2),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      // "${itemBean?.attnum ?? 0}终端，共播放${itemBean?.playnum ?? 0}次",
                      StringUtils.isNotEmpty(itemBean?.position??"")?itemBean?.position:itemBean?.address??"-",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR, fontSize: 12, height: 1.2),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(width: 12,),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(top: 13),
              child: Text(
                "共播放${itemBean?.playDay??0}天",
                style: TextStyle(
                    fontSize: 10,
                    color:
                    (itemBean?.canAdjust()??false)?
                    ThemeRepository.getInstance().getPrimaryColor_1890FF()
                        :ThemeRepository.getInstance().getHintGreenColor_5E687C()
                ),
              ),
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
      ),
    );
  }
  Widget _toPicWidget(AttListBean itemBean) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        width: 40,
        height: 40,
        color: Color(0xFF191E31),
        child: VgCacheNetWorkImage(
          itemBean?.terminalPicurl ?? "",
          imageQualityType: ImageQualityType.middleDown,
          errorWidget: Container(
            child: Image.asset(
              "images/icon_terminal_holder.png",
              fit: BoxFit.cover,
            ),
          ),
          placeWidget: Container(
            child: Image.asset(
              "images/termial_empty_place_holder_ico.png",
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

}
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

///机构logo组件
class LogoWidget extends StatefulWidget {

  final String logo;

  const LogoWidget({Key key, this.logo,}) : super(key: key);

  @override
  _LogoWidgetState createState() => _LogoWidgetState();
}

class _LogoWidgetState extends BaseState<LogoWidget> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(21),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: 48,
            height: 48,
            decoration: BoxDecoration(
                color: Color(0x4DFFFFFF),
                borderRadius: BorderRadius.circular(7)
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: VgCacheNetWorkImage(
                widget?.logo??"",
                width: 42,
                height: 42,
                fit: BoxFit.contain,
                imageQualityType: ImageQualityType.middleUp,
                defaultPlaceType: ImagePlaceType.head,
                defaultErrorType: ImageErrorType.head,
              ),
            ),
          )
        ],
      ),
    );

  }


}

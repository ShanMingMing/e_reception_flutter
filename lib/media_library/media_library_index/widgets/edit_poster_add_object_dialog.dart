import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/set_poster_content_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';

/// 编辑海报新增元素
class EditPosterAddObjectDialog extends StatefulWidget {
  //增加文字
  final Function(String text) addText;

  //增加图片
  final Function(String picurl) addImage;

  const EditPosterAddObjectDialog({
    Key key,
    this.addText,
    this.addImage,
  }) : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context, Function(String text) addText,
      Function(String picurl) addImage,) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      barrierColor: Color(0x00000001),
      child: EditPosterAddObjectDialog(
        addText: addText,
        addImage: addImage,
      ),
    );
  }

  @override
  _EditPosterAddObjectDialogState createState() =>
      _EditPosterAddObjectDialogState();
}

class _EditPosterAddObjectDialogState extends BaseState<EditPosterAddObjectDialog>{

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize : MainAxisSize.min,
          children: [
            Container(
              height: 100,
              child: Row(
                children: <Widget>[
                  _toCommonFunctionWidget("文字", "icon_add_text", widget?.addText),
                  _toCommonFunctionWidget("图片", "icon_add_image", widget?.addImage),
                ],
              ),
            ),
            Container(
              height: 10,
              color: Color(0xFFF6F7F9),
            ),
            _toCancelWidget(),
          ],
        ),
      ),
    );
  }

  Widget _toCancelWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        child: Text(
          "取消",
          style: TextStyle(
              fontSize: 14,
              color: ThemeRepository.getInstance().getLineColor_3A3F50()
          ),
        ),
      ),
    );
  }

  ///通用样式
  Widget _toCommonFunctionWidget(String title, String asset, Function callback){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if("文字" == title){
          RouterUtils.pop(context);
          SetPosterContentDialog.navigatorPushDialog(context,
              onConfirm: (content){
                callback?.call(content);
              });
        }else{
          _chooseSinglePic();
        }
      },
      child: Container(
        height: 100,
        width: ScreenUtils.screenW(context)/2,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "images/${asset}.png",
              width: 30,
            ),
            SizedBox(height: 6,),
            Text(
              title,
              style: TextStyle(
                color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                fontSize: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _chooseSinglePic() async {

    String fileUrl;
    List<String> list = await MatisseUtil.selectPhoto(context: context,maxSize: 1,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        });
    if(list == null || list.isEmpty||list.length <1){
      toast("选择图片失败");
      return;
    }
    fileUrl = list.elementAt(0);
    HudUtils.showLoading(context,true);
    VgMatisseUploadUtils.uploadSingleImage(
        fileUrl,
        VgBaseCallback(onSuccess: (String netPic) {
          HudUtils.showLoading(context,true);
          if (StringUtils.isNotEmpty(netPic)) {
            RouterUtils.pop(context);
            widget?.addImage?.call(netPic);
          } else {
            VgToastUtils.toast(context, "图片上传失败");
          }
        }, onError: (String msg) {
          HudUtils.showLoading(context,false);
          VgToastUtils.toast(context, msg);
        }));
  }
}





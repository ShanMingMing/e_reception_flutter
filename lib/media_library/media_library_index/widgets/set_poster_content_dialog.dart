
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SetPosterContentDialog extends StatefulWidget {

  final String content;
  final Function(String content) onConfirm;

  const SetPosterContentDialog({Key key, this.content, this.onConfirm})
      : super(key: key);

  @override
  _SetPosterContentDialogState createState() =>
      _SetPosterContentDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      {String content, Function onConfirm,}) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SetPosterContentDialog(
          content: content,
          onConfirm: onConfirm,
        )
    );
  }
}

class _SetPosterContentDialogState
    extends BaseState<SetPosterContentDialog> {
  TextEditingController _contentController;
  bool isAlive = false;
  // AttendanceRecordTodayViewModel viewModel;
  String _hint = "添加文字";
  @override
  void initState() {
    super.initState();
    _contentController = TextEditingController();
    if(StringUtils.isNotEmpty(widget?.content)){
      _contentController?.text = widget?.content;
    }
    isAlive = judgeAlive();
    if(isAlive){
      _hint = "修改文字";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
          decoration: BoxDecoration(
              color: Color(0XFFF6F7F9),
              borderRadius:
                  BorderRadius.only(topLeft: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS),
                      topRight: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS))),
          child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                RouterUtils.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.only(left:16, top: 16, right: 40, bottom: 16),
                child: Image.asset(
                  "images/icon_hide_dialog.png",
                  width: 20,
                ),
              ),
            ),
            Spacer(),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                widget?.onConfirm?.call(_contentController.text);
                RouterUtils.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.only(left:40, top: 16, right: 16, bottom: 16),
                child: Image.asset(
                  "images/icon_confirm_set_content.png",
                  width: 20,
                ),
              ),
            ),
          ],
        ),
        ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: 45,
            maxHeight: 80
          ),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4)
                    ),
                    child: VgTextField(
                      controller: _contentController,
                      minLines: 1,
                      autofocus: true,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        counterText: "",
                        contentPadding: const EdgeInsets.only(left: 15, right: 15, top: 13, bottom: 12),
                        hintStyle: TextStyle(
                            color: ThemeRepository.getInstance()
                                .getTextEditHintColor_3A3F50(),
                            fontSize: 14),
                      ),
                      style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance()
                              .getCardBgColor_21263C()),
                      onChanged: (String str) {
                        isAlive = judgeAlive();
                        setState(() {});
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 20,),
      ],
    );
  }


  bool judgeAlive(){
    return StringUtils.isNotEmpty(_contentController.text);
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 微信、朋友圈分享弹窗
class SharePosterMenuDialog extends StatelessWidget {
  final VoidCallback wechatLine;
  final VoidCallback wechat;
  final Color bgColor;
  final Color splitColor;
  final Color textColor;
  final String pyqResource;

  const SharePosterMenuDialog({Key key, this.wechatLine, this.wechat, this.bgColor,
  this.splitColor, this.textColor, this.pyqResource}) : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(
      BuildContext context, VoidCallback wechatLine,
      VoidCallback wechat, {Color bgColor, Color splitColor, Color textColor, String pyqResource}) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      child: SharePosterMenuDialog(
          wechatLine: wechatLine,
        wechat: wechat,
        bgColor:bgColor,
        splitColor:splitColor,
        textColor:textColor,
        pyqResource:pyqResource,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
      child: Container(
        decoration: BoxDecoration(
            color: bgColor??ThemeRepository.getInstance().getCardBgColor_21263C()),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _toMainWidget(context),
            Container(
              height: 10,
              color: splitColor??ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            ),
            _toCancelButtonWidget(context),
          ],
        ),
      ),
    );
  }

  Widget _toMainWidget(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: wechatLine,
            child: Container(
              alignment: Alignment.center,
              height: 90,
              child: Image.asset(
                pyqResource??"images/icon_pyq.png",
                width: 50,
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: wechat,
            child: Container(
              alignment: Alignment.center,
              height: 90,
              child: Image.asset(
                "images/icon_wx.png",
                width: 50,
              ),
            ),
          ),
        ),
      ],
    );

  }


  Widget _toCancelButtonWidget(BuildContext context) {
    final double bottomStatusHeight =  ScreenUtils.getBottomBarH(context);
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 50 + bottomStatusHeight,
        padding: EdgeInsets.only(bottom: bottomStatusHeight),
        child: Center(
          child: Text(
            "取消",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: textColor??ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
        ),
      ),
    );
  }
}

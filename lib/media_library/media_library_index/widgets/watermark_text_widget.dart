import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../media_library_index_view_model.dart';
import 'add_poster_text_dialog.dart';

///水印字幕组件
class WatermarkTextWidget extends StatefulWidget {

  final String id;
  final String watermark;
  final String waterflg;
  final String title;
  final String content;

  const WatermarkTextWidget({Key key, this.id, this.watermark, this.waterflg, this.title, this.content}) : super(key: key);

  @override
  _WatermarkTextWidgetState createState() => _WatermarkTextWidgetState();
}

class _WatermarkTextWidgetState extends BaseState<WatermarkTextWidget> {

  String _watermark;
  String _waterflg;
  String _title;
  String _content;

  MediaLibraryIndexViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = MediaLibraryIndexViewModel(this);
    _watermark = widget?.watermark;
    _waterflg = widget?.waterflg;
    _title = widget?.title;
    _content = widget?.content;
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(StringUtils.isEmpty(_title??"") && StringUtils.isEmpty(_content??"")){
          return;
        }
        if(StringUtils.isEmpty(widget?.id)){
          return;
        }
        CommonMoreMenuDialog.navigatorPushDialog(context, {
          "修改字幕":(){
            AddPosterTextDialog.navigatorPushDialog(context, title: _title??"", content: _content??"",
                onConfirm: (title, content){
                  _viewModel.addOrEditText(widget?.id, title, content);
                  setState(() {
                    _title = title;
                    _content = content;
                  });
                },
                onDelete: (){
                  _viewModel.deleteText(widget?.id);
                  setState(() {
                  _title = "";
                  _content = "";
                  });
                }
            );

          },
          "删除字幕":()async{
            bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                title: "提示",
                content: "确认删除字幕？",
                cancelText: "取消",
                confirmText: "删除",
                confirmBgColor:
                ThemeRepository.getInstance().getMinorRedColor_F95355());
            if (result ?? false) {
              _viewModel.deleteText(widget?.id);
              setState(() {
                _title = "";
                _content = "";
              });
            }
          }

        });
      },

      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 12),
        decoration: (StringUtils.isNotEmpty(_title??"") || StringUtils.isNotEmpty(_content??""))? BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0x00000000),
                  Color(0x4D000000),
                ]
            )
        ): BoxDecoration(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){

              },
              child: Visibility(
                visible: "01" == _waterflg,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height: 24,
                      padding: EdgeInsets.only(left: 6, right: 12),
                      decoration: BoxDecoration(
                        color: Color(0x1A000000),
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(
                            color: Colors.white,
                            width: 0.2),
                      ),
                      child: Text(
                        "· ${_watermark}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          height: 1.22
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: "01" == _waterflg?6:30,),
            Visibility(
              visible: StringUtils.isNotEmpty(_title??""),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  _title??"",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      height: 1.3
                  ),
                ),
              ),
            ),
            Visibility(
                visible: StringUtils.isNotEmpty(_title??""),
                child: SizedBox(height:4,)
            ),
            Visibility(
              visible: StringUtils.isNotEmpty(_content??""),
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  _content??"",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                      height: 1.42
                  ),
                ),
              ),
            ),
            SizedBox(height: ("01" == _waterflg && StringUtils.isEmpty(_content??""))?6:12,),
          ],
        ),
      ),
    );
  }


}

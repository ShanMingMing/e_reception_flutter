import 'package:e_reception_flutter/media_library/media_library_index/widgets/set_poster_content_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/temp_empty_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/set_terminal_volume_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 编辑海报点击文字弹窗
class EditPosterTextFunctionDialog extends StatefulWidget {
  //改字
  final Function changeText;

  //样式
  final Function showStyle;

  //对齐
  final Function align;

  //复制
  final Function copy;

  //删除
  final Function delete;

  final String content;

  const EditPosterTextFunctionDialog({
    Key key,
    this.content,
    this.changeText,
    this.showStyle,
    this.align,
    this.copy,
    this.delete,
  }) : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      String content,
      {Function changeText,
        Function showStyle,
        Function align,
        Function copy,
        Function delete,
      }) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      barrierColor: Color(0x00000001),
      child: EditPosterTextFunctionDialog(
        content: content,
        changeText: changeText,
        showStyle: showStyle,
        align: align,
        copy: copy,
        delete: delete,
      ),
    );
  }

  @override
  _EditPosterTextFunctionDialogState createState() =>
      _EditPosterTextFunctionDialogState();
}

class _EditPosterTextFunctionDialogState extends BaseState<EditPosterTextFunctionDialog>{

  TextEditingController _contentController;
  double _textSpacing;
  double _lineSpacing;
  @override
  void initState() {
    super.initState();
    _contentController = TextEditingController();
    _textSpacing = 52;
    _lineSpacing = 1.5;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(6)),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xFFF6f7F9)
          ),
          child: Column(
            mainAxisSize : MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 12, left: 28, right: 28),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _toCommonFunctionWidget("icon_poster_change_text", "icon_poster_change_text_selected",  "改字", widget?.changeText),
                    _toCommonFunctionWidget("icon_poster_change_style", "icon_poster_change_style_selected", "样式", widget?.showStyle),
                    _toCommonFunctionWidget("icon_poster_change_align", "icon_poster_change_align_selected", "对齐", widget?.align),
                    _toCommonFunctionWidget("icon_poster_copy", "icon_poster_change_text_selected", "复制", widget?.copy),
                    _toCommonFunctionWidget("icon_poster_delete", "icon_poster_change_text_selected", "删除", widget?.delete),
                  ],
                ),
              ),
              // _toSetTextWidget(),
              // _toChangeTextStyleWidget(),
              // __toChangeAlignWidget(),

            ],
          ),
        ),
      ),
    );
  }


  ///设置文字
  Widget _toSetTextWidget(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ConstrainedBox(
          constraints: BoxConstraints(
              minHeight: 45,
              maxHeight: 70
          ),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4)
                    ),
                    child: VgTextField(
                      controller: _contentController,
                      minLines: 1,
                      autofocus: true,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        counterText: "",
                        contentPadding: const EdgeInsets.only(left: 15, right: 15, top: 13, bottom: 12),
                        hintStyle: TextStyle(
                            color: ThemeRepository.getInstance()
                                .getTextEditHintColor_3A3F50(),
                            fontSize: 14),
                      ),
                      style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance()
                              .getCardBgColor_21263C()),
                      onChanged: (String str) {
                        setState(() {});
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        _toCommonConfirmCancelWidget((){}, (){}),
      ],
    );
  }

  ///设置文字样式
  Widget _toChangeTextStyleWidget(){
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4)
          ),
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(horizontal: 15),
          height: 65,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _toCommonFunctionWidget("icon_text_bold", "icon_text_bold_selected", "加粗", widget?.changeText),
              _toSplitLineWidget(),
              _toCommonFunctionWidget("icon_text_italic", "icon_text_italic_selected", "斜体", widget?.showStyle),
              _toSplitLineWidget(),
              _toCommonFunctionWidget("icon_text_under_line", "icon_text_under_line_selected", "下划线", widget?.align),
              _toSplitLineWidget(),
              _toCommonFunctionWidget("icon_text_delete_line", "icon_text_delete_line_selected", "删除线", widget?.copy),
              _toSplitLineWidget(),
              _toCommonFunctionWidget("icon_text_orientation", "icon_text_orientation_selected", "文字方向", widget?.delete),
            ],
          ),
        ),
        SizedBox(height: 10,),
        _toTextSpacingWidget(),
        _toCommonConfirmCancelWidget((){}, (){}),
      ],
    );
  }


  ///设置字间距以及行间距
  Widget _toTextSpacingWidget(){
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4)
      ),
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: 15),
      padding: EdgeInsets.symmetric(horizontal: 15),
      height: 100,
      child: Column(
        children: [
          Container(
            height: 50,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: 40,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "字间距",
                    style: TextStyle(
                        fontSize: 10,
                        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                        height: 1.2
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: SliderTheme(
                    data: SliderThemeData(
                        overlayShape: SliderComponentShape.noOverlay,
                        thumbShape: RingThumbShape(),
                        trackHeight: 2,
                        trackShape: CustomRoundedRectSliderTrackShape(),
                        inactiveTickMarkColor: Colors.transparent,
                        activeTickMarkColor: Colors.transparent,
                        inactiveTrackColor: Color(0xFFEEEEEE),
                        activeTrackColor: ThemeRepository.getInstance().getLineColor_3A3F50(),
                        valueIndicatorColor: Colors.transparent),
                    child: Slider(
                      min: 0.0,
                      max: 100,
                      value: _textSpacing,
                      divisions: 1000,
                      onChanged: (value) {
                        setState(() {
                          this._textSpacing = value;
                        });
                      },
                      onChangeEnd: (value){
                        this._textSpacing = value;
                      },
                    ),
                  ),
                ),
                Container(
                  width: 22,
                  alignment: Alignment.centerRight,
                  child: Text(
                    "${_textSpacing??52}",
                    style: TextStyle(
                        fontSize: 10,
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  width: 40,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "行间距",
                    style: TextStyle(
                        fontSize: 10,
                        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                        height: 1.2
                    ),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: SliderTheme(
                    data: SliderThemeData(
                        overlayShape: SliderComponentShape.noOverlay,
                        thumbShape: RingThumbShape(),
                        trackHeight: 2,
                        trackShape: CustomRoundedRectSliderTrackShape(),
                        inactiveTickMarkColor: Colors.transparent,
                        activeTickMarkColor: Colors.transparent,
                        inactiveTrackColor: Color(0xFFEEEEEE),
                        activeTrackColor: ThemeRepository.getInstance().getLineColor_3A3F50(),
                        valueIndicatorColor: Colors.transparent),
                    child: Slider(
                      min: 0.0,
                      max: 100,
                      value: _lineSpacing,
                      divisions: 1000,
                      onChanged: (value) {
                        setState(() {
                          this._lineSpacing = value;
                        });
                      },
                      onChangeEnd: (value){
                        this._lineSpacing = value;
                      },
                    ),
                  ),
                ),
                Container(
                  width: 22,
                  alignment: Alignment.centerRight,
                  child: Text(
                    "${_lineSpacing??1.5}",
                    style: TextStyle(
                        fontSize: 10,
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


  ///
  Widget __toChangeAlignWidget(){
      return Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4)
            ),
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: 43),
            height: 65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _toCommonFunctionWidget("icon_align_left", "icon_align_left_selected", "左对齐", widget?.changeText),
                _toSplitLineWidget(),
                _toCommonFunctionWidget("icon_align_center", "icon_align_center_selected", "居中对齐", widget?.showStyle),
                _toSplitLineWidget(),
                _toCommonFunctionWidget("icon_align_right", "icon_align_right_selected", "右对齐", widget?.align),
              ],
            ),
          ),
          _toCommonConfirmCancelWidget((){}, (){})
        ],
      );
  }

  ///通用样式
  Widget _toCommonFunctionWidget(String asset, String selectAsset, String title, Function callback){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if("改字" == title){
          SetPosterContentDialog.navigatorPushDialog(context,
              content: widget?.content,
              onConfirm: (content){
                callback.call(content);
              });
        }else{
          TempEmptyPage.navigatorPush(context);
          // VgToastUtils.toast(context, "敬请期待");
        }

      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "images/${asset}.png",
            width: 19,
          ),
          SizedBox(height: 5,),
          Text(
            title,
            style: TextStyle(
              color: ThemeRepository.getInstance().getLineColor_3A3F50(),
              fontSize: 10,
            ),
          ),
        ],
      ),
    );
  }

  ///分割线
  Widget _toSplitLineWidget() {
    return Container(
      height: 24,
      width: 0.5,
      color: Color(0xFFEEEEEE),
    );
  }

  ///通用确定以及取消布局
  Widget _toCommonConfirmCancelWidget(Function cancel, Function confirm){
    return Container(
      height: 50,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(15),
            child: GestureDetector(
              onTap: (){
                cancel.call();
              },
              child: Image.asset(
                "images/icon_hide_dialog.png",
                width: 20,
                height: 20,
              ),
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(15),
            child: GestureDetector(
              onTap: (){
                confirm.call();
              },
              child: Image.asset(
                "images/icon_confirm_set_content.png",
                width: 20,
                height: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

}

///游标形状
class RingThumbShape extends SliderComponentShape {


  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size(14, 22);
  }

  @override
  void paint(PaintingContext context,
      Offset center,
      {Animation<double> activationAnimation,
        Animation<double> enableAnimation,
        bool isDiscrete,
        TextPainter labelPainter,
        RenderBox parentBox,
        SliderThemeData sliderTheme,
        TextDirection textDirection,
        double value,
        double textScaleFactor,
        Size sizeWithOverflow}) {
    Canvas canvas = context.canvas;
    Paint indicatorPaint = new Paint()
      ..style = PaintingStyle.fill
      ..shader = RadialGradient(
        colors: [
          Color(0x66000000),
          Color(0x00000000),
        ],
      ).createShader(Rect.fromCircle(
        center: Offset(center.dx+0.5, center.dy+0.5),
        radius: 13,
      ));
    Paint ringPaint = new Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    canvas.drawCircle(Offset(center.dx, center.dy),
        20, indicatorPaint);
    canvas.drawCircle(Offset(center.dx, center.dy),
        9, ringPaint);
  }

}





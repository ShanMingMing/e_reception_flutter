import 'dart:ffi';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_widget_lib.dart';

/// 海报反馈弹窗
class PosterFeedbackDialog extends StatelessWidget {

  final String content;

  final String cancelText;

  final String confirmText;

  final Color confirmBgColor;

  final VoidCallback onConfrim;

  final VoidCallback onPop;


  final bool isShowLeftButton;

  final Widget contentWidget;

  const PosterFeedbackDialog({Key key, this.content, this.cancelText, this.confirmText, this.confirmBgColor, this.onConfrim, this.onPop, this.isShowLeftButton = true, this.contentWidget}) : super(key: key);

  static Future<bool> navigatorPushDialog(BuildContext context,{String title,String content,String cancelText,String confirmText,Color confirmBgColor,Widget contentWidget, bool isShowLeftButton}){
    return VgDialogUtils.showCommonDialog<bool>(context: context, child: PosterFeedbackDialog(
      content: content,
      cancelText: cancelText,
      confirmText: confirmText,
      confirmBgColor: confirmBgColor,
      contentWidget: contentWidget,
      isShowLeftButton: isShowLeftButton,
    ),barrierDismissible: true);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> RouterUtils.pop(context),
      child: UnconstrainedBox(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){},
          child: Material(
            type: MaterialType.transparency,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                width: 290,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                    color: ThemeRepository.getInstance().getCardBgColor_21263C()
                ),
                child: _toMainColumnWidget(context),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 30,),
        Container(
          child: contentWidget??Text(
            content ?? "是否取消吗？",
            overflow: TextOverflow.ellipsis,
            maxLines: 5,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 16,
            ),
          ),
        ),
        SizedBox(height: 30,),
        _toTwoButtonWidget(context),
      ],
    );
  }

  Widget _toTwoButtonWidget(BuildContext context){
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        children: <Widget>[
          if(isShowLeftButton ?? true)
            Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: (){
                  if(onPop != null){
                    onPop?.call();
                    return ;
                  }
                  RouterUtils.pop(context,result: false);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                        color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                        width: 1),
                  ),
                  child: Center(
                    child:  Text(
                      cancelText ?? "取消",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR,
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                ),
              ),
            ),
          SizedBox(width: 10,),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(onConfrim != null){
                  onConfrim?.call();
                  return ;
                }
                RouterUtils.pop(context,result: true);
              },
              child: Container(
                decoration: BoxDecoration(
                  color:confirmBgColor?? ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Center(
                  child:  Text(
                    confirmText ?? "确定",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        height: 1.2
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

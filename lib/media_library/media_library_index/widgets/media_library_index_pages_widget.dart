// import 'package:e_reception_flutter/media_library/media_library_index/pages/media_library_index_grid_page.dart';
// import 'package:e_reception_flutter/media_library/media_library_index/pages/media_library_index_list_page.dart';
// import 'package:flutter/widgets.dart';
// import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
//
// /// 媒体库pageview
// ///
// /// @author: zengxiangxi
// /// @createTime: 2/3/21 5:28 PM
// /// @specialDemand:
// class MediaLibraryIndexPages extends StatelessWidget {
//
//   final PageController pageController;
//
//   const MediaLibraryIndexPages({Key key, this.pageController}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: _toPageViewWidget(),
//     );
//   }
//
//   Widget _toPageViewWidget() {
//     return ScrollConfiguration(
//       behavior: MyBehavior(),
//       child: PageView(
//         controller: pageController,
//         physics: ClampingScrollPhysics(),
//         children: <Widget>[
//           MediaLibraryIndexGridPage(),
//           MediaLibraryIndexListPage(),
//         ],
//       ),
//     );
//   }
// }

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class AddPosterTextDialog extends StatefulWidget {

  final String title;
  final String content;
  final Function(String title, String content) onConfirm;
  final Function() onDelete;
  final int picSize;

  const AddPosterTextDialog({Key key,this.title, this.content, this.onConfirm, this.onDelete, this.picSize})
      : super(key: key);

  @override
  _AddPosterTextDialogState createState() =>
      _AddPosterTextDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      {String title, String content, Function onConfirm, Function onDelete, int picSize}) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: AddPosterTextDialog(
          title: title,
          content: content,
          onConfirm: onConfirm,
          onDelete: onDelete,
          picSize: picSize,
        )
    );
  }
}

class _AddPosterTextDialogState
    extends BaseState<AddPosterTextDialog> {
  TextEditingController _titleController;
  TextEditingController _contentController;
  bool isAlive = false;
  // AttendanceRecordTodayViewModel viewModel;
  String _hint = "添加字幕";
  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _contentController = TextEditingController();
    // viewModel = AttendanceRecordTodayViewModel(this, null);
    if(StringUtils.isNotEmpty(widget?.title)){
      _titleController?.text = widget?.title;
    }
    if(StringUtils.isNotEmpty(widget?.content)){
      _contentController?.text = widget?.content;
    }
    isAlive = judgeAlive();
    if(isAlive){
      _hint = "修改字幕";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              borderRadius:
                  BorderRadius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS)),
          child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                "${_hint}${(widget?.picSize??0)>1?("·" + widget?.picSize.toString() + "文件"):""}",
                style: TextStyle(
                    fontSize: 17,
                    color: ThemeRepository.getInstance()
                        .getTextMainColor_D0E0F7(),fontWeight: FontWeight.w600
                ),
              ),
            ),
            Spacer(),
            _deleteWidget(),
            _buttonWidget(),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          height: 40,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color:
                    ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                    borderRadius: BorderRadius.circular(8)
                  ),
                  child: VgTextField(
                    controller: _titleController,
                    maxLines: 1,
                    minLines: 1,
                    maxLength: 30,
                    autofocus: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      counterText: "",
                      contentPadding: const EdgeInsets.only(left: 15, bottom: 10),
                      hintText: "标题，30字以内",
                      hintStyle: TextStyle(
                          color: ThemeRepository.getInstance()
                              .getTextEditHintColor_3A3F50(),
                          fontSize: 14),
                    ),
                    style: TextStyle(
                        fontSize: 14,
                        color: ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7()),
                    onChanged: (String str) {
                      isAlive = judgeAlive();
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10,),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          alignment: Alignment.topLeft,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 120,
                  decoration: BoxDecoration(
                      color:
                      ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: VgTextField(
                    controller: _contentController,
                    minLines: 4,
                    maxLength: 100,
                    autofocus: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      counterText: "",
                      contentPadding: const EdgeInsets.only(left: 15, top: 12),
                      hintText: "内容，100字以内",
                      hintStyle: TextStyle(
                          color: ThemeRepository.getInstance()
                              .getTextEditHintColor_3A3F50(),
                          fontSize: 14),
                    ),
                    style: TextStyle(
                        fontSize: 14,
                        color: ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7()),
                    onChanged: (String str) {
                      isAlive = judgeAlive();
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: (widget?.picSize??0)>1,
          child: SizedBox(height: 8,),
        ),
        Visibility(
          visible: (widget?.picSize??0)>1,
          child: Container(
            margin: EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            child: Text(
              "注：如需为单个文件添加字幕，请在上传成功后操作",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 12
              ),
            ),
          ),
        ),
        SizedBox(height: 30,),
      ],
    );
  }

  Widget _buttonWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isAlive ?? false,
        width: 48,
        height: 23,
        unSelectBgColor:
            ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 14,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
          height: 1.2,
        ),
        text: "确定",
        onTap: () {
          if (isAlive) {
            widget?.onConfirm?.call(_titleController.text, _contentController.text);
            RouterUtils.pop(context);
          }
        },
      ),
    );
  }

  Widget _deleteWidget(){
    return Opacity(
        opacity: (StringUtils.isNotEmpty(widget?.title??"") || StringUtils.isNotEmpty(widget?.content??"")) ? 1 : 0,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () async {
            if(!judgeAlive()){
              return;
            }
            bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                title: "提示",
                content: "确认删除字幕？",
                cancelText: "取消",
                confirmText: "删除",
                confirmBgColor:
                ThemeRepository.getInstance().getMinorRedColor_F95355());
            if (result ?? false) {
              VgToolUtils.removeAllFocus(context);
              widget?.onDelete?.call();
              RouterUtils.pop(context);
            }
          },
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 10,),
            child: Text(
              "删除",
              style: TextStyle(
                  fontSize: 14,
                  height: 1.2,
                  color: ThemeRepository.getInstance()
                      .getHintGreenColor_5E687C()),
            ),
          ),
        ));
  }

  bool judgeAlive(){
    return StringUtils.isNotEmpty(_titleController.text) || StringUtils.isNotEmpty(_contentController.text);
  }
}

import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/set_poster_content_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/temp_empty_page.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 编辑海报点击图片弹窗
class EditPosterImageFunctionDialog extends StatefulWidget {
  //换图
  final Function changeImage;

  //裁剪
  final Function clip;

  //复制
  final Function copy;

  //删除
  final Function delete;


  const EditPosterImageFunctionDialog({
    Key key,
    this.changeImage,
    this.clip,
    this.copy,
    this.delete,
  }) : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context,
  {Function changeImage,
      Function showStyle,
      Function copy,
      Function delete,
  }) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      barrierColor: Color(0x00000001),
      child: EditPosterImageFunctionDialog(
        changeImage: changeImage,
        clip: showStyle,
        copy: copy,
        delete: delete,
      ),
    );
  }

  @override
  _EditPosterImageFunctionDialogState createState() =>
      _EditPosterImageFunctionDialogState();
}

class _EditPosterImageFunctionDialogState extends BaseState<EditPosterImageFunctionDialog>{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 11, bottom: 12, left: 40, right: 40),
      decoration: BoxDecoration(
          color: Colors.white
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _toCommonFunctionWidget("icon_poster_change_image", "换图", widget?.changeImage),
          _toCommonFunctionWidget("icon_poster_clip", "裁剪", widget?.clip),
          _toCommonFunctionWidget("icon_poster_copy", "复制", widget?.copy),
          _toCommonFunctionWidget("icon_poster_delete", "删除", widget?.delete),
        ],
      ),
    );
  }

  Widget _toCommonFunctionWidget(String asset, String title, Function callback){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if("换图" == title){
          _chooseSinglePic();
        }else{
          TempEmptyPage.navigatorPush(context);
          // VgToastUtils.toast(context, "敬请期待");
        }

      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "images/${asset}.png",
            width: 19,
          ),
          SizedBox(height: 5,),
          Text(
            title,
            style: TextStyle(
              color: ThemeRepository.getInstance().getLineColor_3A3F50(),
              fontSize: 10,
            ),
          ),
        ],
      ),
    );
  }

  void _chooseSinglePic() async {
    String fileUrl;
    List<String> list = await MatisseUtil.selectPhoto(context: context,maxSize: 1,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        });
    if(list == null || list.isEmpty||list.length <1){
      toast("选择图片失败");
      return;
    }
    fileUrl = list.elementAt(0);
    VgMatisseUploadUtils.uploadSingleImage(
        fileUrl,
        VgBaseCallback(onSuccess: (String netPic) {
          if (StringUtils.isNotEmpty(netPic)) {
            widget?.changeImage?.call(netPic);
          } else {
            VgToastUtils.toast(context, "图片上传失败");
          }
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }
}





import 'package:e_reception_flutter/media_library/media_library_index/bean/SelfStorageInfoBean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/StorageInfoBean.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 资料库-topbar
///
/// @author: zengxiangxi
/// @createTime: 2/3/21 1:49 PM
/// @specialDemand:
class MediaLibraryIndexTopBarWidget extends StatelessWidget {

  final ValueNotifier<StorageDataBean> storageInfoNotifier;
  final ValueNotifier<SelfStorageDataBean> selfStorageInfoNotifier;

  final VoidCallback onAddTap;

  final VoidCallback onHandleTap;

  const MediaLibraryIndexTopBarWidget({Key key, this.storageInfoNotifier,
    this.selfStorageInfoNotifier,
    this.onAddTap, this.onHandleTap,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toTopBarWidget(context);
  }

  String showMemoryStr(int mbValue) {
    if (mbValue == null || mbValue <= 0) {
      return "0MB";
    }
    if (mbValue < 1024) {
      return "${mbValue}MB";
    }
    if(mbValue % 1024 == 0){
      return "${(mbValue / 1024)}GB";
    }else{
      return "${((mbValue*1.0) / 1024).toStringAsFixed(2)}GB";
    }

  }

  Widget _toTopBarWidget(BuildContext context) {
    final double statusHeight = ScreenUtils.getStatusBarH(context);
    int total = 500;
    return Container(
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      padding: EdgeInsets.only(left: 15),
      child: Column(
        children: [
          Container(
            height: 44,
            child: Row(
              children: <Widget>[
                ValueListenableBuilder(
                  valueListenable: storageInfoNotifier,
                  builder: (BuildContext context, StorageDataBean storageInfo, Widget child) {
                    if(storageInfo != null){
                      int num = storageInfo.hsnNum??1;
                      int size = storageInfo.spacesize??500;
                      total = num*size;
                      if(total == 0){
                        total = 500;
                      }
                    }
                    return Text(
                      "免费云存储空间·${showMemoryStr(total)}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 17,
                          height: 1.2,
                          fontWeight: FontWeight.w600),
                    );
                  },
                ),
                Spacer(),
                Visibility(
                  visible: false,
                  child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: (){
                        onHandleTap?.call();
                      },
                      child: Container(
                        padding: EdgeInsets.only(right: 15),
                        child: Text(
                          "购买",
                          style: TextStyle(
                              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                              fontSize: 13,
                              height: 1.2
                          ),
                        ),
                      )
                  ),
                ),
              ],
            ),
          ),
          ValueListenableBuilder(
            valueListenable: selfStorageInfoNotifier,
            builder: (BuildContext context, SelfStorageDataBean storageInfo, Widget child) {
              double width = ScreenUtils.screenW(context) - 30;
              double picWidth = (((storageInfo?.picSpacesize??0)*1.0)/(total*1024)) * width;
              double videoWidth = (((storageInfo?.videoSpacesize??0)*1.0)/(total*1024)) * width;
              double audioWidth = 0;
              double percent = 0;
              if(total > 0){
                percent = (((storageInfo?.picSpacesize??0) + (storageInfo?.videoSpacesize??0)) * 1.0)/(total*1024);
              }
              return Column(
                children: [
                  Container(
                    height: 20,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "已用${storageInfo?.getStorageStr()??"0MB"}",
                      style: TextStyle(
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                        fontSize: 14,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(2),
                    child: Container(
                      height: 12,
                      color: Color(0x21FFFFFF),
                      margin: EdgeInsets.only(right: 15),
                      child: Row(
                        children: [
                          Container(
                            height: 12,
                            width: picWidth??0,
                            decoration: BoxDecoration(
                              color: Color(0xFF00C6C4),
                            ),
                          ),
                          Container(
                            height: 12,
                            width: videoWidth??0,
                            decoration: BoxDecoration(
                              color: Color(0xFF1890FF),
                            ),
                          ),
                          Container(
                            height: 12,
                            width: audioWidth??0,
                            decoration: BoxDecoration(
                              color: Color(0xFFFFB714),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      _toSampleWidget(0xFF00C6C4, "图片", storageInfo?.picSpacesize??0),
                      Visibility(
                        visible: (storageInfo?.picSpacesize??0)>0,
                        child: SizedBox(width: 15,),
                      ),
                      _toSampleWidget(0xFF1890FF, "视频", storageInfo?.videoSpacesize??0),
                      Visibility(
                        visible: (storageInfo?.videoSpacesize??0)>0,
                        child: SizedBox(width: 15,),
                      ),
                      _toSampleWidget(0xFFFFB714, "音频", 0),
                      Spacer(),
                      Text(
                        (percent <= 0)?"0%":"${(percent??0).toStringAsFixed(2)}%",
                        style: TextStyle(
                            color: Color(0xFFBFC2CC),
                            fontSize: 11,
                            height: 1.2
                        ),
                      ),
                      SizedBox(width: 15,)
                    ],
                  ),
                ],
              );
            },
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  String getStorageStr(int size){
    if(size <= 1024){
      return size.toStringAsPrecision(3) + "KB";
    }else if(size > 1024 && size <= 1024*1024){
      return (size/1024).toStringAsPrecision(3) + "MB";
    }else{
      return (size/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }

  Widget _toSampleWidget(int color, String type, int size){
    return Visibility(
      visible: (size??0)>0,
      child: Container(
        alignment: Alignment.center,
        child: Row(
          children: [
            Container(
              width: 5,
              height: 5,
              decoration: BoxDecoration(
                  color: Color(color),
                  borderRadius: BorderRadius.circular(1)
              ),
            ),
            SizedBox(width: 4,),
            Text(
              "${type??""}${getStorageStr(size)}",
              style: TextStyle(
                  color: Color(0xFFBFC2CC),
                  fontSize: 11,
                  height: 1.2
              ),
            ),
          ],
        ),
      ),
    );
  }
}

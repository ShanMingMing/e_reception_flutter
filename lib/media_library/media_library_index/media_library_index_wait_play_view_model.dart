import 'dart:convert';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_library_index_response_bean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/media_library_play_page.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'even/media_num_event.dart';


class MediaLibraryIndexWaitPlayViewModel extends BasePagerViewModel<
    MediaLibraryIndexListItemBean,
    MediaLibraryIndexResponseBean> {

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  MediaLibraryIndexWaitPlayViewModel(BaseState<StatefulWidget> state)
      : super(state){
    totalValueNotifier = ValueNotifier(null);
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();

  }

  @override
  bool isNeedCache() {
    return true;
  }

  void getCache(){
    String cacheKey = getUrl() + (UserRepository.getInstance().authId??"");
    ///获取缓存数据
    Future<String> future = SharePreferenceUtil.getString(cacheKey);
    future.then((jsonStr){
      if(!StringUtils.isEmpty(jsonStr)){
        Map map = json.decode(jsonStr);
        MediaLibraryIndexResponseBean vo = MediaLibraryIndexResponseBean.fromMap(map);
        FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
        loading(false);
        if((vo?.data?.page?.total??0) > 0){
          statusTypeValueNotifier?.value = null;
        }else{
          statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
        }
        totalValueNotifier.value = vo?.data?.page?.total;
      }
    });
  }



  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appGetNextComPicList";
  }

  @override
  MediaLibraryIndexResponseBean parseData(VgHttpResponse resp) {
    MediaLibraryIndexResponseBean vo = MediaLibraryIndexResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    VgEventBus.global.send(new MediaNumEvent(totalValueNotifier.value, MediaLibraryPlayPage.TYPE_WAIT_PLAY));
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

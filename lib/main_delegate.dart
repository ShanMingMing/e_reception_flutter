
import 'dart:ui';
import 'package:camera/camera.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/location_repository/location_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/singleton/web_socket_repository/util/web_socket_util.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/vg_oss_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/services.dart';
import 'package:sharesdk_plugin/sharesdk_interface.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'constants/constant.dart';
import 'generated/l10n.dart';

abstract class LifecycleInterface{

  initState();

  dispose();
}

List<CameraDescription> camerasList;

///启动项配置文件
class MainDelegate implements LifecycleInterface{

  static MainDelegate _instance;

  MainDelegate._();

  static MainDelegate getInstance() {
    if (_instance == null) {
      _instance = new MainDelegate._();
    }
    return _instance;
  }

  static void initWithCondition() async {
    /// 上传ShareSdk隐私协议
    SharesdkPlugin.uploadPrivacyPermissionStatus("main_delegate", 1, (result){});
  }

  @override
  initState() async {
    ///初始化中文
    S.load(Locale('zh', 'CN'));
    ///获取相机配置
    camerasList = await availableCameras();
    // await WebSocketUtil.setDeviceId();
    ///设置OSS上传位置文件夹
    VgOssUtils.FOLDER_IMG = ConstantRepository.of().getOssImageFolder();
    VgOssUtils.FOLDER_VIDEO = ConstantRepository.of().getOssVideoFolder();
    ///android状态栏设置透明
    // if (Platform.isAndroid) {
    //   // 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    //   SystemUiOverlayStyle systemUiOverlayStyle =
    //   SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    //   SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // }
    /// 强制竖屏
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);
    ///设置Android高刷
    VgToolUtils.proccessRefreshMode();
    // ///加载城市区号文件
    // LocationRepository.getInstance().init();
    //监听全局authid失效
    FlutterGlobalMonitoringUtils.init();
    // SharePreferenceUtil.getBool(SP_FIRST_INSTALL).then((value) {
    //   if (value != null) initWithCondition();
    // });
  }

  @override
  dispose() {
    UserRepository.getInstance().dispose();
    LocationRepository.getInstance().dispose();
    UpgradeRepository.getLastInstantce()?.dispose();
  }
}


import 'package:e_reception_flutter/net/server_api.dart';

class NetApi {

  ///首页获取企业信息
  static const String HOME_COMPANY_BASE_INFO = ServerApi.BASE_URL + "app/appHomeBaseInfo";
  ///首页打卡以及终端信息
  static const String HOME_PUNCH_AND_TERMINAL_INFO = ServerApi.BASE_URL + "app/appHomeInfo";

  ///更多刷脸记录
  static const String CHECK_IN_REC_BY_MONTH =
     ServerApi.BASE_URL + "app/appMonthPunchRecord";

  ///考勤规则设置接口（新增、编辑、删除）
  static const String SAVE_CHECK_IN_RULE =
     ServerApi.BASE_URL + "app/appSaveAttendanceRules";

  ///每日刷脸记录
  static const String DAILY_PUNCH_RECORD = ServerApi.BASE_URL+"app/appDayPunchRecord";

  ///企业主页 管理员列表
  static const String MANAGER_LIST_PAGER = ServerApi.BASE_URL+"app/appCompanyAdminList";

  ///企业主页 未激活列表
  static const String UN_ACTIVE_PERSON_LIST_PAGER = ServerApi.BASE_URL+"app/appCompanyUnActiveList";

  ///企业主页 选择员工列表
  static const String ALL_MEMBER_LIST = ServerApi.BASE_URL+"app/appCompanyPages?searchOthers=&isVisitor=01&visitorGroupId=&groupid=";

  ///企业主页 选择非管理员的员工列表
  static const String EXCEPT_ADMIN_MEMBER_LIST = ServerApi.BASE_URL+"app/appCompanyPages?searchOthers=03&isVisitor=01&visitorGroupId=&groupid=";

  ///企业主页 部门/班级列表
  static const String DEPARTMENT_OR_CLASS_LIST = ServerApi.BASE_URL+"app/appCompanyDepartmentClassList";

  ///企业主页 部门/班级筛选列表
  static const String SELECT_DEPARTMENT_OR_CLASS_LIST = ServerApi.BASE_URL+"app/departmentAndClassList";

  ///企业主页 部门/班级 详情
  static const String DEPARTMENT_OR_CLASS_DETAIL = ServerApi.BASE_URL+"app/appCompanyDepartmentClassDetail";

  ///企业主页 部门/班级 用户校验
  static const String DEPARTMENT_OR_CLASS_USER_CHECK = ServerApi.BASE_URL+"app/appDepartClaCheckName";

  ///考勤规则 添加个性化规则
  static const String ADD_CUSTOMIZE_PUNCH_IN_RULE = ServerApi.BASE_URL+"app/appAddPersonalRule";

  ///考勤规则 企业考勤规则列表
  static const String RULE_LIST = ServerApi.BASE_URL+"app/appAttRuleList";

  ///考勤规则 编辑通用规则
  static const String Edit_GENERAL_PUNCH_IN_RULE = ServerApi.BASE_URL+"app/appEditCurrencyRule";

  ///考勤规则 添加个性化规则
  static const String EDIT_CUSTOMIZE_PUNCH_IN_RULE = ServerApi.BASE_URL+"app/appEditPersonalRule";

  ///考勤规则 删除个性化规则
  static const String DEL_CUSTOMIZE_PUNCH_IN_RULE = ServerApi.BASE_URL+"app/appRemovePersonalRule";

  ///考勤规则 查询某月有无考勤规则
  static const String APP_HAVE_RULE_IN_MONTH = ServerApi.BASE_URL+"app/appHaveRuleInMonth";

  ///考勤规则 查询无考勤规则某月数据
  static const String APP_NO_RULE_PUNCH_IN_MONTH_DATA = ServerApi.BASE_URL+"app/appNoRuleMonthRecord";

  ///考勤规则 查询有考勤规则某月数据
  static const String APP_HAS_RULE_PUNCH_IN_MONTH_DATA = ServerApi.BASE_URL+"app/appHaveRuleMonthRecord";

  ///部门/班级中添加管理员选择列表
  static const String CLASS_DEPARTMENT_ADD_ADMIN_LIST =
      ServerApi.BASE_URL + "app/appPDAddAdminList";

  ///部门/班级中添加管理员
  static const String CLASS_DEPARTMENT_ADD_ADMIN =
      ServerApi.BASE_URL + "app/appPDAddAdmin";

  ///App查询终端获取资源是否成功
  static const String UPDATE_TERMINAL_PLAY_LIST_STATUS_API =
      ServerApi.BASE_URL + "app/appGetObtainflg";

  ///导出excel接口
  static const String EXPORT_EXCEL =
      ServerApi.BASE_URL + "app/attRecordExcelSendEmail";

  ///分支机构
  static const String ADDRESS_LIST = ServerApi.BASE_URL + "app/appComBranchList";

  ///播放列表接口
  // static const String TERMINAL_PLAY_LIST_API = ServerApi.BASE_URL + "app/appGetComAttPicListByhsn";
  static const String TERMINAL_PLAY_LIST_API = ServerApi.BASE_URL + "app/appNewGetComAttPicListByhsn";

  ///查询水牌模板
  static const String BUILDING_INDEX_MODULE_LIST = ServerApi.BASE_URL + "app/appGetSysWBrandList";
  ///查询已制作水牌列表
  static const String BUILDING_INDEX_DIY_LIST = ServerApi.BASE_URL + "app/appGetComWBrandList";
  ///水牌模板设置
  static const String SET_BUILDING_INDEX_MODULE_TYPE = ServerApi.BASE_URL + "app/appUpdateComWbflg";
  ///创建水牌
  static const String CREATE_BUILDING_INDEX = ServerApi.BASE_URL + "app/appSaveComWBrand";
  ///已制作水牌详情
  static const String BUILDING_INDEX_DETAIL = ServerApi.BASE_URL + "app/appGetComWBrandInfo";
  ///修改水牌
  static const String EDIT_BUILDING_INDEX = ServerApi.BASE_URL + "app/appUpdateComWBrand";
  ///上传水牌截图
  static const String UPDATE_BUILDING_INDEX_PIC = ServerApi.BASE_URL + "app/appUpdateComWBrandPic";
  ///添加企业水牌企业
  static const String ADD_BUILDING_INDEX_FLOOR_COMPANY = ServerApi.BASE_URL + "app/appAddComWBrandCom";
  ///编辑企业水牌企业
  static const String EDIT_BUILDING_INDEX_FLOOR_COMPANY = ServerApi.BASE_URL + "app/appUpdateComWBrandCom";
  ///删除水牌中的企业
  static const String DELETE_BUILDING_INDEX_FLOOR_COMPANY = ServerApi.BASE_URL + "app/appDelComWBrandCom";
  ///删除diy水牌
  static const String DELETE_BUILDING_INDEX = ServerApi.BASE_URL + "app/appDelComWBrand";
  ///更换模板
  static const String BUILDING_INDEX_CHANGE_MODULE = ServerApi.BASE_URL + "app/appUpdateComWBrandHTML";
  ///水牌投放至终端
  static const String BUILDING_INDEX_SET_TERMINAL = ServerApi.BASE_URL + "app/appPushComWBrand";
  ///App水牌终端列表
  static const String BUILDING_INDEX_GET_TERMINAL = ServerApi.BASE_URL + "app/appGetComWBrandhsnList";
  ///删除今日推送海报
  static const String DELETE_TODAY_PUSH_POSTER = ServerApi.BASE_URL + "app/appDelPushPic";
  ///删除资料库推送海报
  static const String DELETE_MEDIA_LIBRARY_PUSH_POSTER = ServerApi.BASE_URL + "app/appDelComPushPic";
  ///删除终端使用的楼层指示牌
  static const String DELETE_TERMINAL_BUILDING_INDEX = ServerApi.BASE_URL + "app/appDelComWBrandByhsn";
  ///永久删除终端使用的楼层指示牌
  static const String FOREVER_DELETE_TERMINAL_BUILDING_INDEX = ServerApi.BASE_URL + "app/appDelComWBrandBycomwid";
  ///重复、取消重复播放
  static const String REPEAT_BUILDING_INDEX = ServerApi.BASE_URL + "app/appStopComWBrand";
  ///批量操作水牌企业
  static const String EDIT_BUILDING_INDEX_ALL_COMPANY = ServerApi.BASE_URL + "app/appPLAddComWBrandCom";
  ///按楼层操作水牌企业
  static const String EDIT_BUILDING_INDEX_COMPANY_BY_FLOOR = ServerApi.BASE_URL + "app/appPLAddComWBrandComByFloor";
  ///删除diy就海报
  static const String DELETE_DIY_POSTER = ServerApi.BASE_URL + "app/appDelDIYPicLog";
  ///变化海报拼合地址状态
  static const String UPDATE_SPLICING_POSTER_STATUS = ServerApi.BASE_URL + "app/appChooseSplicing";
  ///录入sim
  static const String SAVE_SIM_INFO = ServerApi.BASE_URL + "app/saveSIMCardInfo";
  ///编辑sim
  static const String EDIT_SIM_INFO = ServerApi.BASE_URL + "app/editSIMCardInfo";
  ///查询sim卡信息
  static const String GET_SIM_INFO = ServerApi.BASE_URL + "app/queryOneCard";
  ///运营商列表，代理商列表，套餐列表
  static const String GET_SIM_INFO_LIST = ServerApi.BASE_URL + "app/SIMInfoList";
  ///新增运营商列表，代理商列表，套餐
  static const String ADD_SIM_INFO_LIST = ServerApi.BASE_URL + "app/addSIMInfoList";
  ///新增智能家居门店
  static const String ADD_SMART_HOME_STORE = ServerApi.BASE_URL + "app/appAddSmartHomeStore";
  ///编辑门店
  static const String EDIT_SMART_HOME_STORE = ServerApi.BASE_URL + "app/appEditSmartHomeStore";
  ///获取智能家居门店列表
  static const String GET_SMART_HOME_LIST = ServerApi.BASE_URL + "app/appSmartHomeStoreList";
  ///新增门店图片
  static const String ADD_STORE_PIC = ServerApi.BASE_URL + "app/appAddStorePic";
  ///新增智能家居门店
  static const String ADD_STORE_CATEGORY = ServerApi.BASE_URL + "app/appEditSpatialType";
  ///查询空间分类
  static const String GET_CATEGORY_LIST = ServerApi.BASE_URL + "app/appFindSpatialType";
  ///分类排序
  static const String MODIFY_CATEGORY_ORDER = ServerApi.BASE_URL + "app/appSpatialTypeSort";
  ///查询门店详情
  static const String GET_STORE_DETAIL = ServerApi.BASE_URL + "app/appSmartHomeStoreDetails";
  ///查询门店图片
  static const String GET_STORE_ONE_PIC_DETAIL = ServerApi.BASE_URL + "app/appStorePicInfo";
  ///删除门店图片
  static const String DELETE_STORE_PIC = ServerApi.BASE_URL + "app/appDelStorePic";
  ///编辑图片
  static const String EDIT_IMAGE = ServerApi.BASE_URL + "app/appEditStorePic";
  ///查询投屏终端
  static const String SMART_HOME_GET_TERMINAL = ServerApi.BASE_URL + "app/appFindStoreTer";
  ///智能家居投放至终端
  static const String SMART_HOME_SET_TERMINAL = ServerApi.BASE_URL + "app/appAddStoreToTer";
  ///智能家居投放至终端-新
  static const String SMART_HOME_SET_TERMINAL_NEW = ServerApi.BASE_URL + "app/appSetStoreToTer";
  ///查询制作中心使用情况
  static const String GET_DIY_CENTER_INFO = ServerApi.BASE_URL + "app/appApplicationInfo";
  ///编辑行业应用开关
  static const String EDIT_BUSINESS_APPLICATION_INFO = ServerApi.BASE_URL + "app/appEditApplicationInfo";
  ///智能家居设置重复播放
  static const String REPEAT_SMART_HOME = ServerApi.BASE_URL + "app/appTerRePlaySmartHome";

  ///获取背景音乐列表
  static const String GET_BG_MUSIC_LIST = ServerApi.BASE_URL + "app/appGetMusicList";
  ///查询系统音乐列表
  static const String GET_SYS_MUSIC_LIST = ServerApi.BASE_URL + "app/appGetSysMusic";
  ///系统音乐添加到终端
  static const String SET_BG_MUSIC = ServerApi.BASE_URL + "app/appSetMusic";
  ///本地音乐添加到终端
  static const String UPLOAD_BG_MUSIC = ServerApi.BASE_URL + "app/appUploadMusic";
  ///设置终端的背景音乐开启或者关闭
  static const String OPEN_OR_CLOSE_MUSIC = ServerApi.BASE_URL + "app/appSetMusicflg";
  ///终端移除背景音乐
  static const String DELETE_BG_MUSIC = ServerApi.BASE_URL + "app/appDelMusicByhsn";
  ///背景音乐排序
  static const String MODIFY_BG_MUSIC_ORDER = ServerApi.BASE_URL + "app/appOrderMusicByhsn";
  ///设置/取消 特别关注
  static const String SPECIAL_FOCUS_FACE_USER_API = ServerApi.BASE_URL + "app/appSpecialFocusFaceUser";
  ///删除服务器端文件
  static const String DELETE_OSS_FILE = ServerApi.BASE_URL + "app/ossFileDelete";
  ///删除服务端视频
  static const String DELETE_VOD_VIDEO_FILE = ServerApi.BASE_URL + "aliyunvod/deleteVideo";
  ///智能家居水印列表
  static const String SMART_HOME_WATER_LIST = ServerApi.BASE_URL+"app/appSmartHomeWatermarkList";
  ///记录智能家居最后所在的门店
  static const String RECORD_LAST_VISIT_SMART_HOME = ServerApi.BASE_URL+"app/appUpdateLastShid";
  ///门店设备列表
  static const String SMART_HOME_STORE_AND_TERMINAL_LIST = ServerApi.BASE_URL+"app/appSHStoreTerList";
  ///门店简约列表
  static const String SMART_HOME_SIMPLE_STORE_LIST = ServerApi.BASE_URL+"app/appSimpleStoreList";
  ///智能家居绑定终端
  static const String SMART_HOME_BIND_TERMINAL = ServerApi.BASE_URL+"app/appStoreBindNewTer";
  ///终端更换所属门店---终端更换智能家居
  static const String TERMINAL_CHANGE_SMART_HOME = ServerApi.BASE_URL+"app/appStoreChangeHsn";

  ///智能家居新增栏目
  static const String SMART_HOME_ADD_EDIT_DELETE_COLUMN = ServerApi.BASE_URL + "app/appEditSmartHomeColumn";
  ///栏目排序
  static const String MODIFY_COLUMN_ORDER = ServerApi.BASE_URL + "app/appSmartHomeColumnSort";
  ///查询栏目信息 老接口 appFindBaseAndColumnInfo
  static const String SMART_HOME_GET_COLUMN_LIST = ServerApi.BASE_URL + "app/appNewFindBaseAndColumnInfo";
  ///智能家居发布动态 appReleaseStorePic
  static const String SMART_HOME_PUBLISH = ServerApi.BASE_URL + "app/appReleaseStorePicPackage";
  ///智能家居编辑动态 appSmartHomeEditFolder
  static const String SMART_HOME_EDIT_FOLDER = ServerApi.BASE_URL + "app/appSmartHomeEditGroup";
  ///查询全屋分类的门店图片列表 appSmartHomeColumnDetails
  static const String GET_STORE_ALL_COLUMN_IMAGE_LIST = ServerApi.BASE_URL + "app/appNewSmartHomeColumnDetails";
  ///查询除全屋外其他分类的门店图片列表 appSmartHomeColumnDetails
  static const String GET_STORE_COLUMN_IMAGE_LIST_BY_TYPE = ServerApi.BASE_URL + "app/appNewSmartHomeOtherColumnDetails";
  ///查询动态or文件详情
  static const String GET_SMART_HOME_FOLDER_DETAILS = ServerApi.BASE_URL + "app/appSmartHomeFolderDetails";
  ///删除动态or文件夹
  static const String GET_SMART_HOME_DELETE_FOLDER = ServerApi.BASE_URL + "app/appRemoveSmartHomeFolder";
  ///App更新资料库、智能家居播放切换时间
  static const String UPDATE_MEDIA_CHANGE_INTERVAL =ServerApi.BASE_URL + "app/appUpdateCpsecond";
  ///编辑封面
  static const String EDIT_SMART_HOME_FOLDER_COVER = ServerApi.BASE_URL + "app/appSmartHomeEditFolderCover";
  ///发布门店动态-独立上传
  static const String SMART_HOME_PUBLISH_SINGLE_UPLOAD = ServerApi.BASE_URL + "app/appReleaseStorePicAlone";
  ///发布门店动态-独立上传编辑
  static const String SMART_HOME_EDIT_SINGLE_UPLOAD = ServerApi.BASE_URL + "app/appSmartHomeEditAlone";
  ///发布门店动态-打包上传
  static const String SMART_HOME_PUBLISH_PACKAGE_UPLOAD = ServerApi.BASE_URL + "app/appReleaseStorePicPackage";
  ///发布门店动态-打包上传编辑
  static const String SMART_HOME_EDIT_PACKAGE_UPLOAD = ServerApi.BASE_URL + "app/appSmartHomeEditGroup";
  ///获取类目、空间、风格
  static const String SMART_HOME_GET_COLUMN_CATEGORY_STYLE_DATA = ServerApi.BASE_URL + "shApplet/findColumnSpatialStyle";
}

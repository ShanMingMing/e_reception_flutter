import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_select_item_page/common_select_item_page.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/ui/test/test_list/test_list_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:flutter/material.dart';

/// 通用选中列表页
///
/// @author: zengxiangxi
/// @createTime: 1/25/21 6:20 PM
/// @specialDemand:
class TestCommonSelectListPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "TestCommonSelectListPage";

  @override
  _TestCommonSelectListPageState createState() =>
      _TestCommonSelectListPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      TestCommonSelectListPage(),
      routeName: TestCommonSelectListPage.ROUTER,
    );
  }
}

class _TestCommonSelectListPageState extends State<TestCommonSelectListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CommonListPageWidget<TestListItemBean, TestListResponseBean>(
          queryMapFunc:(int page)=> {
            "authId": "6563e51841517ddc4d323794b3c27074",
            "current": 1,
            "size": 20,
            "groupid": "11e2bec33bf743aaae4522b51c022f32",
            "hsn": "",
            "isVisitor": "00"
          },
          netUrl:ServerApi.BASE_URL + "app/appTodayGroupList",
          httpType: VgHttpType.post,
          parseDataFunc: (resp) => TestListResponseBean.fromMap(resp?.data),
          customListBuilder: (List data){
            return _toGridWidget(data);
          },
          itemBuilder:
              (BuildContext context, int index, TestListItemBean itemBean) {
            return Container(
              height: 70,
              color: Colors.black,
              child: Center(
                child: Text(
                  itemBean?.name ?? "",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _toGridWidget(List data) {
    return GridView.builder(
        padding: EdgeInsets.only(left: 15, right: 15, top: 3, bottom: 15),
        itemCount: data?.length ?? 0,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          childAspectRatio: 1 / 1,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(color: Colors.green,child: Center(child:  Text(
                    "${index}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),));
        });
  }
}

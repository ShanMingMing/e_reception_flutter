import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"groupPerson":"","todayPerson":0,"page":{"records":[{"fid":"78317ee824ab45829debae50c05160e3","mintime":1611557435,"temperature":"0.0","maxtime":1611557435,"isSign":"00","name":"用户4637","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611557783512-Unknown_84a6c39c-243d-4787-931a-5e98c1d112e6_.jpg"},{"fid":"d14dec83c8fa440a80cb24fb7f67b154","mintime":1611569134,"temperature":"0.0","maxtime":1611569134,"isSign":"00","name":"用户0677","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611569134217-Unknown_0b8e32b9-3186-4d10-a220-04a490728f6e_.jpg"},{"fid":"d822de5388ed46ca982f086acec364da","mintime":1611559145,"temperature":"0.0","maxtime":1611559145,"isSign":"00","name":"用户8528","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg"},{"fid":"752a91176dd64619901d96c547bba22b","mintime":1611553699,"temperature":"0.0","signFuid":"fd35ca9dee0742a0a21c2a0039698c8e","maxtime":1611553699,"isSign":"","name":"","napicurl":""}],"total":4,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1},"todayRecordCnt":""}
/// extra : null

class TestListResponseBean extends BasePagerBean<TestListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static TestListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestListResponseBean testListResponseBeanBean = TestListResponseBean();
    testListResponseBeanBean.success = map['success'];
    testListResponseBeanBean.code = map['code'];
    testListResponseBeanBean.msg = map['msg'];
    testListResponseBeanBean.data = DataBean.fromMap(map['data']);
    testListResponseBeanBean.extra = map['extra'];
    return testListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<TestListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// groupPerson : ""
/// todayPerson : 0
/// page : {"records":[{"fid":"78317ee824ab45829debae50c05160e3","mintime":1611557435,"temperature":"0.0","maxtime":1611557435,"isSign":"00","name":"用户4637","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611557783512-Unknown_84a6c39c-243d-4787-931a-5e98c1d112e6_.jpg"},{"fid":"d14dec83c8fa440a80cb24fb7f67b154","mintime":1611569134,"temperature":"0.0","maxtime":1611569134,"isSign":"00","name":"用户0677","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611569134217-Unknown_0b8e32b9-3186-4d10-a220-04a490728f6e_.jpg"},{"fid":"d822de5388ed46ca982f086acec364da","mintime":1611559145,"temperature":"0.0","maxtime":1611559145,"isSign":"00","name":"用户8528","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg"},{"fid":"752a91176dd64619901d96c547bba22b","mintime":1611553699,"temperature":"0.0","signFuid":"fd35ca9dee0742a0a21c2a0039698c8e","maxtime":1611553699,"isSign":"","name":"","napicurl":""}],"total":4,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}
/// todayRecordCnt : ""

class DataBean {
  String groupPerson;
  int todayPerson;
  PageBean page;
  String todayRecordCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.groupPerson = map['groupPerson'];
    dataBean.todayPerson = map['todayPerson'];
    dataBean.page = PageBean.fromMap(map['page']);
    dataBean.todayRecordCnt = map['todayRecordCnt'];
    return dataBean;
  }

  Map toJson() => {
    "groupPerson": groupPerson,
    "todayPerson": todayPerson,
    "page": page,
    "todayRecordCnt": todayRecordCnt,
  };
}

/// records : [{"fid":"78317ee824ab45829debae50c05160e3","mintime":1611557435,"temperature":"0.0","maxtime":1611557435,"isSign":"00","name":"用户4637","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611557783512-Unknown_84a6c39c-243d-4787-931a-5e98c1d112e6_.jpg"},{"fid":"d14dec83c8fa440a80cb24fb7f67b154","mintime":1611569134,"temperature":"0.0","maxtime":1611569134,"isSign":"00","name":"用户0677","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611569134217-Unknown_0b8e32b9-3186-4d10-a220-04a490728f6e_.jpg"},{"fid":"d822de5388ed46ca982f086acec364da","mintime":1611559145,"temperature":"0.0","maxtime":1611559145,"isSign":"00","name":"用户8528","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg"},{"fid":"752a91176dd64619901d96c547bba22b","mintime":1611553699,"temperature":"0.0","signFuid":"fd35ca9dee0742a0a21c2a0039698c8e","maxtime":1611553699,"isSign":"","name":"","napicurl":""}]
/// total : 4
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<TestListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => TestListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// fid : "78317ee824ab45829debae50c05160e3"
/// mintime : 1611557435
/// temperature : "0.0"
/// maxtime : 1611557435
/// isSign : "00"
/// name : "用户4637"
/// napicurl : "http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611557783512-Unknown_84a6c39c-243d-4787-931a-5e98c1d112e6_.jpg"

class TestListItemBean {
  String fid;
  int mintime;
  String temperature;
  int maxtime;
  String isSign;
  String name;
  String napicurl;

  static TestListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestListItemBean recordsBean = TestListItemBean();
    recordsBean.fid = map['fid'];
    recordsBean.mintime = map['mintime'];
    recordsBean.temperature = map['temperature'];
    recordsBean.maxtime = map['maxtime'];
    recordsBean.isSign = map['isSign'];
    recordsBean.name = map['name'];
    recordsBean.napicurl = map['napicurl'];
    return recordsBean;
  }

  Map toJson() => {
    "fid": fid,
    "mintime": mintime,
    "temperature": temperature,
    "maxtime": maxtime,
    "isSign": isSign,
    "name": name,
    "napicurl": napicurl,
  };
}
import 'dart:io';

import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_oss_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/material.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_single_upload_oss_utils.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class UploadDemoPage extends StatefulWidget {
  @override
  _UploadDemoPageState createState() => _UploadDemoPageState();
}

class _UploadDemoPageState extends BaseState<UploadDemoPage> {

  String _url;

  ///预览网络图
  String _netUrl;

  String _videoUrl;

  String _netVideoUrl;
  String _netVideoCorverUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: ScreenUtils.getStatusBarH(context),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RaisedButton(
                  onPressed: () async{
                    List<String> list = await MatisseUtil.selectPhoto(context: context,maxSize: 1);
                    if(list == null || list.isEmpty||list.length <1){
                      toast("选择图片失败");
                      return;
                    }
                    _url = list.elementAt(0);
                    setState(() { });
                  },
                  child: Text("选择图片"),
                ),
                SizedBox(
                  width: 20,
                ),
                RaisedButton(
                  onPressed: () async{
                    List<String> list = await MatisseUtil.selectVideo(context: context,maxSize: 1);
                    if(list == null || list.isEmpty||list.length <1){
                      toast("选择视频失败");
                      return;
                    }
                    _videoUrl = list.elementAt(0);
                    setState(() { });
                  },
                  child: Text("选择视频"),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "图片路径：${_url ?? "空"}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),

            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "视频路径：${_videoUrl ?? "空"}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RaisedButton(
                  onPressed: () async{
                    if(StringUtils.isEmpty(_url)){
                      toast("路径为空");
                      return;
                    }
                    VgOssUtils.uploadSingleImage(path: _url,callback: BaseCallback(
                      onSuccess: (val){
                        _netUrl = val;
                        setState(() { });
                      },
                      onError: (msg){
                        toast(msg);
                      }
                    ));

                  },
                  child: Text("上传图片"),
                ),
                SizedBox(width: 20,),
                RaisedButton(
                  onPressed: () async{
                    if(StringUtils.isEmpty(_videoUrl)){
                      toast("路径为空");
                      return;
                    }
                    VgOssUtils.uploadMulVideo(paths: [_videoUrl],callback: BaseCallback(
                        onSuccess: ( val){
                          if(val == null ||val is! OssUploadBean){
                            return;
                          }
                          OssUploadBean result = val;
                          if((result?.imgList?.length ?? 0 ) != 1 || (result.videoList?.length ?? 0) != 1){
                            return;
                          }
                          _netVideoUrl = result?.videoList?.elementAt(0);
                          _netVideoCorverUrl = result?.imgList?.elementAt(0);
                          setState(() { });
                        },
                        onError: (msg){
                          toast(msg);
                        }
                    ));

                  },
                  child: Text("上传视频"),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            GestureDetector(
              onLongPress: (){
                VgStringUtils.setClipboardData(context, _netUrl);
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "图片网络路径：${_netUrl ?? "空"}",
                ),
              ),
            ),
            GestureDetector(
              onLongPress: (){
                VgStringUtils.setClipboardData(context, _netVideoUrl);
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "视频网络路径：${_netVideoUrl ?? "空"}",
                ),
              ),
            ),

            GestureDetector(
              onLongPress: (){
                VgStringUtils.setClipboardData(context, _netVideoCorverUrl);
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "视频封面网络路径：${_netVideoCorverUrl ?? "空"}",
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
             Container(
                       alignment: Alignment.centerLeft,
                       child:  Text(
                         "预览图",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,

                       ),
                     ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child:
              StringUtils.isEmpty(_url)?  Text(
                        "空",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,

                      )
              :Image.file(File(_url)
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child:  Text(
                "网络图",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,

              ),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: VgCacheNetWorkImage(
                 _netUrl?? "",
              ),
            )
          ],
        ),
      ),
    );
  }


}

import 'dart:async';

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/web_socket_repository/util/web_socket_util.dart';
import 'package:e_reception_flutter/singleton/web_socket_repository/web_socket_respository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:web_socket_channel/io.dart';

/// 测试tab页
///
/// @author: zengxiangxi
/// @createTime: 3/8/21 4:39 PM
/// @specialDemand:
class TestTabPage extends StatefulWidget {
  @override
  _TestTabPageState createState() => _TestTabPageState();
}

class _TestTabPageState extends State<TestTabPage> {

  IOWebSocketChannel channel;
  @override
  void initState() {
    super.initState();


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            RaisedButton(
              child: Text("建立WebSocket连接"),
              onPressed: (){
                _connect();
              },
            ),
            RaisedButton(
              child: Text("监听"),
              // onPressed: _listener
            ),
            RaisedButton(
              child: Text("发送值"),
              onPressed: _send,
            ),
          ],
        ),
      ),
    );
  }

  void _connect(){
    channel =  WebSocketRepository.getInstance().connectAndListener("ws://echo.websocket.org",onData: (val){
     print("打印：$val");
    },onChannel: (channel){
      this.channel = channel;
    });
  }

  // void _listener(){
  //  channel.stream.listen((event) {
  //     print("打印websocket:$event");
  //   },onError: (e){
  //     print("打印websocket onError: ${e}");
  //   },onDone: (){
  //     print("打印websocket onDone ");
  //   })..onError((e,r){
  //     print("打印websocket big: ${e}");
  //
  //   });
  // }

  void _send(){
    channel.sink.add("1");
  }
}

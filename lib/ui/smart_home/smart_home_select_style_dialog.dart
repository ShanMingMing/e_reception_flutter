import 'dart:core';
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_style_type_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 智能家居某个详情选择风格弹窗
class SmartHomeSelectStyleDialog extends StatefulWidget {
  final List<SmartHomeStyleTypeBean> styleList;
  final List<String> selectStyleList;

  final double offset;
  final Function(List<String> selectStyleList) onSelect;

  const SmartHomeSelectStyleDialog({Key key, this.styleList, this.selectStyleList, this.offset, this.onSelect}) : super(key: key);

  @override
  _SmartHomeSelectStyleDialogState createState() =>
      _SmartHomeSelectStyleDialogState();

  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,
      List<SmartHomeStyleTypeBean> styleList,
      List<String> selectStyleList, double offset, Function(List<String>) onSelect) {
    return VgDialogUtils.showCommonDialog<String>(
      barrierDismissible: true,
      context: context,
      barrierColor: Colors.white.withOpacity(0),
      needSafeArea: false,
      child: SmartHomeSelectStyleDialog(
        styleList: styleList,
        selectStyleList: selectStyleList,
        offset: offset,
        onSelect: onSelect,
      ),
    );
  }

}

class _SmartHomeSelectStyleDialogState extends BaseState<SmartHomeSelectStyleDialog>{
  List<SmartHomeStyleTypeBean> _styleList = new List();
  List<String> _selectStyleList;
  @override
  void initState() {
    super.initState();
    _selectStyleList = new List();
    if(widget?.selectStyleList != null){
      _selectStyleList.addAll(widget?.selectStyleList);
    }
    if(widget?.styleList != null){
      _styleList.addAll(widget?.styleList);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 40 + widget?.offset),
      decoration: BoxDecoration(
        color: Color(0xFF000000).withOpacity(0.4),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
              color: Color(0xFFF6F7F9),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _toStyleGridWidget(),
                _toSelectStyleWidget(),
              ],
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                RouterUtils.pop(context);
              },
              child: Container(
                // color: Color(0xFF000000).withOpacity(0.4),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///风格网格布局
  Widget _toStyleGridWidget(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          bottom: 12
      ),
      itemCount: _styleList?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        crossAxisSpacing: 8,
        mainAxisSpacing: 9,
        childAspectRatio: 80 / 32,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toStyleGridItemWidget(_styleList?.elementAt(index));
      },
    );
  }
  ///网格item
  Widget _toStyleGridItemWidget(SmartHomeStyleTypeBean styleTypeBean){
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: _selectStyleList?.contains(styleTypeBean?.style),
      width: 80,
      height: 32,
      radius: BorderRadius.circular(4),
      unSelectBgColor: Colors.white,
      selectedBgColor: Color(0xFFE7F3FF),
      selectedBoxBorder: Border.all(
          color: ThemeRepository.getInstance()
              .getPrimaryColor_1890FF(),
          width: 0.5),
      unSelectTextStyle: TextStyle(
        color: VgColors.INPUT_BG_COLOR,
        fontSize: 12,
      ),
      selectedTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getPrimaryColor_1890FF(),
        fontSize: 12,
      ),
      text: styleTypeBean?.name??"-",
      onTap: (){
        setState(() {
          if(_selectStyleList.contains(styleTypeBean?.style)){
            _selectStyleList.remove(styleTypeBean?.style);
          }else{
            _selectStyleList.add(styleTypeBean?.style);
          }
        });
      },
    );
  }
  ///筛选网格功能
  Widget _toSelectStyleWidget(){
    return Container(
      height: 70,
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15, right: 15),
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                setState(() {
                  _selectStyleList.clear();
                  widget?.onSelect?.call(_selectStyleList);
                  RouterUtils.pop(context);
                });
              },
              child: Container(
                height: 40,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white
                ),
                child: Text(
                  "重置",
                  style: TextStyle(
                      fontSize: 15,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.normal,
                      color: ThemeRepository.getInstance().getCardBgColor_21263C()
                  ),
                ),
              ),
            ),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                widget?.onSelect?.call(_selectStyleList);
                RouterUtils.pop(context);
              },
              child: Container(
                height: 40,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                ),
                child: Text(
                  "完成",
                  style: TextStyle(
                      fontSize: 15,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.normal,
                      color: Colors.white
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


}

import 'dart:async';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 智能家居文件播放模式页面
class SmartHomeMediaPlayModulePage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomeMediaPlayModulePage";
  //品牌或门店信息
  final ComSmarthomeBean infoBean;

  const SmartHomeMediaPlayModulePage(
      {Key key, this.infoBean}) : super(key:key);

  @override
  _SmartHomeMediaPlayModulePageState createState() => _SmartHomeMediaPlayModulePageState();

  static Future<dynamic> navigatorPush(BuildContext context, ComSmarthomeBean infoBean,){
    return RouterUtils.routeForFutureResult(
        context,
        SmartHomeMediaPlayModulePage(
          infoBean: infoBean,
        ),
        routeName: SmartHomeMediaPlayModulePage.ROUTER
    );
  }
}

class _SmartHomeMediaPlayModulePageState
    extends BaseState<SmartHomeMediaPlayModulePage> {

  SmartHomeViewModel _viewModel;
  ComSmarthomeBean _infoBean;
  //00动态模式，01静态模式
  String _type;
  @override
  void initState() {
    super.initState();
    _viewModel = SmartHomeViewModel(this);
    _infoBean = widget?.infoBean;
    _type = _infoBean?.type??"00";
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Color(0xFFF6F7F9),
          body: _toMainColumnWidget(),
        )
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: [
        _toTopBarWidget(),
        _toDynamicWidget(),
        SizedBox(height: 1,),
        _toStaticWidget(),
      ],
    );
  }
  ///页面标题
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "文件播放",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      // rightWidget: _toSaveButtonWidget(),
    );
  }
  ///动态模式
  Widget _toDynamicWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if("00" == _type){
          return;
        }
        setState(() {
          _type = "00";
        });
        _viewModel.changeMediaPlayModule(context, _infoBean?.shid, _type);
      },
      child: Container(
        height: 70,
        alignment: Alignment.centerLeft,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "动态模式",
                      style: TextStyle(
                        color: Color(0XFF21263C),
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(width: 4,),
                    Container(
                      width: 26,
                      height: 15,
                      margin: EdgeInsets.only(top: 4),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Color(0xFFF95355),
                        borderRadius: BorderRadius.circular(2),
                      ),
                      child: Text(
                        "推荐",
                        style: TextStyle(
                            color:Colors.white,
                            fontSize: 10
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 2,),
                Text(
                  "文件在播放时有放大动态效果",
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0XFF8B93A5)
                  ),
                )
              ],
            ),
            Spacer(),
            Visibility(
              visible: "00" == _type,
              child: Image.asset(
                "images/icon_select_smart_home_play_module.png",
                width: 16,
              ),
            )
          ],
        ),
      ),
    );
  }

  ///静态模式
  Widget _toStaticWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if("01" == _type){
          return;
        }
        setState(() {
          _type = "01";
        });
        _viewModel.changeMediaPlayModule(context, _infoBean?.shid, _type);
      },
      child: Container(
        height: 70,
        alignment: Alignment.centerLeft,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "静态模式",
                  style: TextStyle(
                    color: Color(0XFF21263C),
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: 2,),
                Text(
                  "文件在播放时固定不变",
                  style: TextStyle(
                      fontSize: 12,
                      color: Color(0XFF8B93A5)
                  ),
                )
              ],
            ),
            Spacer(),
            Visibility(
              visible: "01" == _type,
              child: Image.asset(
                "images/icon_select_smart_home_play_module.png",
                width: 16,
              ),
            )
          ],
        ),
      ),
    );
  }
}

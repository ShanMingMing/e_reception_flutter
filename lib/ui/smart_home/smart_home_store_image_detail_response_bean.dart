import 'package:flutter/material.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"storePicInfo":{"picurl":"http://etpic.we17.com/test/20220308190952_4338.jpg","backup":"不吃饭减肥减肥减肥减肥减肥减肥就不方便的不对不对恒大华府不符合防火防盗","createtime":1646737794,"spatialname":"综合","picsize":"266*251","picstorage":"12","name":"好的开端开始","style":"06","type":"01"}}
/// extra : null

class SmartHomeStoreImageDetailResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static SmartHomeStoreImageDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreImageDetailResponseBean smartHomeStoreImageDetailResponseBeanBean = SmartHomeStoreImageDetailResponseBean();
    smartHomeStoreImageDetailResponseBeanBean.success = map['success'];
    smartHomeStoreImageDetailResponseBeanBean.code = map['code'];
    smartHomeStoreImageDetailResponseBeanBean.msg = map['msg'];
    smartHomeStoreImageDetailResponseBeanBean.data = DataBean.fromMap(map['data']);
    smartHomeStoreImageDetailResponseBeanBean.extra = map['extra'];
    return smartHomeStoreImageDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// storePicInfo : {"picurl":"http://etpic.we17.com/test/20220308190952_4338.jpg","backup":"不吃饭减肥减肥减肥减肥减肥减肥就不方便的不对不对恒大华府不符合防火防盗","createtime":1646737794,"spatialname":"综合","picsize":"266*251","picstorage":"12","name":"好的开端开始","style":"06","type":"01"}

class DataBean {
  StorePicInfoBean storePicInfo;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.storePicInfo = StorePicInfoBean.fromMap(map['storePicInfo']);
    return dataBean;
  }

  Map toJson() => {
    "storePicInfo": storePicInfo,
  };
}

/// picurl : "http://etpic.we17.com/test/20220308190952_4338.jpg"
/// backup : "不吃饭减肥减肥减肥减肥减肥减肥就不方便的不对不对恒大华府不符合防火防盗"
/// createtime : 1646737794
/// spatialname : "综合"
/// picsize : "266*251"
/// picstorage : "12"
/// name : "好的开端开始"
/// style : "06"
/// type : "01"

class StorePicInfoBean {
  String picurl;
  String backup;
  int createtime;
  String spatialname;
  String stid;
  String picsize;
  String picstorage;
  String name;
  String style;
  String type;
  String videopic;


  String getCategoryName(){
    if(StringUtils.isEmpty(spatialname)){
      return "其他";
    }
    return spatialname;
  }

  static StorePicInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StorePicInfoBean storePicInfoBean = StorePicInfoBean();
    storePicInfoBean.picurl = map['picurl'];
    storePicInfoBean.backup = map['backup'];
    storePicInfoBean.createtime = map['createtime'];
    storePicInfoBean.spatialname = map['spatialname'];
    storePicInfoBean.picsize = map['picsize'];
    storePicInfoBean.picstorage = map['picstorage'];
    storePicInfoBean.name = map['name'];
    storePicInfoBean.style = map['style'];
    storePicInfoBean.type = map['type'];
    storePicInfoBean.stid = map['stid'];
    storePicInfoBean.videopic = map['videopic'];
    return storePicInfoBean;
  }
  String getPicStorageStr(){
    if(StringUtils.isEmpty(picstorage)){
      picstorage = "0";
    }
    double size = double.parse(picstorage);
    if(size <= 1024){
      if(size < 10){
        return size.toStringAsPrecision(1) + "KB";
      }else if(size < 100){
        return size.toStringAsPrecision(2) + "KB";
      }else if(size < 1000){
        return size.toStringAsPrecision(3) + "KB";
      }else {
        return size.toStringAsPrecision(4) + "KB";
      }
    }else if(size > 1024 && size <= 1024*1024){
      return (size/1024).toStringAsPrecision(3) + "MB";
    }else{
      return (size/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return picurl;
  }

  Map toJson() => {
    "picurl": picurl,
    "backup": backup,
    "createtime": createtime,
    "spatialname": spatialname,
    "picsize": picsize,
    "picstorage": picstorage,
    "name": name,
    "style": style,
    "type": type,
    "stid": stid,
    "videopic": videopic,
  };
}
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"orderflg":0,"defaultflg":"00","name":"真实案例","cnt":8,"scid":"6dc8e673582b415d8ce774e3dee0c788"},{"orderflg":1,"defaultflg":"00","name":"设计参考","cnt":0,"scid":"16d3e961758c4eb0be10740838d7e4c8"},{"orderflg":2,"defaultflg":"00","name":"避坑学习","cnt":0,"scid":"69e8733bdf3e481b8012728f9e18ba14"},{"orderflg":3,"defaultflg":"00","name":"营销活动","cnt":0,"scid":"5485625ed621472f891de38e71ed7bf9"},{"orderflg":4,"defaultflg":"00","name":"其他","cnt":0,"scid":"efa28315c5ed4335ab1e842d56814376"}]}
/// extra : null

class ColumnDataResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static ColumnDataResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ColumnDataResponseBean columnDataResponseBeanBean = ColumnDataResponseBean();
    columnDataResponseBeanBean.success = map['success'];
    columnDataResponseBeanBean.code = map['code'];
    columnDataResponseBeanBean.msg = map['msg'];
    columnDataResponseBeanBean.data = DataBean.fromMap(map['data']);
    columnDataResponseBeanBean.extra = map['extra'];
    return columnDataResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"orderflg":0,"defaultflg":"00","name":"真实案例","cnt":8,"scid":"6dc8e673582b415d8ce774e3dee0c788"},{"orderflg":1,"defaultflg":"00","name":"设计参考","cnt":0,"scid":"16d3e961758c4eb0be10740838d7e4c8"},{"orderflg":2,"defaultflg":"00","name":"避坑学习","cnt":0,"scid":"69e8733bdf3e481b8012728f9e18ba14"},{"orderflg":3,"defaultflg":"00","name":"营销活动","cnt":0,"scid":"5485625ed621472f891de38e71ed7bf9"},{"orderflg":4,"defaultflg":"00","name":"其他","cnt":0,"scid":"efa28315c5ed4335ab1e842d56814376"}]

class DataBean {
  List<ColumnListBean> list;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => ColumnListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}
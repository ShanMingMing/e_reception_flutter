import 'dart:async';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_publish_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../smart_home_upload_service.dart';

/// 创建栏目
class  SmartHomePublishSetImageCoverPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomePublishSetImageCoverPage";
  //门店id
  final List<SmartHomeUploadItemBean> uploadList;
  final int selectIndex;
  final bool editCover;

  const SmartHomePublishSetImageCoverPage(
      {Key key, this.uploadList, this.selectIndex, this.editCover}) : super(key:key);

  @override
  _SmartHomePublishSetImageCoverPageState createState() => _SmartHomePublishSetImageCoverPageState();

  static Future<int> navigatorPush(BuildContext context,
      List<SmartHomeUploadItemBean> uploadList, int selectIndex, {bool editCover}){
    return RouterUtils.routeForFutureResult(
        context,
        SmartHomePublishSetImageCoverPage(
          uploadList: uploadList,
          selectIndex: selectIndex,
          editCover: editCover,
        ),
        routeName: SmartHomePublishSetImageCoverPage.ROUTER
    );
  }
}

class _SmartHomePublishSetImageCoverPageState
    extends BaseState<SmartHomePublishSetImageCoverPage> {

  SmartHomePublishViewModel _viewModel;
  int _selectIndex = 0;

  SwiperController _swiperController;

  @override
  void initState() {
    super.initState();
    _viewModel = SmartHomePublishViewModel(this);
    _selectIndex = widget?.selectIndex;
    _swiperController = new SwiperController();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: _toMainColumnWidget(),
        )
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: [
        _toTopBarWidget(),
        Container(height: 10, color: Color(0XFFF6F7F9),),
        _toSelectedWidget(),
        SizedBox(height: 18,),
        Container(
          height: 56,
            child: _toImageListWidget()
        ),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "选择封面",
      backgroundColor: Color(0XFFF6F7F9),
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      rightWidget: _toSaveButtonWidget(),
    );
  }

  Widget _toSaveButtonWidget() {
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: true,
      width: 48,
      height: 24,
      unSelectBgColor:Color(0xFFCFD4DB),
      selectedBgColor:
      ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      unSelectTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 12,
          fontWeight: FontWeight.w600),
      selectedTextStyle: TextStyle(
          color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
      text: "保存",
      onTap: (){
        if(widget?.editCover??false){
          SmartHomeUploadItemBean uploadItemBean = widget?.uploadList?.elementAt(_selectIndex);

          _viewModel.editFolderCover(context, uploadItemBean.getSize(), uploadItemBean.getPicUrl(), uploadItemBean.sfid, uploadItemBean.shid);
        }else{
          RouterUtils.pop(context, result: _selectIndex);
        }
      },
    );
  }

  ///选中的图片
  Widget _toSelectedWidget(){
    return Container(
      height: 470,
      width: ScreenUtils.screenW(context),
      padding: EdgeInsets.only(left: 15, right: 15, top: 10),
      color: Color(0XFFF6F7F9),
      child: Swiper(
        loop: true,
        index: _selectIndex,
        controller: _swiperController,
        itemBuilder: (BuildContext context, int index) {
          return _toSwiperItemWidget(index);
        },
        itemCount: widget?.uploadList?.length,
        onIndexChanged: (index){
          setState(() {
            _selectIndex = index;
          });
        },
      ),
    );
  }

  Widget _toSwiperItemWidget(int index){
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: VgCacheNetWorkImage(
          widget?.uploadList?.elementAt(index)?.getPicOrVideoUrl(),
          fit: BoxFit.contain,
        ),
      ),
    );
  }


  ///图片列表
  Widget _toImageListWidget(){
    return ListView.separated(
        padding: EdgeInsets.only(
            left: 15,
            right: 15,
            top: 0,
            bottom: 0
        ),
        scrollDirection: Axis.horizontal,
        itemCount: widget?.uploadList?.length ?? 0,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(index, widget?.uploadList?.elementAt(index));
        },
        separatorBuilder: (BuildContext context, int index) {
          if(index == _selectIndex || index == _selectIndex-1){
            return SizedBox(width: 4);
          }else{
            return SizedBox(width: 8);
          }
        });
  }

  Widget _toListItemWidget(int index, SmartHomeUploadItemBean item){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        _swiperController.move(index,animation: false);
        // setState(() {
        //   _selectIndex = index;
        // });

      },
      child: Container(
        width: (index == _selectIndex)?56:48,
        height: (index == _selectIndex)?56:48,
        alignment: Alignment.center,
        foregroundDecoration: BoxDecoration(
          border: Border.all(
              color: (index == _selectIndex)?ThemeRepository.getInstance().getPrimaryColor_1890FF():Colors.transparent,
              width: (index == _selectIndex)? 2:0),
          borderRadius: BorderRadius.circular(4),
          color: Colors.transparent
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Container(
            width: 48,
            height: 48,
            decoration: BoxDecoration(color: Colors.black),
            child: VgCacheNetWorkImage(
              item?.getPicOrVideoUrl() ?? "",
              fit: BoxFit.cover,
              imageQualityType: ImageQualityType.middleDown,
            ),
          ),
        ),
      ),
    );
  }



}

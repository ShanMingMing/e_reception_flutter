import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_folder_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_image_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_terminal_list_response_bean.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';


class SmartHomeStoreDetailAllListViewModel extends BasePagerViewModel<
    SmartHomeStoreDetailListItemBean,
    SmartHomeStoreDetailResponseBean> {

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  //门店id
  String shid;
  //栏目id
  String scid;
  String flg;
  //选样推送 00关闭 01开启
  String samplePush;

  SmartHomeStoreDetailAllListViewModel(BaseState<StatefulWidget> state,
      {String shid, String scid, String flg, String samplePush,}) : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);
    this.shid = shid??"";
    this.scid = scid??"";
    this.flg = flg??"00";
    this.samplePush = samplePush??"00";
  }


  @override
  void onDisposed() {
    // totalValueNotifier?.dispose();
    // statusTypeValueNotifier?.dispose();
  }


  @override
  bool isNeedCache() {
    return true;
  }


  @override
  void refresh() {
    setCacheKey(getCacheKey());
    super.refresh();
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "shid": shid??"",
      "scid": scid??"",//栏目
      "samplePush":samplePush??"00",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getCacheKey() {
    String cacheKey = "${NetApi.GET_STORE_ALL_COLUMN_IMAGE_LIST}${shid??""}${scid??""}";
    print("cacheKey:" + cacheKey);
    return cacheKey;
  }

  @override
  String getUrl() {
      return NetApi.GET_STORE_ALL_COLUMN_IMAGE_LIST;
  }

  @override
  SmartHomeStoreDetailResponseBean parseData(VgHttpResponse resp) {
    SmartHomeStoreDetailResponseBean vo = SmartHomeStoreDetailResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{

      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

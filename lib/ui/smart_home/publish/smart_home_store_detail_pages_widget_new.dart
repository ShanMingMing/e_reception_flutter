import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_store_detail_list_widget_new.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart'
as extended;

class SmartHomeStoreDetailPagesWidgetNew extends StatelessWidget{
  final TabController tabController;
  final List<ColumnListBean> columnList;
  final String shid;
  final String title;
  final String samplePush;
  //风格
  final String styles;
  //类型/空间id
  final String stids;
  //类型/空间名
  final String stnames;
  const SmartHomeStoreDetailPagesWidgetNew({Key key, this.tabController,
    this.columnList, this.shid,this.title, this.samplePush,
    this.styles, this.stids, this.stnames,
  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toPagesWidget();
  }

  Widget _toPagesWidget() {
    return TabBarView(
      controller: tabController,
      physics: BouncingScrollPhysics(),
      children: List.generate(columnList?.length ?? 0, (index) {
        return extended.NestedScrollViewInnerScrollPositionKeyWidget(
            Key(columnList?.elementAt(index)?.scid),
            SmartHomeStoreDetailListWidgetNew(
              shid: shid,
              samplePush: samplePush,
              scid: columnList?.elementAt(index)?.scid,
              column: columnList?.elementAt(index)?.name,
              title: title,
              columnList: columnList,
              styles: styles,
              stnames: stnames,
              stids: stids,
            ));
      }),
    );
  }
}
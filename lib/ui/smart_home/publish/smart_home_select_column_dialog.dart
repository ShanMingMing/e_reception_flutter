import 'dart:core';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_modify_order_page.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_modify_order_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_one_details_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 智能家居某个详情选择图片分类弹窗
class SmartHomeSelectColumnDialog extends StatefulWidget {
  final ColumnListBean selectType;
  final List<ColumnListBean> columnList;
  final String shid;
  final Function(ColumnListBean selectType) onSelect;
  const SmartHomeSelectColumnDialog({Key key, this.selectType, this.columnList, this.shid, this.onSelect}) : super(key: key);

  @override
  _SmartHomeSelectColumnDialogState createState() =>
      _SmartHomeSelectColumnDialogState();

  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,
      ColumnListBean selectType,
      List<ColumnListBean> columnList, String shid, Function(ColumnListBean selectType) onSelect) {
    return VgDialogUtils.showCommonDialog<String>(
      barrierDismissible: true,
      context: context,
      child: SmartHomeSelectColumnDialog(
        selectType: selectType,
        columnList: columnList,
        shid: shid,
        onSelect: onSelect,
      ),
    );
  }

}

class _SmartHomeSelectColumnDialogState extends BaseState<SmartHomeSelectColumnDialog>{
  List<ColumnListBean> _columnList;
  ColumnListBean _selectType;
  @override
  void initState() {
    super.initState();
    _columnList = widget?.columnList;
    if(widget?.selectType == null){
      _selectType = _columnList[0];
    }else{
      _selectType = widget?.selectType;
    }

  }

  @override
  Widget build(BuildContext context) {
    double height = 54+30.0;
    int lines = ((_columnList.length+1)/2).floor();
    int columnHeight = lines*45 + (lines-1)*10;
    height = height + columnHeight;
    double maxHeight = 54 + 30.0+6*45 + 6*10 + 22.5;
    double min = 54 + 30.0+3*45 + 3*10;
    if(height < min){
      height = min;
    }else if(height > maxHeight){
      height = maxHeight;
    }
    // if(_columnList.length < 10){
    //   height = 299;
    // }else{
    //   height = 414;
    // }
    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: height,
            width: 320,
            padding: EdgeInsets.only(bottom: 30),
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _toTitleWidget(),
                _toCategoryWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///标题 管理
  Widget _toTitleWidget(){
    return Container(
      height: 54,
      margin: EdgeInsets.only(left: 20),
      // padding: EdgeInsets.only(bottom: 5),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "选择类别",
            style: TextStyle(
                fontSize: 17,
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                fontWeight: FontWeight.w600
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              RouterUtils.pop(context);
              SmartHomeColumnModifyOrderPage.navigatorPush(context, _columnList, widget?.shid,);
            },
            child: Container(
              height: 60,
              width: 48,
              alignment: Alignment.center,
              child: Text(
                "管理",
                style: TextStyle(
                    fontSize: 12,
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                ),
              ),
            ),
          ),
          Spacer(),
          _toCancelWidget(),
        ],
      ),
    );
  }

  ///分类布局
  Widget _toCategoryWidget(){
    return Expanded(
      child: GridView.builder(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        itemCount: _columnList?.length ?? 0,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          childAspectRatio: 135 / 45,
        ),
        itemBuilder: (BuildContext context, int index) {
          return _toGridItemWidget(context, index, _columnList?.elementAt(index));
        },
      ),
    );
  }

  Widget _toGridItemWidget(BuildContext context, int index, ColumnListBean item) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
          setState(() {
            _selectType = item;
            widget?.onSelect?.call(_selectType);
            RouterUtils.pop(context);
          });
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          padding: EdgeInsets.only(left: 20),
          height: 45,
          width: 135,
          alignment: Alignment.centerLeft,
          color: (_selectType?.scid == item?.scid)?ThemeRepository.getInstance().getPrimaryColor_1890FF():Color(0xFFF6F7F9),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 70,
                child: Text(
                  item?.name??"-",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 14,
                      color: (_selectType?.scid == item?.scid)?Colors.white:ThemeRepository.getInstance().getCardBgColor_21263C()
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 45,
                alignment: Alignment.center,
                child: Text(
                  "${item?.cnt??0}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 14,
                      color: (_selectType?.scid == item?.scid)?Colors.white:Color(0xFF8B93A5)
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }

  ///取消
  Widget _toCancelWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 60,
        width: 55,
        alignment: Alignment.center,
        child: Image.asset(
          "images/login_close_ico.png",
          color: Color(0xFFB0B3BF),
          width: 15,
          height: 15,
        ),
      ),
    );
  }

}

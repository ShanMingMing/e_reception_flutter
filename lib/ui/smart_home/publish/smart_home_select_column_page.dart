import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';
import 'package:e_reception_flutter/ui/test_function/bean/sim_info_list_response_bean.dart';
import 'package:e_reception_flutter/ui/test_function/sim_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SmartHomeSelectColumnPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "SmartHomeSelectColumnPage";

  final List<ColumnListBean> columnList;
  final ColumnListBean currentColumn;

  const SmartHomeSelectColumnPage({Key key, this.columnList, this.currentColumn,}):super(key: key);

  @override
  _SmartHomeSelectColumnPageState createState() => _SmartHomeSelectColumnPageState();

  ///跳转方法
  static Future<ColumnListBean> navigatorPush(BuildContext context,
      List<ColumnListBean> columnList, ColumnListBean currentColumn) {
    return RouterUtils.routeForFutureResult(
      context,
      SmartHomeSelectColumnPage(
        columnList: columnList,
        currentColumn: currentColumn,
      ),
      routeName: SmartHomeSelectColumnPage.ROUTER,
    );
  }
}

class _SmartHomeSelectColumnPageState extends BaseState<SmartHomeSelectColumnPage>{

  List<ColumnListBean> _columnList;

  ColumnListBean _selectColumn;

  @override
  void initState() {
    super.initState();
    _columnList = new List();
    _selectColumn = widget?.currentColumn;
    if(widget?.columnList != null && widget.columnList.isNotEmpty){
      _columnList.addAll(widget?.columnList);
      if(_columnList.elementAt(0).scid == "-1"){
        _columnList.removeAt(0);
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        backgroundColor: Color(0XFFF6F7F9),
        body: _toInfoWidget(),
      ),
    );
  }


  Widget _toInfoWidget(){
    return Column(
      children: [
        _toTopBarWidget(),
        _toListWidget(),
      ],
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "选择类别",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  ///列表
  Widget _toListWidget(){
    return Expanded(
      child: ListView.separated(
          padding: EdgeInsets.all(0),
          itemCount: _columnList?.length??0,
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return _toItemWidget(_columnList[index]);
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox();
          }),
    );
  }

  ///item
  Widget _toItemWidget(ColumnListBean item){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(item?.scid == _selectColumn?.scid){
          return;
        }
        setState(() {
          _selectColumn = item;
        });
        RouterUtils.pop(context, result: _selectColumn);


      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        child: Row(
          children: [
            SizedBox(width: 15,),
            Expanded(
              child: Text(
                item?.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 14,
                  color: (item?.scid == _selectColumn?.scid)?ThemeRepository.getInstance().getPrimaryColor_1890FF():ThemeRepository.getInstance().getCardBgColor_21263C(),
                ),
              ),
            ),
            SizedBox(width: 5,),
            Spacer(),
            Visibility(
              visible: item?.scid == _selectColumn?.scid,
              child: Image.asset(
                "images/icon_selected.png",
                width: 16,
              ),
            ),
            SizedBox(width: 20,)
          ],
        ),
      ),
    );
  }

  ///分割线
  Widget _toFilterWidget(){
    return Container(
      height: 1,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Container(
        height: 1,
        color: Color(0XFF303546),
        margin: EdgeInsets.only(left: 15),
      ),
    );
  }
}
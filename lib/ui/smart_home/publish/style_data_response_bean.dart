import '../smart_home_style_type_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"name":"现代简约","style":"00"},{"name":"中式现代","style":"01"},{"name":"美式田园","style":"02"},{"name":"美式经典","style":"03"},{"name":"欧式豪华","style":"04"},{"name":"北欧极简","style":"05"},{"name":"日式","style":"06"},{"name":"地中海","style":"07"},{"name":"潮流混搭","style":"08"},{"name":"轻奢","style":"09"},{"name":"其他","style":"99"}]}
/// extra : null

class StyleDataResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static StyleDataResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StyleDataResponseBean styleDataResponseBeanBean = StyleDataResponseBean();
    styleDataResponseBeanBean.success = map['success'];
    styleDataResponseBeanBean.code = map['code'];
    styleDataResponseBeanBean.msg = map['msg'];
    styleDataResponseBeanBean.data = DataBean.fromMap(map['data']);
    styleDataResponseBeanBean.extra = map['extra'];
    return styleDataResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"name":"现代简约","style":"00"},{"name":"中式现代","style":"01"},{"name":"美式田园","style":"02"},{"name":"美式经典","style":"03"},{"name":"欧式豪华","style":"04"},{"name":"北欧极简","style":"05"},{"name":"日式","style":"06"},{"name":"地中海","style":"07"},{"name":"潮流混搭","style":"08"},{"name":"轻奢","style":"09"},{"name":"其他","style":"99"}]

class DataBean {
  List<SmartHomeStyleTypeBean> list;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => SmartHomeStyleTypeBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

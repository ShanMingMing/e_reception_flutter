import 'dart:async';
import 'dart:ui';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart' as prefix ;
import 'package:webview_flutter/webview_flutter.dart';

///文件夹、动态详情
class SmartHome3DLinkDemoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHome3DLinkDemoPage";

  final String linkUrl;

  const SmartHome3DLinkDemoPage({
    Key key,
    this.linkUrl,
  }) : super(key: key);

  @override
  _SmartHome3DLinkDemoPageState createState() =>
      _SmartHome3DLinkDemoPageState();

  static Future<dynamic> navigatorPush(BuildContext context,
      String linkUrl) {
    return RouterUtils.routeForFutureResult(
        context,
        SmartHome3DLinkDemoPage(
          linkUrl: linkUrl,
        ),
        routeName: SmartHome3DLinkDemoPage.ROUTER);
  }
}

class _SmartHome3DLinkDemoPageState
    extends BaseState<SmartHome3DLinkDemoPage> {

  //webview加载成功
  bool isUrlLoadComplete = false;

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }


  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: _toMainWidget(),
        ));
  }

  @override
  void dispose() {
    super.dispose();
  }


  
  Widget _toMainWidget() {
    return Stack(
      children: [
        _toContentWidget(),
        _toTopBarWidget(),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget() {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)),
      child: Container(
        height: 44,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              left: 16,
              child: prefix.ClickAnimateWidget(
                child: Container(
                  width: 30,
                  height: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0X66000000),
                  ),
                  child: Image.asset(
                    "images/top_bar_back_ico.png",
                    width: 9,
                    height: 16,
                    color: Colors.white,
                  ),
                ),
                scale: 1.4,
                onClick: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.of(context).maybePop();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }


  ///根据类型判断展示布局
  Widget _toContentWidget(){
      return _to3DLinkWidget();
  }




  WebViewController _webViewController;
  ///链接
  Widget _to3DLinkWidget(){
    return Container(
      width: ScreenUtils.screenW(context),
      height: ScreenUtils.screenH(context),
      color: Colors.white,
      child: Stack(
        children: [
          Opacity(
            opacity: isUrlLoadComplete ? 1 : 0,
            child: WebView(
              initialUrl: widget?.linkUrl,
              javascriptMode: JavascriptMode.unrestricted,
              javascriptChannels: <JavascriptChannel>[
              ].toSet(),
              onPageFinished: (String url) {
                print("加载完成\n" + url);
                if(!(isUrlLoadComplete??false)){
                  setState(() {
                    isUrlLoadComplete = true;
                  });
                }
              },
              onWebViewCreated: (controller) {
                _webViewController = controller;
              },
              onWebResourceError: (error){
                print("加载失败：" + error?.description??"");
                String errorInfo = error?.description??"";
                if(errorInfo.contains("net::ERR_INTERNET_DISCONNECTED")
                    || errorInfo.contains("404")
                    || errorInfo.contains("500")
                    || errorInfo.contains("Error")
                    || errorInfo.contains("找不到网页")
                    || errorInfo.contains("网页无法打开")){

                }
              },
            ),
          ),
          Center(
            child: Visibility(
              visible: !isUrlLoadComplete,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "正在加载",
                    style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getCardBgColor_21263C(),
                        fontSize: 10),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}

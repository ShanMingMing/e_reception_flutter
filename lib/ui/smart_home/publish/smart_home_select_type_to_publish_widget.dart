import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SmartHomeSelectTypeToPublishWidget extends StatelessWidget {

  final VoidCallback onTapImage;
  final VoidCallback onTapVideo;
  final VoidCallback onTap3DLink;
  final double offset;

  const SmartHomeSelectTypeToPublishWidget({Key key,
    @required this.offset,
    @required this.onTapImage,
    @required this.onTapVideo,
    @required this.onTap3DLink,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double itemWidth = (ScreenUtils.screenW(context) - 30)/3;
    return Material(
      color: Colors.transparent,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          RouterUtils.pop(context);
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          child: Stack(
            children: [
              Positioned(
                  top: offset,
                  child: Column(
                    children: [
                      Container(
                        height: 70,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xff000000).withOpacity(0.1),
                                  offset: Offset(0, 0),
                                  blurRadius: 40
                              )
                            ]
                        ),
                        alignment: Alignment.center,
                        child: Row(
                          children: [
                            _toPublishImage(context, itemWidth),
                            _toPublishVideo(context, itemWidth),
                            _toPublish3DLink(context,itemWidth),
                          ],
                        ),
                      ),
                      Image.asset(
                        "images/icon_arrow_down_white.png",
                        width: 11,
                        height: 6,
                      ),
                      SizedBox(height: 10,),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }

  ///发布图片
  Widget _toPublishImage(BuildContext context, double width){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
        onTapImage.call();
      },
      child: Container(
        width: width,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 24,),
            Image.asset(
              "images/icon_publish_image.png",
              width: 30,
              height:30,
            ),
            SizedBox(width: 8,),
            Text(
              "图片",
              style: TextStyle(
                fontSize: 14,
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              ),
            ),
            Spacer(),
            _toSplitWidget(),
          ],
        ),
      ),
    );

  }

  ///发布视频
  Widget _toPublishVideo(BuildContext context, double width){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
        onTapVideo.call();
      },
      child: Container(
        width: width,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: 24,),
            Image.asset(
              "images/icon_publish_video.png",
              width: 30,
              height:30,
            ),
            SizedBox(width: 8,),
            Text(
              "视频",
              style: TextStyle(
                fontSize: 14,
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              ),
            ),
            Spacer(),
            _toSplitWidget(),
          ],
        ),
      ),
    );

  }

  ///发布视频
  Widget _toPublish3DLink(BuildContext context, double width){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
        onTap3DLink.call();
      },
      child: Container(
        width: width,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "images/icon_publish_3d_link.png",
              width: 30,
              height:30,
            ),
            SizedBox(width: 8,),
            Text(
              "3D全景",
              style: TextStyle(
                fontSize: 14,
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              ),
            ),
          ],
        ),
      ),
    );

  }

  Widget _toSplitWidget(){
    return Container(
      height: 30,
      width: 1,
      color: Color(0XFFDDDDDD),
    );
  }

}





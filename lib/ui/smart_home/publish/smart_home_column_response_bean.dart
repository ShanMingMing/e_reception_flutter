import 'dart:convert';

import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"columnList":[{"orderflg":-1,"defaultflg":"00","name":"全部","cnt":30,"stid":"-1"},{"orderflg":0,"defaultflg":"00","name":"真实案例","cnt":0,"scid":"b308d41d8d9a417b9fecb6f2bfa15535"},{"orderflg":1,"defaultflg":"00","name":"设计参考","cnt":0,"scid":"ea11f2cf134740eea250231ba5d39afc"},{"orderflg":2,"defaultflg":"00","name":"避坑学习","cnt":0,"scid":"85d3bae3f8c241f5ab1a952cd3041467"},{"orderflg":3,"defaultflg":"00","name":"营销活动","cnt":0,"scid":"62c29e161896452097a05ba97456a45f"},{"orderflg":4,"defaultflg":"00","name":"其他","cnt":0,"scid":"400509be5c184ce1a80e5b4762cb6b34"},{"orderflg":12,"defaultflg":"01","name":"栏目111","cnt":0,"scid":"684672e76a164841b66ae57b2158c8d5"}],"comSmarthome":{"shid":"ed8ff4ee5d344e228cef0a672bf40c4d","companyid":"f524a02361674e60b2f569d066fc713f","name":"555","brand":"666","logo":"","improveLogo":null,"type":"00","dayflg":"00","watermark":"00","wmid":null,"gps":"","addrProvince":"","addrCity":"","addrDistrict":"","addrCode":"","address":"","createtime":1659346177,"createuserid":"1b14d48f34b143ed90b8fd20e0d47bea","updatetime":1659346177,"updateuid":"1b14d48f34b143ed90b8fd20e0d47bea","delflg":"00","bjtime":"2022-08-01T17:29:37"},"terCnt":0}
/// extra : null

class SmartHomeColumnResponseBean extends BasePagerBean<ColumnListBean>{
  bool success;
  String code;
  String msg;
  ColumnDataBean data;
  dynamic extra;

  static SmartHomeColumnResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeColumnResponseBean smartHomeColumnResponseBeanBean = SmartHomeColumnResponseBean();
    smartHomeColumnResponseBeanBean.success = map['success'];
    smartHomeColumnResponseBeanBean.code = map['code'];
    smartHomeColumnResponseBeanBean.msg = map['msg'];
    smartHomeColumnResponseBeanBean.data = ColumnDataBean.fromMap(map['data']);
    smartHomeColumnResponseBeanBean.extra = map['extra'];
    return smartHomeColumnResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<ColumnListBean> getDataList() => data?.columnList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// columnList : [{"orderflg":-1,"defaultflg":"00","name":"全部","cnt":30,"stid":"-1"},{"orderflg":0,"defaultflg":"00","name":"真实案例","cnt":0,"scid":"b308d41d8d9a417b9fecb6f2bfa15535"},{"orderflg":1,"defaultflg":"00","name":"设计参考","cnt":0,"scid":"ea11f2cf134740eea250231ba5d39afc"},{"orderflg":2,"defaultflg":"00","name":"避坑学习","cnt":0,"scid":"85d3bae3f8c241f5ab1a952cd3041467"},{"orderflg":3,"defaultflg":"00","name":"营销活动","cnt":0,"scid":"62c29e161896452097a05ba97456a45f"},{"orderflg":4,"defaultflg":"00","name":"其他","cnt":0,"scid":"400509be5c184ce1a80e5b4762cb6b34"},{"orderflg":12,"defaultflg":"01","name":"栏目111","cnt":0,"scid":"684672e76a164841b66ae57b2158c8d5"}]
/// comSmarthome : {"shid":"ed8ff4ee5d344e228cef0a672bf40c4d","companyid":"f524a02361674e60b2f569d066fc713f","name":"555","brand":"666","logo":"","improveLogo":null,"type":"00","dayflg":"00","watermark":"00","wmid":null,"gps":"","addrProvince":"","addrCity":"","addrDistrict":"","addrCode":"","address":"","createtime":1659346177,"createuserid":"1b14d48f34b143ed90b8fd20e0d47bea","updatetime":1659346177,"updateuid":"1b14d48f34b143ed90b8fd20e0d47bea","delflg":"00","bjtime":"2022-08-01T17:29:37"}
/// terCnt : 0

class ColumnDataBean {
  List<ColumnListBean> columnList;
  ComSmarthomeBean comSmarthome;
  int terCnt;

  int getPicTotalCnt(){
    if(columnList == null || columnList.length == 0){
      return 0;
    }
    int count = 0;
    columnList.forEach((element) {
      count += (element.cnt??0);
    });
    return count;
  }

  static ColumnDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ColumnDataBean dataBean = ColumnDataBean();
    dataBean.columnList = List()..addAll(
      (map['columnList'] as List ?? []).map((o) => ColumnListBean.fromMap(o))
    );
    dataBean.comSmarthome = ComSmarthomeBean.fromMap(map['comSmarthome']);
    dataBean.terCnt = map['terCnt'];
    return dataBean;
  }

  Map toJson() => {
    "columnList": columnList,
    "comSmarthome": comSmarthome,
    "terCnt": terCnt,
  };


  @override
  String toString() {
    return json.encode(this).toString();
  }
}

/// shid : "ed8ff4ee5d344e228cef0a672bf40c4d"
/// companyid : "f524a02361674e60b2f569d066fc713f"
/// name : "555"
/// brand : "666"
/// logo : ""
/// improveLogo : null
/// type : "00"
/// dayflg : "00"
/// watermark : "00"
/// wmid : null
/// gps : ""
/// addrProvince : ""
/// addrCity : ""
/// addrDistrict : ""
/// addrCode : ""
/// address : ""
/// createtime : 1659346177
/// createuserid : "1b14d48f34b143ed90b8fd20e0d47bea"
/// updatetime : 1659346177
/// updateuid : "1b14d48f34b143ed90b8fd20e0d47bea"
/// delflg : "00"
/// bjtime : "2022-08-01T17:29:37"

class ComSmarthomeBean {
  String shid;
  String companyid;
  String name;
  String brand;
  String logo;
  dynamic improveLogo;
  String type;
  String dayflg;
  String watermark;
  dynamic wmid;
  String gps;
  String addrProvince;
  String addrCity;
  String addrDistrict;
  String addrCode;
  String address;
  int createtime;
  String createuserid;
  int updatetime;
  String updateuid;
  String delflg;
  String bjtime;
  String samplePush;//选样推送 00关闭 01开启


  static ComSmarthomeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComSmarthomeBean comSmarthomeBean = ComSmarthomeBean();
    comSmarthomeBean.shid = map['shid'];
    comSmarthomeBean.companyid = map['companyid'];
    comSmarthomeBean.name = map['name'];
    comSmarthomeBean.brand = map['brand'];
    comSmarthomeBean.logo = map['logo'];
    comSmarthomeBean.improveLogo = map['improveLogo'];
    comSmarthomeBean.type = map['type'];
    comSmarthomeBean.dayflg = map['dayflg'];
    comSmarthomeBean.watermark = map['watermark'];
    comSmarthomeBean.wmid = map['wmid'];
    comSmarthomeBean.gps = map['gps'];
    comSmarthomeBean.addrProvince = map['addrProvince'];
    comSmarthomeBean.addrCity = map['addrCity'];
    comSmarthomeBean.addrDistrict = map['addrDistrict'];
    comSmarthomeBean.addrCode = map['addrCode'];
    comSmarthomeBean.address = map['address'];
    comSmarthomeBean.createtime = map['createtime'];
    comSmarthomeBean.createuserid = map['createuserid'];
    comSmarthomeBean.updatetime = map['updatetime'];
    comSmarthomeBean.updateuid = map['updateuid'];
    comSmarthomeBean.delflg = map['delflg'];
    comSmarthomeBean.bjtime = map['bjtime'];
    comSmarthomeBean.samplePush = map['samplePush'];
    return comSmarthomeBean;
  }

  Map toJson() => {
    "shid": shid,
    "companyid": companyid,
    "name": name,
    "brand": brand,
    "logo": logo,
    "improveLogo": improveLogo,
    "type": type,
    "dayflg": dayflg,
    "watermark": watermark,
    "wmid": wmid,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "addrCode": addrCode,
    "address": address,
    "createtime": createtime,
    "createuserid": createuserid,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "bjtime": bjtime,
    "samplePush": samplePush,
  };
}

/// orderflg : -1
/// defaultflg : "00"
/// name : "全部"
/// cnt : 30
/// stid : "-1"

class ColumnListBean {
  int orderflg;
  String defaultflg;
  String name;
  int cnt;
  String scid;


  ColumnListBean({this.scid});

  String getScid(){
    return scid;
  }

  static ColumnListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ColumnListBean columnListBean = ColumnListBean();
    columnListBean.orderflg = map['orderflg'];
    columnListBean.defaultflg = map['defaultflg'];
    columnListBean.name = map['name'];
    columnListBean.cnt = map['cnt'];
    columnListBean.scid = map['scid'];
    return columnListBean;
  }

  Map toJson() => {
    "orderflg": orderflg,
    "defaultflg": defaultflg,
    "name": name,
    "cnt": cnt,
    "scid": scid,
  };
}
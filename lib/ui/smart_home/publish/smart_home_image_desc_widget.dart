import 'dart:io';

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../smart_home_upload_service.dart';

class SmartHomeImageDescWidget extends StatefulWidget{

  SmartHomeUploadItemBean itemBean;
  double width;
  SmartHomeImageDescWidget({Key key, this.itemBean, this.width}) : super(key: key);

  @override
  _SmartHomeImageDescWidgetState createState() => _SmartHomeImageDescWidgetState();
}

class _SmartHomeImageDescWidgetState extends State<SmartHomeImageDescWidget>{
  TextEditingController _descController;
  @override
  void initState() {
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    _descController = TextEditingController(text: widget?.itemBean?.backup??"");
    _descController.selection = TextSelection.fromPosition(TextPosition(
        affinity: TextAffinity.downstream,
        offset: _descController.text.length));

    return Container(
      width: widget?.width??ScreenUtils.screenW(context) - 112,
      padding: EdgeInsets.only(left: 8, right: 8, top: 6, bottom: 6),
      decoration: BoxDecoration(
        color: Color(0XFFF2F3F4),
        borderRadius: BorderRadius.circular(4),
      ),
      alignment: Alignment.topLeft,
      child: ConstrainedBox(
        constraints: BoxConstraints(
            minHeight: 34,
            maxHeight: 148
        ),
        child: VgTextField(
          controller: _descController,
          keyboardType: TextInputType.text,
          autofocus: false,
          textAlign: TextAlign.left,
          style: TextStyle(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              fontSize: 13
          ),
          decoration: new InputDecoration(
              counterText: "",
              hintText: "描述（选填）",
              isCollapsed: true,
              border: InputBorder.none,
              hintStyle: TextStyle(
                  fontSize: 13,
                  color: Color(0XFFB0B3BF))),
          onChanged: (value){
            widget?.itemBean?.backup = value.trim();
          },
          maxLimitLength: Platform.isAndroid ? 200 : null,
          zhCountEqEnCount: true,
          limitCallback:(int){
            Platform.isAndroid
                ? VgToastUtils.toast(context, "最多可输入${200}个字")
                : {};
          },
          // minLines: 1,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

}
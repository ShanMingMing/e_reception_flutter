import 'dart:async';
import 'dart:ui';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/media_library/media_library_index/widgets/share_poster_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/share_repository/share_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/bean/bg_terminal_music_list_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_category_event.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_folder_detail_event.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_folder_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_publish_image_package_upload_page.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_publish_image_single_upload_edit_page.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_publish_page.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_store_detail_common_view_model.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_store_detail_type_list_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/vg_widgets/expandable_text.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/delegate/custom_video_cache_manager.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:sharesdk_plugin/sharesdk_defines.dart';
import 'package:sharesdk_plugin/sharesdk_map.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_save_util_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart' as prefix ;
import 'package:webview_flutter/webview_flutter.dart';

import '../smart_home_upload_service.dart';

///文件夹、动态详情
class SmartHomeFolderBannerImagePage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomeFolderBannerImagePage";
  final String scid;
  final String title;
  ///01图片 02视频 03链接
  final String type;
  final SmartHomeFolderDetailDataBean folderDetail;
  final List<ColumnListBean> columnList;
  final int selectIndex;
  final String picid;

  const SmartHomeFolderBannerImagePage({
    Key key,
    this.scid,
    this.type,
    this.title,
    this.folderDetail,
    this.columnList,
    this.selectIndex,
    this.picid,
  }) : super(key: key);

  @override
  _SmartHomeFolderBannerImagePageState createState() =>
      _SmartHomeFolderBannerImagePageState();

  static Future<dynamic> navigatorPush(BuildContext context,
      String scid, String type, String title,
      SmartHomeFolderDetailDataBean folderDetail,
      List<ColumnListBean> columnList,{int selectIndex, String picid}) {
    return RouterUtils.routeForFutureResult(
        context,
        SmartHomeFolderBannerImagePage(
          scid: scid,
          type: type,
          title: title,
          folderDetail: folderDetail,
          columnList: columnList,
          selectIndex: selectIndex,
          picid: picid,
        ),
        routeName: SmartHomeFolderBannerImagePage.ROUTER);
  }
}

class _SmartHomeFolderBannerImagePageState
    extends BaseState<SmartHomeFolderBannerImagePage> {
  SmartHomeStoreDetailCommonViewModel _viewModel;

  List<FolderPicListBean> _folderPicList;
  FolderInfoBean _folderInfo;
  FolderPicListBean _currentPicInfo;
  PhotoPreviewInfoVo _currentVideoInfo;
  int _selectIndex = 0;

  double _indexWidth;
  CustomVideoCacheManager _customVideoCacheManager;

  //webview加载成功
  bool isUrlLoadComplete = false;
  StreamSubscription _detailUpdateStreamSubscription;

  AssetsAudioPlayer _assetsAudioPlayer;
  Audio _playingAudio;

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _viewModel = new SmartHomeStoreDetailCommonViewModel(this);
    _detailUpdateStreamSubscription =
        VgEventBus.global.on<RefreshSmartHomeFolderDetailEvent>()?.listen((event) {
          _viewModel.getFolderDetailWithValueNotifier(_folderInfo?.shid??"",
              _folderInfo?.sfid??"",  _folderInfo?.getFlg()??"",
                  widget?.picid??"", (folderDetailData){
            _folderPicList = folderDetailData?.folderPicList;
            _folderInfo = folderDetailData?.folderInfo;
            _currentPicInfo = _folderPicList.elementAt(_selectIndex);
            setState(() {});
            if("01" == widget?.type){
              openPlayer();
            }
          });
        });
    _selectIndex = widget?.selectIndex??0;
    _folderPicList = widget?.folderDetail?.folderPicList;
    _folderInfo = widget?.folderDetail?.folderInfo;
    _currentPicInfo = _folderPicList.elementAt(_selectIndex);
    _indexWidth = 53;
    _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    _assetsAudioPlayer.onReadyToPlay.listen((event) {
      VgHudUtils.hide(context);
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if("02" == widget?.type){
        //初始化缓存
        _customVideoCacheManager = CustomVideoCacheManager();
        String url = _currentPicInfo?.picurl;
        if (PhotoPreviewToolUtils.isNetUrl(url) && (PhotoPreviewType.video == PhotoPreviewToolUtils.getType(url))) {
          //判断是否已缓存好
          //获取存储路径
          Future<FileInfo> cacheFile = _customVideoCacheManager.getFileFromCache(url);
          cacheFile.then((value) {
            if(value != null){
              if (cacheFile != null && value.file != null) {
                url = value.file.path;
              }
            }else{
              _customVideoCacheManager.getFileStream(url).listen((event) {
                FileInfo fileInfo = event as FileInfo;
                print("fileInfo:" + fileInfo.file.path);
              });
            }
            _currentVideoInfo = new PhotoPreviewInfoVo(
              url: url,
              type: PhotoPreviewType.video,
              loadingCoverUrl:VgPhotoPreview.getImageQualityStr(_currentPicInfo?.videopic, ImageQualityType.middleUp),
            );
            setState(() { });
          });
        }
      }
      if("01" == widget?.type){
        openPlayer();
      }
    });

  }


  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: _toMainWidget(),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    _detailUpdateStreamSubscription?.cancel();
    _assetsAudioPlayer?.stop();
    _assetsAudioPlayer?.dispose();
    _videoPlayerController?.dispose();
  }



  Widget _toMainWidget() {
    return Stack(
      children: [
        _toContentWidget(),
        _toTopBarWidget(),
        _toBottomWidget(),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget() {
    return Container(
      color: Colors.transparent,
      padding: EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)),
      child: Container(
        height: 44,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              left: 16,
              child: prefix.ClickAnimateWidget(
                child: Container(
                  width: 30,
                  height: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0X66000000),
                  ),
                  child: Image.asset(
                    "images/top_bar_back_ico.png",
                    width: 9,
                    height: 16,
                    color: Colors.white,
                  ),
                ),
                scale: 1.4,
                onClick: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.of(context).maybePop();
                },
              ),
            ),
            Positioned(
              left: 58,
              child: Visibility(
                visible: StringUtils.isNotEmpty(_currentPicInfo?.getLabel()),
                child: Text(
                  _currentPicInfo?.getLabel(),
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.white
                  ),
                ),
              ),
            ),
            Positioned(
              right: 0,
              child: Padding(
                padding: EdgeInsets.only(right: 8),
                child: _toMoreOperationsWidget(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _toMoreOperationsWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: "01" == widget?.type,
          child: _toOperationsWidget("icon_download", () {
            _download();
          }),
        ),
        Visibility(
          // visible: "02" != widget?.type,
          visible: false,
          child: _toOperationsWidget("icon_share", () {
            _share();
          }),
        ),
        Visibility(
          visible: (("01" == widget?.type && _folderPicList?.length == 1) || ("01" != widget?.type)) && !_currentPicInfo.isExample() && !_currentPicInfo.isSysPush() ,
          child: _toOperationsWidget("icon_more_horizontal", () {
            _more();
          }),
        ),
      ],
    );
  }

  ///下载 分享 更多
  Widget _toOperationsWidget(String resource, Function onTap) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        onTap.call();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8),
        child: Container(
          height: 30,
          width: 30,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Color(0X66000000),
          ),
          child: Image.asset(
            "images/${resource}.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  ///根据类型判断展示布局
  Widget _toContentWidget(){
    if("01" == widget?.type){
      return _toSwiper();
    }
    if("02" == widget?.type){
      return _toVideoWidget();
    }
    if("03" == widget?.type){
      return _to3DLinkWidget();
    }
    return _toSwiper();
  }

  ///图片轮播
  Widget _toSwiper() {
    double width = ScreenUtils.screenW(context);
    double height = ScreenUtils.screenH(context);
    if((_folderPicList?.length??0) < 1){
      return Container();
    }
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      child:
      Swiper(
        layout: SwiperLayout.DEFAULT,
        loop: (_folderPicList?.length??0) > 1,
        itemBuilder: (BuildContext context, int index) {
          return _toSwiperItemWidget(_folderPicList?.elementAt(index));
        },
        autoplay: true,
        index: _selectIndex,
        itemCount: _folderPicList?.length,
        onIndexChanged: (index){
          _selectIndex = index;
          _currentPicInfo = _folderPicList
              ?.elementAt(_selectIndex);
          setState(() {});
        },
      ),
    );
  }
  ///图片item
  Widget _toSwiperItemWidget(FolderPicListBean item){
    BoxFit boxFit = BoxFit.contain;
    double screenWidth = ScreenUtils.screenW(context);
    double screenHeight = ScreenUtils.screenH(context);
    double width = 1;
    double height = 1;
    String picsize = item?.picsize;
    if (StringUtils.isNotEmpty(picsize)) {
      width = double.parse(picsize?.split("*")[0]);
      height = double.parse(picsize?.split("*")[1]);
    }
    double imageScale = height / width;
    width = screenWidth;
    height = width*imageScale;
    return ClipRect(
      child: Container(
        width: ScreenUtils.screenW(context),
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            _toBgWidget(item?.getCoverUrl() ?? ""),
            VgCacheNetWorkImage(
              item?.getCoverUrl() ?? "",
              imageQualityType: ImageQualityType.original,
              fit: BoxFit.cover,
              height: height,
              width: width,
            ),
          ],
        ),
      ),
    );
  }

  Widget _toBgWidget(String url){
    print("ImageFiltered:" + url);
    return Container(
      child: ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaY: 80, sigmaX: 350),
        child: VgCacheNetWorkImage(
          url,
          imageQualityType: ImageQualityType.original,
          fit: BoxFit.fill,
          height: ScreenUtils.screenW(context),
          width: double.infinity,
        ),
      ),
    );
  }

  VideoPlayerController _videoPlayerController;
  ///视频
  Widget _toVideoWidget(){
    return Stack(
      children: [
        ClipRect(child: Container(
            width: ScreenUtils.screenW(context),
            height: ScreenUtils.screenH(context),
            child: _toBgWidget(_currentPicInfo?.videopic??""))),
        _toVideo(),
      ],
    );
  }

  Widget _toVideo(){
    if(_currentVideoInfo != null){
      return Center(
        child: VideoWidget(
            videoInfo: _currentVideoInfo,
            videoMargin: EdgeInsets.only(bottom: 0),
            onPlayerControllerCreated:(controller){
              _videoPlayerController = controller;
            }
        ),
      );
    }
    return Container();
  }


  WebViewController _webViewController;
  ///链接
  Widget _to3DLinkWidget(){
    return Container(
      width: ScreenUtils.screenW(context),
      height: ScreenUtils.screenH(context),
      color: Colors.white,
      child: Stack(
        children: [
          Opacity(
            opacity: isUrlLoadComplete ? 1 : 0,
            child: WebView(
              initialUrl: _currentPicInfo.picurl,
              javascriptMode: JavascriptMode.unrestricted,
              javascriptChannels: <JavascriptChannel>[
              ].toSet(),
              onPageFinished: (String url) {
                print("加载完成\n" + url);
                if(!(isUrlLoadComplete??false)){
                  setState(() {
                    isUrlLoadComplete = true;
                  });
                }
              },
              onWebViewCreated: (controller) {
                _webViewController = controller;
              },
              onWebResourceError: (error){
                print("加载失败：" + error?.description??"");
                String errorInfo = error?.description??"";
                if(errorInfo.contains("net::ERR_INTERNET_DISCONNECTED")
                    || errorInfo.contains("404")
                    || errorInfo.contains("500")
                    || errorInfo.contains("Error")
                    || errorInfo.contains("找不到网页")
                    || errorInfo.contains("网页无法打开")){

                }
              },
            ),
          ),
          Center(
            child: Visibility(
              visible: !isUrlLoadComplete,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "正在加载",
                    style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getCardBgColor_21263C(),
                        fontSize: 10),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }



  Widget _toItemWidget(BuildContext context, FolderPicListBean itemBean,
      int index) {
    return Container(
      decoration: BoxDecoration(color: Colors.black),
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          VgCacheNetWorkImage(
            itemBean?.getCoverUrl() ?? "",
            fit: BoxFit.contain,
            imageQualityType: ImageQualityType.original,
          ),
          Center(
            child: Offstage(
                offstage: !(itemBean?.isVideo() ?? false),
                child: Opacity(
                  opacity: 0.5,
                  child: Image.asset(
                    "images/video_play_ico.png",
                    width: 52,
                  ),
                )),
          )
        ],
      ),
    );
  }

  ///底部门店以及文件夹信息
  Widget _toBottomWidget() {
    //屏幕宽度
    double screenWidth = ScreenUtils.screenW(context);
    //左右间距
    double padding = 32;
    //间隔距离
    double gap = (_folderPicList.length - 1) * 5.0;
    double width = (screenWidth - padding - gap)/_folderPicList.length;
    _indexWidth = width;
    // if(_indexWidth > width){
    //   _indexWidth = width;
    // }
    double bottom = ScreenUtils.getBottomBarH(context) + ((widget?.type == "02")?32:0);
    return Positioned(
      bottom: 0,
      child: IgnorePointer(
        child: Visibility(
          visible: (widget?.type != "03") || isUrlLoadComplete,
          child: Container(
            child: Container(
              width: screenWidth,
              color: Colors.transparent,
              child: Column(
                children: [
                  _toFixGradientWidget(),
                  _toInfoWidget(),
                  _toIndexWidget(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  ///固定渐变区域
  Widget _toFixGradientWidget() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTapDown: (enter){
        print("onPointerEnter");
      },
      child: Container(
        height: 80,
        width: ScreenUtils.screenW(context),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
//                Color(0xFF191E31),
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.0),
              Colors.black.withOpacity(0.1),
              Colors.black.withOpacity(0.3),
              Colors.black.withOpacity(0.4),
//                Colors.black.withOpacity(0.7),
            ],
          ),
        ),
      ),
    );
  }

  ///信息区域
  Widget _toInfoWidget() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: ScreenUtils.screenW(context),
        color: Colors.black.withOpacity(0.4),
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 16, top: 4, right: 16, bottom: ((widget?.type == "02")?32:0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ///门店信息
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 20,
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  margin: EdgeInsets.only(bottom: 4),
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white.withOpacity(0.3),
                  ),
                  child: Text(
                    widget?.title,
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.white
                    ),
                  ),
                ),
              ],
            ),

            ///标题
            Visibility(
              visible: StringUtils.isNotEmpty(_folderInfo?.title??""),
              child: Text(
                _folderInfo?.title??"",
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
            ),

            ///描述
            Visibility(
              visible: StringUtils.isNotEmpty(_folderInfo?.backup??""),
              child: Container(
                child: ExpandableText(
                  _folderInfo?.backup??"",
                  expandText: "展开",
                  collapseText: "收起",
                  linkColor: Colors.white,
                  maxLines: 3,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Visibility(
              visible: _folderPicList.length < 2,
              child: SizedBox(height: 16,),
            )
          ],
        ),
      ),
    );
  }

  ///索引
  Widget _toIndexWidget() {
    return Visibility(
      visible: _folderPicList.length > 1,
      child: Container(
        height: 30 + ScreenUtils.getBottomBarH(context),
        width: ScreenUtils.screenW(context),
        color: Colors.black.withOpacity(0.4),
        padding: EdgeInsets.only(left: 16, right: 16, bottom: ScreenUtils.getBottomBarH(context)/2),
        child: ListView.separated(
            itemCount: _folderPicList?.length ?? 0,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return _toIndexItemWidget(index);
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                width: 5,
              );
            }),
      ),
    );
  }
  Widget _toIndexItemWidget(int index){
    //默认宽度
    return Center(
      child: Container(
        width: _indexWidth,
        height: 3,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1.5),
          color: _selectIndex == index?Colors.white:Colors.white.withOpacity(0.3),
        ),
      ),
    );
  }

  ///下载
  _download() async {
    // if(_picInfoBean == null){
    //   return;
    // }
    if(widget?.type == "01"){
      loading(true, msg: "下载中");
      String saveResult = await SaveUtils.saveImage(url: _currentPicInfo.picurl);
      if (StringUtils.isNotEmpty(saveResult)) {
        loading(false);
        if (saveResult == "保存成功") {
          toast("下载成功");
        } else {
          toast(saveResult);
        }
      }
    }else if(widget?.type == "02"){

      FileInfo saveResult = await _customVideoCacheManager.downloadFile(_currentPicInfo.picurl,);
      if(saveResult != null){
        toast("下载成功");
        print(saveResult.file.path);
      }
    }
  }

  ///分享
  _share() {
    // if(_picInfoBean == null){
    //   return;
    // }
    SSDKContentType contentType = SSDKContentTypes.image;
    if(_currentPicInfo.isVideo()){
      contentType = SSDKContentTypes.video;
    }
    if(_currentPicInfo.is3DLink()){
      contentType = SSDKContentTypes.webpage;
    }
    SharePosterMenuDialog.navigatorPushDialog(context, () {
      RouterUtils.pop(context);
      //直接分享图片
      SSDKMap params = SSDKMap()
        ..setGeneral(
            "AI前台App",
            "智能家居",
            null,
            _currentPicInfo.isImage()?_currentPicInfo?.picurl:"",
            "",
            _currentPicInfo?.picurl,
            "",
            "",
            _currentPicInfo.isVideo()?_currentPicInfo?.picurl:"",
            _currentPicInfo.isVideo()?_currentVideoInfo?.url:"",
            contentType);
      _weChatShare(context, params, "00");
    }, () {
      RouterUtils.pop(context);
      //直接分享图片
      SSDKMap params = SSDKMap()
        ..setGeneral(
            "AI前台App",
            "智能家居",
            null,
            _currentPicInfo.isImage()?_currentPicInfo?.picurl:"",
            "",
            _currentPicInfo?.picurl,
            "",
            "",
            _currentPicInfo.isVideo()?_currentPicInfo?.picurl:"",
            _currentPicInfo.isVideo()?_currentVideoInfo?.url:"",
            contentType);
      _weChatShare(context, params, "01");
    },
        bgColor: Colors.white,
        splitColor: Color(0xFFF6F7F9),
        textColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        pyqResource: "images/icon_pyq_white.png");
  }

  void _weChatShare(BuildContext context, SSDKMap params, String shareFlag) {
    ShareRepository.getInstance().shareImageToWechat(context, params,
        platforms: ("00" == shareFlag)
            ? ShareSDKPlatforms.wechatTimeline
            : ShareSDKPlatforms.wechatSession);
  }

  ///更多
  _more() {
    Map<String, VoidCallback> map = {
      "编辑": () async {
        ColumnListBean currentColumn = null;
        widget?.columnList?.forEach((element) {
          if (element?.scid == _folderInfo?.scid) {
            currentColumn = element;
          }
        });
        List<SmartHomeUploadItemBean> currentList = new List();
        if("01" == widget?.type){
          //图片
          int coverIndex = 0;
          _folderPicList.forEach((element) {
            currentList.add(SmartHomeUploadItemBean(
              netUrl: element.picurl,
              type: "01",
              scid: element.scid??"",
              scidName: element.scname??"",
              folderWidth: int.parse(element?.picsize?.split("*")[0]),
              folderHeight: int.parse(element?.picsize?.split("*")[1]),
              picstorage: element?.picstorage??"",
              stid: element.stid??"",
              stidName: element.stname??"",
              style: element?.style??"99",
              backup: element.backup??"",
              picid: element?.picid??"",
              sysmid: _folderInfo?.id??"",
              styleName: element?.stylename??"",
            ));
            if(element.picurl == _folderInfo.coverurl){
              coverIndex = _folderPicList.indexOf(element);
            }
          });
          _assetsAudioPlayer?.pause();
          MusicListBean musicInfo;
          if(StringUtils.isNotEmpty(_folderInfo?.id)){
            musicInfo = new MusicListBean();
            musicInfo.murl = _folderInfo?.murl;
            musicInfo.author = _folderInfo?.author;
            musicInfo.mname = _folderInfo?.mname;
            musicInfo.msize = _folderInfo?.msize;
            musicInfo.id = _folderInfo?.id;
            musicInfo.mtime = _folderInfo?.mtime;
          }
          if(currentList.length == 1){
            dynamic result = await SmartHomePublishImageSingleUploadEditPage.navigatorPush(context,
              _folderInfo?.shid,
              currentList,
              sfid: _folderInfo.sfid,
              musicInfo: musicInfo,
              router: SmartHomeFolderBannerImagePage.ROUTER,);
            if(result == null){
              _assetsAudioPlayer?.play();
            }else{
              _assetsAudioPlayer?.play();
            }

          }else{
            SmartHomePublishImagePackageUploadPage.navigatorPush(
                context,
                _folderInfo?.shid,
                currentList: currentList,
                sfid: _folderInfo.sfid,
                title: _folderInfo.title,
                desc: _folderInfo.backup,
                coverIndex: coverIndex,
                router: SmartHomeFolderBannerImagePage.ROUTER,
                musicInfo: musicInfo,
                isCreate: false
            );
          }
          // SmartHomePublishPage.navigatorPush(
          //     context, _folderInfo?.shid, "01", widget?.columnList, currentColumn,
          //     currentList: currentList, sfid: _folderInfo.sfid,
          //     title: _folderInfo.title,
          //     desc: _folderInfo.backup,
          //     coverIndex: coverIndex,
          //     isCreate: false,
          //     router: SmartHomeFolderBannerImagePage.ROUTER);
        }else if("02" == widget?.type){
          //视频
          currentList.add(SmartHomeUploadItemBean(
            netUrl: _currentPicInfo?.picurl??"",
            type: "02",
            scid: _currentPicInfo?.scid??"",
            folderWidth: int.parse(_currentPicInfo?.picsize?.split("*")[0]),
            folderHeight: int.parse(_currentPicInfo?.picsize?.split("*")[1]),
            picstorage: _currentPicInfo?.picstorage??"",
            stid: _currentPicInfo?.scid??"",
            style: _currentPicInfo?.style??"99",
            backup: _currentPicInfo?.backup??"",
            videoid:_currentPicInfo?.videoid??"",
            sdVideo: _currentPicInfo?.sdVideo??"",
            sdStorage: _currentPicInfo?.sdStorage??"",
            videoNetCover: _currentPicInfo?.videopic??"",
            videoDuration: _currentPicInfo?.videotime??0,
            picid: _currentPicInfo?.picid??"",
          ));
          _videoPlayerController?.pause();
          SmartHomePublishPage.navigatorPush(
              context, _folderInfo?.shid, "02", widget?.columnList, currentColumn,
              currentList: currentList, sfid: _folderInfo.sfid,
              title: _folderInfo.title,
              desc: _folderInfo.backup,
              isCreate: false,
              router: SmartHomeFolderBannerImagePage.ROUTER);
        }else{
          //链接
          currentList.add(SmartHomeUploadItemBean(
            linkUrl: _currentPicInfo?.picurl??"",
            linkCover: _folderInfo?.coverurl??"",
            netUrl: _folderInfo?.coverurl??"",
            type: "03",
            scid: _currentPicInfo?.scid??"",
            folderWidth: int.parse(_folderInfo?.coversize?.split("*")[0]),
            folderHeight: int.parse(_folderInfo?.coversize?.split("*")[1]),
            picstorage: _currentPicInfo?.picstorage??"",
            stid: _currentPicInfo?.scid??"",
            style: _currentPicInfo?.style??"99",
            backup: _currentPicInfo?.backup??"",
            picid: _currentPicInfo?.picid??"",
          ));
          SmartHomePublishPage.navigatorPush(
              context, _folderInfo?.shid, "03", widget?.columnList, currentColumn,
              currentList: currentList, sfid: _folderInfo.sfid,
              title: _folderInfo.title,
              desc: _folderInfo.backup,
              isCreate: false,
              router: SmartHomeFolderBannerImagePage.ROUTER);
        }

      },
      "删除": () async {
        bool result = await CommonConfirmCancelDialog.navigatorPushDialog(
          context,
          title: "提示",
          content: "是否确认删除当前动态？",
          cancelText: "取消",
          confirmText: "确认",
          confirmBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          cancelBgColor: Color(0xFFF6F7F9),
          titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          widgetBgColor: Colors.white,
        );
        if (result ?? false) {
          _viewModel.deleteFolder(
              context,  _folderInfo?.shid, widget?.scid??"", _folderInfo?.sfid, _folderPicList
              .elementAt(0)
              .videoid);
        }
      }
    };

    CommonMoreMenuDialog.navigatorPushDialog(
      context,
      map,
      bgColor: Colors.white,
      splitColor: Color(0xFFF6F7F9),
      textColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      lineColor: Color(0xFFEEEEEE),
    );
  }

  void openPlayer() async {
    if(StringUtils.isEmpty(_folderInfo?.id)){
      _assetsAudioPlayer.stop();
      return;
    }

    MusicListBean itemBean = new MusicListBean();
    itemBean.murl = _folderInfo?.murl;
    itemBean.author = _folderInfo?.author;
    itemBean.mname = _folderInfo?.mname;
    itemBean.msize = _folderInfo?.msize;
    itemBean.id = _folderInfo?.id;
    itemBean.mtime = _folderInfo?.mtime;
    if(_folderInfo?.id == _playingAudio?.metas?.id){
      return;
    }
    _assetsAudioPlayer.stop();
    VgHudUtils.show(context, "加载中");
    _playingAudio = Audio.network(
        itemBean.murl,
        metas: Metas(
          id: itemBean.id,
          title: itemBean.mname,
          artist: itemBean.author,
          album: itemBean.author,
        ),
        cached: true
    );
    setState(() {});
    try {
      await _assetsAudioPlayer.open(
        _playingAudio,
        autoStart: true,
        showNotification: false,
        loopMode: LoopMode.single,
        playInBackground: PlayInBackground.disabledRestoreOnForeground,
        audioFocusStrategy: AudioFocusStrategy.request(
            resumeAfterInterruption: true,
            resumeOthersPlayersAfterDone: true),
        headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
      );
    } catch (e) {
      print(e);
    }
  }
}


import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_style_type_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

class SmartHomePublishImageSetStyleDialog extends StatefulWidget {
  final List<SmartHomeStyleTypeBean> styleList;
  final String style;
  final Function(SmartHomeStyleTypeBean styleBean) onConfirm;

  const SmartHomePublishImageSetStyleDialog({Key key, this.style, this.styleList, this.onConfirm})
      : super(key: key);

  @override
  _SmartHomePublishImageSetStyleDialogState createState() =>
      _SmartHomePublishImageSetStyleDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      String style, List<SmartHomeStyleTypeBean> styleList, Function(SmartHomeStyleTypeBean styleBean) onConfirm) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SmartHomePublishImageSetStyleDialog(
          style: style,
          styleList: styleList,
          onConfirm: onConfirm,
        )
    );
  }
}

class _SmartHomePublishImageSetStyleDialogState
    extends BaseState<SmartHomePublishImageSetStyleDialog> {
  SmartHomeStyleTypeBean _selectTypeBean;
  List<SmartHomeStyleTypeBean> _styleList = new List();
  @override
  void initState() {
    if(widget?.styleList != null){
      _styleList.addAll(widget?.styleList);
    }
    _selectTypeBean = getSelectBean(widget?.style);
    super.initState();
  }

  SmartHomeStyleTypeBean getSelectBean(String style){
    SmartHomeStyleTypeBean bean;
    for(int i = 0; i < _styleList?.length; i++){
      if(style == _styleList[i].style){
        bean = _styleList[i];
        break;
      }
    }
    return bean;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: ClipRRect(
        borderRadius:
        BorderRadius.only(topLeft: Radius.circular(12), topRight:Radius.circular(12)),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: _toMainColumnWidget()),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 54,
          alignment: Alignment.center,
          child: Text(
            "选择风格",
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: ThemeRepository.getInstance().getCardBgColor_21263C()
            ),
          ),
        ),
        _toGridWidget(),
      ],
    );
  }


  Widget _toGridWidget(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
        bottom: 20 + ScreenUtils.getBottomBarH(context),
      ),
      itemCount: _styleList?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 14,
        mainAxisSpacing: 12,
        childAspectRatio: 106 / 32,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toStyleGridItemWidget(_styleList?.elementAt(index));
      },
    );
  }

  ///网格item
  Widget _toStyleGridItemWidget(SmartHomeStyleTypeBean styleTypeBean){
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: _selectTypeBean?.style == styleTypeBean?.style,
      width: 106,
      height: 32,
      radius: BorderRadius.circular(4),
      unSelectBgColor: Color(0xFFF2F2F2),
      selectedBgColor: Color(0xFFFFFFFF),
      selectedBoxBorder: Border.all(
          color: ThemeRepository.getInstance()
              .getCardBgColor_21263C(),
          width: 0.5),
      unSelectTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getCardBgColor_21263C(),
        fontSize: 13,
      ),
      selectedTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getCardBgColor_21263C(),
        fontSize: 13,
      ),
      text: styleTypeBean?.getName()??"-",
      onTap: (){
          if(_selectTypeBean?.style == styleTypeBean?.style){
            _selectTypeBean = null;
          }else{
            _selectTypeBean = styleTypeBean;
          }
          widget?.onConfirm?.call(styleTypeBean);
          RouterUtils.pop(context);

      },
    );
  }

}

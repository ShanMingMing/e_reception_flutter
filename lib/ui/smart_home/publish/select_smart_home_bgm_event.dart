import 'package:e_reception_flutter/ui/new_terminal/bg_music/bean/bg_terminal_music_list_response_bean.dart';

///选中某个bgm的事件
class SelectSmartHomeBgmEvent{
  final MusicListBean musicListBean;
  final bool cancelUse;
  SelectSmartHomeBgmEvent(this.musicListBean, this.cancelUse);
}
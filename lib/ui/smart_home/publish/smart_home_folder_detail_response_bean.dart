import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"folderPicList":[{"picurl":"https://etpic.we17.com/test/20220808171813_9278.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"340","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"5fb55664e1274c88b5cbcdd89507c65b","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"},{"picurl":"https://etpic.we17.com/test/20220808171813_7259.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"354","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"68089c689720409a96968f83c1ea899c","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"},{"picurl":"https://etpic.we17.com/test/20220808171813_1572.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"373","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"e65576a06937450caf336fde4eb79b34","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"}],"folderInfo":{"sfid":"3db7e931f8d644029710f63e83392dfa","scid":"56394d14edfc4c34a9b85f327c3ff099","shid":"75726d4615c9437d9183ae7d7f216866","companyid":"3451fd291d234c669d9e200e538a6047","title":"gghh","coverurl":"https://etpic.we17.com/test/20220808171813_9278.jpg","coversize":"2240*3968","orderflg":0,"backup":"hhhhhj","defaultflg":"01","createtime":1659950296,"createuserid":"4a3d21386acb4f3d923227515c130470","updatetime":1659950296,"updateuid":"4a3d21386acb4f3d923227515c130470","delflg":"00","bjtime":"2022-08-08T17:18:16"}}
/// extra : null

class SmartHomeFolderDetailResponseBean {
  bool success;
  String code;
  String msg;
  SmartHomeFolderDetailDataBean data;
  dynamic extra;

  static SmartHomeFolderDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeFolderDetailResponseBean smartHomeFolderDetailResponseBeanBean = SmartHomeFolderDetailResponseBean();
    smartHomeFolderDetailResponseBeanBean.success = map['success'];
    smartHomeFolderDetailResponseBeanBean.code = map['code'];
    smartHomeFolderDetailResponseBeanBean.msg = map['msg'];
    smartHomeFolderDetailResponseBeanBean.data = SmartHomeFolderDetailDataBean.fromMap(map['data']);
    smartHomeFolderDetailResponseBeanBean.extra = map['extra'];
    return smartHomeFolderDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// folderPicList : [{"picurl":"https://etpic.we17.com/test/20220808171813_9278.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"340","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"5fb55664e1274c88b5cbcdd89507c65b","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"},{"picurl":"https://etpic.we17.com/test/20220808171813_7259.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"354","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"68089c689720409a96968f83c1ea899c","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"},{"picurl":"https://etpic.we17.com/test/20220808171813_1572.jpg","backup":"","picsize":"2240*3968","shid":"75726d4615c9437d9183ae7d7f216866","picstorage":"373","style":"99","sfid":"3db7e931f8d644029710f63e83392dfa","type":"01","picid":"e65576a06937450caf336fde4eb79b34","scid":"56394d14edfc4c34a9b85f327c3ff099","example":"00"}]
/// folderInfo : {"sfid":"3db7e931f8d644029710f63e83392dfa","scid":"56394d14edfc4c34a9b85f327c3ff099","shid":"75726d4615c9437d9183ae7d7f216866","companyid":"3451fd291d234c669d9e200e538a6047","title":"gghh","coverurl":"https://etpic.we17.com/test/20220808171813_9278.jpg","coversize":"2240*3968","orderflg":0,"backup":"hhhhhj","defaultflg":"01","createtime":1659950296,"createuserid":"4a3d21386acb4f3d923227515c130470","updatetime":1659950296,"updateuid":"4a3d21386acb4f3d923227515c130470","delflg":"00","bjtime":"2022-08-08T17:18:16"}

class SmartHomeFolderDetailDataBean {
  List<FolderPicListBean> folderPicList;
  FolderInfoBean folderInfo;

  static SmartHomeFolderDetailDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeFolderDetailDataBean dataBean = SmartHomeFolderDetailDataBean();
    dataBean.folderPicList = List()..addAll(
        (map['folderPicList'] as List ?? []).map((o) => FolderPicListBean.fromMap(o))
    );
    dataBean.folderInfo = FolderInfoBean.fromMap(map['folderInfo']);
    return dataBean;
  }

  Map toJson() => {
    "folderPicList": folderPicList,
    "folderInfo": folderInfo,
  };
}

/// sfid : "3db7e931f8d644029710f63e83392dfa"
/// scid : "56394d14edfc4c34a9b85f327c3ff099"
/// shid : "75726d4615c9437d9183ae7d7f216866"
/// companyid : "3451fd291d234c669d9e200e538a6047"
/// title : "gghh"
/// coverurl : "https://etpic.we17.com/test/20220808171813_9278.jpg"
/// coversize : "2240*3968"
/// orderflg : 0
/// backup : "hhhhhj"
/// defaultflg : "01"
/// createtime : 1659950296
/// createuserid : "4a3d21386acb4f3d923227515c130470"
/// updatetime : 1659950296
/// updateuid : "4a3d21386acb4f3d923227515c130470"
/// delflg : "00"
/// bjtime : "2022-08-08T17:18:16"

class FolderInfoBean {
  String sfid;
  String scid;
  String shid;
  String companyid;
  String title;
  String coverurl;
  String coversize;
  int orderflg;
  String backup;
  String defaultflg;
  int createtime;
  int operatetime;
  String createuserid;
  int updatetime;
  String updateuid;
  String delflg;
  String bjtime;
  String name;
  //bgm相关
  String murl;
  String author;
  String mname;
  int msize;
  String id;
  String mtime;

  String getFlg(){
    if(isSysPush()){
      return "01";
    }else{
      return "00";
    }
  }

  //是否为系统推送
  bool isSysPush(){
    return "sys" == companyid;
  }

  String getTitle(){
    if(StringUtils.isNotEmpty(title)){
      return title;
    }
    return "未命名";
  }

  String getName(){
    if((operatetime??0) >= (updatetime??0)){
      return "蔚来无限";
    }
    return name??"";
  }

  String getPublishInfo(){
    return getTimeStr() + "/" + getName();
  }

  int getTime(){
    if((operatetime??0) > (updatetime??0)){
      return operatetime;
    }
    return updatetime;
  }

  String getTimeStr() {
    return TimeUtil.formatBySimpleYear((getTime()??0)*1000, dayFormat: DayFormat.Full);
  }

  static FolderInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FolderInfoBean folderInfoBean = FolderInfoBean();
    folderInfoBean.sfid = map['sfid'];
    folderInfoBean.scid = map['scid'];
    folderInfoBean.shid = map['shid'];
    folderInfoBean.companyid = map['companyid'];
    folderInfoBean.title = map['title'];
    folderInfoBean.coverurl = map['coverurl'];
    folderInfoBean.coversize = map['coversize'];
    folderInfoBean.orderflg = map['orderflg'];
    folderInfoBean.backup = map['backup'];
    folderInfoBean.defaultflg = map['defaultflg'];
    folderInfoBean.createtime = map['createtime'];
    folderInfoBean.operatetime = map['operatetime'];
    folderInfoBean.createuserid = map['createuserid'];
    folderInfoBean.updatetime = map['updatetime'];
    folderInfoBean.updateuid = map['updateuid'];
    folderInfoBean.delflg = map['delflg'];
    folderInfoBean.bjtime = map['bjtime'];
    folderInfoBean.name = map['name'];

    folderInfoBean.murl = map['murl'];
    folderInfoBean.author = map['author'];
    folderInfoBean.mname = map['mname'];
    folderInfoBean.msize = map['msize'];
    folderInfoBean.id = map['id'];
    folderInfoBean.mtime = map['mtime'];
    return folderInfoBean;
  }

  Map toJson() => {
    "sfid": sfid,
    "scid": scid,
    "shid": shid,
    "companyid": companyid,
    "title": title,
    "coverurl": coverurl,
    "coversize": coversize,
    "orderflg": orderflg,
    "backup": backup,
    "defaultflg": defaultflg,
    "createtime": createtime,
    "operatetime": operatetime,
    "createuserid": createuserid,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "bjtime": bjtime,
    "name": name,
    "murl": murl,
    "author": author,
    "mname": mname,
    "msize": msize,
    "id": id,
    "mtime": mtime,
  };
}

/// picurl : "https://etpic.we17.com/test/20220808171813_9278.jpg"
/// backup : ""
/// picsize : "2240*3968"
/// shid : "75726d4615c9437d9183ae7d7f216866"
/// picstorage : "340"
/// style : "99"
/// sfid : "3db7e931f8d644029710f63e83392dfa"
/// type : "01"
/// picid : "5fb55664e1274c88b5cbcdd89507c65b"
/// scid : "56394d14edfc4c34a9b85f327c3ff099"
/// example : "00"

class FolderPicListBean {
  //图片就是图片地址，视频是视频地址，3dlink是链接地址
  String picurl;
  String backup;
  String picsize;
  String shid;
  String picstorage;
  String style;
  String sfid;
  String type;
  String picid;
  String scid;
  String scname;
  String stid;
  String stname;
  String example;
  //视频相关
  int videotime;
  String videoid;
  String videopic;
  String sdVideo;
  String sdStorage;
  String stylename;
  //"-1"为样例 "sys"为系统推送
  String companyid;

  //是否为系统推送
  bool isSysPush(){
    return "sys" == companyid;
  }

  String getLabel(){
    String label = "";
    if(StringUtils.isNotEmpty(scname)){
      label += scname;
      label += "/";
    }
    if(StringUtils.isNotEmpty(stname) && "其他" != stname){
      label += stname;
      label += "/";
    }
    if(StringUtils.isNotEmpty(stylename)){
      label += stylename;
    }else if(StringUtils.isNotEmpty(style)){
      String tempStyle = ConstantRepository.of().getStyleNameByStyle(style);
      if("未知风格" != tempStyle){
        label += ConstantRepository.of().getStyleNameByStyle(style);
      }
    }
    if(label.endsWith("/")){
      label = label.substring(0, label.length-1);
    }
    return label;
  }

  bool isExample(){
    return "-1" == companyid;
  }





  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return picurl;
  }

  ///01 图片 02 视频 03 3dLink
  bool isImage(){
    return type == "01";
  }
  bool isVideo(){
    return type == "02";
  }
  bool is3DLink(){
    return type == "03";
  }



  static FolderPicListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FolderPicListBean folderPicListBean = FolderPicListBean();
    folderPicListBean.picurl = map['picurl'];
    folderPicListBean.backup = map['backup'];
    folderPicListBean.picsize = map['picsize'];
    folderPicListBean.shid = map['shid'];
    folderPicListBean.picstorage = map['picstorage'];
    folderPicListBean.style = map['style'];
    folderPicListBean.stylename = map['stylename'];
    folderPicListBean.sfid = map['sfid'];
    folderPicListBean.type = map['type'];
    folderPicListBean.picid = map['picid'];
    folderPicListBean.scid = map['scid'];
    folderPicListBean.scname = map['scname'];
    folderPicListBean.stid = map['stid'];
    folderPicListBean.stname = map['stname'];
    folderPicListBean.example = map['example'];
    folderPicListBean.videotime = map['videotime'];
    folderPicListBean.videoid = map['videoid'];
    folderPicListBean.videopic = map['videopic'];
    folderPicListBean.sdVideo = map['sdVideo'];
    folderPicListBean.sdStorage = map['sdStorage'];
    folderPicListBean.companyid = map['companyid'];
    return folderPicListBean;
  }

  Map toJson() => {
    "picurl": picurl,
    "backup": backup,
    "picsize": picsize,
    "shid": shid,
    "picstorage": picstorage,
    "style": style,
    "stylename": stylename,
    "sfid": sfid,
    "type": type,
    "picid": picid,
    "scid": scid,
    "scname": scname,
    "stid": stid,
    "stname": stname,
    "example": example,
    "videotime": videotime,
    "videoid": videoid,
    "videopic": videopic,
    "sdVideo": sdVideo,
    "sdStorage": sdStorage,
    "companyid": companyid,
  };
}
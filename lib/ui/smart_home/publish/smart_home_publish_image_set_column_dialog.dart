import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_column_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

class SmartHomePublishImageSetColumnDialog extends StatefulWidget {
  final List<ColumnListBean> columnList;
  final String scid;
  final Function(ColumnListBean columnListBean) onConfirm;

  const SmartHomePublishImageSetColumnDialog({Key key, this.scid, this.columnList, this.onConfirm})
      : super(key: key);

  @override
  _SmartHomePublishImageSetColumnDialogState createState() =>
      _SmartHomePublishImageSetColumnDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      String scid, List<ColumnListBean> columnList, Function(ColumnListBean columnListBean) onConfirm) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SmartHomePublishImageSetColumnDialog(
          scid: scid,
          columnList: columnList,
          onConfirm: onConfirm,
        )
    );
  }
}

class _SmartHomePublishImageSetColumnDialogState
    extends BaseState<SmartHomePublishImageSetColumnDialog> {
  ColumnListBean _selectBean;
  List<ColumnListBean> _columnList = new List();
  @override
  void initState() {
    if(widget?.columnList != null){
      for(int i = 0; i < widget?.columnList?.length; i++){
        if("全部" != widget?.columnList[i]?.name){
          _columnList.add(widget?.columnList[i]);
        }
      }
    }
    _selectBean = getSelectBean(widget?.scid);
    super.initState();
  }

  ColumnListBean getSelectBean(String scid){
    ColumnListBean bean;
    for(int i = 0; i < _columnList?.length; i++){
      if(scid == _columnList[i].scid){
        bean = _columnList[i];
        break;
      }
    }
    return bean;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: ClipRRect(
        borderRadius:
        BorderRadius.only(topLeft: Radius.circular(12), topRight:Radius.circular(12)),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: _toMainColumnWidget()),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 54,
          alignment: Alignment.center,
          child: Text(
            "选择类别",
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: ThemeRepository.getInstance().getCardBgColor_21263C()
            ),
          ),
        ),
        _toGridWidget(),
      ],
    );
  }


  Widget _toGridWidget(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
        bottom: 20 + ScreenUtils.getBottomBarH(context),
      ),
      itemCount: _columnList?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 14,
        mainAxisSpacing: 12,
        childAspectRatio: 106 / 32,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toStyleGridItemWidget(_columnList?.elementAt(index));
      },
    );
  }

  ///网格item
  Widget _toStyleGridItemWidget(ColumnListBean columnListBean){
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: _selectBean?.scid == columnListBean?.scid,
      width: 106,
      height: 32,
      radius: BorderRadius.circular(4),
      unSelectBgColor: Color(0xFFF2F2F2),
      selectedBgColor: Color(0xFFFFFFFF),
      selectedBoxBorder: Border.all(
          color: ThemeRepository.getInstance()
              .getCardBgColor_21263C(),
          width: 0.5),
      unSelectTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getCardBgColor_21263C(),
        fontSize: 13,
      ),
      selectedTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getCardBgColor_21263C(),
        fontSize: 13,
      ),
      text: getName(columnListBean?.name??"-"),
      onTap: (){
          if(_selectBean?.scid == columnListBean?.scid){
            _selectBean = null;
          }else{
            _selectBean = columnListBean;
          }
          widget?.onConfirm?.call(_selectBean);
          RouterUtils.pop(context);

      },
    );
  }



  String getName(String name){
    if((name?.length??0) > 4){
      return name.substring(0, 4) + "...";
    }
    return name;
  }

}

import 'dart:convert';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_category_event.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_folder_detail_event.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_index_event.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_folder_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_store_details_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_one_details_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_image_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_terminal_list_response_bean.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../../app_main.dart';


class SmartHomeStoreDetailTypeListViewModel extends BasePagerViewModel<
    SmartHomeStoreDetailListItemBean,
    SmartHomeStoreDetailResponseBean> {


  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  //门店id
  String shid;
  //栏目id
  String scid;
  String scname;
  String flg;
  //选样推送 00关闭 01开启
  String samplePush;
  //风格
  String styles;
  //类型/空间id
  String stids;
  //类型/空间名
  String stnames;

  SmartHomeStoreDetailTypeListViewModel(BaseState<StatefulWidget> state,
      {String shid, String scid, String scname, String flg, String samplePush,
        String styles, String stids, String stnames}) : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);

    this.shid = shid??"";
    this.scid = scid??"";
    this.scname = scname??"";
    this.flg = flg??"00";
    this.samplePush = samplePush??"00";
    this.styles = styles??"";
    this.stids = stids??"";
    this.stnames = stnames??"";
  }


  @override
  void onDisposed() {
    // totalValueNotifier?.dispose();
    // statusTypeValueNotifier?.dispose();
  }


  @override
  bool isNeedCache() {
    return true;
  }


  @override
  void refresh() {
    setCacheKey(getCacheKey());
    super.refresh();
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "shid": shid??"",
      "scid": scid??"",//栏目
      "scname": scname??"",//栏目
      "samplePush":samplePush??"00",
      "styles":styles??"",
      "stids":stids??"",
      "stnames":stnames??"",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getCacheKey() {
    String cacheKey = "${NetApi.GET_STORE_COLUMN_IMAGE_LIST_BY_TYPE}${shid??""}${scid??""}";

    print("cacheKey:" + cacheKey);
    return cacheKey;
  }

  @override
  String getUrl() {
    return NetApi.GET_STORE_COLUMN_IMAGE_LIST_BY_TYPE;
  }

  @override
  SmartHomeStoreDetailResponseBean parseData(VgHttpResponse resp) {
    SmartHomeStoreDetailResponseBean vo = SmartHomeStoreDetailResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{

      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

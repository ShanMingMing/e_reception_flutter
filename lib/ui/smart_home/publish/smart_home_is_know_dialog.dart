import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SmartHomeISeeDialog extends StatelessWidget {
  final String content;
  final Color confirmBgColor;
  final Color titleColor;
  final Color contentColor;
  final Color widgetBgColor;

  const SmartHomeISeeDialog({Key key, this.content,  this.confirmBgColor,
    this.titleColor, this.contentColor, this.widgetBgColor,}) : super(key: key);

  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,{String content,Color confirmBgColor,
    Color titleColor, Color contentColor, Color widgetBgColor}) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: SmartHomeISeeDialog(
          content: content,
          confirmBgColor: confirmBgColor,
          titleColor: titleColor,
          contentColor: contentColor,
          widgetBgColor: widgetBgColor,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: Center(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            color: widgetBgColor??Colors.white,
            width: 290,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    content ?? "是否取消吗？",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 5,
                    style: TextStyle(
                      color: contentColor??ThemeRepository.getInstance().getCardBgColor_21263C(),
                      fontSize: 16,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                ),
                SizedBox(height: 30,),
                CommonFixedHeightConfirmButtonWidget(
                  isAlive: true,
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  radius:BorderRadius.circular(4),
                  unSelectBgColor: Color(0xff3a3f50),
                  selectedBgColor: Color(0xFFF6F7F9),
                  unSelectTextStyle: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                      fontSize: 14,
                  ),
                  selectedTextStyle: TextStyle(
                      color: Color(0XFF1890FF),
                      fontSize: 14,
                  ),
                  text: "我知道了",
                  onTap: (){
                    RouterUtils.pop(context);
                  },
                ),
                SizedBox(height: 20,),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

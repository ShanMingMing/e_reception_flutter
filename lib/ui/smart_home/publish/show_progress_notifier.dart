
import 'package:flutter/material.dart';

import '../smart_home_upload_service.dart';

///进度条进度显示notifier
class ShowProgressNotifier extends ChangeNotifier {
  double _progress;
  String _id;

  ///类型 [ShowType]
  String _type;
  String _msg;
  UploadStatus _status;

  String get msg => _msg;

  set msg(String value) {
    _msg = value;
    notifyListeners();
  } //更新数据

  update({String id, double progress}) {
    _progress = progress;
    _id = id;
    notifyListeners();
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  double get progress => _progress;

  set progress(double value) {
    _progress = value;
  }

  String get type => _type;

  set type(String value) {
    _type = value;
  }

  UploadStatus get status => _status;

  set status(UploadStatus value) {
    _status = value;
    notifyListeners();
  }

  ///重置
  void reset() {
    _id = null;
    _status = null;
    _type = null;
    _progress = null;
    notifyListeners();
  }
}

//页面类型
enum ShowType {
  //资料库
  MEDIA_LIBRARY,
  //智能家居
  SMART_HOME
}

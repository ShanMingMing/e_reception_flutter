import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';


class SmartHomeStoreDetailResponseBean extends BasePagerBean<SmartHomeStoreDetailListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;

  static SmartHomeStoreDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreDetailResponseBean smartHomeIndexResponseBeanBean = SmartHomeStoreDetailResponseBean();
    smartHomeIndexResponseBeanBean.success = map['success'];
    smartHomeIndexResponseBeanBean.code = map['code'];
    smartHomeIndexResponseBeanBean.msg = map['msg'];
    smartHomeIndexResponseBeanBean.data = DataBean.fromMap(map['data']);
    return smartHomeIndexResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
  };
  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<SmartHomeStoreDetailListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"matype":"01","logodiy":"","picsize":94,"calx":"","caly":"","cretype":"07","effecturl":"http://etpic.we17.com/test/20210608093439_1129.jpg","htmlurl":"http://etpic.we17.com/test/20210802101431_5380.html","calflg":"00","namediy":"","psdurl":"[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]","addressdiy":"","picurl":"http://etpic.we17.com/test/20210608094231_1804.jpg","colour":"","pictype":"05","colorflg":"00","theme":"01","updatetime":"2021-08-12 11:39:31","picid":"af648b086e4c4d5c93b073d034c0be60"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"matype":"01","logodiy":"","picsize":94,"calx":"","caly":"","cretype":"07","effecturl":"http://etpic.we17.com/test/20210608093439_1129.jpg","htmlurl":"http://etpic.we17.com/test/20210802101431_5380.html","calflg":"00","namediy":"","psdurl":"[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]","addressdiy":"","picurl":"http://etpic.we17.com/test/20210608094231_1804.jpg","colour":"","pictype":"05","colorflg":"00","theme":"01","updatetime":"2021-08-12 11:39:31","picid":"af648b086e4c4d5c93b073d034c0be60"}]
/// total : 1
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<SmartHomeStoreDetailListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => SmartHomeStoreDetailListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}


class SmartHomeStoreDetailListItemBean {
  String coverurl;
  String picurl;
  String backup;
  String spatialname;
  String coversize;
  String picsize;
  String picstorage;
  int videotime;
  String style;
  String type;
  String picid;
  String videopic;
  String videoid;
  bool selectStatus;
  //是否是样例 01
  String example;
  //文件夹id
  String sfid;
  String scid;
  int piccnt;
  String title;
  int updatetime;
  //'01 单图 02 组图',
  String pictype;
  //-1时为样例
  String companyid;

  String getPicSize(){
    if(StringUtils.isNotEmpty(coversize)){
      return coversize;
    }
    return picsize;
  }

  bool isMultiImage(){
    return "01" == type && "02" == pictype;
  }

  String getFlg(){
    if(isSysPush()){
      return "01";
    }else{
      return "00";
    }
  }

  //是否为系统推送
  bool isSysPush(){
    return "sys" == companyid;
  }


  bool isExample(){
    return "-1" == companyid;
  }

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  ///01 图片 02 视频 03链接
  bool is3DLink(){
    return type == "03";
  }

  ///根据类型获取封面
  String getCover() {
    if(StringUtils.isNotEmpty(coverurl)){
      return coverurl;
    }else{
      return picurl;
    }

  }

  String getTimeStr() {
    return TimeUtil.formatBySimpleYear((updatetime??0)*1000, dayFormat: DayFormat.Full);
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      if(StringUtils.isNotEmpty(videopic)){
        return videopic;
      }
      return getCover();
    }
    return picurl;
  }

  String getPicStorageStr(){
    if(StringUtils.isEmpty(picstorage)){
      picstorage = "0";
    }
    double size = double.parse(picstorage);
    if(size <= 1024){
      if(size < 10){
        return size.toStringAsPrecision(1) + "KB";
      }else if(size < 100){
        return size.toStringAsPrecision(2) + "KB";
      }else if(size < 1000){
        return size.toStringAsPrecision(3) + "KB";
      }else {
        return size.toStringAsPrecision(4) + "KB";
      }
    }else if(size > 1024 && size <= 1024*1024){
      return (size/1024).toStringAsPrecision(3) + "MB";
    }else{
      return (size/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }


  static SmartHomeStoreDetailListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreDetailListItemBean recordsBean = SmartHomeStoreDetailListItemBean();
    recordsBean.coverurl = map['coverurl'];
    recordsBean.picurl = map['picurl'];
    recordsBean.backup = map['backup'];
    recordsBean.spatialname = map['spatialname'];
    recordsBean.coversize = map['coversize'];
    recordsBean.picsize = map['picsize'];
    recordsBean.picstorage = map['picstorage'];
    recordsBean.videotime = map['videotime'];
    recordsBean.style = map['style'];
    recordsBean.type = map['type'];
    recordsBean.picid = map['picid'];
    recordsBean.videopic = map['videopic'];
    recordsBean.videoid = map['videoid'];
    recordsBean.selectStatus = map['selectStatus'];
    recordsBean.example = map['example'];
    recordsBean.sfid = map['sfid'];
    recordsBean.scid = map['scid'];
    recordsBean.piccnt = map['piccnt'];
    recordsBean.title = map['title'];
    recordsBean.updatetime = map['updatetime'];
    recordsBean.pictype = map['pictype'];
    recordsBean.companyid = map['companyid'];
    return recordsBean;
  }

  Map toJson() => {
    "coverurl": coverurl,
    "picurl": picurl,
    "backup": backup,
    "spatialname": spatialname,
    "coversize": coversize,
    "picsize": picsize,
    "picstorage": picstorage,
    "videotime": videotime,
    "style": style,
    "type": type,
    "picid": picid,
    "videopic": videopic,
    "videoid": videoid,
    "selectStatus": selectStatus,
    "example": example,
    "sfid": sfid,
    "scid": scid,
    "piccnt": piccnt,
    "title": title,
    "updatetime": updatetime,
    "pictype": pictype,
    "companyid": companyid,
  };
}
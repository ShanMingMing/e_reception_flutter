import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"watermark":"00","shid":"2da2091b37214e11850dd2e465db01f7","name":"njn","logo":"","brand":"ssss"},{"watermark":"00","shid":"c4b4a76bc53d4deb830b291e7d8f870f","name":"xccc","logo":"","brand":"测试"},{"watermark":"00","shid":"c705dac69262472eaa042e4b8ff535bd","name":"一下","logo":"","brand":"测试"},{"watermark":"00","shid":"0f748aa54cae4a1abd27917922bd694f","name":"门店","logo":"","brand":"今天"},{"watermark":"00","shid":"3a242e8a922e4adeb91f3314970e7dbd","name":"嘿嘿","logo":"","brand":"公司的人物简介吗"},{"watermark":"00","shid":"ac108f8f736648b7aeca2531f9e0638c","name":"双龙大道点好好的好的","logo":"https://etpic.we17.com/test/20220722103915_7857.jpg","brand":"顾家家具大大方方","improveLogo":"https://etpic.we17.com/test/20220722104003_4779.png"},{"watermark":"00","shid":"95e732c9dc64425892cfaf28f60d9780","name":"嘎嘎嘎评论了","logo":"https://etpic.we17.com/test/20220718171024_7508.jpg","improveLogo":""},{"watermark":"00","shid":"3e62379d746742b8a48750fcaeb580a7","name":"童话里有话","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145856_2752.jpg"},{"watermark":"00","shid":"5139b0c31a1c4edbb441ada447e59c28","name":"客气啦哈哈哈嘿嘿噶","logo":"https://etpic.we17.com/test/20220715084100_9177.jpg","brand":"好的好的","improveLogo":"https://etpic.we17.com/test/20220719143222_8849.png"},{"watermark":"00","shid":"279495d2c1cc4917a9d77fd6b1750c29","name":"芝士","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145512_2008.png"},{"watermark":"00","shid":"de848855036b490e9c4a71910a011fd1","name":"样例","logo":""},{"watermark":"00","shid":"3e14888644194765aa4c5849f3150ec8","name":"赶紧抢古古怪怪滚滚","logo":""},{"watermark":"00","shid":"f791e1eaa8484d1e9fd143acc239e0c4","name":"说话啊说话","logo":""},{"watermark":"00","shid":"4a68ea2d3ded4c51b8d39fd9ffc49820","name":"好好吃饭","logo":"https://etpic.we17.com/test/20220712175125_7575.jpg"},{"watermark":"00","shid":"12e0d5534546488f983edbac6a21300f","name":"富以后滚滚滚不会","logo":"https://etpic.we17.com/test/20220705100846_9584.jpg"}]}
/// extra : null

class SmartHomeSimpleStoreListResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static SmartHomeSimpleStoreListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeSimpleStoreListResponseBean smartHomeSimpleStoreListResponseBeanBean = SmartHomeSimpleStoreListResponseBean();
    smartHomeSimpleStoreListResponseBeanBean.success = map['success'];
    smartHomeSimpleStoreListResponseBeanBean.code = map['code'];
    smartHomeSimpleStoreListResponseBeanBean.msg = map['msg'];
    smartHomeSimpleStoreListResponseBeanBean.data = DataBean.fromMap(map['data']);
    smartHomeSimpleStoreListResponseBeanBean.extra = map['extra'];
    return smartHomeSimpleStoreListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"watermark":"00","shid":"2da2091b37214e11850dd2e465db01f7","name":"njn","logo":"","brand":"ssss"},{"watermark":"00","shid":"c4b4a76bc53d4deb830b291e7d8f870f","name":"xccc","logo":"","brand":"测试"},{"watermark":"00","shid":"c705dac69262472eaa042e4b8ff535bd","name":"一下","logo":"","brand":"测试"},{"watermark":"00","shid":"0f748aa54cae4a1abd27917922bd694f","name":"门店","logo":"","brand":"今天"},{"watermark":"00","shid":"3a242e8a922e4adeb91f3314970e7dbd","name":"嘿嘿","logo":"","brand":"公司的人物简介吗"},{"watermark":"00","shid":"ac108f8f736648b7aeca2531f9e0638c","name":"双龙大道点好好的好的","logo":"https://etpic.we17.com/test/20220722103915_7857.jpg","brand":"顾家家具大大方方","improveLogo":"https://etpic.we17.com/test/20220722104003_4779.png"},{"watermark":"00","shid":"95e732c9dc64425892cfaf28f60d9780","name":"嘎嘎嘎评论了","logo":"https://etpic.we17.com/test/20220718171024_7508.jpg","improveLogo":""},{"watermark":"00","shid":"3e62379d746742b8a48750fcaeb580a7","name":"童话里有话","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145856_2752.jpg"},{"watermark":"00","shid":"5139b0c31a1c4edbb441ada447e59c28","name":"客气啦哈哈哈嘿嘿噶","logo":"https://etpic.we17.com/test/20220715084100_9177.jpg","brand":"好的好的","improveLogo":"https://etpic.we17.com/test/20220719143222_8849.png"},{"watermark":"00","shid":"279495d2c1cc4917a9d77fd6b1750c29","name":"芝士","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145512_2008.png"},{"watermark":"00","shid":"de848855036b490e9c4a71910a011fd1","name":"样例","logo":""},{"watermark":"00","shid":"3e14888644194765aa4c5849f3150ec8","name":"赶紧抢古古怪怪滚滚","logo":""},{"watermark":"00","shid":"f791e1eaa8484d1e9fd143acc239e0c4","name":"说话啊说话","logo":""},{"watermark":"00","shid":"4a68ea2d3ded4c51b8d39fd9ffc49820","name":"好好吃饭","logo":"https://etpic.we17.com/test/20220712175125_7575.jpg"},{"watermark":"00","shid":"12e0d5534546488f983edbac6a21300f","name":"富以后滚滚滚不会","logo":"https://etpic.we17.com/test/20220705100846_9584.jpg"}]

class DataBean {
  List<SimpleStoreListBean> list;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => SimpleStoreListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

/// watermark : "00"
/// shid : "2da2091b37214e11850dd2e465db01f7"
/// name : "njn"
/// logo : ""
/// brand : "ssss"

class SimpleStoreListBean {
  String watermark;
  String shid;
  String name;
  String logo;
  String brand;
  String addrProvince;
  String addrCity;
  String addrDistrict;
  String address;

  String getTitle(){
    return "${brand??""}（${name??""}）";
  }

  String getShowAddress(){
    String showAddress;
    if (addrProvince == addrCity) {
      showAddress = "${addrProvince??""}${addrDistrict??""}${address??""}";
    } else {
      showAddress = "${addrProvince??""}${addrCity??""}${addrDistrict??""}${address??""}";
    }
    if(StringUtils.isEmpty(showAddress)){
      showAddress = "暂无详细地址";
    }
    return showAddress;
  }

  static SimpleStoreListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SimpleStoreListBean listBean = SimpleStoreListBean();
    listBean.watermark = map['watermark'];
    listBean.shid = map['shid'];
    listBean.name = map['name'];
    listBean.logo = map['logo'];
    listBean.brand = map['brand'];
    listBean.addrProvince = map['addrProvince'];
    listBean.addrCity = map['addrCity'];
    listBean.addrDistrict = map['addrDistrict'];
    listBean.address = map['address'];
    return listBean;
  }

  Map toJson() => {
    "watermark": watermark,
    "shid": shid,
    "name": name,
    "logo": logo,
    "brand": brand,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "address": address,
  };
}
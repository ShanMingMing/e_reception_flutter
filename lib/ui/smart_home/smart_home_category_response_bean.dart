import 'dart:convert';

import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"spatialTypeList":[{"orderflg":0,"name":"综合","cnt":0,"stid":"fb91a63be65440cc84fc36de495ea23a"},{"orderflg":1,"name":"客厅","cnt":0,"stid":"dcb9243a22554d49884efb40fdbef2ed"},{"orderflg":2,"name":"卧室","cnt":0,"stid":"ea2c297884c2479d889b47210255cb68"},{"orderflg":3,"name":"儿童房","cnt":0,"stid":"5e41014c19224829be761b41cdddd2e7"},{"orderflg":4,"name":"厨房","cnt":0,"stid":"5b70ac55eb8140dca54f5f8fb313f05e"},{"orderflg":5,"name":"餐厅","cnt":0,"stid":"51e4c11a8c414f048b4e213d14fce04c"},{"orderflg":6,"name":"卫生间","cnt":0,"stid":"1b339baa61f14a08889a9b5a4ff47a2a"},{"orderflg":7,"name":"阳台","cnt":0,"stid":"ccedda15e7064601816a6d8c61c0ae39"},{"orderflg":8,"name":"玄关","cnt":0,"stid":"e493cceaeb884d39ae9ba137013ec09e"},{"orderflg":9,"name":"功能区","cnt":0,"stid":"d17c36de3ea845c2bcf3f13c207a0f45"},{"orderflg":10,"name":"书房","cnt":0,"stid":"9e3f9454b3cd413882762423c0ea9482"},{"orderflg":11,"name":"吧台","cnt":0,"stid":"f961e6691c3140169f79cdcb2b34a34f"},{"orderflg":12,"name":"其他","cnt":0,"stid":"37f756c66dca4e3faee89ca5bbadeb1e"}],"comSmarthome":{"shid":"b0097794c3db474da8a1d44df330fe19","companyid":"d6024c77362c474c9203cdabcf33d446","name":"哈骨灰盒","logo":"http://etpic.we17.com/test/20211210105736_3840.jpg","createtime":1646624958,"createuserid":"8be565cbe42842cdb2d4da011f2d3fb4","updatetime":1646624958,"updateuid":"8be565cbe42842cdb2d4da011f2d3fb4","delflg":"00","bjtime":"2022-03-07T11:49:15"},"terCnt":0}
/// extra : null

class SmartHomeCategoryResponseBean extends BasePagerBean<SpatialTypeListBean>{
  bool success;
  String code;
  String msg;
  CategoryDataBean data;
  dynamic extra;
  static SmartHomeCategoryResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeCategoryResponseBean smartHomeCategoryResponseBeanBean = SmartHomeCategoryResponseBean();
    smartHomeCategoryResponseBeanBean.success = map['success'];
    smartHomeCategoryResponseBeanBean.code = map['code'];
    smartHomeCategoryResponseBeanBean.msg = map['msg'];
    smartHomeCategoryResponseBeanBean.data = CategoryDataBean.fromMap(map['data']);
    smartHomeCategoryResponseBeanBean.extra = map['extra'];
    return smartHomeCategoryResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<SpatialTypeListBean> getDataList() => data?.spatialTypeList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// spatialTypeList : [{"orderflg":0,"name":"综合","cnt":0,"stid":"fb91a63be65440cc84fc36de495ea23a"},{"orderflg":1,"name":"客厅","cnt":0,"stid":"dcb9243a22554d49884efb40fdbef2ed"},{"orderflg":2,"name":"卧室","cnt":0,"stid":"ea2c297884c2479d889b47210255cb68"},{"orderflg":3,"name":"儿童房","cnt":0,"stid":"5e41014c19224829be761b41cdddd2e7"},{"orderflg":4,"name":"厨房","cnt":0,"stid":"5b70ac55eb8140dca54f5f8fb313f05e"},{"orderflg":5,"name":"餐厅","cnt":0,"stid":"51e4c11a8c414f048b4e213d14fce04c"},{"orderflg":6,"name":"卫生间","cnt":0,"stid":"1b339baa61f14a08889a9b5a4ff47a2a"},{"orderflg":7,"name":"阳台","cnt":0,"stid":"ccedda15e7064601816a6d8c61c0ae39"},{"orderflg":8,"name":"玄关","cnt":0,"stid":"e493cceaeb884d39ae9ba137013ec09e"},{"orderflg":9,"name":"功能区","cnt":0,"stid":"d17c36de3ea845c2bcf3f13c207a0f45"},{"orderflg":10,"name":"书房","cnt":0,"stid":"9e3f9454b3cd413882762423c0ea9482"},{"orderflg":11,"name":"吧台","cnt":0,"stid":"f961e6691c3140169f79cdcb2b34a34f"},{"orderflg":12,"name":"其他","cnt":0,"stid":"37f756c66dca4e3faee89ca5bbadeb1e"}]
/// comSmarthome : {"shid":"b0097794c3db474da8a1d44df330fe19","companyid":"d6024c77362c474c9203cdabcf33d446","name":"哈骨灰盒","logo":"http://etpic.we17.com/test/20211210105736_3840.jpg","createtime":1646624958,"createuserid":"8be565cbe42842cdb2d4da011f2d3fb4","updatetime":1646624958,"updateuid":"8be565cbe42842cdb2d4da011f2d3fb4","delflg":"00","bjtime":"2022-03-07T11:49:15"}
/// terCnt : 0

class CategoryDataBean {
  List<SpatialTypeListBean> spatialTypeList;
  ComSmarthomeBean comSmarthome;
  int terCnt;

  int getPicTotalCnt(){
    if(spatialTypeList == null || spatialTypeList.length == 0){
      return 0;
    }
    int count = 0;
    spatialTypeList.forEach((element) {
      count += (element.cnt??0);
    });
    return count;
  }

  int getCategoryPicCnt(String stid){
    if(spatialTypeList == null || spatialTypeList.length == 0){
      return 0;
    }
    if(StringUtils.isEmpty(stid)){
      return 0;
    }
    int count = 0;
    spatialTypeList.forEach((element) {
      if(stid == element.stid){
        count = element.uploadcnt;
        return count;
      }
    });
    return count;
  }

  static CategoryDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CategoryDataBean dataBean = CategoryDataBean();
    dataBean.spatialTypeList = List()..addAll(
      (map['spatialTypeList'] as List ?? []).map((o) => SpatialTypeListBean.fromMap(o))
    );
    dataBean.comSmarthome = ComSmarthomeBean.fromMap(map['comSmarthome']);
    dataBean.terCnt = map['terCnt'];
    return dataBean;
  }

  Map toJson() => {
    "spatialTypeList": spatialTypeList,
    "comSmarthome": comSmarthome,
    "terCnt": terCnt,
  };

  @override
  String toString() {
    return json.encode(this).toString();
  }
}

/// shid : "b0097794c3db474da8a1d44df330fe19"
/// companyid : "d6024c77362c474c9203cdabcf33d446"
/// name : "哈骨灰盒"
/// logo : "http://etpic.we17.com/test/20211210105736_3840.jpg"
/// createtime : 1646624958
/// createuserid : "8be565cbe42842cdb2d4da011f2d3fb4"
/// updatetime : 1646624958
/// updateuid : "8be565cbe42842cdb2d4da011f2d3fb4"
/// delflg : "00"
/// bjtime : "2022-03-07T11:49:15"

class ComSmarthomeBean {
  String shid;
  String companyid;
  String name;
  String brand;
  String logo;
  int createtime;
  String createuserid;
  int updatetime;
  String updateuid;
  String delflg;
  String bjtime;
  //00开 01关
  String watermark;
  String gps;
  String addrProvince;
  String addrCity;
  String addrDistrict;
  String addrCode;
  String address;
  //00每日更换 01固定一种
  String dayflg;
  //模式 00动态 01静态
  String type;
  int terCnt;

  String getTitle(){
    return "${brand??""}（${name??""}）";
  }


  bool isWaterOpen(){
    return "00" == watermark;
  }

  String getShowAddress(){
    if (addrProvince == addrCity) {
      return "${addrProvince??""}${addrDistrict??""}${address??""}";
    } else {
      return "${addrProvince??""}${addrCity??""}${addrDistrict??""}${address??""}";
    }
  }

  //获取水印状态
  String getWaterStatus(){
    if(isWaterOpen()){
      if("00" == dayflg){
        return "每日更换";
      }else{
        return "固定一种";
      }
    }else{
      return "未启用";
    }
  }

  //获取播放模式
  String getPlayStatus(){
    if("00" == type){
      return "动态模式";
    }
    return "静态模式";
  }

  static ComSmarthomeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComSmarthomeBean comSmarthomeBean = ComSmarthomeBean();
    comSmarthomeBean.shid = map['shid'];
    comSmarthomeBean.companyid = map['companyid'];
    comSmarthomeBean.name = map['name'];
    comSmarthomeBean.brand = map['brand'];
    comSmarthomeBean.logo = map['logo'];
    comSmarthomeBean.createtime = map['createtime'];
    comSmarthomeBean.createuserid = map['createuserid'];
    comSmarthomeBean.updatetime = map['updatetime'];
    comSmarthomeBean.updateuid = map['updateuid'];
    comSmarthomeBean.delflg = map['delflg'];
    comSmarthomeBean.bjtime = map['bjtime'];
    comSmarthomeBean.watermark = map['watermark'];
    comSmarthomeBean.gps = map['gps'];
    comSmarthomeBean.addrProvince = map['addrProvince'];
    comSmarthomeBean.addrCity = map['addrCity'];
    comSmarthomeBean.addrDistrict = map['addrDistrict'];
    comSmarthomeBean.addrCode = map['addrCode'];
    comSmarthomeBean.address = map['address'];
    comSmarthomeBean.dayflg = map['dayflg'];
    comSmarthomeBean.type = map['type'];
    comSmarthomeBean.terCnt = map['terCnt'];
    return comSmarthomeBean;
  }

  Map toJson() => {
    "shid": shid,
    "companyid": companyid,
    "name": name,
    "brand": brand,
    "logo": logo,
    "createtime": createtime,
    "createuserid": createuserid,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "watermark": watermark,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "addrCode": addrCode,
    "address": address,
    "dayflg": dayflg,
    "type": type,
    "terCnt": terCnt,
  };
}

/// orderflg : 0
/// name : "综合"
/// cnt : 0
/// stid : "fb91a63be65440cc84fc36de495ea23a"


class SpatialTypeListBean {
  int orderflg;
  String name;
  int cnt;
  int uploadcnt;//用户自传数量
  int examplecnt;//样例数量
  String stid;
  String defaultflg;

  String getStid(){
    if("全部" == name){
      return "";
    }
    return stid;
  }


  bool isShowExampleHint(){
    if(StringUtils.isEmpty(stid)){
      return false;
    }
      if(name != "全部"){
        if((examplecnt??0) > 0 && (uploadcnt??0) < 1){
          return true;
        }
      }
    return false;
  }

  String getCategoryName(){
    if(StringUtils.isEmpty(name)){
      return "其他";
    }
    return name;
  }

  static SpatialTypeListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SpatialTypeListBean spatialTypeListBean = SpatialTypeListBean();
    spatialTypeListBean.orderflg = map['orderflg'];
    spatialTypeListBean.name = map['name'];
    spatialTypeListBean.cnt = map['cnt'];
    spatialTypeListBean.stid = map['stid'];
    spatialTypeListBean.defaultflg = map['defaultflg'];
    spatialTypeListBean.uploadcnt = map['uploadcnt'];
    spatialTypeListBean.examplecnt = map['examplecnt'];
    return spatialTypeListBean;
  }

  Map toJson() => {
    "orderflg": orderflg,
    "name": name,
    "cnt": cnt,
    "uploadcnt": uploadcnt,
    "examplecnt": examplecnt,
    "stid": stid,
    "defaultflg": defaultflg
    ,
  };
}
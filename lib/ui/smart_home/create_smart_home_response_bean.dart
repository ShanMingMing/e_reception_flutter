/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : "2f885960e5454285a5457022565fda25"
/// extra : null

class CreateSmartHomeResponseBean {
  bool success;
  String code;
  String msg;
  String data;
  dynamic extra;

  static CreateSmartHomeResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CreateSmartHomeResponseBean createSmartHomeResponseBeanBean = CreateSmartHomeResponseBean();
    createSmartHomeResponseBeanBean.success = map['success'];
    createSmartHomeResponseBeanBean.code = map['code'];
    createSmartHomeResponseBeanBean.msg = map['msg'];
    createSmartHomeResponseBeanBean.data = map['data'];
    createSmartHomeResponseBeanBean.extra = map['extra'];
    return createSmartHomeResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}
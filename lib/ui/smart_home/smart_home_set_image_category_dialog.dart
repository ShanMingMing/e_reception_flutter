import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

class SmartHomeSetImageCategoryDialog extends StatefulWidget {
  final List<SpatialTypeListBean> categoryList;
  final String stid;
  final Function(SpatialTypeListBean typeListBean) onConfirm;

  const SmartHomeSetImageCategoryDialog({Key key, this.stid, this.categoryList, this.onConfirm})
      : super(key: key);

  @override
  _SmartHomeSetImageCategoryDialogState createState() =>
      _SmartHomeSetImageCategoryDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      String stid, List<SpatialTypeListBean> categoryList, Function(SpatialTypeListBean typeListBean) onConfirm) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SmartHomeSetImageCategoryDialog(
          stid: stid,
          categoryList: categoryList,
          onConfirm: onConfirm,
        )
    );
  }
}

class _SmartHomeSetImageCategoryDialogState
    extends BaseState<SmartHomeSetImageCategoryDialog> {
  SpatialTypeListBean _selectBean;
  List<SpatialTypeListBean> _categoryList = new List();
  @override
  void initState() {
    if(widget?.categoryList != null){
      for(int i = 0; i < widget?.categoryList?.length; i++){
        if("全部" != widget?.categoryList[i]?.name){
          _categoryList.add(widget?.categoryList[i]);
        }
      }
    }
    _selectBean = getSelectBean(widget?.stid);
    super.initState();
  }

  SpatialTypeListBean getSelectBean(String stid){
    SpatialTypeListBean bean;
    for(int i = 0; i < _categoryList?.length; i++){
      if(stid == _categoryList[i].stid){
        bean = _categoryList[i];
        break;
      }
    }
    return bean;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: ClipRRect(
        borderRadius:
        BorderRadius.only(topLeft: Radius.circular(12), topRight:Radius.circular(12)),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: _toMainColumnWidget()),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 54,
          alignment: Alignment.center,
          child: Text(
            "空间分类",
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w600,
              color: ThemeRepository.getInstance().getCardBgColor_21263C()
            ),
          ),
        ),
        _toGridWidget(),
      ],
    );
  }


  Widget _toGridWidget(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          bottom: 30
      ),
      itemCount: _categoryList?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        crossAxisSpacing: 8,
        mainAxisSpacing: 9,
        childAspectRatio: 80 / 32,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toStyleGridItemWidget(_categoryList?.elementAt(index));
      },
    );
  }

  ///网格item
  Widget _toStyleGridItemWidget(SpatialTypeListBean categoryBean){
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: _selectBean?.stid == categoryBean?.stid,
      width: 80,
      height: 32,
      radius: BorderRadius.circular(4),
      unSelectBgColor: Color(0xFFF6F7F9),
      selectedBgColor: Color(0xFFE7F3FF),
      selectedBoxBorder: Border.all(
          color: ThemeRepository.getInstance()
              .getPrimaryColor_1890FF(),
          width: 0.5),
      unSelectTextStyle: TextStyle(
        color: VgColors.INPUT_BG_COLOR,
        fontSize: 12,
      ),
      selectedTextStyle: TextStyle(
        color: ThemeRepository.getInstance()
            .getPrimaryColor_1890FF(),
        fontSize: 12,
      ),
      text: getName(categoryBean?.getCategoryName()??"-"),
      onTap: (){
          if(_selectBean?.stid == categoryBean?.stid){
            _selectBean = null;
          }else{
            _selectBean = categoryBean;
          }
          widget?.onConfirm?.call(_selectBean);
          RouterUtils.pop(context);

      },
    );
  }



  String getName(String name){
    if((name?.length??0) > 4){
      return name.substring(0, 4) + "...";
    }
    return name;
  }

}

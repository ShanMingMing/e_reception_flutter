import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"terInfo":[{"terminalName":"北京50","terminalPicurl":"http://etpic.we17.com/test/20220228164753_8254.jpg","hsn":"15CC8E20433E81F2BF2480D51244431133","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路14号靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"","position":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","onOff":1,"manager":"普通管理186","loginName":"岁"},{"terminalName":"北京三星pad","terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","companyid":"5097e6cbbe544fde99df5685871818cb","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"ABsx12345578","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"北京小米pad","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路14号靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"02","bindflg":"01","sideNumber":"","position":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","manager":"张啸","loginName":"岁"},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AAC0D2B","companyid":"5097e6cbbe544fde99df5685871818cb","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近小龙湾(地铁站)","terflg":"00","screenStatus":"02","bindflg":"01","sideNumber":"AB100000","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"","terminalPicurl":"","hsn":"15E60F87A8DD9783F7085D3214762EF4F2","companyid":"5097e6cbbe544fde99df5685871818cb","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近江宁开发区总部基地","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"AB1234567899877","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"北京55","terminalPicurl":"","hsn":"15C3130F5F1879EC29C1C7163646D322A8","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"AB20210916373","position":"","manager":"普通管理186","loginName":"普通管理186"}]}
/// extra : null

class SmartHomeTerminalListResponseBean {
  bool success;
  String code;
  String msg;
  TerminalListDataBean data;
  dynamic extra;

  static SmartHomeTerminalListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeTerminalListResponseBean smartHomeTerminalListResponseBeanBean = SmartHomeTerminalListResponseBean();
    smartHomeTerminalListResponseBeanBean.success = map['success'];
    smartHomeTerminalListResponseBeanBean.code = map['code'];
    smartHomeTerminalListResponseBeanBean.msg = map['msg'];
    smartHomeTerminalListResponseBeanBean.data = TerminalListDataBean.fromMap(map['data']);
    smartHomeTerminalListResponseBeanBean.extra = map['extra'];
    return smartHomeTerminalListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// terInfo : [{"terminalName":"北京50","terminalPicurl":"http://etpic.we17.com/test/20220228164753_8254.jpg","hsn":"15CC8E20433E81F2BF2480D51244431133","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路14号靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"","position":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","onOff":1,"manager":"普通管理186","loginName":"岁"},{"terminalName":"北京三星pad","terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","companyid":"5097e6cbbe544fde99df5685871818cb","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"ABsx12345578","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"北京小米pad","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路14号靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"02","bindflg":"01","sideNumber":"","position":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","manager":"张啸","loginName":"岁"},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AAC0D2B","companyid":"5097e6cbbe544fde99df5685871818cb","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近小龙湾(地铁站)","terflg":"00","screenStatus":"02","bindflg":"01","sideNumber":"AB100000","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"","terminalPicurl":"","hsn":"15E60F87A8DD9783F7085D3214762EF4F2","companyid":"5097e6cbbe544fde99df5685871818cb","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近江宁开发区总部基地","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"AB1234567899877","position":"","manager":"张啸","loginName":"岁"},{"terminalName":"北京55","terminalPicurl":"","hsn":"15C3130F5F1879EC29C1C7163646D322A8","companyid":"5097e6cbbe544fde99df5685871818cb","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","terflg":"00","screenStatus":"00","bindflg":"01","sideNumber":"AB20210916373","position":"","manager":"普通管理186","loginName":"普通管理186"}]

class TerminalListDataBean {
  List<TerInfoBean> terInfo;

  static TerminalListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalListDataBean dataBean = TerminalListDataBean();
    dataBean.terInfo = List()..addAll(
      (map['terInfo'] as List ?? []).map((o) => TerInfoBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "terInfo": terInfo,
  };
}

/// terminalName : "北京50"
/// terminalPicurl : "http://etpic.we17.com/test/20220228164753_8254.jpg"
/// hsn : "15CC8E20433E81F2BF2480D51244431133"
/// companyid : "5097e6cbbe544fde99df5685871818cb"
/// address : "北京市丰台区南三环西路辅路14号靠近中国工商银行(北京搜宝商务中心支行)"
/// terflg : "00"
/// screenStatus : "00"
/// bindflg : "01"
/// sideNumber : ""
/// position : "北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)"
/// onOff : 1
/// manager : "普通管理186"
/// loginName : "岁"

class TerInfoBean {
  String terminalName;
  String terminalPicurl;
  String hsn;
  String rasid;
  String companyid;
  String address;
  String terflg;//01 使用中
  String screenStatus;
  String bindflg;
  String sideNumber;
  String position;
  int onOff;
  String manager;
  String loginName;
  String logout;
  String reportHsnLastTime;
  String endday;
  int id;



  String getLoginStr() {
    if (loginName == null || loginName == "") {
      return null;
    }
    return "（$loginName账号登录）";
  }




  static TerInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerInfoBean terInfoBean = TerInfoBean();
    terInfoBean.terminalName = map['terminalName'];
    terInfoBean.terminalPicurl = map['terminalPicurl'];
    terInfoBean.hsn = map['hsn'];
    terInfoBean.rasid = map['rasid'];
    terInfoBean.companyid = map['companyid'];
    terInfoBean.address = map['address'];
    terInfoBean.terflg = map['terflg'];
    terInfoBean.screenStatus = map['screenStatus'];
    terInfoBean.bindflg = map['bindflg'];
    terInfoBean.sideNumber = map['sideNumber'];
    terInfoBean.position = map['position'];
    terInfoBean.onOff = map['onOff'];
    terInfoBean.manager = map['manager'];
    terInfoBean.loginName = map['loginName'];
    terInfoBean.logout = map['logout'];
    terInfoBean.id = map['id'];
    terInfoBean.endday = map['endday'];
    terInfoBean.reportHsnLastTime = map['reportHsnLastTime'];
    return terInfoBean;
  }

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }
  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }


  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "rasid": rasid,
    "companyid": companyid,
    "address": address,
    "terflg": terflg,
    "screenStatus": screenStatus,
    "bindflg": bindflg,
    "sideNumber": sideNumber,
    "position": position,
    "onOff": onOff,
    "manager": manager,
    "loginName": loginName,
    "logout": logout,
    "id": id,
    "endday": endday,
    "reportHsnLastTime": reportHsnLastTime,
  };
}
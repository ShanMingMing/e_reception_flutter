import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/index_nav/even/index_refresh_even.dart';
import 'package:e_reception_flutter/ui/mypage/mypage.dart';
import 'package:e_reception_flutter/ui/smart_home/index_nav_page/smart_home_my_page.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_bottom_navigation_bar/vg_bottom_navigation_bar.dart';
import 'package:e_reception_flutter/vg_widgets/vg_bottom_navigation_bar/vg_bottom_navigation_bar_item.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
import '../smart_home_store_list_page.dart';
import 'smart_home_terminal_manage_page.dart';

/// 智能家居导航页
class SmartHomeIndexNavPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomeIndexNavPage";
  final int currentPage;

  const SmartHomeIndexNavPage({Key key, this.currentPage}) : super(key: key);

  @override
  _SmartHomeIndexNavPageState createState() => _SmartHomeIndexNavPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      SmartHomeIndexNavPage(),
      routeName: SmartHomeIndexNavPage.ROUTER,
    );
  }
}

class _SmartHomeIndexNavPageState extends BaseState<SmartHomeIndexNavPage> with  WidgetsBindingObserver {
  static const DEFAULT_PAGE_POSITION = 0;

  int _currentPage = DEFAULT_PAGE_POSITION;

  ///控制器
  PreloadPageController _pageController;

  List<Widget> _pageList;

  ///底部项
  final List<VgBottomNavigationBarItem> bottomNavItems = [
    VgBottomNavigationBarItem(
      customUnselectWidget: _NavigationItemWidget(
        iconAsset: "images/nav_terminal_un_selected_sm_ico.png",
        text: "终端管理",
        textColor: ThemeRepository.getInstance().getSmUnSelectedColor_808388(),
        assetOpacity: 1,
      ),
      customSelectedWidget: _NavigationItemWidget(
        iconAsset: "images/nav_terminal_selected_sm_ico.png",
        text: "终端管理",
        textColor: ThemeRepository.getInstance().getSmSelectedColor_1890FF(),
        assetOpacity: 1,
      ),


    ),
    VgBottomNavigationBarItem(
        customUnselectWidget: _NavigationItemWidget(
          iconAsset: "images/nav_sm_un_selected_ico.png",
          text: "智能家居展示",
          textColor: ThemeRepository.getInstance().getSmUnSelectedColor_808388(),
          assetOpacity: 1,
        ),
        customSelectedWidget: _NavigationItemWidget(
          iconAsset: "images/nav_sm_selected_ico.png",
          text: "智能家居展示",
          textColor: ThemeRepository.getInstance().getSmSelectedColor_1890FF(),
          assetOpacity: 1,
        )
    ),
    VgBottomNavigationBarItem(
        customUnselectWidget: _NavigationItemWidget(
          iconAsset: "images/nav_my_sm_un_selected_ico.png",
          text: "我的",
          textColor: ThemeRepository.getInstance().getSmUnSelectedColor_808388(),
          assetOpacity: 1,
        ),
        customSelectedWidget: _NavigationItemWidget(
          iconAsset: "images/nav_my_sm_selected_ico.png",
          text: "我的",
          textColor: ThemeRepository.getInstance().getSmSelectedColor_1890FF(),
          assetOpacity: 1,
        )
    ),
  ];

  @override
  void initState() {
    super.initState();
      _currentPage = widget?.currentPage??DEFAULT_PAGE_POSITION;
    _pageController = PreloadPageController(
        initialPage: _currentPage, keepPage: true);
    _pageList = [SmartHomeTerminalManagePage(), SmartHomeStoreListPage(showBack: false,), SmartHomeMyPage()];
    ///加载高德定位插件
    AmapRepository.getInstance().init();
  }

  @override
  void dispose() {
    _pageController?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        break;
      case AppLifecycleState.resumed: //从后台切换前台，界面可见
        break;
      case AppLifecycleState.paused: // 界面不可见，后台
        break;
      case AppLifecycleState.detached: // APP结束时调用
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: _toStackWidget(),
    );
  }

  Widget _toStackWidget() {
    return Stack(
      children: <Widget>[
        _toPagesWidget(),
        Align(
          alignment: Alignment.bottomCenter,
          child: _toNavigatorBarWidget(),
        )
      ],
    );
  }

  Widget _toNavigatorBarWidget() {
    return Theme(
        data: ThemeData(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent),
        child: _toBottomNavigationBarWidget());
  }

  Widget _toPagesWidget() {
    return PreloadPageView(
      controller: _pageController,
      preloadPagesCount: _pageList?.length ?? 0,
      physics: NeverScrollableScrollPhysics(),
      children: _pageList,
    );
  }

  Widget _toBottomNavigationBarWidget() {
    return VgBottomNavigationBar(
      backgroundColor: ColorConstants.NAV_COLOR_WHITE,
      elevation: 15,
      selectedLabelStyle: TextStyle(
        // color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        fontSize: 10,
      ),
      unselectedLabelStyle: TextStyle(fontSize: 10),
      items: bottomNavItems,
      currentIndex: _currentPage,
      type: VgBottomNavigationBarType.fixed,
      onTap: (index) {
        _changePage(index);
        //点击
        if (index == 0) {
          VgEventBus.global.send(IndexRefreshEven());
        }
        if (index == 1) {
          VgEventBus.global.send(MonitoringMediaLibraryIndexClass());
        }
        if (index == 2) {
          VgEventBus.global.send(MonitoringAiPosterIndexClass());
        }
        if (index == 3) {
          VgEventBus.global.send(SubmitProposalInterfaceEventMonitor());
        }
      },
    );
  }

  ///更新页面
  _changePage(int index) {
    if (index == null || index < 0) {
      return;
    }
    _pageController?.jumpToPage(index);
    _currentPage = index;
    setState(() {});
  }

}

class _NavigationItemWidget extends StatelessWidget {
  final String iconAsset;
  final String text;
  final Color textColor;
  final double assetOpacity;
  final Color iconColor;

  final int redNumber;

  const _NavigationItemWidget(
      {Key key,
        this.iconAsset,
        this.text,
        this.textColor,
        this.redNumber,
        this.assetOpacity,
        this.iconColor})
      : assert(iconAsset != null && iconAsset != ""),
        assert(textColor != null),
        assert(text != null && text != ""),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: NAV_HEIGHT,
      alignment: Alignment.center,
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 26,
                height: 26,
                alignment: Alignment.center,
                child: AnimatedOpacity(
                  duration: DEFAULT_ANIM_DURATION,
                  opacity: assetOpacity ?? 1,
                  child: Image.asset(
                    iconAsset ?? "",
                    width: 26,
                    color: iconColor,
                    gaplessPlayback: true,
                  ),
                ),
              ),
              SizedBox(
                height: 1,
              ),
              AnimatedDefaultTextStyle(
                duration: DEFAULT_ANIM_DURATION,
                style: TextStyle(color: textColor, fontSize: 10),
                child: Text(
                  text ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
          Positioned(
              top: -4,
              left: 18,
              child: Offstage(
                offstage: redNumber == null || redNumber <= 0,
                child: Container(
                  height: 20,
                  constraints: BoxConstraints(minWidth: 20),
                  decoration: BoxDecoration(
                      color: Color(0xffFF455E),
                      borderRadius: BorderRadius.circular(10)),
                  padding: const EdgeInsets.symmetric(horizontal: 3),
                  child: Center(
                    child: Text(
                      "${redNumber ?? 0}",
                      style: TextStyle(
                          color: Colors.white, fontSize: 12, height: 1.3),
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_urgent_response_bean.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_detail_update.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_urgent_update.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/update_terminal_play_list_status_event.dart';
import 'package:e_reception_flutter/media_library/media_library_index/media_library_index_view_model.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/mixin/user_stream_mixin.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/building_index/refresh_diy_page_event.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_detail_page.dart';
import 'package:e_reception_flutter/ui/index/bean/index_bind_termainal_info.dart';
import 'package:e_reception_flutter/ui/index/bean/index_top_base_info.dart';
import 'package:e_reception_flutter/ui/index/even/clear_index_company_info_event.dart';
import 'package:e_reception_flutter/ui/index/even/location_changed_event.dart';
import 'package:e_reception_flutter/ui/index/index_view_model.dart';
import 'package:e_reception_flutter/ui/index/widgets/index_grid_terminal_widget.dart';
import 'package:e_reception_flutter/ui/index/widgets/index_guidancenotes_widget.dart';
import 'package:e_reception_flutter/ui/index/widgets/index_horizontal_vertical_play_list_widget.dart';
import 'package:e_reception_flutter/ui/index/widgets/index_manage_terminal_title_widget.dart';
import 'package:e_reception_flutter/ui/index_nav/even/index_refresh_even.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/play_file_update_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/terminal_info_update_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/terminal_number_update_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/common_terminal_settings_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_list_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_volume_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/terminal_scan_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/set_terminal_status_menu_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/bean/terminal_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/single_terminal_manage_page.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_index_event.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_mixin/navigator_page_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
import '../../app_main.dart';

/// 智能家居首页
class SmartHomeTerminalManagePage extends StatefulWidget {
  static const String ROUTER = "SmartHomeTerminalManagePage";
  static const String AUTO_LOGIN_TIME = "auto_login_time";

  SmartHomeTerminalManagePage({Key key}) : super(key: key);

  @override
  _SmartHomeTerminalManagePageState createState() => _SmartHomeTerminalManagePageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(context, SmartHomeTerminalManagePage(),
        routeName: SmartHomeTerminalManagePage.ROUTER);
  }
}

class _SmartHomeTerminalManagePageState extends BaseState<SmartHomeTerminalManagePage>
    with NavigatorPageMixin, UserStreamMixin {
  IndexViewModel _viewModel;
  NewTerminalListViewModel _terminalListViewModel;
  UserDataBean _user;

  bool isShowTips;

  StreamSubscription _streamSubscription;

  StreamSubscription _returnPageStreamSubscription;

  StreamSubscription _terminalNumRefreshSubscription;


  int _selectedIndex = 0;

  ScrollController _horizontalController;

  VgLocation _locationPlugin;

  StreamSubscription<ConnectivityResult> _netSubscription;
  //网络发生变化前，网络的状态
  bool _networkFlg = true;
  ValueNotifier<int> _initPageNotifier;

  //00不显示代表已经更新到 01显示
  String _showTerminalStatus = "00";
  String _hintHsn = "";


  networkRecoverAndDoSomeRefresh(){
    //之前网络的状态为无网络，并且网络状态变好了
    if(!_networkFlg){
      _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
      _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
    }
    _networkFlg = true;
  }

//网络初始状态
  connectivityInitState() {
    _netSubscription =
        Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
          print('netStatus' + result.toString());
          if(result == ConnectivityResult.none){
            _networkFlg = false;
          }else if(result == ConnectivityResult.mobile){
            networkRecoverAndDoSomeRefresh();
          }else if(result == ConnectivityResult.wifi){
            networkRecoverAndDoSomeRefresh();
          }
        });
  }

  IndexBindTermainalInfo _topInfo;
  TerminalDetailResponseBean _terminalDetailResponseBean;
  String _latestGps;
  String _punchAndTerminalInfoCacheKey;
  @override
  void initState() {
    super.initState();
    _viewModel = IndexViewModel(this);
    _punchAndTerminalInfoCacheKey = UserRepository.getInstance().getHomePunchAndTerminalInfoCacheKey();
    Future<String> latlng = VgLocationUtils.getCacheLatLngString();
    latlng.then((value){
      print("缓存gps:" + value);
      _latestGps = value;
      _viewModel.getHomeTerminalInfo(_selectedIndex, gps: value);
    });
    MediaLibraryIndexViewModel.EMPTY_FLAG = false;
    _terminalListViewModel = NewTerminalListViewModel(this);
    _locationPlugin = VgLocation();
    _locationPlugin.requestSingleLocation((data) {
      if(data.containsKey("latitude") && data.containsKey("longitude")){
        LatLngBean latLngBean = LatLngBean.fromMap(data);
        String gps = latLngBean.latitude.toString() + "," + latLngBean.longitude.toString();
        print("获取到了最新gps:" + gps);
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: gps);
      }
    });
    VgEventBus.global.streamController.stream.listen((event) {
      if (event is LocationChangedEvent) {
        print("收到gps更新的:" + event?.gps);
        if(StringUtils.isNotEmpty(event?.gps??"")){
          if(event?.gps == _latestGps){
            print("更新的gps与当前一致，不执行刷新");
            return;
          }
          _latestGps = event?.gps;
          _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
        }
      }
    });

    _initPageNotifier = new ValueNotifier(_selectedIndex);
    connectivityInitState();
    //获取终端信息
    _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
    _viewModel.getInitTerminalInfoAndPlayList(gps: _latestGps);
    //获取首页企业信息以及打卡信息
    _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
    _horizontalController= ScrollController();
    addPageListener(
      onPop: () {
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
        _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
      },
    );

    addUserStreamListen((value) {
      Future.delayed(Duration(milliseconds: 50), (){
        print("收到用户信息变化的回调");

        _viewModel.getHomeCompanyBaseInfoAndPunchInfo(authId: value?.authId);
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps, authId: value?.authId);
        setState(() { _selectedIndex = 0;});
      });
      // _viewModel?.getTopInfoHttp(_selectedIndex);
    });
    _streamSubscription =
        VgEventBus.global.on<IndexRefreshEven>().listen((event) {
          if (event is! IndexRefreshEven) {
            return;
          }
          if(StringUtils.isNotEmpty(event.hsn)){
            String hsn = event.hsn;
            if(_topInfo != null && _topInfo?.data?.bindTerminalList!= null
                && hsn != _topInfo?.data?.bindTerminalList[_selectedIndex]?.hsn){
              //不做刷新
            }else{
              _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
              _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
            }
          }else{
            _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
            _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
          }
        });


    _returnPageStreamSubscription = VgEventBus.global.on<MonitoringIndexPageClass>().listen((event) {
      _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
      _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
    });

    _terminalNumRefreshSubscription = VgEventBus.global.streamController.stream.listen((event) {
      if (event is PlayFileUpdateEvent
          || event is MediaLibraryRefreshEven
          || event is MediaDetailUpdateEvent
          || event is TerminalVolumeRefreshEvent
          || event is TerminalListRefreshEvent
          || event is TerminalInfoUpdateEvent
          || event is RefreshSmartHomeIndexEvent
          || event is RefreshDiyPageEvent
      ) {
        if(event is MediaLibraryRefreshEven){
          setState(() {
            if((_topInfo?.data?.bindTerminalList?.length??0) > _selectedIndex){
              _showTerminalStatus = "00";
              if(StringUtils.isNotEmpty(_topInfo?.data?.bindTerminalList[_selectedIndex]?.hsn??"")){
                _hintHsn = _topInfo?.data?.bindTerminalList[_selectedIndex]?.hsn;
              }
            }
          });
        }
        if(event is TerminalVolumeRefreshEvent || event is TerminalListRefreshEvent || (event is RefreshDiyPageEvent && (event?.isDelete??false))){
          _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps,);
          _viewModel.getHomeCompanyBaseInfoAndPunchInfo( hideShowLoad: true);
        }else{
          _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps,);
          _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
        }


      }
      if(event is TerminalNumberUpdateEvent){
        setState(() { _selectedIndex = 0;});
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps, unbindHsn: event?.hsn);
        _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
      }
      if(event is MediaUrgentUpdateEvent){
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
        _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
      }
      if(event is UpdateTerminalPlayListStatusEvent){
        setState(() {
          _showTerminalStatus = event.obtainflg;
          _hintHsn = event.hsn??"";
        });
      }
      if(event is ClearIndexCompanyInfoEvent){
        print("收到清空首页信息的指令:" + (event.getCache??false).toString());
        if(event.getCache??false){
          _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
          _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
        }else{
          _viewModel.clearHomePunchInfoCache();
          _viewModel.clearHomeTerInfo();
          _viewModel.clearHomeBaseInfoCache();
        }
      }
    });

    _getSPValue();

  }

  @override
  void dispose() {
    //网络结束监听
    _netSubscription?.cancel();
    _terminalNumRefreshSubscription?.cancel();
    _streamSubscription?.cancel();
    _returnPageStreamSubscription.cancel();
    _initPageNotifier?.dispose();
    _terminalListViewModel.onDisposed();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _toScaffoldWidget();
  }

  @override
  void didChangeDependencies() {
    _user = UserRepository.getInstance().of(context);
    super.didChangeDependencies();
  }

  Widget _toScaffoldWidget() {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isIOS?
      SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.light)
          :SystemUISetting.smLight,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        backgroundColor: Color(0XFFF6F7F9),
        body: _toMainColumnWidget(),
      ),
    );
  }

  Widget _toCompanyInfoWidget(){
    return ValueListenableBuilder(
      valueListenable: _viewModel?.indexTopBaseValueNotifier,
      builder: (BuildContext context, IndexTopBaseInfo topBaseInfo, Widget child) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: (){
            _setTips();
            CompanyDetailPage.navigatorPush(context, _latestGps);
          },
          child: Container(
            height: 76,
            color: ThemeRepository.getInstance().getSmSelectedColor_1890FF(),
            padding: const EdgeInsets.only(left: 15, top: 6, bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _toCompanyNameAndGoWidget(topBaseInfo),
                SizedBox(height: 4,),
                _toRowPersonNameWidget(topBaseInfo),
              ],
            ),
          ),
        );
      },
    );
  }

  ///企业名+箭头
  Widget _toCompanyNameAndGoWidget(IndexTopBaseInfo topBaseInfo) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Row(
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: ScreenUtils.screenW(context) - 151,
                ),
                child: Text(VgStringUtils.subStringAndAppendSymbol(
                    topBaseInfo?.data?.companyInfo?.companynick, 12,
                    symbol: "...") ?? "(公司名称)",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      height: 1.2,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                width: 6.5,
              ),
              Image.asset(
                "images/index_arrow_ico.png",
                width: 8,
                color: Colors.white,
              ),
              SizedBox(
                width: 6.5,
              ),
            ],
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async{
            bool result = await TerminalScanPage.navigatorPush(context, _latestGps, AppMain.ROUTER);
            if(result != null){
              _viewModel.getHomeTerminalInfo(0, gps: _latestGps);
            }
          },
          child: Container(
              width: 50,
              height: 28,
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Center(
                child: Image.asset(
                  "images/icon_scan_code.png",
                  color: Colors.white,
                  width: 20,
                ),
              )),
        ),

      ],
    );
  }

  ///人名
  Widget _toRowPersonNameWidget(IndexTopBaseInfo topBaseInfo) {
    final bool isSuperAdmin = VgRoleUtils.isSuperAdmin(topBaseInfo?.data?.roleid);
    return Row(
      children: <Widget>[
        Image.asset(
          "images/index_person_label_ico.png",
          width: 12,
          color: isSuperAdmin ? null: Color(0xFFFFFFFF),
        ),
        SizedBox(
          width: 5,
        ),
        CommonConstraintMaxWidthWidget(
          maxWidth: (VgToolUtils.getScreenWidth() /3)*2,
          child: Text(
            "${VgStringUtils.subStringAndAppendSymbol(
                _user?.comUser?.name, 7,
                symbol: "...") ?? "-"}"
                "·"
                "${isSuperAdmin ?"超级管理员" :"普通管理员"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Color(0xFFFFFFFF), fontSize: 13, height: 1.2),
          ),
        ),
      ],
    );
  }

  //管理终端
  Widget _toIndexManageTerminalBarWidget(){
    return ValueListenableBuilder(
        valueListenable: _viewModel?.bindTermainalInfoValueNotifier,
        builder: (BuildContext context, IndexBindTermainalInfo topInfo, Widget child) {
          _topInfo = topInfo;
          int bindTerminalList = topInfo?.data?.bindTerminalList?.length ?? 0;
          int newTerminalCnt = topInfo?.data?.newTerminalCnt ?? 0;
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () async{
              if (bindTerminalList == 0 && newTerminalCnt == 0) {
                return;
              }
              if(Platform.isAndroid){
                if(StringUtils.isNotEmpty(_latestGps)){
                  NewTerminalListPage.navigatorPush(context, gps: _latestGps);
                  print("使用首页的gps:" + _latestGps);
                  return;
                }
                if (await VgLocation().checkPermission() == false) {
                  NewTerminalListPage.navigatorPush(context);
                  print("无gps权限");
                }else{
                  VgLocation().requestSingleLocation((value) {
                    Future<String> latlng = VgLocationUtils.getCacheLatLngString();
                    latlng.then((value){
                      print("gps:" + value);
                      NewTerminalListPage.navigatorPush(context, gps: value);
                      // _clickFlag = true;
                    });
                  });
                }
              }else{
                NewTerminalListPage.navigatorPush(context, gps: _latestGps);
              }
            },
            child: Container(
              height: 50,
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: <Widget>[
                  Text(
                    // "终端显示屏·${topInfo?.bindTerminalList?.length ?? 0}",
                    "终端显示屏·",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getCardBgColor_21263C(),
                        fontSize: 17,
                        height: 1.22),
                  ),
                  Text(
                    "${topInfo?.data?.powerOnCnt??0.toString().toString()}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ((topInfo?.data?.bindTerminalList?.length ?? 0)==0)?ThemeRepository.getInstance().getCardBgColor_21263C():ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                        fontSize: 17,
                        height: 1.2),
                  ),
                  Visibility(
                    visible: (topInfo?.data?.bindTerminalList?.length ?? 0)> 0,
                    child: Text(
                      "/",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                          ThemeRepository.getInstance().getCardBgColor_21263C(),
                          fontSize: 17,
                          height: 1.2),
                    ),
                  ),
                  Visibility(
                    visible: (topInfo?.data?.bindTerminalList?.length ?? 0)> 0,
                    child: Text(
                      "${topInfo?.data?.bindTerminalList?.length ?? 0}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                          ThemeRepository.getInstance().getCardBgColor_21263C(),
                          fontSize: 17,
                          height: 1.2),
                    ),
                  ),
                  Spacer(),
                  Text(
                    ((bindTerminalList??0) > 0) ? "设备管理" : "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style:
                    TextStyle(color: ThemeRepository.getInstance().getCardBgColor_21263C(), fontSize: 14, height: 1.22),
                  ),
                  // _toRedDotWidget()
                ],
              ),
            ),
          );
        }
    );
  }

  //播放列表
  Widget _playListWidget(){
    return ValueListenableBuilder(
        valueListenable: _viewModel?.bindTermainalInfoValueNotifier,
        builder: (BuildContext context, IndexBindTermainalInfo topInfo, Widget child) {
          _topInfo = topInfo;
          SharePreferenceUtil.putString(SP_ALL_TERMINAL_HSNS, _getHsns(_topInfo?.data?.bindTerminalList));
          return judgeTerminalState(topInfo) ?IndexGuidanceNotesWidget(topInfo:topInfo,viewModel :_viewModel,gps: _latestGps, lightTheme: true,): Expanded(child: _contentDisplay(topInfo));
        }
    );
  }

  Widget _toMainColumnWidget() {
    int urgentWidth = VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId())?208:161;
    return Stack(
      children: [
        Column(
          children: <Widget>[
            Container(
                height: ScreenUtils.getStatusBarH(context),
              color: ThemeRepository.getInstance().getSmSelectedColor_1890FF(),
            ),
            _toNetworkStatusWidget(),
            ///公司信息
            _toCompanyInfoWidget(),
            ///管理终端
            _toIndexManageTerminalBarWidget(),
            _playListWidget(),
          ],
        ),
      ],
    );
    //   },
    // );
  }

  /// 请求客户端
  Future<bool> checkNetwork()async{
    final dio = Dio();
    try {
      Response response = await dio.get(
        "http://123.207.32.32:8000/api/v1/recommend",
      );
      if (response.statusCode == HttpStatus.ok) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }catch(exception){
      return false;
    }
  }

  ///弱网显示
  Widget _toNetworkStatusWidget(){
    return FutureBuilder(
        future: (Connectivity().checkConnectivity()),
        builder: (context, snapshot) {
          ConnectivityResult result;
          if (snapshot.connectionState == ConnectionState.done) {
            result = snapshot.data;
          }
          return StreamBuilder(
            stream: Connectivity().onConnectivityChanged,
            builder: (context,  snapshot) {
              if(snapshot.connectionState == ConnectionState.active){
                result = snapshot.data;
              }
              return FutureBuilder(
                future: checkNetwork(),
                builder: (context, connectState){
                  print("result:" + result.toString());
                  print("connectState?.data:" + (connectState?.data??true).toString());
                  return Visibility(
                    visible: (ConnectivityResult.none == result || !(connectState?.data??true)),
                    child: Container(
                      color: Color(0x33F95355),
                      height: 40,
                      child: Row(
                        children: [
                          SizedBox(width: 15,),
                          Image.asset(
                            "images/icon_tip_red.png",
                            width: 16,
                            height: 16,
                          ),
                          SizedBox(width: 6,),
                          Text(
                            "当前网络不可用，请检查你的网络",
                            style: TextStyle(
                              fontSize: 13,
                              color: Color(0xFFF95355),
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: (){
                              _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
                              _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
                            },
                            child: Container(
                              width: 50,
                              alignment: Alignment.center,
                              child: Text(
                                "刷新",
                                style: TextStyle(
                                  fontSize: 11,
                                  color: Color(0xFFBFC2CC),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );

            },
          );
        }
    );


  }

  ///显示屏网络不佳提示
  Widget _toTerminalUpdatePlayListStatusWidget(NewBindTerminalListBean listBean){
    String days;
    if(listBean.getTerminalIsOff()){
      if(StringUtils.isNotEmpty(listBean?.reportHsnLastTime??"")) {
        String currentTimeStr = DateUtil.getNowDateStr();
        int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
        int time = DateTime.parse(listBean?.reportHsnLastTime).millisecondsSinceEpoch;
        int interval = (currentTime - time) ~/ (24 * 60 * 60 * 1000);
        if (interval > 2) {
          days = "已关机${interval}天";
        }else{
          days = "已关机";
        }
      }else{
        days = "已关机";
      }
      return Container(
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.only(bottom: 11),
        height: 30,
        padding: EdgeInsets.only(left: 15, right: 15),
        decoration: BoxDecoration(
          color: Color(0x33F95355),
        ),
        child: Text(
          "${listBean?.getTerminalName()??"当前设备"}：${days}",
          style: TextStyle(
              fontSize: 12,
              color: ThemeRepository.getInstance().getMinorRedColor_F95355()
          ),
        ),
      );
    }
    return (("00" != _showTerminalStatus) && (_hintHsn == (listBean?.hsn??"")))?
    Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(bottom: 11),
      height: 30,
      padding: EdgeInsets.only(left: 15, right: 15),
      decoration: BoxDecoration(
        color: Color(0x33F95355),
      ),
      child: Text(
        "注：当前显示屏网络状况不佳，播控数据指令未完全同步",
        style: TextStyle(
            fontSize: 12,
            color: ThemeRepository.getInstance().getMinorRedColor_F95355()
        ),
      ),
    ): SizedBox();
  }



  bool judgeTerminalState(IndexBindTermainalInfo topInfo){
    //是否有新终端
    int _newTerminalCnt = topInfo?.data?.newTerminalCnt ?? 0;
    //终端列表是否有数据
    int _bindTerminalList = topInfo?.data?.bindTerminalList?.length ?? 0;
    //终端列表没有数据
    if(_bindTerminalList==0){
      return true;
    }
    return false;
  }

  Widget _contentDisplay(IndexBindTermainalInfo topInfo) {
    return VgPlaceHolderStatusWidget(
      emptyStatus:
      topInfo?.data?.bindTerminalList == null || topInfo?.data?.bindTerminalList?.isEmpty,
      emptyOnClick: ()=>_viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps),
      child: ScrollConfiguration(
        behavior: MyBehavior(),
        child: NestedScrollView(
          physics: BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverToBoxAdapter(
                child: _judgeTerminalDisplay(topInfo),
              ),
            ];
          },
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 4,
                ),
                _getPlayList(topInfo),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _judgeTerminalDisplay(IndexBindTermainalInfo topInfo) {
    if(_selectedIndex != _initPageNotifier?.value){
      _initPageNotifier?.value = _selectedIndex;
    }
    return Container(
      height: ((ScreenUtils.screenW(context) - 15 - 16) / 2.5 + 12),
      color: Colors.white,
      child: IndexGridTerminalWidget(
        list: topInfo?.data?.bindTerminalList,
        selectIndex: _selectedIndex,
        onSelect: (index) {
          //更换今日播放内容
          String hsn = topInfo?.data?.bindTerminalList[index].hsn;
          _viewModel.getTerminalDetail(hsn);
          setState(() {
            _hintHsn = hsn;
            _selectedIndex = index;
            _showTerminalStatus = "00";
          });
          if(_horizontalController != null && _horizontalController.hasClients){
            _horizontalController.jumpTo(0);
          }
        },
        cancelRepeat: _onCancelRepeatByTerminal,
        selectValue: _initPageNotifier,
        onOff: _onOff,
        lightTheme: true,
        // setVolume: _setVolume,
      ),
    );
  }

  int _getIndex(TerminalDetailListItemBean itemBean){

    int index = 0;
    for(int i = 0; i < _terminalDetailResponseBean.data.list.length; i++){
      TerminalDetailListItemBean temp = _terminalDetailResponseBean.data.list[i];
      if(itemBean.isIndex() && (itemBean.buildingIndexId == temp.buildingIndexId)){
        index = i;
        break;
      }else if(itemBean.isSmart() && (itemBean.rasid == temp.rasid)){
        index = i;
        break;
      } else{
        if((itemBean?.picid??"-1") == temp.picid){
          index = i;
          break;
        }
      }
    }
    return index;
  }

  _onRepeat(TerminalDetailListItemBean itemBean) {
    int index = _getIndex(itemBean);
    _terminalDetailResponseBean.data.list.forEach((element) {
      //如果此次操作是设置重复，那么要将原来的数据都置为不重复，防止两个重复标出现的状况
      element.stopflg = "00";
    });
    if("01" == itemBean.stopflg){
      itemBean.stopflg = "00";
    }else{
      itemBean.stopflg = "01";
    }
    _terminalDetailResponseBean.data.list[index] = itemBean;
    _viewModel.detailValueNotifier.value = _terminalDetailResponseBean;
    String detailCacheKey = NetApi.TERMINAL_PLAY_LIST_API + UserRepository.getInstance().authId ?? ""
        + itemBean.hsn ?? "";

    SharePreferenceUtil.putString(detailCacheKey, json.encode(_terminalDetailResponseBean));

    _topInfo?.data?.bindTerminalList?.forEach((element) {
      if(itemBean.hsn == element.hsn){
        if("01" == element.stopflg){
          element.stopflg = "00";
        }else{
          element.stopflg = "01";
        }
        _viewModel.bindTermainalInfoValueNotifier.value = _topInfo;
        // IndexTopInfoResponseBean topInfoResponseBean = new IndexTopInfoResponseBean();
        // topInfoResponseBean.data = _topInfo;
        // topInfoResponseBean.success = true;
        // topInfoResponseBean.code = "200";
        // topInfoResponseBean.msg = "处理成功";
        SharePreferenceUtil.putString(_punchAndTerminalInfoCacheKey, json.encode(_topInfo));
      }
    });

    if(itemBean.isIndex()){
      _viewModel?.repeatBuildingIndex(context, itemBean.hsn, itemBean.buildingIndexId, itemBean.stopflg);
    }else if(itemBean.isSmart()){
      _viewModel?.repeatSmartHome(context, itemBean.hsn, itemBean.rasid, itemBean.stopflg);
    }else{
      _viewModel?.repeatPlay(context, itemBean.picid, itemBean.editStopFlg,
          itemBean.id, itemBean.hsn);
    }
  }


  _onOff(bool offStatus, String hsn, int id, String terminalName, ComAutoSwitchBean autoBean) async{
    SetTerminalStatusMenuDialog.navigatorPushDialog(context,
          (currentState){
        _terminalListViewModel.terminalOffScreen(context, hsn, "00", callback: (){
        });
      },
          (currentState){
        _terminalListViewModel.terminalOffScreen(context, hsn, "01", callback: (){
        });
      },
          (currentState){
        _terminalListViewModel.terminalOff(context, hsn, callback: (){
        });
      },
          (time){

      },
      offStatus?SetTerminalStateType.off:SetTerminalStateType.open,
      "",
      "",
      hsn,
      id,
      terminalName,
      "",
      autoBean,
    );
  }

  _onCancelRepeatByTerminal(String hsn) {
    if (StringUtils.isEmpty(hsn)) {
      VgToastUtils.toast(context, "设备信息异常");
      return;
    }
    _topInfo?.data?.bindTerminalList?.forEach((element) {
      if(hsn == element.hsn){
        element.stopflg = "00";
      }
    });
    _viewModel.bindTermainalInfoValueNotifier.value = _topInfo;
    SharePreferenceUtil.putString(_punchAndTerminalInfoCacheKey, json.encode(_topInfo));
    if(hsn == _terminalDetailResponseBean.data.hsnMap.hsn){
      for(int i = 0; i < _terminalDetailResponseBean.data.list.length; i++){
        if("00" != _terminalDetailResponseBean.data.list[i].stopflg){
          _terminalDetailResponseBean.data.list[i].stopflg = "00";
          _viewModel.detailValueNotifier.value = _terminalDetailResponseBean;
          String detailCacheKey = NetApi.TERMINAL_PLAY_LIST_API + UserRepository.getInstance().authId ?? ""
              + hsn ?? "";
          SharePreferenceUtil.putString(detailCacheKey, json.encode(_terminalDetailResponseBean));
          break;
        }
      }
    }
    _viewModel?.cancelRepeatPlayByHsn(context, hsn);
  }

  String _getHsns(List<NewBindTerminalListBean> list){
    if(list == null || list.isEmpty){
      return "";
    }
    return list.map((e) => e.hsn).toList().join(",");
  }

  Widget _getPlayList(IndexBindTermainalInfo topInfo) {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.detailValueNotifier,
      builder: (BuildContext context,
          TerminalDetailResponseBean detailResponseBean, Widget child) {
        Map map = json.decode(json.encode(detailResponseBean));
        _terminalDetailResponseBean = TerminalDetailResponseBean.fromMap(map, isCache: true);

        if(topInfo != null && topInfo.data.bindTerminalList != null && _selectedIndex >= topInfo.data.bindTerminalList.length){
          _selectedIndex = 0;
          _initPageNotifier?.value = _selectedIndex;
          _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
          _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
        }
        NewBindTerminalListBean listBean = topInfo.data?.bindTerminalList[_selectedIndex];
        return Container(
          color: Color(0XFFF6F7F9),
          child: Column(
            children: <Widget>[
              //今日播放
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  if(UserRepository.getInstance().isSmartHomeCompany()){
                    return;
                  }
                  NewBindTerminalListBean listBean = topInfo.data?.bindTerminalList[_selectedIndex];
                  SingleTerminalMangePage.navigatorPush(
                    context,
                    listBean.hsn,
                    _getHsns(topInfo.data?.bindTerminalList),
                    listBean?.getTerminalName(),
                    listBean?.branchname,
                    listBean?.cbid,
                    listBean.getTerminalState(), listBean.rcaid,
                    onOffTime: listBean.autoOnoffTime,
                    onOffWeek: listBean.autoOnoffWeek,
                    autoOnOff: listBean?.autoOnoff,
                    autoBean: listBean?.comAutoSwitch,
                  );
                },
                child: Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "今日播放文件·${(detailResponseBean?.data?.list?.length??0)}",
                        style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance()
                              .getCardBgColor_21263C(),
                        ),
                      ),
                      Spacer(),
                      Visibility(
                        visible: !UserRepository.getInstance().isSmartHomeCompany(),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              "images/icon_add_pic.png",
                              width: 11,
                              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                            ),
                            SizedBox(width: 3,),
                            Text(
                              "添加",
                              style: TextStyle(
                                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _toTerminalUpdatePlayListStatusWidget(listBean),
              true
                  ? _getHorizontalList(
                  detailResponseBean?.data?.list ?? null, topInfo ?? null)
                  : _getVerticalList(detailResponseBean?.data?.list ?? null,
                  topInfo ?? null),
              SizedBox(height: 20,)
            ],
          ),
        );
      },
    );
  }

  Widget _getHorizontalList(List<TerminalDetailListItemBean> terminalList,
      IndexBindTermainalInfo topInfo) {
    String hsn = (topInfo == null)?"" : topInfo.data.bindTerminalList[_selectedIndex].hsn;
    String terminalName = (topInfo == null)? "" : topInfo.data.bindTerminalList[_selectedIndex].terminalName;
    bool isOff = (topInfo == null)? false : topInfo.data.bindTerminalList[_selectedIndex].getTerminalIsOff();
    String position = (topInfo == null)? "" : topInfo.data.bindTerminalList[_selectedIndex].branchname;
    String cbid = (topInfo == null)? "" : topInfo.data.bindTerminalList[_selectedIndex].cbid;
    return Container(
      height: ((ScreenUtils.screenW(context) - 15 - 16) / 2.5) * 244 / 137,
      child: IndexHorizontalVerticalPlayListWidget(
        list: terminalList,
        direction: 0,
        hsn:_hintHsn,
        initOffSet: 0.0,
        controller: _horizontalController,
        onRepeat: _onRepeat,
        terminalName: terminalName,
        isOff: isOff,
        // position: _terminalDetailResponseBean?.data?.hsnMap?.position??"",
        position: position??"",
        cbid: cbid??"",
        showRepeat: false,
      ),
    );
  }


  Widget _getVerticalList(List<TerminalDetailListItemBean> terminalList,
      IndexBindTermainalInfo topInfo) {
    return Expanded(
        child: IndexHorizontalVerticalPlayListWidget(
          list: terminalList,
          direction: 1,
          initOffSet: 0.0,
          showRepeat: false,
        ));
  }

  void _getSPValue() {
    _getTips().then((value) {
      isShowTips = value ?? true;
    });
    setState(() {});
  }

  ///设置缓存提示
  void _setTips() {
    if (isShowTips == false) {
      return;
    }
    SharePreferenceUtil.putBool(
        UserRepository.getInstance().userData?.user?.loginphone, false);
    isShowTips = false;
    setState(() {});
  }

  ///获取提示缓存
  Future<bool> _getTips() {
    return SharePreferenceUtil.getBool(
        UserRepository.getInstance().userData?.user?.loginphone);
  }

  /// 定位权限
  Future<String> requestLocation() async {
    PermissionStatus locationPermission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);
    if (locationPermission == PermissionStatus.granted) {
      return "";
    } else {
      Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler().requestPermissions([
        PermissionGroup.location,
      ]);
      bool give =
          permissions[PermissionGroup.location] == PermissionStatus.granted;
      if(give){
        return "";
      }else {
        return "请在设置界面给予App定位权限";
      }
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        break;
      case AppLifecycleState.resumed: //从后台切换前台，界面可见
        VgLocation().requestSingleLocationWithoutCheckPermission((value){
          print("回到前台，请求更新位置");
        });
        connectivityInitState();
        _viewModel.getHomeTerminalInfo(_selectedIndex, gps: _latestGps);
        _viewModel.getHomeCompanyBaseInfoAndPunchInfo();
        VgEventBus.global.send(AppRefreshEvent());
        break;
      case AppLifecycleState.paused: // 界面不可见，后台
        break;
      case AppLifecycleState.detached: // APP结束时调用
        break;
    }
  }
}

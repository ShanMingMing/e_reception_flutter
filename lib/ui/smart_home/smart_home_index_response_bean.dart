import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"matype":"01","logodiy":"","picsize":94,"calx":"","caly":"","cretype":"07","effecturl":"http://etpic.we17.com/test/20210608093439_1129.jpg","htmlurl":"http://etpic.we17.com/test/20210802101431_5380.html","calflg":"00","namediy":"","psdurl":"[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]","addressdiy":"","picurl":"http://etpic.we17.com/test/20210608094231_1804.jpg","colour":"","pictype":"05","colorflg":"00","theme":"01","updatetime":"2021-08-12 11:39:31","picid":"af648b086e4c4d5c93b073d034c0be60"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class SmartHomeIndexResponseBean extends BasePagerBean<SmartHomeIndexListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;

  static SmartHomeIndexResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeIndexResponseBean smartHomeIndexResponseBeanBean = SmartHomeIndexResponseBean();
    smartHomeIndexResponseBeanBean.success = map['success'];
    smartHomeIndexResponseBeanBean.code = map['code'];
    smartHomeIndexResponseBeanBean.msg = map['msg'];
    smartHomeIndexResponseBeanBean.data = DataBean.fromMap(map['data']);
    return smartHomeIndexResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
  };
  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<SmartHomeIndexListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"matype":"01","logodiy":"","picsize":94,"calx":"","caly":"","cretype":"07","effecturl":"http://etpic.we17.com/test/20210608093439_1129.jpg","htmlurl":"http://etpic.we17.com/test/20210802101431_5380.html","calflg":"00","namediy":"","psdurl":"[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]","addressdiy":"","picurl":"http://etpic.we17.com/test/20210608094231_1804.jpg","colour":"","pictype":"05","colorflg":"00","theme":"01","updatetime":"2021-08-12 11:39:31","picid":"af648b086e4c4d5c93b073d034c0be60"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"matype":"01","logodiy":"","picsize":94,"calx":"","caly":"","cretype":"07","effecturl":"http://etpic.we17.com/test/20210608093439_1129.jpg","htmlurl":"http://etpic.we17.com/test/20210802101431_5380.html","calflg":"00","namediy":"","psdurl":"[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]","addressdiy":"","picurl":"http://etpic.we17.com/test/20210608094231_1804.jpg","colour":"","pictype":"05","colorflg":"00","theme":"01","updatetime":"2021-08-12 11:39:31","picid":"af648b086e4c4d5c93b073d034c0be60"}]
/// total : 1
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<SmartHomeIndexListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
        (map['records'] as List ?? []).map((o) => SmartHomeIndexListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// matype : "01"
/// logodiy : ""
/// picsize : 94
/// calx : ""
/// caly : ""
/// cretype : "07"
/// effecturl : "http://etpic.we17.com/test/20210608093439_1129.jpg"
/// htmlurl : "http://etpic.we17.com/test/20210802101431_5380.html"
/// calflg : "00"
/// namediy : ""
/// psdurl : "[{\"cover\":\"http://etpic.we17.com/test/20210812113923_8854.png\",\"data\":[{\"left\":0,\"top\":0,\"width\":1080,\"height\":1920,\"opacity\":1,\"name\":\"背景\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113915_7147.png\"},{\"left\":-579,\"top\":-80,\"width\":2339,\"height\":2000,\"opacity\":1,\"name\":\"图层 1\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_0591.png\"},{\"left\":911,\"top\":51,\"width\":121,\"height\":121,\"opacity\":1,\"name\":\"src=http___s11_sinaimg_cn_bmiddle_4c7d339ffbbfcb3e5cd6a&refer=http___s11_sinaimg\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1125.png\"},{\"left\":918,\"top\":198,\"width\":53,\"height\":504,\"opacity\":1,\"name\":\"蔚 来 艺 术 培 训 中 心\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_6564.png\"},{\"left\":991,\"top\":198,\"width\":35,\"height\":1036,\"opacity\":1,\"name\":\"北京市朝阳区朝阳公园路6号蓝色港湾国际商区1号楼DS130号\",\"type\":\"02\",\"content\":\"http://etpic.we17.com/test/20210812113921_1507.png\"}],\"psd\":\"http://etpic.we17.com/test/20210812113923_0630.psd\",\"size\":\"17935\",\"createname\":\"王凡语\",\"createtime\":\"2021-08-12 11:39:31\"}]"
/// addressdiy : ""
/// picurl : "http://etpic.we17.com/test/20210608094231_1804.jpg"
/// colour : ""
/// pictype : "05"
/// colorflg : "00"
/// theme : "01"
/// updatetime : "2021-08-12 11:39:31"
/// picid : "af648b086e4c4d5c93b073d034c0be60"

class SmartHomeIndexListItemBean {
  String shid;
  String brand;
  String name;
  String logo;
  List<PicListBean> picList;
  int picCnt;
  int terCnt;
  //00开 01关
  String watermark;

  String getTitle(){
    return "${brand??""}（${name??""}）";
  }

  static SmartHomeIndexListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeIndexListItemBean smartHomeStoreListBean = SmartHomeIndexListItemBean();
    smartHomeStoreListBean.shid = map['shid'];
    smartHomeStoreListBean.brand = map['brand'];
    smartHomeStoreListBean.name = map['name'];
    smartHomeStoreListBean.logo = map['logo'];
    smartHomeStoreListBean.picList = List()..addAll(
        (map['picList'] as List ?? []).map((o) => PicListBean.fromMap(o))
    );
    smartHomeStoreListBean.picCnt = map['picCnt'];
    smartHomeStoreListBean.terCnt = map['terCnt'];
    smartHomeStoreListBean.watermark = map['watermark'];
    return smartHomeStoreListBean;
  }

  String getImageInfo(){
    return "${picCnt??0}图/";
  }

  String getTerInfo(){
    if(((terCnt??0)>0)){
      return "${terCnt??0}台播放终端";
    }
    return "暂无终端显示屏";
  }


  Map toJson() => {
    "shid": shid,
    "brand": brand,
    "name": name,
    "logo": logo,
    "picList": picList,
    "picCnt": picCnt,
    "terCnt": terCnt,
    "watermark": watermark,
  };
}

class PicListBean {
  String picurl;
  String picsize;
  String picstorage;
  int videotime;
  String type;
  String videopic;
  String coverurl;
  //是否是样例 01
  String example;

  bool isExample(){
    return "01" == example;
  }

  static PicListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PicListBean picListBean = PicListBean();
    picListBean.picurl = map['picurl'];
    picListBean.picsize = map['picsize'];
    picListBean.picstorage = map['picstorage'];
    picListBean.videotime = map['videotime'];
    picListBean.type = map['type'];
    picListBean.videopic = map['videopic'];
    picListBean.example = map['example'];
    picListBean.coverurl = map['coverurl'];
    return picListBean;
  }

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    if(is3DLink()){
      return coverurl;
    }
    return picurl;
  }

  bool is3DLink(){
    return type == "03";
  }

  Map toJson() => {
    "picurl": picurl,
    "picsize": picsize,
    "picstorage": picstorage,
    "videotime": videotime,
    "type": type,
    "videopic": videopic,
    "example": example,
    "coverurl": coverurl,
  };
}
class RefreshStoreNameEvent {
  final String brandName;
  final String storeName;
  final String logo;
  RefreshStoreNameEvent({this.brandName, this.storeName, this.logo});
}
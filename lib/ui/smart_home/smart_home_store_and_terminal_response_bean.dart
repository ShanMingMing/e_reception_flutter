import 'package:e_reception_flutter/ui/smart_home/smart_home_terminal_list_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"watermark":"00","shid":"ac108f8f736648b7aeca2531f9e0638c","name":"双龙大道点好好的好的","logo":"https://etpic.we17.com/test/20220722103915_7857.jpg","brand":"顾家家具大大方方","improveLogo":"https://etpic.we17.com/test/20220722104003_4779.png","terList":[{"terminalPicurl":"","hsn":"159562CBB68188C113A69E68D20F2F1133","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:06:47","screenStatus":"00","endday":"2023-06-06","sideNumber":"","id":2167,"position":"","onOff":1,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE22AEB71F2ACF","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-21 09:54:24","screenStatus":"00","endday":"2023-07-15","juli":"68","id":2183,"position":"手机","onOff":0,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"15E60F87A8DD9783F7085D3214762EF4F2","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近如家商旅酒店(南京百家湖小龙湾地铁站店)","bindflg":"01","terminalName":"终端显示屏2","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:02:45","screenStatus":"00","endday":"2023-07-11","sideNumber":"","juli":"115","id":2181,"position":"","onOff":1,"manager":"刘","loginName":"刘"}],"terCnt":3},{"watermark":"00","shid":"0f748aa54cae4a1abd27917922bd694f","name":"门店","logo":"","brand":"今天","terList":[],"terCnt":0},{"watermark":"00","shid":"3a242e8a922e4adeb91f3314970e7dbd","name":"嘿嘿","logo":"","brand":"公司的人物简介吗","terList":[],"terCnt":0},{"watermark":"00","shid":"95e732c9dc64425892cfaf28f60d9780","name":"嘎嘎嘎评论了","logo":"https://etpic.we17.com/test/20220718171024_7508.jpg","improveLogo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"3e62379d746742b8a48750fcaeb580a7","name":"童话里有话","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145856_2752.jpg","terList":[],"terCnt":0},{"watermark":"00","shid":"5139b0c31a1c4edbb441ada447e59c28","name":"客气啦哈哈哈嘿嘿噶","logo":"https://etpic.we17.com/test/20220715084100_9177.jpg","brand":"好的好的","improveLogo":"https://etpic.we17.com/test/20220719143222_8849.png","terList":[],"terCnt":0},{"watermark":"00","shid":"279495d2c1cc4917a9d77fd6b1750c29","name":"芝士","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145512_2008.png","terList":[],"terCnt":0},{"watermark":"00","shid":"de848855036b490e9c4a71910a011fd1","name":"样例","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"3e14888644194765aa4c5849f3150ec8","name":"赶紧抢古古怪怪滚滚","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"f791e1eaa8484d1e9fd143acc239e0c4","name":"说话啊说话","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"4a68ea2d3ded4c51b8d39fd9ffc49820","name":"好好吃饭","logo":"https://etpic.we17.com/test/20220712175125_7575.jpg","terList":[],"terCnt":0},{"watermark":"00","shid":"12e0d5534546488f983edbac6a21300f","name":"富以后滚滚滚不会","logo":"https://etpic.we17.com/test/20220705100846_9584.jpg","terList":[],"terCnt":0}]}
/// extra : null

class SmartHomeStoreAndTerminalResponseBean {
  bool success;
  String code;
  String msg;
  SmartHomeStoreAndTerminalDataBean data;
  dynamic extra;

  static SmartHomeStoreAndTerminalResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreAndTerminalResponseBean smartHomeStoreAndTerminalResponseBeanBean = SmartHomeStoreAndTerminalResponseBean();
    smartHomeStoreAndTerminalResponseBeanBean.success = map['success'];
    smartHomeStoreAndTerminalResponseBeanBean.code = map['code'];
    smartHomeStoreAndTerminalResponseBeanBean.msg = map['msg'];
    smartHomeStoreAndTerminalResponseBeanBean.data = SmartHomeStoreAndTerminalDataBean.fromMap(map['data']);
    smartHomeStoreAndTerminalResponseBeanBean.extra = map['extra'];
    return smartHomeStoreAndTerminalResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"watermark":"00","shid":"ac108f8f736648b7aeca2531f9e0638c","name":"双龙大道点好好的好的","logo":"https://etpic.we17.com/test/20220722103915_7857.jpg","brand":"顾家家具大大方方","improveLogo":"https://etpic.we17.com/test/20220722104003_4779.png","terList":[{"terminalPicurl":"","hsn":"159562CBB68188C113A69E68D20F2F1133","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:06:47","screenStatus":"00","endday":"2023-06-06","sideNumber":"","id":2167,"position":"","onOff":1,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE22AEB71F2ACF","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-21 09:54:24","screenStatus":"00","endday":"2023-07-15","juli":"68","id":2183,"position":"手机","onOff":0,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"15E60F87A8DD9783F7085D3214762EF4F2","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近如家商旅酒店(南京百家湖小龙湾地铁站店)","bindflg":"01","terminalName":"终端显示屏2","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:02:45","screenStatus":"00","endday":"2023-07-11","sideNumber":"","juli":"115","id":2181,"position":"","onOff":1,"manager":"刘","loginName":"刘"}],"terCnt":3},{"watermark":"00","shid":"0f748aa54cae4a1abd27917922bd694f","name":"门店","logo":"","brand":"今天","terList":[],"terCnt":0},{"watermark":"00","shid":"3a242e8a922e4adeb91f3314970e7dbd","name":"嘿嘿","logo":"","brand":"公司的人物简介吗","terList":[],"terCnt":0},{"watermark":"00","shid":"95e732c9dc64425892cfaf28f60d9780","name":"嘎嘎嘎评论了","logo":"https://etpic.we17.com/test/20220718171024_7508.jpg","improveLogo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"3e62379d746742b8a48750fcaeb580a7","name":"童话里有话","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145856_2752.jpg","terList":[],"terCnt":0},{"watermark":"00","shid":"5139b0c31a1c4edbb441ada447e59c28","name":"客气啦哈哈哈嘿嘿噶","logo":"https://etpic.we17.com/test/20220715084100_9177.jpg","brand":"好的好的","improveLogo":"https://etpic.we17.com/test/20220719143222_8849.png","terList":[],"terCnt":0},{"watermark":"00","shid":"279495d2c1cc4917a9d77fd6b1750c29","name":"芝士","logo":"","improveLogo":"https://etpic.we17.com/test/20220715145512_2008.png","terList":[],"terCnt":0},{"watermark":"00","shid":"de848855036b490e9c4a71910a011fd1","name":"样例","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"3e14888644194765aa4c5849f3150ec8","name":"赶紧抢古古怪怪滚滚","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"f791e1eaa8484d1e9fd143acc239e0c4","name":"说话啊说话","logo":"","terList":[],"terCnt":0},{"watermark":"00","shid":"4a68ea2d3ded4c51b8d39fd9ffc49820","name":"好好吃饭","logo":"https://etpic.we17.com/test/20220712175125_7575.jpg","terList":[],"terCnt":0},{"watermark":"00","shid":"12e0d5534546488f983edbac6a21300f","name":"富以后滚滚滚不会","logo":"https://etpic.we17.com/test/20220705100846_9584.jpg","terList":[],"terCnt":0}]

class SmartHomeStoreAndTerminalDataBean {
  List<SmartHomeStoreAndTerminalListData> list;

  static SmartHomeStoreAndTerminalDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreAndTerminalDataBean dataBean = SmartHomeStoreAndTerminalDataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => SmartHomeStoreAndTerminalListData.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

/// watermark : "00"
/// shid : "ac108f8f736648b7aeca2531f9e0638c"
/// name : "双龙大道点好好的好的"
/// logo : "https://etpic.we17.com/test/20220722103915_7857.jpg"
/// brand : "顾家家具大大方方"
/// improveLogo : "https://etpic.we17.com/test/20220722104003_4779.png"
/// terList : [{"terminalPicurl":"","hsn":"159562CBB68188C113A69E68D20F2F1133","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:06:47","screenStatus":"00","endday":"2023-06-06","sideNumber":"","id":2167,"position":"","onOff":1,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE22AEB71F2ACF","address":"江苏省南京市江宁区天元中路辅路118号靠近润葳酒店","bindflg":"01","terminalName":"","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-21 09:54:24","screenStatus":"00","endday":"2023-07-15","juli":"68","id":2183,"position":"手机","onOff":0,"manager":"刘","loginName":"刘"},{"terminalPicurl":"","hsn":"15E60F87A8DD9783F7085D3214762EF4F2","address":"江苏省南京市江宁区天元中路辅路70号35幢靠近如家商旅酒店(南京百家湖小龙湾地铁站店)","bindflg":"01","terminalName":"终端显示屏2","companyid":"832d0096a4004d59a2331e9d0a9577a3","logout":"00","reportHsnLastTime":"2022-07-27 11:02:45","screenStatus":"00","endday":"2023-07-11","sideNumber":"","juli":"115","id":2181,"position":"","onOff":1,"manager":"刘","loginName":"刘"}]
/// terCnt : 3

class SmartHomeStoreAndTerminalListData {
  String watermark;
  String shid;
  String name;
  String logo;
  String brand;
  String improveLogo;
  List<TerInfoBean> terList;
  int terCnt;
  bool expandTerList = true;

  String getTitle(){
    String title = "${brand??""}（${name??""}）";
    if(terList != null && terList.length > 0){
      title = "${title??""}·${terList.length.toString()}";
    }
    return title;
  }

  static SmartHomeStoreAndTerminalListData fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStoreAndTerminalListData listBean = SmartHomeStoreAndTerminalListData();
    listBean.watermark = map['watermark'];
    listBean.shid = map['shid'];
    listBean.name = map['name'];
    listBean.logo = map['logo'];
    listBean.brand = map['brand'];
    listBean.improveLogo = map['improveLogo'];
    listBean.terList = List()..addAll(
      (map['terList'] as List ?? []).map((o) => TerInfoBean.fromMap(o))
    );
    listBean.terCnt = map['terCnt'];
    listBean.expandTerList = map['expandTerList'];
    return listBean;
  }

  Map toJson() => {
    "watermark": watermark,
    "shid": shid,
    "name": name,
    "logo": logo,
    "brand": brand,
    "improveLogo": improveLogo,
    "terList": terList,
    "terCnt": terCnt,
    "expandTerList": expandTerList,
  };
}

import 'dart:async';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/index/index_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/terminal_scan_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_and_terminal_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vg_base/vg_arch_lib.dart';

import '../app_main.dart';

/// 绑定新设备
class SmartHomeBindNewTerminalPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomeBindNewTerminalPage";

  final String latestGps;
  final String shid;

  const SmartHomeBindNewTerminalPage({Key key, this.latestGps, this.shid}) : super(key:key);

  @override
  _SmartHomeBindNewTerminalPageState createState() => _SmartHomeBindNewTerminalPageState();

  static Future<dynamic> navigatorPush(BuildContext context, String latestGps, String shid){
    return RouterUtils.routeForFutureResult(
        context,
        SmartHomeBindNewTerminalPage(
          latestGps: latestGps,
          shid: shid,
        ),
        routeName: SmartHomeBindNewTerminalPage.ROUTER
    );
  }
}

class _SmartHomeBindNewTerminalPageState
    extends BaseState<SmartHomeBindNewTerminalPage> {
  SmartHomeViewModel _viewModel;
  IndexViewModel _indexViewModel;
  String _latestGps;
  @override
  void initState() {
    super.initState();
    _latestGps = widget?.latestGps;
    _viewModel = SmartHomeViewModel(this);
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: _toMainColumnWidget(),
        )
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: [
        _toTopBarWidget(),
        _toInfoWidget(),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "绑定新设备",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      backgroundColor: Color(0xFFF6F7F9),
      // rightWidget: _toSaveButtonWidget(),
    );
  }




  Widget _toInfoWidget(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 80,
        ),
        Container(
            height: 120,
            width: 112,
            // padding: EdgeInsets.only(top: 30),
            child: Image.asset("images/icon_un_bind_light.png")),
        SizedBox(
          height: 11,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "购买使用显示屏事宜请联系",
              style: TextStyle(
                  fontSize: 13, color: Color(0XFF8B93A5)),
            ),
            GestureDetector(
              onTap: (){
                launch("tel://18665289540");
              },
              child: Text(
                "18665289540",
                style: TextStyle(
                    fontSize: 14,
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    height: 1.35
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Color(0XFFF6F7F9),
            border: Border.all(
                color: Color(0XFFE5E5E5),
                width: 1),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 20, top: 15),
                    child: Container(
                      child: Text(
                        "使用须知：",
                        style: TextStyle(
                            fontSize: 14, color: ThemeRepository.getInstance().getCardBgColor_21263C(), fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Row(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(32),
                      child: Container(
                        height: 20,
                        width: 20,
                        color: Color(0XFF8B93A5),
                        child: Text(
                          "1",
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              height: 1.5),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Container(
                      child: Text(
                        "启动终端显示屏，并确保成功联网",
                        style: TextStyle(
                            fontSize: 14, color: ThemeRepository.getInstance().getCardBgColor_21263C()),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Row(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(32),
                      child: Container(
                        height: 20,
                        width: 20,
                        color:Color(0XFF8B93A5),
                        child: Text(
                          "2",
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                              height: 1.5),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Container(
                      child: Text(
                        "扫描屏幕上的二维码完成绑定确认",
                        style: TextStyle(
                          fontSize: 14, color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              GestureDetector(
                onTap: ()async{
                  RouterUtils.pop(context);
                  TerminalScanPage.navigatorPush(context, _latestGps, SmartHomeStoreAndTerminalPage.ROUTER, shid: widget?.shid??"");
                },
                child: Container(
                  alignment: Alignment.center,
                  width: 160,
                  height: 40,
                  decoration: BoxDecoration(
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "images/icon_scan.png",
                        width: 14,
                      ),
                      SizedBox(width: 8,),
                      Text(
                        "扫一扫",
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),

        SizedBox(
          height: NAV_HEIGHT + 30,
        )
      ],
    );
  }

}

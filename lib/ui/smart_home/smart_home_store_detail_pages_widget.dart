import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_detail_list_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart'
as extended;

class SmartHomeStoreDetailPagesWidget extends StatelessWidget{
  final TabController tabController;
  final List<SpatialTypeListBean> categoryList;
  final String shid;
  final String style;
  const SmartHomeStoreDetailPagesWidget({Key key, this.tabController, this.categoryList, this.shid, this.style})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toPagesWidget();
  }

  Widget _toPagesWidget() {
    return TabBarView(
      controller: tabController,
      physics: BouncingScrollPhysics(),
      children: List.generate(categoryList?.length ?? 0, (index) {
        return extended.NestedScrollViewInnerScrollPositionKeyWidget(
            Key(categoryList?.elementAt(index)?.stid),
            SmartHomeStoreDetailListWidget(
              stid: categoryList?.elementAt(index)?.stid,
              category: categoryList?.elementAt(index)?.name,
              shid: shid,
              style: style,
            ));
      }),
    );
  }
}
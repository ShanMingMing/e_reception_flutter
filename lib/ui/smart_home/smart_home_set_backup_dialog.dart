import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SmartHomeSetBackupDialog extends StatefulWidget {
  final String backup;
  final Function(String backup) onConfirm;
  final Function() onDelete;

  const SmartHomeSetBackupDialog({Key key, this.backup, this.onConfirm, this.onDelete})
      : super(key: key);

  @override
  _SmartHomeSetBackupDialogState createState() =>
      _SmartHomeSetBackupDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      {String backup, Function onConfirm, Function onDelete}) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SmartHomeSetBackupDialog(
          backup: backup,
          onConfirm: onConfirm,
          onDelete: onDelete,
        )
    );
  }
}

class _SmartHomeSetBackupDialogState
    extends BaseState<SmartHomeSetBackupDialog> {
  TextEditingController _backupController;
  bool isAlive = false;
  @override
  void initState() {
    super.initState();
    _backupController = TextEditingController();
    if(StringUtils.isNotEmpty(widget?.backup)){
      _backupController?.text = widget?.backup;
    }
    isAlive = judgeAlive();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: ClipRRect(
        borderRadius:
        BorderRadius.only(topLeft: Radius.circular(12), topRight:Radius.circular(12)),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: _toMainColumnWidget()),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                "备注信息",
                style: TextStyle(
                    fontSize: 17,
                    color: ThemeRepository.getInstance()
                        .getCardBgColor_21263C(),fontWeight: FontWeight.w600
                ),
              ),
            ),
            Spacer(),
            _deleteWidget(),
            _buttonWidget(),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          height: 80,
          decoration: BoxDecoration(
              color: Color(0xFFF6F7F9),
              borderRadius: BorderRadius.circular(8)
          ),
          child: VgTextField(
              controller: _backupController,
              maxLines: 3,
              minLines: 1,
              autofocus: true,
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(left: 15, top:10, right: 15),
                hintText: "请输入（50字以内）",
                hintStyle: TextStyle(
                    color: Color(0xFFB0B3BF),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C()),
              onChanged: (String str) {
                isAlive = judgeAlive();
                setState(() {});
              },
              maxLimitLength: 50,
              zhCountEqEnCount: false,
              limitCallback:(int){
                VgToastUtils.toast(context, "最多可输入50字");
              }
          ),
        ),
        SizedBox(height: 20,),
      ],
    );
  }

  Widget _deleteWidget(){
    return Opacity(
        opacity: (StringUtils.isNotEmpty(widget?.backup??"")) ? 1 : 0,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            if(StringUtils.isEmpty(widget?.backup??"")){
              return;
            }
            bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
              title: "提示",
              content: "确认删除备注说明？",
              cancelText: "取消",
              confirmText: "删除",
              confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              cancelBgColor: Color(0xFFF6F7F9),
              titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
              contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
              widgetBgColor: Colors.white,
            );
            if (result ?? false) {
              VgToolUtils.removeAllFocus(context);
              widget?.onDelete?.call();
              RouterUtils.pop(context);
            }
          },
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 10,),
            child: Text(
              "删除",
              style: TextStyle(
                  fontSize: 14,
                  height: 1.2,
                  color: Color(0xFF8B93A5)),
            ),
          ),
        ));
  }

  Widget _buttonWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isAlive ?? false,
        width: 76,
        height: 30,
        unSelectBgColor: Color(0xFFCFD4DB),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
          height: 1.2,
        ),
        text: "保存",
        onTap: () {
          if (isAlive) {
            widget?.onConfirm?.call(_backupController.text,);
            RouterUtils.pop(context);
          }
        },
      ),
    );
  }


  bool judgeAlive(){
    return StringUtils.isNotEmpty(_backupController.text);
  }
}

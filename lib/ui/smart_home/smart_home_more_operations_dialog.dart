import 'dart:core';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 智能家居某个详情更多操作弹窗
class SmartHomeMoreOperationsDialog extends StatefulWidget {

  final String title;
  final VoidCallback onEdit;
  final VoidCallback onSetTerminal;
  final int useCnt;

  const SmartHomeMoreOperationsDialog({Key key,
    this.title,
    this.onEdit,
    this.onSetTerminal,
    this.useCnt
  }) : super(key: key);

  @override
  _SmartHomeMoreOperationsDialogState createState() =>
      _SmartHomeMoreOperationsDialogState();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      String title,
      int useCnt,
      VoidCallback onEdit,
      VoidCallback onSetTerminal) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      child: SmartHomeMoreOperationsDialog(
        title:title,
        onEdit:onEdit,
        onSetTerminal:onSetTerminal,
        useCnt:useCnt,
      ),
    );
  }

}

class _SmartHomeMoreOperationsDialogState extends BaseState<SmartHomeMoreOperationsDialog>{

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xFFF6F7F9)
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 12),
              height: 35,
              child: Text(
                "${widget?.title??"这里展示品牌或门店名"}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF8B93A5)
                ),
              ),
            ),
            _toPlayTerminalWidget(widget?.useCnt),
            Container(
              height: 1,
              color: Color(0xFFEEEEEE),
            ),
            _toEditWidget(),
            Container(
              height: 10,
              color: Color(0xFFF6F7F9),
            ),
            _toCancelButtonWidget(context),
          ],
        ),
      ),
    );
  }

  ///播放设备
  Widget _toPlayTerminalWidget(int count){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(widget?.onSetTerminal != null){
          widget?.onSetTerminal?.call();
        }
      },
      child: Container(
        height: 55,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        alignment: Alignment.center,
        child: Row(
          children: [
            Text(
              "播放设备",
              style: TextStyle(
                  fontSize: 14,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C()
              ),
            ),
            Spacer(),
            _toTerminalCountWidget(count),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
            )
          ],
        ),
      ),
    );
  }

  ///播放设备数目
  Widget _toTerminalCountWidget(int count){
    if(count == null){
      count = 0;
    }else if(count > 99){
      count = 99;
    }
    if(count == 0){
      return Container(
        height:18,
        width: 18,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: ThemeRepository.getInstance()
              .getMinorRedColor_F95355(),
          shape: BoxShape.circle,),
        child: Text(
          "0",
          style: TextStyle(
              fontSize: 12,
              color: Colors.white
          ),
        ),
      );
    }else{
      return Container(
        width: 25,
        height:18,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Color(0xFF00C6C4),
          borderRadius: BorderRadius.circular(9),),
        child: Text(
          "${count}",
          style: TextStyle(
              fontSize: 12,
              color: Colors.white
          ),
        ),
      );
    }


  }

  ///编辑
  Widget _toEditWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(widget?.onEdit != null){
          widget?.onEdit?.call();
        }
      },
      child: Container(
        height: 55,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        alignment: Alignment.centerLeft,
        child: Text(
          "编辑",
          style: TextStyle(
              fontSize: 14,
              color: ThemeRepository.getInstance().getCardBgColor_21263C()
          ),
        ),
      ),
    );
  }

  ///取消
  Widget _toCancelButtonWidget(BuildContext context) {
    final double bottomStatusHeight =  ScreenUtils.getBottomBarH(context);
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 55 + bottomStatusHeight,
        padding: EdgeInsets.only(bottom: bottomStatusHeight),
        color: Colors.white,
        child: Center(
          child: Text(
            "取消",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                fontSize: 14,
                height: 1.2),
          ),
        ),
      ),
    );
  }

}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/create_smart_home_category.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SmartHomeModifyOrderListItemWidget extends StatelessWidget {
  final String shid;
  final SpatialTypeListBean itemBean;
  final int index;
  final Function(int start,int end) onAccept;


  const SmartHomeModifyOrderListItemWidget({
    Key key,
    this.shid, this.itemBean, this.index, this.onAccept,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toDraggable(context);
  }

  Widget _toDraggable(BuildContext context) {
    final Widget child = _toContainerWidget(context);
    return LongPressDraggable<int>(
      data: index,
        maxSimultaneousDrags: 1,
        childWhenDragging: Opacity(
          opacity: 0.5,
          child: child,
        ),
        feedback: Material(
          child: Container(
            height: 60,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.white.withOpacity(0.5),
                  blurRadius: 5
                )
              ]
            ),
            width: ScreenUtils.screenW(context),
            child: _toContainerWidget(context),
          ),
        ),
        child: DragTarget<int>(
          onAccept: (int start){
            onAccept?.call(start,index);
          },
          onWillAccept: (int fromIndex) {
            return true;
          },
          builder: (BuildContext context, candidateData,
              List<dynamic> rejectedData) {
            return child;
          },
        ));
  }

  Widget _toContainerWidget(BuildContext context) {
    String categoryName = itemBean?.name;
    String stid = itemBean?.stid;
    int count = itemBean?.uploadcnt??0;
    return Container(
      height: 50,
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
          children:[
            Text(
              categoryName??"",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize:14,
                // color: ("00" == (itemBean?.defaultflg??"00"))?Color(0xFFB0B3BF):ThemeRepository.getInstance().getCardBgColor_21263C(),
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              ),
            ),
            Spacer(),
            Visibility(
              // visible: "00" != (itemBean?.defaultflg??"00"),
              visible: true,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: (){
                  CreateSmartHomeCategoryPage.navigatorPush(context, shid, categoryName, count, index, stid: stid);
                },
                child: Container(
                  height: 50,
                  width: 84,
                  alignment: Alignment.center,
                  child: Text(
                    "编辑",
                    style: TextStyle(
                        fontSize: 12,
                        color: Color(0xFF8B93A5)
                    ),
                  ),
                ),
              ),
            ),
            Image.asset(
              "images/icon_edit_order.png",
              width: 20,
            ),
          ]
      ),
    );
  }

}

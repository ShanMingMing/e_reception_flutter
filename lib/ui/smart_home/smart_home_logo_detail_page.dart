import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';


typedef ClipCompleteCallback = void Function(String path, Function cancelLoadingCallback);

class SmartHomeLogoDetailPage extends StatefulWidget {

  static const ROUTER = "SmartHomeLogoDetailPage";

  final String url;
  final ClipCompleteCallback clipCompleteCallback;
  final bool isClipCompleteShowLoading;
  final VoidCallback onDelete;


  const SmartHomeLogoDetailPage({
    Key key,
    this.url,
    this.clipCompleteCallback,
    this.onDelete,
    this.isClipCompleteShowLoading = false,
  }) : assert(url != null && url != "", "url cannot be empty");


  @override
  State<StatefulWidget> createState() => _CompanyUserLogoState();

  static Future<dynamic> navigatorPush(BuildContext context, String url,
      ClipCompleteCallback clipCompleteCallback, VoidCallback onDelete) {
    return RouterUtils.routeForFutureResult(context,
        SmartHomeLogoDetailPage(url: url,
          clipCompleteCallback: clipCompleteCallback,
          onDelete: onDelete,
        ),
        routeName: SmartHomeLogoDetailPage.ROUTER);
  }
}

class _CompanyUserLogoState extends BaseState<SmartHomeLogoDetailPage> {

  String _path;

  @override
  void initState() {
    super.initState();
    _path = widget?.url;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(children: <Widget>[
        _toTopBarWidget(context),
        _buildImageLogo(),
        buildOptionsWidget(),
        Container(height: ScreenUtils.getBottomBarH(context))
      ]),
    );
  }

  Row buildOptionsWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _buildHandleBtn("删除", () => widget?.onDelete?.call()),
        _buildHandleBtn("更换", () => addUserLogo(context))
      ],
    );
  }

  void addUserLogo(BuildContext context) async{
    String path = await MatisseUtil.clipOneImage(context, scaleX: 1, scaleY: 1, isCanSelectRadio: false,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
            confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            cancelBgColor: Color(0xFFF6F7F9),
            titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
            contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
            widgetBgColor: Colors.white,
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        });
    if (path == null || path == "") {
      return;
    }
    _path = path;
    setState(() {});
    if (widget?.isClipCompleteShowLoading ?? false) loading(true, msg: "上传中");
    if (widget?.clipCompleteCallback != null) widget?.clipCompleteCallback(_path, () {
      loading(false);
      toast("上传成功");
      RouterUtils.pop(context);
    });
  }

  Widget _toTopBarWidget(BuildContext context) {
    return VgTopBarWidget(
      title: "",
      isShowBack: true,
      backImgColor: Color(0xff5e687c),
      isShowGrayLine: false,
      backgroundColor: Colors.transparent,
      rightPadding: 0,
      rightWidget: Offstage(
        offstage: true,
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Image.asset("images/more_white_dark.png", width: 20, height: 20)
        ),
      ),
    );
  }

  Widget _buildImageLogo() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          VgCacheNetWorkImage(
            _path,
            width: ScreenUtils.screenW(context),
            height: ScreenUtils.screenW(context),
            placeWidget: Container(color: Colors.black),

          ),
        ],
      ),
    );
  }



  Widget _buildHandleBtn(String text, VoidCallback onClickCallback) {
    return ClickAnimateWidget(
      scale: 1.05,
      onClick: onClickCallback ?? (){},
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
        child: Text(
          "${text ?? ""}",
          style: TextStyle(fontSize: 15,
              color: Colors.white
          ),
        ),
      ),
    );
  }
}
import 'dart:core';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_modify_order_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_one_details_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 智能家居某个详情选择图片分类弹窗
class SmartHomeSelectCategoryDialog extends StatefulWidget {
  final SpatialTypeListBean selectType;
  final List<SpatialTypeListBean> categoryList;
  final String shid;
  final Function(SpatialTypeListBean selectType) onSelect;
  const SmartHomeSelectCategoryDialog({Key key, this.selectType, this.categoryList, this.shid, this.onSelect}) : super(key: key);

  @override
  _SmartHomeSelectCategoryDialogState createState() =>
      _SmartHomeSelectCategoryDialogState();

  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,
      SpatialTypeListBean selectType,
      List<SpatialTypeListBean> categoryList, String shid, Function(SpatialTypeListBean selectType) onSelect) {
    return VgDialogUtils.showCommonDialog<String>(
      barrierDismissible: true,
      context: context,
      child: SmartHomeSelectCategoryDialog(
        selectType: selectType,
        categoryList: categoryList,
        shid: shid,
        onSelect: onSelect,
      ),
    );
  }

}

class _SmartHomeSelectCategoryDialogState extends BaseState<SmartHomeSelectCategoryDialog>{
  List<SpatialTypeListBean> _categoryList;
  SpatialTypeListBean _selectType;
  @override
  void initState() {
    super.initState();
    _categoryList = widget?.categoryList;
    if(widget?.selectType == null){
      _selectType = _categoryList[0];
    }else{
      _selectType = widget?.selectType;
    }

  }

  @override
  Widget build(BuildContext context) {
    double height = 0;
    if(_categoryList.length < 8){
      height = 299;
    }else{
      height = 414;
    }
    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: height,
            width: 320,
            padding: EdgeInsets.only(bottom: 30),
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _toTitleWidget(),
                _toCategoryWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///标题 管理
  Widget _toTitleWidget(){
    return Container(
      height: 60,
      margin: EdgeInsets.only(left: 20),
      padding: EdgeInsets.only(bottom: 5),
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "选择分类",
            style: TextStyle(
                fontSize: 17,
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                fontWeight: FontWeight.w600
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              RouterUtils.pop(context);
              SmartHomeCategoryModifyOrderPage.navigatorPush(context, _categoryList, widget?.shid,);
            },
            child: Container(
              height: 60,
              width: 48,
              alignment: Alignment.center,
              child: Text(
                "管理",
                style: TextStyle(
                    fontSize: 12,
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                ),
              ),
            ),
          ),
          Spacer(),
          _toCancelWidget(),
        ],
      ),
    );
  }

  ///分类布局
  Widget _toCategoryWidget(){
    return Expanded(
      child: GridView.builder(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        itemCount: _categoryList?.length ?? 0,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          childAspectRatio: 135 / 45,
        ),
        itemBuilder: (BuildContext context, int index) {
          return _toGridItemWidget(context, index, _categoryList?.elementAt(index));
        },
      ),
    );
  }

  Widget _toGridItemWidget(BuildContext context, int index, SpatialTypeListBean item) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
          setState(() {
            _selectType = item;
            widget?.onSelect?.call(_selectType);
            RouterUtils.pop(context);
          });
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          padding: EdgeInsets.only(left: 20),
          height: 45,
          width: 135,
          alignment: Alignment.centerLeft,
          color: (_selectType?.stid == item?.stid)?ThemeRepository.getInstance().getPrimaryColor_1890FF():Color(0xFFF6F7F9),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 70,
                child: Text(
                  item?.name??"-",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 14,
                      color: (_selectType?.stid == item?.stid)?Colors.white:ThemeRepository.getInstance().getCardBgColor_21263C()
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 45,
                alignment: Alignment.center,
                child: Text(
                  "${item?.cnt??0}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 14,
                      color: (_selectType?.stid == item?.stid)?Colors.white:Color(0xFF8B93A5)
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }

  ///取消
  Widget _toCancelWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 60,
        width: 55,
        alignment: Alignment.center,
        child: Image.asset(
          "images/login_close_ico.png",
          color: Color(0xFFB0B3BF),
          width: 15,
          height: 15,
        ),
      ),
    );
  }

}

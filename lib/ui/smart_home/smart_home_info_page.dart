import 'dart:async';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_info_widget.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_category_response_bean.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_create_or_edit_address_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_logo_detail_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_more_operations_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_select_terminal_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_and_terminal_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_store_detail_view_model.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_title_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_view_model.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_water_list_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/logo_detail_page.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'event/refresh_smart_home_category_event.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:vg_base/vg_permission_lib.dart';

/// 门店信息
class SmartHomeInfoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "SmartHomeInfoPage";
  //品牌或门店id
  final String id;
  final String router;

  const SmartHomeInfoPage(
      {Key key, this.id, this.router,}) : super(key:key);

  @override
  _SmartHomeInfoPageState createState() => _SmartHomeInfoPageState();

  static Future<dynamic> navigatorPush(BuildContext context, String id, String router){
    return RouterUtils.routeForFutureResult(
        context,
        SmartHomeInfoPage(
          id: id,
          router: router,
        ),
        routeName: SmartHomeInfoPage.ROUTER
    );
  }
}

class _SmartHomeInfoPageState
    extends BaseState<SmartHomeInfoPage> {

  SmartHomeViewModel _viewModel;
  SmartHomeStoreDetailViewModel _detailViewModel;
  //品牌名称
  String _brandName = "";
  String _originBrandName = "";

  //门店名称
  String _storeName = "";
  String _originStoreName = "";

  String _imageUrl;
  String _originImageUrl;

  int _useCnt = 0;
  int _totalPicCnt = 0;

  String _waterflg = "00";
  StreamSubscription _categoryUpdateStreamSubscription;
  String _id;
  String _cacheKey;
  String _detailAddress;
  ComSmarthomeBean _infoBean;
  @override
  void initState() {
    super.initState();
    _viewModel = SmartHomeViewModel(this);
    _detailViewModel = new SmartHomeStoreDetailViewModel(this);
    _id = widget?.id;
    if(StringUtils.isNotEmpty(_id)){
      _detailViewModel.getCategoryList(context, _id);
    }
    if(StringUtils.isNotEmpty(_id)){
      _cacheKey = NetApi.GET_CATEGORY_LIST + (UserRepository.getInstance().getCacheKeySuffix()?? "")
          + _id;
    }
    _categoryUpdateStreamSubscription =
        VgEventBus.global.on<RefreshSmartHomeCategoryEvent>()?.listen((event) {
          _detailViewModel?.getCategoryOnLine(_id, _cacheKey, null);
        });
  }


  @override
  void dispose() {
    _categoryUpdateStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Color(0xFFF6F7F9),
          body: _toMainColumnWidget(),
        )
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: [
        _toTopBarWidget(),
        (StringUtils.isNotEmpty(_id))?_toPlaceHolderWidget():_toInfoWidget(),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "门店信息",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      // rightWidget: _toSaveButtonWidget(),
    );
  }

  Widget _toPlaceHolderWidget(){
    return ValueListenableBuilder(
        valueListenable: _detailViewModel?.statusTypeValueNotifier,
        builder:
            (BuildContext context, PlaceHolderStatusType value, Widget child) {
          return VgPlaceHolderStatusWidget(
            loadingStatus: value == PlaceHolderStatusType.loading,
            errorStatus: value == PlaceHolderStatusType.error,
            errorOnClick: () => _detailViewModel
                .getCategoryList(context, _id),
            loadingOnClick: () => _detailViewModel
                .getCategoryList(context, _id),
            child: child,
          );
        },
        child: _toValueWidget());
  }

  Widget _toValueWidget(){
    return ValueListenableBuilder(
      valueListenable: _detailViewModel.categoryValueNotifier,
      builder: (BuildContext context, CategoryDataBean dataBean, Widget child){
        _imageUrl = dataBean?.comSmarthome?.logo??"";
        _brandName = dataBean?.comSmarthome?.brand??"";
        _storeName = dataBean?.comSmarthome?.name??"";
        _useCnt = dataBean?.terCnt??0;
        _totalPicCnt = dataBean?.getPicTotalCnt();
        _waterflg = dataBean?.comSmarthome?.watermark??"00";
        // _detailAddress = dataBean?.comSmarthome?.getShowAddress();
        _detailAddress = dataBean?.comSmarthome?.address;
        _infoBean = dataBean?.comSmarthome;
        return _toInfoWidget();
      },
    );
  }


  Widget _toInfoWidget(){
    return Column(
      children: [
        _toBrandNameWidget(),
        SizedBox(height: 1,),
        _toStoreNameWidget(),
        SizedBox(height: 1,),
        _toSetTerminalWidget(),
        // SizedBox(height: 10,),
        // _toLogoWidget(),
        SizedBox(height: 1,),
        _toAddressWidget(),
        SizedBox(height: 20,),
        _toMoreWidget(),
      ],
    );
  }






  ///logo
  Widget _toLogoWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: ()async{
        VgToolUtils.removeAllFocus(context);
        if(StringUtils.isEmpty(_imageUrl)){
          String path = await MatisseUtil.clipOneImage(context, scaleX: 1, scaleY: 1, isCanSelectRadio: false,
              onRequestPermission: (msg)async{
                bool result =
                await CommonConfirmCancelDialog.navigatorPushDialog(context,
                  title: "提示",
                  content: msg,
                  cancelText: "取消",
                  confirmText: "去授权",
                  confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  cancelBgColor: Color(0xFFF6F7F9),
                  titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  widgetBgColor: Colors.white,
                );
                if (result ?? false) {
                  PermissionUtil.openAppSettings();
                }
              });
          if (path == null || path == "") {
            return;
          }
          _imageUrl = path;
          setState(() {});
          if(StringUtils.isNotEmpty(_imageUrl) && !PhotoPreviewToolUtils.isNetUrl(_imageUrl)){
            VgHudUtils.show(context, "上传中");
            VgMatisseUploadUtils.uploadSingleImage(
                _imageUrl,
                VgBaseCallback(onSuccess: (String netPic) {
                  if (StringUtils.isNotEmpty(netPic)) {
                    //编辑
                    _viewModel.editStoreLogo(context, _id, netPic);
                  } else {
                    VgToastUtils.toast(context, "图片上传失败");
                  }
                }, onError: (String msg) {
                  VgToastUtils.toast(context, msg);
                }));
          }else{
            //编辑
            _viewModel.editStoreLogo(context, _id, _imageUrl);
          }
          return;
        }
        SmartHomeLogoDetailPage.navigatorPush(context, _imageUrl,  (path, cancelLoadingCallback) {
          RouterUtils.pop(context);
          _imageUrl = path;
          setState(() {});
          if(StringUtils.isNotEmpty(_imageUrl) && !PhotoPreviewToolUtils.isNetUrl(_imageUrl)){
            VgHudUtils.show(context, "上传中");
            VgMatisseUploadUtils.uploadSingleImage(
                _imageUrl,
                VgBaseCallback(onSuccess: (String netPic) {
                  if (StringUtils.isNotEmpty(netPic)) {
                    //编辑
                    _viewModel.editStoreLogo(context, _id, netPic);
                  } else {
                    VgToastUtils.toast(context, "图片上传失败");
                  }
                }, onError: (String msg) {
                  VgToastUtils.toast(context, msg);
                }));
          }else{
            //编辑
            _viewModel.editStoreLogo(context, _id, _imageUrl);
          }
        },(){
          RouterUtils.pop(context);
          setState(() {
            _imageUrl = "";
          });
          _viewModel.editStoreLogo(context, _id, _imageUrl);
        },);
        // CommonMoreMenuDialog.navigatorPushDialog(context, {
        //   "更换":()async{
        //     String path = await MatisseUtil.clipOneImage(context, scaleX: 1, scaleY: 1, isCanSelectRadio: false);
        //     if (path == null || path == "") {
        //       return;
        //     }
        //     _imageUrl = path;
        //     setState(() {});
        //     if(StringUtils.isNotEmpty(_imageUrl) && !PhotoPreviewToolUtils.isNetUrl(_imageUrl)){
        //       VgHudUtils.show(context, "上传中");
        //       VgMatisseUploadUtils.uploadSingleImage(
        //           _imageUrl,
        //           VgBaseCallback(onSuccess: (String netPic) {
        //             if (StringUtils.isNotEmpty(netPic)) {
        //               //编辑
        //               _viewModel.editStoreLogo(context, _id, netPic);
        //             } else {
        //               VgToastUtils.toast(context, "图片上传失败");
        //             }
        //           }, onError: (String msg) {
        //             VgToastUtils.toast(context, msg);
        //           }));
        //     }else{
        //       //编辑
        //       _viewModel.editStoreLogo(context, _id, _imageUrl);
        //     }
        //   },
        //   "删除":()async{
        //     setState(() {
        //       _imageUrl = "";
        //     });
        //     _viewModel.editStoreLogo(context, _id, _imageUrl);
        //   }
        // },
        //   bgColor: Colors.white,
        //   splitColor: Color(0xFFF6F7F9),
        //   textColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        //   lineColor: Color(0xFFEEEEEE),
        // );
      },
      child: Container(
        height: 50,
        color: Colors.white,
        alignment: Alignment.center,
        child: Row(
          children: [
            SizedBox(width: 15,),
            Text(
              "品牌Logo",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF5E687C),
              ),
            ),
            Spacer(),
            StringUtils.isNotEmpty(_imageUrl)?ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Container(
                width: 30,
                height: 30,
                alignment: Alignment.center,
                child: VgCacheNetWorkImage(
                  _imageUrl,
                ),
              ),
            ):
            Text(
              "去设置",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFFB0B3BF),
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
            SizedBox(width: 15,),
          ],
        ),
      ),
    );
  }


  ///门店地址
  Widget _toAddressWidget(){
    double maxWidth = ScreenUtils.screenW(context) - 120;
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
       SmartHomeCreateOrEditAddressPage.navigatorPush(context, _infoBean?.shid,
            _infoBean.gps, _infoBean.addrProvince, _infoBean.addrCity,
            _infoBean.addrDistrict, _infoBean.address, _infoBean.addrCode, (address, gps){
        });
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        child: Row(
          children: [
            SizedBox(width: 15,),
            Text(
              "门店地址",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF5E687C),
              ),
            ),
            SizedBox(width: 15,),
            Spacer(),
            Container(
              constraints: BoxConstraints(
                maxWidth: maxWidth,
              ),
              child: Text(
                StringUtils.isNotEmpty(_detailAddress)?_detailAddress:"去设置",
                style: TextStyle(
                  fontSize: 14,
                  color: StringUtils.isNotEmpty(_detailAddress)?Color(0XFF21263C):Color(0XFFB0B3BF),
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
            SizedBox(width: 15,),
          ],
        ),
      ),
    );
  }

  Widget _toEmptyLogoWidget(){
    return Container(
      color: Color(0xFFF6F7F9),
      width: 85,
      height: 85,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "images/icon_add_logo.png",
            width: 34,
            height: 34,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            "LOGO/选填",
            style: TextStyle(
              fontSize: 11,
              color: Color(0XFFB0B3BF),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toBrandNameWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: ()async{
        Map result = await SmartHomeTitlePage.navigatorPush(context, _id, brandName: _brandName);
        if(result == null){
          return;
        }
        if(StringUtils.isEmpty(_id)){
          _id = result['id'];
          _cacheKey = NetApi.GET_CATEGORY_LIST + (UserRepository.getInstance().getCacheKeySuffix()?? "")
              + _id;
        }
        _detailViewModel?.statusTypeValueNotifier?.value = null;
        _detailViewModel?.getCategoryOnLine(_id, _cacheKey, null);
        setState(() {
          _brandName = result['title'];
        });
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Text(
              "品牌名称",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF5E687C),
              ),
            ),
            Spacer(),
            Text(
              StringUtils.isNotEmpty(_brandName??"")?_brandName:"",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF21263C),
              ),
            ),
            Visibility(
              visible: StringUtils.isEmpty(_brandName??""),
              child: Text(
                "必填",
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0XFFB0B3BF),
                ),
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
          ],
        ),
      ),
    );
  }

  ///门店名称
  Widget _toStoreNameWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: ()async{
        Map result = await SmartHomeTitlePage.navigatorPush(context, _id, storeName: _storeName);
        if(result == null){
          return;
        }
        if(StringUtils.isEmpty(_id)){
          _id = result['id'];
          _cacheKey = NetApi.GET_CATEGORY_LIST + (UserRepository.getInstance().getCacheKeySuffix()?? "")
              + _id;
        }
        _detailViewModel?.statusTypeValueNotifier?.value = null;
        _detailViewModel?.getCategoryOnLine(_id, _cacheKey, null);
        setState(() {
          _storeName = result['title'];
        });
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            Text(
              "门店名称",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF5E687C),
              ),
            ),
            Spacer(),
            Text(
              StringUtils.isNotEmpty(_storeName??"")?_storeName:"",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF21263C),
              ),
            ),
            Visibility(
              visible: StringUtils.isEmpty(_storeName??""),
              child: Text(
                "必填",
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0XFFB0B3BF),
                ),
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
          ],
        ),
      ),
    );
  }


  ///播放设备
  Widget _toSetTerminalWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(StringUtils.isEmpty(_id)){
          VgToastUtils.toast(context, "请先填写品牌或门店名称");
          return;
        }
        SmartHomeSelectTerminalPage.navigatorPush(context, _id, _totalPicCnt);
        // SmartHomeStoreAndTerminalPage.navigatorPush(context, widget?.id);

      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        child: Row(
          children: [
            SizedBox(width: 15,),
            Text(
              "播放设备",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF5E687C),
              ),
            ),
            Spacer(),
            Text(
              _useCnt>0?"${_useCnt}":"去设置",
              style: TextStyle(
                fontSize: 14,
                color: _useCnt>0?Color(0XFF00C6C4):Color(0XFFB0B3BF),
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
            SizedBox(width: 15,),
          ],
        ),
      ),
    );
  }

  ///设置文件水印
  Widget _toSetWaterWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(StringUtils.isEmpty(_id)){
          VgToastUtils.toast(context, "请先填写品牌或门店名称");
          return;
        }
        SmartHomeWaterListPage.navigatorPush(context, _infoBean);
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: Colors.white,
        child: Row(
          children: [
            SizedBox(width: 15,),
            Text(
              "文件水印",
              style: TextStyle(
                fontSize: 14,
                color: Color(0XFF21263C),
              ),
            ),
            Spacer(),
            Text(
              ("00" == _waterflg)?"启用":"关闭",
              style: TextStyle(
                fontSize: 14,
                color: ("00" == _waterflg)?Color(0XFF00C6C4):Color(0XFFB0B3BF),
              ),
            ),
            SizedBox(width: 9,),
            Image.asset(
              "images/icon_arrow_right_grey.png",
              width: 6,
              color: Color(0XFF8B93A5),
            ),
            SizedBox(width: 15,),
          ],
        ),
      ),
    );
  }


  ///更多
  Widget _toMoreWidget(){
    return Visibility(
      visible: StringUtils.isNotEmpty(_id??""),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: (){
          _infoBean.terCnt = _useCnt;
          SmartHomeMoreOperationsPage.navigatorPush(context, _infoBean);
        },
        child: Container(
          height: 50,
          alignment: Alignment.center,
          color: Colors.white,
          child: Row(
            children: [
              SizedBox(width: 15,),
              Text(
                "更多设置",
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0XFF5E687C),
                ),
              ),
              Spacer(),
              Image.asset(
                "images/icon_arrow_right_grey.png",
                width: 6,
                color: Color(0XFF8B93A5),
              ),
              SizedBox(width: 15,),
            ],
          ),
        ),
      ),
    );
  }

///通知更新
// _notifyChange() {
//   _title = _titleController?.text?.trim();
//   setState(() { });
// }
//
// bool isCanSave(){
//   //没有内容
//   if(StringUtils.isEmpty(_title)){
//     return false;
//   }
//   //与原来内容相同
//   if(_title.trim() == _originTitle.trim() && _imageUrl == _originImageUrl){
//     return false;
//   }
//   return true;
// }

// Widget _toSaveButtonWidget() {
//   return Row(
//     children: [
//       Visibility(
//         visible: StringUtils.isNotEmpty(_originTitle),
//         child: GestureDetector(
//           behavior: HitTestBehavior.opaque,
//           onTap: ()async{
//             bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
//               title: "提示",
//               content: "确认删除当前智能家居品牌(${_title})？",
//               cancelText: "取消",
//               confirmText: "确认",
//               confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
//               cancelBgColor: Color(0xFFF6F7F9),
//               titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
//               contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
//               widgetBgColor: Colors.white,
//             );
//             if (result ?? false) {
//               _viewModel.deleteStore(context, _id, "01");
//             }
//           },
//           child: Container(
//             height: 44,
//             alignment: Alignment.center,
//             padding: EdgeInsets.symmetric(horizontal: 22),
//             child: Text(
//               "删除",
//               style: TextStyle(
//                 fontSize: 12,
//                 color: Color(0xFF8B93A5),
//               ),
//             ),
//           ),
//         ),
//       ),
//       GestureDetector(
//         behavior: HitTestBehavior.opaque,
//         onTap: (){
//           _save();
//         },
//         child: Container(
//           width: 48,
//           height: 24,
//           alignment: Alignment.center,
//           decoration: BoxDecoration(
//             color: isCanSave()?ThemeRepository.getInstance().getPrimaryColor_1890FF():Color(0xFFCFD4DB),
//             borderRadius: BorderRadius.circular(12),
//           ),
//           child: Text(
//             "保存",
//             style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 12,
//                 height: 1.2,
//                 fontWeight: FontWeight.w600),
//           ),
//         ),
//       ),
//     ],
//   );
// }

// void _save(){
//   if(!isCanSave()){
//     return;
//   }
//   if(StringUtils.isNotEmpty(_imageUrl) && !PhotoPreviewToolUtils.isNetUrl(_imageUrl)){
//     VgHudUtils.show(context, "上传中");
//     VgMatisseUploadUtils.uploadSingleImage(
//         _imageUrl,
//         VgBaseCallback(onSuccess: (String netPic) {
//           if (StringUtils.isNotEmpty(netPic)) {
//             if(StringUtils.isNotEmpty(_id??"")){
//               //编辑
//               _viewModel.editStore(context, _id, null, _title, netPic);
//             }else{
//               _viewModel.createStore(context, _title, netPic, widget?.router);
//             }
//           } else {
//             VgToastUtils.toast(context, "图片上传失败");
//           }
//         }, onError: (String msg) {
//           VgToastUtils.toast(context, msg);
//         }));
//   }else{
//     if(StringUtils.isNotEmpty(_id??"")){
//       //编辑
//       _viewModel.editStore(context, _id, null, _title, _imageUrl);
//     }else{
//       _viewModel.createStore(context, _title, _imageUrl, widget?.router);
//     }
//   }
// }

}

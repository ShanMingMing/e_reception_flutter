class SmartHomeStyleTypeBean {
  //风格 00现代简约 01中式现代 02美式田园 03美式经典 04欧式豪华 05北欧极简 06日式 07地中海 08潮流混搭 09轻奢 99其他
  String style;
  String name;

  String getName(){
    if("其他" == name){
      return "未知风格";
    }
    return name;
  }

  SmartHomeStyleTypeBean(this.name, this.style);

  static SmartHomeStyleTypeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeStyleTypeBean styleTypeBean = SmartHomeStyleTypeBean(map['name'], map['style']);
    return styleTypeBean;
  }
}
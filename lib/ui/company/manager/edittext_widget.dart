import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/common_title_edit_with_underline_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

/// 编辑框组件
class EditTextWidget extends StatelessWidget {
  final String title;

  final String hintText;

  final CommonTitleEditEditAndValueChangedCallback onChanged;

  ///点击
  final CommonTitleNavigatorPushCallback onTap;

  ///解析跳转返回值
  final CommonTitlePopResultDecodeCallback onDecodeResult;

  final bool isShowRedStar;

  ///中字极限
  final int maxZHCharLimit;

  ///极限字数回调
  final ValueChanged<int> limitCharFunc;

  ///限制
  final List<TextInputFormatter> inputFormatters;

  ///是否显示跳转icon
  final bool isShowGoIcon;

  ///初始化数据
  final String initContent;

  ///初始值
  final dynamic initValue;

  ///只读
  final bool readOnly;

  ///是否去掉红星
  final bool isGoneRedStar;

  ///是否跟随编辑框
  final bool isFollowEdit;

  final TextInputType keyboardType;

  final bool autofocus;

  final TextEditingController controller;

  final Function(Widget titleWidget, Widget textFieldWidget, Widget leftWidget,
      Widget rightWidget) customWidget;

  const EditTextWidget(
      {Key key,
      this.title,
      this.hintText,
      this.onChanged,
      this.isShowRedStar = false,
      this.onTap,
      this.onDecodeResult,
      this.maxZHCharLimit,
      this.limitCharFunc,
      this.inputFormatters,
      this.isShowGoIcon = false,
      this.initContent,
      this.initValue,
      this.readOnly,
      this.isGoneRedStar = false,
      this.isFollowEdit = false,
      this.customWidget,
      this.controller,
      this.keyboardType, this.autofocus})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommonTitleEditWithUnderlineWidget(
      controller: controller,
      titleTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 14),
      editTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 14),
      title: title,
      titleMarginRight: 15,
      height: 50,
      onChanged: onChanged,
      onDecodeResult: onDecodeResult,
      onTap: onTap,
      autofocus: autofocus,
      minLines: 1,
      maxLines: 1,
      readOnly: readOnly,
      maxZHCharLimit: maxZHCharLimit,
      limitCharFunc: limitCharFunc,
      inputFormatters: inputFormatters,
      customWidget: customWidget,
      keyboardType: keyboardType,
      lineMargin: const EdgeInsets.only(left: 15, right: 0),
      lineUnselectColor: Color(0xFF303546),
      lineHeight: 0,
      initContent: initContent,
      initValue: initValue,
      lineSelectedColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      hintText: hintText,
      autoDispose: false,
      hintTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextEditHintColor(),
          fontSize: 14),
      rightWidget: Offstage(
        offstage: !isShowGoIcon,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Image.asset(
            "images/go_ico.png",
            width: 6,
            color: VgColors.INPUT_BG_COLOR,
          ),
        ),
      ),
      leftWidget: Offstage(
        offstage: isGoneRedStar,
        child: Container(
          width: 15,
          child: Opacity(
            opacity: isShowRedStar ? 1 : 0,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  "*",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getMinorRedColor_F95355(),
                      fontSize: 14,
                      height: 1.1),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/company_choose_terminal_list_page.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_info_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/edit_user_page_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/multi_select_class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/multi_select_department_widget.dart';
import 'package:e_reception_flutter/ui/mypage/edit_name_dialog_widget.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/logo_detail_page.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'edittext_widget.dart';

///添加管理员 (其实就是编辑用户给添加管理员)
class AddManagerFromSelectPage extends StatefulWidget {
  final String fuid;

  // 从部门/班级页面添加 以下要锁死
  final String department;
  final String departmentId;
  final String classid;
  final String className;
  static const String ROUTER = 'AddManagerFromSelectPage';

  AddManagerFromSelectPage(this.fuid,
      {this.department, this.departmentId, this.classid, this.className});

  ///跳转方法
  static Future<bool> navigatorPush(BuildContext context, String fuid,
      {String department,
        String departmentId,
        String classid,
        String className}) {
    if (fuid == null) {
      return null;
    }
    return RouterUtils.routeForFutureResult<bool>(
      context,
      AddManagerFromSelectPage(
        fuid,
        department: department,
        departmentId: departmentId,
        classid: classid,
        className: className,
      ),
      routeName: AddManagerFromSelectPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return AddManagerFromSelectPageState();
  }
}

class AddManagerFromSelectPageState
    extends BaseState<AddManagerFromSelectPage> {
  ///保存notifier
  ValueNotifier<bool> _isAllowConfirmValueNotifier;

  ///编辑用户viewModel
  EditUserPageViewModel _viewModel;

  ///待上传数据
  EditUserUploadBean uploadBean = EditUserUploadBean();

  ///正在加载
  bool isLoading;

  ///用户数据
  EditUserInfoDataBean infoDataBean;

  ///根据权限判断是否显示身份和终端
  bool isLastedIdentityAndTerminal;

  TextEditingController _nameController;

  TextEditingController _terminalController;
  TextEditingController _departmentController;
  TextEditingController _classController;

  ///是企业
  bool isCompany =
  UserRepository
      .getInstance()
      .userData
      .companyInfo
      .isCompanyLike();

  bool isSchool =
  UserRepository
      .getInstance()
      .userData
      .companyInfo
      .isSchoolLike();

  ValueNotifier<String> updateNameValueNotifier;

  ///锁定管理班级
  bool lockClass = false;

  ///锁定管理部门
  bool lockDepartment = false;

  ///锁定手机号
  bool lockPhone=true;

  @override
  void initState() {
    super.initState();
    _viewModel = EditUserPageViewModel(this);
    _isAllowConfirmValueNotifier = ValueNotifier(false);
    updateNameValueNotifier = ValueNotifier(null);
    updateNameValueNotifier.addListener(_processUpdateName);
    _terminalController = TextEditingController(text: "");
    _departmentController = TextEditingController(text: "");
    _classController = TextEditingController(text: "");
    initListener();
    //获取用户数据
    _viewModel.getUserInfoHttp(widget.fuid);
  }

  void _processUpdateName() {
    final String updateName = updateNameValueNotifier.value;
    if (_nameController == null) {
      return;
    }
    uploadBean.nick = updateName;
    _nameController.text = _getNameAndNickSplitStr();
  }

  void initListener() {
    _viewModel.userInfoValueNotifier.addListener(() {
      infoDataBean = _viewModel.userInfoValueNotifier.value;
      if (infoDataBean == null) {
        return;
      }
      //转换成上传的bean
      transferToUpdateData(infoDataBean);
      isLoading = false;
      setState(() {});
      checkCommit();
    });
  }

  ///判断提交
  void checkCommit() {
    String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
    _isAllowConfirmValueNotifier.value = errorMsg == null;
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnnotatedRegion(
       Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
      ),
      navigationBarColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(child: _toPlaceHolderWidget()),
      ],
    );
  }

  Widget _toPlaceHolderWidget() {
    return VgPlaceHolderStatusWidget(
        loadingStatus: infoDataBean == null, child: _toEditUserEditWidget());
  }

  Widget _toEditUserEditWidget() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_toNameAndPhoneRowWidget(), _managerWidget()],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      title: "添加管理员",
      rightWidget: ValueListenableBuilder(
        valueListenable: _isAllowConfirmValueNotifier,
        builder: (BuildContext context, bool isAllowConfirm, Widget child) {
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () async {
              VgToolUtils.removeAllFocus(context);
              String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
              if (errorMsg != null && errorMsg.isNotEmpty) {
                _isAllowConfirmValueNotifier.value = false;
                VgToastUtils.toast(context, errorMsg);
                return;
              }
              _isAllowConfirmValueNotifier.value = true;
              if(await setManageDepartmentOrClassHint(uploadBean)){
                _viewModel.saveUserHttp(context, uploadBean,infoDataBean?.groupid);
              }
            },
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: isAllowConfirm,
              width: 48,
              height: 24,
              unSelectBgColor:
              ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              selectedTextStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              text: "保存",
            ),
          );
        },
      ),
    );
  }

  Widget _toNameAndPhoneRowWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(top: 10),
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                EditTextWidget(
                  title: "姓名",
                  hintText: "请输入",
                  isShowRedStar: true,
                  controller: _nameController,
                  initContent: _getNameAndNickSplitStr(),
                  readOnly: true,
                  onTap: StringUtils.isEmpty(uploadBean.nick)
                      ? null
                      : (String editText, dynamic value) {
                    return EditNameDialogWidget.navigatorPushDialog(
                        context, uploadBean.name, uploadBean.nick);
                  },
                  onDecodeResult: (dynamic result) {
                    if (result == null || result is! Map) {
                      return null;
                    }
                    uploadBean.name = result[EDIT_NAME_AND_NICK_BY_NAME_KEY];
                    uploadBean.nick = result[EDIT_NAME_AND_NICK_BY_NICK_KEY];
                    _nameController.text = _getNameAndNickSplitStr();
                    return null;
                  },
                  onChanged: (String editText, dynamic value) {
                    uploadBean.name = editText;
                    uploadBean.nick = value;
                  },
                ),
                _toSplitLineWidget(),
                EditTextWidget(
                  title: "手机",
                  hintText: "请输入",
                  keyboardType: TextInputType.number,
                  readOnly: lockPhone?? false,
                  inputFormatters: [
                    WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                    LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
                  ],
                  initContent: uploadBean.phone,
                  isShowRedStar: uploadBean.phoneRedStar(),
                  onChanged: (String editText, dynamic value) {
                    uploadBean.phone = editText;
                    checkCommit();
                  },
                ),
              ],
            ),
          ),
          SizedBox(
            width: 15,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              onClickUserLogo();
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: Container(
                width: 90,
                height: 90,
                child: StringUtils.isNotEmpty(uploadBean.headFaceUrl)
                    ? VgCacheNetWorkImage(
                  uploadBean.headFaceUrl,
                  imageQualityType: ImageQualityType.middleUp,
                  defaultErrorType: ImageErrorType.head,
                  defaultPlaceType: ImagePlaceType.head,
                )
                    : Image.asset("images/create_user_head_ico.png"),
              ),
            ),
          ),
          SizedBox(width: 15),
        ],
      ),
    );
  }

  void onClickUserLogo() {
    FocusScope.of(context).requestFocus(FocusNode());
    if (StringUtils.isNotEmpty(uploadBean.headFaceUrl)) {
      LogoDetailPage.navigatorPush(context,
          url: uploadBean.headFaceUrl,
          selectMode: SelectMode.HeadBorder,
          clipCompleteCallback: (path, cancelLoadingCallback) {
            uploadBean.headFaceUrl = path;
            setState(() {});
          }
      );
      return;
    }
    addUserLogo();
  }

  void addUserLogo() {
    CommonMoreMenuDialog.navigatorPushDialog(context, {
      "上传图片": () async {
        String path = await ClipImageHeadBorderUtil.clipOneImage(context,
            scaleY: 1, scaleX: 1, maxAutoFinish: true,
            isShowHeadBorderWidget: true);
        if (path == null || path == "") {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      },
      "拍照": () async {
        String path = await FrontCameraHeadPage.navigatorPush(context);
        if (path == null || path.isEmpty) {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      }
    });
  }

  String _getNameAndNickSplitStr() {
    StringBuffer stringBuffer = StringBuffer();
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.name, 7,
          symbol: "..."));
    } else {
      stringBuffer.write(uploadBean.name);
    }
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(" / ");
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.nick, 7,
          symbol: "..."));
    }
    return stringBuffer.toString();
  }

  ///分割线
  Widget _toSplitLineWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 15),
      height: 0.5,
      child: Container(
        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
      ),
    );
  }

  ///转化成待上传的数据
  void transferToUpdateData(EditUserInfoDataBean origin) {
    uploadBean
      ..fid = ''
      ..fuid = widget.fuid
      ..userid = infoDataBean?.userid
      ..nick = infoDataBean?.nick
      ..name = infoDataBean?.name
      ..phone = infoDataBean?.phone
      ..cityId = infoDataBean?.city
      ..cityName = infoDataBean?.city
      ..projectId = infoDataBean?.projectid
      ..projectName = infoDataBean?.projectname
      ..departmentId = infoDataBean?.departmentid
      ..departmentName = infoDataBean?.department
      ..gradeId = (infoDataBean?.classInfo
          ?.map((e) => e.classid)
          ?.toList()
          ?.join(',')) ??
          '' //所在班级id
      ..gradeName = (infoDataBean?.classInfo
          ?.map((e) => e.classname)
          ?.toList()
          ?.join('、')) ??
          '' //所在班级名
      ..classList = infoDataBean?.classInfo
          ?.map((e) => ClassCourseListItemBean(e.classname, e.classid))
          ?.toList() //所在班级
      ..userNumber = infoDataBean?.number
      ..createuid = infoDataBean?.createuid

      ..headFaceUrl = VgStringUtils.getFirstNotEmptyStrByList([
        infoDataBean?.putpicurl,
        infoDataBean?.napicurl,
      ])
      ..terminalList = infoDataBean?.hsnInfo?.map((e) {
        return CompanyChooseTerminalListItemBean()
          ..hsn = e?.hsn
          ..terminalName = e?.terminalName;
      })?.toList()
      ..classIdList = infoDataBean.MClassInfo.map((e) => e.classid).toList()
      ..managerClassName =
      infoDataBean.MClassInfo.map((e) => e.classname).toList().join(',')
      ..managerDepartmentName =
      infoDataBean.MDepartmentInfo.map((e) => e.department)
          .toList()
          .join(',')
      ..departmentList =
      infoDataBean.MDepartmentInfo.map((e) => e.departmentid).toList()
      ..groupBean = (GroupListBean()
        ..groupType = infoDataBean?.groupType
        ..groupid = infoDataBean?.groupid
        ..groupName = infoDataBean?.groupName)
      ..isManagerTerminal = true //添加管理员页面默认打开管理
      ..identityType = CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
          '90')
      ..identityName = CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
          '90')
          .getTypeToStr() ;//添加管理员默认添加为普通管理员
    // 判断锁定部门
    lockDepartment = widget.department != null && widget.departmentId != null;
    List<String> departmentNames =
    infoDataBean.MDepartmentInfo.map((e) => e.department).toList();
    if (lockDepartment) {
      if (!uploadBean.departmentList.contains(widget.departmentId)) {
        uploadBean.departmentList.add(widget.departmentId);
        departmentNames.add(widget.department);
      }
    }
    uploadBean..managerDepartmentName = departmentNames.join('、');

    //判断锁定班级
    lockClass = widget.classid != null && widget.className != null;
    List<String> classNames =
    infoDataBean.MClassInfo.map((e) => e.classname).toList();
    if (lockClass) {
      if (!uploadBean.classIdList.contains(widget.classid)) {
        uploadBean.classIdList.add(widget.classid);
        classNames.add(widget.className);
      }
    }
    uploadBean..managerClassName = classNames.join('、');

    //判断锁定手机号
    lockPhone=uploadBean?.phone?.isNotEmpty??false;

    //
  }

  _managerWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _toIdentityWidget(),
          _toCommonAdminTermainlWidget(), //管理终端
          _toManageDepartmentWidget(), //管理部门
          _toManageClassWidget() //管理班级
        ],
      ),
    );
  }

  ///管理级别
  Widget _toIdentityWidget() {
    return EditTextWidget(
      title: "管理级别",
      hintText: "请选择",
      isShowGoIcon: true,
      isShowRedStar: true,
      initContent: uploadBean.identityName,
      onTap: (String editText, dynamic value) {
        return SelectUtil.showListSelectDialog(
          context: context,
          title: "管理级别",
          positionStr: uploadBean.identityName,
          textList: CompanyAddUserEditInfoIdentityExtension.getListStr(),
        );
      },
      onDecodeResult: (dynamic result) {
        if (result is String && StringUtils.isNotEmpty(result)) {
          return EditTextAndValue(
              editText: result,
              value:
              CompanyAddUserEditInfoIdentityExtension.getStrToType(result));
        }
        return null;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.identityName = editText;
        uploadBean.identityType = value;
        checkCommit();

        setState(() {
          if (value == CompanyAddUserEditInfoIdentityType.superAdmin) {
            _terminalController.text = "全部终端";
            _departmentController.text = "全部部门";
            _classController.text = "全部班级";
          } else {
            //切回普通管理员回复原来的值
            _terminalController.text = _getTerminalEditValueStr(uploadBean.terminalList);

            if (lockClass) {
              _classController.text = uploadBean.managerClassName;
            }else{
              _classController.text="";
            }
            if (lockDepartment) {
              _departmentController.text = uploadBean.managerDepartmentName;
            }else{
              _departmentController.text="";
            }
          }
        });
      },
    );
  }

  ///管理终端
  Widget _toCommonAdminTermainlWidget() {
    return Column(
      children: <Widget>[
        _toSplitLineWidget(),
        EditTextWidget(
          title: "管理终端",
          controller: _terminalController,
          hintText: uploadBean.isSuperAdminRole() ? "全部终端" : "请选择",
          isShowGoIcon: !uploadBean.isSuperAdminRole(),
          initContent: uploadBean.isSuperAdminRole()
              ? ""
              : _getTerminalEditValueStr(uploadBean.terminalList),
          isShowRedStar: false,
          onTap: (String editText, dynamic value) {
            if (uploadBean.isSuperAdminRole()) {
              return null;
            }
            return CompanyChooseTerminalListPage.navigatorPush(
                context, uploadBean?.terminalList);
          },
          onDecodeResult: (dynamic result) {
            if (result is! List<CompanyChooseTerminalListItemBean>) {
              return null;
            }
            List<CompanyChooseTerminalListItemBean> resultList = result;

            EditTextAndValue editTextAndValue = EditTextAndValue(
              editText: _getTerminalEditValueStr(resultList),
              value: result,
            );
            return editTextAndValue;
          },
          onChanged: (String editText, dynamic value) {
            uploadBean.terminalList = value;
          },
        ),
      ],
    );
  }

  ///管理部门
  Widget _toManageDepartmentWidget() {
    return Column(
      children: <Widget>[
        _toSplitLineWidget(),
        EditTextWidget(
            title: "管理部门",
            controller: _departmentController,
            hintText: uploadBean.isSuperAdminRole() ? "全部部门" : "请选择",
            initContent: uploadBean.isSuperAdminRole()
                ? ''
                : uploadBean.managerDepartmentName,
            isShowGoIcon: !uploadBean.isSuperAdminRole(),
            isShowRedStar: false,
            onTap: (String editText, dynamic value) {
              if (lockDepartment) {
                return null;
              }
              if (uploadBean.isSuperAdminRole()) {
                return null;
              }
              return MultiSelectDepartmentWidget.navigatorPush(context,
                  selectIds: uploadBean?.departmentList, multiSelect: true);
            },
            onDecodeResult: (dynamic result) {
              if (result is! EditTextAndValue) {
                return null;
              }
              return result;
            },
            onChanged: (String editText, dynamic value) {
              uploadBean.managerDepartmentName = editText;
              uploadBean?.departmentList = value;
            }),
      ],
    );
  }

  ///管理班级
  _toManageClassWidget() {
    return Visibility(
      visible: isSchool,
      child: Column(
        children: <Widget>[
          _toSplitLineWidget(),
          EditTextWidget(
              title: "管理班级",
              hintText: uploadBean.isSuperAdminRole() ? "全部班级" : "请选择",
              controller: _classController,
              initContent: uploadBean.isSuperAdminRole()
                  ? ''
                  : uploadBean.managerClassName,
              isShowGoIcon: !uploadBean.isSuperAdminRole(),
              isShowRedStar: false,
              onTap: (String editText, dynamic value) {
                if (lockClass) {
                  return null;
                }
                if (uploadBean.isSuperAdminRole()) {
                  return null;
                }
                return MultiSelectClassCourseWidget.navigatorPush(
                    context, uploadBean.classIdList);
              },
              onDecodeResult: (dynamic result) {
                if (result is! EditTextAndValue) {
                  return null;
                }
                return result;
              },
              onChanged: (String editText, dynamic value) {
                uploadBean.managerClassName = editText;
                uploadBean?.classIdList = value;
              }),
        ],
      ),
    );
  }

  ///获取终端编辑值串
  String _getTerminalEditValueStr(
      List<CompanyChooseTerminalListItemBean> resultList) {
    String editTextStr;
    if (resultList == null || resultList.isEmpty) {
      editTextStr = "";
    } else if (resultList.length == 1) {
      editTextStr = resultList
          ?.elementAt(0)
          ?.terminalName;
      //处理空名字
      if (StringUtils.isEmpty(editTextStr)) {
        editTextStr = "1个终端";
      }
    } else {
      editTextStr = "${resultList?.length ?? 0}个终端";
    }
    return editTextStr;
  }

  ///企业用户要有设置管理部门提示，机构用户设置管理班级提示
  Future<bool> setManageDepartmentOrClassHint(EditUserUploadBean uploadBean) async {
    if(isCompany){
      if(uploadBean.departmentList.length>0 ||uploadBean.identityType == CompanyAddUserEditInfoIdentityType.superAdmin){
        return true;
      }else{
        return await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: "确认不添加管理部门？",
            cancelText: "取消",
            confirmText: "确认",
            confirmBgColor: ThemeRepository.getInstance()
                .getPrimaryColor_1890FF());
      }
    }else{
      if(uploadBean.classIdList.length>0&&uploadBean.departmentList.length>0||uploadBean.identityType == CompanyAddUserEditInfoIdentityType.superAdmin){
        return true;
      }else{
        if(uploadBean.classIdList.length>0){
          return await CommonConfirmCancelDialog.navigatorPushDialog(context,
              title: "提示",
              content: "确认不添加管理部门？",
              cancelText: "取消",
              confirmText: "确认",
              confirmBgColor: ThemeRepository.getInstance()
                  .getPrimaryColor_1890FF());
        }
        if(uploadBean.departmentList.length>0){
          return await CommonConfirmCancelDialog.navigatorPushDialog(context,
              title: "提示",
              content: "确认不添加管理班级？",
              cancelText: "取消",
              confirmText: "确认",
              confirmBgColor: ThemeRepository.getInstance()
                  .getPrimaryColor_1890FF());
        }
        return await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: "确认不添加管理部门和管理班级？",
            cancelText: "取消",
            confirmText: "确认",
            confirmBgColor: ThemeRepository.getInstance()
                .getPrimaryColor_1890FF());
      }
    }
  }
}

import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"hsn":"6b93696da4ea7c35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"00","manager":"李岁红","loginName":"王凡语"},{"terminalName":"台式机","terminalPicurl":"http://etpic.we17.com/test/20210122114419_4013.jpg","hsn":"5b63afb7016f1f00","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近润葳酒店","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"2021百事可乐呀","terminalPicurl":"http://etpic.we17.com/test/20210120170519_7532.jpg","hsn":"1ee7ba9ca715b8e3","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"摆放位置嘛","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝1","terminalPicurl":"http://etpic.we17.com/test/20210121101911_5050.jpg","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"3好楼2911","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210122175723_5242.jpg","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210121101755_0700.jpg","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210120213028_3276.jpg","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}],"total":8,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class CompanyChooseTerminalResponseBean extends BasePagerBean<CompanyChooseTerminalListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CompanyChooseTerminalResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyChooseTerminalResponseBean companyChooseTerminalResponseBeanBean = CompanyChooseTerminalResponseBean();
    companyChooseTerminalResponseBeanBean.success = map['success'];
    companyChooseTerminalResponseBeanBean.code = map['code'];
    companyChooseTerminalResponseBeanBean.msg = map['msg'];
    companyChooseTerminalResponseBeanBean.data = DataBean.fromMap(map['data']);
    companyChooseTerminalResponseBeanBean.extra = map['extra'];
    return companyChooseTerminalResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0
      ? 1
      : data.page.current;

  ///得到List列表
  @override
  List<CompanyChooseTerminalListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() =>
      data?.page?.pages == null || data.page.pages <= 0 ? 1 : data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"hsn":"6b93696da4ea7c35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"00","manager":"李岁红","loginName":"王凡语"},{"terminalName":"台式机","terminalPicurl":"http://etpic.we17.com/test/20210122114419_4013.jpg","hsn":"5b63afb7016f1f00","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近润葳酒店","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"2021百事可乐呀","terminalPicurl":"http://etpic.we17.com/test/20210120170519_7532.jpg","hsn":"1ee7ba9ca715b8e3","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"摆放位置嘛","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝1","terminalPicurl":"http://etpic.we17.com/test/20210121101911_5050.jpg","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"3好楼2911","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210122175723_5242.jpg","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210121101755_0700.jpg","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210120213028_3276.jpg","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}],"total":8,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"hsn":"6b93696da4ea7c35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"00","manager":"李岁红","loginName":"王凡语"},{"terminalName":"台式机","terminalPicurl":"http://etpic.we17.com/test/20210122114419_4013.jpg","hsn":"5b63afb7016f1f00","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近润葳酒店","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"2021百事可乐呀","terminalPicurl":"http://etpic.we17.com/test/20210120170519_7532.jpg","hsn":"1ee7ba9ca715b8e3","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"摆放位置嘛","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝1","terminalPicurl":"http://etpic.we17.com/test/20210121101911_5050.jpg","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"3好楼2911","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210122175723_5242.jpg","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210121101755_0700.jpg","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210120213028_3276.jpg","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}]
/// total : 8
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CompanyChooseTerminalListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CompanyChooseTerminalListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// hsn : "6b93696da4ea7c35"
/// companyid : "55a438b79b7c4cd3bf7fec1f603e774a"
/// address : "江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地"
/// bindflg : "00"
/// manager : "李岁红"
/// loginName : "王凡语"

class CompanyChooseTerminalListItemBean {
  String hsn;
  String companyid;
  String address;
  String bindflg;
  String manager;
  String loginName;
  String terminalName;
  String terminalPicurl;
  String sideNumber;

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }

  CommonSingleChoiceStatus selectStatus;


  CompanyChooseTerminalListItemBean({
      this.hsn,
      this.companyid,
      this.address,
      this.bindflg,
      this.manager,
      this.loginName,
      this.terminalName,
      this.terminalPicurl,
      this.selectStatus,
      this.sideNumber
  });

  static CompanyChooseTerminalListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyChooseTerminalListItemBean recordsBean = CompanyChooseTerminalListItemBean();
    recordsBean.hsn = map['hsn'];
    recordsBean.companyid = map['companyid'];
    recordsBean.address = map['address'];
    recordsBean.bindflg = map['bindflg'];
    recordsBean.manager = map['manager'];
    recordsBean.loginName = map['loginName'];
    recordsBean.terminalName = map['terminalName'];
    recordsBean.terminalPicurl = map['terminalPicurl'];
    recordsBean.sideNumber = map['sideNumber'];
    recordsBean.selectStatus = map['selectStatus'];
    return recordsBean;
  }

  Map toJson() => {
    "hsn": hsn,
    "companyid": companyid,
    "address": address,
    "bindflg": bindflg,
    "manager": manager,
    "loginName": loginName,
    "terminalName":terminalName,
    "terminalPicurl":terminalPicurl,
    "sideNumber":sideNumber,
    "selectStatus":selectStatus,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CompanyChooseTerminalListItemBean &&
          hsn == other.hsn;

  @override
  int get hashCode => hsn.hashCode;
}
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 企业选择终端-列表
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 10:58 AM
/// @specialDemand:
class CompanyChooseTerminalListItemWidget extends StatelessWidget {
  final CompanyChooseTerminalListItemBean itemBean;
  final ValueChanged<CompanyChooseTerminalListItemBean> onTap;

  const CompanyChooseTerminalListItemWidget({Key key, this.itemBean, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(onTap != null){
          onTap.call(itemBean);
        }
      },
      child: Container(
        height: 87,
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: _toMainRowWidget(),
      ),
    );
  }

  Widget _toMainRowWidget() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: CommonSingleChoiceButtonWidget(itemBean?.selectStatus),
        ),
        Expanded(
          child: _toColumnWidget(),
        ),
        Offstage(
          offstage: true,
          child: Container(
            width: 90,
            child: Center(
              child:  Text(
                        "开机状态",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                            fontSize: 12,
                            ),
                      ),
            ),
          ),
        )
      ],
    );
  }

  Widget _toColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
         Text(
                   itemBean?.getTerminalName(0) ?? "-",
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                       color: Colors.white,
                       fontSize: 15,
                       ),
                 ),
        SizedBox(height: 4,),
        Text(
          itemBean?.address ?? "-",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
          ),
        ),
        SizedBox(height: 4,),
        Text(
          // "管理员：李珊珊等5人（李珊珊账号登录）",
          "管理员：${itemBean?.manager ?? "-"}"
              "${StringUtils.isNotEmpty(itemBean?.loginName) ? "（${itemBean.loginName}账号登录）":""}",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}


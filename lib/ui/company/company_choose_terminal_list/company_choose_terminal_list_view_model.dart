import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class CompanyChooseTerminalListViewModel extends BasePagerViewModel<
    CompanyChooseTerminalListItemBean, CompanyChooseTerminalResponseBean> {
  ValueNotifier<int> totalValueNotifier;

  final CommonSearchGetContentCallback searchStrFunc;
  List<CompanyChooseTerminalListItemBean> tmpSelectList;

  CompanyChooseTerminalListViewModel(BaseState<StatefulWidget> state,
      this.searchStrFunc, List<CompanyChooseTerminalListItemBean> selectedList)
      : super(state) {
    totalValueNotifier = ValueNotifier(0);
    if (selectedList != null && selectedList.isNotEmpty) {
      tmpSelectList = List()..addAll(selectedList);
    }
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    super.onDisposed();
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "companyid": UserRepository.getInstance().companyId ?? "",
      "current": page ?? 1,
      "size": 2000,
      "flg": "01", // 00全部 01绑定的
      if(StringUtils.isNotEmpty(searchStrFunc?.call() ?? ""))
      "tname":searchStrFunc?.call() ?? "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appTerminalList";
  }

  @override
  CompanyChooseTerminalResponseBean parseData(VgHttpResponse resp) {
    CompanyChooseTerminalResponseBean vo =
        CompanyChooseTerminalResponseBean.fromMap(resp?.data);
    loading(false);
    totalValueNotifier?.value = vo?.data?.page?.total ?? 0;
    List<CompanyChooseTerminalListItemBean> list = vo?.data?.page?.records;
    if (list != null && list.isNotEmpty) {
      List<CompanyChooseTerminalListItemBean> newList = List();
      for (CompanyChooseTerminalListItemBean item in list) {
        if (tmpSelectList != null &&
            tmpSelectList.isNotEmpty &&
            tmpSelectList.indexOf(item) != -1) {
          item.selectStatus = CommonSingleChoiceStatus.selected;
          tmpSelectList[tmpSelectList.indexOf(item)] = item;
          newList.insert(0, item);
        } else {
          item.selectStatus = CommonSingleChoiceStatus.unSelect;
          newList.add(item);
        }
      }
      vo.data.page.records=newList;
    }
    return vo;
  }
}

///刷新企业详情页
class RefreshCompanyDetailPageEvent {
  String msg;
  RefreshCompanyDetailPageEvent({this.msg}){
    print('刷新企业详情页事件：${msg??''}');
  }
}
///公司切换
class ChangeCompanyEvent{
  String companyName;
  ChangeCompanyEvent({this.companyName}){
    print('公司切换事件：${companyName??''}');
  }
}

//刷新个人详情界面
class RefreshPersonDetailEvent{}

//刷新分支机构详情界面
class RefreshBranchDetailEvent{}

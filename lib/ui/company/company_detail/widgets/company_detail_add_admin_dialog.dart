import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/company_choose_terminal_list_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_screen_lib.dart';

/// 企业主页-新增弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 9:34 AM
/// @specialDemand:
class CompanyDetailAddAdminDialog extends StatefulWidget {
  @override
  _CompanyDetailAddAdminDialogState createState() =>
      _CompanyDetailAddAdminDialogState();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context) {
    VgToolUtils.removeAllFocus(context);
    return VgDialogUtils.showCommonBottomDialog(
      context: context,
      child: CompanyDetailAddAdminDialog(),
    );
  }
}

class _CompanyDetailAddAdminDialogState
    extends State<CompanyDetailAddAdminDialog> {
  TextEditingController _nameEditingController;

  TextEditingController _phoneEditingController;

  TextEditingController _terminalEditingController;

  ///是否是超级管理员
  bool _isSuperAdmin = true;

  @override
  void initState() {
    super.initState();
    _nameEditingController = TextEditingController();
    _phoneEditingController = TextEditingController();
    _terminalEditingController = TextEditingController();
  }

  @override
  void dispose() {
    _nameEditingController?.dispose();
    _phoneEditingController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            borderRadius: BorderRadius.vertical(
                top: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS))),
        child: _toMainColumnWidget(),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 20),
        _toRowNamePhoneWithPicWidget(),
        SizedBox(height: 12),
        _toTerminalEditWidget(),
        SizedBox(height: 12),
        _toCompetenceWidget(),
        SizedBox(height: 12),
        _toTipsTextWidget(),
        SizedBox(height: 30),
        _toConfirmButtonWidget(),
      ],
    );
  }

  Widget _toRowNamePhoneWithPicWidget(){
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            height: 102,
            child: Column(
              children: <Widget>[

                _toNameEditWidget(),
                Spacer(),
                _toPhoneEditWidget(),
              ],
            ),
          ),
        ),
        // SizedBox(width: 15,),
        ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Container(
            width: 102,
            height: 102,
            color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                 Image.asset("images/add_ico.png",width: 34,),
                  
                  SizedBox(height: 10.5,),
                   Text(
                             "照片/选填",
                             maxLines: 1,
                             overflow: TextOverflow.ellipsis,
                             style: TextStyle(
                                 color: Color(0xFF3A3F50),
                                 fontSize: 12,
                                 ),
                           )
                ],
              ),
            ),
          ),
        ),
        SizedBox(width: 15,)
      ],
    );
  }

  Container _toTipsTextWidget() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.centerLeft,
      child: Text(
        "注：添加后，对方将收到短信通知～",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 12,
        ),
      ),
    );
  }

  _BindingAddAdminDialogEditWidget _toCompetenceWidget() {
    return _BindingAddAdminDialogEditWidget(
      text: "权限",
      hintText: "请输入",
      editingController: _phoneEditingController,
      isPhone: true,
      isShowRedDot: false,
      customMainWidget: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
                onTap: (){
                  if(_isSuperAdmin){
                    return;
                  }
                  _isSuperAdmin = true;
                  setState(() {

                  });
                },
                child: _radioWithTextWidget(text: "超级管理员",isSelected: _isSuperAdmin,)),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
                onTap: (){
                  if(!_isSuperAdmin){
                    return;
                  }
                  _isSuperAdmin = false;
                  setState(() {

                  });
                },
                child: _radioWithTextWidget(text: "普通管理员",isSelected: !_isSuperAdmin,)),
          ),
        ],
      ),
    );
  }

  _BindingAddAdminDialogEditWidget _toTerminalEditWidget() {
    return _BindingAddAdminDialogEditWidget(
      text: "终端",
      hintText: "请选择",
      editingController: _terminalEditingController,
      isPhone: true,
      isShowRedDot: false,
      isReadOnly: true,
      rightWidget: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Image.asset(
          "images/go_ico.png",
          width: 6,
          color: ThemeRepository.getInstance()
              .getTextMainColor_D0E0F7()
              .withOpacity(0.6),
        ),
      ),
      onTap: () {
        // CompanyChooseTerminalListPage.navigatorPush(context);
      },
    );
  }

  _BindingAddAdminDialogEditWidget _toPhoneEditWidget() {
    return _BindingAddAdminDialogEditWidget(
      text: "手机",
      hintText: "请输入",
      editingController: _phoneEditingController,
      isPhone: true,
    );
  }

  _BindingAddAdminDialogEditWidget _toNameEditWidget() {
    return _BindingAddAdminDialogEditWidget(
      text: "姓名",
      hintText: "请输入",
      editingController: _nameEditingController,
    );
  }

  Widget _toTopTitleAndConfirmWidget() {
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 15,
          ),
          Text(
            "新建管理员",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
          Spacer(),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: false,
              width: 76,
              height: 30,
              unSelectBgColor:
                  ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              selectedBgColor:
                  ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 14,
              ),
              selectedTextStyle: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              text: "确定",
              onTap: () {
                RouterUtils.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _toConfirmButtonWidget() {
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: false,
      height: 40,
      margin: const EdgeInsets.symmetric(horizontal: 25),
      unSelectBgColor: Color(0xFF3A3F50),
      selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      unSelectTextStyle: TextStyle(
        color: VgColors.INPUT_BG_COLOR,
        fontSize: 15,
      ),
      selectedTextStyle: TextStyle(
        color: Colors.white,
        fontSize: 15,
      ),
      text: "确认",
    );
  }
}

/// 新增管理员弹窗编辑组件
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 5:18 PM
/// @specialDemand:
class _BindingAddAdminDialogEditWidget extends StatelessWidget {
  final String text;

  final String hintText;

  final Widget rightWidget;

  final TextEditingController editingController;

  final bool isPhone;

  final bool isShowRedDot;

  final bool isReadOnly;

  final VoidCallback onTap;

  final Widget customMainWidget;

  const _BindingAddAdminDialogEditWidget(
      {Key key,
      this.hintText,
      this.rightWidget,
      this.text,
      this.editingController,
      this.isPhone = false,
      this.isShowRedDot = true,
      this.isReadOnly = false,
      this.onTap, this.customMainWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          borderRadius: BorderRadius.circular(4)),
      child: Row(
        children: <Widget>[
          Opacity(
            opacity: isShowRedDot ? 1 : 0,
            child: Container(
              width: 15,
              alignment: Alignment(2 / 5, 0),
              padding: const EdgeInsets.only(top: 7),
              child: Text(
                "*",
                style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getMinorRedColor_F95355(),
                    fontSize: 14,
                    height: 1.2),
              ),
            ),
          ),
          Container(
            width: 43,
            alignment: Alignment.centerLeft,
            child: Text(
              text ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance()
                    .getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: customMainWidget?? VgTextField(
              maxLines: 1,
              controller: editingController,
              readOnly: isReadOnly,
              keyboardType: isPhone ? TextInputType.number : null,
              inputFormatters: isPhone
                  ? [
                      WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                      LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
                    ]
                  : null,
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(bottom: 2),
                hintText: hintText ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              onTap: onTap,
              style: TextStyle(
                  fontSize: 14,
                  color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
            ),
          ),
          rightWidget ?? SizedBox(width: 15)
        ],
      ),
    );
  }
}

/// 单选带文字的组件
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 11:45 AM
/// @specialDemand:
class _radioWithTextWidget extends StatelessWidget {
  final String text;

  final bool isSelected;

  const _radioWithTextWidget({Key key, this.text, this.isSelected = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CommonSingleChoiceButtonWidget(
                isSelected ? CommonSingleChoiceStatus.selected:CommonSingleChoiceStatus.unSelect
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              text ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color:
                  ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 13,
                  height: 1.2),
            )
          ],
        ),
      ),
    );
  }
}



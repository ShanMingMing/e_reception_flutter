import 'package:flutter/widgets.dart';

import 'company_detail_setting_dialog.dart';

/// 企业详情-设置按钮
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 2:10 PM
/// @specialDemand:
class CompanyDetailSettingButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        CompanyDetailSettingDialog.navigatorPushDialog(context);
      },
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Center(
              child: Image.asset(
            "images/company_detail_more_setting_ico.png",
            width: 32,
          ))),
    );
  }
}



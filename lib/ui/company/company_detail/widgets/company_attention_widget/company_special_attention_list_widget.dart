import 'dart:async';

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_topbar_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_batch_operation_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

class CompanySpecialAttentionListPage extends StatefulWidget {
  final List<GroupPersonCntBean> tabsSelectedList;
  final int selectIndex;
  const CompanySpecialAttentionListPage(
      {Key key,this.tabsSelectedList, this.selectIndex})
      : super(key: key);

  @override
  CompanySpecialAttentionListPageState createState() =>
      CompanySpecialAttentionListPageState();
}

class CompanySpecialAttentionListPageState extends BasePagerState<
    CompanyDetailByTypeListItemBean,
    CompanySpecialAttentionListPage> with TickerProviderStateMixin {


  List<CompanyDetailByTypeListItemBean> listItemSelect;
  StreamController<CompanyDetailByTypeListItemBean> listBeanController = StreamController.broadcast();
  TabController tabController;
  List<GroupPersonCntBean> tabsListGroupPersonCntBean = new List<GroupPersonCntBean>();
  int _selectIndex = 0;
  @override
  void initState() {
    super.initState();
    _selectIndex = widget?.selectIndex??0;
    if(widget?.tabsSelectedList != null){
      widget?.tabsSelectedList?.forEach((element) {
        if(element?.groupid!="special"){
          tabsListGroupPersonCntBean.add(element);
        }
      });
    }
    if(tabsListGroupPersonCntBean == null){
      VgToastUtils.toast(context, "暂无数据");
    }
    // _viewModel = SpecialAttentionStaffListViewModel(this);
    if((widget?.selectIndex??0) >= (tabsListGroupPersonCntBean?.length??0)){
      _selectIndex = 0;
    }
    listItemSelect = new List<CompanyDetailByTypeListItemBean>();
    tabController = TabController(length: tabsListGroupPersonCntBean?.length, vsync: this, initialIndex: _selectIndex);
    // _viewModel?.refresh();
  }

  @override
  void dispose() {
    tabController?.dispose();
    listBeanController?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: ScrollConfiguration(
        behavior: MyBehavior(),
        child: _specialAttentionList(context),
      ),
    );
  }

  Widget _specialAttentionList(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _vgTopBarWidget(),
        SpecialAttentionTopBarWidget(
          tabController: tabController,
          tabsSelectedList: tabsListGroupPersonCntBean,
        ),
        // Expanded(child: _toListBean()),
        // _toFunctionBar(),
        Expanded(
            child: TabBarView(
              controller: tabController,
              // children: List.generate(3, (index) => Container(child: Center(child: Text("$index",style: TextStyle(color: Colors.white),),),)),
              children: List.generate(
                  tabsListGroupPersonCntBean?.length, (index) => _toListBean(index)),
            ),
        ),
        SpecialBatchOperationWidget(controller:listBeanController),
      ],
    );
  }

  Widget _toListBean(int index) {
    return SpecialAttentionStaffListWidget(tabController:tabController,tabsList:tabsListGroupPersonCntBean[index], listItemSelect: listItemSelect,multiHandleCallBack: (companyDetailByTypeListItemBean) {
      listBeanController?.add(companyDetailByTypeListItemBean);
    },);
  }

  Widget _vgTopBarWidget() {
    return VgTopBarWidget(
      title: "请选择",
      isShowBack: true,
    );
  }
}

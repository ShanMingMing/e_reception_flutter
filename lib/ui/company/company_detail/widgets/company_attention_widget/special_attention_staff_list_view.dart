import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_delete.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../../../app_main.dart';
import '../../company_detail_page.dart';

class SpecialAttentionStaffListViewModel extends BasePagerViewModel<
    CompanyDetailByTypeListItemBean, CompanyDetailByTypeListResponseBean> {
  ///App删除用户
  static const String DELETE_USER_API =ServerApi.BASE_URL + "app/appRmoveUser";

  ///App显示用户列表
  // static const String SELECT_USER_API =ServerApi.BASE_URL + "app/appCompanyPages";
  final GroupPersonCntBean tabsList;

  ValueNotifier<CompanyDetailByTypeListResponseBean> topInfoValueNotifier;

  SpecialAttentionStaffListViewModel(BaseState state, this.tabsList)
      : super(state) {
    topInfoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    topInfoValueNotifier?.dispose();
    super.onDisposed();
  }

  //删除用户
  void deleteUserHttp(BuildContext context,
      {String fuid, String roleid, String userid}) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "人员获取信息错误");
      return;
    }
    if (StringUtils.isEmpty(roleid)) {
      VgToastUtils.toast(context, "身份错误");
      return;
    }
    VgHudUtils.show(context, "删除中");
    VgHttpUtils.get(DELETE_USER_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
          "roleid": roleid ?? "",
          "userid": userid ?? ""
        },
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, "删除成功");
          //刷新
          VgEventBus.global.send(MultiDelegeRefreshEvent());
          RouterUtils.pop(context);
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }

  //批量删除用户
  void batchDeleteUserHttp(BuildContext context,
      {String fuid, String roleid, String userid}) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "人员获取信息错误");
      return;
    }
    if (StringUtils.isEmpty(roleid)) {
      VgToastUtils.toast(context, "身份错误");
      return;
    }
    VgHudUtils.show(context, "删除中");
    VgHttpUtils.get(DELETE_USER_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
          "roleid": roleid ?? "",
          "userid": userid ?? ""
        },
        callback: BaseCallback(onSuccess: (val) {
          VgEventBus.global.send(MultiDelegeRefreshEvent());
          VgEventBus.global.send(CompanyNumBarEventMonitor());
          VgHudUtils.hide(context);
          // VgToastUtils.toast(context, "删除成功");
          //刷新
          // RouterUtils.pop(context);
          //跳转到企业详情界面
          RouterUtils?.popUntil(context, "CompanyDetailPage");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }


//  批量关注
  void batchChangeVeryCareStatus(
    BuildContext context,
    String fuid,
    bool isSpecial,
  ) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "请选择非特别关注人员");
      return;
    }
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appSpecialFocusFaceUser",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          //00取消 01关注
          "flg": (isSpecial ?? false) ? "01" : "00",
          "fuid": fuid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          // refreshMultiPage();
          ///回调处理特别关心回调（接口保存判断，回调方法去除）
          // if(state!=null){
          //   state?.widget?.onChangeVeryCare?.call();
          // }
          VgHudUtils.hide(context);
          VgEventBus.global.send(MultiDelegeRefreshEvent());
          // RouterUtils.pop(context);
          //跳转到企业详情界面
          RouterUtils?.popUntil(context, "CompanyDetailPage");
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

//  批量更改部门/城市/项目/班级信息
  void batchAppUpdateChooseInfo(
    BuildContext context,
      String fuids,
    String city,
    String classid,
    String courseid,
    String departmentid,
    String projectid
  ) {
    if (StringUtils.isEmpty(fuids)) {
      VgToastUtils.toast(context, "个人信息获取失败");
      return;
    }
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appUpdateChooseInfo",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuids": fuids ?? "",
          "city": city ?? "",
          "classid": classid ?? "",
          "courseid": courseid ?? "",
          "departmentid": departmentid ?? "",
          "projectid": projectid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          // refreshMultiPage();
          ///回调处理特别关心回调（接口保存判断，回调方法去除）
          // if(state!=null){
          //   state?.widget?.onChangeVeryCare?.call();
          // }
          VgHudUtils.hide(context);
          VgEventBus.global.send(MultiDelegeRefreshEvent());
          // RouterUtils.pop(context);
          //跳转到企业详情界面
          RouterUtils?.popUntil(context, "CompanyDetailPage");
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "groupid": tabsList?.groupid ?? "",
      "groupType": tabsList?.groupType ?? "",
      // "searchName":"",
      // "searchOthers": state?.SpecialAttentionStaffListWidget?.companyDetailFilterValueNotifier?.value?.getParamsStr() ?? "",
      // "isVisitor": "",
      // "visitorGroupId":  "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appCompanyPages";
  }

  @override
  CompanyDetailByTypeListResponseBean parseData(VgHttpResponse resp) {
    CompanyDetailByTypeListResponseBean vo =
        CompanyDetailByTypeListResponseBean.fromMap(resp?.data);
    loading(false);
    // statisticsValueNotifier?.value = vo?.data?.statistics;
    // if (groupBean.isAllGroup()) {
    //   state?.SpecialAttentionStaffListWidget?.statisticsValueNotifier?.value =
    //   (vo?.data?.statistics);
    // }
    return vo;
  }

  @override
  void refresh() {
    if (isStateDisposed) {
      return;
    }
    super.refresh();
  }

  @override
  void refreshMultiPage() {
    if (isStateDisposed) {
      return;
    }
    super.refreshMultiPage();
  }
}

class MultiDelegeRefreshEvent {}

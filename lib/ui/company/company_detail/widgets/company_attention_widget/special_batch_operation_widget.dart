import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list_view.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_batch_operation_dialog.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

class SpecialBatchOperationWidget extends StatefulWidget {
  final StreamController<CompanyDetailByTypeListItemBean> controller;

  const SpecialBatchOperationWidget({Key key, this.controller})
      : super(key: key);

  @override
  _SpecialBatchOperationWidgetState createState() =>
      _SpecialBatchOperationWidgetState();
}

class _SpecialBatchOperationWidgetState
    extends State<SpecialBatchOperationWidget> {
  bool isPictureList = true;
  List<CompanyDetailByTypeListItemBean> totalList;
  StreamSubscription eventStreamSubscription;

  @override
  void initState() {
    super.initState();
    //观察者模式,监听接口是否使用
    eventStreamSubscription =
        VgEventBus.global.on<MultiDelegeRefreshEvent>().listen((event) {
      totalList = new List<CompanyDetailByTypeListItemBean>();
      setState(() {});
    });
    totalList = new List<CompanyDetailByTypeListItemBean>();
    widget?.controller?.stream?.listen((event) {
      for (int i = 0; i < totalList?.length; i++) {
        if (totalList[i] == event) {
          totalList?.remove(totalList[i]);
          setState(() {});
          return;
        }
      }
      totalList.add(event);
      setState(() {});
    });
  }

  @override
  void dispose() {
    eventStreamSubscription.cancel();
    widget?.controller?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
          height: 60,
          color: Color(0xFF3A3F50),
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                isPictureList
                    ? _pictureList()
                    : Text(
                        "请选择",
                        style:
                            TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 15),
                      ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    // SpecialAttentionStaffListWidget(vgBaseCallbackSelect: (List<CompanyDetailByTypeListItemBean> bean,bool isSelectList){
                    //
                    // },);
                    SpecialBatchOperationDialog.navigatorPushDialog(
                        context, totalList);
                    setState(() {});
                  },
                  child: Container(
                    height: 40,
                    width: 120,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Color(0xFF1890FF),
                        borderRadius: BorderRadius.circular(20)),
                    child: Text(
                      "批量操作",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
  }

  Widget _pictureList() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          width: 67,
          child: Text(
            "已选${totalList?.length ?? 0}人",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
        // Spacer(),
        for (int i = 0; i < totalList?.length; i++)
            if(i <= 3)
          Stack(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.only(right: 6),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(32),
                    child: VgCacheNetWorkImage(
                      showOriginEmptyStr(totalList[i]?.putpicurl == ""
                          ? null
                          : totalList[i]?.putpicurl) ??
                          showOriginEmptyStr((totalList[i]?.napicurl == ""
                              ? null
                              : totalList[i]?.napicurl) ??
                              (totalList[i]?.pictureUrl == ""
                                  ? null
                                  : totalList[i]?.pictureUrl ??
                                  "images/default_head_ico.png")),
                      fit: BoxFit.cover,
                      imageQualityType: ImageQualityType.middleDown,
                      defaultErrorType: ImageErrorType.head,
                      defaultPlaceType: ImagePlaceType.head,
                      width: (ScreenUtils.screenW(context) - 266) / 4,
                      height: (ScreenUtils.screenW(context) - 266) / 4,
                    ),
                  ),
                ),
              ),
              if(i >=3)
              _textCount()
            ],
          ),
      ],
    );
  }

  Widget _textCount() {
    int textCount = totalList?.length ?? 0;
    return Positioned(
      top: 0,
      right: 0,
      child: Opacity(
        opacity: textCount - 4 > 0 ? 1 : 0,
        child: Padding(
          padding: EdgeInsets.only(right: 6),
          child: Container(
            alignment: Alignment.center,
            width: (ScreenUtils.screenW(context) - 266) / 4,
            height: (ScreenUtils.screenW(context) - 266) / 4,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              color: Color(0x80000000).withOpacity(0.5),
            ),
            child: Text(
              "+${textCount - 4}",
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),
        ),
      ),
    );
  }

}

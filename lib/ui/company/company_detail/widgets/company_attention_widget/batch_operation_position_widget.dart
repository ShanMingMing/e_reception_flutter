import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/common_title_edit_with_underline_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list_view.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/cityWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/departmentWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/projectWidget.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class BatchOperationPositionWidget extends StatefulWidget {

  final bool isEmptyHide;
  final List<CompanyDetailByTypeListItemBean> totalList;
  BatchOperationPositionWidget({Key key,this.isEmptyHide = false, this.totalList}) : super(key: key);

  @override
  _BatchOperationPositionWidgetState createState() => _BatchOperationPositionWidgetState();
}

class _BatchOperationPositionWidgetState extends BasePagerState<
    CompanyDetailByTypeListItemBean, BatchOperationPositionWidget> {
  bool clickSave = false;
  SpecialAttentionStaffListViewModel _viewModel;

  String selectedText = "";
  String cityText = "";
  String projectText = "";
  String classText = "";
  String courseText = "";
  String departmentText = "";
  CreateUserUploadBean uploadBean;
  @override
  void initState() {
    super.initState();
    _viewModel = SpecialAttentionStaffListViewModel(this, null);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF191E31),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _toTopBarWidget(),
          //根据选择不同的身份判断显示界面不同
          widget.isEmptyHide ?_toDepartmentProjectCity(context):_toClassCourse(context),
          // _toClassCourse(context),//课程班级界面
          // _toDepartmentProjectCity(context),//项目城市部门界面

        ],
      ),
    );
  }

  Widget _toDepartmentProjectCity(BuildContext context){
    return  Container(
      color: Color(0xFF21263C),
      height: 50,
      // child: _toMainRowWidget(context),
      // child: _toDepartmentWidget(context),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _toDepartmentWidget(context),
          // _toProjectWidget(context),
          // _toCityWidget(context),
          Spacer(),
        ],
      ),
    );
  }

  Widget _toClassCourse(BuildContext context){
    return Container(
      color: Color(0xFF21263C),
      height: 50,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _toClassWidget(context),
          // _toCourseWidget(context),
          Spacer(),
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      // title: "",
      isShowBack: true,
      rightWidget: CommonFixedHeightConfirmButtonWidget(
        isAlive: clickSave,
        width: 48,
        height: 24,
        unSelectBgColor:
        ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor:
        ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
            fontWeight: FontWeight.w600),
        selectedTextStyle: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        text: "保存",
        onTap: () {
          List<String> listFuid = new List<String>();
          String fuidStrs = "";
          if (widget?.totalList?.length == 0) {
            VgToastUtils.toast(context, "请选择需要设置的人员");
            return;
          }
            if (widget?.totalList?.length != 0) {
              for (int i = 0; i < widget?.totalList?.length; i++) {
                listFuid.add(widget?.totalList[i]?.fuid);
              }
              fuidStrs = VgStringUtils.getSplitStr(listFuid, symbol: ",");
            }
            _viewModel?.batchAppUpdateChooseInfo(
                context, fuidStrs,cityText, classText,courseText,departmentText,projectText);

          // _viewModel?.refreshMultiPage();
          setState(() {});
        },
      ),
    );
  }

  Widget _toDepartmentWidget(BuildContext context) {
    return _EditTextWidget(
      title: "部门",
      hintText: "请选择",
      isShowGoIcon: true,
      initValue: "",
      initContent: "",
      onTap: (String editText, dynamic value) {
        return DepartmentWidget.navigatorPush(
            context, value);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        departmentText = value;
        clickSave = true;
        setState(() {

        });
        // widget.uploadBean.departmentId = value;
        // widget.uploadBean.departmentName = editText;
      },
    );
  }

  Widget _toProjectWidget(BuildContext context) {
    return _EditTextWidget(
      title: "项目",
      hintText: "请选择",
      isShowGoIcon: true,
      initValue: "",
      initContent: "",
      onTap: (String editText, dynamic value) {
        return ProjectWidget.navigatorPush(
            context, value);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        projectText = value;
        clickSave = true;
        setState(() {

        });
        // widget.uploadBean.projectId = value;
        // widget.uploadBean.projectName = editText;
      },
    );
  }

  Widget _toCityWidget(BuildContext context) {
    return _EditTextWidget(
      title: "城市",
      hintText: "请选择",
      isShowGoIcon: true,
      initValue: "",
      initContent: "",
      onTap: (String editText, dynamic value) {
        return CityWidget.navigatorPush(context, value);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        cityText = editText;
        clickSave = true;
        setState(() {

        });
        // widget.uploadBean.cityId = editText;
        // widget.uploadBean.cityName = editText;
      },
    );
  }

  Widget _toClassWidget(BuildContext context) {
    List<ClassCourseListItemBean> classListName;
    List<String> listClassName = new List<String>();
    List<String> listClassId = new List<String>();
    String classNameStrs = "";
    String classIdStrs = "";
    return _EditTextWidget(
      title: "班级",
      hintText: "请选择",
      isShowGoIcon: true,
      onTap: (String editText, dynamic value) {
        return ClassCourseWidget.navigatorPush(context, uploadBean?.classList);
      },
      onDecodeResult: (dynamic result) {
        if (result is! List<ClassCourseListItemBean>) {
          return null;
        }
        classListName = result;
        classListName?.forEach((element) {
          listClassName?.add(element?.name);
          listClassId?.add(element?.id);
        });
        classNameStrs = VgStringUtils.getSplitStr(listClassName, symbol: "、");
        classIdStrs = VgStringUtils.getSplitStr(listClassId, symbol: ",");
        classText = classIdStrs;
        return EditTextAndValue(
            value: result,
            editText: VgStringUtils.subStringAndAppendSymbol(
                classNameStrs, 7,
                symbol: "...")??""
        );
      },
      onChanged: (String editText, dynamic value) {
        uploadBean?.classList = value;
        clickSave = true;
        setState(() {

        });
        // widget.uploadBean.cityId = editText;
        // widget.uploadBean.cityName = editText;
      },
    );
  }

  Widget _toCourseWidget(BuildContext context) {
    return _EditTextWidget(
      title: "课程",
      hintText: "请选择",
      isShowGoIcon: true,
      initValue: "",
      initContent: "",
      onTap: (String editText, dynamic value) {
        return CourseClassWidget.navigatorPush(context, value);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        courseText = value;
        clickSave = true;
        setState(() {

        });
        // widget.uploadBean.cityId = editText;
        // widget.uploadBean.cityName = editText;
      },
    );
  }
}

/// 编辑框组件
///
class _EditTextWidget extends StatelessWidget {
  final String title;

  final String hintText;

  final CommonTitleEditEditAndValueChangedCallback onChanged;

  ///点击
  final CommonTitleNavigatorPushCallback onTap;

  ///解析跳转返回值
  final CommonTitlePopResultDecodeCallback onDecodeResult;

  final bool isShowRedStar;

  ///中字极限
  final int maxZHCharLimit;

  ///极限字数回调
  final ValueChanged<int> limitCharFunc;

  ///限制
  final List<TextInputFormatter> inputFormatters;

  ///是否显示跳转icon
  final bool isShowGoIcon;

  ///初始化数据
  final String initContent;

  ///初始值
  final dynamic initValue;

  ///只读
  final bool readOnly;

  ///是否去掉红星
  final bool isGoneRedStar;

  ///是否跟随编辑框
  final bool isFollowEdit;

  final TextInputType keyboardType;

  final TextEditingController controller;

  final Function(Widget titleWidget, Widget textFieldWidget, Widget leftWidget,
      Widget rightWidget) customWidget;

  const _EditTextWidget(
      {Key key,
        this.title,
        this.hintText,
        this.onChanged,
        this.isShowRedStar = false,
        this.onTap,
        this.onDecodeResult,
        this.maxZHCharLimit,
        this.limitCharFunc,
        this.inputFormatters,
        this.isShowGoIcon = false,
        this.initContent,
        this.initValue,
        this.readOnly,
        this.isGoneRedStar = false,
        this.isFollowEdit = false,
        this.customWidget, this.controller, this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommonTitleEditWithUnderlineWidget(
      controller: controller,
      titleTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 14),
      editTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 14),
      titleWidth: 43,
      title: title,
      height: 50,
      onChanged: onChanged,
      onDecodeResult: onDecodeResult,
      onTap: onTap,
      minLines: 1,
      maxLines: 1,
      readOnly: readOnly,
      maxZHCharLimit: maxZHCharLimit,
      limitCharFunc: limitCharFunc,
      inputFormatters: inputFormatters,
      customWidget: customWidget,
      keyboardType: keyboardType,
      lineMargin: const EdgeInsets.only(left: 15, right: 0),
      lineUnselectColor: Color(0xFF303546),
      lineHeight: 0,
      initContent: initContent,
      initValue: initValue,
      lineSelectedColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      hintText: hintText,
      hintTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          fontSize: 14),
      rightWidget: Offstage(
        offstage: !isShowGoIcon,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Image.asset(
            "images/go_ico.png",
            width: 6,
            color: VgColors.INPUT_BG_COLOR,
          ),
        ),
      ),
      leftWidget: Offstage(
        offstage: isGoneRedStar,
        child: Container(
          width: 15,
          child: Opacity(
            opacity: isShowRedStar ? 1 : 0,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  "*",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getMinorRedColor_F95355(),
                      fontSize: 14,
                      height: 1.1),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

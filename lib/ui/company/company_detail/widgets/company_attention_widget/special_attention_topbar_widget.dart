import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SpecialAttentionTopBarWidget extends StatelessWidget {
  final TabController tabController;
  final List<GroupPersonCntBean> tabsSelectedList;

  SpecialAttentionTopBarWidget({Key key, this.tabController, this.tabsSelectedList})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: const EdgeInsets.only(bottom: 5),
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      child: Row(
        children: [
          Expanded(
            child: Theme(
                data: ThemeData(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent),
                child: _toTabBarWidget(context)),
          ),
          // StreamBuilder(
          //   stream: CompanyDetailPageState.of(context).tabsChangeStream,
          //   builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          //     return CompanyDetailFilterButtonWidget();
          //   },
          // )
        ],
      ),
    );
  }

  Widget _toTabBarWidget(BuildContext context) {

    return TabBar(
      controller: tabController,
      isScrollable: true,
      indicatorSize: TabBarIndicatorSize.label,
      unselectedLabelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 15,
          height: 1.2),
      labelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 15,
          fontWeight: FontWeight.w600,
          height: 1.2),
      labelPadding: EdgeInsets.only(left: 15, right: 0),
      indicatorColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
      indicatorPadding: EdgeInsets.only(left: 10, right: 24),
      tabs: List.generate(tabsSelectedList?.length ?? 0, (index) {
        return Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(tabsSelectedList[index]?.groupName ?? ""),
              SizedBox(
                width: 3,
              ),
              Container(
                width: 24,
                child: Text(
                  "${tabsSelectedList[index]?.cnt}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}

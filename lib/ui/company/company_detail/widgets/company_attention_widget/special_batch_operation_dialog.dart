import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_with_up_or_down_arrows_dialog/common_with_up_or_down_arrows_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list_view.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/departmentWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/personal_details_information_widget/personal_class_widget.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'batch_operation_position_widget.dart';

///特别关心批量操作

class SpecialBatchOperationDialog extends StatefulWidget {
  final BuildContext context;
  final List<CompanyDetailByTypeListItemBean> totalList;

  const SpecialBatchOperationDialog({
    Key key,
    this.context,
    this.totalList,
  }) : super(key: key);

  // final FaceUserInfoBean faceUserInfoBean;
  // SpecialBatchOperationDialog({
  //   // Key key,this.faceUserInfoBean,
  // }) : super(key: key);

  static Future<dynamic> navigatorPushDialog(
      BuildContext context, List<CompanyDetailByTypeListItemBean> totalList) {
    bool isShowStudentOrStaff = true;
    String groupNameStrs = "";
    List<String> listGroupName = new List<String>();
    if (totalList?.length != 0) {
      for (int i = 0; i < totalList?.length; i++) {
        if(totalList[0]?.groupName!=totalList[i]?.groupName){
          isShowStudentOrStaff = false;
        }
        listGroupName.add(totalList[i]?.groupName);
      }
      if(isShowStudentOrStaff){
        groupNameStrs = VgStringUtils.getSplitStr(listGroupName, symbol: ",");
        if(groupNameStrs.contains("员工")|| groupNameStrs.contains("教工")){
          isShowStudentOrStaff = true;
        }
        if(groupNameStrs.contains("学员")|| groupNameStrs.contains("学生")){
          isShowStudentOrStaff = true;
        }
        if(groupNameStrs.contains("家长")){
          isShowStudentOrStaff = false;
        }
        // if(!AppOverallDistinguish.comefromAiOrWeStudy()){
        //   isShowStudentOrStaff = false;
        // }
      }

    }else{
      isShowStudentOrStaff = false;
    }
    return CommonWithUpOrDownArrowsDialog.navigatorPushForDialog<dynamic>(
        context,
        dialogMaxHeight:isShowStudentOrStaff&&AppOverallDistinguish.comefromAiOrWeStudy() ?155:120,//120
        arrowsSize: Size(11, 6),
        safeY: 0,
        arrowsOffsetX: 330,
        dialogToBoxOffsetY: 0,
        arrowsColor: Color(0xFF303546),
        child: Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: SpecialBatchOperationDialog(
                context: context, totalList: totalList
                //   faceUserInfoBean:faceUserInfoBean
                ),
          ),
        ));
  }

  @override
  _SpecialBatchOperationDialogState createState() =>
      _SpecialBatchOperationDialogState();
}

class _SpecialBatchOperationDialogState extends BasePagerState<
    CompanyDetailByTypeListItemBean, SpecialBatchOperationDialog> {
  SpecialAttentionStaffListViewModel _viewModel;
  @override
  void initState() {
    super.initState();
    _viewModel = SpecialAttentionStaffListViewModel(this, null);
  }

  @override
  Widget build(BuildContext context) {
    return bubbleWidget(context);
  }

  Widget bubbleWidget(BuildContext context) {
    bool studentOrStaff = true;
    bool isShowStudentOrStaff = true;
    List<String> listGroupName = new List<String>();
    String groupNameStrs = "";
    if (widget?.totalList?.length != 0) {
      for (int i = 0; i < widget?.totalList?.length; i++) {
        if(widget?.totalList[0]?.groupName!=widget?.totalList[i]?.groupName){
          isShowStudentOrStaff = false;
        }
        listGroupName.add(widget?.totalList[i]?.groupName);
      }
      if(isShowStudentOrStaff){
        groupNameStrs = VgStringUtils.getSplitStr(listGroupName, symbol: ",");
        if(groupNameStrs.contains("员工")|| groupNameStrs.contains("教工")){
          studentOrStaff = true;
        }
        if(groupNameStrs.contains("学员")|| groupNameStrs.contains("学生")){
          studentOrStaff = false;
        }
        if(groupNameStrs.contains("家长")){
          isShowStudentOrStaff = false;
        }
        // if(!AppOverallDistinguish.comefromAiOrWeStudy()){
        //   isShowStudentOrStaff = false;
        // }
      }

    }else{
      isShowStudentOrStaff = false;
    }

    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        width: 195,
        padding: const EdgeInsets.symmetric(vertical: 10),
        color: Color(0xFF303546),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () async {
                // Navigator.push(context, new MaterialPageRoute(builder: (context){
                //   return new CompanySpecialAttentionListPage();
                // }));
                // if (StringUtils.isNotEmpty(infoBean?.roleid)) {
                //   if(infoBean?.roleid=="99"){//超级管理员
                //     return;
                //   }
                // }
                List<String> listFuid = new List<String>();
                String fuidStrs = "";
                bool result =
                    await CommonConfirmCancelDialog.navigatorPushDialog(context,
                        title: "提示",
                        content: "确定设为特别关注吗？",
                        cancelText: "取消",
                        confirmText: "确定",
                        confirmBgColor: ThemeRepository.getInstance()
                            .getMinorRedColor_F95355());
                if (widget?.totalList?.length == 0 && result == true) {
                  VgToastUtils.toast(context, "请先选择需要关注的人员");
                  return;
                }
                if (result ?? false) {
                  if (widget?.totalList?.length != 0) {
                    for (int i = 0; i < widget?.totalList?.length; i++) {
                      if(widget?.totalList[i]?.special??false){
                        continue;
                      }
                      listFuid.add(widget?.totalList[i]?.fuid);
                    }
                    fuidStrs = VgStringUtils.getSplitStr(listFuid, symbol: ",");
                  }
                  _viewModel?.batchChangeVeryCareStatus(
                      context, fuidStrs, true);
                }
                // _viewModel?.refreshMultiPage();
              },
              child: Container(
                height: 45,
                padding: const EdgeInsets.only(
                  left: 28,
                ),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "images/batch_attention.png",
                      height: 20,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text("设为特别关注",
                        style:
                            TextStyle(color: Color(0xFFD0E0F7), fontSize: 14)),
                  ],
                ),
              ),
            ),
            // Spacer(),
            if(isShowStudentOrStaff&&AppOverallDistinguish.comefromAiOrWeStudy())
            studentOrStaff ? _setBatchOperationPosition(studentOrStaff) : _setBatchClassCourse(studentOrStaff),

            Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () async {
                  // Navigator.push(context, new MaterialPageRoute(builder: (context){
                  //   return new CompanySpecialAttentionListPage();
                  // }));
                  // if (StringUtils.isNotEmpty(infoBean?.roleid)) {
                  //   if(infoBean?.roleid=="99"){//超级管理员
                  //     return;
                  //   }
                  // }
                  List<String> listFuid = new List<String>();
                  List<String> listRoleid = new List<String>();
                  List<String> listUserid = new List<String>();
                  String fuidStrs = "";
                  String roleidStrs = "";
                  String useridStrs = "";
                  bool result =
                      await CommonConfirmCancelDialog.navigatorPushDialog(
                          context,
                          title: "提示",
                          content: "确定删除选择的用户吗？",
                          cancelText: "取消",
                          confirmText: "删除",
                          confirmBgColor: ThemeRepository.getInstance()
                              .getMinorRedColor_F95355());
                  if (widget?.totalList?.length == 0 && result == true) {
                    VgToastUtils.toast(context, "请先选择需要删除的人员");
                    return;
                  }
                  if (result ?? false) {
                    if (widget?.totalList != null) {
                      for (int i = 0; i < widget?.totalList?.length; i++) {
                        if (widget?.totalList[i]?.roleid == "99") {
                          continue;
                        }
                        listFuid.add(widget?.totalList[i]?.fuid);
                        listRoleid.add(widget?.totalList[i]?.roleid);
                        if(widget?.totalList[i]?.userid==null){
                          listUserid.add("-1");
                        }else{
                          listUserid.add(widget?.totalList[i]?.userid);
                        }

                      }
                      fuidStrs =
                          VgStringUtils.getSplitStr(listFuid, symbol: ",");
                      roleidStrs =
                          VgStringUtils.getSplitStr(listRoleid, symbol: ",");
                      useridStrs =
                          VgStringUtils.getSplitStr(listUserid, symbol: ",");
                    }
                    _viewModel?.batchDeleteUserHttp(context,
                        fuid: fuidStrs ?? "",
                        roleid: roleidStrs ?? "",
                        userid: useridStrs ?? "");
                  }

                  // setState(() { });
                },
                child: Container(
                  height: 40,
                  padding: const EdgeInsets.only(
                    left: 28,
                  ),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        "images/batch_delect.png",
                        height: 20,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text("删除",
                          style: TextStyle(
                              color: Color(0xFFD0E0F7), fontSize: 14)),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _setBatchOperationPosition(bool studentOrStaff) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        if (widget?.totalList?.length == 0) {
          VgToastUtils.toast(context, "请先选择需要设置的人员");
          return;
        }
        List<String> listFuid = new List<String>();
        String fuidStrs = "";
        if (widget?.totalList?.length != 0) {
          for (int i = 0; i < widget?.totalList?.length; i++) {
            listFuid.add(widget?.totalList[i]?.fuid);
          }
          fuidStrs = VgStringUtils.getSplitStr(listFuid, symbol: ",");
        }
        DepartmentWidget.navigatorPush(context, "").then((value){
          _viewModel?.batchAppUpdateChooseInfo(
              context, fuidStrs,"", "","",value?.value,"");
        });
          // Navigator.push(context, new MaterialPageRoute(builder: (context) {
          //   return new BatchOperationPositionWidget(isEmptyHide:studentOrStaff,totalList: widget?.totalList,);
          // }));
        setState(() {});
      },
      child: Container(
        height: 45,
        padding: const EdgeInsets.only(
          left: 28,
        ),
        child: Row(
          children: <Widget>[
            Image.asset(
              "images/batch_setup.png",
              height: 20,
            ),
            SizedBox(
              width: 4,
            ),
            Text("设置部门",
                style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14)),
          ],
        ),
      ),
    );
  }

  Widget _setBatchClassCourse(bool studentOrStaff) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        if (widget?.totalList?.length == 0) {
          VgToastUtils.toast(context, "请先选择需要设置的人员");
          return;
        }

        FaceUserInfoBean faceUserInfoBean = FaceUserInfoBean();
        List<String> listFuid = new List<String>();
        String fuidStrs = "";
        if (widget?.totalList?.length != 0) {
          for (int i = 0; i < widget?.totalList?.length; i++) {
            listFuid.add(widget?.totalList[i]?.fuid);
          }
          fuidStrs = VgStringUtils.getSplitStr(listFuid, symbol: ",");
          faceUserInfoBean?.fuid = fuidStrs;
        }

        PersonalClassWidget.navigatorPush(context, faceUserInfoBean,"SpecialBatchOperationDialog").then((value){
          RouterUtils.popUntil(context, "CompanyDetailPage");
          setState(() { });
        });

      },
      child: Container(
        height: 45,
        padding: const EdgeInsets.only(
          left: 28,
        ),
        child: Row(
          children: <Widget>[
            Image.asset(
              "images/batch_setup.png",
              height: 20,
            ),
            SizedBox(
              width: 4,
            ),
            Text("设置班级",
                style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14)),
          ],
        ),
      ),
    );
  }
}

import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/company_detail_by_type_list_widget.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 企业主页搜索展示列表
///
/// @author: zengxiangxi
/// @createTime: 2/3/21 2:51 PM
/// @specialDemand:
class CompanyDetailSearchListWidget extends StatelessWidget {
  final ValueNotifier<String> searchPageValueNotifier;

  const CompanyDetailSearchListWidget({
    Key key,
    this.searchPageValueNotifier,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: searchPageValueNotifier,
      builder: (BuildContext context, String searchStr, Widget child) {
        return StringUtils.isNotEmpty(searchStr?.trim())
            ? Container(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                child: _toMainColumnWidget(searchStr?.trim()),
              )
            : Container();
      },
    );
  }

  Widget _toMainColumnWidget(String searchStr) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 15, bottom: 6),
          child: CommonSearchBarWidget(
              hintText: "搜索姓名",
              initContent: searchStr,
              onChanged: (String searchStr) {
                searchPageValueNotifier.value = searchStr?.trim();
              },
              onSubmitted: (String searchStr) {
                searchPageValueNotifier.value = searchStr?.trim();
              }),
        ),
        Expanded(
          child: _toListWidget(searchStr),
        ),
      ],
    );
  }

  Widget _toListWidget(String searchStr) {
    return CompanyDetailByTypeListWidget(
      groupInfoBean: GroupPersonCntBean()..visitor = "01",
      searchPageValueNotifier: searchPageValueNotifier,
    );
  }


}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/widgets.dart';

/// 企业详情-标题分割线
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 10:33 AM 
/// @specialDemand:
class CompanyDetailTitleSplitWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          Expanded(child: _toSplitLineWidget(),),
           Padding(
             padding: const EdgeInsets.symmetric(horizontal: 13),
             child: Text(
                       "已注册人脸并激活",
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                       style: TextStyle(
                         color: VgColors.INPUT_BG_COLOR,
                           fontSize: 12,
                           ),
                     ),
           ),
          Expanded(child: _toSplitLineWidget(),),
        ],
      ),
    );
  }

  Widget _toSplitLineWidget(){
    return Container(
      height: 0.5,
      color: Color(0xFF303546),
    );
  }
}

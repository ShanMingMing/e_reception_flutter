import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_web_page/common_web_page.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_const.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_upgrade/vg_net_delegate.dart';
import 'package:e_reception_flutter/vg_widgets/vg_upgrade/vg_windows_delegate.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:package_info/package_info.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 企业详情-设置弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 2:12 PM
/// @specialDemand:
class CompanyDetailSettingDialog extends StatelessWidget {
  ///版本号
  final String versionName;

  const CompanyDetailSettingDialog({Key key, this.versionName})
      : super(key: key);

  ///跳转方法
  ///返回 DateTime
  static Future<dynamic> navigatorPushDialog(
    BuildContext context,
  ) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionName = packageInfo?.version;
    return VgDialogUtils.showCommonDialog(
        context: context,
        child: CompanyDetailSettingDialog(
          versionName: versionName,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Align(
          alignment: Alignment.bottomRight,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {},
            child: Container(
              width: 160,
              margin: EdgeInsets.only(
                  right: 15, bottom: 15 + ScreenUtils.getBottomBarH(context)),
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                borderRadius: BorderRadius.circular(12),
              ),
              child: _toMainColumnWidget(context),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _SettingItemWidget(
          text: "版本号",
          textColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          rightWidget: Text(
            versionName ?? "-",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
            ),
          ),
          onTap: () {
            // VgToastUtils.toast(context, "敬请期待");
            UpgradeRepository.getInstance(
                VgNetDelegate.of(true), VgWindowsDelegate.of(),onUpgradeErrorChanged: (UpgradeError error){
              if(StringUtils.isEmpty(error?.message)){
                if(!(error.message.contains("获取升级详情失败"))){
                  VgToastUtils.toast(AppMain.context, error?.message);
                }
              }
            })
              .httpCheckUpgrade();
          },
        ),
        _SettingItemWidget(
          text: "隐私协议",
          textColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          rightWidget: Image.asset(
            "images/go_ico.png",
            width: 5,
            color: VgColors.INPUT_BG_COLOR,
          ),
          onTap: () {
            CommonWebPage.navigatorPush(
              context,
              PRIVACY_H5,
              "隐私协议",
            );
          },
        ),
        _SettingItemWidget(
          text: "退出登录",
          textColor: ThemeRepository.getInstance().getMinorRedColor_F95355(),
          onTap: () async {
            bool result = await CommonConfirmCancelDialog.navigatorPushDialog(
                context,
                title: "提示",
                content: "是否退出登录？");
            if (result == null || result == false) {
              return;
            }
            Future.delayed(Duration(milliseconds: 0), () async {
              await UserRepository.getInstance().clearUserInfo();
              RouterUtils.popUntil(AppMain.context, AppMain.ROUTER,
                  rootNavigator: false);
              VgToastUtils.toast(AppMain.context, "退出成功");
            });
          },
        ),
      ],
    );
  }
}

/// 设置项
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 2:16 PM
/// @specialDemand:
class _SettingItemWidget extends StatelessWidget {
  final String text;

  final Color textColor;

  final Widget rightWidget;

  final VoidCallback onTap;

  const _SettingItemWidget(
      {Key key,
      @required this.text,
      @required this.textColor,
      this.rightWidget,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
        if (onTap != null) {
          onTap();
        }
      },
      child: DefaultTextStyle(
        style: TextStyle(height: 1.2),
        child: Container(
          height: 45,
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  text ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: textColor,
                    fontSize: 14,
                  ),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              rightWidget ?? Container()
            ],
          ),
        ),
      ),
    );
  }
}

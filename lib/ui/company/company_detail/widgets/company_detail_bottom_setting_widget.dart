import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'company_detail_setting_button_widget.dart';

/// 企业详情-底部设置
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 1:49 PM
/// @specialDemand:
class CompanyDetailBottomSettingWidget extends StatelessWidget {

  final UserDataBean user;

  const CompanyDetailBottomSettingWidget({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      padding: EdgeInsets.only(bottom: ScreenUtils.getBottomBarH(context)),
      child: Container(
        height: 52,
        child: _toMainRowWidget(),
      ),
    );
  }

  Widget _toMainRowWidget() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 15,
        ),
        ClipOval(
          child: Container(
            height: 36,
            width: 36,
            child: VgCacheNetWorkImage(
            showOriginEmptyStr(user?.comUser?.putpicurl) ?? (user?.comUser?.napicurl ?? ""),
              fit: BoxFit.cover,
              defaultErrorType: ImageErrorType.head,
              defaultPlaceType: ImagePlaceType.head,
              imageQualityType: ImageQualityType.middleDown,
            ),
          ),
        ),
        SizedBox(
          width: 8,
        ),
        Expanded(
          child: Container(height: 36, child: _toColumnWidget()),
        ),
        CompanyDetailSettingButtonWidget(),
      ],
    );
  }

  Widget _toColumnWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          user?.comUser?.name ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
            fontSize: 15,
          ),
        ),
        Text(
          user?.comUser?.phone ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white.withOpacity(0.7), fontSize: 11, height: 1.2),
        ),
      ],
    );
  }
}

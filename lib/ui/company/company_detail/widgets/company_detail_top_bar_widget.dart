import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/company_add_user_edit_info_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_detail_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/ui/company/company_detail_search/company_detail_search_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail_search/company_search_detail_list_widget.dart';
import 'package:e_reception_flutter/ui/company/company_switch/bean/company_switch_list_bean.dart';
import 'package:e_reception_flutter/ui/company/company_switch/company_switch_page.dart';
import 'package:e_reception_flutter/ui/company/company_switch/company_switch_view_model.dart';
import 'package:e_reception_flutter/ui/login/login_bean/login_search_organization_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

import 'company_detail_menu_dialog.dart';

/// 企业详情-topbar
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 4:20 PM
/// @specialDemand:
class CompanyDetailTopBarWidget extends StatefulWidget {
  final CompanyInfoBean companyInfoBean;

  final VoidCallback onRefresh;

  final List<OrgInfoListBean> orgSwitchInfoList;

  final CompanySwitchListBean companySwitchListBean;

  const CompanyDetailTopBarWidget(
      {Key key, this.companyInfoBean, this.onRefresh, this.orgSwitchInfoList, this.companySwitchListBean})
      : super(key: key);

  @override
  _CompanyDetailTopBarWidgetState createState() =>
      _CompanyDetailTopBarWidgetState();
}

class _CompanyDetailTopBarWidgetState extends BaseState<CompanyDetailTopBarWidget> {

  // CompanySwitchViewModel _viewModel;


  @override
  void initState() {
    super.initState();
    // _viewModel = CompanySwitchViewModel(this);
    //选择需要导入的机构
    // _viewModel?.searchOrganization(context,UserRepository.getInstance().getPhone());
    // setState(() { });
  }

  @override
  void dispose() {
    // isHideTopBarTitle?.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return _toTopBarWidget();
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      centerWidget: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          // if(widget?.companySwitchListBean?.data?.userComList==null){
          //   return;
          // }
          if ((UserRepository.getInstance().companyList?.length ?? 0) <= 1 && (widget?.orgSwitchInfoList?.length??0)<=1) {
            return;
          }
          bool result = await CompanySwitchPage.navigatorPush(context,orgInfoList: widget?.orgSwitchInfoList,companySwitchListBean:widget?.companySwitchListBean);
          if (result != null && result) {
            //切换企业成功
            VgEventBus.global.send(ChangeCompanyEvent());
            widget.onRefresh();
          }
          setState(() { });
        },
        child: Container(
          width: ScreenUtils.screenW(context) / 1.2,
          alignment: Alignment.center,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: 18,
              ),
              ValueListenableBuilder(
                valueListenable: CompanyDetailPageState.of(context)
                    ?.singleCompanyTitleValueNotifier,
                builder: (BuildContext context, String singleCompanyTitle,
                    Widget child) {
                  return Container(
                    // width: ScreenUtils.screenW(context) / 2,
                    child: Text(VgStringUtils.subStringAndAppendSymbol(
                        _strCompanyName(singleCompanyTitle), 14,
                        symbol: "..."),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xFFD0E0F7),
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          height: 1.2),
                    ),
                  );
                },
              ),
              Opacity(
                opacity:
                    ((UserRepository.getInstance().companyList?.length ?? 0) <=
                            1 && (widget?.orgSwitchInfoList?.length??0)<=1)
                        ? 0
                        : 1,
                child: Container(
                  width: 15,
                  height: 15,
                  margin: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withOpacity(0.1)),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    size: 13,
                    color:
                        ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      rightPadding: 0,
      rightWidget: _toTopBarRightWidget(),
    );
  }

  String _strCompanyName(String singleCompanyTitle){
    return (UserRepository.getInstance().companyList?.length ?? 0) <= 1 && (widget?.orgSwitchInfoList?.length??0)<=1
        ? (singleCompanyTitle ?? "基本设置")
        : "${widget?.companyInfoBean?.companynick ?? "-"}";
  }

  Widget _toTopBarRightWidget() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        //企业列表人员信息搜索界面搜索按钮
        CompanyDetailSearchPage.navigatorPush(context);
      },
      child: Container(
          height: 44,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Center(
            child: Image.asset(
              "images/common_search_ico.png",
              color: Colors.white,
              width: 20,
            ),
          )),
    );
  }
}

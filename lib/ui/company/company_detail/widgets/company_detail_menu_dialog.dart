import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_switch/company_switch_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../enterprise_information_page.dart';

/// 企业详情-功能菜单
///
/// @author: zengxiangxi
/// @createTime: 2/2/21 4:05 PM
/// @specialDemand:
class CompanyDetailMenuDialog extends StatefulWidget {
  ///跳转方法
  ///返回
  static Future<dynamic> navigatorPushDialog(
    BuildContext context,
  ) {
    return VgDialogUtils.showCommonDialog(
        context: context, child: CompanyDetailMenuDialog());
  }

  @override
  _CompanyDetailMenuDialogState createState() =>
      _CompanyDetailMenuDialogState();
}

class _CompanyDetailMenuDialogState extends State<CompanyDetailMenuDialog> {
  List<UserComListBean> _companyList;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _companyList = UserRepository.getInstance().companyList;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          alignment: Alignment.topCenter,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {},
            child: Container(
              width: 160,
              decoration: BoxDecoration(
                  color: Color(0xFF21263C),
                  borderRadius: BorderRadius.circular(8)),
              margin:
                  EdgeInsets.only(top: ScreenUtils.getStatusBarH(context) + 44),
              child: _toListWidget(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toListWidget() {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.symmetric(vertical: 8),
      children: <Widget>[
        _toListItemWidget("企业资料", () {
          RouterUtils.pop(context);
          EnterpriseInformationPage.navigatorPush(context);
          // VgToastUtils.toast(context, "点击企业资料...");
        }),
        Offstage(
          offstage: _companyList == null ||
              _companyList.isEmpty == null ||
              _companyList.length <= 1,
          child: _toListItemWidget("切换企业", () {
            RouterUtils.pop(context);
            CompanySwitchPage.navigatorPush(context);
          }),
        ),
      ],
    );
  }

  Widget _toListItemWidget(String text, VoidCallback onTap) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => onTap?.call(),
      child: Container(
        height: 50,
        child: Center(
          child: Text(
            text ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }
}

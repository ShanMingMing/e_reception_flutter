import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_string_util_lib.dart';

const Color DEFAULT_COLOR = Color(0xffd0e0f7);

///拼接头像组件
class StitchingAvatarWidget extends StatelessWidget {
  ///尺寸
  final double size;

  ///背景色
  final Color bgColor;

  ///间距
  final double padding;

  final List<String> avatarUrlList;

  final Widget emptyWidget;

  const StitchingAvatarWidget(
      {Key key,
      this.size = 36,
      this.bgColor = DEFAULT_COLOR,
      this.padding = 2,
      this.avatarUrlList,
      this.emptyWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Spacer(),
          Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                border: Border.all(color: bgColor, width: 0),
                color: bgColor),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Positioned(
                  left: padding,
                  top: padding,
                  child: _childAvatar(0),
                ),
                Positioned(
                  right: padding,
                  top: padding,
                  child: _childAvatar(1),
                ),
                Positioned(
                  left: padding,
                  bottom: padding,
                  child: _childAvatar(2),
                ),
                Positioned(
                  right: padding,
                  bottom: padding,
                  child: _childAvatar(3),
                ),
                if (avatarUrlList == null || avatarUrlList.length == 0)
                  emptyWidget ?? SizedBox()
              ],
            ),
          ),
          Spacer()
        ],
      ),
    );
  }

  _childAvatar(int index) {
    if (avatarUrlList == null || avatarUrlList.length == 0) {
      return SizedBox();
    }
    if (index < avatarUrlList.length) {
      double childSize = (size - 3 * padding) / 2;
      String picUrl = avatarUrlList[index] ?? '';
      return Container(
        width: childSize,
        height: childSize,
        child: VgCacheNetWorkImage(
          picUrl,
          imageQualityType:ImageQualityType.lowest,
          // errorWidget: Container(color: bgColor,),
          defaultErrorType: ImageErrorType.head,
          placeWidget: Container(color: bgColor,),
        ),
      );
    } else {
      return SizedBox();
    }
  }
}

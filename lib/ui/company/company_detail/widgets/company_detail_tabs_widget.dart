import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_detail_page.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'company_detail_filter_button_widget.dart';

/// 企业详情-tab栏
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 3:31 PM
/// @specialDemand:
class CompanyDetailTabsWidget extends StatelessWidget {
  final TabController tabController;

  final List<GroupPersonCntBean> tabsList;

  const CompanyDetailTabsWidget({Key key, this.tabController, this.tabsList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      margin: const EdgeInsets.only(bottom: 5),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Expanded(
            child: Theme(
                data: ThemeData(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent),
                child: _toTabBarWidget()),
          ),
          if(AppOverallDistinguish.comefromAiOrWeStudy())
          StreamBuilder(
            stream: CompanyDetailPageState.of(context).tabsChangeStream,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              return CompanyDetailFilterButtonWidget(tabsList:tabsList, selectIndex: tabController.index,);
            },
          )
        ],
      ),
    );
  }

  Widget _toTabBarWidget() {
    return TabBar(
      controller: tabController,
      isScrollable: true,
      indicatorSize: TabBarIndicatorSize.label,
      unselectedLabelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 15,
          height: 1.2),
      labelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 15,
          fontWeight: FontWeight.w600,
          height: 1.2),
      labelPadding: EdgeInsets.only(left: 15, right: 0),
      indicatorColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
      indicatorPadding: EdgeInsets.only(left: 10, right: 24),
      tabs: List.generate(tabsList?.length ?? 0, (index) {
        return Offstage(
          offstage: _isSpecialAttention(index),
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(tabsList?.elementAt(index)?.groupName ?? ""),
                SizedBox(
                  width: 3,
                ),
                Container(
                  width: 24,
                  child: Text(
                    "${tabsList?.elementAt(index)?.cnt ?? 0}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  bool _isSpecialAttention(int index){
    if(tabsList[index]?.groupName == "特别关注" && tabsList?.elementAt(index)?.cnt == 0){
      // tabsList?.remove(tabsList[index]);
      return true;
    }else return false;
  }

  bool _isShowFilterButtonWidget() {
    final int currentPage = tabController.index;
    if (tabsList == null || tabsList.isEmpty) {
      return false;
    }
    if (currentPage == null ||
        currentPage < 0 ||
        currentPage >= tabsList.length) {
      return false;
    }
    final GroupPersonCntBean groupPersonCntBean =
        tabsList.elementAt(currentPage);
    if (groupPersonCntBean == null) {
      return false;
    }
    return groupPersonCntBean.groupid == null ||
        groupPersonCntBean.groupid.isEmpty;
  }
}

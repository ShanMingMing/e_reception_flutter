import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_add_save_user_dialog/common_ave_user_tips_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/company_add_user_edit_info_page.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/create_user_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_sp_lib.dart';

/// 公司详情添加按钮组件
///
/// @author: zengxiangxi
/// @createTime: 3/22/21 6:02 PM
/// @specialDemand:
class CompanyDetailAddFlatButtonWidget extends StatelessWidget {
  final VoidCallback onRefresh;
  //当前角色 00 企业 01 机构
  final String role;

  final String groupid;

  const CompanyDetailAddFlatButtonWidget({Key key, this.onRefresh, this.role, this.groupid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: true,
        width: 140,
        height: 40,
        margin: const EdgeInsets.symmetric(horizontal: 25),
        unSelectBgColor: Color(0xFF3A3F50),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 15,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontFamily: "PingFang"
                // fontWeight: FontWeight.w600
        ),
        text: UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition()?"用户/人脸照片":"添加用户",
        textLeftSelectedWidget: Padding(
          padding: const EdgeInsets.only(bottom: 1),
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
        onTap: (){
          //保存此次进入的类型
          SharePreferenceUtil.putString(KEY_LAST_ADD_USER_TYPE +
              UserRepository.getInstance().getCacheKeySuffix(),groupid);
          CreateUserPage.navigatorPush(context,role:role)..then((value){
            if(value ?? false) {
              onRefresh?.call();
            }
          });
        },
      ),
    );
  }

}

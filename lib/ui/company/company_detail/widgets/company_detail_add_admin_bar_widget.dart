import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 企业主页-添加管理员学员成员栏

class TitleNumberAndAddBtnBarWidget extends StatefulWidget {
  final String title;
  final int cnt;
  final VoidCallback onClick;
  final Widget secondWidget;
  final int type;

  TitleNumberAndAddBtnBarWidget(this.title, this.cnt, this.onClick,{this.secondWidget, this.type,});



  @override
  State<StatefulWidget> createState() {
     return TitleNumberAndAddBtnBarState();
  }
}
class TitleNumberAndAddBtnBarState extends State<TitleNumberAndAddBtnBarWidget>{
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.centerLeft,
      child: Row(
        children: <Widget>[
          Text(
            "${widget.title ?? ''}·${widget.cnt ?? 0}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextBFC2CC_BFC2CC(),
                fontSize: 15,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          widget.secondWidget??SizedBox(),
          Spacer(),
          if(AppOverallDistinguish.comefromAiOrWeStudy() || widget?.type == 0)
          GestureDetector(
            onTap: () {
             widget. onClick();
            },
            child: Container(
              height: 24,
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                  color:
                  ThemeRepository.getInstance().getPrimaryColor_1890FF()),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add,
                    size: 12,
                    color: Colors.white,
                  ),
                  Text(
                    "添加",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.white, fontSize: 12, height: 1.2),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

}

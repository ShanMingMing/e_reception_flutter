import 'dart:async';

import 'package:e_reception_flutter/common_widgets/common_with_up_or_down_arrows_dialog/common_with_up_or_down_arrows_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_detail_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'company_attention_widget/company_special_attention_list_widget.dart';

/// 公司详情过滤按钮组件
///
/// @author: zengxiangxi
/// @createTime: 3/24/21 1:51 PM
/// @specialDemand:
class CompanyDetailFilterButtonWidget extends StatelessWidget {
  final List<GroupPersonCntBean> tabsList;
  final int selectIndex;
  CompanyDetailFilterButtonWidget({Key key, this.tabsList, this.selectIndex}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // return _toFilterButtonWidget(context);
    return _imagesSpecialAttention(context);
  }

  Widget _toFilterButtonWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        CompanyDetailFilterNotification filterNotification =
            CompanyDetailPageState.of(context).filterNotification;
        CommonWithUpOrDownArrowsDialog.navigatorPushForDialog(context,
            dialogMaxHeight: 110,
            arrowsSize: Size(11, 6),
            safeY: 0,
            arrowsOffsetX: 20,
            arrowsColor: Color(0xFF303546),
            child: Align(
              alignment: Alignment.centerRight,
              child: _CompanyDetailFilterButtonMenuDialog(
                parentContext: context,
                filterNotification: filterNotification,
              ),
            ));
      },
      child: ValueListenableBuilder(
        valueListenable: CompanyDetailPageState.of(context)
            .filterNotification
            ?.companyDetailFilterValueNotifier,
        builder:
            (BuildContext context, CompanyDetailFilterType type, Widget child) {
          return
              Row(
                children: <Widget>[
                  // _imagesSpecialAttention(context),
                  SizedBox(width: 15,),
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: Image.asset(
                      type != null
                          ? "images/tab_menu_ico.png"
                          : "images/xhdpi_shaixuan_jrsl.png",
                      width: 18,
                      gaplessPlayback: true,
                    ),
                  ),
                ],
              );

        },
      ),
    );
  }

  Widget _imagesSpecialAttention(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        Navigator.push(context, new MaterialPageRoute(builder: (context) {
          return new CompanySpecialAttentionListPage(tabsSelectedList:tabsList, selectIndex: selectIndex,);
        }));
      },
      child: Transform.scale(
        scale: 1.5,
        child: Container(

          padding: const EdgeInsets.only(right: 5),
          child: Image.asset("images/piliang.png",
            width: 55,
            height: 50,
            gaplessPlayback: true,
          ),
        ),
      ),
    );
  }
}

/// 公司详情过滤按钮菜单弹窗
///
/// @author: zengxiangxi
/// @createTime: 3/24/21 1:55 PM
/// @specialDemand:
class _CompanyDetailFilterButtonMenuDialog extends StatelessWidget {
  final BuildContext parentContext;
  CompanyDetailFilterType _filterType;
  final CompanyDetailFilterNotification filterNotification;

  _CompanyDetailFilterButtonMenuDialog(
      {Key key, @required this.parentContext, this.filterNotification})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _filterType = filterNotification.companyDetailFilterValueNotifier.value;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Color(0xFF303546),
        borderRadius: BorderRadius.circular(12),
      ),
      height: 110,
      child: ValueListenableBuilder(
          valueListenable: filterNotification?.statisticsValueNotifier,
          builder: (context, StatisticsBean statisticsBean, Widget child) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Spacer(),
                _toListItemWidget(
                    context,
                    "管理员",
                    (statisticsBean?.adminCnt ?? 0),
                    _filterType == CompanyDetailFilterType.admin, () {
                  filterNotification.companyDetailFilterValueNotifier.value =
                      _filterType == CompanyDetailFilterType.admin
                          ? null
                          : CompanyDetailFilterType.admin;
                }),
                _toListItemWidget(
                    context,
                    "未激活",
                    (statisticsBean?.unactivateCnt ?? 0),
                    _filterType == CompanyDetailFilterType.inactivated, () {
                  filterNotification.companyDetailFilterValueNotifier.value =
                      _filterType == CompanyDetailFilterType.inactivated
                          ? null
                          : CompanyDetailFilterType.inactivated;
                }),
                Spacer(),
              ],
            );
          }),
    );
  }

  Widget _toListItemWidget(BuildContext context, String title, int personcnt,
      bool isSelected, VoidCallback onTap) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
        onTap?.call();
      },
      child: Container(
        height: 45,
        width: 125,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: 20,
              ),
              Container(
                width: 52,
                alignment: Alignment.centerLeft,
                child: Text(
                  title ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: isSelected
                        ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                        : Color(0xFFD0E0F7),
                    fontSize: 14,
                  ),
                ),
              ),
              // Expanded(
              //   child: Text(
              //     "${personcnt ?? 0}人",
              //     maxLines: 1,
              //     overflow: TextOverflow.ellipsis,
              //     style: TextStyle(
              //       color: isSelected
              //           ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
              //           : Color(0xFFD0E0F7),
              //       fontSize: 11,
              //     ),
              //   ),
              // ),
              SizedBox(
                width: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum CompanyDetailFilterType {
  //管理员
  admin,
  //未激活
  inactivated,
}

extension CompanyDetailFilterTypeExtension on CompanyDetailFilterType {
  getParamsStr() {
    if (this == null) {
      return null;
    }

    ///"00"管理员，"01"激活，"02"未激活
    switch (this) {
      case CompanyDetailFilterType.admin:
        return "00";
      case CompanyDetailFilterType.inactivated:
        return "02";
    }
    return null;
  }
}

///双向过滤通知类
class CompanyDetailFilterNotification {
  ValueNotifier<CompanyDetailFilterType> companyDetailFilterValueNotifier;

  ValueNotifier<StatisticsBean> statisticsValueNotifier;

  CompanyDetailFilterNotification() {
    companyDetailFilterValueNotifier = ValueNotifier(null);
    statisticsValueNotifier = ValueNotifier(null);
  }

  dispose() {
    companyDetailFilterValueNotifier?.dispose();
    statisticsValueNotifier?.dispose();
  }
}

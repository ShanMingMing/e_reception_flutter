// import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/check_in_record_by_type_list_widget.dart';
// import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/company_detail_by_type_list_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
//
// /// 企业详情-tab和page
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/11/21 10:40 AM
// /// @specialDemand:
// class CompanyDetailTabsAndPagesWidget extends StatefulWidget {
//   @override
//   _CompanyDetailTabsAndPagesWidgetState createState() =>
//       _CompanyDetailTabsAndPagesWidgetState();
// }
//
// class _CompanyDetailTabsAndPagesWidgetState
//     extends State<CompanyDetailTabsAndPagesWidget>
//     with TickerProviderStateMixin {
//   TabController _tabController;
//
//   List<String> _tabsList;
//
//   @override
//   void initState() {
//     super.initState();
//     _tabsList = ["全部", "员工", "学员", "家长", "测一", "测二", "访客"];
//
//     _tabController = TabController(length: _tabsList?.length ?? 0, vsync: this);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return _toMainColumnWidget();
//   }
//
//   Widget _toMainColumnWidget() {
//     return ScrollConfiguration(
//       behavior: MyBehavior(),
//       child: Column(
//         children: <Widget>[
//           Container(
//             height: 30,
//             child: Theme(
//                 data: ThemeData(
//                     highlightColor: Colors.transparent,
//                     splashColor: Colors.transparent),
//                 child: _toTabBarWidget()),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(top: 15,bottom: 6),
//             child: CommonSearchBarWidget(hintText: "搜索名字",),
//           ),
//           Expanded(
//             child: _toPagesWidget(),
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget _toTabBarWidget() {
//     return TabBar(
//       controller: _tabController,
//       isScrollable: true,
//       indicatorSize: TabBarIndicatorSize.label,
//       unselectedLabelStyle: TextStyle(
//           color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//           fontSize: 15,
//           height: 1.2),
//       labelStyle: TextStyle(
//           color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//           fontSize: 15,
//           fontWeight: FontWeight.w600,
//           height: 1.2),
//       indicatorColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//       indicatorPadding: EdgeInsets.only(left: 4, right: 20),
//       tabs: List.generate(_tabsList?.length ?? 0, (index) {
//         return Row(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Text(_tabsList?.elementAt(index)),
//             SizedBox(
//               width: 3,
//             ),
//             Text(
//               "12",
//               maxLines: 1,
//               overflow: TextOverflow.ellipsis,
//               style: TextStyle(
//                 fontSize: 12,
//               ),
//             )
//           ],
//         );
//       }),
//     );
//   }
//
//   Widget _toPagesWidget() {
//     return TabBarView(
//       controller: _tabController,
//       physics: BouncingScrollPhysics(),
//       children: List.generate(_tabsList?.length ?? 0, (index) {
//         return CompanyDetailByTypeListWidget();
//       }),
//     );
//   }
// }

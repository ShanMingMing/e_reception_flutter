import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef OnClick = void Function(String text);

class CompanyMorePopWindow extends StatefulWidget {
  final TopCnt topCnt;
  final OnClick onClick;

  const CompanyMorePopWindow({Key key, this.topCnt, this.onClick})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CompanyMorePopWindowState();
  }
}

class CompanyMorePopWindowState extends State<CompanyMorePopWindow> {
  @override
  Widget build(BuildContext context) {
    List<Widget> widgetArray =
        UserRepository.getInstance().userData.companyInfo.isCompanyLike()
            ? [
                //企业顺序
                _cntTextWidget("项目", widget.topCnt.projectCnt),
                _cntTextWidget("城市", widget.topCnt.cityCnt),
              ]
            : [
                //机构顺序
                _cntTextWidget("部门", widget.topCnt.departmentCnt),
                _cntTextWidget("项目", widget.topCnt.projectCnt),
                _cntTextWidget("城市", widget.topCnt.cityCnt),
              ];

    return Column(
      children: <Widget>[
        SizedBox(
          height: 2,
        ),
        Stack(
          children: <Widget>[
            Container(
              width: 100,
              height: 6,
              padding: EdgeInsets.only(right: 27),
              child: Align(
                alignment: Alignment.centerRight,
                child: Image.asset(
                  'images/sanjiao.png',
                  width: 11,
                  height: 6,
                ),
              ),
            ),
          ],
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
              color: Color(0xff303546)),
          child: Column(
            children: widgetArray,
          ),
          width: 100,
        ),
      ],
    );
  }

  ///顶部数字
  _cntTextWidget(String text, int cnt) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        Navigator.pop(context, text);
        widget.onClick(text);
      },
      child: Container(
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              text,
              style:
                  TextStyle(fontSize: 12, color: Color(0xffbfc2cc), height: 1),
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              '$cnt',
              style: TextStyle(fontSize: 12, color: Colors.white, height: 0.9),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/company_detail_by_type_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart'
    as extended;

/// 企业详情-pages
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 3:30 PM
/// @specialDemand:
class CompanyDetailPagesWidget extends StatelessWidget {
  final TabController tabController;

  final List<GroupPersonCntBean> tabsList;

  final String currentCompanyId;

  const CompanyDetailPagesWidget({Key key, this.currentCompanyId, this.tabController, this.tabsList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toPagesWidget();
  }

  Widget _toPagesWidget() {
    return TabBarView(
      controller: tabController,
      physics: BouncingScrollPhysics(),
      children: List.generate(tabsList?.length ?? 0, (index) {
        return extended.NestedScrollViewInnerScrollPositionKeyWidget(
            Key(tabsList?.elementAt(index)?.groupid),
            CompanyDetailByTypeListWidget(
              currentCompanyId: currentCompanyId,
              groupInfoBean: tabsList?.elementAt(index),
            ));
      }),
    );
  }
}

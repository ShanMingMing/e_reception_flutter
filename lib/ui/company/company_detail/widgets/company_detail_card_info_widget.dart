// import 'dart:math';
//
// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/ui/company/company_add_admin_list/company_add_admin_list_page.dart';
// import 'package:e_reception_flutter/ui/company/company_add_user_list/company_add_user_page.dart';
// import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
// import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:vg_base/vg_string_util_lib.dart';
//
// /// 企业主页-卡片详情
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/7/21 5:59 PM
// /// @specialDemand:
// class CompanyDetailCardInfoWidget extends StatelessWidget {
//
//   final CompanyDetailDataBean infoBean;
//
//   const CompanyDetailCardInfoWidget({Key key, this.infoBean}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.symmetric(horizontal: 15),
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           _toMainRowWidget(),
//           _toBottomAdmin(context),
//         ],
//       ),
//     );
//   }
//
//   Widget _toMainRowWidget() {
//     return Container(
//       decoration: BoxDecoration(
//           color: Color(0xFF303546),
//           borderRadius: BorderRadius.vertical(top: Radius.circular(12))),
//       child: Row(
//         children: <Widget>[
//           Expanded(
//             child: _toColumnWidget(),
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget _toColumnWidget() {
//     return Container(
//       margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 12),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[
//           Row(
//             children: <Widget>[
//               Text(
//                 infoBean?.companyInfo?.companynick ?? "",
//                 maxLines: 1,
//                 overflow: TextOverflow.ellipsis,
//                 style: TextStyle(
//                     color:
//                         ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                     fontSize: 24,
//                     fontWeight: FontWeight.w600),
//               ),
//               Spacer(),
//               Container(
//                 width: 40,
//                 height: 20,
//                 child: Center(
//                   child: Image.asset(
//                     "images/company_detail_card_info_update_ico.png",
//                     width: 10,
//                   ),
//                 ),
//               )
//             ],
//           ),
//           // SizedBox(height: 1,),
//         ],
//       ),
//     );
//   }
//
//   Widget _toBottomAdmin(BuildContext context) {
//     return GestureDetector(
//       behavior: HitTestBehavior.translucent,
//       onTap: (){
//         CompanyAddUserPage.navigatorPush(context);
//
//       },
//       child: Container(
//         padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
//         decoration: BoxDecoration(
//             color: Color(0xFF2E394F),
//             borderRadius: BorderRadius.vertical(bottom: Radius.circular(12))),
//         child: Row(
//           children: <Widget>[
//             Text(
//               "共添加用户${infoBean?.InputActivateStatistics?.inputCnt ?? 0}人，其中管理员${infoBean?.InputActivateStatistics?.activateCnt ?? 0}人"
//               "\n"
//               "目前已激活0人，占0%",
//               maxLines: 2,
//               overflow: TextOverflow.ellipsis,
//               style: TextStyle(
//                   color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                   fontSize: 13,
//                   height: 21 / 13),
//             ),
//             Spacer(),
//             _toMultiImagesWidget(infoBean?.InputActivateStatistics?.pics),
//             SizedBox(
//               width: 9,
//             ),
//             Image.asset(
//               "images/go_ico.png",
//               width: 6,
//               color: VgColors.INPUT_BG_COLOR,
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _toMultiImagesWidget(String pics) {
//     List<String> picsList = _getHeadList(pics);
//     if(picsList == null || picsList.isEmpty){
//       return Container();
//     }
//     return Stack(
//       children: List.generate(picsList?.length ?? 0, (index) {
//         return Padding(
//           padding: EdgeInsets.only(left: index * 18.0, top: 0, bottom: 0),
//           child: _toImagesWidget(picsList?.elementAt(index)),
//         );
//       }),
//     );
//   }
//
//   Widget _toImagesWidget(String picurl) {
//     return Container(
//       width: 24,
//       height: 24,
//       decoration: BoxDecoration(
//           shape: BoxShape.circle,
//           border:
//               Border.all(width: 2, color: Color(0xFFD0E0F7).withOpacity(0.2))),
//       child: ClipRRect(
//         borderRadius: BorderRadius.circular(12),
//         child: VgCacheNetWorkImage(
//           picurl ?? "",
//           fit: BoxFit.cover,
//           imageQualityType: ImageQualityType.middleDown,
//           defaultErrorType: ImageErrorType.head,
//           defaultPlaceType: ImagePlaceType.head,
//         ),
//       ),
//     );
//   }
//
//   List<String> _getHeadList(String pics){
//     if(pics == null || pics == ""){
//       return null;
//     }
//     List<String> list = pics.split(",");
//     list.removeWhere((element) => StringUtils.isEmpty(element));
//     return list;
//   }
// }

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"companyInfo":{"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"be97b4557acc49a782c2e52b53bb77a1","createdate":1611712498,"updatetime":1611712498,"updateuid":"be97b4557acc49a782c2e52b53bb77a1"},"groupPersonCnt":[{"groupName":"员工","groupid":"eadc02553f2a40509f05e407125f15e9","cnt":8,"visitor":"01"},{"groupName":"访客","groupid":"35fe36fc21004cfca1bccb7989fea7e1","cnt":0,"visitor":"00"}],"InputActivateStatistics":{"inputCnt":6,"activateCnt":3,"pics":"http://etpic.we17.com/test/20210127105005_2814.jpg,http://etpic.we17.com/test/20210127110434_9981.jpg,"}}
/// extra : null

class CompanyDetailResponseBean {
  bool success;
  String code;
  String msg;
  CompanyDetailDataBean data;
  dynamic extra;

  static CompanyDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyDetailResponseBean companyDetailResponseBeanBean =
    CompanyDetailResponseBean();
    companyDetailResponseBeanBean.success = map['success'];
    companyDetailResponseBeanBean.code = map['code'];
    companyDetailResponseBeanBean.msg = map['msg'];
    companyDetailResponseBeanBean.data =
        CompanyDetailDataBean.fromMap(map['data']);
    companyDetailResponseBeanBean.extra = map['extra'];
    return companyDetailResponseBeanBean;
  }

  Map toJson() =>
      {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// companyInfo : {"companyid":"cd191d29404e4a5dbb6ff2eb8bad48e9","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"be97b4557acc49a782c2e52b53bb77a1","createdate":1611712498,"updatetime":1611712498,"updateuid":"be97b4557acc49a782c2e52b53bb77a1"}
/// groupPersonCnt : [{"groupName":"员工","groupid":"eadc02553f2a40509f05e407125f15e9","cnt":8,"visitor":"01"},{"groupName":"访客","groupid":"35fe36fc21004cfca1bccb7989fea7e1","cnt":0,"visitor":"00"}]
/// InputActivateStatistics : {"inputCnt":6,"activateCnt":3,"pics":"http://etpic.we17.com/test/20210127105005_2814.jpg,http://etpic.we17.com/test/20210127110434_9981.jpg,"}

class CompanyDetailDataBean {
  CompanyInfoBean companyInfo;
  List<GroupPersonCntBean> groupPersonCnt;
  TopCnt topCnt;
  bool isCache;
  int noFaceCnt;

  InputActivateStatisticsBean InputActivateStatistics;

  static CompanyDetailDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyDetailDataBean dataBean = CompanyDetailDataBean();
    dataBean.companyInfo = CompanyInfoBean.fromMap(map['companyInfo']);
    dataBean.topCnt = TopCnt.fromMap(map['topCnt']);
    dataBean.noFaceCnt = map['noFaceCnt'];
    dataBean.groupPersonCnt = List()
      ..addAll((map['groupPersonCnt'] as List ?? [])
          .map((o) => GroupPersonCntBean.fromMap(o)));
    dataBean.InputActivateStatistics =
        InputActivateStatisticsBean.fromMap(map['InputActivateStatistics']);
    return dataBean;
  }

  Map<String, dynamic> toJson() => {
  "companyInfo": companyInfo,
  "groupPersonCnt": groupPersonCnt,
  "InputActivateStatistics": InputActivateStatistics,
  "topCnt":topCnt,
    "noFaceCnt": noFaceCnt,
};}

///企业主页顶部数字
class TopCnt {
  int courseCnt;
  int classCnt;
  int projectCnt;
  int adminCnt;
  int departmentCnt;
  int cityCnt;
  int myUnActiveCnt;

  static TopCnt fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TopCnt topcnt = TopCnt();
    topcnt.courseCnt = map['courseCnt']??0;
    topcnt.classCnt = map['classCnt']??0;
    topcnt.projectCnt = map['projectCnt'];
    topcnt.adminCnt = map['adminCnt']??0;
    topcnt.departmentCnt = map['departmentCnt']??0;
    topcnt.cityCnt = map['cityCnt']??0;
    topcnt.myUnActiveCnt = map['myUnActiveCnt']??0;
    return topcnt;
  }

  Map toJson() =>
      {
        "courseCnt": courseCnt,
        "classCnt": classCnt,
        "projectCnt": projectCnt,
        "adminCnt": adminCnt,
        "departmentCnt": departmentCnt,
        "myUnActiveCnt": myUnActiveCnt,
        "cityCnt": cityCnt,

      };

}

/// inputCnt : 6
/// activateCnt : 3
/// pics : "http://etpic.we17.com/test/20210127105005_2814.jpg,http://etpic.we17.com/test/20210127110434_9981.jpg,"

class InputActivateStatisticsBean {
  int inputCnt;
  int activateCnt;
  String pics;

  static InputActivateStatisticsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    InputActivateStatisticsBean inputActivateStatisticsBean =
    InputActivateStatisticsBean();
    inputActivateStatisticsBean.inputCnt = map['inputCnt'];
    inputActivateStatisticsBean.activateCnt = map['activateCnt'];
    inputActivateStatisticsBean.pics = map['pics'];
    return inputActivateStatisticsBean;
  }

  Map toJson() =>
      {
        "inputCnt": inputCnt,
        "activateCnt": activateCnt,
        "pics": pics,
      };
}

/// groupName : "员工"
/// groupid : "eadc02553f2a40509f05e407125f15e9"
/// cnt : 8
/// visitor : "01"

class GroupPersonCntBean {
  String groupName;
  String groupid;
  int cnt;
  String visitor;

  //访客ID
  String visitorGroupId;

  /// 04员工 01访客 02学员 03家长 99普通
  String groupType;

  static GroupPersonCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GroupPersonCntBean groupPersonCntBean = GroupPersonCntBean();
    groupPersonCntBean.groupName = map['groupName'];
    groupPersonCntBean.groupid = map['groupid'];
    groupPersonCntBean.cnt = map['cnt'];
    groupPersonCntBean.visitor = map['visitor'];
    groupPersonCntBean.visitorGroupId = map['visitorGroupId'];
    groupPersonCntBean.groupType = map['groupType'];
    return groupPersonCntBean;
  }

  Map toJson() =>
      {
        "groupName": groupName,
        "groupid": groupid,
        "cnt": cnt,
        "visitor": visitor,
        "visitorGroupId": visitorGroupId,
        "groupType": groupType,
      };

  @override
  String toString() {
    return 'GroupPersonCntBean{groupName: $groupName, groupid: $groupid, cnt: $cnt, visitor: $visitor}';
  }

  bool isAllGroup() {
    return groupid == null || groupid.isEmpty;
  }

  bool isStaff() {
    return groupType == "04";
  }

  bool isStudent() {
    return groupType == "02";
  }

  bool isParent() {
    return groupType == "03";
  }
}

/// companyid : "cd191d29404e4a5dbb6ff2eb8bad48e9"
/// status : "00"
/// companyname : "北京湛腾世纪科技有限公司"
/// companynick : "湛腾科技"
/// companylogo : null
/// type : "00"
/// city : "{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}"
/// gps : null
/// address : null
/// createuid : "be97b4557acc49a782c2e52b53bb77a1"
/// createdate : 1611712498
/// updatetime : 1611712498
/// updateuid : "be97b4557acc49a782c2e52b53bb77a1"

class CompanyInfoBean {
  String companyid;
  String status;
  String companyname;
  String companynick;
  dynamic companylogo;
  String type;
  String city;
  dynamic gps;
  dynamic addrProvince;
  dynamic addrCity;
  dynamic addrDistrict;
  dynamic addrCode;
  // String enableFaceRecognition;//启用人脸识别 00启用 01关闭
  dynamic address;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;

  static CompanyInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyInfoBean companyInfoBean = CompanyInfoBean();
    companyInfoBean.companyid = map['companyid'];
    companyInfoBean.status = map['status'];
    companyInfoBean.companyname = map['companyname'];
    companyInfoBean.companynick = map['companynick'];
    companyInfoBean.companylogo = map['companylogo'];
    companyInfoBean.type = map['type'];
    companyInfoBean.city = map['city'];
    companyInfoBean.gps = map['gps'];
    companyInfoBean.addrProvince = map['addrProvince'];
    companyInfoBean.addrCity = map['addrCity'];
    companyInfoBean.addrDistrict = map['addrDistrict'];
    companyInfoBean.addrCode = map['addrCode'];
    // companyInfoBean.enableFaceRecognition = map['enableFaceRecognition'];
    companyInfoBean.address = map['address'];
    companyInfoBean.createuid = map['createuid'];
    companyInfoBean.createdate = map['createdate'];
    companyInfoBean.updatetime = map['updatetime'];
    companyInfoBean.updateuid = map['updateuid'];
    return companyInfoBean;
  }

  Map<String, dynamic> toJson() =>
      {
        "companyid": companyid,
        "status": status,
        "companyname": companyname,
        "companynick": companynick,
        "companylogo": companylogo,
        "type": type,
        "city": city,
        "gps": gps,
        "addrProvince": addrProvince,
        "addrCity": addrCity,
        "addrDistrict": addrDistrict,
        "addrCode": addrCode,
        // "enableFaceRecognition": enableFaceRecognition,
        "address": address,
        "createuid": createuid,
        "createdate": createdate,
        "updatetime": updatetime,
        "updateuid": updateuid,
      };

  bool isCompany(){
    if(type==null){
      return true;
    }
    //01培训机构 02国有企业 03民营企业 04党政机关 05大中小学 06外资企业
    // 党政机关  国有企业  民营企业 外资企业
    return type == "S02"||type == "S03"||type == "S04"||type == "S06";
  }
}

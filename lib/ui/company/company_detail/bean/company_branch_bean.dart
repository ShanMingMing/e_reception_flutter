import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "����ɹ�"
/// data : {"page":{"records":[{"address":"�Ͼ�ŶŶ","comefrom":"00","createdate":1630551041,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:41.000+00:00","cbid":"9067c2ebf05648b28503dc5ef4f938fd","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨�Ͼ���֧��"},{"address":"��ϸ��ַŶŶŶ","comefrom":"00","createdate":1630551000,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:00.000+00:00","cbid":"a03fd46f8fde4d6297f2a091b959f661","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨������֧��"},{"address":"������","comefrom":"00","createdate":1630551071,"gps":"23.128998��N,113.263114��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"����续","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:51:11.000+00:00","cbid":"ba5643807cdf435891abcf8c7907e8bd","juli":"10046122","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨���ݷ�֧��"}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class CompanyBranchBean extends BasePagerBean<CompanyBranchListInfoBean> {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CompanyBranchBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyBranchBean companyBranchBeanBean = CompanyBranchBean();
    companyBranchBeanBean.success = map['success'];
    companyBranchBeanBean.code = map['code'];
    companyBranchBeanBean.msg = map['msg'];
    companyBranchBeanBean.data = DataBean.fromMap(map['data']);
    companyBranchBeanBean.extra = map['extra'];
    return companyBranchBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<CompanyBranchListInfoBean> getDataList() => data.page.records;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data.page.records = list.cast();
  }
}

/// page : {"records":[{"address":"�Ͼ�ŶŶ","comefrom":"00","createdate":1630551041,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:41.000+00:00","cbid":"9067c2ebf05648b28503dc5ef4f938fd","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨�Ͼ���֧��"},{"address":"��ϸ��ַŶŶŶ","comefrom":"00","createdate":1630551000,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:00.000+00:00","cbid":"a03fd46f8fde4d6297f2a091b959f661","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨������֧��"},{"address":"������","comefrom":"00","createdate":1630551071,"gps":"23.128998��N,113.263114��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"����续","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:51:11.000+00:00","cbid":"ba5643807cdf435891abcf8c7907e8bd","juli":"10046122","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨���ݷ�֧��"}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"address":"�Ͼ�ŶŶ","comefrom":"00","createdate":1630551041,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:41.000+00:00","cbid":"9067c2ebf05648b28503dc5ef4f938fd","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨�Ͼ���֧��"},{"address":"��ϸ��ַŶŶŶ","comefrom":"00","createdate":1630551000,"gps":"39.855256��N,116.368748��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:50:00.000+00:00","cbid":"a03fd46f8fde4d6297f2a091b959f661","juli":"8945130","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨������֧��"},{"address":"������","comefrom":"00","createdate":1630551071,"gps":"23.128998��N,113.263114��E","addrProvince":"������","addrCity":"������","delflg":"00","gpsaddress":"����续","companyid":"5dee411ca9ab4e279c004c2778e55008","addrCode":"110106","bjtime":"2021-09-02T10:51:11.000+00:00","cbid":"ba5643807cdf435891abcf8c7907e8bd","juli":"10046122","addrDistrict":"��̨��","createuid":"4a3d21386acb4f3d923227515c130470","branchname":"�����꣨���ݷ�֧��"}]
/// total : 3
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CompanyBranchListInfoBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CompanyBranchListInfoBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// address : "�Ͼ�ŶŶ"
/// comefrom : "00"
/// createdate : 1630551041
/// gps : "39.855256��N,116.368748��E"
/// addrProvince : "������"
/// addrCity : "������"
/// delflg : "00"
/// gpsaddress : ""
/// companyid : "5dee411ca9ab4e279c004c2778e55008"
/// addrCode : "110106"
/// bjtime : "2021-09-02T10:50:41.000+00:00"
/// cbid : "9067c2ebf05648b28503dc5ef4f938fd"
/// juli : "8945130"
/// addrDistrict : "��̨��"
/// createuid : "4a3d21386acb4f3d923227515c130470"
/// branchname : "�����꣨�Ͼ���֧��"

class CompanyBranchListInfoBean {
  String address;
  String comefrom;
  int createdate;
  String gps;
  String addrProvince;
  String addrCity;
  String delflg;
  String gpsaddress;
  String companyid;
  String addrCode;
  String bjtime;
  String cbid;
  String juli;
  String addrDistrict;
  String createuid;
  String branchname;
  int allCnt;
  int powerOnCnt;
  bool selected;
  bool editSelected;

  static CompanyBranchListInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyBranchListInfoBean recordsBean = CompanyBranchListInfoBean();
    recordsBean.address = map['address'];
    recordsBean.comefrom = map['comefrom'];
    recordsBean.createdate = map['createdate'];
    recordsBean.gps = map['gps'];
    recordsBean.addrProvince = map['addrProvince'];
    recordsBean.addrCity = map['addrCity'];
    recordsBean.delflg = map['delflg'];
    recordsBean.gpsaddress = map['gpsaddress'];
    recordsBean.companyid = map['companyid'];
    recordsBean.addrCode = map['addrCode'];
    recordsBean.bjtime = map['bjtime'];
    recordsBean.cbid = map['cbid'];
    recordsBean.juli = map['juli'];
    recordsBean.addrDistrict = map['addrDistrict'];
    recordsBean.createuid = map['createuid'];
    recordsBean.branchname = map['branchname'];
    recordsBean.allCnt = map['allCnt'];
    recordsBean.powerOnCnt = map['powerOnCnt'];
    return recordsBean;
  }

  String getAddressStr(){
    return "${branchname??""}/${address??""}";
  }

  Map toJson() => {
    "address": address,
    "comefrom": comefrom,
    "createdate": createdate,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "delflg": delflg,
    "gpsaddress": gpsaddress,
    "companyid": companyid,
    "addrCode": addrCode,
    "bjtime": bjtime,
    "cbid": cbid,
    "juli": juli,
    "addrDistrict": addrDistrict,
    "createuid": createuid,
    "branchname": branchname,
    "allCnt": allCnt,
    "powerOnCnt": powerOnCnt,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CompanyBranchListInfoBean &&
          runtimeType == other.runtimeType &&
          cbid == other.cbid;

  @override
  int get hashCode => cbid.hashCode;
}
import '../../common_person_list_response.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"memberList":[{"nick":"","number":"","groupName":"员工","uname":"李岁岁0322","phone":"18711338897","city":"北京","roleid":"10","putpicurl":"http://etpic.we17.com/test/20210412152651_5605.jpg","name":"佟儒仙","fuid":"32a38f33ad0d4771891309c6e9ff2f1b","napicurl":"http://etpic.we17.com/test/20210412152651_5605.jpg","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"北京","roleid":"90","napicurl":"","userid":"6789c03be44744988f74b44021959864","nick":"","number":"","groupName":"员工","phone":"15298353932","putpicurl":"","name":"图","fuid":"5b276961906648d291b0ae64eaf96099","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"南京","roleid":"10","napicurl":"","userid":"9c8d5f9525f844c7a9ef502cd0029e19","nick":"","number":"","groupName":"员工","phone":"13313313321","putpicurl":"","name":"部门","fuid":"71b1ed00e15746f7ac0278fc27bed944","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null}],"adminList":[{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"a05b489fda1f496c873b87ab403adb95","nick":"","number":"1236666","groupName":"员工","phone":"13313313340","putpicurl":"","name":"部门1","fuid":"1f7a1905217c4f18b0882ebc50b42722","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"北京","roleid":"90","napicurl":"","userid":"b97a7385d8364e0f9217efbdf7434702","nick":"","number":"","groupName":"员工","phone":"13313313310","putpicurl":"","name":"员工两个班级两个部门","fuid":"39e4c8ad320148a0814047823d3a3e72","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"75a91d8d383b430ea6b44e5d8d62cf81","nick":"","number":"","groupName":"员工","phone":"13313313325","putpicurl":"","name":"部门123456","fuid":"ac034c5cab06487da3cdd1eb05e372d8","status":"","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"518eb0debea74e72924514d16264e272","nick":"","number":"","groupName":"员工","phone":"13313313327","putpicurl":"","name":"部门2","fuid":"e16aa679d9f945e299d6fd1e08889cbf","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"南京","roleid":"90","napicurl":"http://etpic.we17.com/test/20210413183919_6696.jpg","userid":"bc83d68def904c76bc5cfa51660a1f67","nick":"","number":"3467679","groupName":"员工","phone":"13313313351","putpicurl":"http://etpic.we17.com/test/20210413183919_6696.jpg","name":"全都有","fuid":"ebf979676e3d40719da5cf651e1a3bd7","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"nick":"昵称哇","number":"","groupName":"学员","phone":"13211111111","city":"","roleid":"99","putpicurl":"http://etpic.we17.com/test/20210329135237_2227.jpg","name":"李岁岁0322","fuid":"49fcc70f18974deda7b54a709c52b177","napicurl":"http://etpic.we17.com/test/20210329135237_2227.jpg","userid":"9bf983d2f5b34c89993bfe793dee5f3c","status":"01","lastPunchTime":1617872152,"department":null,"projectname":null,"classname":"班级3,兔兔,班级1,班级6","coursename":null},{"nick":"","number":"111","groupName":"员工","phone":"18665289540","city":"","roleid":"90","putpicurl":"","name":"186","fuid":"5063ee633c0b459a862fce55f0da58b4","napicurl":"","userid":"d5647a27c47547f38051e3cba92f5f46","status":"00","lastPunchTime":null,"department":null,"projectname":null,"classname":null,"coursename":null},{"nick":"","number":"","groupName":"员工","phone":"13313313325","city":"","roleid":"90","putpicurl":"","name":"笑嘿嘿嘿嘿","fuid":"a7c9ebf0c3b14583a398e55b6d86d31e","napicurl":"","userid":"75a91d8d383b430ea6b44e5d8d62cf81","status":"","lastPunchTime":null,"department":null,"projectname":null,"classname":null,"coursename":null}]}
/// extra : null

///班级或部门详情
class DepartmentOrClassDetailResponse {
  bool success;
  String code;
  String msg;
  AdminMemberListBean data;
  dynamic extra;

  static DepartmentOrClassDetailResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentOrClassDetailResponse departmentOrClassDetailResponseBean = DepartmentOrClassDetailResponse();
    departmentOrClassDetailResponseBean.success = map['success'];
    departmentOrClassDetailResponseBean.code = map['code'];
    departmentOrClassDetailResponseBean.msg = map['msg'];
    departmentOrClassDetailResponseBean.data = AdminMemberListBean.fromMap(map['data']);
    departmentOrClassDetailResponseBean.extra = map['extra'];
    return departmentOrClassDetailResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// memberList : [{"nick":"","number":"","groupName":"员工","uname":"李岁岁0322","phone":"18711338897","city":"北京","roleid":"10","putpicurl":"http://etpic.we17.com/test/20210412152651_5605.jpg","name":"佟儒仙","fuid":"32a38f33ad0d4771891309c6e9ff2f1b","napicurl":"http://etpic.we17.com/test/20210412152651_5605.jpg","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"北京","roleid":"90","napicurl":"","userid":"6789c03be44744988f74b44021959864","nick":"","number":"","groupName":"员工","phone":"15298353932","putpicurl":"","name":"图","fuid":"5b276961906648d291b0ae64eaf96099","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"南京","roleid":"10","napicurl":"","userid":"9c8d5f9525f844c7a9ef502cd0029e19","nick":"","number":"","groupName":"员工","phone":"13313313321","putpicurl":"","name":"部门","fuid":"71b1ed00e15746f7ac0278fc27bed944","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null}]
/// adminList : [{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"a05b489fda1f496c873b87ab403adb95","nick":"","number":"1236666","groupName":"员工","phone":"13313313340","putpicurl":"","name":"部门1","fuid":"1f7a1905217c4f18b0882ebc50b42722","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"北京","roleid":"90","napicurl":"","userid":"b97a7385d8364e0f9217efbdf7434702","nick":"","number":"","groupName":"员工","phone":"13313313310","putpicurl":"","name":"员工两个班级两个部门","fuid":"39e4c8ad320148a0814047823d3a3e72","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"75a91d8d383b430ea6b44e5d8d62cf81","nick":"","number":"","groupName":"员工","phone":"13313313325","putpicurl":"","name":"部门123456","fuid":"ac034c5cab06487da3cdd1eb05e372d8","status":"","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"","roleid":"90","napicurl":"","userid":"518eb0debea74e72924514d16264e272","nick":"","number":"","groupName":"员工","phone":"13313313327","putpicurl":"","name":"部门2","fuid":"e16aa679d9f945e299d6fd1e08889cbf","status":"00","lastPunchTime":null,"department":"开发","projectname":null,"classname":null,"coursename":null},{"uname":"李岁岁0322","city":"南京","roleid":"90","napicurl":"http://etpic.we17.com/test/20210413183919_6696.jpg","userid":"bc83d68def904c76bc5cfa51660a1f67","nick":"","number":"3467679","groupName":"员工","phone":"13313313351","putpicurl":"http://etpic.we17.com/test/20210413183919_6696.jpg","name":"全都有","fuid":"ebf979676e3d40719da5cf651e1a3bd7","status":"00","lastPunchTime":null,"department":"开发","projectname":"项目","classname":null,"coursename":null},{"nick":"昵称哇","number":"","groupName":"学员","phone":"13211111111","city":"","roleid":"99","putpicurl":"http://etpic.we17.com/test/20210329135237_2227.jpg","name":"李岁岁0322","fuid":"49fcc70f18974deda7b54a709c52b177","napicurl":"http://etpic.we17.com/test/20210329135237_2227.jpg","userid":"9bf983d2f5b34c89993bfe793dee5f3c","status":"01","lastPunchTime":1617872152,"department":null,"projectname":null,"classname":"班级3,兔兔,班级1,班级6","coursename":null},{"nick":"","number":"111","groupName":"员工","phone":"18665289540","city":"","roleid":"90","putpicurl":"","name":"186","fuid":"5063ee633c0b459a862fce55f0da58b4","napicurl":"","userid":"d5647a27c47547f38051e3cba92f5f46","status":"00","lastPunchTime":null,"department":null,"projectname":null,"classname":null,"coursename":null},{"nick":"","number":"","groupName":"员工","phone":"13313313325","city":"","roleid":"90","putpicurl":"","name":"笑嘿嘿嘿嘿","fuid":"a7c9ebf0c3b14583a398e55b6d86d31e","napicurl":"","userid":"75a91d8d383b430ea6b44e5d8d62cf81","status":"","lastPunchTime":null,"department":null,"projectname":null,"classname":null,"coursename":null}]

class AdminMemberListBean {
  List<CommonPersonBean> memberList;
  List<CommonPersonBean> adminList;
  int todayFaceCnt;

  static AdminMemberListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AdminMemberListBean dataBean = AdminMemberListBean();
    dataBean.memberList = List()..addAll(
      (map['memberList'] as List ?? []).map((o) => CommonPersonBean.fromMap(o))
    );
    dataBean.adminList = List()..addAll(
      (map['adminList'] as List ?? []).map((o) => CommonPersonBean.fromMap(o))
    );
    dataBean.todayFaceCnt=map['todayFaceCnt'];
    return dataBean;
  }

  Map toJson() => {
    'todayFaceCnt':todayFaceCnt,
    "memberList": memberList,
    "adminList": adminList,
  };
}


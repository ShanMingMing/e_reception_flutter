import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"address":"北京市丰台区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"39.855205,116.368721","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:11:39.000+00:00","caid":"320972887a2244f2869346379115d198","juli":"9569022","addrDistrict":"丰台区"},{"address":"苏州市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"独墅湖区怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:19:14.000+00:00","caid":"5b3199de65e64f1f92d92b58caefd17b","juli":"10058599","addrDistrict":"金鸡湖区"},{"address":"北京市搜宝大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"搜宝商务中心","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:41:46.000+00:00","caid":"69d747d20724431d908e7861db52f2ed","juli":"10058599","addrDistrict":"丰台区"},{"address":"南京市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"江苏基地小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:12:20.000+00:00","caid":"a5f4a4089a5f4f97b0be3bc2b9f6cdf8","juli":"10058599","addrDistrict":"江宁区"}],"total":4,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class CompanyAddressBean extends BasePagerBean<CompanyAddressListInfoBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CompanyAddressBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyAddressBean comapnyAddressBeanBean = CompanyAddressBean();
    comapnyAddressBeanBean.success = map['success'];
    comapnyAddressBeanBean.code = map['code'];
    comapnyAddressBeanBean.msg = map['msg'];
    comapnyAddressBeanBean.data = DataBean.fromMap(map['data']);
    comapnyAddressBeanBean.extra = map['extra'];
    return comapnyAddressBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<CompanyAddressListInfoBean> getDataList() => data.page.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data.page.records = list.cast();
  }
}

/// page : {"records":[{"address":"北京市丰台区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"39.855205,116.368721","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:11:39.000+00:00","caid":"320972887a2244f2869346379115d198","juli":"9569022","addrDistrict":"丰台区"},{"address":"苏州市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"独墅湖区怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:19:14.000+00:00","caid":"5b3199de65e64f1f92d92b58caefd17b","juli":"10058599","addrDistrict":"金鸡湖区"},{"address":"北京市搜宝大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"搜宝商务中心","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:41:46.000+00:00","caid":"69d747d20724431d908e7861db52f2ed","juli":"10058599","addrDistrict":"丰台区"},{"address":"南京市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"江苏基地小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:12:20.000+00:00","caid":"a5f4a4089a5f4f97b0be3bc2b9f6cdf8","juli":"10058599","addrDistrict":"江宁区"}],"total":4,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"address":"北京市丰台区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"39.855205,116.368721","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:11:39.000+00:00","caid":"320972887a2244f2869346379115d198","juli":"9569022","addrDistrict":"丰台区"},{"address":"苏州市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"独墅湖区怡然家园小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:19:14.000+00:00","caid":"5b3199de65e64f1f92d92b58caefd17b","juli":"10058599","addrDistrict":"金鸡湖区"},{"address":"北京市搜宝大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"北京市","addrCity":"北京市","delflg":"00","gpsaddress":"搜宝商务中心","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:41:46.000+00:00","caid":"69d747d20724431d908e7861db52f2ed","juli":"10058599","addrDistrict":"丰台区"},{"address":"南京市江宁区大红门街道怡然家园6号院8号楼1单元603","comefrom":"00","gps":"31.930527,118.830917","addrProvince":"江苏省","addrCity":"南京市","delflg":"00","gpsaddress":"江苏基地小区","companyid":"0126e97aed8146c785dc9be2d6a4c41c","addrCode":"110106","bjtime":"2021-08-11T10:12:20.000+00:00","caid":"a5f4a4089a5f4f97b0be3bc2b9f6cdf8","juli":"10058599","addrDistrict":"江宁区"}]
/// total : 4
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CompanyAddressListInfoBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CompanyAddressListInfoBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// address : "北京市丰台区大红门街道怡然家园6号院8号楼1单元603"
/// comefrom : "00"
/// gps : "39.855205,116.368721"
/// addrProvince : "北京市"
/// addrCity : "北京市"
/// delflg : "00"
/// gpsaddress : "怡然家园小区"
/// companyid : "0126e97aed8146c785dc9be2d6a4c41c"
/// addrCode : "110106"
/// bjtime : "2021-08-11T10:11:39.000+00:00"
/// caid : "320972887a2244f2869346379115d198"
/// juli : "9569022"
/// addrDistrict : "丰台区"

class CompanyAddressListInfoBean {
  String address;
  String comefrom;
  String gps;
  String addrProvince;
  String addrCity;
  String delflg;
  String gpsaddress;
  String companyid;
  String addrCode;
  String bjtime;
  String caid;
  String juli;
  String addrDistrict;
  String createuid;
  bool selected;
  bool editSelected;
  int bindHsnCnt;
  String cbid;//分支id
  String branchname;//分支名

  static CompanyAddressListInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyAddressListInfoBean recordsBean = CompanyAddressListInfoBean();
    recordsBean.address = map['address'];
    recordsBean.createuid = map['createuid'];
    recordsBean.comefrom = map['comefrom'];
    recordsBean.gps = map['gps'];
    recordsBean.addrProvince = map['addrProvince'];
    recordsBean.addrCity = map['addrCity'];
    recordsBean.delflg = map['delflg'];
    recordsBean.gpsaddress = map['gpsaddress'];
    recordsBean.companyid = map['companyid'];
    recordsBean.addrCode = map['addrCode'];
    recordsBean.bjtime = map['bjtime'];
    recordsBean.caid = map['caid'];
    recordsBean.juli = map['juli'];
    recordsBean.addrDistrict = map['addrDistrict'];
    recordsBean.bindHsnCnt = map['bindHsnCnt'];
    recordsBean.cbid = map['cbid'];
    recordsBean.branchname = map['branchname'];
    return recordsBean;
  }

  Map toJson() => {
    "bindHsnCnt": bindHsnCnt,
    "address": address,
    "createuid": createuid,
    "comefrom": comefrom,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "delflg": delflg,
    "gpsaddress": gpsaddress,
    "companyid": companyid,
    "addrCode": addrCode,
    "bjtime": bjtime,
    "caid": caid,
    "juli": juli,
    "addrDistrict": addrDistrict,
    "cbid": cbid,
    "branchname": branchname,
  };
}
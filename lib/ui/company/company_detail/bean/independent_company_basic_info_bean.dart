/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"companyBasicInfo":{"companyid":"1a172b06414b4770ba2c2f7d9d0aab95","address":"1春节快乐啦可快可快了可快可快了来啦来啦来啦来啦就看","companylogo":"http://etpic.we17.com/test/20210419181408_3643.jpg","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","companyname":"1","companynick":"湛腾企业无规则"}}
/// extra : null

class IndependentCompanyBasicInfoBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static IndependentCompanyBasicInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndependentCompanyBasicInfoBean independentCompanyBasicInfoBeanBean = IndependentCompanyBasicInfoBean();
    independentCompanyBasicInfoBeanBean.success = map['success'];
    independentCompanyBasicInfoBeanBean.code = map['code'];
    independentCompanyBasicInfoBeanBean.msg = map['msg'];
    independentCompanyBasicInfoBeanBean.data = DataBean.fromMap(map['data']);
    independentCompanyBasicInfoBeanBean.extra = map['extra'];
    return independentCompanyBasicInfoBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// companyBasicInfo : {"companyid":"1a172b06414b4770ba2c2f7d9d0aab95","address":"1春节快乐啦可快可快了可快可快了来啦来啦来啦来啦就看","companylogo":"http://etpic.we17.com/test/20210419181408_3643.jpg","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","companyname":"1","companynick":"湛腾企业无规则"}

class DataBean {
  CompanyBasicInfoBean companyBasicInfo;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.companyBasicInfo = CompanyBasicInfoBean.fromMap(map['companyBasicInfo']);
    return dataBean;
  }

  Map toJson() => {
    "companyBasicInfo": companyBasicInfo,
  };
}

/// companyid : "1a172b06414b4770ba2c2f7d9d0aab95"
/// address : "1春节快乐啦可快可快了可快可快了来啦来啦来啦来啦就看"
/// companylogo : "http://etpic.we17.com/test/20210419181408_3643.jpg"
/// city : "{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}"
/// companyname : "1"
/// companynick : "湛腾企业无规则"

class CompanyBasicInfoBean {
  String companyid;
  String address;
  String companylogo;
  String city;
  String companyname;
  String companynick;
  String userAddress;
  String userCity;
  String type;
  int addressCnt;
  List<ComAddressInfoBean> comAddressInfo;

  static CompanyBasicInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyBasicInfoBean companyBasicInfoBean = CompanyBasicInfoBean();
    companyBasicInfoBean.companyid = map['companyid'];
    companyBasicInfoBean.address = map['address'];
    companyBasicInfoBean.companylogo = map['companylogo'];
    companyBasicInfoBean.city = map['city'];
    companyBasicInfoBean.companyname = map['companyname'];
    companyBasicInfoBean.companynick = map['companynick'];
    companyBasicInfoBean.userAddress = map['userAddress'];
    companyBasicInfoBean.userCity = map['userCity'];
    companyBasicInfoBean.type = map['type'];
    companyBasicInfoBean.addressCnt = map['addressCnt'];
    companyBasicInfoBean.comAddressInfo = List()..addAll(
        (map['comAddressInfo'] as List ?? []).map((o) => ComAddressInfoBean.fromMap(o))
    );
    return companyBasicInfoBean;
  }

  Map toJson() => {
    "comAddressInfo": comAddressInfo,
    "companyid": companyid,
    "address": address,
    "companylogo": companylogo,
    "city": city,
    "companyname": companyname,
    "companynick": companynick,
    "userAddress": userAddress,
    "userCity": userCity,
    "type": type,
    "addressCnt": addressCnt,
  };
}

/// address : "搜宝商务中心二号楼"
/// comefrom : "00"
/// createdate : 1628735373
/// gps : "String gps"
/// addrProvince : "北京市"
/// addrCity : "北京市"
/// delflg : "00"
/// gpsaddress : "String gpsadress"
/// companyid : "0126e97aed8146c785dc9be2d6a4c41c"
/// addrCode : "addrCode"
/// bjtime : "2021-08-12T10:29:33.000+00:00"
/// updateuid : "b9b16482b7b74f97a8735cb17d2ebafb"
/// caid : "f0eadababa344fdcbb2a4ed93d0322d7"
/// juli : "4170874"
/// updatetime : 1628844988
/// addrDistrict : "丰台区"
/// createuid : "4a3d21386acb4f3d923227515c130470"

class ComAddressInfoBean {
  String address;
  String comefrom;
  int createdate;
  String gps;
  String addrProvince;
  String addrCity;
  String delflg;
  String gpsaddress;
  String companyid;
  String addrCode;
  String bjtime;
  String updateuid;
  String caid;
  String juli;
  int updatetime;
  String addrDistrict;
  String createuid;

  static ComAddressInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComAddressInfoBean comAddressInfoBean = ComAddressInfoBean();
    comAddressInfoBean.address = map['address'];
    comAddressInfoBean.comefrom = map['comefrom'];
    comAddressInfoBean.createdate = map['createdate'];
    comAddressInfoBean.gps = map['gps'];
    comAddressInfoBean.addrProvince = map['addrProvince'];
    comAddressInfoBean.addrCity = map['addrCity'];
    comAddressInfoBean.delflg = map['delflg'];
    comAddressInfoBean.gpsaddress = map['gpsaddress'];
    comAddressInfoBean.companyid = map['companyid'];
    comAddressInfoBean.addrCode = map['addrCode'];
    comAddressInfoBean.bjtime = map['bjtime'];
    comAddressInfoBean.updateuid = map['updateuid'];
    comAddressInfoBean.caid = map['caid'];
    comAddressInfoBean.juli = map['juli'];
    comAddressInfoBean.updatetime = map['updatetime'];
    comAddressInfoBean.addrDistrict = map['addrDistrict'];
    comAddressInfoBean.createuid = map['createuid'];
    return comAddressInfoBean;
  }

  Map toJson() => {
    "address": address,
    "comefrom": comefrom,
    "createdate": createdate,
    "gps": gps,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "delflg": delflg,
    "gpsaddress": gpsaddress,
    "companyid": companyid,
    "addrCode": addrCode,
    "bjtime": bjtime,
    "updateuid": updateuid,
    "caid": caid,
    "juli": juli,
    "updatetime": updatetime,
    "addrDistrict": addrDistrict,
    "createuid": createuid,
  };
}
// import 'package:vg_base/vg_pull_to_refresh_lib.dart';
//
// /// success : true
// /// code : "200"
// /// msg : "处理成功"
// /// data : {"page":{"records":[{"classid":"354dc842ff224f0f9f6719c9a0af5862","classname":"班级3","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bd0e5eeeecda48c98d385abb01127700","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bce1301b067d44ffbc2a02434dacde78","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"5a5f2f4c46f94c07ae84b5f407aeda93","classname":"班级6","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a45dd9e145134188b095d74a2513f015","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"182e1c69732347259eb189d97fc4b0d1","classname":"班级哇哇","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"259c15004f6646d4af7aaf8743e7ebfc","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a4d7164246884c0585ef21b5d0d204d0","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"49ac26e7051d4a9ea265ed8f9faf3547","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"6c5dd64ada154cecbf7d63214defefb8","classname":"班级1","adminCnt":15,"adminName":"员工两个班级两个部门,李岁岁0322,员工2,部门2,李岁岁0322,李岁岁0322,超管,全都有,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,186,笑嘿嘿嘿嘿,员工1,李岁岁0322,186,部门123456,员工1,李岁岁0322,图,李岁红,员工两个班级两个部门,李岁岁0322,员工2,部门2","memberCnt":6},{"classid":"ccf5cd80a98549fd97cc2b6c00367e07","classname":"班级哇哇1","adminCnt":0,"adminName":"","memberCnt":0}],"total":11,"size":50,"current":1,"orders":[],"searchCount":true,"pages":1}}
// /// extra : null
//
// class ClassListResponse extends BasePagerBean<RecordsBean>{
//   bool success;
//   String code;
//   String msg;
//   DataBean data;
//   dynamic extra;
//
//   static ClassListResponse fromMap(Map<String, dynamic> map) {
//     if (map == null) return null;
//     ClassListResponse classListResponseBean = ClassListResponse();
//     classListResponseBean.success = map['success'];
//     classListResponseBean.code = map['code'];
//     classListResponseBean.msg = map['msg'];
//     classListResponseBean.data = DataBean.fromMap(map['data']);
//     classListResponseBean.extra = map['extra'];
//     return classListResponseBean;
//   }
//
//   Map toJson() => {
//     "success": success,
//     "code": code,
//     "msg": msg,
//     "data": data,
//     "extra": extra,
//   };
//
//   @override
//   int getCurrentPage() {
//     return data?.page?.current??1;
//   }
//
//   @override
//   List<RecordsBean> getDataList() {
//     return data?.page?.records;
//   }
//
//   @override
//   int getMaxPage() {
//     return data?.page?.total??1;
//   }
//
//   @override
//   String getMessage() {
//     return msg;
//   }
//
//   @override
//   bool isSucceed() {
//     return success;
//   }
//
// }
//
// /// page : {"records":[{"classid":"354dc842ff224f0f9f6719c9a0af5862","classname":"班级3","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bd0e5eeeecda48c98d385abb01127700","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bce1301b067d44ffbc2a02434dacde78","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"5a5f2f4c46f94c07ae84b5f407aeda93","classname":"班级6","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a45dd9e145134188b095d74a2513f015","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"182e1c69732347259eb189d97fc4b0d1","classname":"班级哇哇","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"259c15004f6646d4af7aaf8743e7ebfc","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a4d7164246884c0585ef21b5d0d204d0","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"49ac26e7051d4a9ea265ed8f9faf3547","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"6c5dd64ada154cecbf7d63214defefb8","classname":"班级1","adminCnt":15,"adminName":"员工两个班级两个部门,李岁岁0322,员工2,部门2,李岁岁0322,李岁岁0322,超管,全都有,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,186,笑嘿嘿嘿嘿,员工1,李岁岁0322,186,部门123456,员工1,李岁岁0322,图,李岁红,员工两个班级两个部门,李岁岁0322,员工2,部门2","memberCnt":6},{"classid":"ccf5cd80a98549fd97cc2b6c00367e07","classname":"班级哇哇1","adminCnt":0,"adminName":"","memberCnt":0}],"total":11,"size":50,"current":1,"orders":[],"searchCount":true,"pages":1}
//
// class DataBean {
//   PageBean page;
//   int noClassCnt;//未分配班级数
//   int noDepartmentCnt;//未分配部门数
//   static DataBean fromMap(Map<String, dynamic> map) {
//     if (map == null) return null;
//     DataBean dataBean = DataBean();
//     dataBean.page = PageBean.fromMap(map['page']);
//     dataBean.noClassCnt = map['noClassCnt'];
//     dataBean.noDepartmentCnt = map['noDepartmentCnt'];
//     return dataBean;
//   }
//
//   Map toJson() => {
//     "page": page,
//   };
// }
//
// /// records : [{"classid":"354dc842ff224f0f9f6719c9a0af5862","classname":"班级3","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bd0e5eeeecda48c98d385abb01127700","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"bce1301b067d44ffbc2a02434dacde78","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"5a5f2f4c46f94c07ae84b5f407aeda93","classname":"班级6","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a45dd9e145134188b095d74a2513f015","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"182e1c69732347259eb189d97fc4b0d1","classname":"班级哇哇","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"259c15004f6646d4af7aaf8743e7ebfc","classname":"班级4","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"a4d7164246884c0585ef21b5d0d204d0","classname":"班级1","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"49ac26e7051d4a9ea265ed8f9faf3547","classname":"兔兔","adminCnt":0,"adminName":"","memberCnt":0},{"classid":"6c5dd64ada154cecbf7d63214defefb8","classname":"班级1","adminCnt":15,"adminName":"员工两个班级两个部门,李岁岁0322,员工2,部门2,李岁岁0322,李岁岁0322,超管,全都有,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,李岁岁0322,员工两个班级,部门1,李岁岁0322,186,笑嘿嘿嘿嘿,员工1,李岁岁0322,186,部门123456,员工1,李岁岁0322,图,李岁红,员工两个班级两个部门,李岁岁0322,员工2,部门2","memberCnt":6},{"classid":"ccf5cd80a98549fd97cc2b6c00367e07","classname":"班级哇哇1","adminCnt":0,"adminName":"","memberCnt":0}]
// /// total : 11
// /// size : 50
// /// current : 1
// /// orders : []
// /// searchCount : true
// /// pages : 1
//
// class PageBean {
//   List<RecordsBean> records;
//   int total;
//   int size;
//   int current;
//   List<dynamic> orders;
//   bool searchCount;
//   int pages;
//
//   static PageBean fromMap(Map<String, dynamic> map) {
//     if (map == null) return null;
//     PageBean pageBean = PageBean();
//     pageBean.records = List()..addAll(
//       (map['records'] as List ?? []).map((o) => RecordsBean.fromMap(o))
//     );
//     pageBean.total = map['total'];
//     pageBean.size = map['size'];
//     pageBean.current = map['current'];
//     pageBean.orders = map['orders'];
//     pageBean.searchCount = map['searchCount'];
//     pageBean.pages = map['pages'];
//     return pageBean;
//   }
//
//   Map toJson() => {
//     "records": records,
//     "total": total,
//     "size": size,
//     "current": current,
//     "orders": orders,
//     "searchCount": searchCount,
//     "pages": pages,
//   };
// }
//
// /// classid : "354dc842ff224f0f9f6719c9a0af5862"
// /// classname : "班级3"
// /// adminCnt : 0
// /// adminName : ""
// /// memberCnt : 0
//
// class RecordsBean {
//   String classid;
//   String classname;
//   int adminCnt;
//   String adminName;
//   int memberCnt;
//   int faceCnt;
//   String picurls;
//   String dpPicurl;
//
//   bool isSelect;
//
//
//   static RecordsBean fromMap(Map<String, dynamic> map) {
//     if (map == null) return null;
//     RecordsBean recordsBean = RecordsBean();
//     recordsBean.classid = map['classid'];
//     recordsBean.classname = map['classname'];
//     recordsBean.adminCnt = map['adminCnt'];
//     recordsBean.faceCnt = map['faceCnt'];
//     recordsBean.adminName = map['adminName'];
//     recordsBean.memberCnt = map['memberCnt'];
//     recordsBean.picurls = map['picurls'];
//     recordsBean.dpPicurl = map['dpPicurl'];
//
//     return recordsBean;
//   }
//
//   Map toJson() => {
//     "classid": classid,
//     "classname": classname,
//     "faceCnt": faceCnt,
//     "adminCnt": adminCnt,
//     "adminName": adminName,
//     "memberCnt": memberCnt,
//     "picurls": picurls,
//     "dpPicurl": dpPicurl,
//
//   };
// }
import 'package:e_reception_flutter/ui/company/common_person_list_response.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"sameNameUser":[{"nick":"","phone":"","putpicurl":"","name":"3389","napicurl":"","fuid":"6c98c6ed40194b0a8ae3aa7a30a23a9d"}]}
/// extra : null

class DuplicateUserResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static DuplicateUserResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DuplicateUserResponse duplicateUserResponseBean = DuplicateUserResponse();
    duplicateUserResponseBean.success = map['success'];
    duplicateUserResponseBean.code = map['code'];
    duplicateUserResponseBean.msg = map['msg'];
    duplicateUserResponseBean.data = DataBean.fromMap(map['data']);
    duplicateUserResponseBean.extra = map['extra'];
    return duplicateUserResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// sameNameUser : [{"nick":"","phone":"","putpicurl":"","name":"3389","napicurl":"","fuid":"6c98c6ed40194b0a8ae3aa7a30a23a9d"}]

class DataBean {
  List<CommonPersonBean> sameNameUser;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.sameNameUser = List()..addAll(
      (map['sameNameUser'] as List ?? []).map((o) => CommonPersonBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "sameNameUser": sameNameUser,
  };
}


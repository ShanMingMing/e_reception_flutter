import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/common_terminal_settings_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "����ɹ�"
/// data : {"allCnt":0,"ComBranch":{"cbid":"b5318e1097ac40d68bb6d1d78fb211d0","branchname":"LOL��գ�������","comefrom":"00","companyid":"2697f30aec4b4c56a27be23e6a5798bb","gps":"39.855228��N,116.368936��E","gpsaddress":"","addrProvince":"������","addrCity":"������","addrDistrict":"��̨��","addrCode":"110106","address":"��������ϸ��ַѽѽѽ","createuid":"b2c9cec02f1e4ac995e62bde62761209","createdate":1630573068,"updatetime":null,"updateuid":null,"delflg":"00","bjtime":"2021-09-02T16:57:48"},"powerOnCnt":0,"page":{"records":[{"terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","address":"�����з�̨����������·��·16-3��¥�����й���������(�ѱ���������֧��)","bindflg":"01","terminalName":"�ն���ʾ��2","companyid":"2697f30aec4b4c56a27be23e6a5798bb","logout":"00","reportHsnLastTime":"2021-09-02 17:00:24","screenStatus":"00","endday":"2022-09-02","sideNumber":"6948939665959","juli":"8945127","id":1403,"position":"�Ͼ��ĵĵ�ַŶŶ","autoOnoffTime":"08:00-18:00","onOff":0,"manager":"�����","loginName":"�����"}],"total":1,"size":500,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class BranchDetailBean {
  bool success;
  String code;
  String msg;
  BranchDetailDataBean data;
  dynamic extra;

  static BranchDetailBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BranchDetailBean branchDetailBeanBean = BranchDetailBean();
    branchDetailBeanBean.success = map['success'];
    branchDetailBeanBean.code = map['code'];
    branchDetailBeanBean.msg = map['msg'];
    branchDetailBeanBean.data = BranchDetailDataBean.fromMap(map['data']);
    branchDetailBeanBean.extra = map['extra'];
    return branchDetailBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<CompanyBranchDetailListItemBean> getDataList() => data.page.records;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

}

/// allCnt : 0
/// ComBranch : {"cbid":"b5318e1097ac40d68bb6d1d78fb211d0","branchname":"LOL��գ�������","comefrom":"00","companyid":"2697f30aec4b4c56a27be23e6a5798bb","gps":"39.855228��N,116.368936��E","gpsaddress":"","addrProvince":"������","addrCity":"������","addrDistrict":"��̨��","addrCode":"110106","address":"��������ϸ��ַѽѽѽ","createuid":"b2c9cec02f1e4ac995e62bde62761209","createdate":1630573068,"updatetime":null,"updateuid":null,"delflg":"00","bjtime":"2021-09-02T16:57:48"}
/// powerOnCnt : 0
/// page : {"records":[{"terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","address":"�����з�̨����������·��·16-3��¥�����й���������(�ѱ���������֧��)","bindflg":"01","terminalName":"�ն���ʾ��2","companyid":"2697f30aec4b4c56a27be23e6a5798bb","logout":"00","reportHsnLastTime":"2021-09-02 17:00:24","screenStatus":"00","endday":"2022-09-02","sideNumber":"6948939665959","juli":"8945127","id":1403,"position":"�Ͼ��ĵĵ�ַŶŶ","autoOnoffTime":"08:00-18:00","onOff":0,"manager":"�����","loginName":"�����"}],"total":1,"size":500,"current":1,"orders":[],"searchCount":true,"pages":1}

class BranchDetailDataBean {
  int allCnt;
  ComBranchBean ComBranch;
  int powerOnCnt;
  PageBean page;

  static BranchDetailDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BranchDetailDataBean dataBean = BranchDetailDataBean();
    dataBean.allCnt = map['allCnt'];
    dataBean.ComBranch = ComBranchBean.fromMap(map['ComBranch']);
    dataBean.powerOnCnt = map['powerOnCnt'];
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "allCnt": allCnt,
    "ComBranch": ComBranch,
    "powerOnCnt": powerOnCnt,
    "page": page,
  };
}

/// records : [{"terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","address":"�����з�̨����������·��·16-3��¥�����й���������(�ѱ���������֧��)","bindflg":"01","terminalName":"�ն���ʾ��2","companyid":"2697f30aec4b4c56a27be23e6a5798bb","logout":"00","reportHsnLastTime":"2021-09-02 17:00:24","screenStatus":"00","endday":"2022-09-02","sideNumber":"6948939665959","juli":"8945127","id":1403,"position":"�Ͼ��ĵĵ�ַŶŶ","autoOnoffTime":"08:00-18:00","onOff":0,"manager":"�����","loginName":"�����"}]
/// total : 1
/// size : 500
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CompanyBranchDetailListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CompanyBranchDetailListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// terminalPicurl : ""
/// hsn : "1572BA5FCA24E0E6F66B7A67ED3E636CA0"
/// address : "�����з�̨����������·��·16-3��¥�����й���������(�ѱ���������֧��)"
/// bindflg : "01"
/// terminalName : "�ն���ʾ��2"
/// companyid : "2697f30aec4b4c56a27be23e6a5798bb"
/// logout : "00"
/// reportHsnLastTime : "2021-09-02 17:00:24"
/// screenStatus : "00"
/// endday : "2022-09-02"
/// sideNumber : "6948939665959"
/// juli : "8945127"
/// id : 1403
/// position : "�Ͼ��ĵĵ�ַŶŶ"
/// autoOnoffTime : "08:00-18:00"
/// onOff : 0
/// manager : "�����"
/// loginName : "�����"

class CompanyBranchDetailListItemBean {
  String terminalPicurl;
  String hsn;
  String address;
  String bindflg;
  String terminalName;
  String companyid;
  String logout;
  String reportHsnLastTime;
  String screenStatus;
  String endday;
  String sideNumber;
  String juli;
  int id;
  String position;
  String autoOnoffTime;
  String autoOnoffWeek;
  String autoOnoff;
  int onOff;
  String manager;
  String loginName;
  ComAutoSwitchBean comAutoSwitch;

  static CompanyBranchDetailListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyBranchDetailListItemBean recordsBean = CompanyBranchDetailListItemBean();
    recordsBean.terminalPicurl = map['terminalPicurl'];
    recordsBean.hsn = map['hsn'];
    recordsBean.address = map['address'];
    recordsBean.bindflg = map['bindflg'];
    recordsBean.terminalName = map['terminalName'];
    recordsBean.companyid = map['companyid'];
    recordsBean.logout = map['logout'];
    recordsBean.reportHsnLastTime = map['reportHsnLastTime'];
    recordsBean.screenStatus = map['screenStatus'];
    recordsBean.endday = map['endday'];
    recordsBean.sideNumber = map['sideNumber'];
    recordsBean.juli = map['juli'];
    recordsBean.id = map['id'];
    recordsBean.position = map['position'];
    recordsBean.autoOnoffTime = map['autoOnoffTime'];
    recordsBean.autoOnoffWeek = map['autoOnoffWeek'];
    recordsBean.autoOnoff = map['autoOnoff'];
    recordsBean.onOff = map['onOff'];
    recordsBean.manager = map['manager'];
    recordsBean.loginName = map['loginName'];
    recordsBean.comAutoSwitch = ComAutoSwitchBean.fromMap(map);
    return recordsBean;
  }

  Map toJson() => {
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "address": address,
    "bindflg": bindflg,
    "terminalName": terminalName,
    "companyid": companyid,
    "logout": logout,
    "reportHsnLastTime": reportHsnLastTime,
    "screenStatus": screenStatus,
    "endday": endday,
    "sideNumber": sideNumber,
    "juli": juli,
    "id": id,
    "position": position,
    "autoOnoffTime": autoOnoffTime,
    "onOff": onOff,
    "manager": manager,
    "loginName": loginName,
    "autoOnoffWeek": autoOnoffWeek,
    "autoOnoff": autoOnoff,
    "comAutoSwitch": comAutoSwitch,
  };

  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      // return "硬件ID：" + hsn;
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }

  SetTerminalStateType getTerminalState(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return SetTerminalStateType.off;
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return SetTerminalStateType.screenOff;
      }else{
        return SetTerminalStateType.open;
      }
    }
  }

  String getTerminalStateString(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      if(StringUtils.isNotEmpty(reportHsnLastTime??"")){
        String currentTimeStr = DateUtil.getNowDateStr();
        int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
        int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
        int interval = (currentTime - time)~/(24*60*60*1000);
        if(interval > 2){
          return "已关机${interval}天";
        }else{
          return "已关机";
        }
      }
      return "已关机";
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return "已关机";
        // return "息屏开机";
      }else{
        return "开机状态";
      }
    }
  }

  String getEndDayString(){
    if(StringUtils.isEmpty(endday)){
      return "-";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "";
    }
    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      return endday.replaceAll("-", "/") + "到期";
    }
  }

  String getNotifyString(){
    if(StringUtils.isEmpty(endday)){
      return "";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "已到期";
    }

    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "已到期" +  (((currentDay.millisecondsSinceEpoch - endDayDateTime.millisecondsSinceEpoch)~/(1000*3600*24))).toString() + "天";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      int interval = (((endDayDateTime.millisecondsSinceEpoch - currentDay.millisecondsSinceEpoch)~/(1000*3600*24)));
      if(interval >= 100){
        return "";
      }
      return "（${interval}天后）";
    }
  }
}

/// cbid : "b5318e1097ac40d68bb6d1d78fb211d0"
/// branchname : "LOL��գ�������"
/// comefrom : "00"
/// companyid : "2697f30aec4b4c56a27be23e6a5798bb"
/// gps : "39.855228��N,116.368936��E"
/// gpsaddress : ""
/// addrProvince : "������"
/// addrCity : "������"
/// addrDistrict : "��̨��"
/// addrCode : "110106"
/// address : "��������ϸ��ַѽѽѽ"
/// createuid : "b2c9cec02f1e4ac995e62bde62761209"
/// createdate : 1630573068
/// updatetime : null
/// updateuid : null
/// delflg : "00"
/// bjtime : "2021-09-02T16:57:48"

class ComBranchBean {
  String cbid;
  String branchname;
  String comefrom;
  String companyid;
  String gps;
  String gpsaddress;
  String addrProvince;
  String addrCity;
  String addrDistrict;
  String addrCode;
  String address;
  String createuid;
  int createdate;
  dynamic updatetime;
  dynamic updateuid;
  String delflg;
  String bjtime;

  static ComBranchBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComBranchBean comBranchBean = ComBranchBean();
    comBranchBean.cbid = map['cbid'];
    comBranchBean.branchname = map['branchname'];
    comBranchBean.comefrom = map['comefrom'];
    comBranchBean.companyid = map['companyid'];
    comBranchBean.gps = map['gps'];
    comBranchBean.gpsaddress = map['gpsaddress'];
    comBranchBean.addrProvince = map['addrProvince'];
    comBranchBean.addrCity = map['addrCity'];
    comBranchBean.addrDistrict = map['addrDistrict'];
    comBranchBean.addrCode = map['addrCode'];
    comBranchBean.address = map['address'];
    comBranchBean.createuid = map['createuid'];
    comBranchBean.createdate = map['createdate'];
    comBranchBean.updatetime = map['updatetime'];
    comBranchBean.updateuid = map['updateuid'];
    comBranchBean.delflg = map['delflg'];
    comBranchBean.bjtime = map['bjtime'];
    return comBranchBean;
  }

  Map toJson() => {
    "cbid": cbid,
    "branchname": branchname,
    "comefrom": comefrom,
    "companyid": companyid,
    "gps": gps,
    "gpsaddress": gpsaddress,
    "addrProvince": addrProvince,
    "addrCity": addrCity,
    "addrDistrict": addrDistrict,
    "addrCode": addrCode,
    "address": address,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "bjtime": bjtime,
  };
}
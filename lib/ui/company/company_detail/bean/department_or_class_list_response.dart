import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_page.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"departmentid":"d0f4b439dfa1411fad16f56e963b54dc","department":"部门、","adminCnt":1,"adminName":"李威廉","memberCnt":2},{"departmentid":"76d6acddac7549cbaf0ebd136423818a","department":"部门","adminCnt":2,"adminName":"1241412,李威廉","memberCnt":2},{"departmentid":"1e631ce90fd24045ae8164c45147a43d","department":"123","adminCnt":1,"adminName":"李威廉","memberCnt":0}],"total":3,"size":50,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class DepartmentOrClassListResponse extends BasePagerBean<DepartmentOrClassListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static DepartmentOrClassListResponse fromMap(Map<String, dynamic> map, int type) {
    if (map == null) return null;
    DepartmentOrClassListResponse departmentListResponseBean = DepartmentOrClassListResponse();
    departmentListResponseBean.success = map['success'];
    departmentListResponseBean.code = map['code'];
    departmentListResponseBean.msg = map['msg'];
    departmentListResponseBean.data = DataBean.fromMap(map['data'], type);
    departmentListResponseBean.extra = map['extra'];
    return departmentListResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  @override
  int getCurrentPage() {
    return data?.page?.current??1;
  }

  @override
  List<DepartmentOrClassListItemBean> getDataList() {
    return data?.page?.records;
  }

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages ?? 1;

  @override
  String getMessage() {
    return msg;
  }

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// page : {"records":[{"departmentid":"d0f4b439dfa1411fad16f56e963b54dc","department":"部门、","adminCnt":1,"adminName":"李威廉","memberCnt":2},{"departmentid":"76d6acddac7549cbaf0ebd136423818a","department":"部门","adminCnt":2,"adminName":"1241412,李威廉","memberCnt":2},{"departmentid":"1e631ce90fd24045ae8164c45147a43d","department":"123","adminCnt":1,"adminName":"李威廉","memberCnt":0}],"total":3,"size":50,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;
  int noClassCnt;//未分配班级数
  int noDepartmentCnt;//未分配部门数
  static DataBean fromMap(Map<String, dynamic> map, int type) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page'], type);
    dataBean.noClassCnt = map['noClassCnt'];
    dataBean.noDepartmentCnt = map['noDepartmentCnt'];
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"departmentid":"d0f4b439dfa1411fad16f56e963b54dc","department":"部门、","adminCnt":1,"adminName":"李威廉","memberCnt":2},{"departmentid":"76d6acddac7549cbaf0ebd136423818a","department":"部门","adminCnt":2,"adminName":"1241412,李威廉","memberCnt":2},{"departmentid":"1e631ce90fd24045ae8164c45147a43d","department":"123","adminCnt":1,"adminName":"李威廉","memberCnt":0}]
/// total : 3
/// size : 50
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<DepartmentOrClassListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map, int type) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => DepartmentOrClassListItemBean.fromMap(o, type))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// departmentid : "d0f4b439dfa1411fad16f56e963b54dc"
/// department : "部门、"
/// adminCnt : 1
/// adminName : "李威廉"
/// memberCnt : 2

class DepartmentOrClassListItemBean {
  String departmentid;
  String department;
  int adminCnt;
  String adminName;
  int memberCnt;
  int faceCnt;
  String picurls;
  String dpPicurl;

  String classid;
  String classname;

  bool isSelect;

    ///页面类型
  int type;

  ///是班级数据
  bool isClass() => ClassOrDepartmentPage.TYPE_CLASS == type;

   ///是部门数据
  bool isDepartment() => ClassOrDepartmentPage.TYPE_DEPARTMENT == type;

  void setName(String name){
    if(ClassOrDepartmentPage.TYPE_CLASS == type){
      classname = name;
    }else{
      department = name;
    }
  }

  String getName(){
    if(ClassOrDepartmentPage.TYPE_CLASS == type){
      return classname;
    }
    return department;
  }

  String getId(){
    if(ClassOrDepartmentPage.TYPE_CLASS == type){
      return classid;
    }
    return departmentid;
  }

  static DepartmentOrClassListItemBean fromMap(Map<String, dynamic> map, int type) {
    if (map == null) return null;
    DepartmentOrClassListItemBean recordsBean = DepartmentOrClassListItemBean();
    recordsBean.departmentid = map['departmentid'];
    recordsBean.department = map['department'];
    recordsBean.adminCnt = map['adminCnt'];
    recordsBean.adminName = map['adminName'];
    recordsBean.faceCnt = map['faceCnt'];
    recordsBean.memberCnt = map['memberCnt'];
    recordsBean.picurls = map['picurls'];
    recordsBean.dpPicurl = map['dpPicurl'];

    recordsBean.classid = map['classid'];
    recordsBean.classname = map['classname'];
    recordsBean.isSelect = map['isSelect'];
    recordsBean.type = type;
    return recordsBean;
  }

  Map toJson() => {
    "departmentid": departmentid,
    "department": department,
    "faceCnt": faceCnt,
    "adminCnt": adminCnt,
    "adminName": adminName,
    "memberCnt": memberCnt,
    "picurls": picurls,
    "dpPicurl": dpPicurl,
    "classid": classid,
    "classname": classname,
    "isSelect": isSelect,
    "type": type,
  };
}
/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"companyInfo":{"companyid":"787f1dd0a2a94129b6ff2bb027be45b9","status":"00","companyname":"","companynick":"0204企业","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"4ab65f9a027040208efdbd9472e33ae7","createdate":1612428889,"updatetime":1612428889,"updateuid":"4ab65f9a027040208efdbd9472e33ae7","groupList":null},"groupPersonCnt":[{"groupName":"全部","groupid":"","cnt":6,"visitor":"01"},{"groupName":"员工","groupid":"6ec391275f824372a0da06c47cd0428d","cnt":4,"visitor":"01"},{"groupName":"访客","groupid":"8610db7c4e2a4074817573deda833f7f","cnt":2,"visitor":"00"},{"groupName":"不记打卡分组","groupid":"ec3dc49554d64a02a483fea2c3178e99","cnt":0,"visitor":"01"}],"InputActivateStatistics":{"inputCnt":6,"activateCnt":2,"pics":"http://etpic.we17.com/test/20210224175307_6461.jpg,http://etpic.we17.com/test/20210301161137_1266.jpg,"}}
/// extra : null

class EnterpriseInformationBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static EnterpriseInformationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EnterpriseInformationBean enterpriseInformationBeanBean = EnterpriseInformationBean();
    enterpriseInformationBeanBean.success = map['success'];
    enterpriseInformationBeanBean.code = map['code'];
    enterpriseInformationBeanBean.msg = map['msg'];
    enterpriseInformationBeanBean.data = DataBean.fromMap(map['data']);
    enterpriseInformationBeanBean.extra = map['extra'];
    return enterpriseInformationBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// companyInfo : {"companyid":"787f1dd0a2a94129b6ff2bb027be45b9","status":"00","companyname":"","companynick":"0204企业","companylogo":null,"type":"00","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"createuid":"4ab65f9a027040208efdbd9472e33ae7","createdate":1612428889,"updatetime":1612428889,"updateuid":"4ab65f9a027040208efdbd9472e33ae7","groupList":null}
/// groupPersonCnt : [{"groupName":"全部","groupid":"","cnt":6,"visitor":"01"},{"groupName":"员工","groupid":"6ec391275f824372a0da06c47cd0428d","cnt":4,"visitor":"01"},{"groupName":"访客","groupid":"8610db7c4e2a4074817573deda833f7f","cnt":2,"visitor":"00"},{"groupName":"不记打卡分组","groupid":"ec3dc49554d64a02a483fea2c3178e99","cnt":0,"visitor":"01"}]
/// InputActivateStatistics : {"inputCnt":6,"activateCnt":2,"pics":"http://etpic.we17.com/test/20210224175307_6461.jpg,http://etpic.we17.com/test/20210301161137_1266.jpg,"}

class DataBean {
  CompanyInfoBean companyInfo;
  List<GroupPersonCntBean> groupPersonCnt;
  InputActivateStatisticsBean InputActivateStatistics;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.companyInfo = CompanyInfoBean.fromMap(map['companyInfo']);
    dataBean.groupPersonCnt = List()..addAll(
      (map['groupPersonCnt'] as List ?? []).map((o) => GroupPersonCntBean.fromMap(o))
    );
    dataBean.InputActivateStatistics = InputActivateStatisticsBean.fromMap(map['InputActivateStatistics']);
    return dataBean;
  }

  Map toJson() => {
    "companyInfo": companyInfo,
    "groupPersonCnt": groupPersonCnt,
    "InputActivateStatistics": InputActivateStatistics,
  };
}

/// inputCnt : 6
/// activateCnt : 2
/// pics : "http://etpic.we17.com/test/20210224175307_6461.jpg,http://etpic.we17.com/test/20210301161137_1266.jpg,"

class InputActivateStatisticsBean {
  int inputCnt;
  int activateCnt;
  String pics;

  static InputActivateStatisticsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    InputActivateStatisticsBean inputActivateStatisticsBean = InputActivateStatisticsBean();
    inputActivateStatisticsBean.inputCnt = map['inputCnt'];
    inputActivateStatisticsBean.activateCnt = map['activateCnt'];
    inputActivateStatisticsBean.pics = map['pics'];
    return inputActivateStatisticsBean;
  }

  Map toJson() => {
    "inputCnt": inputCnt,
    "activateCnt": activateCnt,
    "pics": pics,
  };
}

/// groupName : "全部"
/// groupid : ""
/// cnt : 6
/// visitor : "01"

class GroupPersonCntBean {
  String groupName;
  String groupid;
  int cnt;
  String visitor;

  static GroupPersonCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GroupPersonCntBean groupPersonCntBean = GroupPersonCntBean();
    groupPersonCntBean.groupName = map['groupName'];
    groupPersonCntBean.groupid = map['groupid'];
    groupPersonCntBean.cnt = map['cnt'];
    groupPersonCntBean.visitor = map['visitor'];
    return groupPersonCntBean;
  }

  Map toJson() => {
    "groupName": groupName,
    "groupid": groupid,
    "cnt": cnt,
    "visitor": visitor,
  };
}

/// companyid : "787f1dd0a2a94129b6ff2bb027be45b9"
/// status : "00"
/// companyname : ""
/// companynick : "0204企业"
/// companylogo : null
/// type : "00"
/// city : "{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}"
/// gps : null
/// address : null
/// createuid : "4ab65f9a027040208efdbd9472e33ae7"
/// createdate : 1612428889
/// updatetime : 1612428889
/// updateuid : "4ab65f9a027040208efdbd9472e33ae7"
/// groupList : null

class CompanyInfoBean {
  String companyid;
  String status;
  String companyname;
  String companynick;
  dynamic companylogo;
  String type;
  String city;
  dynamic gps;
  dynamic address;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;
  dynamic groupList;

  static CompanyInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyInfoBean companyInfoBean = CompanyInfoBean();
    companyInfoBean.companyid = map['companyid'];
    companyInfoBean.status = map['status'];
    companyInfoBean.companyname = map['companyname'];
    companyInfoBean.companynick = map['companynick'];
    companyInfoBean.companylogo = map['companylogo'];
    companyInfoBean.type = map['type'];
    companyInfoBean.city = map['city'];
    companyInfoBean.gps = map['gps'];
    companyInfoBean.address = map['address'];
    companyInfoBean.createuid = map['createuid'];
    companyInfoBean.createdate = map['createdate'];
    companyInfoBean.updatetime = map['updatetime'];
    companyInfoBean.updateuid = map['updateuid'];
    companyInfoBean.groupList = map['groupList'];
    return companyInfoBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "status": status,
    "companyname": companyname,
    "companynick": companynick,
    "companylogo": companylogo,
    "type": type,
    "city": city,
    "gps": gps,
    "address": address,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "groupList": groupList,
  };
}
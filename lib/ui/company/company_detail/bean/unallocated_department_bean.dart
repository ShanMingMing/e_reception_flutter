/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg","name":"这是谁","napicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg"},{"nick":"","phone":"19747283242","putpicurl":"","name":"而非","napicurl":""},{"nick":"","phone":"13226332406","putpicurl":"","name":"超管","napicurl":""},{"nick":"","phone":"13313313309","putpicurl":"","name":"员工2","napicurl":""},{"nick":"","phone":"13313313306","putpicurl":"","name":"员工1","napicurl":""},{"nick":"","phone":"13313313305","putpicurl":"","name":"员工123","napicurl":""},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg","name":"赵丽颖","napicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg","name":"天仙","napicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg","name":"千玺","napicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg","name":"陈伟霆","napicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg"}],"total":11,"size":100,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class UnallocatedDepartmentBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static UnallocatedDepartmentBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UnallocatedDepartmentBean unallocatedDepartmentBeanBean = UnallocatedDepartmentBean();
    unallocatedDepartmentBeanBean.success = map['success'];
    unallocatedDepartmentBeanBean.code = map['code'];
    unallocatedDepartmentBeanBean.msg = map['msg'];
    unallocatedDepartmentBeanBean.data = DataBean.fromMap(map['data']);
    unallocatedDepartmentBeanBean.extra = map['extra'];
    return unallocatedDepartmentBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// page : {"records":[{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg","name":"这是谁","napicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg"},{"nick":"","phone":"19747283242","putpicurl":"","name":"而非","napicurl":""},{"nick":"","phone":"13226332406","putpicurl":"","name":"超管","napicurl":""},{"nick":"","phone":"13313313309","putpicurl":"","name":"员工2","napicurl":""},{"nick":"","phone":"13313313306","putpicurl":"","name":"员工1","napicurl":""},{"nick":"","phone":"13313313305","putpicurl":"","name":"员工123","napicurl":""},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg","name":"赵丽颖","napicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg","name":"天仙","napicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg","name":"千玺","napicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg","name":"陈伟霆","napicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg"}],"total":11,"size":100,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg","name":"这是谁","napicurl":"http://etpic.we17.com/test/20210416183855_6602.jpg"},{"nick":"","phone":"19747283242","putpicurl":"","name":"而非","napicurl":""},{"nick":"","phone":"13226332406","putpicurl":"","name":"超管","napicurl":""},{"nick":"","phone":"13313313309","putpicurl":"","name":"员工2","napicurl":""},{"nick":"","phone":"13313313306","putpicurl":"","name":"员工1","napicurl":""},{"nick":"","phone":"13313313305","putpicurl":"","name":"员工123","napicurl":""},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210331184533_8889.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg","name":"赵丽颖","napicurl":"http://etpic.we17.com/test/20210329135215_5571.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg","name":"天仙","napicurl":"http://etpic.we17.com/test/20210329135157_5859.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg","name":"千玺","napicurl":"http://etpic.we17.com/test/20210329135138_8610.jpg"},{"nick":"","phone":"","putpicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg","name":"陈伟霆","napicurl":"http://etpic.we17.com/test/20210329135051_4770.jpg"}]
/// total : 11
/// size : 100
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<RecordsBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => RecordsBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// nick : ""
/// phone : ""
/// putpicurl : "http://etpic.we17.com/test/20210416183855_6602.jpg"
/// name : "这是谁"
/// napicurl : "http://etpic.we17.com/test/20210416183855_6602.jpg"

class RecordsBean {
  String nick;
  String phone;
  String putpicurl;
  String name;
  String napicurl;
  bool isSelected;
  String fuid;
  String uname;

  static RecordsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RecordsBean recordsBean = RecordsBean();
    recordsBean.nick = map['nick'];
    recordsBean.phone = map['phone'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.name = map['name'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.isSelected = map['isSelected'];
    recordsBean.fuid = map['fuid'];
    recordsBean.uname = map['uname'];
    return recordsBean;
  }

  Map toJson() => {
    "nick": nick,
    "phone": phone,
    "putpicurl": putpicurl,
    "name": name,
    "napicurl": napicurl,
    "isSelected": isSelected,
    "fuid": fuid,
    "uname": uname,
  };
}
import 'dart:math';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_info_widget.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/utils/address_about_utils.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/cache_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/company_address_bean.dart';
import 'bean/company_branch_bean.dart';
import 'bean/independent_company_basic_info_bean.dart';
import 'company_address/company_create_or_edit_address_page.dart';
import 'company_basic_data_view_model.dart';
import 'company_branch/company_branch_detail_page.dart';
import 'company_branch/company_branch_detail_view_model.dart';
import 'company_branch/company_create_or_edit_branch_page.dart';
import 'company_detail_view_model.dart';
import 'edit_name_and_nick_page.dart';

class EnterpriseDetailPage extends StatefulWidget {

  // final ValueNotifier<CompanyRegisterUploadBean> onChanged;

  final CompanyBasicInfoBean companyBasicInfoBean;

  final String gps;

  const EnterpriseDetailPage({Key key, this.gps, this.companyBasicInfoBean}) : super(key: key);

  ///路由名称
  static const String ROUTER = "EnterpriseDetailPage";

  @override
  _EnterpriseDetailPageState createState() => _EnterpriseDetailPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,String gps, {CompanyBasicInfoBean itemBean}) {
    return RouterUtils.routeForFutureResult(
      context,
      EnterpriseDetailPage(gps:gps,companyBasicInfoBean: itemBean),
      routeName: EnterpriseDetailPage.ROUTER,
    );
  }

}

class _EnterpriseDetailPageState extends BaseState<EnterpriseDetailPage> {

  ///简称
  TextEditingController _editingNickController;
  TextEditingController _editingFullNameController;
  TextEditingController _editingAddressController;

  CompanyBasicInfoBean _companyInfoBean;

  CompanyBasicDataViewModel _viewModel;

  CommonListPageWidgetState mState;

  int addressNum = 0;

  // CompanyBranchListInfoBean addBean = CompanyBranchListInfoBean();

  static const String BRANCH_OFFICE_LIST_INFO =ServerApi.BASE_URL + "app/appComBranchList";

  @override
  void initState() {
    super.initState();
    _viewModel = CompanyBasicDataViewModel(this,widget?.gps);
    _editingNickController = TextEditingController()
      ..addListener(() => _notifyChange());
    _editingFullNameController = TextEditingController()
      ..addListener(() => _notifyChange());
    _initDate();
    _refreshInfo();
  }

  _initDate(){
    //获取新接口的公司数据
    _viewModel?.groupInfoCompanyBasic?.addListener(() {
      setState(() {
        if (_viewModel.groupInfoCompanyBasic.value != null) {
          _companyInfoBean = _viewModel?.groupInfoCompanyBasic?.value;
          _editingNickController.text =
              _companyInfoBean?.companynick ?? "";
          _editingFullNameController.text =
              _companyInfoBean?.companyname ?? "";
        }
      });
    });
  }

  _refreshInfo(){
    _viewModel.getCompanyBasicInfo();
    _viewModel.getBasicInfoCache();
  }

  ///通知更新
  _notifyChange() {
    if(_editingNickController?.text?.trim()!=null && _editingNickController?.text?.trim()!=""){
      if(!(_companyInfoBean?.companynick==_editingNickController?.text?.trim())){
        _companyInfoBean?.companynick = _editingNickController?.text?.trim();

      }
    }
    if( _editingFullNameController?.text?.trim()!=null && _editingFullNameController?.text?.trim()!=""){
      if(!(_companyInfoBean?.companyname==_editingFullNameController?.text?.trim())){
        _companyInfoBean?.companyname = _editingFullNameController?.text?.trim();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF191E31),
      body: Stack(
        children: [
          Column(
            children: [
              _toTopBarWidget(),
              _toEditWidget(),
              _toBranchTitleWidget(),
              // _toInfoWidget(),
              _companyBranchOfficeList()
            ],
          ),
          _toAddAddressButtonWidget(),
        ],
      ),
    );
  }



  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "基本资料",
      isShowBack: true,
    );
  }

  Widget _companyBranchOfficeList(){
    return Visibility(
      // visible: AppOverallDistinguish.comefromAiOrWeStudy(),
      child: Expanded(
        child: VgPlaceHolderStatusWidget(
          // emptyStatus: mState?.data!=null,
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: CommonListPageWidget<CompanyBranchListInfoBean,CompanyBranchBean>(
              enablePullUp: false,
              enablePullDown: true,
              netUrl: BRANCH_OFFICE_LIST_INFO,
              httpType: VgHttpType.post,
              queryMapFunc: (int page) => {
                "authId": UserRepository.getInstance().authId ?? "",
                // "gps":widget?.gps??"",
                "gps":"",
                "current":1,
                "size":20
              },
              parseDataFunc: (VgHttpResponse resp) {
                CompanyBranchBean bean = CompanyBranchBean.fromMap(resp.data);
                return bean;
              },
              listFunc: (List<CompanyBranchListInfoBean> branchList) {
                addressNum = branchList?.length??0;
                setState(() { });
                  return branchList;
              },
              stateFunc: (CommonListPageWidgetState state) {
                mState = state;
              },
              itemBuilder: (BuildContext context, int index,
                  CompanyBranchListInfoBean itemBean) {
                if (itemBean == null) return Container();
                return _buildItemWidget(itemBean, index, addressNum);
              },
              //分割器构造器
              separatorBuilder: (context, int index, _) {
                return Container(
                  height: 0.5,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Container(
                    color: Color(0xFF303546),
                  ),
                );
              },
              placeHolderFunc: (List<CompanyBranchListInfoBean> list, Widget child,
                  VoidCallback onRefresh) {
                return VgPlaceHolderStatusWidget(
                  emptyStatus: list == null || list.isEmpty,
                  emptyOnClick: () => onRefresh(),
                  child: child,
                );
              },
              cacheKey: BRANCH_OFFICE_LIST_INFO,
              needCache: true,
            ),
          ),
        ),
      ),
    );
  }

  Widget _toEditWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              if (StringUtils.isEmpty(widget.companyBasicInfoBean?.companylogo ?? "")) {
                if (!VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId())) {
                  return;
                }
                _chooseSinglePicAndClip();
                return;
              }
              CompanyPicDetailPage.navigatorPush(context,
                  url: widget.companyBasicInfoBean?.companylogo,
                  selectMode: SelectMode.Normal,
                  clipCompleteCallback: (path, cancelLoadingCallback) {
                    widget.companyBasicInfoBean?.companylogo = path;
                    setState(() {});
                    _viewModel.setCompanyPic(
                        context, widget.companyBasicInfoBean?.companylogo,
                        widget.companyBasicInfoBean?.companyid);
                  });
            },
            // onTap: () => StringUtils.isEmpty(widget.companyBasicInfoBean?.companylogo ?? "")
            //     ?_chooseSinglePicAndClip()
            //     :CompanyPicDetailPage.navigatorPush(context,
            //     url: widget.companyBasicInfoBean?.companylogo,
            //     selectMode: SelectMode.Normal,
            //     clipCompleteCallback: (path, cancelLoadingCallback) {
            //       widget.companyBasicInfoBean?.companylogo = path;
            //       setState(() {});
            //       _viewModel.setCompanyPic(context, widget.companyBasicInfoBean?.companylogo,
            //           widget.companyBasicInfoBean?.companyid);
            //     }),
            child: Container(
              height: 70,
              child: Row(
                children: <Widget>[
                  Container(
                    width: 56,
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                    child: Text(
                      "LOGO",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getTextMinorGreyColor_808388(),
                        fontSize: 14,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(color: Color(0xFF191E31)),
                          child: VgCacheNetWorkImage(
                            widget.companyBasicInfoBean?.companylogo ?? "",
                            imageQualityType: ImageQualityType.middleUp,
                            fit: BoxFit.contain,
                            errorWidget:
                            Image.asset("images/company_detail_card_info_logo_ico.png"),
                            placeWidget:
                            Image.asset("images/company_detail_card_info_logo_ico.png"),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 9,
                  ),
                  Opacity(
                    opacity: VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId()) ? 1 : 0,
                    child: Container(
                        padding: const EdgeInsets.only(right: 15, top: 20, bottom: 19),
                        child: Image.asset(
                          "images/index_arrow_ico.png",
                          width: 6,
                        )),
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              EditNameAndNickPage.navigatorPush(context,"${_type()}简称",_editingNickController,(){
                _notifyChange();
                _viewModel.setEditCompanyInfo(_companyInfoBean, context);
              });
            },
            child: _singleSelectWidget(
              controller: _editingNickController,
              title: "${_type()}简称",
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              EditNameAndNickPage.navigatorPush(context,"${_type()}全称",_editingFullNameController,(){
                _notifyChange();
                _viewModel.setEditCompanyInfo(_companyInfoBean, context);
              });
            },
            child: _singleSelectWidget(
              controller: _editingFullNameController,
              title: "${_type()}全称",
            ),
          ),
          Visibility(
            visible: false,
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                if(!AppOverallDistinguish.comefromAiOrWeStudy()){
                  return;
                }
                CompanyAddressListInfoBean item =  CompanyAddressListInfoBean();
                item = AddressAboutUtils.getCompanyAddressListInfoBeanNew();
                CompanyCreateOrEditAddressPage.navigatorPush(context,"${_type()}",item,widget?.gps,widget?.companyBasicInfoBean,(){
                }).then((value) {
                  if (value != null && value) setState(() { });
               });
              },
                child: Container(
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 56,
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                        child: Text(
                          "${_type()}地址",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: Text(
                            AddressAboutUtils.getAddressStr() == null ? "未设置" : AddressAboutUtils.getAddressStr()+"·${UserRepository.getInstance()?.userData?.companyInfo?.address}",
                            style: TextStyle(
                              fontSize: 14,
                              color: AddressAboutUtils.getAddressStr() == null
                                  ? ThemeRepository.getInstance().getHintGreenColor_5E687C()
                                  : ThemeRepository.getInstance().getTextColor_D0E0F7(),
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Container(
                        width: 9,
                      ),
                      Opacity(
                        opacity: VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId()) ? 1 : 0,
                        child: Container(
                            padding: const EdgeInsets.only(right: 15, top: 20, bottom: 19),
                            child: Image.asset(
                              "images/index_arrow_ico.png",
                              width: 6,
                            )),
                      )
                    ],
                  ),
                ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toBranchTitleWidget(){
    return Visibility(
      // visible: AppOverallDistinguish.comefromAiOrWeStudy(),
      child: Container(
        height: 40,
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(vertical: 11, horizontal: 15),
        child: Text(
          "地址管理",
          style: TextStyle(
            fontSize: 13,
            color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          ),
        ),
      ),
    );
  }

  Widget _toInfoWidget() {
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            child: Text(
              "地址管理 · ${addressNum}",
              style: TextStyle(
                color: ThemeRepository.getInstance()
                    .getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          if(false)
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 17, right: 15, bottom: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    "images/shai_xuan1.png",
                    height: 16,
                  ),
                  SizedBox(width: 4),
                  Text(
                    "筛选",
                    style: TextStyle(
                      fontSize: 12,
                      color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItemWidget(CompanyBranchListInfoBean itemBean, int index, int addressNum) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        // addBean = itemBean;
        // if(!AppOverallDistinguish.comefromAiOrWeStudy()){
        //   return;
        // }
        CompanyBranchDetailPage.navigatorPush(context, itemBean, addressNum).then((value) => mState?.viewModel?.refresh());
      },
      child: Column(
        children: [
          Container(
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            padding: EdgeInsets.symmetric(horizontal: 15,vertical: 12),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "${itemBean?.branchname}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                          ),
                        ),
                      ),
                    ),
                    Image.asset(itemBean?.powerOnCnt == 0
                        ? "images/branch_guanji.png"
                        : "images/branch_kaiji.png",
                      height: 12,
                    ),
                    SizedBox(width: 4),
                    Text(
                      "${itemBean?.powerOnCnt}",
                      style: TextStyle(
                        fontSize: 12,
                        color: itemBean?.powerOnCnt == 0
                            ? ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C()
                            : Color(0xFF00C6C4),
                      ),
                    ),
                    Text(
                      "/${itemBean?.allCnt}",
                      style: TextStyle(
                        fontSize: 12,
                        color: ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C(),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          _branchAddressStr(itemBean),
                          softWrap: true,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 12,
                            color: ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C(),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 3),
                      child: Image.asset(
                        "images/index_arrow_ico.png",
                        width: 6,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          if(index+1==addressNum)
            Container(
              height: 95,
            ),
        ],
      ),
    );
  }

  bool _updateIco(CompanyAddressListInfoBean itemBean){
    if(UserRepository.getInstance().getCurrentRoleId()=="99"){
      return true;
    }else{
      if(itemBean?.createuid == UserRepository.getInstance().userData.comUser.userid && UserRepository.getInstance().getCurrentRoleId()=="90"){
        return true;
      }
      return false;
    }
  }

  String _branchAddressStr(CompanyBranchListInfoBean itemBean){
    String addressStr = itemBean?.address;
    if(itemBean?.addrProvince == itemBean?.addrCity){
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrDistrict}·${itemBean?.address}";
    }else{
      addressStr = "${itemBean?.addrProvince}${itemBean?.addrCity}${itemBean?.addrDistrict}·${itemBean?.address}";
    }
    return addressStr;
  }

  Widget _toAddAddressButtonWidget() {
    return Visibility(
      visible: AppOverallDistinguish.comefromAiOrWeStudy(),
      child: Positioned(
        right: 0,
        left: 0,
        bottom: ScreenUtils.getBottomBarH(context) + 50,
        child: Center(
          child: CommonFixedHeightConfirmButtonWidget(
            isAlive: true,
            width: 132,
            height: 40,
            margin: const EdgeInsets.symmetric(horizontal: 27),
            unSelectBgColor: Color(0xFF3A3F50),
            selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 15,
            ),
            selectedTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 15,
              // fontWeight: FontWeight.w600
            ),
            text: "添加地址",
            textLeftSelectedWidget: Padding(
              padding: const EdgeInsets.only(bottom: 1),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            onTap: (){
              CompanyCreateOrEditBranchPage.navigatorPush(context,"新增",null,widget?.gps,_companyInfoBean?.companynick,
                      (){}).then((value) => mState?.viewModel?.refresh());
            },
          ),
        ),
      ),
    );
  }

  String _type(){
    if(_companyInfoBean?.type!=null){
      String registrationType = _companyInfoBean?.type;
      if(registrationType=="S02" || registrationType=="S03" || registrationType=="S06"){
        return registrationType = "企业"; //国有企业//民营企业//外资企业
      }
      else if(registrationType=="S04"){//党政机关
        return registrationType = "单位";
      }
      else if(registrationType=="S05"){//大中小学
        return registrationType = "学校";
      }
      else if(registrationType=="S01"){//培训机构
        return registrationType = "机构";
      }else{
        return registrationType = "企业";
      }
    }else{
      return "机构";
    }
  }

  void _chooseSinglePicAndClip() async {
    String fileUrl =
    await MatisseUtil.clipOneImage(context,
        scaleX: 1, scaleY: 1,
        isCanSelectRadio: true,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        }
    );
    if (fileUrl == null || fileUrl == "") {
      return null;
    }
    setState(() {
      widget.companyBasicInfoBean?.companylogo = fileUrl;
    });
    _viewModel.setCompanyPic(context, widget.companyBasicInfoBean?.companylogo,
        widget.companyBasicInfoBean?.companyid);
  }
}

class _singleSelectWidget extends StatefulWidget {
  final String title;

  final String line;

  final bool isShowGoIcon;

  final TextEditingController controller;

  const _singleSelectWidget(
      {Key key,
        this.title,
        this.isShowGoIcon = true,
        this.controller, this.line})
      : super(key: key);

  @override
  _singleSelectWidgetState createState() =>
      _singleSelectWidgetState();
}

class _singleSelectWidgetState extends State<_singleSelectWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        children: <Widget>[
          Container(
            width: 56,
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            child: Text(
              widget.title ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance()
                    .getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                "${widget?.controller?.text?.isEmpty?"未设置":widget?.controller?.text}",
                style: TextStyle(
                  fontSize: 14,
                  color: widget?.controller?.text?.isEmpty
                      ? ThemeRepository.getInstance().getHintGreenColor_5E687C()
                      : ThemeRepository.getInstance().getTextColor_D0E0F7(),
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Container(
            width: 9,
          ),
          Opacity(
            opacity: VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId()) ? 1 : 0,
            child: Container(
                padding: const EdgeInsets.only(right: 15, top: 20, bottom: 19),
                child: Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                )),
          )
        ],
      ),
    );
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_edit_callback_widget/common_edit_callback_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/department_or_class_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_viewmodel.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

///新增或编辑部门、班级页面
class AddOrEditClassDepartmentPage extends StatefulWidget {
  ///是否编辑
  final bool isEdit;

  ///页面类型
  final int type;

  final DepartmentOrClassListItemBean oldBean;

  static const String ROUTER = 'AddOrEditDepartmentPage';

  const AddOrEditClassDepartmentPage(
      {Key key, this.isEdit, this.type, this.oldBean})
      : super(key: key);

  ///跳转方法
  ///[type] 页面类型
  ///[isEdit] 编辑标志
  static Future<DepartmentOrClassListItemBean> navigatorPush(
      BuildContext context, int type, bool isEdit,
      {DepartmentOrClassListItemBean oldBean}) {
    return RouterUtils.routeForFutureResult<DepartmentOrClassListItemBean>(
      context,
      AddOrEditClassDepartmentPage(
        isEdit: isEdit ?? false,
        type: type,
        oldBean: oldBean,
      ),
      routeName: AddOrEditClassDepartmentPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return AddOrEditClassDepartmentPageState();
  }
}

class AddOrEditClassDepartmentPageState
    extends BaseState<AddOrEditClassDepartmentPage> {
  ClassOrDepartmentViewModel _viewModel;
  TextEditingController editingController;

  bool isLightUp = false;

  /// 输入文字
  String inputText;

  @override
  void initState() {
    _viewModel = ClassOrDepartmentViewModel(this);
    super.initState();
    editingController = TextEditingController();
    _viewModel.operateResultController.listen((t) {
      if (t != null) {
        VgEventBus.global.send(RefreshCompanyDetailPageEvent(msg: '编辑班级'));
        //新建或保存成功将值返回到上个页面
        RouterUtils.pop(context, result: t);
      }
    });
    if (widget.oldBean != null) {
      isLightUp = true;
      inputText=widget.oldBean.getName();
    }
    Future.delayed(Duration(milliseconds: 100))
        .then((value) {
      editingController.text = (widget.oldBean?.getName() ?? '');
      editingController.selection=TextSelection.fromPosition(TextPosition(
          affinity: TextAffinity.downstream,
          offset: editingController.text.length));
    }
    );
  }
  @override
  void onDispose(listener) {
    super.onDispose(listener);
  }

  @override
  Widget build(BuildContext context) {
    String title;
    if (widget.type == ClassOrDepartmentPage.TYPE_DEPARTMENT) {
      title = widget.isEdit ? "编辑部门" : "新增部门";
    } else {
      title = widget.isEdit ? "编辑班级" : "新增班级";
    }
    return CustomAnnotatedRegion(
      Scaffold(
        backgroundColor:
            ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        body: Column(
          children: <Widget>[
            VgTopBarWidget(
              title: title,
              isShowBack: true,
              rightWidget: _buttonWidget(),
            ),
            CommonEditCallbackWidget(
              iconWidth: 0,
              fontSize: 14,
              showIcon: false,
              textInputAction: TextInputAction.done,
              editingController: editingController,
              autoFocus: true,
              hintText:
                  "请输入${widget.type == ClassOrDepartmentPage.TYPE_DEPARTMENT ? '部门' : '班级'}名称",
              onChanged: (String text) {
                inputText = text;
                if (text == null || text == "" || text.trim().isEmpty)
                  isLightUp = false;
                else
                  isLightUp = true;
                setState(() {});
              },
              onSubmitted: (String text) {
                inputText = text;
                if (text == null || text == "" || text.trim().isEmpty)
                  isLightUp = false;
                else
                  isLightUp = true;
                setState(() {});
              },
            )
          ],
        ),
      ),
      navigationBarColor:
          ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  Widget _buttonWidget() {
    return Container(
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isLightUp,
        width: 48,
        height: 24,
        unSelectBgColor:
            ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
            fontWeight: FontWeight.w600),
        selectedTextStyle: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        text: "确定",
        onTap: () {
          //提交按钮
          // 触摸收起键盘
          FocusScope.of(context).requestFocus(FocusNode());
          if (isLightUp) {
            _viewModel.save(inputText);
          }
        },
      ),
    );
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_edit_callback_widget/common_edit_callback_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/department_or_class_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_viewmodel.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/member/add_member_viewmodel.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/member/duplicate_user_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

import 'add_member_commit_page.dart';

///部门/班级 添加成员
class AddMemberPage extends StatefulWidget {
  ///部门班级数据
  final DepartmentOrClassListItemBean oldBean;

  static const String ROUTER = 'AddMemberPage';

  const AddMemberPage({Key key, this.oldBean}) : super(key: key);

  ///跳转方法
  ///[bean] 班级或部门数据
  static Future<bool> navigatorPush(
      BuildContext context, DepartmentOrClassListItemBean bean) {
    return RouterUtils.routeForFutureResult<bool>(
      context,
      AddMemberPage(
        oldBean: bean,
      ),
      routeName: AddMemberPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return AddMemberPageState();
  }
}

class AddMemberPageState extends BaseState<AddMemberPage> {
  String inputText;

  bool isLightUp = false;
  AddMemberViewModel _viewModel;

  @override
  void initState() {
    _viewModel = AddMemberViewModel(this);
    super.initState();
    _viewModel.checkNameResultController.listen((t) async {
      if (t != null && t.length > 0) {
        DuplicateUserPage.navigatorPush(context, inputText, t, widget.oldBean);
      } else {
        bool result;
        if (widget.oldBean.isClass()) {
          result = await AddMemberFromSelectPage.navigatorPush(context,inputText,
              classid: widget.oldBean?.getId(), className: widget.oldBean?.getName(),type: "班级");
        } else if (widget.oldBean.isDepartment()) {
          result = await AddMemberFromSelectPage.navigatorPush(context,inputText,
              departmentId: widget.oldBean?.getId(),
              department: widget.oldBean?.getName(),type: "部门");
        }
        if(result!=null&&result){
          RouterUtils.pop(context,result: true);
        }
      }
    });
  }

  @override
  void onDispose(listener) {
    super.onDispose(listener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: <Widget>[
          VgTopBarWidget(
            title: '添加用户',
            isShowBack: true,
          ),
          CommonEditCallbackWidget(
            iconWidth: 0,
            fontSize: 14,
            showIcon: false,
            textInputAction: TextInputAction.done,
            hintText: "请输入姓名",
            onChanged: (String text) {
              inputText = text;
              if (text == null || text == "" || text.trim().isEmpty)
                isLightUp = false;
              else
                isLightUp = true;
              setState(() {});
            },
            onSubmitted: (String text) {
              inputText = text;
              if (text == null || text == "" || text.trim().isEmpty)
                isLightUp = false;
              else
                isLightUp = true;
              setState(() {});
            },
          ),
          _buttonWidget(),
        ],
      ),
    );
  }

  Widget _buttonWidget() {
    return Container(
      width: double.infinity,
      height: 40,
      margin: EdgeInsets.only(left: 12, right: 12, top: 30),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isLightUp,
        height: 40,
        unSelectBgColor:
            ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
            fontWeight: FontWeight.w600),
        selectedTextStyle: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        text: "确定",
        textSize: 15,
        onTap: () {
          //提交按钮
          // 触摸收起键盘
          FocusScope.of(context).requestFocus(FocusNode());
          if (isLightUp) {
            _viewModel.checkName(inputText);
          }
        },
      ),
    );
  }
}

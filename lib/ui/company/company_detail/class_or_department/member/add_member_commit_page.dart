import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_select_item_for_list_page/common_select_item_for_list_page.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/company/manager/edittext_widget.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/create_user_page_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/relative_stu/select_relative_stu_list_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_overall_location/user_branch_location.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/cityWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_address_attend_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/departmentWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/projectWidget.dart';
import 'package:e_reception_flutter/ui/mypage/edit_name_dialog_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/logo_detail_page.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

///添加用户提交页面
class AddMemberFromSelectPage extends StatefulWidget {
  static const String ROUTER = 'AddManagerFromSelectPage';

  // 从部门/班级页面添加 以下要锁死
  final String department;
  final String departmentId;
  final String classid;
  final String className;
  final String name;
  final String nick;
  final String type;

  AddMemberFromSelectPage(this.name,
      {this.nick,
      this.department,
      this.departmentId,
      this.classid,
      this.className, this.type});

  ///跳转方法
  static Future<bool> navigatorPush(BuildContext context, String name,
      {String nick,
      String department,
      String departmentId,
      String classid,
      String className,String type}) {
    return RouterUtils.routeForFutureResult<bool>(
      context,
      AddMemberFromSelectPage(
        name,
        nick: nick,
        department: department,
        departmentId: departmentId,
        classid: classid,
        className: className,
        type: type,
      ),
      routeName: AddMemberFromSelectPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return AddMemberFromSelectPageState();
  }
}

class AddMemberFromSelectPageState extends BaseState<AddMemberFromSelectPage> {
  ///保存notifier
  ValueNotifier<bool> _isAllowConfirmValueNotifier;

  ///编辑用户viewModel
  CreateUserPageViewModel _viewModel;

  ///待上传数据
  CreateUserUploadBean uploadBean = CreateUserUploadBean();

  ///正在加载
  bool isLoading;



  ///根据权限判断是否显示身份和终端
  bool isLastedIdentityAndTerminal;

  TextEditingController _nameController;

  TextEditingController _terminalController;
  TextEditingController _departmentController;
  TextEditingController _classController;

  ///是企业
  bool isCompany =
      UserRepository.getInstance().userData.companyInfo.isCompanyLike();

  ///锁定管理班级
  bool lockClass = false;

  ///锁定管理部门
  bool lockDepartment = false;

  ///锁定手机号
  bool lockPhone = true;

  bool isShowProjectAndCity = false;
  TextEditingController _addressController;
  UserBranchLocationUtil locationUtil;
  List<CompanyBranchListInfoBean> branchUserLocation;
  VgLocation _locationPlugin;
  @override
  void initState() {
    super.initState();
    _viewModel = CreateUserPageViewModel(this);
    _isAllowConfirmValueNotifier = ValueNotifier(false);

    _terminalController = TextEditingController(text: "");
    _departmentController = TextEditingController(text: "");
    _classController = TextEditingController(text: "");
    transferToUpdateData();
    setState(() { });
    branchUserLocation = List<CompanyBranchListInfoBean>();
    locationUtil = UserBranchLocationUtil(this);
    _addressController = TextEditingController(text: "");
    Future<String> latlng = VgLocationUtils.getCacheLatLngString();
    latlng.then((value){
      print("使用缓存gps:" + value);
      locationUtil.userLocationInfoGps(value);
    });
    _locationFirstGps();
  }

  void _locationFirstGps(){
    locationUtil?.valueNotifier?.addListener(() {
      branchUserLocation = locationUtil?.valueNotifier?.value;
      setState(() { });
      if((branchUserLocation?.length??0)==0){
        return;
      }
      _addressController?.text = branchUserLocation[0]?.getAddressStr()?.toString();
      uploadBean.cbid = branchUserLocation[0]?.cbid;
    });
    _locationPlugin = VgLocation();
    _locationPlugin.requestSingleLocationWithoutCheckPermission((value) {
      Future<String> latlng = VgLocationUtils.getCacheLatLngString();
      latlng.then((value){
        print("添加用户最新gps:" + value);
        locationUtil.userLocationInfoGps(value);
      });
    });
  }

  ///判断提交
  void checkCommit() {
    String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
    _isAllowConfirmValueNotifier.value = errorMsg == null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(child: _toPlaceHolderWidget()),
      ],
    );
  }

  Widget _toPlaceHolderWidget() {
    return VgPlaceHolderStatusWidget(
        child: Column(
      children: <Widget>[
        _toOneMineWidget(),
      ],
    ));
  }

  Widget _toOneMineWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[

          _toClassifyAndIdRowWidget(),
          Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
          _toNameAndPhoneRowWidget(),
          Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
          _parentRelativeStuWidget(),
          _toHideOrShowStaffWidget(),
          _toAddressWidget(),
          _toHideOrShowGradeWidget(),

        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      isShowBack: true,
      rightWidget: ValueListenableBuilder(
        valueListenable: _isAllowConfirmValueNotifier,
        builder: (BuildContext context, bool isAllowConfirm, Widget child) {
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () async {
              VgToolUtils.removeAllFocus(context);
              String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
              if (errorMsg != null && errorMsg.isNotEmpty) {
                _isAllowConfirmValueNotifier.value = false;
                VgToastUtils.toast(context, errorMsg);
                return;
              }
              _isAllowConfirmValueNotifier.value = true;
              _viewModel.saveUserHttp(context, uploadBean,showDialog: false);
            },
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: true,//isAllowConfirm ??
              width: 48,
              height: 24,
              unSelectBgColor:
                  ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              selectedBgColor:
                  ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              selectedTextStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              text: "保存",
            ),
          );
        },
      ),
    );
  }

  Widget _toNameAndPhoneRowWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(top: 10),
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                EditTextWidget(
                  title: "姓名",
                  hintText: "请输入",
                  isShowRedStar: true,
                  controller: _nameController,
                  initContent: _getNameAndNickSplitStr(),
                  readOnly: true,
                  // onTap: StringUtils.isEmpty(uploadBean.nick)
                  //     ? null
                  //     : (String editText, dynamic value) {
                  //         return EditNameDialogWidget.navigatorPushDialog(
                  //             context, uploadBean.name, uploadBean.nick);
                  //       },
                  onDecodeResult: (dynamic result) {
                    if (result == null || result is! Map) {
                      return null;
                    }
                    uploadBean.name = result[EDIT_NAME_AND_NICK_BY_NAME_KEY];
                    uploadBean.nick = result[EDIT_NAME_AND_NICK_BY_NICK_KEY];
                    _nameController.text = _getNameAndNickSplitStr();
                    return null;
                  },
                  onChanged: (String editText, dynamic value) {
                    uploadBean.name = editText;
                    uploadBean.nick = value;
                  },
                ),
                _toSplitLineWidget(),
                EditTextWidget(
                  title: "手机",
                  hintText: "请输入",
                  keyboardType: TextInputType.number,
                  readOnly: lockPhone ?? false,
                  autofocus: true,
                  inputFormatters: [
                    WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                    LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
                  ],
                  initContent: uploadBean.phone,
                  isShowRedStar: uploadBean.phoneRedStar(),
                  onChanged: (String editText, dynamic value) {
                    uploadBean.phone = editText;
                    checkCommit();
                  },
                ),
              ],
            ),
          ),
          SizedBox(
            width: 15,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              onClickUserLogo();
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: Container(
                width: 90,
                height: 90,
                child: StringUtils.isNotEmpty(uploadBean.headFaceUrl)
                    ? VgCacheNetWorkImage(
                        uploadBean.headFaceUrl,
                        imageQualityType: ImageQualityType.middleUp,
                        defaultErrorType: ImageErrorType.head,
                        defaultPlaceType: ImagePlaceType.head,
                      )
                    : Image.asset("images/create_user_head_ico.png"),
              ),
            ),
          ),
          SizedBox(width: 15),
        ],
      ),
    );
  }

  void onClickUserLogo() {
    FocusScope.of(context).requestFocus(FocusNode());
    if (StringUtils.isNotEmpty(uploadBean.headFaceUrl)) {
      LogoDetailPage.navigatorPush(context,
          url: uploadBean.headFaceUrl,
          selectMode: SelectMode.HeadBorder,
          clipCompleteCallback: (path, cancelLoadingCallback) {
            uploadBean.headFaceUrl = path;
            setState(() {});
          }
      );
      return;
    }
    addUserLogo();
  }

  void addUserLogo() {
    CommonMoreMenuDialog.navigatorPushDialog(context, {
      "上传图片": () async {
        String path = await ClipImageHeadBorderUtil.clipOneImage(context,
            scaleY: 1, scaleX: 1, maxAutoFinish: true,
            isShowHeadBorderWidget: true);
        if (path == null || path == "") {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      },
      "拍照": () async {
        String path = await FrontCameraHeadPage.navigatorPush(context);
        if (path == null || path.isEmpty) {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      }
    });
  }

  String _getNameAndNickSplitStr() {
    StringBuffer stringBuffer = StringBuffer();
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.name, 7,
          symbol: "..."));
    } else {
      stringBuffer.write(uploadBean.name);
    }
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(" / ");
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.nick, 7,
          symbol: "..."));
    }
    return stringBuffer.toString();
  }

  ///分割线
  Widget _toSplitLineWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 15),
      height: 0.5,
      child: Container(
        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
      ),
    );
  }

  ///转化成待上传的数据
  void transferToUpdateData() {
    GroupListBean groupListBean = GroupListBean();
    UserRepository.getInstance().userData.companyInfo?.groupList?.forEach((element){
      if(widget?.type == "部门" && element?.groupType == "04"){
        groupListBean = element;
      }else if(widget?.type == "班级" && element?.groupType == "02"){
        groupListBean = element;
      }
    });
    uploadBean
          ..nick = widget?.nick
          ..name = widget.name
          ..departmentId = widget?.departmentId
          ..departmentName = widget?.department
          ..classList = [ClassCourseListItemBean(widget?.className, widget.classid)] //所在班级
          ..identityType =
                  isCompany?CompanyAddUserEditInfoIdentityType.commonUser:CompanyAddUserEditInfoIdentityType.commonUser
          ..identityName =
              CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
                      '10')
                  .getTypeToStr()
      ..isManagerTerminal = false
      // ..groupBean = (UserRepository.getInstance().userData.companyInfo?.groupList?.firstWhere((element) => isCompany?element.isStaff():element.isStudent(),orElse:(){
      //   return ;
      // }))
      ..groupBean = groupListBean;
//添加管理员页面默认打开管理
    // 判断锁定部门
    lockDepartment = widget.department != null && widget.departmentId != null;

    //判断锁定班级
    lockClass = widget.classid != null && widget.className != null;

    //判断锁定手机号
    lockPhone = uploadBean?.phone?.isNotEmpty ?? false;



    checkCommit();
  }

  ///分类 家长不显示编号
  Widget _toClassifyAndIdRowWidget() {
    return Row(
      children: <Widget>[
        Expanded(
          child: EditTextWidget(
            title: "分类",
            hintText: "请选择",
            isShowRedStar: true,
            isShowGoIcon: false,
            initContent: uploadBean.groupBean.groupName,
            customWidget: (Widget titleWidget, Widget textFieldWidget,
                Widget leftWidget, Widget rightWidget) {
              return Row(
                children: <Widget>[
                  leftWidget,
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        if(uploadBean.groupBean!=null){
                          return;
                        }
                      GroupListBean groupListBean =
                      await CommonSelectItemForListPage.navigatorPush<
                          GroupListBean>(context,
                          title: "分类选择",
                          selectedItem: uploadBean.groupBean,
                          list: UserRepository.getInstance()
                              .userData
                              ?.companyInfo
                              ?.groupList,
                          getTextFunc: (GroupListBean groupItemBean) {
                            return groupItemBean?.groupName;
                          });
                      if (groupListBean == null) {
                        return;
                      }
                      uploadBean.groupBean = groupListBean;
                      setState(() {});
                    },
                      child: Row(
                        children: <Widget>[
                          titleWidget,
                          CommonConstraintMaxWidthWidget(
                            maxWidth: ScreenUtils.screenW(context) / 4,
                            child: Text(
                              showOriginEmptyStr(
                                      uploadBean.groupBean?.groupName) ??
                                  "请选择",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Color(0xFFD0E0F7),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          (uploadBean.groupBean?.isParent() ?? false)
                              ? Spacer()
                              : SizedBox(
                                  width: 0,
                                ),
                          // Padding(
                          //   padding: const EdgeInsets.only(
                          //       left: 15, right: 15, top: 2),
                          //   child: Image.asset(
                          //     "images/go_ico.png",
                          //     width: 6,
                          //     color: VgColors.INPUT_BG_COLOR,
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
        (uploadBean.groupBean?.isParent() ?? false)
            ? SizedBox(
                width: 1,
              )
            : Expanded(
                //家长不显示编号
                child: EditTextWidget(
                  title: "编号",
                  hintText: "员工号/学号等",
                  isShowRedStar: false,
                  isGoneRedStar: true,
                  onChanged: (String editText, dynamic value) {
                    uploadBean.userNumber = editText;
                  },
                ),
              )
      ],
    );
  }

  Widget _toDepartmentWidget() {
    return EditTextWidget(
      title: "部门",
      hintText: "请选择",
      isShowGoIcon: !lockDepartment,
      initContent: widget.department??'',
      onTap: (String editText, dynamic value) {
        if(lockDepartment){
          return null;
        }
        return DepartmentWidget.navigatorPush(
            context, uploadBean?.departmentId,fuid:uploadBean?.fuid,selectOnly: true);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.departmentId = value;
        uploadBean.departmentName = editText;
      },
    );
  }

  Widget _toAddressWidget() {
    return EditTextWidget(
      controller: _addressController,
      title: "刷脸地址",
      hintText: "请选择",
      isShowGoIcon: true,
      isShowRedStar: true,
      onTap: (String editText, dynamic value) {
        return AddUserAddressAttendPage.navigatorPush(
            context, branchUserLocation,"","",
            createEdit: true).then((value){
          if(value!=null && (branchUserLocation?.length??0)!=0){
            branchUserLocation = value;
            List<String> cbids = [];
            branchUserLocation?.forEach((element) {
              cbids?.add(element?.cbid);
            });
            uploadBean.cbid = VgStringUtils.getSplitStr(cbids, symbol: ",");
            if(cbids?.length==1){
              _addressController?.text = branchUserLocation[0]?.getAddressStr();
            }else{
              _addressController?.text = branchUserLocation[0]?.getAddressStr()+"等${value?.length}个地址";
            }

          }
        });
      },
    );
  }

  Widget _toProjectWidget() {
    return EditTextWidget(
      title: "项目",
      hintText: "请选择",
      isShowGoIcon: true,
      onTap: (String editText, dynamic value) {
        return ProjectWidget.navigatorPush(context, uploadBean?.projectId);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.projectId = value;
        uploadBean.projectName = editText;
      },
    );
  }

  Widget _toCityWidget() {
    return EditTextWidget(
      title: "城市",
      hintText: "请选择",
      isShowGoIcon: true,
      onTap: (String editText, dynamic value) {
        return CityWidget.navigatorPush(context, uploadBean?.cityId);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.cityId = editText;
        uploadBean.cityName = editText;
      },
    );
  }

  ///更多按钮
  Widget _toMoreButtonWidget() {
    return Offstage(
      offstage: isShowProjectAndCity,
      child: GestureDetector(
        onTap: () {
          VgToolUtils.removeAllFocus(context);
          isShowProjectAndCity = true;
          setState(() {});
        },
        child: Container(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          padding: const EdgeInsets.only(top: 13, bottom: 30),
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "更多信息",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 12,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: VgColors.INPUT_BG_COLOR,
                    size: 14,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///家长身份 显示关联学员接口
  _parentRelativeStuWidget() {
    return Visibility(
        visible: uploadBean.groupBean?.isParent() ?? false,
        child: EditTextWidget(
          title: "关联学员",
          hintText: "请选择",
          isShowGoIcon: true,
          isShowRedStar: false,
          readOnly: true,
          onTap: (String editText, dynamic value) {
            GroupListBean stuListBean = UserRepository.getInstance()
                .userData
                ?.companyInfo
                ?.groupList
                ?.firstWhere((element) => element.isStudent(), orElse: () {
              return null;
            });
            // //学员groupid
            // String groupid = stuListBean?.groupid ?? null;

            return SelectRelativeStuListPage.navigatorPush(
                context, uploadBean?.relativeStuIds, stuListBean,null);
          },
          onDecodeResult: (dynamic result) {
            if (result is! Map) {
              return null;
            }

            EditTextAndValue editTextAndValue = EditTextAndValue(
              editText: result['name'],
              value: result['ids'],
            );
            return editTextAndValue;
          },
          onChanged: (String editText, dynamic value) {
            // uploadBean.relativeStuIds = value;
          },
        ));
  }

  Widget _toHideOrShowStaffWidget() {
    return Offstage(
      offstage: !(uploadBean?.groupBean?.isStaff() ?? false),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _toDepartmentWidget(),
          Offstage(
            offstage: !isShowProjectAndCity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _toSplitLineWidget(),
                _toProjectWidget(),
                _toSplitLineWidget(),
                _toCityWidget(),
              ],
            ),
          ),
          // _toMoreButtonWidget(),
        ],
      ),
    );
  }

  ///是否隐藏和显示班级
  Widget _toHideOrShowGradeWidget() {
    return Offstage(
      offstage: !(uploadBean.groupBean?.isStudent() ?? false),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // _toCourseWidget(),
          // _toSplitLineWidget(),
          _toGradeWidget(),
        ],
      ),
    );
  }

  Widget _toGradeWidget() {
    return EditTextWidget(
      title: "班级",
      hintText: "请选择",
      initContent: widget.className??'',
      isShowGoIcon: !lockClass,
      onTap: (String editText, dynamic value) {
        if(lockClass){
          return null;
        }
        return ClassCourseWidget.navigatorPush(context, uploadBean.classList);
      },
      onDecodeResult: (dynamic result) {
        if (result is! List<ClassCourseListItemBean>) {
          return null;
        }
        return EditTextAndValue(
            value: result,
            editText: (result?.length ?? 0) <= 0 ? "" : "${result.length}个班级");
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.classList = value;
      },
    );
  }
}

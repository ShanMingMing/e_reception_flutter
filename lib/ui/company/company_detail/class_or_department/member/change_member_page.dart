import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/company/manager/edittext_widget.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_info_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/edit_user_page_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/cityWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/add_and_remove_user_branch_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_address_attend_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/departmentWidget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/projectWidget.dart';
import 'package:e_reception_flutter/ui/mypage/edit_name_dialog_widget.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_group_widget.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/logo_detail_page.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../class_or_department_detail.dart';

///添加用户提交页面
class ChangeMemberFromSelectPage extends StatefulWidget {
  static const String ROUTER = 'AddManagerFromSelectPage';

  // 从部门/班级页面添加 以下要锁死
  final String department;
  final String departmentId;
  final String classid;
  final String className;
  final String nick;
  final String fuid;
  final String fid;

  ChangeMemberFromSelectPage(
      this.fuid,
      this.fid,
      {this.nick,
      this.department,
      this.departmentId,
      this.classid,
      this.className});

  ///跳转方法
  static Future<bool> navigatorPush(
      BuildContext context, String name, String fuid, String fid,
      {String nick,
      String department,
      String departmentId,
      String classid,
      String className}) {
    return RouterUtils.routeForFutureResult<bool>(
      context,
      ChangeMemberFromSelectPage(
        fuid,
        fid,
        nick: nick,
        department: department,
        departmentId: departmentId,
        classid: classid,
        className: className,
      ),
      routeName: ChangeMemberFromSelectPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return ChangeMemberFromSelectPageState();
  }
}

class ChangeMemberFromSelectPageState
    extends BaseState<ChangeMemberFromSelectPage> {
  ///保存notifier
  ValueNotifier<bool> _isAllowConfirmValueNotifier;

  ///编辑用户viewModel
  EditUserPageViewModel _viewModel;

  ///正在加载
  bool isLoading;

  ///根据权限判断是否显示身份和终端
  bool isLastedIdentityAndTerminal;

  TextEditingController _nameController;
  TextEditingController _numController;
  TextEditingController _phoneController;
  TextEditingController _terminalController;
  TextEditingController _departmentController;
  TextEditingController _classController;

  ///是企业
  bool isCompany =
      UserRepository.getInstance().userData.companyInfo.isCompanyLike();

  ///锁定管理班级
  bool lockClass = false;

  ///锁定管理部门
  bool lockDepartment = false;

  ///锁定手机号
  bool lockPhone = true;

  bool isShowProjectAndCity = false;

  ValueNotifier<String> _updateNameValueNotifier;

  EditUserUploadBean uploadBean;

  EditUserInfoDataBean infoDataBean;
  //刷脸地址相关
  List<CompanyBranchListInfoBean> listBranchBean;
  List<String> fuserAddressNameList = new List<String>();
  String fuserAddressNames = "";
  List<FuserBranchListBean> fuserBranchList;
  @override
  void initState() {
    super.initState();
    fuserBranchList = List<FuserBranchListBean>();
    _viewModel = EditUserPageViewModel(this);
    _isAllowConfirmValueNotifier = ValueNotifier(false);

    _terminalController = TextEditingController(text: "");
    _departmentController = TextEditingController(text: "");
    _classController = TextEditingController(text: "");
    _viewModel = EditUserPageViewModel(this);
    _updateNameValueNotifier = ValueNotifier(null);
    _isAllowConfirmValueNotifier = ValueNotifier(false);
    uploadBean = EditUserUploadBean();
    //变化监听
    // _mUploadBean.addListener(_notifyValue);
    _viewModel.getUserInfoHttp(widget.fuid);
    _nameController=TextEditingController(text: "");
    _numController=TextEditingController(text: "");
    _phoneController=TextEditingController(text: "");
    _viewModel.userInfoValueNotifier.addListener(_refreshInfo);
  }

  void _refreshInfo() {
    infoDataBean = _viewModel.userInfoValueNotifier.value;
    listBranchBean = List<CompanyBranchListInfoBean>();
    if(fuserAddressNameList != null){
      fuserAddressNameList.clear();
    }else{
      fuserAddressNameList = new List<String>();
    }
    fuserBranchList = infoDataBean?.fuserBranchList;
    fuserBranchList?.forEach((element) {
      CompanyBranchListInfoBean addbean = CompanyBranchListInfoBean();
      addbean?.cbid = element?.cbid;
      addbean?.address = element?.address;
      addbean?.selected = true;
      addbean?.addrProvince = element?.addrProvince;
      addbean?.addrCity = element?.addrCity;
      addbean?.addrDistrict = element?.addrDistrict;
      addbean?.branchname = element?.branchname;
      listBranchBean?.add(addbean);
      fuserAddressNameList?.add(element?.getAddressStr());
    });
    // fuserAddressNames = _addressStr(fuserAddressNameList);
    fuserAddressNames = VgStringUtils?.getMultipleStr(fuserAddressNameList);
    //赋值初始化
    uploadBean
      ..fid = ''
      ..fuid = widget.fuid
      ..userid = infoDataBean?.userid
      ..nick = infoDataBean?.nick
      ..name = infoDataBean?.name
      ..phone = infoDataBean?.phone
      ..cityId = infoDataBean?.city
      ..cityName = infoDataBean?.city
      ..projectId = infoDataBean?.projectid
      ..projectName = infoDataBean?.projectname
      ..departmentId = infoDataBean?.departmentid
      ..departmentName = infoDataBean?.department
      ..gradeId = (infoDataBean?.classInfo
              ?.map((e) => e.classid)
              ?.toList()
              ?.join(',')) ??
          '' //所在班级id
      ..gradeName = (infoDataBean?.classInfo
              ?.map((e) => e.classname)
              ?.toList()
              ?.join('、')) ??
          '' //所在班级名
      ..classList = infoDataBean?.classInfo
          ?.map((e) => ClassCourseListItemBean(e.classname, e.classid))
          ?.toList() //所在班级
      ..userNumber = infoDataBean?.number
      ..identityType = CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
          infoDataBean?.roleid)
      ..identityName = CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
              infoDataBean?.roleid)
          .getTypeToStr()
      ..isManagerTerminal = VgRoleUtils.isSuperAdmin(infoDataBean?.roleid) ||
          VgRoleUtils.isCommonAdmin(infoDataBean?.roleid)
      ..headFaceUrl = VgStringUtils.getFirstNotEmptyStrByList([
        infoDataBean?.putpicurl,
        infoDataBean?.napicurl,
      ])
      ..terminalList = infoDataBean?.hsnInfo?.map((e) {
        return CompanyChooseTerminalListItemBean()
          ..hsn = e?.hsn
          ..terminalName = e?.terminalName;
      })?.toList()
      ..classIdList = infoDataBean.MClassInfo.map((e) => e.classid).toList()
      ..managerClassName =
          infoDataBean.MClassInfo.map((e) => e.classname).toList().join(',')
      ..managerDepartmentName =
          infoDataBean.MDepartmentInfo.map((e) => e.department)
              .toList()
              .join(',')
      ..departmentList =
          infoDataBean.MDepartmentInfo.map((e) => e.departmentid).toList()
      ..branchName = fuserAddressNames
      ..listBranchBean = listBranchBean
      ..addCbids = ""
      ..removeCbids = ""
      ..groupBean = (GroupListBean()
        ..groupType = infoDataBean?.groupType
        ..groupid = infoDataBean?.groupid
        ..groupName = infoDataBean?.groupName
      );
    // 判断锁定部门
    lockDepartment = widget.department != null && widget.departmentId != null;
    List<String> departmentNames =
    infoDataBean.MDepartmentInfo.map((e) => e.department).toList();
    if (lockDepartment) {
      uploadBean..departmentId = widget.departmentId;
      if (!uploadBean.departmentList.contains(widget.departmentId)) {
        uploadBean.departmentList.add(widget.departmentId);
        departmentNames.add(widget.department);
      }
    }
    uploadBean..managerDepartmentName = departmentNames.join('、');

    //判断锁定班级
    lockClass = widget.classid != null && widget.className != null;
    List<String> classNames =
    infoDataBean.MClassInfo.map((e) => e.classname).toList();
    if (lockClass) {
      if (!uploadBean.classIdList.contains(widget.classid)) {
        uploadBean.classIdList.add(widget.classid);
        classNames.add(widget.className);
      }
    }
    uploadBean..managerClassName = classNames.join('、');

    //判断锁定手机号
    lockPhone=uploadBean?.phone?.isNotEmpty??false;
    _nameController.text=uploadBean.name;
    _numController?.text=uploadBean?.userNumber??"";
    _phoneController?.text = uploadBean?.phone??"";
    Future.delayed(Duration(milliseconds: 0), () {
      setState(() {});
    });
  }

  ///判断提交
  void checkCommit() {
    // String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
    // _isAllowConfirmValueNotifier.value = errorMsg == null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(child: _toPlaceHolderWidget()),
      ],
    );
  }

  Widget _toPlaceHolderWidget() {
    return VgPlaceHolderStatusWidget(
        child: Column(
      children: <Widget>[
        _toOneMineWidget(),
      ],
    ));
  }

  Widget _toOneMineWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _toClassifyAndIdRowWidget(),
          Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
          _toNameAndPhoneRowWidget(),
          Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
          // _parentRelativeStuWidget(),
          // _toHideOrShowStaffWidget(),
          _toFaceAddressWidget(),
          _toHideOrShowGradeWidget(),
        ],
      ),
    );
  }

  bool _whetherSuperAdministrator() {//多处加此判断，以防后期变动
    //如果登录人是管理员只能查看自己管理的部门员工信息并更改
    if (UserRepository.getInstance()?.userData?.comUser?.roleid == "90") {
      if (infoDataBean?.roleid != "10") {
        if(UserRepository.getInstance()?.userData?.comUser?.fuid == widget?.fuid){
          return false;
        }else return true;
      } else {
        return false;
      }
    }
    //如果登录人是超级管理员可以更改所有人
    if (UserRepository.getInstance()?.userData?.comUser?.roleid == "99") {
      return false;
    } else {
      return true;
    }
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      isShowBack: true,
      rightWidget: ValueListenableBuilder(
        valueListenable: _isAllowConfirmValueNotifier,
        builder: (BuildContext context, bool isAllowConfirm, Widget child) {
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () async {
              VgToolUtils.removeAllFocus(context);
              String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
              if (errorMsg != null && errorMsg.isNotEmpty) {
                _isAllowConfirmValueNotifier.value = false;
                VgToastUtils.toast(context, errorMsg);
                return;
              }
              _isAllowConfirmValueNotifier.value = true;
              _viewModel.saveUserHttp(context, uploadBean,infoDataBean?.groupid, onSuccess: (){
                RouterUtils.popUntil(context, ClassOrDepartmentDetail.ROUTER);
              });
            },
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: true,//isAllowConfirm
              width: 48,
              height: 24,
              unSelectBgColor:
                  ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              selectedBgColor:
                  ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              selectedTextStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
              text: "保存",
            ),
          );
        },
      ),
    );
  }

  Widget _toNameAndPhoneRowWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(top: 10),
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                EditTextWidget(
                  title: "姓名",
                  hintText: "请输入",
                  isShowRedStar: true,
                  controller: _nameController,
                  initContent: _getNameAndNickSplitStr(),
                  readOnly: true,
                  // onTap: StringUtils.isEmpty(uploadBean.nick)
                  //     ? null
                  //     : (String editText, dynamic value) {
                  //         return EditNameDialogWidget.navigatorPushDialog(
                  //             context, uploadBean.name, uploadBean.nick);
                  //       },
                  onDecodeResult: (dynamic result) {
                    if (result == null || result is! Map) {
                      return null;
                    }
                    uploadBean.name = result[EDIT_NAME_AND_NICK_BY_NAME_KEY];
                    uploadBean.nick = result[EDIT_NAME_AND_NICK_BY_NICK_KEY];
                    _nameController.text = _getNameAndNickSplitStr();
                    return null;
                  },
                  onChanged: (String editText, dynamic value) {
                    uploadBean.name = editText;
                    uploadBean.nick = value;
                  },
                ),
                _toSplitLineWidget(),
                EditTextWidget(
                  title: "手机",
                  hintText: "请输入",
                  controller: _phoneController,
                  keyboardType: TextInputType.number,
                  readOnly: lockPhone ?? false,
                  initValue: uploadBean.phone,
                  inputFormatters: [
                    WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                    LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
                  ],
                  initContent: uploadBean.phone,
                  isShowRedStar: uploadBean.phoneRedStar(),
                  onChanged: (String editText, dynamic value) {
                    uploadBean.phone = editText;
                    checkCommit();
                  },
                ),
              ],
            ),
          ),
          SizedBox(
            width: 15,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              onClickUserLogo();
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: Container(
                width: 90,
                height: 90,
                child: StringUtils.isNotEmpty(uploadBean.headFaceUrl)
                    ? VgCacheNetWorkImage(
                        uploadBean.headFaceUrl,
                        imageQualityType: ImageQualityType.middleUp,
                        defaultErrorType: ImageErrorType.head,
                        defaultPlaceType: ImagePlaceType.head,
                      )
                    : Image.asset("images/create_user_head_ico.png"),
              ),
            ),
          ),
          SizedBox(width: 15),
        ],
      ),
    );
  }


  void onClickUserLogo() {
    FocusScope.of(context).requestFocus(FocusNode());
    if (StringUtils.isNotEmpty(uploadBean.headFaceUrl)) {
      LogoDetailPage.navigatorPush(context,
          url: uploadBean.headFaceUrl,
          selectMode: SelectMode.HeadBorder,
          clipCompleteCallback: (path, cancelLoadingCallback) {
            uploadBean.headFaceUrl = path;
            setState(() {});
          }
      );
      return;
    }
    addUserLogo();
  }

  void addUserLogo() {
    CommonMoreMenuDialog.navigatorPushDialog(context, {
      "上传图片": () async {
        String path = await ClipImageHeadBorderUtil.clipOneImage(context,
            scaleY: 1, scaleX: 1, maxAutoFinish: true,
            isShowHeadBorderWidget: true);
        if (path == null || path == "") {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      },
      "拍照": () async {
        String path = await FrontCameraHeadPage.navigatorPush(context);
        if (path == null || path.isEmpty) {
          return;
        }
        uploadBean.headFaceUrl = path;
        setState(() {});
      }
    });
  }

  String _getNameAndNickSplitStr() {
    StringBuffer stringBuffer = StringBuffer();
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.name, 7,
          symbol: "..."));
    } else {
      stringBuffer.write(uploadBean.name);
    }
    if (StringUtils.isNotEmpty(uploadBean.nick)) {
      stringBuffer.write(" / ");
      stringBuffer.write(VgStringUtils.subStringAndAppendSymbol(
          uploadBean.nick, 7,
          symbol: "..."));
    }
    return stringBuffer.toString();
  }

  ///分割线
  Widget _toSplitLineWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 15),
      height: 0.5,
      child: Container(
        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
      ),
    );
  }



  ///分类 家长不显示编号
  Widget _toClassifyAndIdRowWidget() {
    return Row(
      children: <Widget>[
        Expanded(
          child: EditTextWidget(
            title: "分类",
            hintText: "请选择",
            isShowRedStar: true,
            isShowGoIcon: false,
            customWidget: (Widget titleWidget, Widget textFieldWidget,
                Widget leftWidget, Widget rightWidget) {
              return Row(
                children: <Widget>[
                  leftWidget,
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      // onTap: () async {
                      //   GroupListBean groupListBean =
                      //       await CommonSelectItemForListPage.navigatorPush<
                      //               GroupListBean>(context,
                      //           title: "分类选择",
                      //           selectedItem: uploadBean.groupBean,
                      //           list: UserRepository.getInstance()
                      //               .userData
                      //               ?.companyInfo
                      //               ?.groupList,
                      //           getTextFunc: (GroupListBean groupItemBean) {
                      //     return groupItemBean?.groupName;
                      //   });
                      //   if (groupListBean == null) {
                      //     return;
                      //   }
                      //   uploadBean.groupBean = groupListBean;
                      //   setState(() {});
                      // },
                      child: Row(
                        children: <Widget>[
                          titleWidget,
                          CommonConstraintMaxWidthWidget(
                            maxWidth: ScreenUtils.screenW(context) / 4,
                            child: Text(
                              showOriginEmptyStr(
                                      uploadBean.groupBean?.groupName) ??
                                  "请选择",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Color(0xFFD0E0F7),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          (uploadBean.groupBean?.isParent() ?? false)
                              ? Spacer()
                              : SizedBox(
                                  width: 0,
                                ),
                          // Padding(
                          //   padding: const EdgeInsets.only(
                          //       left: 15, right: 15, top: 2),
                          //   child: Image.asset(
                          //     "images/go_ico.png",
                          //     width: 6,
                          //     color: VgColors.INPUT_BG_COLOR,
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
        (uploadBean.groupBean?.isParent() ?? false)
            ? SizedBox(
                width: 1,
              )
            : Expanded(
                //家长不显示编号
                child: EditTextWidget(
                  title: "编号",
                  hintText: "员工号/学号等",
                  isShowRedStar: false,
                  isGoneRedStar: true,
                  controller: _numController,
                  initContent: uploadBean?.userNumber ?? "",
                  initValue: uploadBean.userNumber,
                  onChanged: (String editText, dynamic value) {
                    uploadBean.userNumber = editText;
                  },
                ),
              )
      ],
    );
  }

  Widget _toDepartmentWidget() {
    return EditTextWidget(
      title: "部门",
      hintText: "请选择",
      isShowGoIcon: !lockDepartment,
      initContent: widget.department ?? '',
      onTap: (String editText, dynamic value) {
        if (lockDepartment) {
          return null;
        }
        return DepartmentWidget.navigatorPush(
            context, uploadBean?.departmentId,fuid:uploadBean?.fuid);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.departmentId = value;
        uploadBean.departmentName = editText;
      },
    );
  }

  Widget _toProjectWidget() {
    return EditTextWidget(
      title: "项目",
      hintText: "请选择",
      isShowGoIcon: true,
      onTap: (String editText, dynamic value) {
        return ProjectWidget.navigatorPush(context, uploadBean?.projectId);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.projectId = value;
        uploadBean.projectName = editText;
      },
    );
  }

  Widget _toCityWidget() {
    return EditTextWidget(
      title: "城市",
      hintText: "请选择",
      isShowGoIcon: true,
      onTap: (String editText, dynamic value) {
        return CityWidget.navigatorPush(context, uploadBean?.cityId);
      },
      onDecodeResult: (dynamic result) {
        if (result is! EditTextAndValue) {
          return null;
        }
        return result;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.cityId = editText;
        uploadBean.cityName = editText;
      },
    );
  }

  ///更多按钮
  Widget _toMoreButtonWidget() {
    return Offstage(
      offstage: isShowProjectAndCity,
      child: GestureDetector(
        onTap: () {
          VgToolUtils.removeAllFocus(context);
          isShowProjectAndCity = true;
          setState(() {});
        },
        child: Container(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          padding: const EdgeInsets.only(top: 13, bottom: 30),
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "更多信息",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 12,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: VgColors.INPUT_BG_COLOR,
                    size: 14,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///家长身份 显示关联学员接口
  // _parentRelativeStuWidget() {
  //   return Visibility(
  //       visible: uploadBean.groupBean?.isParent() ?? false,
  //       child: EditTextWidget(
  //         title: "关联学员",
  //         hintText: "请选择",
  //         isShowGoIcon: true,
  //         isShowRedStar: false,
  //         readOnly: true,
  //         onTap: (String editText, dynamic value) {
  //           GroupListBean stuListBean = UserRepository.getInstance()
  //               .userData
  //               ?.companyInfo
  //               ?.groupList
  //               ?.firstWhere((element) => element.isStudent(), orElse: () {
  //             return null;
  //           });
  //           //学员groupid
  //           String groupid = stuListBean?.groupid ?? null;
  //
  //           return SelectRelativeStuListPage.navigatorPush(
  //               context, uploadBean?.relativeStuIds, groupid);
  //         },
  //         onDecodeResult: (dynamic result) {
  //           if (result is! Map) {
  //             return null;
  //           }
  //
  //           EditTextAndValue editTextAndValue = EditTextAndValue(
  //             editText: result['name'],
  //             value: result['ids'],
  //           );
  //           return editTextAndValue;
  //         },
  //         onChanged: (String editText, dynamic value) {
  //           // uploadBean.relativeStuIds = value;
  //         },
  //       ));
  // }

  Widget _toFaceAddressWidget(){
    if(AppOverallDistinguish.comefromAiOrWeStudy() && UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition())
     return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            if(!AppOverallDistinguish.comefromAiOrWeStudy()){
              return null;
            }
            if (_whetherSuperAdministrator()) return null;
            if (StringUtils.isEmpty(widget.fuid)) return null;
            AddAndRemoveUserBranchPage.navigatorPush(
                context, listBranchBean,
                widget?.fuid, widget?.fid,
                createEdit: true).then((value){
              AddRemoveAddressData data = value;
              if ((data?.addList?.isEmpty ?? true) && (data?.removeList?.isEmpty ?? true)) return;
              List<String> addCbids = [];
              data?.addList?.forEach((element) {
                if(element?.editSelected ?? false){
                  addCbids?.add(element?.cbid);
                }
              });
              List<String> removeCbids = [];
              data?.removeList?.forEach((element) {
                if(element?.selected == false){
                  removeCbids?.add(element?.cbid);
                }
              });
              uploadBean.addCbids = VgStringUtils.getSplitStr(addCbids, symbol: ",");
              uploadBean?.removeCbids = VgStringUtils.getSplitStr(removeCbids, symbol: ",");

            });
          },
          child: PersonDetailGroupWidget(
            title: "刷脸地址",
            content: fuserAddressNames,
            isEmptyHide: true,
            showSplit: false,
          ));
    return null;
  }

  Widget _toHideOrShowStaffWidget() {
    return Offstage(
      offstage: !(uploadBean.groupBean?.isStaff() ?? false),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _toDepartmentWidget(),
          Offstage(
            offstage: !isShowProjectAndCity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _toSplitLineWidget(),
                _toProjectWidget(),
                _toSplitLineWidget(),
                _toCityWidget(),
              ],
            ),
          ),
          _toMoreButtonWidget(),
        ],
      ),
    );
  }

  ///是否隐藏和显示班级
  Widget _toHideOrShowGradeWidget() {
    return Offstage(
      offstage: !(uploadBean.groupBean?.isStudent() ?? false),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // _toCourseWidget(),
          // _toSplitLineWidget(),
          _toGradeWidget(),
        ],
      ),
    );
  }

  Widget _toGradeWidget() {
    return EditTextWidget(
      title: "班级",
      hintText: "请选择",
      initContent: widget.className ?? '',
      isShowGoIcon: !lockClass,
      onTap: (String editText, dynamic value) {
        if (lockClass) {
          return null;
        }
        return ClassCourseWidget.navigatorPush(context, uploadBean.classList);
      },
      onDecodeResult: (dynamic result) {
        if (result is! List<ClassCourseListItemBean>) {
          return null;
        }
        return EditTextAndValue(
            value: result,
            editText: (result?.length ?? 0) <= 0 ? "" : "${result.length}个班级");
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.classList = value;
      },
    );
  }
}

import 'dart:async';

import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/common_person_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/duplicate_user_response.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_stream_lib.dart';

class AddMemberViewModel extends BaseViewModel {
  VgStreamController<List<CommonPersonBean>> checkNameResultController;

  AddMemberViewModel(BaseState<StatefulWidget> state) : super(state) {
    checkNameResultController = newAutoReleaseBroadcast();
  }

  ///校验姓名
  void checkName(String name) {
    // checkNameResultController.add([]);
    //
    // return;
    loading(true);

    VgHttpUtils.post(NetApi.DEPARTMENT_OR_CLASS_USER_CHECK,
        params: {'authId': UserRepository.getInstance().authId, 'name': name},
        callback: BaseCallback(onSuccess: (jsonMap) {
          loading(false);

          DuplicateUserResponse response =
              DuplicateUserResponse.fromMap(jsonMap);
          if (response.data != null &&
              response.data.sameNameUser != null &&
              response.data.sameNameUser.length > 0) {
            //存在重复的用户
            checkNameResultController.add(response.data.sameNameUser);
          } else {
            checkNameResultController.add([]);
          }
        }, onError: (e) {
          loading(false);
          toast(e);
        }));
  }
}

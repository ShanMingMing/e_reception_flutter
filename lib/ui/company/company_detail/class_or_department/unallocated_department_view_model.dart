import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/unallocated_department_bean.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_stream_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../../app_main.dart';

class UnallocatedDepartmentStaffViewModel extends BaseViewModel {

  ///详情页面数据结果
  VgStreamController<UnallocatedDepartmentBean> departmentValueStream;
  UnallocatedDepartmentStaffViewModel(BaseState<StatefulWidget> state) : super(state){
    departmentValueStream = newAutoReleaseBroadcast();
  }

//  App未分配 部门员工/班级学员
  void unallocatedUser(
      BuildContext context,
      String type
      ) {
    if (StringUtils.isEmpty(type)) {
      VgToastUtils.toast(context, "个人信息获取失败");
      return;
    }
    type == "0"?type ="00":type ="01";
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appUnallocatedUser",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current":1,
          "size":1000,
          "type":type??"00"
        },
        callback: BaseCallback(onSuccess: (val) {
          UnallocatedDepartmentBean unallocatedDepartmentBean = UnallocatedDepartmentBean.fromMap(val);
          departmentValueStream.add(unallocatedDepartmentBean);
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

}

class BatchAppUpdateChooseInfoRefreshEvent {}
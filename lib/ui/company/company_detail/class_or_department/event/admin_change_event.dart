class AdminChangeEvent {
  String message;

  AdminChangeEvent(this.message){
    print('发送${message??'AdminChangeEvent'}');
  }

  @override
  String toString() {
    return "收到${message??'AdminChangeEvent'}事件";
  }
}
import 'dart:convert';

import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/play_file_update_event.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/cache_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';
import 'bean/company_address_bean.dart';
import 'bean/independent_company_basic_info_bean.dart';
import 'event/refresh_company_detail_page_event.dart';

class CompanyBasicDataViewModel extends BaseViewModel{
  ValueNotifier<CompanyBasicInfoBean> groupInfoCompanyBasic;
  final String gps;
  CompanyBasicDataViewModel(BaseState<StatefulWidget> state, this.gps) : super(state){
    groupInfoCompanyBasic = ValueNotifier(null);
  }
  static const String TOP_COMPANY_INFO_API =
     ServerApi.BASE_URL + "app/appCompanyBasicInfo";

  static const String SET_COMPANY_INFO_API =
     ServerApi.BASE_URL + "app/appEditCompany";

  static const String REMOVE_COMPANY_INFO_ADDRESS_API =
     ServerApi.BASE_URL + "app/appRemoveComAddress";


  static const String SET_COMPANY_INFO_ADDRESS_API =
     ServerApi.BASE_URL + "app/appAddComAddress";

  @override
  void onDisposed() {
    groupInfoCompanyBasic?.dispose();
    super.onDisposed();
  }

  ///获取顶部信息请求
  void getCompanyBasicInfo() {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.get(TOP_COMPANY_INFO_API,
        params: {
          "authId": authId ?? "",
          "gps":gps??""
        },
        callback: BaseCallback(onSuccess: (val) {
          IndependentCompanyBasicInfoBean bean =
          IndependentCompanyBasicInfoBean.fromMap(val);
          if (bean != null && bean?.data?.companyBasicInfo != null) {
            CacheUtil.putCache(TOP_COMPANY_INFO_API, bean.toJson());
          }
          if(!isStateDisposed){
            groupInfoCompanyBasic?.value = bean?.data?.companyBasicInfo;
          }
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, "网络连接不可用");
        }));
  }

  ///修改单位名称
  void setEditCompanyInfo(CompanyBasicInfoBean companyInfoBean,BuildContext context) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(SET_COMPANY_INFO_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "companyid": companyInfoBean?.companyid,
          "city": companyInfoBean.city??"",
          "companyname": companyInfoBean.companyname??"",
          "companynick": companyInfoBean.companynick??"",
          "companylogo": companyInfoBean?.companylogo ?? "",
          // "type": _companyInfoBean?.type ?? "",
          "address": companyInfoBean.address??"",
        },
        callback: BaseCallback(onSuccess: (dynamic val) {
          VgToastUtils.toast(context, "保存成功");
          VgEventBus.global.send(new MonitoringIndexPageClass());
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          RouterUtils.pop(context);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///修改单位地址
  void setEditCompanyAddress(BuildContext context, String companyid, String address,
      String addrCity,String addrCode,String addrDistrict,String addrProvince,String gps) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    if(StringUtils.isEmpty(companyid)){
      companyid = UserRepository.getInstance().companyId;
    }
    VgHttpUtils.post(SET_COMPANY_INFO_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "companyid": companyid ?? "",
          "address": address ?? "",
          "addrCode": addrCode ?? "",
          "addrProvince": addrProvince ?? "",
          "addrCity": addrCity ?? "",
          "addrDistrict": addrDistrict ?? "",
          "gps": gps ?? "",
        },
        callback: BaseCallback(onSuccess: (dynamic val) {
          VgToastUtils.toast(context, "保存成功");
          VgEventBus.global.send(new MonitoringIndexPageClass());
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          UserRepository.getInstance().userData.companyInfo.setAddressInfo(address: address, gps: gps,
            code: addrCode, province: addrProvince, city: addrCity, district: addrDistrict);
          RouterUtils.pop(context, result: true);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///修改单位名称
  void setEditCompanyAddressInfo(BuildContext context,
      String addrCity,String addrCode,String addrDistrict,String addrProvince,String address,
      String gps,String gpsadress,String caid) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(SET_COMPANY_INFO_ADDRESS_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "caid": caid??"",
          "addrCity": addrCity??"",
          "addrCode": addrCode??"",
          "addrDistrict": addrDistrict??"",
          "addrProvince": addrProvince??"",
          "address":address??"",
          "gps": gps??"",
          "gpsadress":gpsadress?? "",
        },
        callback: BaseCallback(onSuccess: (dynamic val) {
          VgToastUtils.toast(context, "保存成功");
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          RouterUtils.pop(context);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///删除列表地址
  void removeCompanyAdd(BuildContext context,String caid) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(REMOVE_COMPANY_INFO_ADDRESS_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "caid": caid??"",
        },
        callback: BaseCallback(onSuccess: (val) {
          VgToastUtils.toast(context, "删除成功");
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          RouterUtils.pop(context);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///修改单位logo
  void setCompanyPic(BuildContext context, String picurl, String companyid){
    VgMatisseUploadUtils.uploadSingleImage(
        picurl,
        VgBaseCallback(onSuccess: (String netPic) {
          if (StringUtils.isNotEmpty(netPic)) {
            VgHttpUtils.post(SET_COMPANY_INFO_API,
                params: {
                  "authId": UserRepository.getInstance().authId ?? "",
                  "companyid": companyid ?? "",
                  "companylogo": netPic ?? "",
                },
                callback: BaseCallback(onSuccess: (val) {
                  VgEventBus.global.send(new PlayFileUpdateEvent("终端照片更新"));
                  UserRepository.getInstance().loginService(isBackIndexPage: false).autoLogin();
                }, onError: (msg) {
                  VgToastUtils.toast(AppMain.context, msg);
                }));
          } else {
            VgToastUtils.toast(context, "图片上传失败");
          }
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///获取顶部信息缓存
  void getBasicInfoCache() {
    CacheUtil.getCache(TOP_COMPANY_INFO_API).then((value) {
      if (value != null) {
        print('加载缓存');
        IndependentCompanyBasicInfoBean data =
        IndependentCompanyBasicInfoBean.fromMap(value);
        groupInfoCompanyBasic?.value = data?.data?.companyBasicInfo;
      }
    });
  }

}
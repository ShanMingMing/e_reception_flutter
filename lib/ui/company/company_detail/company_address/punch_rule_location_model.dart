import 'package:flutter/cupertino.dart';

class PunchRuleLocationModel {
  String title;
  String detail;
  String extendTitle;
  double lat;
  double long;

  String adCode;
  String province;
  String city;
  String district;
  PunchRuleLocationModel(this.title,this.extendTitle, this.detail, this.adCode, this.province, this.city,this.district,{this.lat,this.long});
}

class PunchRuleLocationSelectModel extends PunchRuleLocationModel {
  bool isSelect;
  PunchRuleLocationSelectModel(
      String title,
      String ex,
      String detail,
      String adCode,
      String province,
      String city,
      String district,
      this.isSelect,
      {double lat,double long})
      :super(title,ex,detail,adCode,province,city,district,lat:lat,long:long);
}
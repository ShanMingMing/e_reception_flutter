import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_http_lib.dart';

import '../enterprise_detail_page.dart';
import 'company_create_or_edit_address_page.dart';

class EnterpriseAddressListPage extends StatefulWidget {

  // final CompanyAddressListInfoBean itemBean;
  final String gps;

  const EnterpriseAddressListPage({Key key, this.gps}) : super(key: key);

  static const String ROUTER = "EnterpriseAddressListPage";

  @override
  _EnterpriseAddressListPageState createState() => _EnterpriseAddressListPageState();

  static Future<dynamic> navigatorPush(BuildContext context,String gps) {
    return RouterUtils.routeForFutureResult(
      context,
      EnterpriseAddressListPage(
        // itemBean: itemBean,
          gps:gps
      ),
      routeName: EnterpriseAddressListPage.ROUTER,
    );
  }
}

class _EnterpriseAddressListPageState extends State<EnterpriseAddressListPage> {

  CommonListPageWidgetState mState;

  CompanyAddressListInfoBean addBean = CompanyAddressListInfoBean();

  int numInfo = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF191E31),
      body: Stack(
        children: [
          Column(
            children: [
              _toTopBarWidget(),
              _toScreenWidget(),
              _companyAddressList(),
            ],
          ),
          _toAddressButtonWidget(),
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "选择摆放位置",
      isShowBack: true,
    );
  }

  Widget _toScreenWidget() {
    return Container(
      height: 40,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 15, top: 12, bottom: 11),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Image.asset(
            "images/shai_xuan1.png",
            height: 16,
          ),
          SizedBox(width: 4),
          Text(
            "筛选",
            style: TextStyle(
              color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
              fontSize: 12
            ),
          )
        ],
      ),
    );
  }

  Widget _companyAddressList() {
    return Expanded(
      child: CommonListPageWidget<CompanyAddressListInfoBean,CompanyAddressBean>(
        enablePullUp: false,
        enablePullDown: true,
        netUrl:ServerApi.BASE_URL + "app/appComAddressList",
        httpType: VgHttpType.post,
        queryMapFunc: (int page) => {
          "authId": UserRepository.getInstance().authId ?? "",
          "gps":widget?.gps ??"",
          "current":1,
          "size":20
        },
        parseDataFunc: (VgHttpResponse resp) {
          CompanyAddressBean bean = CompanyAddressBean.fromMap(resp.data);
          numInfo = bean?.data?.page?.records?.length;
          return bean;
        },
        stateFunc: (CommonListPageWidgetState state) {
          mState = state;
        },
        itemBuilder: (BuildContext context, int index, CompanyAddressListInfoBean itemBean) {
          return _buildItemWidget(itemBean,index);
        },
        separatorBuilder: (context, int index, _) {
          return Container(
            height: 0.5,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              color: Color(0xFF303546),
            ),
          );
        },
      ),
    );
  }

  Widget _buildItemWidget(CompanyAddressListInfoBean itemBean,int index) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          addBean = itemBean;
          RouterUtils.pop(context,result: addBean);
        },
        child: Column(
          children: [
            Container(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              child: Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 15, bottom: 15, left: 15),
                child: Text(
                  "${itemBean?.address ?? ""}",
                  softWrap: true,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 14,
                    color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                  ),
                ),
              ),
            ),
            if(index+1 == numInfo)
            Container(
              height: 95,
            )
          ],
        ),
    );
  }

  Widget _toAddressButtonWidget() {
    return Positioned(
        right: 0,
        left: 0,
        bottom: ScreenUtils.getBottomBarH(context) + 50,
        child: Center(
          child: Visibility(
            visible: AppOverallDistinguish.comefromAiOrWeStudy(),
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: true,
              width: 132,
              height: 40,
              margin: const EdgeInsets.symmetric(horizontal: 27),
              unSelectBgColor: Color(0xFF3A3F50),
              selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 15,
              ),
              selectedTextStyle: TextStyle(
                color: Colors.white,
                fontSize: 15,
                // fontWeight: FontWeight.w600
              ),
              text: "添加地址",
              textLeftSelectedWidget: Padding(
                padding: const EdgeInsets.only(bottom: 1),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
              onTap: (){
                CompanyCreateOrEditAddressPage.navigatorPush(context,"添加",addBean,widget?.gps,null,(){}).then((value) => mState?.viewModel?.refresh());
              },
            ),
          ),
        )
    );
  }
}



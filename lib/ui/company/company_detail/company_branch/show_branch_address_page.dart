import 'dart:async';
import 'dart:typed_data';
import 'package:amap_flutter_map/amap_flutter_map.dart';
import 'package:amap_flutter_base/amap_flutter_base.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/branch_detail_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/terminal_address_marker_to_image.dart';
import 'package:e_reception_flutter/utils/address_about_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

class ShowBranchAddressPage extends StatefulWidget{

  static const String ROUTER = "ShowTerminalAddressPage";
  final ComBranchBean branchBean;
  const ShowBranchAddressPage({Key key, this.branchBean})
      : super(key: key);

  @override
  _ShowBranchAddressPageState createState() => _ShowBranchAddressPageState();

  static Future<dynamic> navigatorPush(
      BuildContext context, ComBranchBean branchBean) {
    return RouterUtils.routeForFutureResult(
      context,
      ShowBranchAddressPage(
        branchBean: branchBean,
      ),
      routeName: ShowBranchAddressPage.ROUTER,
    );
  }
}

class _ShowBranchAddressPageState extends BaseState<ShowBranchAddressPage> {

  LatLng _latLng;
  Set<Marker> _markerList;
  GlobalKey _globalKey = GlobalKey();
  bool _initFlag;

  int indexStart;
  int endIndex;

  @override
  void initState() {
    super.initState();
    indexStart = widget.branchBean.gps.indexOf(",")+1;
    endIndex = widget.branchBean.gps.indexOf("°E");
    _markerList = new Set();

    Future((){
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(widget?.branchBean?.gps?.contains("°N")){
      _latLng = new LatLng(double.parse(widget?.branchBean?.gps?.split("°N")[0]),
          double.parse(widget.branchBean.gps.substring(indexStart,endIndex)));
    }else if(widget?.branchBean?.gps?.contains(",")){
      _latLng = new LatLng(double.parse(widget?.branchBean?.gps?.split(",")[1]),
          double.parse(widget?.branchBean?.gps?.split(",")[0]));
    }


    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if(_initFlag??false){
        return;
      }
      try{
        Future<Uint8List> file = GetWidgetToImage.getInstance(_globalKey).getUint8List();
        file.then((value) => {
          setState((){
            _markerList.add(new Marker(
              position: _latLng,
              icon: BitmapDescriptor.fromBytes(value),
              infoWindowEnable: true,

            ));
            _initFlag = true;
          })
        });
      }on Exception{
      }catch (e){
        print("'!debugNeedsPaint': is not true.");
      }
    });

    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toInfoWidget(widget?.branchBean),
    );
  }

  Widget _toInfoWidget(ComBranchBean branchBean){
    return Stack(
      children:[
        Positioned(
          top: (ScreenUtils.screenH(context) - 44 - 2 - ScreenUtils.getStatusBarH(context))/2,
          child: TerminalAddressMarkerToImage(
            branchBean: widget?.branchBean,
            globalKeys: _globalKey,
          ),
        ),
        Column(
          children: <Widget>[
            _toTopBarWidget(branchBean?.branchname??""),
            _toMapWidget(branchBean),
          ],
        ),

      ],
    );
  }

  Widget _toTopBarWidget(String terminalName){
    return  VgTopBarWidget(
      title: terminalName??"新分支",
      isShowBack: true,
    );

  }

  Widget _toMapWidget(ComBranchBean branchBean){
    return Expanded(
      child: AMapWidget(
        initialCameraPosition: CameraPosition(
          target: _latLng,
          zoom: 17,
        ),
        markers: _markerList,
        scaleEnabled: false,
      ),
    );
  }

  onMapMoveEnd(){
    setState(() { });
  }
}

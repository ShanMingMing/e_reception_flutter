import 'package:e_reception_flutter/common_widgets/common_is_know_dialog.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/branch_detail_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/play_file_update_event.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/utils/cache_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

import '../../../app_main.dart';

class CompanyBranchDetailViewModel extends BaseViewModel{
  ValueNotifier<BranchDetailDataBean> groupInfoComBranch;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  CompanyBranchDetailViewModel(BaseState<StatefulWidget> state) : super(state){
    groupInfoComBranch = ValueNotifier(null);
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
  }

  static const String SET_COMPANY_INFO_BRANCH_API =
     ServerApi.BASE_URL + "app/appAddComBranch";

  static const String REMOVE_COMPANY_INFO_BRANCH_API =
     ServerApi.BASE_URL + "app/appRemoveComBranch";

  ///获取分支终端详情
  static const String BRANCH_TERMINAL_DETAIl_INFO_API =
     ServerApi.BASE_URL + "app/appComBranchDetails";
  //
  // static const String SET_COMPANY_INFO_API =
  //    ServerApi.BASE_URL + "app/appEditCompany";

  @override
  void onDisposed() {
    groupInfoComBranch?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();
  }

  ///修改单位分支
  void setEditCompanyBranchInfo(BuildContext context,
      String addrCity,String addrCode,String addrDistrict,String addrProvince,String address,
      String branchname, String gps,String gpsadress,String cbid) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(SET_COMPANY_INFO_BRANCH_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "cbid": cbid ?? "",
          "addrCity": addrCity ?? "",
          "addrCode": addrCode ?? "",
          "addrDistrict": addrDistrict ?? "",
          "addrProvince": addrProvince ?? "",
          "address": address ?? "",
          "branchname":branchname ?? "",
          "gps": gps ?? "",
          "gpsadress": gpsadress ?? "",
        },
        callback: BaseCallback(onSuccess: (dynamic val) {
          VgToastUtils.toast(context, "保存成功");
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          VgEventBus.global.send(RefreshBranchDetailEvent());
          RouterUtils.pop(context);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///删除列表分支
  void removeCompanyBranch(BuildContext context,String cbid) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(REMOVE_COMPANY_INFO_BRANCH_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "cbid": cbid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          VgToastUtils.toast(context, "删除成功");
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          RouterUtils.pop(context);
          RouterUtils.pop(context);
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///获取分支终端详情
  void branchTerminalDetailInfo(BuildContext context, String cbid, String gps) {
    final String authId = UserRepository.getInstance().authId ?? "";
    if (authId == null || authId.isEmpty) {
      return;
    }
    VgHttpUtils.post(BRANCH_TERMINAL_DETAIl_INFO_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "cbid": cbid ?? "",
          "gps": gps ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          BranchDetailBean bean =
          BranchDetailBean.fromMap(val);
          if (bean != null && bean?.data != null) {
            CacheUtil.putCache(BRANCH_TERMINAL_DETAIl_INFO_API, bean.toJson());
          }
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            groupInfoComBranch?.value = bean?.data;
          }
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, "网络连接不可用");
          statusTypeValueNotifier?.value = PlaceHolderStatusType.error;
        }));
  }

  ///顶部缓存
  void getBasicInfoCache() {
    CacheUtil.getCache(BRANCH_TERMINAL_DETAIl_INFO_API).then((value) {
      if (value != null) {
        print('加载缓存');
        BranchDetailBean data =
        BranchDetailBean.fromMap(value);
        groupInfoComBranch?.value = data?.data;
      }
    });
  }
}
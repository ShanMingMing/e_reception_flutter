import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EditNameAndNickPage extends StatefulWidget {
  final String title;
  final TextEditingController editStrController;
  final VoidCallback callBack;
  const EditNameAndNickPage({Key key, this.title, this.editStrController, this.callBack}) : super(key: key);

  static const String ROUTER = "EditNameAndNickPage";

  @override
  _EditNameAndNickPageState createState() => _EditNameAndNickPageState();

  static Future<dynamic> navigatorPush(BuildContext context,String title,TextEditingController editStrController,VoidCallback callBack) {
    return RouterUtils.routeForFutureResult(
      context,
      EditNameAndNickPage(
          title:title,
          editStrController:editStrController,
          callBack:callBack
      ),
      routeName: EditNameAndNickPage.ROUTER,
    );
  }
}

class _EditNameAndNickPageState extends State<EditNameAndNickPage> {

  String editStr;

  bool isAlive = false;

  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    controller.text = widget.editStrController?.text??"-";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF191E31),
      body: Column(
        children: [
          _toTopBarWidget(),
          _toEditWidget(),
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "${widget?.title}",
      rightWidget: _buttonWidget(),
    );
  }

  Widget _buttonWidget() {
    _aliveButton();
    return Container(
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isAlive,
        width: 48,
        height: 24,
        unSelectBgColor: ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
            fontSize: 12,
            fontWeight: FontWeight.w600),
        selectedTextStyle: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        text: "保存",
        onTap: () {
          widget?.editStrController?.text = controller?.text;
          widget.callBack();
            setState(() {});
          },
      ),
    );
  }

  void _aliveButton(){
    if((editStr?.length??0)!=0){
      isAlive = true;
    }else{
      isAlive = false;
    }
  }

  Widget _toEditWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(left: 15),
      child: VgTextField(
        controller: controller,
        autofocus: true,
        keyboardType: TextInputType.text,
        style: TextStyle(
            color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
            fontSize: 14),
        decoration: new InputDecoration(
            counterText: "",
            hintText: "请输入",
            border: InputBorder.none,
            hintStyle: TextStyle(
                fontSize: 14,
                color: Color(0XFF3A3F50))),
        maxLines: 1,
        maxLimitLength: widget?.title == "全称" ? 30 : 12,
        onChanged: (String str){
          editStr = str;
          // _editingController?.text = str;
          if(str != null && str != ""){
            isAlive = true;
          }
          setState(() {

          });
        },
      ),
    );
  }
}

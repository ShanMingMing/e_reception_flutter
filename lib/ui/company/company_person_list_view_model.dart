
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/common_person_list_response.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

///企业成员列表相关
class CommonPersonListViewModel
    extends BasePagerViewModel<CommonPersonBean, CommonPersonListResponse> {
  ///管理员列表
  static const int TYPE_MANAGER = 0;

  ///未激活列表
  static const int TYPE_UN_ACTIVE = 1;

  ///所有成员列表
  static const int TYPE_SELECT_MEMBER = 2;

  ///非管理员的成员列表
  static const int TYPE_SELECT_MEMBER_NO_ADMIN = 3;

  ///接口类型
  final int type;

  ///页面类型 部门/班级
  final String  pageType;

  ///搜索关键字
  String keyword;
  String id;

  CommonPersonListViewModel(BaseState<StatefulWidget> state, this.type,
      {this.id,this.pageType})
      : super(state);

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  String getCacheKey() {
    if (TYPE_MANAGER == type)
      return NetApi.MANAGER_LIST_PAGER +
          UserRepository.getInstance().getPhone() +
          UserRepository.getInstance().getCompanyId();
    return null;
  }

  @override
  bool isNeedCache() {
    if (TYPE_MANAGER == type) return true;
    return super.isNeedCache();
  }

  @override
  Map<String, String> getQuery(int page) {
    return {
      'authId': UserRepository.getInstance().authId,
      'current': page.toString(),
      'size': '20',
      'searchName': keyword ?? '',
      if (TYPE_SELECT_MEMBER == type)
        'type': pageType,
      if (TYPE_SELECT_MEMBER == type) 'id': id ?? '',
    };
  }

  @override
  String getRequestMethod() {
    if (TYPE_SELECT_MEMBER_NO_ADMIN == type) return HttpUtils.GET;
    return HttpUtils.POST;
  }

  @override
  String getUrl() {
    if (TYPE_MANAGER == type)
      return NetApi.MANAGER_LIST_PAGER +
          '?type=' +
          (UserRepository.getInstance().userData.companyInfo.type ?? '00');
    if (TYPE_UN_ACTIVE == type) return NetApi.UN_ACTIVE_PERSON_LIST_PAGER;
    if (TYPE_SELECT_MEMBER == type)
      return NetApi.CLASS_DEPARTMENT_ADD_ADMIN_LIST;
    // if (TYPE_SELECT_MEMBER == type) return NetApi.ALL_MEMBER_LIST+UserRepository.getInstance().userData.companyInfo.groupList.firstWhere((element) => element.isStaff()).groupid;
    if (TYPE_SELECT_MEMBER_NO_ADMIN == type)
      return NetApi.EXCEPT_ADMIN_MEMBER_LIST +
          UserRepository.getInstance()
              .userData
              .companyInfo
              .groupList
              .firstWhere((element) => element.isStaff())
              .groupid;
    return NetApi.MANAGER_LIST_PAGER;
  }

  @override
  CommonPersonListResponse parseData(VgHttpResponse resp) {
    loading(false);
    CommonPersonListResponse response =
        CommonPersonListResponse.fromMap(resp.data);
    return response;
  }

  ///添加管理员
  void addAdmin(BuildContext context,String id,  String userid) {
    loading(true,msg:'正在添加');
    VgHttpUtils.post(NetApi.CLASS_DEPARTMENT_ADD_ADMIN,
        params: {
          'authId': UserRepository.getInstance().authId,
          'id': id,
          'type': pageType??'00',
          'userid': userid
        },
        callback: BaseCallback(onSuccess: (val) {
          loading(false);
          RouterUtils.pop(context, result: true);
        },onError: (error){
          loading(false);
          toast(error);
        }));
  }
}

import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';
import 'bean/company_add_user_update_info_upload_bean.dart';
import 'bean/face_user_info_response_bean.dart';

class CompanyAddUserUpdateInfoViewModel extends BaseViewModel {
  ///App添加用户
  static const String COMPANY_ADD_USER_UPDATE_API =
     ServerApi.BASE_URL + "app/appEditUser";

  ///App用户详情
  static const String USER_INFO_API =ServerApi.BASE_URL + "app/appFaceUserInfo";

  final String fuid;

  final String fid;

  ValueNotifier<FaceUserInfoDataBean> userInfoValueNotifier;

  CompanyAddUserUpdateInfoViewModel(
      BaseState<StatefulWidget> state,this.fuid,this.fid)
      : super(state) {
    userInfoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    userInfoValueNotifier?.dispose();
    super.onDisposed();
  }

  ///获取用户信息请求
  void getUserInfoHttp() {
    VgHttpUtils.get(USER_INFO_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          FaceUserInfoResponseBean bean = FaceUserInfoResponseBean.fromMap(val);
          userInfoValueNotifier.value = bean?.data;
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  ///保存或绑定
  void saveUpdateUserHttp(BuildContext context,
      CompanyAddUserUpdateInfoUploadBean uploadBean) async {
    if (uploadBean == null) {
      return;
    }
    if (StringUtils.isEmpty(uploadBean?.picUrl)) {
      _toSaveHttp(context, uploadBean);
      return;
    }
    VgMatisseUploadUtils.uploadSingleImage(
        uploadBean?.picUrl,
        VgBaseCallback(onSuccess: (String netPic) {
          if (StringUtils.isNotEmpty(netPic)) {
            uploadBean.picUrl = netPic;
            _toSaveHttp(context, uploadBean);
          } else {
            VgToastUtils.toast(context, "图片上传失败");
          }
        }, onError: (String msg) {
          VgToastUtils.toast(context, msg);
        }),
      isFace: true
    );
  }

  void _toSaveHttp(
      BuildContext context, CompanyAddUserUpdateInfoUploadBean uploadBean) {
    if(StringUtils.isEmpty(fuid) || uploadBean == null){
      toast("保存参数不正确");
      return;
    }
    print("保存");
    print("----------------\n");
    print(uploadBean?.toString());
    print("-----------------\n");
    // return;
    Map<String,dynamic> params = {
      "authId": UserRepository.getInstance().authId ?? "",
      "fid": fid ?? "",
      "fuid": fuid ?? "",
      "groupid": uploadBean?.groupId ?? "",
      "hsn": uploadBean?.getHsnSplitStr() ?? "",
      "name": uploadBean?.name ?? "",
      "roleid": uploadBean?.identityType?.getTypeToIdStr() ?? "",
      if(uploadBean.isUpdatePic())
        "napicurl": uploadBean?.picUrl ?? "",
      if(uploadBean.userid != null && uploadBean.userid.isNotEmpty)
        "userid": uploadBean?.userid ?? "",
      "nick": uploadBean?.nick ?? "",
      "number": uploadBean?.id ?? "",
      "phone": globalSubPhone(uploadBean?.phone) ?? "",
    };
    VgHudUtils.show(context, "保存中");
    VgHttpUtils.get(COMPANY_ADD_USER_UPDATE_API,
        params: params,
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          RouterUtils.pop(context, result: true);
          VgToastUtils.toast(AppMain.context, "编辑成功");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }
}

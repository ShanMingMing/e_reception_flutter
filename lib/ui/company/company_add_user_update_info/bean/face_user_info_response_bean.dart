/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"nick":"","number":"","groupName":"员工","phone":"15522223333","roleid":"90","name":"管理员普通","napicurl":"","userid":"68a34fb1797d4e019ca735fd7750c8cd","hsnInfo":[{"terminalName":"搜宝29楼立式机","hsn":"YGBU3DYAVI"}]}
/// extra : null

class FaceUserInfoResponseBean {
  bool success;
  String code;
  String msg;
  FaceUserInfoDataBean data;
  dynamic extra;

  static FaceUserInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceUserInfoResponseBean faceUserInfoResponseBeanBean = FaceUserInfoResponseBean();
    faceUserInfoResponseBeanBean.success = map['success'];
    faceUserInfoResponseBeanBean.code = map['code'];
    faceUserInfoResponseBeanBean.msg = map['msg'];
    faceUserInfoResponseBeanBean.data = FaceUserInfoDataBean.fromMap(map['data']);
    faceUserInfoResponseBeanBean.extra = map['extra'];
    return faceUserInfoResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// nick : ""
/// number : ""
/// groupName : "员工"
/// phone : "15522223333"
/// roleid : "90"
/// name : "管理员普通"
/// napicurl : ""
/// userid : "68a34fb1797d4e019ca735fd7750c8cd"
/// hsnInfo : [{"terminalName":"搜宝29楼立式机","hsn":"YGBU3DYAVI"}]

class FaceUserInfoDataBean {
  String nick;
  String number;
  String groupName;
  String phone;
  String roleid;
  String name;
  String napicurl;
  String userid;
  String putpicurl;
  List<HsnInfoBean> hsnInfo;

  String groupid;

  static FaceUserInfoDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceUserInfoDataBean dataBean = FaceUserInfoDataBean();
    dataBean.nick = map['nick'];
    dataBean.number = map['number'];
    dataBean.groupName = map['groupName'];
    dataBean.phone = map['phone'];
    dataBean.roleid = map['roleid'];
    dataBean.name = map['name'];
    dataBean.napicurl = map['napicurl'];
    dataBean.userid = map['userid'];
    dataBean.groupid = map['groupid'];
    dataBean.putpicurl = map['putpicurl'];
    dataBean.hsnInfo = List()..addAll(
      (map['hsnInfo'] as List ?? []).map((o) => HsnInfoBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "phone": phone,
    "roleid": roleid,
    "name": name,
    "napicurl": napicurl,
    "userid": userid,
    "hsnInfo": hsnInfo,
    "groupid": groupid,
    "putpicurl": putpicurl,
  };
}

/// terminalName : "搜宝29楼立式机"
/// hsn : "YGBU3DYAVI"

class HsnInfoBean {
  String terminalName;
  String hsn;

  static HsnInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnInfoBean hsnInfoBean = HsnInfoBean();
    hsnInfoBean.terminalName = map['terminalName'];
    hsnInfoBean.hsn = map['hsn'];
    return hsnInfoBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
  };
}
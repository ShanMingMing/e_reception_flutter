import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_list/company_add_user_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/company_add_user_update_info_upload_bean.dart';
import 'bean/company_person_info_bean.dart';
import 'bean/face_user_info_response_bean.dart';
import 'company_add_user_update_info_view_model.dart';
import 'widgets/company_add_user_update_info_widget.dart';

/// 企业添加用户编辑信息
///
/// @author: zengxiangxi
/// @createTime: 1/23/21 10:37 AM
/// @specialDemand:


/// 已弃用
class CompanyAddUserUpdateInfoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CompanyAddUserUpdateInfoPage";

  final String fuid;

  final String fid;

  ///是否禁用角色权限修改（默认可以修改）
  final bool isDisableRole;

  const CompanyAddUserUpdateInfoPage(
      {Key key, this.fuid, this.fid, this.isDisableRole})
      : super(key: key);

  @override
  CompanyAddUserUpdateInfoPageState createState() =>
      CompanyAddUserUpdateInfoPageState();

  ///跳转方法
  static Future<bool> navigatorPush(
      BuildContext context, String fuid, String fid,
      {bool isDisableRole = false}) {
    return RouterUtils.routeForFutureResult<bool>(
      context,
      CompanyAddUserUpdateInfoPage(
        fuid: fuid,
        fid: fid,
        isDisableRole: isDisableRole,
      ),
      routeName: CompanyAddUserUpdateInfoPage.ROUTER,
    );
  }
}

class CompanyAddUserUpdateInfoPageState
    extends BaseState<CompanyAddUserUpdateInfoPage> {
  ValueNotifier<CompanyAddUserUpdateInfoUploadBean> _editsValueNotifier;

  CompanyAddUserUpdateInfoViewModel _viewModel;

  static CompanyAddUserUpdateInfoPageState of(BuildContext context) {
    return context
        ?.findAncestorStateOfType<CompanyAddUserUpdateInfoPageState>();
  }

  @override
  void initState() {
    super.initState();
    _viewModel =
        CompanyAddUserUpdateInfoViewModel(this, widget.fuid, widget.fid);
    _editsValueNotifier = ValueNotifier(null);
    _viewModel.getUserInfoHttp();
  }

  @override
  void dispose() {
    _editsValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toPlaceHolderWidget(),
        ),
      ],
    );
  }

  Widget _toPlaceHolderWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel.userInfoValueNotifier,
      builder: (BuildContext context, FaceUserInfoDataBean userInfoBean,
          Widget child) {
        return VgPlaceHolderStatusWidget(
          loadingStatus: userInfoBean == null,
          loadingOnClick: () => _viewModel.getUserInfoHttp(),
          child: _toScrollViewWidget(userInfoBean),
        );
      },
    );
  }

  Widget _toScrollViewWidget(FaceUserInfoDataBean userInfoBean) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: ListView(
        padding: const EdgeInsets.all(0),
        physics: ClampingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        children: <Widget>[
          Container(
            height: 10,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          ),
          CompanyAddUserUpdateInfoWidget(
            onChanged: _editsValueNotifier,
            infoBean: userInfoBean,
          ),
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "编辑用户",
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      rightWidget: _toSaveButtonWidget(),
    );
  }

  Widget _toSaveButtonWidget() {
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context,
          CompanyAddUserUpdateInfoUploadBean uploadBean, Widget child) {
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: uploadBean?.isAlive() ?? false,
          width: 48,
          height: 24,
          unSelectBgColor:
              ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
              fontWeight: FontWeight.w600),
          selectedTextStyle: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          text: "保存",
          onTap: () {
            String msg = uploadBean?.checkVerify();
            if (StringUtils.isEmpty(msg)) {
              _viewModel?.saveUpdateUserHttp(context, uploadBean);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_sliver_delegate/common_sliver_delegate.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/common_person_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_person_list_view_model.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import '../common_person_list_item_widget.dart';

///企业主页 未激活列表
class UnactivePersonPage extends StatefulWidget {
  static const String ROUTER = 'UnactivePersonPage';

  ///跳转方法
  static Future<Map> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult<Map>(
      context,
      UnactivePersonPage(),
      routeName: UnactivePersonPage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return UnactivePersonPageState();
  }
}

class UnactivePersonPageState
    extends BasePagerState<CommonPersonBean, UnactivePersonPage>
    with VgPlaceHolderStatusMixin<CommonPersonBean, UnactivePersonPage> {
  CommonPersonListViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CommonPersonListViewModel(
        this, CommonPersonListViewModel.TYPE_UN_ACTIVE);
    _viewModel?.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAnnotatedRegion(
      Scaffold(
          backgroundColor:
              ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          body: Container(
            child: Column(
              children: <Widget>[
                _toTopBarWidget(),
                Expanded(
                    child: StreamBuilder(
                        stream: _viewModel?.onEndRefresh,
                        builder: (context, snapshot) {
                          bool empty =
                              StringUtils.isEmpty(_viewModel.keyword) &&
                                  ((data?.length ?? 0) == 0);
                          return NestedScrollView(
                            headerSliverBuilder: (context, bool) {
                              return [
                                SliverPersistentHeader(
                                  delegate:
                                      CommonSliverPersistentHeaderDelegate(
                                    minHeight: empty ? 0 : 50,
                                    maxHeight: empty ? 0 : 50,
                                    child: Container(
                                      color: ThemeRepository.getInstance()
                                          .getCardBgColor_21263C(),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 10),
                                      child: CommonSearchBarWidget(
                                        hintText: "搜索",
                                        onChanged: (keyword) {
                                          _viewModel.keyword = keyword;
                                          _viewModel.refresh();
                                        },
                                        onSubmitted: (keyword) {
                                          _viewModel.keyword = keyword;
                                          _viewModel.refresh();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ];
                            },
                            body: _toPullRefreshWidget(),
                          );
                        })),
              ],
            ),
          )),
      navigationBarColor:
          ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "未激活",
      titleFontSize: 17,
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  Widget _toPullRefreshWidget() {
    return VgPlaceHolderStatusWidget(
        loadingStatus: data == null,
        emptyStatus: data?.isEmpty ?? false,
        child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: VgPullRefreshWidget.bind(
              state: this,
              viewModel: _viewModel,
              enablePullDown: false,
              child: ListView.separated(
                  itemCount: data?.length ?? 0,
                  padding: const EdgeInsets.all(0),
                  keyboardDismissBehavior:
                      ScrollViewKeyboardDismissBehavior.onDrag,
                  itemBuilder: (BuildContext context, int index) {
                    if (data == null || data.length <= index) {
                      return Container();
                    }
                    return _listItem(data[index]);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: 0,
                    );
                  })),
        ));
  }

  @override
  void finishRefresh({Duration duration}) {
    super.finishRefresh(duration: duration);
    loading(false);
  }

  @override
  void finishLoadMore() {
    super.finishLoadMore();
    loading(false);
  }

  Widget _listItem(CommonPersonBean data) {
    return GestureDetector(
      onTap: () {
        PersonDetailPage.navigatorPush(context, data.fuid, fid:data.fid);
      },
      behavior: HitTestBehavior.opaque,
      child: Container(
        height: 60,
        child: CommonListItemWidget(
          isAlive: false,
          putpicurl: data.putpicurl,
          napicurl: data.napicurl,
          name: data.name,
          nick: data.nick,
          phone: data.phone,
          department: data.department,
          projectname: data.projectname,
          city: data.city,
          groupName: data.groupName,
          number: data.number,
          uname: data.uname,
          lastPunchTime: null,
          classname: data?.classname,
        ),
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      ),
    );
  }
}

import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';

///企业主页 成员列表（管理员 未激活）
class CommonPersonListResponse extends BasePagerBean<CommonPersonBean> {
  @override
  int getCurrentPage() {
    return data?.page?.current ?? 1;
  }

  @override
  List<CommonPersonBean> getDataList() {
    return data?.page?.records ?? [];
  }

  @override
  int getMaxPage() {
    return data?.page?.pages ?? 1;
  }

  @override
  String getMessage() {
    return msg ?? '';
  }

  @override
  bool isSucceed() {
    return success ?? false;
  }

  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CommonPersonListResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CommonPersonListResponse managerListResponseBean =
        CommonPersonListResponse();
    managerListResponseBean.success = map['success'];
    managerListResponseBean.code = map['code'];
    managerListResponseBean.msg = map['msg'];
    managerListResponseBean.data = DataBean.fromMap(map['data']);
    managerListResponseBean.extra = map['extra'];
    return managerListResponseBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// page : {"records":[{"nick":"","lasttime":1618319768,"phone":"13200000000","putpicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","roleid":"99","name":"李大红呀abc","hsnCnt":3,"napicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","userid":"4895ada8d22749159caebcb838387513","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1618321733,"phone":"13226332406","putpicurl":"","roleid":"99","name":"李岁1322633","hsnCnt":0,"napicurl":"","userid":"5f0980b626d446d2badbfb62c6fc473e","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1617705287,"phone":"22222222222","putpicurl":"","roleid":"90","name":"李岁红","hsnCnt":1,"napicurl":"","userid":"7172d9ed85b742c488dab7c3166e8c79","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1618320346,"phone":"18665289540","putpicurl":"","roleid":"90","name":"李岁红186","hsnCnt":1,"napicurl":"","userid":"d5647a27c47547f38051e3cba92f5f46","department":"北京开发,789","departmentCnt":2,"personCnt":6}],"total":4,"size":10,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
        "page": page,
      };
}

/// records : [{"nick":"","lasttime":1618319768,"phone":"13200000000","putpicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","roleid":"99","name":"李大红呀abc","hsnCnt":3,"napicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","userid":"4895ada8d22749159caebcb838387513","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1618321733,"phone":"13226332406","putpicurl":"","roleid":"99","name":"李岁1322633","hsnCnt":0,"napicurl":"","userid":"5f0980b626d446d2badbfb62c6fc473e","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1617705287,"phone":"22222222222","putpicurl":"","roleid":"90","name":"李岁红","hsnCnt":1,"napicurl":"","userid":"7172d9ed85b742c488dab7c3166e8c79","department":"","departmentCnt":0,"personCnt":0},{"nick":"","lasttime":1618320346,"phone":"18665289540","putpicurl":"","roleid":"90","name":"李岁红186","hsnCnt":1,"napicurl":"","userid":"d5647a27c47547f38051e3cba92f5f46","department":"北京开发,789","departmentCnt":2,"personCnt":6}]
/// total : 4
/// size : 10
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CommonPersonBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()
      ..addAll((map['records'] as List ?? [])
          .map((o) => CommonPersonBean.fromMap(o)));
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
        "records": records,
        "total": total,
        "size": size,
        "current": current,
        "orders": orders,
        "searchCount": searchCount,
        "pages": pages,
      };
}

/// nick : ""
/// lasttime : 1618319768
/// phone : "13200000000"
/// putpicurl : "http://etpic.we17.com/test/20210226095738_2383.jpg"
/// roleid : "99"
/// name : "李大红呀abc"
/// hsnCnt : 3
/// napicurl : "http://etpic.we17.com/test/20210226095738_2383.jpg"
/// userid : "4895ada8d22749159caebcb838387513"
/// department : ""
/// departmentCnt : 0
/// personCnt : 0

class CommonPersonBean {
  String nick;
  int lasttime;//最后刷脸时间
  int firsttime;//首次开始时间
  String phone;
  String putpicurl;
  String roleid;
  String name;
  int hsnCnt;
  String napicurl;
  String userid;
  String department;
  String departmentid;
  int departmentCnt;
  int personCnt;
  String groupName;
  String groupType;//01访客 02学员 03家长 04员工 99普通

  String number;
  String uname;//添加人
  int createdate;//添加时间
  String groupid;
  String fuid;
  String type;
  String status;//00未激活

  String fid;
  String isSign;
  String pictureUrl;
  int lastPunchTime;
  String projectname;
  String classname;
  String classid;
  int classCnt;
  String coursename;
  String city;
  bool special;


  ///激活
  bool isAlive() {
    return '01'==status;
  }
  ///员工
  bool isStaff(){
    return '04'==groupType;
  }

  ///学生
  bool isStu(){
    return '02'==groupType;
  }
  static CommonPersonBean fake(){
    CommonPersonBean recordsBean = CommonPersonBean();
    recordsBean.name='这是假数据';
    recordsBean.groupid='1';
    recordsBean.groupName='成员';
    recordsBean.department='部门';
    return recordsBean;
  }

  static CommonPersonBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CommonPersonBean recordsBean = CommonPersonBean();
    recordsBean.nick = map['nick'];
    recordsBean.groupType = map['groupType'];

    recordsBean.lasttime = map['lasttime'];
    recordsBean.phone = map['phone'];
    recordsBean.firsttime = map['firsttime'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.roleid = map['roleid'];
    recordsBean.name = map['name'];
    recordsBean.hsnCnt = map['hsnCnt'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.userid = map['userid'];
    recordsBean.departmentCnt = map['departmentCnt'];
    recordsBean.personCnt = map['personCnt'];
    recordsBean.number = map['number'];
    recordsBean.groupName = map['groupName'];
    recordsBean.uname = map['uname'];
    recordsBean.createdate = map['createdate'];
    recordsBean.phone = map['phone'];
    recordsBean.roleid = showOriginEmptyStr(map['roleid']) ??
        CompanyAddUserEditInfoIdentityType.commonUser.getTypeToIdStr();
    recordsBean.groupid = map['groupid'];
    recordsBean.fuid = map['fuid'];
    recordsBean.type = map['type'];
    recordsBean.status = map['status'];
    recordsBean.lastPunchTime = map['lastPunchTime']??map['lastpunchtime'];
    // recordsBean.lastpunchtime= map['lastpunchtime'];
    recordsBean.fid = map['fid'];
    recordsBean.isSign = map['isSign'];
    recordsBean.pictureUrl = map['pictureUrl'];
    recordsBean.department = map['department'];
    recordsBean.projectname = map['projectname'];
    recordsBean.classname = map['classname'];
    recordsBean.classCnt=map['classCnt'];
    recordsBean.coursename = map['coursename'];
    recordsBean.city = map['city'];
    recordsBean.special = map['special'];
    recordsBean.classid = map['classid'];
    recordsBean.departmentid = map['departmentid'];
    return recordsBean;
  }

  Map toJson() => {
        "nick": nick,
    "classid": classid,
    "departmentid": departmentid,
        "lasttime": lasttime,
        "phone": phone,
        "putpicurl": putpicurl,
        "roleid": roleid,
        "name": name,
        "hsnCnt": hsnCnt,
        "napicurl": napicurl,
        "userid": userid,
        "department": department,
        "departmentCnt": departmentCnt,
        "personCnt": personCnt,
        "nick": nick,
        "number": number,
        "groupName": groupName,
        "uname": uname,
        "groupid": groupid,
        "fuid": fuid,
        "napicurl": napicurl,
        "type": type,
        "status": status,
        "lastPunchTime": lastPunchTime,
        "fid": fid,
        "userid": userid,
        "pictureUrl": pictureUrl,
        "department": department,
        "projectname": projectname,
        "classname": classname,
        "coursename": coursename,
        "city": city,
        "special": special,
      };


}

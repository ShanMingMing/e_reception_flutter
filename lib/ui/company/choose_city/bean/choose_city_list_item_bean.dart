class ChooseCityListItemBean{
  final String sid;

  final String sname;

  final double latitude;

  final double longitude;

  ChooseCityListItemBean(this.sid, this.sname, {this.latitude, this.longitude});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChooseCityListItemBean &&
          runtimeType == other.runtimeType &&
          sid == other.sid;

  @override
  int get hashCode => sid.hashCode;

  @override
  String toString() {
    return 'ChooseCityListItemBean{sid: $sid, sname: $sname, latitude: $latitude, longitude: $longitude}';
  }
}
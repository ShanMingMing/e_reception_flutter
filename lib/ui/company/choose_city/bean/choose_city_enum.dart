enum ChooseCityEnum{
  ///省
  province,
  ///市
  city,
  ///区
  district,
}

extension ChooseCityEnumExtension on ChooseCityEnum{

  ///返回串
  String toStr(){
    switch(this){
      case ChooseCityEnum.province:
        return "省份";
      case ChooseCityEnum.city:
        return "城市";
      case ChooseCityEnum.district:
        return "区县";
    }
    return "";
  }
}
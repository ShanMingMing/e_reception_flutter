import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 选择城市-展示地点列表
///
/// @author: zengxiangxi
/// @createTime: 1/9/21 2:51 PM
/// @specialDemand:
class ChooseCityPlaceListWidget extends StatefulWidget {
  final List<ChooseCityListItemBean> list;

  final ValueChanged<ChooseCityListItemBean> onSelectedFunc;

  final ChooseCityListItemBean selectedBean;
  final bool lightTheme;

  const ChooseCityPlaceListWidget(
      {Key key, @required this.list, this.onSelectedFunc, this.selectedBean, this.lightTheme})
      : super(key: key);

  @override
  _ChooseCityPlaceListWidgetState createState() => _ChooseCityPlaceListWidgetState();
}

class _ChooseCityPlaceListWidgetState extends State<ChooseCityPlaceListWidget> with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    super.build(context);
    // if (widget.list == null || widget.list.isEmpty) {
    //   return Container();
    // }
    return VgPlaceHolderStatusWidget(
      // loadingStatus: widget?.list == null || widget.list.length <=0,
      child: Container(
        color: (widget?.lightTheme??false)?Colors.white:ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        child: ListView.separated(
            itemCount: widget.list?.length ?? 0,
            padding: const EdgeInsets.all(0),
            physics: BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return _toListItemWidget(widget.list?.elementAt(index));
            },
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                color: (widget?.lightTheme??false)?Colors.white:ThemeRepository.getInstance().getCardBgColor_21263C(),
                child: Container(
                  height: (widget?.lightTheme??false)?1:0.5,
                  margin: const EdgeInsets.only(left: 20, right: 1),
                  color: (widget?.lightTheme??false)?Color(0xFFF6F7F9):Color(0xFF21283D),
                ),
              );
            }),
      ),
    );
  }

  Widget _toListItemWidget(ChooseCityListItemBean itemBean) {
    if (itemBean == null || itemBean.sname == null || itemBean.sid == null) {
      return Container();
    }
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (widget.onSelectedFunc != null) {
          widget.onSelectedFunc(itemBean);
        }
      },
      child: Container(
        height: 45,
        color: (widget?.lightTheme??false)?Colors.white:ThemeRepository.getInstance().getCardBgColor_21263C(),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.centerLeft,
        child: AnimatedDefaultTextStyle(
          duration: DEFAULT_ANIM_DURATION,
            style: TextStyle(
                color: widget.selectedBean == itemBean
                    ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    : ((widget?.lightTheme??false)?ThemeRepository.getInstance().getCardBgColor_21263C():ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
                fontSize: 14,
                height: 1.2),
          child: Text(
            itemBean?.sname ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 选择城市-GPS栏
///
/// @author: zengxiangxi
/// @createTime: 1/9/21 2:22 PM
/// @specialDemand:
///

enum _GpsStauts {
  loading,
  error,
  complete,
}

class ChooseCityGpsBarWidget extends StatefulWidget {
  final bool lightTheme;
  const ChooseCityGpsBarWidget({Key key, this.lightTheme})
      :super(key: key);
  @override
  _ChooseCityGpsBarWidgetState createState() => _ChooseCityGpsBarWidgetState();
}

class _ChooseCityGpsBarWidgetState extends State<ChooseCityGpsBarWidget> {
  Map<String, Object>  _location;

  _GpsStauts _gpsStauts;
  VgLocation _locationPlugin;

  @override
  void initState() {
    super.initState();
    _getGPS();
  }

  @override
  void dispose() {
    super.dispose();
    if (_locationPlugin != null) _locationPlugin.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {_clickTap();},
      child: Container(
        height: 45,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: _toMainRowWidget(),
      ),
    );
  }

  Widget _toMainRowWidget() {
    return Row(
      children: <Widget>[
        Image.asset(
          "images/map_location_ico.png",
          width: 11.4,
        ),
        SizedBox(
          width: 4.6,
        ),
        Text(
          _getLocationStr() ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: (widget?.lightTheme??false)?ThemeRepository.getInstance().getCardBgColor_21263C():ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
              height: 1.2),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          "GPS定位",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 11, height: 1.2),
        )
      ],
    );
  }

  void _getGPS() {
    _gpsStauts = _GpsStauts.loading;
    _locationPlugin = VgLocation();
    _locationPlugin.requestSingleLocation((locationMap) {
      _location = locationMap;
      if (_checkIsError()) {
        _gpsStauts = _GpsStauts.error;
      } else {
        _gpsStauts = _GpsStauts.complete;
      }
      setState(() {});
    });

  }

  String _getLocationStr() {
    if (_gpsStauts == null) {
      return "GPS定位异常";
    }
    switch (_gpsStauts) {
      case _GpsStauts.loading:
        return "正在获取GPS定位";
        break;

      case _GpsStauts.complete:
        return "${_location["province"]}" +
            "-" +
            "${_location["city"]}" +
            "-" +
            "${_location["district"]}";
        break;

      case _GpsStauts.error:
        return "GPS定位异常，点击重试";
        break;
    }
    return "GPS定位异常";
  }

  bool _checkIsError() {
    if (StringUtils.isEmpty(_location["province"]) ||
        StringUtils.isEmpty(_location["city"]) ||
        StringUtils.isEmpty(_location["district"]) ||
        StringUtils.isEmpty(_location["adCode"])) {
      return true;
    }
    return false;
  }

  void _clickTap() {
    if (_gpsStauts == _GpsStauts.complete) {
      Map map = VgLocationUtils.transLocationToMap(_location);
      if (map == null || map.isEmpty) {
        _errorProcess();
        return;
      }
      RouterUtils.pop(context, result: map);
      return;
    }
    _errorProcess();
  }

  void _errorProcess() {
    _getGPS();
    setState(() {});
  }
}

import 'package:e_reception_flutter/singleton/location_repository/location_repository.dart';
import 'package:e_reception_flutter/singleton/location_repository/vo/area_data.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';

class ChooseCityUtils{

  ///获取省份列表
  static List<ChooseCityListItemBean> getProvinceList(){
    List<ChooseCityListItemBean> _provinceList = List();
    List<LocalProvinceBean> resultProvinceList = LocationRepository.getInstance().allProvinceList();
    if(resultProvinceList != null){
      for(LocalProvinceBean item in resultProvinceList){
        _provinceList?.add(ChooseCityListItemBean(item?.sid,item?.sname));
      }
    }
    return _provinceList;
  }


  ///根据省份获取省份列表
  static List<ChooseCityListItemBean> getCityList(String provinceId){
    if(provinceId == null || provinceId == ""){
      return null;
    }
    Map<String,LocalProvinceBean> allProvinceMap = LocationRepository.getInstance().allProvinceMap();
    if(allProvinceMap == null || allProvinceMap.isEmpty || !allProvinceMap.containsKey(provinceId)){
      return null;
    }
    List<LocalCityBean> resultCityList = allProvinceMap[provinceId]?.city;
    if(resultCityList == null || resultCityList.isEmpty){
      return null;
    }
    List<ChooseCityListItemBean> _cityList = List();
    for(LocalCityBean item in resultCityList){
      _cityList?.add(ChooseCityListItemBean(item?.sid,item?.sname));
    }
    return _cityList;
  }

  ///根据省份，城市获取地区列表
  static List<ChooseCityListItemBean> getDistrictList(String provinceId,String cityId){
    if(provinceId == null || provinceId == "" || cityId == null || cityId == ""){
      return null;
    }
    Map<String,LocalProvinceBean> allProvinceMap = LocationRepository.getInstance().allProvinceMap();
    if(allProvinceMap == null || allProvinceMap.isEmpty || !allProvinceMap.containsKey(provinceId)){
      return null;
    }
    List<LocalCityBean> resultCityList = allProvinceMap[provinceId]?.city;
    if(resultCityList == null || resultCityList.isEmpty){
      return null;
    }
    LocalCityBean currentCity;
    for (LocalCityBean item in resultCityList){
      if(item != null && item.sid == cityId){
        currentCity = item;
        break;
      }
    }
    if(currentCity == null || currentCity.site == null){
      return null;
    }
    List<ChooseCityListItemBean> _districtList = List();
    for(LocalSiteBean item in currentCity.site){
      _districtList?.add(ChooseCityListItemBean(item?.sid,item?.sname,latitude: item?.la,longitude: item?.lo));
    }
    return _districtList;
  }
}
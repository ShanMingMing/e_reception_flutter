import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';

import 'company_search_detail_list_widget.dart';


class CompanySearchDetailViewModel extends BasePagerViewModel<
    CompanyDetailByTypeListItemBean, CompanyDetailByTypeListResponseBean> {

  final GroupPersonCntBean groupBean;

  final CompanySearchDetailListWidgetState state;

  ValueNotifier<StatisticsBean> statisticsValueNotifier;
  ValueNotifier<CompanyDetailByTypeListResponseBean> companyDetailNotifier;


  CompanySearchDetailViewModel(this.state, this.groupBean) : super(state) {
    statisticsValueNotifier = ValueNotifier(null);
    companyDetailNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    Future(() {
      statisticsValueNotifier?.dispose();
    });
    companyDetailNotifier.dispose();
    super.onDisposed();
  }



  @override
  String getBodyData(int page) {
    return null;
  }


  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 1000,
      "groupid": groupBean?.groupid ?? "",
      "groupType": groupBean?.groupType ?? "",
      "searchName": state?.widget?.searchPageValueNotifier?.value ?? "",
      // "searchOthers": state?.companyDetailFilterNotification?.companyDetailFilterValueNotifier?.value?.getParamsStr() ?? "",
      // "isVisitor": groupBean?.visitor ?? "",
      // "visitorGroupId": groupBean?.visitorGroupId ?? "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appCompanyPages";
  }

  @override
  CompanyDetailByTypeListResponseBean parseData(VgHttpResponse resp) {
    CompanyDetailByTypeListResponseBean vo =
    CompanyDetailByTypeListResponseBean.fromMap(resp?.data);
    loading(false);
    // if (!isStateDisposed) {
    //   statisticsValueNotifier?.value = vo?.data?.statistics;
    // }
    // if (groupBean.isAllGroup()) {
    //   state?.companyDetailFilterNotification?.statisticsValueNotifier?.value =
    //   (vo?.data?.statistics);
    // }
    companyDetailNotifier?.value = vo;
    return vo;
  }

  @override
  void refresh() {
    if (isStateDisposed) {
      return;
    }
    super.refresh();
  }

  @override
  void refreshMultiPage() {
    if (isStateDisposed) {
      return;
    }
    super.refreshMultiPage();
  }
}

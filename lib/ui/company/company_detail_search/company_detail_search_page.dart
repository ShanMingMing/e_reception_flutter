import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_search_list_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/company_detail_by_type_list_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';

import 'company_search_detail_list_widget.dart';

/// 企业详情搜索页
///
/// @author: zengxiangxi
/// @createTime: 3/22/21 6:39 PM
/// @specialDemand:
class CompanyDetailSearchPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CompanyDetailSearchPage";

  @override
  _CompanyDetailSearchPageState createState() =>
      _CompanyDetailSearchPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CompanyDetailSearchPage(),
      routeName: CompanyDetailSearchPage.ROUTER,
    );
  }
}

class _CompanyDetailSearchPageState extends State<CompanyDetailSearchPage> {
  ValueNotifier<String> searchPageValueNotifier;

  @override
  void initState() {
    super.initState();
    searchPageValueNotifier = ValueNotifier(null);
  }

  @override
  void dispose() {
    searchPageValueNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xFF22263a),
        backgroundColor:ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: ScreenUtils.getStatusBarH(context),
          ),
          _toSearchBarWidget(),
          Expanded(
            child: _toListWidget(),
          )
        ],
      ),
    );
  }

  Widget _toSearchBarWidget() {
    return Container(
      height: 44,
      child: Row(
        children: <Widget>[
          Expanded(
            child: CommonSearchBarWidget(
                hintText: "输入姓名",
                autoFocus: true,
                margin: const EdgeInsets.only(left: 15),
                onChanged: (String searchStr) {
                  searchPageValueNotifier.value = searchStr?.trim();
                },
                onSubmitted: (String searchStr) {
                  searchPageValueNotifier.value = searchStr?.trim();
                }),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              RouterUtils.pop(context);
            },
            child: Container(
              width: 60,
              height: 30,
              child: Center(
                child: Text(
                  "取消",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Color(0xFFD0E0F7), fontSize: 15, height: 1.2),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _toListWidget() {
    return CompanySearchDetailListWidget(
      groupInfoBean: GroupPersonCntBean()..visitor = "01",
      searchPageValueNotifier: searchPageValueNotifier,
    );
  }

  // Widget _toListWidget() {
  //   return CompanyDetailByTypeListWidget(
  //     groupInfoBean: GroupPersonCntBean()..visitor = "01",
  //     searchPageValueNotifier: searchPageValueNotifier,
  //     padding: const EdgeInsets.all(0),
  //   );
  // }
}

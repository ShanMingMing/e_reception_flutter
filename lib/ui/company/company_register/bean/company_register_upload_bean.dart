import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_type_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class CompanyRegisterUploadBean {
  ///地点（用于比对）
  ///地点储存值
  String locaiton;
  Map<String, ChooseCityListItemBean> locationMap;

  ///公司昵称 品牌名称
  String companyNick;

  ///门店名称
  String storeName;

  ///姓名
  String name;

  ///地址
  String address;

  ///gps
  String gps;

  ///手机号
  String phone;

  ///公司名称
  String companyName;

  ///详细地址
  String detailAddress;

  ///验证码
  String authCode;

  // ///注册类型
  // String type;

  ListBean typeBean;

  ///备注信息
  String backup;

  ///功能类型
  String registionedition;

  CompanyRegisterUploadBean(
      {this.locaiton,
      this.locationMap,
      this.companyNick,
      this.name,
      this.phone,
      this.companyName,
      this.detailAddress,
      this.authCode,
      // this.type,
      this.typeBean,
      this.registionedition,this.backup});

  ///设置值
  setValue(
      {String locaiton,
      Map<String, ChooseCityListItemBean> locationMap,
      String companyNick,
      String storeName,
      String name,
      String address,
      String gps,
      String phone,
      String companyName,
      String detailAddress,
      String authCode,
      // String type,
      ListBean typeBean,
      String registionedition,String backup}) {
    this.locaiton = locaiton;
    this.locationMap = locationMap;
    this.companyName = companyName;
    this.storeName = storeName;
    this.companyNick = companyNick;
    this.detailAddress = detailAddress;
    this.name = name;
    this.address = address;
    this.gps = gps;
    this.phone = phone;
    this.authCode = authCode;
    // this.type = type;
    this.typeBean = typeBean;
    this.registionedition = registionedition;
    this.backup = backup;
  }

  ///是否检查通过
  bool isAlive() {
    //demand  全称非必填
    if(typeBean?.isLikeSmartHome()??false){
      if (StringUtils.isEmpty(companyNick) || StringUtils.isEmpty(storeName)) {
        return false;
      }
    }else{
      if (StringUtils.isEmpty(locaiton) ||
          StringUtils.isEmpty(detailAddress) ||
          StringUtils.isEmpty(companyNick) ||
          locationMap == null) {
        return false;
      }
    }
    return true;
  }

  bool isOtherAlive() {
    //demand  全称非必填
    if (StringUtils.isEmpty(name) ||
        StringUtils.isEmpty(phone) ||
        StringUtils.isEmpty(authCode)
       ) {
      return false;
    }
    if (phone.length != DEFAULT_PHONE_LENGTH_LIMIT ||
        authCode.length != DEFAULT_AUTH_LENGTH_LIMIT) {
      return false;
    }
    return true;
  }

  ///检查校验
  //demand  全称非必填
  String checkVerify(){
    if(typeBean?.isLikeSmartHome()??false){
      if(StringUtils.isEmpty(companyNick)){
        return "品牌名称不能为空";
      }
      if(StringUtils.isEmpty(storeName)){
        return "门店名称不能为空";
      }
      return null;
    }else{
      if(StringUtils.isEmpty(companyNick)){
        return "企业/单位简称不能为空";
      }
      if(StringUtils.isEmpty(locaiton) || locationMap.isEmpty){
        return "所在城市/区县不能为空";
      }
      if(StringUtils.isEmpty(detailAddress)){
        return "详细地址不能为空";
      }
      return null;
    }

  }

  String newCheckVerify(){
    if(StringUtils.isEmpty(name)){
      return "姓名不能为空";
    }
    if(StringUtils.isEmpty(phone)){
      return "手机号不能为空";
    }
    if(phone.length != DEFAULT_PHONE_LENGTH_LIMIT){
      return "手机号位数不正确";
    }
    if(StringUtils.isEmpty(authCode)){
      return "验证码不能为空";
    }
    if(authCode.length != DEFAULT_AUTH_LENGTH_LIMIT){
      return "验证码位数不正确";
    }
    return null;
  }

  // String getShowStr(){
  //   if(type != null){
  //     if(type =="00"){//民营企业
  //       return "民营企业";
  //     }
  //     if(type=="01"){//培训机构
  //       return "培训机构";
  //     }
  //     if(type =="02"){//党政机关
  //       return "党政机关";
  //     }
  //     if(type=="03"){//大中小学
  //       return "大中小学";
  //     }
  //     if(type =="04"){//国有企业
  //       return "国有企业";
  //     }
  //     if(type=="05"){//外资企业
  //       return "外资企业";
  //     }
  //   }
  // }

  @override
  bool operator ==(Object other) => false;

  // other is CompanyRegisterUploadBean &&
  //     runtimeType == other.runtimeType &&
  //     locaiton == other.locaiton &&
  //     companyNick == other.companyNick &&
  //     name == other.name &&
  //     phone == other.phone &&
  //     companyName == other.companyName &&
  //     authCode == other.authCode;

  @override
  int get hashCode => super.hashCode;

  @override
  String toString() {
    return 'CompanyRegisterUploadBean\n{locaiton: $locaiton, locationMap: $locationMap, companyNick: $companyNick, storeName: $storeName, name: $name, address: $address, gps: $gps,phone: $phone, companyName: $companyName, detailAddress: $detailAddress, authCode: $authCode, registionedition: $registionedition}';
  }
}

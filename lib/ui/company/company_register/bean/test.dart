/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"lname":"民营企业","lid":"S03","secondType":[{"lname":"家居行业","lid":"C144"},{"lname":"餐饮行业","lid":"C115"},{"lname":"物业管理","lid":"C120"}]},{"lname":"培训机构","lid":"S01","secondType":[]},{"lname":"国有企业","lid":"S02","secondType":[]},{"lname":"党政机关","lid":"S04","secondType":[]},{"lname":"大中小学","lid":"S05","secondType":[]},{"lname":"外资企业","lid":"S06","secondType":[]}]}
/// extra : null

class Test {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static Test fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Test testBean = Test();
    testBean.success = map['success'];
    testBean.code = map['code'];
    testBean.msg = map['msg'];
    testBean.data = DataBean.fromMap(map['data']);
    testBean.extra = map['extra'];
    return testBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"lname":"民营企业","lid":"S03","secondType":[{"lname":"家居行业","lid":"C144"},{"lname":"餐饮行业","lid":"C115"},{"lname":"物业管理","lid":"C120"}]},{"lname":"培训机构","lid":"S01","secondType":[]},{"lname":"国有企业","lid":"S02","secondType":[]},{"lname":"党政机关","lid":"S04","secondType":[]},{"lname":"大中小学","lid":"S05","secondType":[]},{"lname":"外资企业","lid":"S06","secondType":[]}]

class DataBean {
  List<ListBean> list;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.list = List()..addAll(
      (map['list'] as List ?? []).map((o) => ListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

/// lname : "民营企业"
/// lid : "S03"
/// secondType : [{"lname":"家居行业","lid":"C144"},{"lname":"餐饮行业","lid":"C115"},{"lname":"物业管理","lid":"C120"}]

class ListBean {
  String lname;
  String lid;
  List<SecondTypeBean> secondType;

  static ListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ListBean listBean = ListBean();
    listBean.lname = map['lname'];
    listBean.lid = map['lid'];
    listBean.secondType = List()..addAll(
      (map['secondType'] as List ?? []).map((o) => SecondTypeBean.fromMap(o))
    );
    return listBean;
  }

  Map toJson() => {
    "lname": lname,
    "lid": lid,
    "secondType": secondType,
  };
}

/// lname : "家居行业"
/// lid : "C144"

class SecondTypeBean {
  String lname;
  String lid;

  static SecondTypeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SecondTypeBean secondTypeBean = SecondTypeBean();
    secondTypeBean.lname = map['lname'];
    secondTypeBean.lid = map['lid'];
    return secondTypeBean;
  }

  Map toJson() => {
    "lname": lname,
    "lid": lid,
  };
}
import '../company_intent_feature_page.dart';

///公司类型
enum RegisterCompanyType{
  organ,//党政机关
  national,//国有企业
  Private,//民营企业
  foreign,//外资企业
  school,//大中小学
  retraining,//培训机构
}
extension RegisterCompanyTypeExtension on RegisterCompanyType {

  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case RegisterCompanyType.Private:
        return "00";
      case RegisterCompanyType.retraining:
        return "01";
      case RegisterCompanyType.organ:
        return "02";
      case RegisterCompanyType.school:
        return "03";
      case RegisterCompanyType.national:
        return "04";
      case RegisterCompanyType.foreign:
        return "05";
    }
    return null;
  }

  ///获取类型名称
  static String getShowStrByType(String type){
    if(type == null || type == ""){
      return "-";
    }
    switch(type){
      case "00" :return "民营企业";
      case "01" :return "培训机构";
      case "02" :return "党政机关";
      case "03" :return "大中小学";
      case "04" :return "国有企业";
      case "05" :return "外资企业";
    }
    return "-";
  }

  static String getRegistionFunctionParams(List<SelectedItemVo> list) {
    if(!list[1].isSelected && !list[2].isSelected) return "00";//基础版
    if(list[1].isSelected && list[2].isSelected) return "03";//智慧版
    if(list[1].isSelected) return "02";//人脸版
    if(list[2].isSelected) return "01";//海报版
    return "00";
  }
}
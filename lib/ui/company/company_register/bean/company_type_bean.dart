/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"list":[{"lname":"培训机构","lid":"S01"},{"lname":"国有企业","lid":"S02"},{"lname":"民营企业","lid":"S03"},{"lname":"党政机关","lid":"S04"},{"lname":"大中小学","lid":"S05"},{"lname":"外资企业","lid":"S06"}]}
/// extra : null

class CompanyTypeBean {
  bool success;
  String code;
  String msg;
  TypeBean data;
  dynamic extra;

  static CompanyTypeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyTypeBean companyTypeBeanBean = CompanyTypeBean();
    companyTypeBeanBean.success = map['success'];
    companyTypeBeanBean.code = map['code'];
    companyTypeBeanBean.msg = map['msg'];
    companyTypeBeanBean.data = TypeBean.fromMap(map['data']);
    companyTypeBeanBean.extra = map['extra'];
    return companyTypeBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// list : [{"lname":"培训机构","lid":"S01"},{"lname":"国有企业","lid":"S02"},{"lname":"民营企业","lid":"S03"},{"lname":"党政机关","lid":"S04"},{"lname":"大中小学","lid":"S05"},{"lname":"外资企业","lid":"S06"}]

class TypeBean {
  List<ListBean> list;

  static TypeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TypeBean dataBean = TypeBean();
    dataBean.list = List()..addAll(
        (map['list'] as List ?? []).map((o) => ListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "list": list,
  };
}

/// lname : "培训机构"
/// lid : "S01"

class ListBean {
  String lname;
  String lid;
  String type;
  List<ListBean> secondType;

  ///获取注册时上传的type参数
  String getType(){
    return type;
  }

  ///获取注册时上传的trade参数
  String getTrade(){
    if("C144" == lid || "C115" == lid || "C120" == lid || "00" == lid){
      return lid;
    }
    return "";
  }

  ///是否类似智能家居类的民营企业
  bool isLikeSmartHome(){
    // return ("C144" == lid || "C115" == lid || "C120" == lid);
    return ("C144" == lid);
  }




  ListBean({this.lname, this.lid, this.type});

  static ListBean fromMap(Map<String, dynamic> map, {String type}) {
    if (map == null) return null;
    ListBean listBean = ListBean();
    listBean.lname = map['lname'];
    listBean.lid = map['lid'];
    if(type != null){
      listBean.type = type;
    }
    listBean.secondType = List()..addAll(
        (map['secondType'] as List ?? []).map((o) => ListBean.fromMap(o, type: map['lid']))
    );
    return listBean;
  }

  Map toJson() => {
    "lname": lname,
    "lid": lid,
    "secondType": secondType,
  };
}
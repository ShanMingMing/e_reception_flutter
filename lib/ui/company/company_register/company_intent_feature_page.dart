import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../app_main.dart';
import 'bean/company_register_upload_bean.dart';
import 'bean/register_type_utils.dart';
import 'company_register_view_model.dart';

class CompanyIntentFeaturePage extends StatefulWidget {
  static const String ROUTER = "CompanyIntentFeaturePage";

  final CompanyRegisterUploadBean itemBean;

  const CompanyIntentFeaturePage({Key key, this.itemBean}) : super(key: key);

  @override
  _CompanyIntentFeaturePageState createState() => _CompanyIntentFeaturePageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, CompanyRegisterUploadBean itemBean) {
    return RouterUtils.routeForFutureResult(context,
        CompanyIntentFeaturePage(
            itemBean: itemBean
        ),
        routeName: CompanyIntentFeaturePage.ROUTER
    );
  }
}

class _CompanyIntentFeaturePageState extends BaseState<CompanyIntentFeaturePage> {

  CompanyRegisterViewModel _viewModel;

  ValueNotifier<CompanyRegisterUploadBean> _editsValueNotifier;

  CompanyRegisterUploadBean _itemBean;

  TextEditingController _editingController = TextEditingController();

  List<SelectedItemVo> _selectList = [
    SelectedItemVo("基础功能包", "终端显示屏管理、自传视频图片播放、AI海报等", false, isMustSelect: true),
    SelectedItemVo("人脸识别考勤", "开启人脸识别功能，考勤打卡一目了然，方便内部管理", false),
    SelectedItemVo("智能海报推送", "根据所在单位特性，智能合成与之匹配的宣传海报", false),
  ];
  FocusNode focusNode = FocusNode();
  @override
  void initState() {
    super.initState();
    _viewModel = CompanyRegisterViewModel(this);
    _itemBean = widget?.itemBean;
    _editsValueNotifier = ValueNotifier(null);
    _editsValueNotifier?.value = _itemBean;
  }

  @override
  void dispose() {
    super.dispose();
    _editsValueNotifier?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Container(
        child: GestureDetector(
          onTap: (){
            focusNode.unfocus();
          },
          child: Column(
            children: <Widget>[
              _toTopBarWidget(),
              _toBlueBoxWidget(),
              _toScrollWidget(),
              _toEditWidget(),
              _toConfirmButtonWidget(),
            ],
          ),
        ),
      ),
    );
  }

  void hideKeyBoard(){
    FocusScope.of(context).requestFocus(FocusNode());
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      centerWidget: _toCenterTextWidget(),
    );
  }

  Widget _toCenterTextWidget() {
    return GestureDetector(
      onTap: (){
        focusNode.unfocus();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "4",
            style: TextStyle(
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              fontSize: 20,
            ),
          ),
          Container(
            height: 18,
            padding: EdgeInsets.only(top: 5),
            child: Text(
              "/4",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 13,
              ),
            ),
          ),
          SizedBox(width: 5,),
          Container(
            height: 24,
            child: Text(
              "意向功能",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 17,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _toBlueBoxWidget(){
    return GestureDetector(
      onTap: (){
        focusNode.unfocus();
      },
      child: Container(
        height: 30,
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 25, top: 7, bottom: 7),
        margin: EdgeInsets.only(bottom: 10),
        color: Color(0xFF1890FF).withOpacity(0.2),
        child: Text((_itemBean?.typeBean?.lname??"-") +
            "/" + _itemBean?.companyNick + "/" + _itemBean?.name,
          style: TextStyle(
            fontSize: 12,
            color: Color(0xFF1890FF),
          ),
        ),
      ),
    );
  }

  ///滚动主体
  Widget _toScrollWidget() {
    return GestureDetector(
      onTap: (){
        focusNode.unfocus();
      },
      child: Container(
        height: 225,
        child: ListView.builder(
          padding: const EdgeInsets.all(0),
          physics: BouncingScrollPhysics(),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          itemCount: _selectList.length,
          itemBuilder: (context, index) => _selectedWidget(item: _selectList[index],
            onClickItem: () {
              focusNode.unfocus();
              _selectList[index].isSelected = !_selectList[index].isSelected;
              setState(() { });
            }
          ),
        ),
      ),
    );
  }

  ///意见编辑
  Widget _toEditWidget() {
    return Container(
      height: 80,
      margin: const EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 30),
      padding: const EdgeInsets.only(left: 15),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          borderRadius: BorderRadius.circular(4)
      ),
      child: VgTextField(
        focusNode: focusNode,
        controller: _editingController,
        keyboardType: TextInputType.text,
        onChanged: (val) {
          if (!StringUtils.isEmpty(_editingController.text) &&
              _editingController.text.length >= 200) {
            toast("不能超过200个字");
            return;
          }

          _itemBean.backup = val;
        },
        style: TextStyle(
            color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
            fontSize: 14),
        decoration: new InputDecoration(
            counterText: "",
            hintText: "你还希望ai前台帮你实现哪些功能？",
            border: InputBorder.none,
            hintStyle: TextStyle(
                fontSize: 14,
                color: ThemeRepository.getInstance().getHintGreenColor_5E687C())),
        inputFormatters: <TextInputFormatter> [
          LengthLimitingTextInputFormatter(200)
        ],
        maxLines: 5,
      ),
    );
  }

  ///按钮
  Widget _toConfirmButtonWidget() {
    //是否启用灰色
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, CompanyRegisterUploadBean value,
          Widget child) {
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: true,
          width: VgToolUtils.getScreenWidth(),
          height: 40,
          margin: const EdgeInsets.only(left: 25, right: 25, bottom: 30),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
            // fontWeight: FontWeight.w600
          ),
          text: "提交",
          onTap: (){
            focusNode.unfocus();
            // RouterUtils.popUntil(context, AppMain.ROUTER);
            _itemBean.registionedition =RegisterCompanyTypeExtension.getRegistionFunctionParams(_selectList);
            _viewModel.companyRegisterHttp(context,value);
          },
        );
      },
    );
  }
}

class SelectedItemVo {
  final String title;
  final String hintText;
  bool isSelected;
  final bool isMustSelect;

  SelectedItemVo(this.title, this.hintText, this.isSelected, {this.isMustSelect = false});
}

class _selectedWidget extends StatelessWidget {

  final SelectedItemVo item;
  final VoidCallback onClickItem;

  const _selectedWidget({Key key,this.item, this.onClickItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onClickItem ?? (){},
      child: Container(
        margin: EdgeInsets.only(top: 10, bottom: 20, left: 25, right: 25),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            item.isMustSelect == true
              ? CommonSingleChoiceButtonWidget(CommonSingleChoiceStatus.disable)
              : CommonSingleChoiceButtonWidget(
              item.isSelected ? CommonSingleChoiceStatus.selected:CommonSingleChoiceStatus.unSelect,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        item.title ?? "",
                        style: TextStyle(
                          fontSize: 16,
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                        ),
                      ),
                    ),
                    SizedBox(height: 1),
                    Text(
                      item.hintText ?? "",
                      softWrap: true,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 12,
                        color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

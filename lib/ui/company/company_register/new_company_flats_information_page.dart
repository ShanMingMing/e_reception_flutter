import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/widgets/new_company_information_edits_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'bean/company_type_bean.dart';
import 'bean/register_type_utils.dart';
import 'company_authentication_page.dart';
import 'company_register_view_model.dart';

/// 新企业注册页

class NewCompanyFlatsInformationPage extends StatefulWidget {
  static const String ROUTER = "NewCompanyFlatsInformationPage";
  final ListBean typeBean;

  const NewCompanyFlatsInformationPage({Key key, this.typeBean}) : super(key: key);

  @override
  _NewCompanyFlatsInformationPageState createState() => _NewCompanyFlatsInformationPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,ListBean typeBean) {
    return RouterUtils.routeForFutureResult(context, NewCompanyFlatsInformationPage(
      typeBean: typeBean,
    ),
        routeName: NewCompanyFlatsInformationPage.ROUTER);
  }
}

class _NewCompanyFlatsInformationPageState extends BaseState<NewCompanyFlatsInformationPage> {
  ValueNotifier<CompanyRegisterUploadBean> _editsValueNotifier;

  CompanyRegisterViewModel _viewModel;


  @override
  void initState() {
    super.initState();
    _viewModel = CompanyRegisterViewModel(this);
    _editsValueNotifier = ValueNotifier(null);
  }

  @override
  void dispose() {
    super.dispose();
    _editsValueNotifier?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: <Widget>[
          _toTopBarWidget(),
          _toBlueBoxWidget(),
          _toScrollWidget()
        ],
      ),
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      centerWidget: _toCenterTextWidget(),
    );
  }

  Widget _toCenterTextWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "2",
          style: TextStyle(
            color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            fontSize: 20,
          ),
        ),
        Container(
          height: 18,
          padding: EdgeInsets.only(top: 4),
          child: Text(
            "/${(widget?.typeBean?.isLikeSmartHome()??false)?3:4}",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 13,
            ),
          ),
        ),
        SizedBox(width: 5,),
        Container(
          height: 24,
          child: Text(
            (widget?.typeBean?.isLikeSmartHome()??false)?"门店信息":"单位信息",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 17,
            ),
          ),
        )
      ],
    );
  }

  Widget _toBlueBoxWidget(){
    return Container(
      height: 30,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 25, top: 7, bottom: 7),
      color: Color(0xFF1890FF).withOpacity(0.2),
      child: Text(
        widget?.typeBean?.lname??"-",
        style: TextStyle(
          fontSize: 12,
          color: Color(0xFF1890FF),
        ),
      ),
    );
  }

  ///滚动主体
  Widget _toScrollWidget() {
    return Expanded(
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: ListView(
          padding: const EdgeInsets.all(0),
          physics: BouncingScrollPhysics(),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          children: <Widget>[
            SizedBox(height: 10),
            _toEditsWidget(),
            SizedBox(height: 30),
            _toConfirmButtonWidget(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  ///编辑相关组件
  Widget _toEditsWidget() {
    return NewCompanyInformationEditsWidget(
      onChanged: _editsValueNotifier,
      typeBean: widget?.typeBean,
    );
  }

  ///按钮
  Widget _toConfirmButtonWidget() {
    //是否启用灰色
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, CompanyRegisterUploadBean value,
          Widget child) {
        final bool isAlive = value?.isAlive() ?? false;
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: isAlive,
          width: VgToolUtils.getScreenWidth(),
          height: 40,
          margin: const EdgeInsets.symmetric(horizontal: 25),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
            // fontWeight: FontWeight.w600
          ),
          text: "下一步",
          onTap: (){
            String msg = value?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              print("type:" + _editsValueNotifier?.value?.typeBean?.toJson().toString());
              CompanyAuthenticationPage.navigatorPush(context, _editsValueNotifier?.value);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

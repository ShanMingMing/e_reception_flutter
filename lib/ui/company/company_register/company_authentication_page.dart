import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_sms_code_widget/service/common_sms_code_service.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/widgets/company_authentication_edits_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/register_type_utils.dart';
import 'company_intent_feature_page.dart';
import 'company_register_view_model.dart';
import 'widgets/company_register_edits_widget.dart';

/// 企业注册页
///
/// @author: zengxiangxi
/// @createTime: 1/5/21 2:10 PM
/// @specialDemand:
class CompanyAuthenticationPage extends StatefulWidget {
  static const String ROUTER = "CompanyAuthenticationPage";

  final CompanyRegisterUploadBean itemBean;

  const CompanyAuthenticationPage({Key key, this.itemBean}) : super(key: key);

  @override
  _CompanyAuthenticationPageState createState() => _CompanyAuthenticationPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, CompanyRegisterUploadBean itemBean) {
    return RouterUtils.routeForFutureResult(context,
      CompanyAuthenticationPage(
       itemBean: itemBean
      ),
        routeName: CompanyAuthenticationPage.ROUTER
    );
  }
}

class _CompanyAuthenticationPageState extends BaseState<CompanyAuthenticationPage> {
  ValueNotifier<CompanyRegisterUploadBean> _editsValueNotifier;

  CompanyRegisterUploadBean _itemBean;

  CompanyRegisterViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CompanyRegisterViewModel(this);
    _itemBean = widget?.itemBean;
    _editsValueNotifier = ValueNotifier(null);
  }

  @override
  void dispose() {
    super.dispose();
    _editsValueNotifier?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: <Widget>[
          _toTopBarWidget(),
          _toBlueBoxWidget(),
          _toScrollWidget()
        ],
      ),
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      centerWidget: _toCenterTextWidget(),
    );
  }

  Widget _toCenterTextWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "3",
          style: TextStyle(
            color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            fontSize: 20,
          ),
        ),
        Container(
          height: 18,
          padding: EdgeInsets.only(top: 4),
          child: Text(
            "/${(_itemBean?.typeBean?.isLikeSmartHome()??false)?3:4}",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 13,
            ),
          ),
        ),
        SizedBox(width: 5,),
        Container(
          height: 24,
          child: Text(
            "身份验证",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 17,
            ),
          ),
        )
      ],
    );
  }

  Widget _toBlueBoxWidget(){
    String title = (_itemBean?.typeBean?.lname??"-") +
        "/" + _itemBean?.companyNick;
    if(_itemBean?.typeBean?.isLikeSmartHome()??false){
      title = "${title}（${_itemBean?.storeName}）";
    }
    return Container(
      height: 30,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 25, top: 7, bottom: 7),
      color: Color(0xFF1890FF).withOpacity(0.2),
      child: Text(title,
        style: TextStyle(
          fontSize: 12,
          color: Color(0xFF1890FF),
        ),
      ),
    );
  }

  ///滚动主体
  Widget _toScrollWidget() {
    return Expanded(
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: ListView(
          padding: const EdgeInsets.all(0),
          physics: BouncingScrollPhysics(),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          children: <Widget>[
            SizedBox(height: 10),
            _toEditsWidget(),
            SizedBox(height: 30),
            _toConfirmButtonWidget(),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  ///编辑相关组件
  Widget _toEditsWidget() {
    return CompanyAuthenticationEditsWidget(
      onChanged: _editsValueNotifier,
      itemBean: _itemBean,
    );
  }

  ///按钮
  Widget _toConfirmButtonWidget() {
    //是否启用灰色
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, CompanyRegisterUploadBean value,
          Widget child) {
        final bool isAlive = value?.isOtherAlive() ?? false;
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: isAlive,
          width: VgToolUtils.getScreenWidth(),
          height: 40,
          margin: const EdgeInsets.symmetric(horizontal: 25),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
            // fontWeight: FontWeight.w600
          ),
          text: (_itemBean?.typeBean?.isLikeSmartHome()??false)?"完成":"下一步",
          onTap: (){
            String msg = value?.newCheckVerify();
            if(StringUtils.isNotEmpty(msg)){
              VgToastUtils.toast(context, msg);
              return;
            }
            loading(true);
            new CommonSmsCodeService().checkSmsCode(
                _itemBean?.phone,
                _itemBean?.authCode,
                BaseCallback(
                onSuccess: (val) {
                  loading(false);
                  if(_itemBean?.typeBean?.isLikeSmartHome()??false){
                    _viewModel.companyRegisterHttp(context,value);
                  }else{
                    CompanyIntentFeaturePage.navigatorPush(context, _editsValueNotifier?.value);
                  }
                },
                onError: (msg) {
                  loading(false);
                  toast(msg);
                }));
          },
        );
      },
    );
  }
}

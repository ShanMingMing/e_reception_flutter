import 'dart:io';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/location_repository/location_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/company/choose_city/choose_city_page.dart';
import 'package:e_reception_flutter/ui/company/choose_city/widgets/choose_city_tabs_and_pages_widget.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_type_bean.dart';
import 'package:e_reception_flutter/ui/index/even/location_changed_event.dart';
import 'package:e_reception_flutter/utils/address_about_utils.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';

import 'company_register_sms_code_widget.dart';

/// 新企业注册-编辑集合组件

class NewCompanyInformationEditsWidget extends StatefulWidget {
  final ListBean typeBean;
  final ValueNotifier<CompanyRegisterUploadBean> onChanged;

  const NewCompanyInformationEditsWidget({Key key, this.onChanged, this.typeBean}) : super(key: key);

  @override
  _NewCompanyInformationEditsWidgetState createState() =>
      _NewCompanyInformationEditsWidgetState();
}

class _NewCompanyInformationEditsWidgetState
    extends State<NewCompanyInformationEditsWidget> {
  Map<String, ChooseCityListItemBean> _resultLocationMap;

  TextEditingController _companyNickEditingController;
  TextEditingController _companyNameEditingController;
  TextEditingController _locationEditingController;
  TextEditingController _detailAddressEditingController;

  CompanyRegisterUploadBean _uploadBean;

  String registrationType;
  //是否类似智能家居类的民营企业
  bool _isLikeSmartHome;

  String gpsFrist;

  @override
  void dispose() {
    _companyNickEditingController?.dispose();
    _companyNameEditingController?.dispose();
    _locationEditingController?.dispose();
    _detailAddressEditingController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _resultLocationMap = Map<String, ChooseCityListItemBean>();
    registrationType = widget?.typeBean?.lid;
    _locationEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
    _companyNickEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
    _companyNameEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
    _detailAddressEditingController = TextEditingController()
      ..addListener(() => _notifyChange());

    VgEventBus.global.streamController.stream.listen((event) {
      if (event is LocationChangedEvent) {
        print("收到gps更新的:" + event?.gps);
        if(StringUtils.isNotEmpty(event?.gps??"")){
          LatLngBean latLngBean = LatLngBean();
          latLngBean = event?.latLngBean;
          gpsFrist = event?.gps;
          gpsFrist = AddressAboutUtils.getLongitudeAndLatitude(latLngBean?.longitude?.toString(), latLngBean?.latitude?.toString());
        }
      }
    });
    AmapRepository.getInstance().init(() {
      VgLocation().requestSingleLocation((map) {
        var transResult = VgLocationUtils.transLocationToMap(map);
        if (transResult == null) {
          return;
        }
        _resultLocationMap = transResult;
        _setLocationEditingValue();
        _notifyChange();
        // _getGpsStr();
      });

    });

    _type();
  }

  void _type(){
    if(widget?.typeBean?.lid!=null){
      if(registrationType=="S02" || registrationType=="S03" || registrationType=="S06"
       || registrationType=="C115" || registrationType=="C120" || registrationType=="00"){
        registrationType = "企业"; //国有企业//民营企业//外资企业
      }
      if(registrationType == "S04"){//党政机关
        registrationType = "单位";
      }
      if(registrationType=="S05"){//大中小学
        registrationType = "学校";
      }
      if(registrationType=="S01"){//培训机构
        registrationType = "机构";
      }
      // _isLikeSmartHome = ("C115" == registrationType || "C144" == registrationType || "C120" == registrationType);
      _isLikeSmartHome = ("C144" == registrationType);
    }
  }

  ///通知更新
  void _notifyChange() {
    if (widget?.onChanged == null) {
      return;
    }
    if (_uploadBean == null) {
      _uploadBean = CompanyRegisterUploadBean();
    }
    _uploadBean.typeBean = widget?.typeBean;
    _uploadBean.locaiton = _locationEditingController?.text?.trim();
    _uploadBean.locationMap = _resultLocationMap;
    _uploadBean.companyNick = _companyNickEditingController?.text?.trim();
    if(_isLikeSmartHome??false){
      _uploadBean.storeName = _companyNameEditingController?.text?.trim();
      if(_uploadBean.companyNick.length > 10){
        _uploadBean.companyNick = _uploadBean.companyNick.substring(0, 10);
      }
      if(_uploadBean.storeName.length > 10){
        _uploadBean.storeName = _uploadBean.storeName.substring(0, 10);
      }
    }else{
      _uploadBean.companyName = _companyNameEditingController?.text?.trim();
    }

    _uploadBean.detailAddress = _detailAddressEditingController?.text?.trim();
    _uploadBean.gps= (_getGpsStr()?.contains("null") ?? false) ? gpsFrist:_getGpsStr() ?? gpsFrist;
    widget?.onChanged?.value = _uploadBean;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 25),
        child: _toMainColumnWidget());
  }

  Widget _toMainColumnWidget() {
    String title1 = "${registrationType ?? "企业"}简称";
    String title2 = "${registrationType ?? "企业"}全称";
    String hint1 = "请输入";
    String hint2 = "选填";
    int maxLength1 = 12;
    int maxLength2 = 30;
    //针对家居这类民营企业作特殊处理
    if(_isLikeSmartHome??false){
      title1 = "品牌名称";
      title2 = "门店名称";
      hint1 = "请输入（必填）";
      hint2 = "如：居然之家十里河店（必填）";
      maxLength1 = Platform.isAndroid?10:null;
      maxLength2= Platform.isAndroid?10:null;
    }

    return Column(
      children: <Widget>[
        //企业单位简称组件
        _EditTextWithTitleWidget(
          controller: _companyNickEditingController,
          title: title1,
          hintText: hint1,
          maxZHCharLimit: maxLength1,
        ),
        SizedBox(height: 15),
        //企业单位全称
        _EditTextWithTitleWidget(
          controller: _companyNameEditingController,
          title: title2,
          hintText: hint2,
          maxZHCharLimit: maxLength2,
        ),
        SizedBox(height: 15),
        //所在城市
        _EditTextWithTitleWidget(
          controller: _locationEditingController,
          title: "所在区县",
          hintText: "请选择",
          isShowGoIcon: true,
          toJumpCallback: () async {
            HudUtils.showLoading(context,true);
            LocationRepository.getInstance().init(callback: (success)async{
              HudUtils.showLoading(context,false);
              Map<String, ChooseCityListItemBean> result =
              await ChooseCityPage.navigatorPush(context,
                  subLevel: 3, selected: _resultLocationMap);
              if (result == null || result.isEmpty) {
                return;
              }
              _resultLocationMap = result;
              _setLocationEditingValue();
            });

          },
        ),
        SizedBox(height: 15),
        //详细地址
        _EditTextWithTitleWidget(
          controller: _detailAddressEditingController,
          title: "详细地址",
          hintText: "请输入",
          maxZHCharLimit: 30,
        ),
      ],
    );
  }

  void _setLocationEditingValue() {
    _locationEditingController?.text = _getAddressStr();
  }

  String _getAddressStr() {
    if (_resultLocationMap == null || _resultLocationMap.isEmpty) {
      return null;
    }
    String province = _resultLocationMap[CHOOSE_PROVINCE_KEY]?.sname;
    String city = _resultLocationMap[CHOOSE_CITY_KEY]?.sname;
    String dirstrict = _resultLocationMap[CHOOSE_DISTRICT_KEY]?.sname;
    if(StringUtils.isEmpty(dirstrict)){
      dirstrict = "";
    }
    if (province == city) {
      return "$province$dirstrict";
    } else {
      return "$province$city$dirstrict";
    }
  }

  String _getGpsStr() {
    if (_resultLocationMap == null || _resultLocationMap.isEmpty) {
      return null;
    }
    double   latitude = _resultLocationMap[CHOOSE_DISTRICT_KEY]?.latitude;
    double   longitude = _resultLocationMap[CHOOSE_DISTRICT_KEY]?.longitude;
    String gps = AddressAboutUtils.getLongitudeAndLatitude(longitude?.toString(), latitude?.toString());
    if(StringUtils.isEmpty(gps)){
      return null;
    }
    return gps;
  }
}

class _EditTextWithTitleWidget extends StatefulWidget {
  final String title;

  final String hintText;

  final bool isShowGoIcon;

  final VoidCallback toJumpCallback;

  final TextEditingController controller;

  final int maxZHCharLimit;

  const _EditTextWithTitleWidget(
      {Key key,
        this.title,
        this.hintText,
        this.isShowGoIcon = false,
        this.toJumpCallback,
        this.controller, this.maxZHCharLimit})
      : super(key: key);

  @override
  _EditTextWithTitleWidgetState createState() =>
      _EditTextWithTitleWidgetState();
}

class _EditTextWithTitleWidgetState extends State<_EditTextWithTitleWidget> {
  FocusNode _focusNode;

  ValueNotifier<bool> _isShowFocusLineNotifier;

  @override
  void initState() {
    super.initState();
    _isShowFocusLineNotifier = ValueNotifier(false);
    _focusNode = FocusNode();
    _focusNode.addListener(() {
      if (_focusNode?.hasFocus ?? false) {
        _isShowFocusLineNotifier.value = true;
      } else {
        _isShowFocusLineNotifier.value = false;
      }
    });
  }

  @override
  void dispose() {
    _focusNode?.dispose();
    _isShowFocusLineNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 45,
          child: Row(
            children: <Widget>[
              Container(
                width: 75,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.title ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextMinorGreyColor_808388(),
                    fontSize: 14,
                  ),
                ),
              ),
              Expanded(
                child: VgTextField(
                  controller: widget?.controller,
                  maxLines: 1,
                  autofocus: true,
                  focusNode: _focusNode,
                  readOnly: widget.toJumpCallback != null ? true : false,
                  onTap: () {
                    if (widget.toJumpCallback != null) {
                      widget.toJumpCallback();
                    }
                  },
                  maxLimitLength: widget?.maxZHCharLimit,
                  zhCountEqEnCount: true,
                  limitCallback:(int){
                    if((widget?.maxZHCharLimit??0)>0){
                      VgToastUtils.toast(context, "最多可输入${widget?.maxZHCharLimit??0}个字");
                    }
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    counterText: "",
                    contentPadding: const EdgeInsets.only(bottom: 2),
                    hintText: widget.hintText ?? "",
                    hintStyle: TextStyle(
                        color: VgColors.INPUT_BG_COLOR,
                        fontSize: 14),
                  ),
                  style: TextStyle(
                      fontSize: 14,
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()),
                ),
              ),
              Opacity(
                opacity: widget.isShowGoIcon ? 1 : 0,
                child: Container(
                    padding: const EdgeInsets.only(left: 5),
                    child: Image.asset(
                      "images/go_ico.png",
                      width: 6,
                    )),
              )
            ],
          ),
        ),
        ValueListenableBuilder(
          builder: (_, value, Widget child) {
            return AnimatedContainer(
              height: 1,
              duration: DEFAULT_ANIM_DURATION,
              color: value
                  ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                  : Color(0xFF303546),
            );
          },
          valueListenable: _isShowFocusLineNotifier,
        )
      ],
    );
  }
}

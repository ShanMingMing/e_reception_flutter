import 'package:e_reception_flutter/common_widgets/common_sms_code_widget/common_sms_code_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/widgets.dart';

/// 企业注册-验证码组件
///
/// @author: zengxiangxi
/// @createTime: 1/5/21 3:48 PM
/// @specialDemand:
class CompanyRegisterSmsCodeWidget extends StatelessWidget {
  final CommonSmsCodeGetPhoneFunc phoneFunc;
  final bool login;

  const CompanyRegisterSmsCodeWidget({Key key, this.phoneFunc, this.login}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CommonSmsCodeWidget(
        login: login ?? false,
        phoneFunc: phoneFunc,
        child: _toChildWidget(),
        downCustomWidget: (int downCount){
         return _toDownWidget(downCount);
        },
        resetChild: _toResetChildWidget(),
      ),
    );
  }

  ///获取验证码组件
  Widget _toChildWidget(){
    return Container(
      width: 95,
      height: 44,
      // padding: const EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      child: Text(
        "获取验证码",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          fontSize: 13,
        ),
      ),
    );
  }

  ///倒计时组件
  Widget _toDownWidget(int codeCountDown){
    return Container(
      width: 95,
      height: 44,
      // padding: const EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      child: login ?? false ?Text(
        "重新获取（${codeCountDown ?? 0}）",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 13,
        ),
      ) :Text(
        "${codeCountDown ?? 0}s",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 13,
        ),
      ),
    );
  }

  Widget _toResetChildWidget(){
    return Container(
      width: 95,
      height: 44,
      // padding: const EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      child: Text(
        "重新获取",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          fontSize: 13,
        ),
      ),
    );
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 注册成功提示弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/14/21 3:23 PM
/// @specialDemand: "我知道了"后回到登录页，其余不能退出
class CompanyRegisterSuccessDialog extends StatelessWidget {
  ///跳转
  static Future<dynamic> navigatorPushDialog(BuildContext context) {
    return VgDialogUtils.showCommonDialog(
        context: context,
        child: CompanyRegisterSuccessDialog(),
        barrierDismissible: false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: Material(
        type: MaterialType.transparency,
        child: UnconstrainedBox(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: ThemeRepository.getInstance().getCardBgColor_21263C()),
            width: 290,
            child: _toMainColumnWidget(context),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 39,
        ),
        Image.asset(
          "images/company_register_success_dialog_ico.png",
          width: 100,
        ),
        SizedBox(
          height: 24,
        ),
        Text(
          "注册成功",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 17,
              fontWeight: FontWeight.w600),
        ),
        SizedBox(
          height: 6,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Text(
            "您可以在终端显示屏上登录当前手机号完成绑定",
            maxLines: 2,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          height: 40,
        ),
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){
            RouterUtils.pop(context);
            // RouterUtils.popUntil(AppMain.context, AppMain.ROUTER,rootNavigator: false);
          },
          child: Container(
            width: 160,
            height: 45,
            decoration: BoxDecoration(
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                borderRadius: BorderRadius.circular(22.5)),
            child: Center(
              child: Text(
                "我知道了",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white, fontSize: 16, height: 1.2),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/company/choose_city/choose_city_page.dart';
import 'package:e_reception_flutter/ui/company/choose_city/widgets/choose_city_tabs_and_pages_widget.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/index/even/location_changed_event.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'company_register_sms_code_widget.dart';

/// 新企业注册-编辑集合组件

class CompanyAuthenticationEditsWidget extends StatefulWidget {
  final CompanyRegisterUploadBean itemBean;
  final ValueNotifier<CompanyRegisterUploadBean> onChanged;

  const CompanyAuthenticationEditsWidget({Key key,this.itemBean , this.onChanged}) : super(key: key);

  @override
  _CompanyAuthenticationEditsWidgetState createState() =>
      _CompanyAuthenticationEditsWidgetState();
}

class _CompanyAuthenticationEditsWidgetState
    extends State<CompanyAuthenticationEditsWidget> {

  TextEditingController _nameEditingController;
  TextEditingController _phoneEditingController;
  TextEditingController _authCodeEditingController;

  CompanyRegisterUploadBean _uploadBean;

  @override
  void initState() {
    super.initState();
    _uploadBean = widget?.itemBean;
    AmapRepository.getInstance().init();
    _nameEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
    _phoneEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
    _authCodeEditingController = TextEditingController()
      ..addListener(() => _notifyChange());
  }


  @override
  void dispose() {
    _nameEditingController?.dispose();
    _phoneEditingController?.dispose();
    _authCodeEditingController?.dispose();
    super.dispose();
  }



  ///通知更新
  _notifyChange() {
    if (widget?.onChanged == null) {
      return;
    }
    if (_uploadBean == null) {
      _uploadBean = CompanyRegisterUploadBean();
    }
    _uploadBean.name = _nameEditingController?.text?.trim();
    _uploadBean.phone = _phoneEditingController?.text?.trim();
    _uploadBean.authCode = _authCodeEditingController?.text?.trim();
    widget?.onChanged?.value = _uploadBean;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 25),
        child: _toMainColumnWidget());
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _EditWithFrameWidget(
          title: "您的称呼",
          controller: _nameEditingController,
          hintText: "请输入",
          maxZHCharLimit: 10,
          isNumber: false,
        ),
        SizedBox(height: 15),
        _EditWithFrameWidget(
          title: "手机号码",
          controller: _phoneEditingController,
          hintText: "请输入",
          maxNumberLimit: DEFAULT_PHONE_LENGTH_LIMIT,
          maxZHCharLimit: DEFAULT_PHONE_LENGTH_LIMIT,
          isNumber: true,
        ),
        SizedBox(height: 15),
        _EditWithFrameWidget(
          title: "验证码",
          controller: _authCodeEditingController,
          hintText: "请输入",
          isNumber: true,
          maxNumberLimit: DEFAULT_AUTH_LENGTH_LIMIT,
          maxZHCharLimit: DEFAULT_AUTH_LENGTH_LIMIT,
          rightWidget: CompanyRegisterSmsCodeWidget(
            phoneFunc: ()=> _phoneEditingController?.text?.trim(),
          ),
        ),
        SizedBox(height: 15)
      ],
    );
  }
}

class _EditWithFrameWidget extends StatefulWidget {
  final String title;

  final String hintText;

  final bool isNumber;

  final Widget rightWidget;

  final int maxNumberLimit;

  final TextEditingController controller;

  final int maxZHCharLimit;

  const _EditWithFrameWidget(
      {Key key,
        this.title,
        this.hintText,
        this.isNumber = false,
        this.rightWidget,
        this.maxNumberLimit,
        this.controller, this.maxZHCharLimit})
      : super(key: key);

  @override
  __EditWithFrameWidgetState createState() =>
      __EditWithFrameWidgetState();
}

class __EditWithFrameWidgetState extends State<_EditWithFrameWidget> {
  FocusNode _focusNode;

  ValueNotifier<bool> _isShowFocusLineNotifier;

  @override
  void initState() {
    super.initState();
    _isShowFocusLineNotifier = ValueNotifier(false);
    _focusNode = FocusNode();
    _focusNode.addListener(() {
      if (_focusNode?.hasFocus ?? false) {
        _isShowFocusLineNotifier.value = true;
      } else {
        _isShowFocusLineNotifier.value = false;
      }
    });
  }

  @override
  void dispose() {
    _focusNode?.dispose();
    _isShowFocusLineNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 45,
          child: Row(
            children: <Widget>[
              Container(
                width: 75,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.title ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextMinorGreyColor_808388(),
                    fontSize: 14,
                  ),
                ),
              ),
              Expanded(
                child: VgTextField(
                  controller: widget?.controller,
                  maxLines: 1,
                  focusNode: _focusNode,
                  autofocus: true,
                  maxLimitLength: widget?.maxZHCharLimit,
                  keyboardType: widget?.isNumber ? TextInputType.number : null,
                  inputFormatters: widget?.isNumber
                  ? [
                  WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                  widget?.maxNumberLimit != null
                      ? LengthLimitingTextInputFormatter(widget?.maxNumberLimit)
                      : null
                  ]
                  : null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    counterText: "",
                    contentPadding: const EdgeInsets.only(bottom: 2),
                    hintText: widget.hintText ?? "",
                    hintStyle: TextStyle(
                        color: VgColors.INPUT_BG_COLOR,
                        fontSize: 14),
                  ),
                  style: TextStyle(
                      fontSize: 14,
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()),
                ),
              ),
              (widget?.rightWidget) ?? SizedBox(width: 15)
            ],
          ),
        ),
        ValueListenableBuilder(
          builder: (_, value, Widget child) {
            return AnimatedContainer(
              height: 1,
              duration: DEFAULT_ANIM_DURATION,
              color: value
                  ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                  : Color(0xFF303546),
            );
          },
          valueListenable: _isShowFocusLineNotifier,
        )
      ],
    );
  }
}

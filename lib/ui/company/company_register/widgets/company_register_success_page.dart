import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/company/company_switch/company_switch_page.dart';
import 'package:e_reception_flutter/ui/company/company_switch/company_switch_view_model.dart';
import 'package:e_reception_flutter/ui/login/login_bean/login_search_organization_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 注册成功提示页
///

class CompanyRegisterSuccessPage extends StatefulWidget {

  final String phone;

  const CompanyRegisterSuccessPage({Key key, this.phone}) : super(key: key);

  ///跳转方法
  static Future<bool> navigatorPush(BuildContext context,String phone) {
    return RouterUtils.routeForFutureResult<bool>(
      context,
      CompanyRegisterSuccessPage(
          phone:phone
      ),
    );
  }

  @override
  _CompanyRegisterSuccessPageState createState() => _CompanyRegisterSuccessPageState();
}

class _CompanyRegisterSuccessPageState extends BaseState<CompanyRegisterSuccessPage> {
  CompanySwitchViewModel _viewModel;
  List<OrgInfoListBean> orgBeanList;

  @override
  void initState() {
    super.initState();
    orgBeanList = List<OrgInfoListBean>();
    _viewModel = CompanySwitchViewModel(this);
    _viewModel?.valueNotifier?.addListener(() {
      orgBeanList = _viewModel?.valueNotifier?.value;
    });
    _viewModel?.searchOrganization(context, widget?.phone);
    //选择需要导入的机构
  }

  @override
  void dispose() {
    _viewModel?.valueNotifier?.removeListener(() { });
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body:  _toMainColumnWidget(context),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "",
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31()
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _toTopBarWidget(),
        SizedBox(
          height: 50,
        ),
        Image.asset(
          "images/company_register_success_dialog_ico.png",
          width: 100,
        ),
        SizedBox(
          height: 24,
        ),
        Text(
          "注册成功",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 17,
              fontWeight: FontWeight.w600),
        ),
        SizedBox(
          height: 6,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Text(
            "请扫描终端显示屏上的二维码，进行设备绑定",
            maxLines: 2,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          height: 30,
        ),
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){
            RouterUtils.pop(context);
            //选择切换一起学机构
            if((orgBeanList?.length??0)!=0){
              // if((orgBeanList?.length??0)==1){
                if(orgBeanList[0]?.isimport=="00"){
                  CompanySwitchPage.navigatorPush(context,orgInfoList:orgBeanList);
                  // OrgInfoListBean orgBean = OrgInfoListBean();
                  // //倒入机构信息并登录
                  // UserRepository.getInstance()
                  //     .loginService(callback:VgBaseCallback(
                  //   onSuccess: (_) {
                  //     VgHudUtils.hide(context);
                  //   },
                  //   onError: (String msg) {
                  //     VgHudUtils.hide(context);
                  //     VgToastUtils.toast(context, msg);
                  //   },
                  // )).companyImportOrgLogin(orgBean?.phone, "2828",orgBean);
                }
              // }else if((orgBeanList?.length??0)>1){
              //   CompanySwitchPage.navigatorPush(context,orgInfoList:orgBeanList);
              // }
            }

            // RouterUtils.popUntil(AppMain.context, AppMain.ROUTER,rootNavigator: false);
          },
          child: Container(
            width: 300,
            height: 40,
            decoration: BoxDecoration(
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                borderRadius: BorderRadius.circular(22.5)),
            child: Center(
              child: Text(
                "我知道了",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white, fontSize: 16, height: 1.2),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}

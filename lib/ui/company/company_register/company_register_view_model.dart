import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_type_bean.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';

import '../../app_main.dart';


class CompanyRegisterViewModel extends BaseViewModel {
  static const String GET_COMPANY_TYPE_API =ServerApi.BASE_URL + "web/getSysComType";
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  ValueNotifier<TypeBean> companyTypeValueNotifier;
  CompanyRegisterViewModel(BaseState<StatefulWidget> state) : super(state){
    companyTypeValueNotifier = ValueNotifier(null);
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
  }

  @override
  void onDisposed() {
    statusTypeValueNotifier?.dispose();
    companyTypeValueNotifier?.dispose();
    super.onDisposed();
  }

  void getCompanyType(){
    VgHttpUtils.get(GET_COMPANY_TYPE_API,params: {
      "authId":"111",
    },callback: BaseCallback(
        onSuccess: (val){
          CompanyTypeBean bean =
          CompanyTypeBean.fromMap(val);
          if(!isStateDisposed){
            statusTypeValueNotifier?.value = null;
            companyTypeValueNotifier?.value = bean?.data;
          }
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
          statusTypeValueNotifier?.value = PlaceHolderStatusType.error;
        }
    ));
  }




  ///企业注册请求
  void companyRegisterHttp(
      BuildContext context, CompanyRegisterUploadBean uploadBean) {
    if (uploadBean == null || !uploadBean.isAlive()) {
      return;
    }
    VgHudUtils.show(context, "正在注册");
    UserRepository.getInstance()
        .loginService(
        isBackIndexPage: true,
        callback: VgBaseCallback(
          onSuccess: (_) {
            VgHudUtils.hide(context);
            VgToastUtils.toast(context, "登录成功");
          },
          onError: (String msg) {
            VgHudUtils.hide(context);
            VgToastUtils.toast(context, msg);
          },
        ))
        .companyRegisterLogin(uploadBean);
  }
}

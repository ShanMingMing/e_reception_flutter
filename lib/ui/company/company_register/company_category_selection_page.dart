import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_type_bean.dart';
import 'package:e_reception_flutter/ui/company/company_register/company_register_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'company_register_page.dart';
import 'new_company_flats_information_page.dart';

class CompanyCategorySelectionPage extends StatefulWidget {

  @override
  _CompanyCategorySelectionPageState createState() => _CompanyCategorySelectionPageState();
  static Future<dynamic> navigatorPush(
      BuildContext context) {
    return RouterUtils.routeForFutureResult(context,
      CompanyCategorySelectionPage(

      ),
    );
  }
}

class _CompanyCategorySelectionPageState extends BaseState<CompanyCategorySelectionPage> {

  ListBean selectedItem;
  CompanyRegisterViewModel _viewModel;
  List<ListBean> _typeList;
  int _process = 4;

  @override
  void initState() {
    super.initState();
    _typeList = new List();
    _viewModel = new CompanyRegisterViewModel(this);
    _viewModel.getCompanyType();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _categorySelectionPage(),
    );
  }

  Widget _categorySelectionPage() {
    return Column(
      children: [
        _toTopBarWidget(),
        Expanded(child: _toPlaceHolderWidget()),
      ],
    );
  }
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      centerWidget: _toCenterTextWidget(),
    );
  }
  Widget _toCenterTextWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "1",
          style: TextStyle(
            color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            fontSize: 20,
          ),
        ),
        Container(
          height: 18,
          padding: EdgeInsets.only(top: 4),
          child: Text(
            "/${_process.toString()}",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 13,
            ),
          ),
        ),
        SizedBox(width: 5,),
        Container(
          height: 24,
          child: Text(
            "选择类别",
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 17,
            ),
          ),
        )
      ],
    );
  }

  Widget _toPlaceHolderWidget() {
    return ValueListenableBuilder(
        valueListenable: _viewModel?.statusTypeValueNotifier,
        builder:
            (BuildContext context, PlaceHolderStatusType value, Widget child) {
          return VgPlaceHolderStatusWidget(
            loadingStatus: value == PlaceHolderStatusType.loading,
            errorStatus: value == PlaceHolderStatusType.error,
            errorOnClick: () => _viewModel.getCompanyType(),
            loadingOnClick: () => _viewModel.getCompanyType(),
            child: child,
          );
        },
        child: _toInfoWidget());
  }

  Widget _toInfoWidget(){
    return ValueListenableBuilder(
      valueListenable: _viewModel?.companyTypeValueNotifier,
      builder: (BuildContext context, TypeBean typeBean, Widget child){
        _typeList.clear();
        if(typeBean != null && typeBean.list != null){
          typeBean.list.forEach((element) {
            if("S03" == element.lid){
              _typeList.addAll(element.secondType);
              _typeList.add(new ListBean(lname: "其他", lid: "00", type: "S03"));
            }
          });
        }
        return Column(
          children: [
            SizedBox(height: 6),
            _toCategoryTitleWidget("民营企业"),
            _categorySelectionWidget(),
            _toCategoryTitleWidget("教育培训"),
            _toEduWidget(),
            _toCategoryTitleWidget("党政国企"),
            _toDzgqWidget(),
            _toCategoryTitleWidget("其他"),
            _toOtherWidget(),
            SizedBox(height: 20,),
            _submitRegister()
          ],
        );
      },
    );
  }


  ///分类标题
  Widget _toCategoryTitleWidget(String title){
    return Container(
      height: 35,
      padding: EdgeInsets.only(top: 10),
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(left: 15),
      child: Text(
          title,
        style: TextStyle(
          fontSize: 12,
          color: ThemeRepository.getInstance().getSmUnSelectedColor_808388()
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  ///民营企业
  Widget _categorySelectionWidget(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 0,
          bottom: 10),
      itemCount: _typeList?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 11,
        crossAxisSpacing: 10,
        childAspectRatio: 108 / 45,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toListItemWidget(_typeList?.elementAt(index));
      },
    );
  }
  
  ///教育培训
  Widget _toEduWidget(){
    List<ListBean> list = new List();
    list.add(new ListBean(lname: "培训机构", lid: "S01", type: "S01"));
    list.add(new ListBean(lname: "大中小学", lid: "S05", type: "S05"));
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 0,
          bottom: 10),
      itemCount: list?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 11,
        crossAxisSpacing: 10,
        childAspectRatio: 108 / 45,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toListItemWidget(list?.elementAt(index));
      },
    );
  }

  ///党政国企
  Widget _toDzgqWidget(){
    List<ListBean> list = new List();
    list.add(new ListBean(lname: "国有企业", lid: "S02", type: "S02"));
    list.add(new ListBean(lname: "党政机关", lid: "S04", type: "S04"));
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 0,
          bottom: 10),
      itemCount: list?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 11,
        crossAxisSpacing: 10,
        childAspectRatio: 108 / 45,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toListItemWidget(list?.elementAt(index));
      },
    );
  }

  ///其他
  Widget _toOtherWidget(){
    List<ListBean> list = new List();
    list.add(new ListBean(lname: "外资企业", lid: "S06", type: "S06"));
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 0,
          bottom: 10),
      itemCount: list?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 11,
        crossAxisSpacing: 10,
        childAspectRatio: 108 / 45,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toListItemWidget(list?.elementAt(index));
      },
    );
  }
  

  Widget _toListItemWidget(ListBean item){
    double width = (ScreenUtils.screenW(context) - 30 - 20)/3;
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        selectedItem = item;
        if(selectedItem.isLikeSmartHome()){
          _process = 3;
        }else{
          _process = 4;
        }
        setState(() {

        });
      },
      child: Container(
        width: width,
        height: 45,
        alignment: Alignment.center,
        decoration:  BoxDecoration(
            color: (selectedItem?.lid == item?.lid) ? ThemeRepository.getInstance().getPrimaryColor_1890FF().withOpacity(0.1) : Colors.transparent,
            border: Border.all(
                color: (selectedItem?.lid == item?.lid) ? ThemeRepository.getInstance().getPrimaryColor_1890FF() : ThemeRepository.getInstance().getLineColor_3A3F50(),
                width: 1),borderRadius: BorderRadius.circular(4)),
        child: Text(
          item?.lname??"",
          style: TextStyle(
            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
            fontSize: 14,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }

  Widget _submitRegister(){
    return Center(
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: selectedItem != null,
        width: VgToolUtils.getScreenWidth(),
        height: 40,
        margin: const EdgeInsets.symmetric(horizontal: 25),
        unSelectBgColor: Color(0xFF3A3F50),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 15,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 15,
          // fontWeight: FontWeight.w600
        ),
        text: "下一步",
        onTap: (){
          if(selectedItem != null){
            NewCompanyFlatsInformationPage.navigatorPush(context, selectedItem);
          }
        },
      ),
    );
  }
}

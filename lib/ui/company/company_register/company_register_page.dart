import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/bean/company_register_upload_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'company_register_view_model.dart';
import 'widgets/company_register_edits_widget.dart';

/// 企业注册页
///
/// @author: zengxiangxi
/// @createTime: 1/5/21 2:10 PM
/// @specialDemand:
class CompanyRegisterPage extends StatefulWidget {
  static const String ROUTER = "CompanyRegisterPage";
  final String type;

  const CompanyRegisterPage({Key key, this.type}) : super(key: key);

  @override
  _CompanyRegisterPageState createState() => _CompanyRegisterPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,String type) {
    return RouterUtils.routeForFutureResult(context, CompanyRegisterPage(
      type: type,
    ),
        routeName: CompanyRegisterPage.ROUTER);
  }
}

class _CompanyRegisterPageState extends BaseState<CompanyRegisterPage> {
  ValueNotifier<CompanyRegisterUploadBean> _editsValueNotifier;

  CompanyRegisterViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CompanyRegisterViewModel(this);
    _editsValueNotifier = ValueNotifier(null);
  }

  @override
  void dispose() {
    super.dispose();
    _editsValueNotifier?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _toScaffoldWidget();
  }

  Widget _toScaffoldWidget() {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toScrollWidget(),
        )
      ],
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "注册账号",
      isShowBack: true,
    );
  }

  ///滚动主体
  Widget _toScrollWidget() {
    return ListView(
      padding: const EdgeInsets.all(0),
      physics: BouncingScrollPhysics(),
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      children: <Widget>[
        SizedBox(height: 10),
        _toEditsWidget(),
        SizedBox(height: 30),
        _toConfirmButtonWidget(),
        SizedBox(height: 30),
      ],
    );
  }

  ///编辑相关组件
  Widget _toEditsWidget() {
    return CompanyRegisterEditsWidget(
      onChanged: _editsValueNotifier,
      type: widget?.type,
    );
  }

  ///提交按钮
  Widget _toConfirmButtonWidget() {
    //是否启用灰色

    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, CompanyRegisterUploadBean value,
          Widget child) {
        final bool isAlive = value?.isAlive() ?? false;
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: isAlive,
          height: 40,
          margin: const EdgeInsets.symmetric(horizontal: 25),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          text: "提交",
          textSize: 17,
          onTap: (){
            String msg = value?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              // value.type = widget?.type;
              _viewModel.companyRegisterHttp(context,value);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

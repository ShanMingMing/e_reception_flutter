import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/widgets.dart';

/// 切换公司-列表项
///
/// @author: zengxiangxi
/// @createTime: 1/23/21 5:30 PM
/// @specialDemand:
class CompanySwitchListItemWidget extends StatelessWidget {

  final UserComListBean itemBean;

  final String selectCompanyId;

  final ValueChanged<UserComListBean> onTap;

  const CompanySwitchListItemWidget({Key key, this.itemBean, this.onTap, this.selectCompanyId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return _toListItemWidget();
  }

  Widget _toListItemWidget() {
    String companyname = itemBean?.companyname;
    if(companyname==""||companyname==null){
      companyname = "暂无全称";
    }
    final bool isSelected = selectCompanyId == (itemBean?.companyid ?? "-1");
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => onTap?.call(itemBean),
      child: Container(
        alignment: Alignment.center,
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        height: 70,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          children: <Widget>[
             Column(
               mainAxisSize: MainAxisSize.min,
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 Container(
                   width: (VgToolUtils.getScreenWidth()/4)*3,
                   child: Text(
                     VgStringUtils.subStringAndAppendSymbol(
                         itemBean?.companynick, 25,
                         symbol: "...") ?? "",
                             maxLines: 1,
                             overflow: TextOverflow.ellipsis,
                             style: TextStyle(
                                 color:isSelected? ThemeRepository.getInstance().getPrimaryColor_1890FF() : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                                 fontSize: 15,
                                 ),
                           ),
                 ),
                 Container(
                   width: 260,
                   child: Text(
                     companyname ?? "暂无全称",
                     maxLines: 1,
                     overflow: TextOverflow.ellipsis,
                     style: TextStyle(
                       color:ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                       fontSize: 12,
                     ),
                     textAlign: TextAlign.left,
                   ),
                 ),
               ],
             ),
            Spacer(),
            Offstage(
              offstage:  !isSelected,
                child: Image.asset("images/current_ico.png",width: 12.3,)),
          ],
        ),

      ),
    );
  }

}

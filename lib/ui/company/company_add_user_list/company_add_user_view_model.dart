import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_list/bean/company_add_user_response_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class CompanyAddUserViewModel extends BasePagerViewModel<CompanyAddUserListItemBean,CompanyAddUserResponseBean>{

  ValueNotifier<int> infoValueNotifier;

  CompanyAddUserViewModel(BaseState<StatefulWidget> state) : super(state){
    infoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    infoValueNotifier?.dispose();
    super.onDisposed();
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "searchName" : "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appCompanyInputUserList";
  }

  @override
  CompanyAddUserResponseBean parseData(VgHttpResponse resp) {
    CompanyAddUserResponseBean vo = CompanyAddUserResponseBean.fromMap(resp?.data);
    loading(false);
    infoValueNotifier?.value = vo?.data?.page?.total ?? 0;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

}
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/company_add_user_edit_info_page.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_list/company_add_user_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/create_user_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/company_add_user_response_bean.dart';
import 'widgets/company_add_user_list_item_widget.dart';

/// 企业已添加用户
///
/// @author: zengxiangxi
/// @createTime: 1/23/21 10:06 AM
/// @specialDemand:
class CompanyAddUserPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CompanyAddUserPage";

  @override
  _CompanyAddUserPageState createState() => _CompanyAddUserPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CompanyAddUserPage(),
      routeName: CompanyAddUserPage.ROUTER,
    );
  }
}

class _CompanyAddUserPageState
    extends BasePagerState<CompanyAddUserListItemBean, CompanyAddUserPage> {
  CompanyAddUserViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CompanyAddUserViewModel(this);
    _viewModel.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        SizedBox(height: 12),
        CommonSearchBarWidget(
          hintText: "搜索",
        ),
        _toTopTitleWidget(),
        Expanded(
          child: _toPullRefreshWidget(),
        )
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "湛腾科技",
      isShowBack: true,
      rightPadding: 0,
      rightWidget: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          dynamic result =
              await CreateUserPage.navigatorPush(context,);
          if (result != null && result is bool && result) {
            _viewModel?.refreshMultiPage();
          }
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.add,
                size: 15,
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              ),
              Text(
                "添加",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    fontSize: 15,
                    height: 1.2),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _toTopTitleWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.infoValueNotifier,
      builder: (BuildContext context, int infoBean, Widget child) {
        return Container(
          height: 35,
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Text(
            "共添加用户${infoBean ?? 0}人，其中管理员0人，已注册激活0人",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
            ),
          ),
        );
      },
    );
  }

  Widget _toPullRefreshWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: VgPullRefreshWidget.bind(
        viewModel: _viewModel,
        state: this,
        child: ListView.separated(
            itemCount: data?.length ?? 0,
            padding: const EdgeInsets.all(0),
            physics: BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return CompanyAddUserListItemWidget(
                itemBean: data?.elementAt(index),
                onLongPress: (_) {},
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(height: 0);
            }),
      ),
    );
  }
}

import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"02935851547","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"管理员不管北京终端","fuid":"84f5e57c751b49eb992a91cd1dcfcfca","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王凡语","fuid":"dfc25c6825c04278bc0ebe9ea7674497","napicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","type":"01","status":"01"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"叶波","fuid":"3ce8f51461e24852ba9a3ee021f7ac82","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"郑文彬","fuid":"0b4fc308deef492daad377237622f4eb","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王丹","fuid":"f1db9f333be542e8a76f8f51d61d8ae6","napicurl":"http://etpic.we17.com/test/20210127110434_9981.jpg","type":"01","status":"01"},{"nick":"小岁","number":"1","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"李岁红","fuid":"d987ccb1a00f447ea04f65d9e33e5a15","napicurl":"http://etpic.we17.com/test/20210127105005_2814.jpg","type":"01","status":"01"}],"total":6,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class CompanyAddUserResponseBean extends BasePagerBean<CompanyAddUserListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CompanyAddUserResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyAddUserResponseBean companyAddUserResponseBeanBean = CompanyAddUserResponseBean();
    companyAddUserResponseBeanBean.success = map['success'];
    companyAddUserResponseBeanBean.code = map['code'];
    companyAddUserResponseBeanBean.msg = map['msg'];
    companyAddUserResponseBeanBean.data = DataBean.fromMap(map['data']);
    companyAddUserResponseBeanBean.extra = map['extra'];
    return companyAddUserResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<CompanyAddUserListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"02935851547","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"管理员不管北京终端","fuid":"84f5e57c751b49eb992a91cd1dcfcfca","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王凡语","fuid":"dfc25c6825c04278bc0ebe9ea7674497","napicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","type":"01","status":"01"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"叶波","fuid":"3ce8f51461e24852ba9a3ee021f7ac82","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"郑文彬","fuid":"0b4fc308deef492daad377237622f4eb","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王丹","fuid":"f1db9f333be542e8a76f8f51d61d8ae6","napicurl":"http://etpic.we17.com/test/20210127110434_9981.jpg","type":"01","status":"01"},{"nick":"小岁","number":"1","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"李岁红","fuid":"d987ccb1a00f447ea04f65d9e33e5a15","napicurl":"http://etpic.we17.com/test/20210127105005_2814.jpg","type":"01","status":"01"}],"total":6,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"02935851547","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"管理员不管北京终端","fuid":"84f5e57c751b49eb992a91cd1dcfcfca","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王凡语","fuid":"dfc25c6825c04278bc0ebe9ea7674497","napicurl":"http://etpic.we17.com/test/20210129101109_2814.jpg","type":"01","status":"01"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"叶波","fuid":"3ce8f51461e24852ba9a3ee021f7ac82","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"郑文彬","fuid":"0b4fc308deef492daad377237622f4eb","napicurl":"","type":"01","status":"00"},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"王丹","fuid":"f1db9f333be542e8a76f8f51d61d8ae6","napicurl":"http://etpic.we17.com/test/20210127110434_9981.jpg","type":"01","status":"01"},{"nick":"小岁","number":"1","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"90","groupid":"eadc02553f2a40509f05e407125f15e9","name":"李岁红","fuid":"d987ccb1a00f447ea04f65d9e33e5a15","napicurl":"http://etpic.we17.com/test/20210127105005_2814.jpg","type":"01","status":"01"}]
/// total : 6
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CompanyAddUserListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CompanyAddUserListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// nick : ""
/// number : ""
/// groupName : "员工"
/// uname : "王凡语"
/// phone : "02935851547"
/// roleid : "90"
/// groupid : "eadc02553f2a40509f05e407125f15e9"
/// name : "管理员不管北京终端"
/// fuid : "84f5e57c751b49eb992a91cd1dcfcfca"
/// napicurl : ""
/// type : "01"
/// status : "00"

class CompanyAddUserListItemBean {
  String nick;
  String number;
  String groupName;
  String uname;
  String phone;
  String roleid;
  String groupid;
  String name;
  String fuid;
  String napicurl;
  String type;
  String status;

  static CompanyAddUserListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyAddUserListItemBean recordsBean = CompanyAddUserListItemBean();
    recordsBean.nick = map['nick'];
    recordsBean.number = map['number'];
    recordsBean.groupName = map['groupName'];
    recordsBean.uname = map['uname'];
    recordsBean.phone = map['phone'];
    recordsBean.roleid = map['roleid'];
    recordsBean.groupid = map['groupid'];
    recordsBean.name = map['name'];
    recordsBean.fuid = map['fuid'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.type = map['type'];
    recordsBean.status = map['status'];
    return recordsBean;
  }

  Map toJson() => {
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "uname": uname,
    "phone": phone,
    "roleid": roleid,
    "groupid": groupid,
    "name": name,
    "fuid": fuid,
    "napicurl": napicurl,
    "type": type,
    "status": status,
  };

  ///是否以标记
  ///`status` 用户状态00未激活 01激活',
  ///type  00标记录入 （已标记）    01app手动添加
  bool isMark(){
    return type == "00";
  }

  ///`status` 用户状态00未激活 01激活',
  bool isAlive(){
    return status == "01";
  }
}
// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_filter_button_widget.dart';
// import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
// import 'package:flutter/material.dart';
//
// // enum CompanyDetailFilterType { admin, activate, unActivate }
// //
// // extension CompanyDetailFilterTypeExtension on CompanyDetailFilterType{
// //   getParamsStr(){
// //     if(this == null){
// //       return null;
// //     }
// //     ///"00"管理员，"01"激活，"02"未激活
// //     switch(this){
// //       case CompanyDetailFilterType.admin: return "00";
// //       case CompanyDetailFilterType.activate: return "01";
// //       case CompanyDetailFilterType.unActivate: return "02";
// //     }
// //     return null;
// //   }
// // }
//
// /// 企业详情分组列表筛选
// ///
// /// @author: zengxiangxi
// /// @createTime: 2/2/21 4:59 PM
// /// @specialDemand:
// class CompanyDetailByTypeListFilterWidget extends StatelessWidget {
//
//   final StatisticsBean statisticsBean;
//
//   final CompanyDetailFilterType type;
//
//   final ValueChanged<CompanyDetailFilterType> onChanged;
//
//   const CompanyDetailByTypeListFilterWidget({Key key, this.type, this.onChanged, this.statisticsBean})
//       : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//     return _toFilterWidget();
//   }
//
//   Widget _toFilterWidget() {
//     return DefaultTextStyle(
//       style: TextStyle(
//         color: VgColors.INPUT_BG_COLOR,
//         fontSize: 12,
//       ),
//       child: Container(
//         height: 30,
//         child: Row(
//           children: <Widget>[
//             GestureDetector(
//               behavior: HitTestBehavior.translucent,
//               onTap: (){
//                 _processType(CompanyDetailFilterType.admin);
//               },
//               child: Container(
//                   alignment: Alignment.center,
//                   padding: const EdgeInsets.symmetric(horizontal: 15),
//                   child: Text(
//                     "管理员${statisticsBean?.adminCnt ?? 0}人",
//                     style: TextStyle(
//                         color: type == CompanyDetailFilterType.admin
//                             ? ThemeRepository.getInstance()
//                                 .getPrimaryColor_1890FF()
//                             : null),
//                   )),
//             ),
//             Container(
//               width: 0.5,
//               height: 12,
//               color: VgColors.INPUT_BG_COLOR,
//             ),
//             GestureDetector(
//               behavior: HitTestBehavior.translucent,
//               onTap: (){
//                 _processType(CompanyDetailFilterType.activate);
//
//               },
//               child: Container(
//                   alignment: Alignment.center,
//                   padding: const EdgeInsets.symmetric(horizontal: 15),
//                   child: Center(
//                       child: Text(
//                     "已激活${statisticsBean?.activateCnt ?? 0}人（${_getActivePercent()}%）",
//                     style: TextStyle(
//                         color: type == CompanyDetailFilterType.activate
//                             ? ThemeRepository.getInstance()
//                                 .getPrimaryColor_1890FF()
//                             : null),
//                   ))),
//             ),
//             Container(
//               width: 0.5,
//               height: 12,
//               color: VgColors.INPUT_BG_COLOR,
//             ),
//             GestureDetector(
//               behavior: HitTestBehavior.translucent,
//               onTap: (){
//                 _processType(CompanyDetailFilterType.unActivate);
//               },
//               child: Container(
//                   alignment: Alignment.center,
//                   padding: const EdgeInsets.symmetric(horizontal: 15),
//                   child: Center(
//                       child: Text(
//                     "未激活${statisticsBean?.unactivateCnt ?? 0}人（${_getUnActivePercent()}%）",
//                     style: TextStyle(
//                         color: type == CompanyDetailFilterType.unActivate
//                             ? ThemeRepository.getInstance()
//                                 .getPrimaryColor_1890FF()
//                             : null),
//                   ))),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   _processType(CompanyDetailFilterType type){
//     if(type == null){
//       return;
//     }
//     if(type == this.type){
//       onChanged?.call(null);
//       return ;
//     }
//     onChanged?.call(type);
//   }
//
//   int _getActivePercent(){
//     if(statisticsBean == null){
//       return 0;
//     }
//     final int activeCnt = statisticsBean?.activateCnt ?? 0;
//     final int unActiveCnt = statisticsBean?.unactivateCnt ?? 0;
//     if(activeCnt + unActiveCnt == 0){
//       return 0;
//     }
//     return ((activeCnt / (activeCnt + unActiveCnt))*100).floor();
//   }
//
//   int _getUnActivePercent(){
//     if(statisticsBean == null){
//       return 0;
//     }
//     final int activeCnt = statisticsBean?.activateCnt ?? 0;
//     final int unActiveCnt = statisticsBean?.unactivateCnt ?? 0;
//     if(activeCnt + unActiveCnt == 0){
//       return 0;
//     }
//     return 100 -((activeCnt / (activeCnt + unActiveCnt))*100).floor();
//   }
// }

import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"nick":"","number":"","groupName":"员工","uname":"李岁红","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"2df5b2cf6ea440d3b8416f32d610a169","napicurl":"","type":"01","status":"00","lastPunchTime":""},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"王","fuid":"a396901fc248485e8e7863164c455004","napicurl":"http://etpic.we17.com/test/20210126184056_1205.jpg","type":"01","status":"01","lastPunchTime":1611657727},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红三员工","fuid":"837d05511d124f768c2c841ceb6b9e72","napicurl":"http://etpic.we17.com/test/20210126183013_4604.jpg","type":"01","status":"01","lastPunchTime":1611657764},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红二","fuid":"9b33f6d8c8274f0589f51dac28fcda59","napicurl":"http://etpic.we17.com/test/20210126181839_0576.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"Liudehua","number":"21000","groupName":"访客","uname":"王凡语","phone":"12319037902","roleid":"90","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"刘德华","fuid":"219e5a29efd8429fa9c5c0b81677be49","napicurl":"http://etpic.we17.com/test/20210126174546_5983.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"cz","number":"005","groupName":"员工","uname":"王凡语","phone":"15256510827","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"b9b488bc30c74d139f58cf10cc3b42fd","napicurl":"http://etpic.we17.com/test/20210126153327_1224.jpg","type":"01","status":"01","lastPunchTime":1611654931},{"nick":"小岁儿","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"388747b511024379931d5b038d5e762d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg","type":"00","status":"01","lastPunchTime":1611559145},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"c0fff6d35fe34ccf87f9d55c009be9a9","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611373973797-Unknown_ee380f35-deba-4ee6-a657-c2b3bbd648ef_.jpg","type":"00","status":"01","lastPunchTime":1611374060},{"nick":"lll","number":"111","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李兰兰","fuid":"d525a97d39da4ff38f39720e2a4b64ed","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611625186614-Unknown_1e07daf2-9e01-42eb-af89-3d6ebca9dfee_.jpg","type":"00","status":"01","lastPunchTime":1611625186},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"丹姐-访客分组","fuid":"fd35ca9dee0742a0a21c2a0039698c8e","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611380580855-Unknown_38b27afb-fe97-4693-98b6-6975e8add60d_.jpg","type":"00","status":"01","lastPunchTime":1611623913},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"敬秋菊","fuid":"600fca753b40424780e370a2a85a4b2d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123518939-1611123518752Unknown_2101201418382_.jpg","type":"00","status":"01","lastPunchTime":1611123728},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"秦志鹏","fuid":"cf14278da9e24158a5051b6e27014b28","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044213139-1611044212944Unknown_2101191616524_.jpg","type":"00","status":"01","lastPunchTime":1611559211},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"叶波","fuid":"07c5b763dc164b3da84a4e304f0ab3d5","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611218807968-1611218807003Unknown_2101211646473_.jpg","type":"00","status":"01","lastPunchTime":1612257921},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"郑文彬","fuid":"b8e9f26e6e3c482dbf011dfc0645aa79","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123673379-1611123673193Unknown_2101201421133_.jpg","type":"00","status":"01","lastPunchTime":1611136984},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"18665289540","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红2","fuid":"4ac93c176a18478697414f4ebb6f81c3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611131762782-1611131762504Unknown_2101201636024_.jpg","type":"00","status":"01","lastPunchTime":1612260686},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红4","fuid":"dfb4d324a5d54726b5c566bac57fd8a3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg","type":"00","status":"01","lastPunchTime":1611623962},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红5","fuid":"a7afec7f32f348d28e3f41e82e729f55","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023696130-1611023695952Unknown_2101191034551_.jpg","type":"00","status":"01","lastPunchTime":1611023692},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"1241241","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红6","fuid":"39c6992bfce54f729ed0120d34e73a60","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044008562-1611044008337Unknown_2101191613287_.jpg","type":"00","status":"01","lastPunchTime":1611044008},{"nick":"ha ","number":"123123123","groupName":"访客","uname":"王凡语","phone":"219038123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"郑文彬1","fuid":"638670befe0e4e9c84d96c0086be9305","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043967057-1611043966865Unknown_2101191612465_.jpg","type":"00","status":"01","lastPunchTime":1611134789},{"nick":"2130912","number":"211","groupName":"访客","uname":"王凡语","phone":"091283123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红1","fuid":"2a6c1a0b5e154593bdd5e98d3d4d9a25","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043396042-1611043395861Unknown_2101191603151_.jpg","type":"00","status":"01","lastPunchTime":1611381680}],"total":22,"size":20,"current":1,"orders":[],"searchCount":true,"pages":2},"statistics":{"activateCnt":3,"adminCnt":1,"unactivateCnt":19}}
/// extra : null

class CompanyDetailByTypeListResponseBean
    extends BasePagerBean<CompanyDetailByTypeListItemBean> {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CompanyDetailByTypeListResponseBean fromMap(Map<String, dynamic> map, {bool isSpecialList}) {
    if (map == null) return null;
    CompanyDetailByTypeListResponseBean
        companyDetailByTypeListResponseBeanBean =
        CompanyDetailByTypeListResponseBean();
    companyDetailByTypeListResponseBeanBean.success = map['success'];
    companyDetailByTypeListResponseBeanBean.code = map['code'];
    companyDetailByTypeListResponseBeanBean.msg = map['msg'];
    companyDetailByTypeListResponseBeanBean.data =
        DataBean.fromMap(map['data'], isSpecialList: isSpecialList);
    companyDetailByTypeListResponseBeanBean.extra = map['extra'];
    return companyDetailByTypeListResponseBeanBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0
      ? 1
      : data.page.current;

  ///得到List列表
  @override
  List<CompanyDetailByTypeListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() =>
      data?.page?.pages == null || data.page.pages <= 0 ? 1 : data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"nick":"","number":"","groupName":"员工","uname":"李岁红","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"2df5b2cf6ea440d3b8416f32d610a169","napicurl":"","type":"01","status":"00","lastPunchTime":""},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"王","fuid":"a396901fc248485e8e7863164c455004","napicurl":"http://etpic.we17.com/test/20210126184056_1205.jpg","type":"01","status":"01","lastPunchTime":1611657727},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红三员工","fuid":"837d05511d124f768c2c841ceb6b9e72","napicurl":"http://etpic.we17.com/test/20210126183013_4604.jpg","type":"01","status":"01","lastPunchTime":1611657764},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红二","fuid":"9b33f6d8c8274f0589f51dac28fcda59","napicurl":"http://etpic.we17.com/test/20210126181839_0576.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"Liudehua","number":"21000","groupName":"访客","uname":"王凡语","phone":"12319037902","roleid":"90","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"刘德华","fuid":"219e5a29efd8429fa9c5c0b81677be49","napicurl":"http://etpic.we17.com/test/20210126174546_5983.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"cz","number":"005","groupName":"员工","uname":"王凡语","phone":"15256510827","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"b9b488bc30c74d139f58cf10cc3b42fd","napicurl":"http://etpic.we17.com/test/20210126153327_1224.jpg","type":"01","status":"01","lastPunchTime":1611654931},{"nick":"小岁儿","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"388747b511024379931d5b038d5e762d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg","type":"00","status":"01","lastPunchTime":1611559145},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"c0fff6d35fe34ccf87f9d55c009be9a9","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611373973797-Unknown_ee380f35-deba-4ee6-a657-c2b3bbd648ef_.jpg","type":"00","status":"01","lastPunchTime":1611374060},{"nick":"lll","number":"111","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李兰兰","fuid":"d525a97d39da4ff38f39720e2a4b64ed","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611625186614-Unknown_1e07daf2-9e01-42eb-af89-3d6ebca9dfee_.jpg","type":"00","status":"01","lastPunchTime":1611625186},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"丹姐-访客分组","fuid":"fd35ca9dee0742a0a21c2a0039698c8e","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611380580855-Unknown_38b27afb-fe97-4693-98b6-6975e8add60d_.jpg","type":"00","status":"01","lastPunchTime":1611623913},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"敬秋菊","fuid":"600fca753b40424780e370a2a85a4b2d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123518939-1611123518752Unknown_2101201418382_.jpg","type":"00","status":"01","lastPunchTime":1611123728},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"秦志鹏","fuid":"cf14278da9e24158a5051b6e27014b28","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044213139-1611044212944Unknown_2101191616524_.jpg","type":"00","status":"01","lastPunchTime":1611559211},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"叶波","fuid":"07c5b763dc164b3da84a4e304f0ab3d5","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611218807968-1611218807003Unknown_2101211646473_.jpg","type":"00","status":"01","lastPunchTime":1612257921},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"郑文彬","fuid":"b8e9f26e6e3c482dbf011dfc0645aa79","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123673379-1611123673193Unknown_2101201421133_.jpg","type":"00","status":"01","lastPunchTime":1611136984},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"18665289540","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红2","fuid":"4ac93c176a18478697414f4ebb6f81c3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611131762782-1611131762504Unknown_2101201636024_.jpg","type":"00","status":"01","lastPunchTime":1612260686},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红4","fuid":"dfb4d324a5d54726b5c566bac57fd8a3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg","type":"00","status":"01","lastPunchTime":1611623962},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红5","fuid":"a7afec7f32f348d28e3f41e82e729f55","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023696130-1611023695952Unknown_2101191034551_.jpg","type":"00","status":"01","lastPunchTime":1611023692},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"1241241","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红6","fuid":"39c6992bfce54f729ed0120d34e73a60","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044008562-1611044008337Unknown_2101191613287_.jpg","type":"00","status":"01","lastPunchTime":1611044008},{"nick":"ha ","number":"123123123","groupName":"访客","uname":"王凡语","phone":"219038123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"郑文彬1","fuid":"638670befe0e4e9c84d96c0086be9305","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043967057-1611043966865Unknown_2101191612465_.jpg","type":"00","status":"01","lastPunchTime":1611134789},{"nick":"2130912","number":"211","groupName":"访客","uname":"王凡语","phone":"091283123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红1","fuid":"2a6c1a0b5e154593bdd5e98d3d4d9a25","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043396042-1611043395861Unknown_2101191603151_.jpg","type":"00","status":"01","lastPunchTime":1611381680}],"total":22,"size":20,"current":1,"orders":[],"searchCount":true,"pages":2}
/// statistics : {"activateCnt":3,"adminCnt":1,"unactivateCnt":19}

class DataBean {
  PageBean page;
  StatisticsBean statistics;

  static DataBean fromMap(Map<String, dynamic> map, {bool isSpecialList}) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page'], isSpecialList: isSpecialList);
    dataBean.statistics = StatisticsBean.fromMap(map['statistics']);
    return dataBean;
  }

  Map toJson() => {
        "page": page,
        "statistics": statistics,
      };
}

/// activateCnt : 3
/// adminCnt : 1
/// unactivateCnt : 19

class StatisticsBean {
  int activateCnt;
  int adminCnt;
  int unactivateCnt;

  static StatisticsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StatisticsBean statisticsBean = StatisticsBean();
    statisticsBean.activateCnt = map['activateCnt'];
    statisticsBean.adminCnt = map['adminCnt'];
    statisticsBean.unactivateCnt = map['unactivateCnt'];
    return statisticsBean;
  }

  Map toJson() => {
        "activateCnt": activateCnt,
        "adminCnt": adminCnt,
        "unactivateCnt": unactivateCnt,
      };
}

/// records : [{"nick":"","number":"","groupName":"员工","uname":"李岁红","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"2df5b2cf6ea440d3b8416f32d610a169","napicurl":"","type":"01","status":"00","lastPunchTime":""},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"王","fuid":"a396901fc248485e8e7863164c455004","napicurl":"http://etpic.we17.com/test/20210126184056_1205.jpg","type":"01","status":"01","lastPunchTime":1611657727},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红三员工","fuid":"837d05511d124f768c2c841ceb6b9e72","napicurl":"http://etpic.we17.com/test/20210126183013_4604.jpg","type":"01","status":"01","lastPunchTime":1611657764},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红二","fuid":"9b33f6d8c8274f0589f51dac28fcda59","napicurl":"http://etpic.we17.com/test/20210126181839_0576.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"Liudehua","number":"21000","groupName":"访客","uname":"王凡语","phone":"12319037902","roleid":"90","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"刘德华","fuid":"219e5a29efd8429fa9c5c0b81677be49","napicurl":"http://etpic.we17.com/test/20210126174546_5983.jpg","type":"01","status":"00","lastPunchTime":null},{"nick":"cz","number":"005","groupName":"员工","uname":"王凡语","phone":"15256510827","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"陈喆","fuid":"b9b488bc30c74d139f58cf10cc3b42fd","napicurl":"http://etpic.we17.com/test/20210126153327_1224.jpg","type":"01","status":"01","lastPunchTime":1611654931},{"nick":"小岁儿","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"388747b511024379931d5b038d5e762d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611559145658-Unknown_a5a9b605-b957-4540-804f-0498eadcb4ed_.jpg","type":"00","status":"01","lastPunchTime":1611559145},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红1","fuid":"c0fff6d35fe34ccf87f9d55c009be9a9","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611373973797-Unknown_ee380f35-deba-4ee6-a657-c2b3bbd648ef_.jpg","type":"00","status":"01","lastPunchTime":1611374060},{"nick":"lll","number":"111","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李兰兰","fuid":"d525a97d39da4ff38f39720e2a4b64ed","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611625186614-Unknown_1e07daf2-9e01-42eb-af89-3d6ebca9dfee_.jpg","type":"00","status":"01","lastPunchTime":1611625186},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"丹姐-访客分组","fuid":"fd35ca9dee0742a0a21c2a0039698c8e","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611380580855-Unknown_38b27afb-fe97-4693-98b6-6975e8add60d_.jpg","type":"00","status":"01","lastPunchTime":1611623913},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"敬秋菊","fuid":"600fca753b40424780e370a2a85a4b2d","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123518939-1611123518752Unknown_2101201418382_.jpg","type":"00","status":"01","lastPunchTime":1611123728},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"秦志鹏","fuid":"cf14278da9e24158a5051b6e27014b28","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044213139-1611044212944Unknown_2101191616524_.jpg","type":"00","status":"01","lastPunchTime":1611559211},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"叶波","fuid":"07c5b763dc164b3da84a4e304f0ab3d5","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611218807968-1611218807003Unknown_2101211646473_.jpg","type":"00","status":"01","lastPunchTime":1612257921},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"郑文彬","fuid":"b8e9f26e6e3c482dbf011dfc0645aa79","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611123673379-1611123673193Unknown_2101201421133_.jpg","type":"00","status":"01","lastPunchTime":1611136984},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"18665289540","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红2","fuid":"4ac93c176a18478697414f4ebb6f81c3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611131762782-1611131762504Unknown_2101201636024_.jpg","type":"00","status":"01","lastPunchTime":1612260686},{"nick":"","number":"","groupName":"员工","uname":"王凡语","phone":"13226332406","roleid":"10","groupid":"11e2bec33bf743aaae4522b51c022f32","name":"李岁红4","fuid":"dfb4d324a5d54726b5c566bac57fd8a3","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg","type":"00","status":"01","lastPunchTime":1611623962},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红5","fuid":"a7afec7f32f348d28e3f41e82e729f55","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023696130-1611023695952Unknown_2101191034551_.jpg","type":"00","status":"01","lastPunchTime":1611023692},{"nick":"","number":"","groupName":"访客","uname":"王凡语","phone":"1241241","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红6","fuid":"39c6992bfce54f729ed0120d34e73a60","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611044008562-1611044008337Unknown_2101191613287_.jpg","type":"00","status":"01","lastPunchTime":1611044008},{"nick":"ha ","number":"123123123","groupName":"访客","uname":"王凡语","phone":"219038123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"郑文彬1","fuid":"638670befe0e4e9c84d96c0086be9305","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043967057-1611043966865Unknown_2101191612465_.jpg","type":"00","status":"01","lastPunchTime":1611134789},{"nick":"2130912","number":"211","groupName":"访客","uname":"王凡语","phone":"091283123123","roleid":"10","groupid":"377750b3c2f14647b1be20bd3cad3ff9","name":"李岁红1","fuid":"2a6c1a0b5e154593bdd5e98d3d4d9a25","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043396042-1611043395861Unknown_2101191603151_.jpg","type":"00","status":"01","lastPunchTime":1611381680}]
/// total : 22
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 2

class PageBean {
  List<CompanyDetailByTypeListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map, {bool isSpecialList}) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()
      ..addAll((map['records'] as List ?? [])
          .map((o) => CompanyDetailByTypeListItemBean.fromMap(o, isSpecialList: isSpecialList)));
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
        "records": records,
        "total": total,
        "size": size,
        "current": current,
        "orders": orders,
        "searchCount": searchCount,
        "pages": pages,
      };
}

/// nick : ""
/// number : ""
/// groupName : "员工"
/// uname : "李岁红"
/// phone : ""
/// roleid : "10"
/// groupid : "11e2bec33bf743aaae4522b51c022f32"
/// name : "陈喆"
/// fuid : "2df5b2cf6ea440d3b8416f32d610a169"
/// napicurl : ""
/// type : "01"
/// status : "00"
/// lastPunchTime : ""

class CompanyDetailByTypeListItemBean {
  String nick;
  String number;
  String groupName;
  String uname;
  String phone;
  String roleid;
  String groupid;
  String name;
  String fuid;
  String napicurl;
  String type;
  String status;

  String fid;
  String userid;

  String putpicurl;

  String isSign;

  String pictureUrl;

  int lastPunchTime;

  String department;
  String projectname;
  String classname;
  String coursename;

  String city;

  bool isSelect;
  bool special;
  bool isSpecialList;

  static CompanyDetailByTypeListItemBean fromMap(Map<String, dynamic> map, {bool isSpecialList}) {
    if (map == null) return null;
    CompanyDetailByTypeListItemBean recordsBean =
        CompanyDetailByTypeListItemBean();
    recordsBean.nick = map['nick'];
    recordsBean.number = map['number'];
    recordsBean.groupName = map['groupName'];
    recordsBean.uname = map['uname'];
    recordsBean.phone = map['phone'];
    recordsBean.roleid = showOriginEmptyStr(map['roleid']) ??
        CompanyAddUserEditInfoIdentityType.commonUser.getTypeToIdStr();
    recordsBean.groupid = map['groupid'];
    recordsBean.name = map['name'];
    recordsBean.fuid = map['fuid'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.type = map['type'];
    recordsBean.status = map['status'];
    recordsBean.lastPunchTime = map['lastPunchTime'];
    recordsBean.fid = map['fid'];
    recordsBean.userid = map['userid'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.isSign = map['isSign'];
    recordsBean.pictureUrl = map['pictureUrl'];
    recordsBean.department = map['department'];
    recordsBean.projectname = map['projectname'];
    recordsBean.classname = map['classname'];
    recordsBean.coursename = map['coursename'];
    recordsBean.city = map['city'];
    recordsBean.isSelect = map['isSelect'];
    recordsBean.special = map['special'];
    recordsBean.isSpecialList = isSpecialList??false;
    return recordsBean;
  }

  Map toJson() => {
        "nick": nick,
        "number": number,
        "groupName": groupName,
        "uname": uname,
        "phone": phone,
        "roleid": roleid,
        "groupid": groupid,
        "name": name,
        "fuid": fuid,
        "napicurl": napicurl,
        "type": type,
        "status": status,
        "lastPunchTime": lastPunchTime,
        "fid": fid,
        "userid": userid,
        "pictureUrl": pictureUrl,
        "department": department,
        "projectname": projectname,
        "classname": classname,
        "coursename": coursename,
        "city": city,
        "isSelect": isSelect,
        "special": special,
        "isSpecialList": isSpecialList,
      };

  ///是否以标记
  ///`status` 用户状态00未激活 01激活',
  ///type  00标记录入 （已标记）    01app手动添加
  bool isMark() {
    return type == "00";
  }

  ///`status` 用户状态00未激活 01激活',
  bool isAlive() {
    return status == "01";
  }

  ///isSign == "00"；未标记
  bool isUnknow() {
    return isSign == "00";
  }
}

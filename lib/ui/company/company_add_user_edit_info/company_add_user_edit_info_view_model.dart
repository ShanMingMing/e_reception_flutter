import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_upload_bean.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_list/company_add_user_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';

class CompanyAddUserEditInfoViewModel extends BaseViewModel{

  ///App添加用户
  static const String COMPANY_ADD_USER_UPLOAD_API =ServerApi.BASE_URL + "app/appAddUser";

  CompanyAddUserEditInfoViewModel(BaseState<StatefulWidget> state) : super(state);

  ///保存或绑定
  void saveAddUserHttp(
      BuildContext context, CompanyAddUserEditInfoUploadBean uploadBean) async {
    if (uploadBean == null) {
      return;
    }
    VgHudUtils.show(context, "保存中");
    if(StringUtils.isEmpty(uploadBean?.picUrl)){
      _toSaveHttp(context, uploadBean);
      return;
    }
    VgMatisseUploadUtils.uploadSingleImage(
        uploadBean?.picUrl,
        VgBaseCallback(onSuccess: (String netPic) {
          if (StringUtils.isNotEmpty(netPic)) {
            uploadBean.picUrl = netPic;
            _toSaveHttp(context, uploadBean);
          } else {
            VgHudUtils.hide(context);
            VgToastUtils.toast(context, "图片上传失败");
          }
        }, onError: (String msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }),
      isFace: true,
    );
  }

  void _toSaveHttp(
      BuildContext context, CompanyAddUserEditInfoUploadBean uploadBean) {
    print("保存");
    print("----------------\n");
    print(uploadBean?.toString());
    print("-----------------\n");
    // return;

    VgHttpUtils.get(COMPANY_ADD_USER_UPLOAD_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "groupid": uploadBean?.groupId ?? "",
          "hsn":uploadBean?.getHsnSplitStr() ?? "",
          "name": uploadBean?.name ?? "",
          "roleid" : uploadBean?.identityType?.getTypeToIdStr()?? "",
          "napicurl":uploadBean?.picUrl ?? "",
          "nick" : uploadBean?.nick ?? "",
          "number": uploadBean?.id ?? "",
          "phone": globalSubPhone(uploadBean?.phone) ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          // RouterUtils.pushAndReplace(context, CompanyAddUserPage());
          RouterUtils.pop(context,result: true);
          VgToastUtils.toast(AppMain.context, "保存成功");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }
}
// import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_upload_bean.dart';
// import 'package:e_reception_flutter/ui/company/company_add_user_list/company_add_user_page.dart';
// import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
// import 'package:e_reception_flutter/ui/create_user/create_user/create_user_page.dart';
// import 'package:e_reception_flutter/utils/router_utils.dart';
// import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
// import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:vg_base/vg_arch_lib.dart';
// import 'package:vg_base/vg_string_util_lib.dart';
// import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
//
// import 'company_add_user_edit_info_view_model.dart';
// import 'widgets/company_add_user_edit_info_widget.dart';
//
// /// 企业添加用户编辑信息
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/23/21 10:37 AM
// /// @specialDemand:
// class CompanyAddUserEditInfoPage extends StatefulWidget {
//   ///路由名称
//   static const String ROUTER = "CompanyAddUserEditInfoPage";
//
//   @override
//   _CompanyAddUserEditInfoPageState createState() =>
//       _CompanyAddUserEditInfoPageState();
//
//   ///跳转方法
//   static Future<bool> navigatorPush(BuildContext context) {
//     return CreateUserPage.navigatorPush(context);
//   }
// }
//
// class _CompanyAddUserEditInfoPageState
//     extends BaseState<CompanyAddUserEditInfoPage> {
//
//   ValueNotifier<CompanyAddUserEditInfoUploadBean> _editsValueNotifier;
//
//   CompanyAddUserEditInfoViewModel _viewModel;
//
//
//   @override
//   void initState() {
//     super.initState();
//     _viewModel = CompanyAddUserEditInfoViewModel(this);
//     _editsValueNotifier = ValueNotifier(null);
//   }
//
//   @override
//   void dispose() {
//     _editsValueNotifier?.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
//       body: _toMainColumnWidget(),
//     );
//   }
//
//   Widget _toMainColumnWidget() {
//     return Column(
//       children: <Widget>[
//         _toTopBarWidget(),
//         Expanded(
//           child: _toScrollViewWidget(),
//         ),
//       ],
//     );
//   }
//
//   Widget _toScrollViewWidget(){
//     return ScrollConfiguration(
//       behavior: MyBehavior(),
//       child: ListView(
//         padding: const EdgeInsets.all(0),
//         keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
//         children: <Widget>[
//           Container(
//             height: 10,
//             color: ThemeRepository.getInstance().getCardBgColor_21263C(),
//           ),
//           CompanyAddUserEditInfoWidget(
//             onChanged: _editsValueNotifier
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget _toTopBarWidget() {
//     return VgTopBarWidget(
//       title: "添加用户",
//       isShowBack: true,
//       backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
//       rightWidget: _toSaveButtonWidget(),
//     );
//   }
//
//   Widget _toSaveButtonWidget() {
//     return ValueListenableBuilder(
//       valueListenable: _editsValueNotifier,
//       builder: (BuildContext context, CompanyAddUserEditInfoUploadBean uploadBean,
//           Widget child) {
//         return CommonFixedHeightConfirmButtonWidget(
//           isAlive: uploadBean?.isAlive() ?? false,
//           width: 48,
//           height: 24,
//           disableMillTime: 1000,
//           unSelectBgColor:
//           ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
//           selectedBgColor:
//           ThemeRepository.getInstance().getPrimaryColor_1890FF(),
//           unSelectTextStyle: TextStyle(
//               color: VgColors.INPUT_BG_COLOR,
//               fontSize: 12,
//               fontWeight: FontWeight.w600),
//           selectedTextStyle: TextStyle(
//               color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
//           text: "保存",
//           onTap: (){
//             String msg = uploadBean?.checkVerify();
//             if(StringUtils.isEmpty(msg)){
//               _viewModel?.saveAddUserHttp(context, uploadBean);
//               return;
//             }
//             VgToastUtils.toast(context, msg);
//           },
//         );
//       },
//     );
//   }
// }

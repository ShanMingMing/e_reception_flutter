import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';

enum CompanyAddUserEditInfoIdentityType {
  ///超级管理员
  superAdmin,

  ///普通管理员
  commonAdmin,

  ///普通用户
  commonUser,

}

extension CompanyAddUserEditInfoIdentityExtension
    on CompanyAddUserEditInfoIdentityType {

  ///获取默认类型
  static CompanyAddUserEditInfoIdentityType getDefaultType(){
    return CompanyAddUserEditInfoIdentityType.commonUser;
  }

  ///或者选择项
  static List<String> getListStr() {
    List<String> list = List();
    for(CompanyAddUserEditInfoIdentityType item in CompanyAddUserEditInfoIdentityType.values){
      if(!VgRoleUtils.isSuperAdmin(UserRepository.getInstance().userData?.comUser?.roleid) && item == CompanyAddUserEditInfoIdentityType.superAdmin){
        continue;
      }
      if(item == CompanyAddUserEditInfoIdentityType.commonUser){
        continue;
      }
      list?.add(item?.getTypeToStr()); 
    }
   return list;
  }

  ///获取类型对应字符串
  String getTypeToStr() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case CompanyAddUserEditInfoIdentityType.superAdmin:
        return "超级管理员";
      case CompanyAddUserEditInfoIdentityType.commonAdmin:
        return "普通管理员";
      case CompanyAddUserEditInfoIdentityType.commonUser:
        // return "普通用户";
        return '';
    }
    return null;
  }

  ///字符串转类型
  static CompanyAddUserEditInfoIdentityType getStrToType(String str) {
    if (str == null || str.isEmpty) {
      return null;
    }
    switch (str) {
      case "超级管理员":
        return CompanyAddUserEditInfoIdentityType.superAdmin;
      case "普通管理员":
        return CompanyAddUserEditInfoIdentityType.commonAdmin;
      case "普通用户":
        return CompanyAddUserEditInfoIdentityType.commonUser;
    }
    return null;
  }

  ///获取类型对应Id字符串
  String getTypeToIdStr() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case CompanyAddUserEditInfoIdentityType.superAdmin:
        return "99";
      case CompanyAddUserEditInfoIdentityType.commonAdmin:
        return "90";
      case CompanyAddUserEditInfoIdentityType.commonUser:
        return "10";
    }
    return null;
  }


  ///获取类型对应Id字符串
  static CompanyAddUserEditInfoIdentityType getIdToTypeStr(String roleid) {
    if (roleid == null || roleid == "") {
      return null;
    }
    switch (roleid) {
      case "99":
        return CompanyAddUserEditInfoIdentityType.superAdmin;
      case "90":
        return CompanyAddUserEditInfoIdentityType.commonAdmin;
      case "10":
        return CompanyAddUserEditInfoIdentityType.commonUser;
    }
    return null;
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/widgets.dart';

/// 企业主页-管理员列表
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 6:03 PM
/// @specialDemand:
class CompanyAddAdminListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _toListWidget();
  }

  Widget _toListWidget() {
    return ListView.separated(
        itemCount:12,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget();
        },
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(height: 0,);
        }
    );
  }

  Widget _toListItemWidget() {
    return Container(
      height: 64,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: _toMainRowWidget(),
    );
  }

  Widget _toMainRowWidget() {
    return Row(
      children: <Widget>[
        ClipOval(
          child: Container(
            width: 40,
            height: 40,
            child: VgCacheNetWorkImage(
              "https://pic.008box.com/headimg/head34.png",
              fit: BoxFit.cover,
              imageQualityType: ImageQualityType.middleDown,
            ),
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          child:  _toColumnWidget(),
        )
      ],
    );
  }

  Widget _toColumnWidget() {
    return Container(
      height: 40,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "路菜菜",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 15,
                ),
              ),
              SizedBox(width: 8,),
              Container(
                height: 16,
                padding: const EdgeInsets.symmetric(horizontal: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: ThemeRepository.getInstance().getMinorYellowColor_FFB714().withOpacity(0.1)
                ),
                child:  Center(
                  child: Text(
                    "超级管理员",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
                        fontSize: 10,height: 1.2
                    ),
                  ),
                ),
              ),

              Spacer(),
              Text(
                "18712345678",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 11,
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Text(
                "15台终端",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 12,
                ),
              ),
              Spacer(),
              Text(
                "登录APP·3天前",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 11,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_add_admin_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_add_admin_edit/binding_add_admin_edit_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';

import 'widgets/company_add_admin_list_widget.dart';

/// 企业管理员列表
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 4:12 PM
/// @specialDemand:
class CompanyAddAdminListPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CompanyAddAdminListPage";

  @override
  _CompanyAddAdminListPageState createState() =>
      _CompanyAddAdminListPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CompanyAddAdminListPage(),
      routeName: CompanyAddAdminListPage.ROUTER,
    );
  }
}

class _CompanyAddAdminListPageState extends State<CompanyAddAdminListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: CompanyAddAdminListWidget(),
        )
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "管理员",
      isShowBack: true,
      backgroundColor: Color(0xFF191E31),
      rightWidget: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            BindingAddAdminEditPage.navigatorPush(context);
          },
          child: _toAddButtonWidget()),
    );
  }

  Widget _toAddButtonWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Icon(
          Icons.add,
          size: 15,
          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        ),
        Text(
          "添加",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              fontSize: 14,
              height: 1.2),
        )
      ],
    );
  }
}

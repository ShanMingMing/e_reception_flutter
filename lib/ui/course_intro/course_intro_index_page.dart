import 'dart:async';
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/ai_poster_index_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_mixin/navigator_page_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// 课程介绍首页
class CourseIntroIndexPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CourseIntroIndexPage";

  @override
  CourseIntroIndexPageState createState() => CourseIntroIndexPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CourseIntroIndexPage(),
      routeName: CourseIntroIndexPage.ROUTER,
    );
  }
}

class CourseIntroIndexPageState
    extends BasePagerState<AiPosterIndexListItemBean, CourseIntroIndexPage>
    with
        AutomaticKeepAliveClientMixin,
        VgPlaceHolderStatusMixin,
        NavigatorPageMixin {

  ///获取state
  static CourseIntroIndexPageState of(BuildContext context) {
    final CourseIntroIndexPageState result =
    context.findAncestorStateOfType<CourseIntroIndexPageState>();
    return result;
  }


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              _toTopBarWidget(),
              Center(child: _defaultEmptyCustomWidget()),
            ],
          ),
        )
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "商品展示",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  Widget _defaultEmptyCustomWidget() {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            "images/icon_empty_white.png",
            width: 120,
            gaplessPlayback: true,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            "暂无内容",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

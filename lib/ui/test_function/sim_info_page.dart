
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/test_function/sim_settings_info_page.dart';
import 'package:e_reception_flutter/ui/test_function/sim_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SimInfoPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "SimInfoPage";

  final String phone;
  final String operator;
  final String agent;
  final String setMeal;
  final String flowSharing;
  final String hsn;

  const SimInfoPage({Key key, this.phone, this.operator, this.agent, this.setMeal, this.flowSharing, this.hsn}):super(key: key);

  @override
  _SimInfoPageState createState() => _SimInfoPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn,
      {String phone, String operator, String agent, String setMeal, String flowSharing}) {
    return RouterUtils.routeForFutureResult(
      context,
      SimInfoPage(
        hsn: hsn,
        phone: phone,
        operator: operator,
        agent: agent,
        setMeal: setMeal,
        flowSharing: flowSharing,
      ),
      routeName: SimInfoPage.ROUTER,
    );
  }

}

class _SimInfoPageState extends BaseState<SimInfoPage>{

  TextEditingController _phoneController;
  bool isAlive = false;
  
  String _operator;
  String _agent;
  String _setMeal;
  
  SimViewModel _viewModel;
  String _originParams = "";
  //是否组池 00是 01不是
  String _flowSharing = "00";
  
  @override
  void initState() {
    super.initState();
    _viewModel = new SimViewModel(this);
    _phoneController = TextEditingController();
    if(StringUtils.isNotEmpty(widget?.phone)){
      _phoneController?.text = widget?.phone;
      _originParams += widget?.phone;
    }
    _operator = widget?.operator??"";
    _agent = widget?.agent??"";
    _setMeal = widget?.setMeal??"";
    _originParams += widget?.operator??"";
    _originParams += widget?.agent??"";
    _originParams += widget?.setMeal??"";
    judgeAlive();
  }

  void judgeAlive(){
    bool isEmpty = StringUtils.isNotEmpty(_phoneController.text)
        && StringUtils.isNotEmpty(_operator)
        && StringUtils.isNotEmpty(_agent)
        && StringUtils.isNotEmpty(_setMeal);
    if(!isEmpty){
      isAlive = false;
      return;
    }
    String currentParams = "";
    currentParams += _phoneController.text;
    currentParams += _operator;
    currentParams += _agent;
    currentParams += _setMeal;
    isAlive = !(currentParams == _originParams);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: [
          _toTopBarWidget(),
          _toPhoneWidget(),
          _toSplitWidget(),
          _toOperatorWidget(),
          _toSplitWidget(),
          _toAgentWidget(),
          _toSplitWidget(),
          _toSetMealWidget(),
          _toGroupPoolWidget(),
          SizedBox(height: 30,),
          _toSaveWidget(),
        ],
      ),
    );
  }


  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "SIM卡信息",
      isShowBack: true,
    );
  }

  ///电话号码
  Widget _toPhoneWidget(){
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Container(
            width: 86,
            padding: EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            child: Text(
              "电话号码",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: VgTextField(
              controller: _phoneController,
              maxLines: 1,
              cursorWidth: 2,
              autofocus: true,
              keyboardType: TextInputType.phone,
              inputFormatters: [
                WhitelistingTextInputFormatter(RegExp("[0-9+]")),
              ],
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                hintText: "请输入",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getHintGreenColor_5E687C(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  color: ThemeRepository.getInstance()
                      .getTextMainColor_D0E0F7()),
              onChanged: (String str) {
                setState(() {
                  judgeAlive();
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _toSplitWidget(){
    return Container(
      height: 1,
      width: ScreenUtils.screenW(context),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        color:  Color(0xFF303546),
      ),
    );
  }

  ///运营商
  Widget _toOperatorWidget(){
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: 86,
            padding: EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            child: Text(
              "运营商",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: ()async{
                String result = await SimSettingsInfoPage.navigatorPush(context, "00", _operator);
                if(StringUtils.isNotEmpty(result)){
                  setState(() {
                    _operator = result;
                    judgeAlive();
                  });
                }
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    StringUtils.isNotEmpty(_operator)?_operator:"请设置",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      color: StringUtils.isNotEmpty(_operator)?ThemeRepository.getInstance().getTextColor_D0E0F7():ThemeRepository.getInstance().getHintGreenColor_5E687C()
                    ),
                  ),
                  Spacer(),
                  SizedBox(width: 5,),
                  Image.asset(
                    "images/icon_arrow_right_grey.png",
                    width: 7,
                  ),
                  SizedBox(width: 15,),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///代理商
  Widget _toAgentWidget(){
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Container(
            width: 86,
            padding: EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            child: Text(
              "代理商",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: ()async{
                String result = await SimSettingsInfoPage.navigatorPush(context, "01", _agent);
                if(StringUtils.isNotEmpty(result)){
                  setState(() {
                    _agent = result;
                    judgeAlive();
                  });
                }
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    StringUtils.isNotEmpty(_agent)?_agent:"请设置",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 14,
                        color: StringUtils.isNotEmpty(_agent)?ThemeRepository.getInstance().getTextColor_D0E0F7():ThemeRepository.getInstance().getHintGreenColor_5E687C()
                    ),
                  ),
                  Spacer(),
                  SizedBox(width: 5,),
                  Image.asset(
                    "images/icon_arrow_right_grey.png",
                    width: 7,
                  ),
                  SizedBox(width: 15,),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///套餐规格
  Widget _toSetMealWidget(){
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Container(
            width: 86,
            padding: EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            child: Text(
              "套餐规格",
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: ()async{
                String result = await SimSettingsInfoPage.navigatorPush(context, "02", _setMeal);
                if(StringUtils.isNotEmpty(result)){
                  setState(() {
                    _setMeal = result;
                    judgeAlive();
                  });
                }
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    StringUtils.isNotEmpty(_setMeal)?_setMeal:"请设置",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 14,
                        color: StringUtils.isNotEmpty(_setMeal)?ThemeRepository.getInstance().getTextColor_D0E0F7():ThemeRepository.getInstance().getHintGreenColor_5E687C()
                    ),
                  ),
                  Spacer(),
                  SizedBox(width: 5,),
                  Image.asset(
                    "images/icon_arrow_right_grey.png",
                    width: 7,
                  ),
                  SizedBox(width: 15,),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///是否组池
  Widget _toGroupPoolWidget() {
    return Container(
      height: 50,
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "是否组池",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                _flowSharing  = "00";
              });
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    "00"==_flowSharing
                        ?"images/icon_white_selected.png"
                        :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "是",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: "00"==_flowSharing
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 40,
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                _flowSharing = "01";
              });
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    "01"==_flowSharing
                        ?"images/icon_white_selected.png"
                        :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "否",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: "01"==_flowSharing
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///保存
  Widget _toSaveWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(!isAlive){
          return;
        }
        if(StringUtils.isNotEmpty(widget?.phone)){
          _viewModel.editSim(context, _phoneController?.text, _operator, _agent, _setMeal, _flowSharing, widget?.hsn);
        }else{
          _viewModel.saveSim(context, _phoneController?.text, _operator, _agent, _setMeal, _flowSharing, widget?.hsn);
        }
      },
      child: Container(
        height: 40,
        width: ScreenUtils.screenW(context),
        margin: EdgeInsets.symmetric(horizontal: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: isAlive?ThemeRepository.getInstance().getPrimaryColor_1890FF():ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        ),
        child: Text(
          "保存",
          style: TextStyle(
            fontSize: 15,
            color:isAlive?Colors.white:VgColors.INPUT_BG_COLOR
          ),
        ),
      ),
    );
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/test_function/hsn_id_scan_page.dart';
import 'package:e_reception_flutter/ui/test_function/test_history_page.dart';
import 'package:e_reception_flutter/ui/test_function/test_scan_id_page.dart';
import 'package:e_reception_flutter/ui/test_function/widgets/input_id_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class TestScanHsnPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "TestScanHsnPage";

  const TestScanHsnPage({Key key}):super(key: key);

  @override
  _TestScanHsnPageState createState() => _TestScanHsnPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      TestScanHsnPage(),
      routeName: TestScanHsnPage.ROUTER,
    );
  }

}

class _TestScanHsnPageState extends BaseState<TestScanHsnPage>{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: [
          _toTopBarWidget(),
          _toInfoWidget(),
        ],
      ),
    );
  }

  Widget _toInfoWidget(){
    return Expanded(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          SizedBox(height: 30,),
          _toScanProcessWidget(),
          SizedBox(height: 9,),
          _toScanProcessTextWidget(),
          SizedBox(height: 30,),
          _toScanHsnWidget(),
        ],
      ),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "标记机身编号",
      isShowBack: true,
      rightWidget: GestureDetector(
        onTap: (){
          TestHistoryPage.navigatorPush(context);
        },
        child: Container(
          padding: EdgeInsets.only(left: 15),
          child: Text(
            "历史记录",
            style: TextStyle(
                fontSize: 14,
                height: 1.2,
                color: ThemeRepository.getInstance().getTextColor_D0E0F7()
            ),
          ),
        ),
      ),
    );
  }

  Widget _toScanProcessWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: Color(0xFF373C4F),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: Color(0xFF373C4F),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: Color(0xFF373C4F),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: Color(0xFF373C4F),
              borderRadius: BorderRadius.circular(7)),
        ),
      ],
    );
  }

  Widget _toScanProcessTextWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(width: 40,),
        Text(
          "扫描屏幕二维码",
          style: TextStyle(
              fontSize: 11,
              height: 1.2,
              color: ThemeRepository.getInstance().getTextColor_D0E0F7()
          ),
        ),
        Spacer(),
        Text(
          "标记机身编号",
          style: TextStyle(
            fontSize: 11,
            height: 1.2,
            color: Color(0xFF373C4F),
          ),
        ),
        Spacer(),
        Text(
          "信息确认",
          style: TextStyle(
            fontSize: 11,
            height: 1.2,
            color: Color(0xFF373C4F),
          ),
        ),
        SizedBox(width: 40,),
      ],
    );
  }

  Widget _toScanHsnWidget(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          borderRadius: BorderRadius.circular(8)
      ),
      child: Column(
        children: [
          SizedBox(height: 46,),
          Image.asset(
            'images/icon_scan_hsn_hint.png',
            width: 110,
            height: 110,
          ),
          SizedBox(height: 30,),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: ()async{
              String result = await HsnIdScanPage.navigatorPush(context, HsnIdScanPage.TYPE_HSN, updateFlag: false);
              if(StringUtils.isNotEmpty(result)){
                TestScanIdPage.navigatorPush(context, result, true);
              }
            },
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  borderRadius: BorderRadius.circular(8)
              ),
              height: 40,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'images/icon_scan_hint.png',
                    width: 20,
                    height: 20,
                  ),
                  SizedBox(width: 6,),
                  Text(
                    "扫描屏幕上二维码",
                    style: TextStyle(
                        fontSize:15,
                        color: Colors.white,
                        height: 1.2
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(horizontal: 27),
            child: Text(
              "1. 显示屏安装“AI显示端”软件\n2. 显示屏将“AI显示端”软件设置为主程序\n3. 扫描显示屏“屏幕”上的二维码",
              style: TextStyle(
                  fontSize: 12,
                  color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
              ),
            ),
          ),
          SizedBox(height: 30,),
        ],
      ),
    );
  }

}
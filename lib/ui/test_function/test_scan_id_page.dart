import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/test_function/test_function_view_model.dart';
import 'package:e_reception_flutter/ui/test_function/test_history_page.dart';
import 'package:e_reception_flutter/ui/test_function/test_scan_bind_hsn_id_page.dart';
import 'package:e_reception_flutter/ui/test_function/widgets/input_id_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../app_main.dart';
import 'hsn_id_scan_page.dart';

class TestScanIdPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "TestScanIdPage";
  final String hsn;
  final bool showSkip;

  const TestScanIdPage({Key key, this.hsn, this.showSkip}):super(key: key);

  @override
  _TestScanIdPageState createState() => _TestScanIdPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn, bool showSkip) {
    return RouterUtils.routeForFutureResult(
      context,
      TestScanIdPage(
        hsn: hsn,
        showSkip: showSkip,
      ),
      routeName: TestScanIdPage.ROUTER,
    );
  }

}

class _TestScanIdPageState extends BaseState<TestScanIdPage>{
  String _terminalId = "";
  TestFunctionViewModel _viewModel;
  @override
  void initState() {
    super.initState();
    _viewModel = TestFunctionViewModel(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: [
          _toTopBarWidget(),
          _toInfoWidget(),
        ],
      ),
    );
  }

  Widget _toInfoWidget(){
    return Expanded(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          SizedBox(height: 30,),
          _toScanProcessWidget(),
          SizedBox(height: 9,),
          _toScanProcessTextWidget(),
          SizedBox(height: 30,),
          _toScanIdWidget(),
        ],
      ),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "标记机身编号",
      isShowBack: true,
    );
  }

  Widget _toScanProcessWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: Color(0xFF373C4F),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: Color(0xFF373C4F),
              borderRadius: BorderRadius.circular(7)),
        ),
      ],
    );
  }

  Widget _toScanProcessTextWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(width: 40,),
        Text(
          "扫描屏幕二维码",
          style: TextStyle(
              fontSize: 11,
              height: 1.2,
              color: ThemeRepository.getInstance().getTextColor_D0E0F7()
          ),
        ),
        Spacer(),
        Text(
          "标记机身编号",
          style: TextStyle(
              fontSize: 11,
              height: 1.2,
              color: ThemeRepository.getInstance().getTextColor_D0E0F7()
          ),
        ),
        Spacer(),
        Text(
          "信息确认",
          style: TextStyle(
            fontSize: 11,
            height: 1.2,
            color: Color(0xFF373C4F),
          ),
        ),
        SizedBox(width: 40,),
      ],
    );
  }


  Widget _toScanIdWidget(){
    return Stack(
      children: [
        Column(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: 94),
              width: ScreenUtils.screenW(context),
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "屏幕显示编码：",
                    style: TextStyle(
                        fontSize: 11,
                        color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    widget?.hsn,
                    style: TextStyle(
                      fontSize: 20,
                      color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                      height: 1.2,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: (){
                      InputIdDialog.navigatorPushDialog(context, id: _terminalId, onConfirm: (value){
                        if(StringUtils.isNotEmpty(value)){
                          TestScanBindHsnIdPage.navigatorPush(context, widget?.hsn, value, widget?.showSkip);
                        }
                        setState(() {
                          _terminalId = value;
                        });
                      });
                    },
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            "机身侧面编号：",
                            style: TextStyle(
                                fontSize: 11,
                                color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                            ),
                          ),
                          Spacer(),
                          Text(
                            "手动输入",
                            style: TextStyle(
                                fontSize: 11,
                                color: ThemeRepository.getInstance().getMinorYellowColor_FFB714()
                            ),
                          ),
                          SizedBox(width: 2,),
                          Image.asset(
                            'images/icon_arrow_yellow.png',
                            width: 6,
                            height: 10,
                          ),
                          SizedBox(width: 5,),
                        ],
                      ),
                    ),
                  ),


                  SizedBox(height: 24,),
                  Image.asset(
                    'images/icon_terminal_id_code.png',
                    width: 110,
                    height: 110,
                  ),
                  SizedBox(height: 30,),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: ()async{
                      String result = await HsnIdScanPage.navigatorPush(context, HsnIdScanPage.TYPE_ID, hsn: widget?.hsn, updateFlag: widget?.showSkip);
                      if(StringUtils.isNotEmpty(result)){
                        TestScanIdPage.navigatorPush(context, result, true);
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                          borderRadius: BorderRadius.circular(8)
                      ),
                      height: 40,
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'images/icon_scan_hint.png',
                            width: 20,
                            height: 20,
                          ),
                          SizedBox(width: 6,),
                          Text(
                            "扫描机身侧面的二维码",
                            style: TextStyle(
                                fontSize:15,
                                color: Colors.white,
                                height: 1.2
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: widget?.showSkip??true,
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: (){
                        _viewModel.bindHsnAndId(context, widget?.hsn, "", (){
                          RouterUtils.popUntil(context, AppMain.ROUTER);
                          TestHistoryPage.navigatorPush(context);
                        });
                      },
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(25),
                        child: Text(
                          "暂无机身二维码，跳过",
                          style: TextStyle(
                              fontSize: 14,
                              color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                ],
              ),
            ),

          ],
        ),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 87),
          child: Align(
            alignment: Alignment.center,
            child: Image.asset(
              'images/icon_scan_link.png',
              width: 24,
              height: 24,
            ),
          ),
        )
      ],
    );
  }

}
class SimInfoJsonBean {

  String phone;
  String operator;
  String agent;
  String setMeal;
  String flowSharing;
  String hsn;

  Map toJson() => {
    "phone": phone,
    "operator": operator,
    "agent": agent,
    "setMeal": setMeal,
    "flowSharing": flowSharing,
    "hsn": hsn,
  };
}

class TestUploadBean {
  String androidVersion;//安卓版本号 00未检测 01通过 02未通过
  String autoUpgrade;//自动升级 00未检测 01通过 02未通过
  String backup;//备注
  String basicInfo;//基本信息设置 00未检测 01通过 02未通过
  String bind;//设备绑定 00未检测 01通过 02未通过
  String cameraAngle;//摄像头角度 00未检测 01通过 02未通过
  String delflg;//00正常 01删除
  String face;//人脸识别 00关闭 01打开
  String filePlay;//文件播放 00未检测 01通过 02未通过
  String homeScreenLock;//主屏幕锁定 00未检测 01通过 02未通过
  String hsn;//分类标签id
  String login;//主体登陆 00未检测 01通过 02未通过
  String mobileData;//移动数据 00未检测 01通过 02未通过
  String openDeviceManage;//启用设备管理 00未检测 01通过 02未通过
  int passcnt = 0;//通过数
  String sideNumber;//机身编号
  String sim;//SIM卡号
  String status;//状态 00不合格 01合格
  String stype;//SIM卡种类
  String temperature;//测温识别 00关闭 01打开
  String timeSwitch;//定时开关 00未检测 01通过 02未通过
  String tts;//TTS中文语音播报 00未检测 01通过 02未通过
  String wlan;

  TestUploadBean(
      {this.androidVersion,
        this.autoUpgrade,
        this.backup,
        this.basicInfo,
        this.bind,
        this.cameraAngle,
        this.delflg,
        this.face,
        this.filePlay,
        this.homeScreenLock,
        this.hsn,
        this.login,
        this.mobileData,
        this.openDeviceManage,
        this.passcnt,
        this.sideNumber,
        this.sim,
        this.status,
        this.stype,
        this.temperature,
        this.timeSwitch,
        this.tts,
        this.wlan
      }); //WLAN 00未检测 01通过 02未通过

  Map toJson() => {
    "androidVersion": androidVersion??""??"",
    "autoUpgrade": autoUpgrade??"",
    "backup": backup??"",
    "basicInfo": basicInfo??"",
    "bind": bind??"",
    "cameraAngle": cameraAngle??"",
    "delflg": delflg??"",
    "face": face??"",
    "filePlay": filePlay??"",
    "homeScreenLock": homeScreenLock??"",
    "hsn": hsn??"",
    "login": login??"",
    "mobileData": mobileData??"",
    "openDeviceManage": homeScreenLock??"",
    "passcnt": passcnt??0,
    "sideNumber": sideNumber??"",
    "sim": sim??"",
    "status": status??"",
    "stype": stype??"",
    "temperature": temperature??"",
    "timeSwitch": timeSwitch??"",
    "tts": tts??"",
    "wlan": wlan??"",
  };
}
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"nick":"敬老师","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACE3BA","passcnt":0,"allcnt":13,"updatedate":1630568821,"sideNumber":"6948939665959","status":"00"},{"nick":"庞蕊","hsn":"12312313","passcnt":0,"allcnt":13,"updatedate":1629194152,"status":"00"},{"nick":"敬老师","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","passcnt":0,"allcnt":13,"updatedate":1630486148,"status":"01"},{"nick":"庞蕊","hsn":"3","passcnt":0,"allcnt":13,"updatedate":1629194151,"status":"01"},{"nick":"庞蕊","hsn":"2","passcnt":0,"allcnt":13,"updatedate":1629194150,"status":"01"}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class TestRecordBean extends BasePagerBean<TestItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static TestRecordBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestRecordBean testRecordBeanBean = TestRecordBean();
    testRecordBeanBean.success = map['success'];
    testRecordBeanBean.code = map['code'];
    testRecordBeanBean.msg = map['msg'];
    testRecordBeanBean.data = DataBean.fromMap(map['data']);
    testRecordBeanBean.extra = map['extra'];
    return testRecordBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  @override
  List<TestItemBean> getDataList()  => data.page.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// page : {"records":[{"nick":"敬老师","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACE3BA","passcnt":0,"allcnt":13,"updatedate":1630568821,"sideNumber":"6948939665959","status":"00"},{"nick":"庞蕊","hsn":"12312313","passcnt":0,"allcnt":13,"updatedate":1629194152,"status":"00"},{"nick":"敬老师","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","passcnt":0,"allcnt":13,"updatedate":1630486148,"status":"01"},{"nick":"庞蕊","hsn":"3","passcnt":0,"allcnt":13,"updatedate":1629194151,"status":"01"},{"nick":"庞蕊","hsn":"2","passcnt":0,"allcnt":13,"updatedate":1629194150,"status":"01"}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"nick":"敬老师","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACE3BA","passcnt":0,"allcnt":13,"updatedate":1630568821,"sideNumber":"6948939665959","status":"00"},{"nick":"庞蕊","hsn":"12312313","passcnt":0,"allcnt":13,"updatedate":1629194152,"status":"00"},{"nick":"敬老师","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","passcnt":0,"allcnt":13,"updatedate":1630486148,"status":"01"},{"nick":"庞蕊","hsn":"3","passcnt":0,"allcnt":13,"updatedate":1629194151,"status":"01"},{"nick":"庞蕊","hsn":"2","passcnt":0,"allcnt":13,"updatedate":1629194150,"status":"01"}]
/// total : 5
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<TestItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => TestItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// nick : "敬老师"
/// hsn : "11140C1F12FEEB2C52DFBEF8EE0AACE3BA"
/// passcnt : 0
/// allcnt : 13
/// updatedate : 1630568821
/// sideNumber : "6948939665959"
/// status : "00"

class TestItemBean {
  String nick;
  String hsn;
  int passcnt;
  int allcnt;
  int updatedate;
  String sideNumber;
  String status;

  static TestItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestItemBean recordsBean = TestItemBean();
    recordsBean.nick = map['nick'];
    recordsBean.hsn = map['hsn'];
    recordsBean.passcnt = map['passcnt'];
    recordsBean.allcnt = map['allcnt'];
    recordsBean.updatedate = map['updatedate'];
    recordsBean.sideNumber = map['sideNumber'];
    recordsBean.status = map['status'];
    return recordsBean;
  }

  Map toJson() => {
    "nick": nick,
    "hsn": hsn,
    "passcnt": passcnt,
    "allcnt": allcnt,
    "updatedate": updatedate,
    "sideNumber": sideNumber,
    "status": status,
  };
}
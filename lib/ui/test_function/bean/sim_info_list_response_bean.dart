/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"SIMInfoList":[{"id":1,"type":"00","content":"中国电信"},{"id":2,"type":"00","content":"中国移动"},{"id":3,"type":"00","content":"中国联通"}]}
/// extra : null

class SimInfoListResponseBean {
  bool success;
  String code;
  String msg;
  SimInfoListDataBean data;
  dynamic extra;

  static SimInfoListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SimInfoListResponseBean simInfoListResponseBeanBean = SimInfoListResponseBean();
    simInfoListResponseBeanBean.success = map['success'];
    simInfoListResponseBeanBean.code = map['code'];
    simInfoListResponseBeanBean.msg = map['msg'];
    simInfoListResponseBeanBean.data = SimInfoListDataBean.fromMap(map['data']);
    simInfoListResponseBeanBean.extra = map['extra'];
    return simInfoListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// SIMInfoList : [{"id":1,"type":"00","content":"中国电信"},{"id":2,"type":"00","content":"中国移动"},{"id":3,"type":"00","content":"中国联通"}]

class SimInfoListDataBean {
  List<SIMInfoListBean> SIMInfoList;

  static SimInfoListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SimInfoListDataBean dataBean = SimInfoListDataBean();
    dataBean.SIMInfoList = List()..addAll(
      (map['SIMInfoList'] as List ?? []).map((o) => SIMInfoListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "SIMInfoList": SIMInfoList,
  };
}

/// id : 1
/// type : "00"
/// content : "中国电信"

class SIMInfoListBean {
  int id;
  String type;
  String content;

  static SIMInfoListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SIMInfoListBean sIMInfoListBean = SIMInfoListBean();
    sIMInfoListBean.id = map['id'];
    sIMInfoListBean.type = map['type'];
    sIMInfoListBean.content = map['content'];
    return sIMInfoListBean;
  }

  Map toJson() => {
    "id": id,
    "type": type,
    "content": content,
  };
}
/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : 59
/// extra : null

class TestUnQualifiedCountResponseBean {
  bool success;
  String code;
  String msg;
  int data;
  dynamic extra;

  static TestUnQualifiedCountResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestUnQualifiedCountResponseBean testUnQualifiedCountResponseBeanBean = TestUnQualifiedCountResponseBean();
    testUnQualifiedCountResponseBeanBean.success = map['success'];
    testUnQualifiedCountResponseBeanBean.code = map['code'];
    testUnQualifiedCountResponseBeanBean.msg = map['msg'];
    testUnQualifiedCountResponseBeanBean.data = map['data'];
    testUnQualifiedCountResponseBeanBean.extra = map['extra'];
    return testUnQualifiedCountResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}
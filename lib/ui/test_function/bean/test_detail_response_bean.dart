import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"outFactoryDetails":{"hsn":"11140C1F12FEEB2C52DFBEF8EE0AACE3BA","allcnt":13,"homeScreenLock":"00","createdate":1630568821,"login":"00","basicInfo":"00","timeSwitch":"00","delflg":"00","nick":"敬老师","bind":"00","bjtime":"2021-09-02T15:47:01.000+00:00","updatedate":1630568821,"sideNumber":"6948939665959","temperature":"00","mobileData":"00","autoUpgrade":"00","tts":"00","passcnt":0,"face":"00","wlan":"00","androidVersion":"00","filePlay":"00","updateuid":"a83985a843544231842d8e44ebe3432b","cameraAngle":"00","createuid":"a83985a843544231842d8e44ebe3432b","status":"00","openDeviceManage":"00"}}
/// extra : null

class TestDetailResponseBean {
  bool success;
  String code;
  String msg;
  TestDetailAndCardInfoBean data;
  dynamic extra;

  static TestDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestDetailResponseBean testDetailResponseBeanBean = TestDetailResponseBean();
    testDetailResponseBeanBean.success = map['success'];
    testDetailResponseBeanBean.code = map['code'];
    testDetailResponseBeanBean.msg = map['msg'];
    testDetailResponseBeanBean.data = TestDetailAndCardInfoBean.fromMap(map['data']);
    testDetailResponseBeanBean.extra = map['extra'];
    return testDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// outFactoryDetails : {"hsn":"11140C1F12FEEB2C52DFBEF8EE0AACE3BA","allcnt":13,"homeScreenLock":"00","createdate":1630568821,"login":"00","basicInfo":"00","timeSwitch":"00","delflg":"00","nick":"敬老师","bind":"00","bjtime":"2021-09-02T15:47:01.000+00:00","updatedate":1630568821,"sideNumber":"6948939665959","temperature":"00","mobileData":"00","autoUpgrade":"00","tts":"00","passcnt":0,"face":"00","wlan":"00","androidVersion":"00","filePlay":"00","updateuid":"a83985a843544231842d8e44ebe3432b","cameraAngle":"00","createuid":"a83985a843544231842d8e44ebe3432b","status":"00","openDeviceManage":"00"}

class TestDetailAndCardInfoBean {
  AttL4gCardBean attL4gCard;
  TestDetailBean outFactoryDetails;

  static TestDetailAndCardInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestDetailAndCardInfoBean dataBean = TestDetailAndCardInfoBean();
    dataBean.attL4gCard = AttL4gCardBean.fromMap(map['attL4gCard']);
    dataBean.outFactoryDetails = TestDetailBean.fromMap(map['outFactoryDetails']);
    return dataBean;
  }

  Map toJson() => {
    "attL4gCard": attL4gCard,
    "outFactoryDetails": outFactoryDetails,
  };
}

class AttL4gCardBean {
  String iccid;
  String hsn;
  dynamic imsi;
  String phone;
  String operator;
  String agent;
  String flowSharing;
  String setMeal;
  int activetime;
  int expiretime;
  dynamic backup;
  int createtime;
  String createuid;
  int uptime;
  String upuid;
  String delflg;
  String bjtime;

  static AttL4gCardBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AttL4gCardBean attL4gCardBean = AttL4gCardBean();
    attL4gCardBean.iccid = map['iccid'];
    attL4gCardBean.hsn = map['hsn'];
    attL4gCardBean.imsi = map['imsi'];
    attL4gCardBean.phone = map['phone'];
    attL4gCardBean.operator = map['operator'];
    attL4gCardBean.agent = map['agent'];
    attL4gCardBean.flowSharing = map['flowSharing'];
    attL4gCardBean.setMeal = map['setMeal'];
    attL4gCardBean.activetime = map['activetime'];
    attL4gCardBean.expiretime = map['expiretime'];
    attL4gCardBean.backup = map['backup'];
    attL4gCardBean.createtime = map['createtime'];
    attL4gCardBean.createuid = map['createuid'];
    attL4gCardBean.uptime = map['uptime'];
    attL4gCardBean.upuid = map['upuid'];
    attL4gCardBean.delflg = map['delflg'];
    attL4gCardBean.bjtime = map['bjtime'];
    return attL4gCardBean;
  }

  String getSimInfo(){
    String info = "";
    if(StringUtils.isNotEmpty(operator)){
      info += operator;
      info += ";";
    }
    if(StringUtils.isNotEmpty(iccid)){
      info += iccid;
      info += ";";
    }
    if(StringUtils.isNotEmpty(agent)){
      info += agent;
      info += ";";
    }
    if(StringUtils.isNotEmpty(setMeal)){
      info += setMeal;
      info += ";";
    }
    if(StringUtils.isNotEmpty(info)){
      return info;
    }
    return null;
  }

  Map toJson() => {
    "iccid": iccid,
    "hsn": hsn,
    "imsi": imsi,
    "phone": phone,
    "operator": operator,
    "agent": agent,
    "flowSharing": flowSharing,
    "setMeal": setMeal,
    "activetime": activetime,
    "expiretime": expiretime,
    "backup": backup,
    "createtime": createtime,
    "createuid": createuid,
    "uptime": uptime,
    "upuid": upuid,
    "delflg": delflg,
    "bjtime": bjtime,
  };
}

/// hsn : "11140C1F12FEEB2C52DFBEF8EE0AACE3BA"
/// allcnt : 13
/// homeScreenLock : "00"
/// createdate : 1630568821
/// login : "00"
/// basicInfo : "00"
/// timeSwitch : "00"
/// delflg : "00"
/// nick : "敬老师"
/// bind : "00"
/// bjtime : "2021-09-02T15:47:01.000+00:00"
/// updatedate : 1630568821
/// sideNumber : "6948939665959"
/// temperature : "00"
/// mobileData : "00"
/// autoUpgrade : "00"
/// tts : "00"
/// passcnt : 0
/// face : "00"
/// wlan : "00"
/// androidVersion : "00"
/// filePlay : "00"
/// updateuid : "a83985a843544231842d8e44ebe3432b"
/// cameraAngle : "00"
/// createuid : "a83985a843544231842d8e44ebe3432b"
/// status : "00"
/// openDeviceManage : "00"

class TestDetailBean {
  String hsn;
  int allcnt;
  String homeScreenLock;
  int createdate;
  String login;
  String basicInfo;
  String timeSwitch;
  String delflg;
  String nick;
  String bind;
  String bjtime;
  int updatedate;
  String sideNumber;
  String temperature;
  String mobileData;
  String autoUpgrade;
  String tts;
  int passcnt;
  String face;
  String wlan;
  String androidVersion;
  String filePlay;
  String updateuid;
  String cameraAngle;
  String createuid;
  String status;
  String openDeviceManage;
  String sim;

  static TestDetailBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TestDetailBean outFactoryDetailsBean = TestDetailBean();
    outFactoryDetailsBean.hsn = map['hsn'];
    outFactoryDetailsBean.allcnt = map['allcnt'];
    outFactoryDetailsBean.homeScreenLock = map['homeScreenLock'];
    outFactoryDetailsBean.createdate = map['createdate'];
    outFactoryDetailsBean.login = map['login'];
    outFactoryDetailsBean.basicInfo = map['basicInfo'];
    outFactoryDetailsBean.timeSwitch = map['timeSwitch'];
    outFactoryDetailsBean.delflg = map['delflg'];
    outFactoryDetailsBean.nick = map['nick'];
    outFactoryDetailsBean.bind = map['bind'];
    outFactoryDetailsBean.bjtime = map['bjtime'];
    outFactoryDetailsBean.updatedate = map['updatedate'];
    outFactoryDetailsBean.sideNumber = map['sideNumber'];
    outFactoryDetailsBean.temperature = map['temperature'];
    outFactoryDetailsBean.mobileData = map['mobileData'];
    outFactoryDetailsBean.autoUpgrade = map['autoUpgrade'];
    outFactoryDetailsBean.tts = map['tts'];
    outFactoryDetailsBean.passcnt = map['passcnt'];
    outFactoryDetailsBean.face = map['face'];
    outFactoryDetailsBean.wlan = map['wlan'];
    outFactoryDetailsBean.androidVersion = map['androidVersion'];
    outFactoryDetailsBean.filePlay = map['filePlay'];
    outFactoryDetailsBean.updateuid = map['updateuid'];
    outFactoryDetailsBean.cameraAngle = map['cameraAngle'];
    outFactoryDetailsBean.createuid = map['createuid'];
    outFactoryDetailsBean.status = map['status'];
    outFactoryDetailsBean.openDeviceManage = map['openDeviceManage'];
    outFactoryDetailsBean.sim = map['sim'];
    return outFactoryDetailsBean;
  }

  Map toJson() => {
    "hsn": hsn,
    "allcnt": allcnt,
    "homeScreenLock": homeScreenLock,
    "createdate": createdate,
    "login": login,
    "basicInfo": basicInfo,
    "timeSwitch": timeSwitch,
    "delflg": delflg,
    "nick": nick,
    "bind": bind,
    "bjtime": bjtime,
    "updatedate": updatedate,
    "sideNumber": sideNumber,
    "temperature": temperature,
    "mobileData": mobileData,
    "autoUpgrade": autoUpgrade,
    "tts": tts,
    "passcnt": passcnt,
    "face": face,
    "wlan": wlan,
    "androidVersion": androidVersion,
    "filePlay": filePlay,
    "updateuid": updateuid,
    "cameraAngle": cameraAngle,
    "createuid": createuid,
    "status": status,
    "openDeviceManage": openDeviceManage,
    "sim": sim,
  };
}
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/test_function/test_function_view_model.dart';
import 'package:e_reception_flutter/ui/test_function/test_terminal_detail_page.dart';
import 'package:e_reception_flutter/ui/test_function/widgets/input_id_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

import '../app_main.dart';

class TestScanBindHsnIdPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "TestScanBindHsnIdPage";
  final String hsn;
  final String id;
  //标记是否是更新机身编码
  final bool updateFlag;

  const TestScanBindHsnIdPage({Key key, this.hsn, this.id, this.updateFlag}):super(key: key);

  @override
  _TestScanBindHsnIdPageState createState() => _TestScanBindHsnIdPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn, String id, bool updateFlag) {
    return RouterUtils.routeForFutureResult(
      context,
      TestScanBindHsnIdPage(
        hsn: hsn,
        id: id,
        updateFlag: updateFlag,
      ),
      routeName: TestScanBindHsnIdPage.ROUTER,
    );
  }

}

class _TestScanBindHsnIdPageState extends BaseState<TestScanBindHsnIdPage>{
  TestFunctionViewModel _viewModel;
  @override
  void initState() {
    super.initState();
    _viewModel = new TestFunctionViewModel(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: [
          _toTopBarWidget(),
          _toInfoWidget(),
        ],
      ),
    );
  }

  Widget _toInfoWidget(){
    return Expanded(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          SizedBox(height: 30,),
          _toScanProcessWidget(),
          SizedBox(height: 9,),
          _toScanProcessTextWidget(),
          SizedBox(height: 30,),
          _toScanIdWidget(),
          SizedBox(height: 30,),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              if(widget?.updateFlag??false){
                _viewModel.bindHsnAndId(context, widget?.hsn, widget?.id, (){
                  RouterUtils.popUntil(context, AppMain.ROUTER);
                  TestTerminalDetailPage.navigatorPush(context, widget?.hsn, TestScanBindHsnIdPage.ROUTER);
                });
              }else{
                _viewModel.updateId(context, widget?.hsn, widget?.id, callback: (){
                  RouterUtils.popUntil(context, AppMain.ROUTER);
                  TestTerminalDetailPage.navigatorPush(context, widget?.hsn, TestScanBindHsnIdPage.ROUTER);
                });
              }

            },
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  borderRadius: BorderRadius.circular(8)
              ),
              height: 40,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 21),
              child: Text(
                "确认无误，关联",
                style: TextStyle(
                    fontSize:15,
                    color: Colors.white,
                    height: 1.2
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "标记机身编号",
      isShowBack: true,
    );
  }

  Widget _toScanProcessWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
        SizedBox(width: 10,),
        Container(
          width: 91,
          height: 3,
          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
        ),
        SizedBox(width: 10,),
        Container(
          height: 14,
          width: 14,
          constraints: BoxConstraints(minWidth: 14),
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
              borderRadius: BorderRadius.circular(7)),
        ),
      ],
    );
  }

  Widget _toScanProcessTextWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(width: 40,),
        Text(
          "扫描屏幕二维码",
          style: TextStyle(
              fontSize: 11,
              height: 1.2,
              color: ThemeRepository.getInstance().getTextColor_D0E0F7()
          ),
        ),
        Spacer(),
        Text(
          "标记机身编号",
          style: TextStyle(
              fontSize: 11,
              height: 1.2,
              color: ThemeRepository.getInstance().getTextColor_D0E0F7()
          ),
        ),
        Spacer(),
        Text(
          "信息确认",
          style: TextStyle(
            fontSize: 11,
            height: 1.2,
            color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
          ),
        ),
        SizedBox(width: 40,),
      ],
    );
  }

  Widget _toScanHsnWidget(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          borderRadius: BorderRadius.circular(8)
      ),
      child: Column(
        children: [
          SizedBox(height: 46,),
          Image.asset(
            'images/icon_scan_hsn_hint.png',
            width: 110,
            height: 110,
          ),
          SizedBox(height: 30,),
          Container(
            decoration: BoxDecoration(
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                borderRadius: BorderRadius.circular(8)
            ),
            height: 40,
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'images/icon_scan_hint.png',
                  width: 20,
                  height: 20,
                ),
                SizedBox(width: 6,),
                Text(
                  "扫描屏幕上二维码",
                  style: TextStyle(
                      fontSize:15,
                      color: Colors.white,
                      height: 1.2
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20,),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(horizontal: 27),
            child: Text(
              "1. 显示屏安装“AI显示端”软件\n2. 显示屏将“AI显示端”软件设置为主程序\n3. 扫描显示屏“屏幕”上的二维码",
              style: TextStyle(
                  fontSize: 12,
                  color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
              ),
            ),
          ),
          SizedBox(height: 30,),
        ],
      ),
    );
  }

  Widget _toScanIdWidget(){
    return Stack(
      children: [
        Column(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: 94,),
              width: ScreenUtils.screenW(context),
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "屏幕显示编码：",
                    style: TextStyle(
                        fontSize: 11,
                        color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    widget?.hsn,
                    style: TextStyle(
                        fontSize: 20,
                        color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                        height: 1.2,
                        fontWeight: FontWeight.w600
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Container(
              alignment: Alignment.centerLeft,
              width: ScreenUtils.screenW(context),
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "机身侧面编号：",
                    style: TextStyle(
                        fontSize: 11,
                        color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    ),
                  ),
                  SizedBox(height: 2,),
                  Text(
                    widget?.id??"",
                    style: TextStyle(
                        fontSize: 20,
                        color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                        height: 1.2,
                        fontWeight: FontWeight.w600
                    ),
                  ),

                  SizedBox(height: 10,),
                ],
              ),
            ),

          ],
        ),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 87),
          child: Align(
            alignment: Alignment.center,
            child: Image.asset(
              'images/icon_scan_link.png',
              width: 24,
              height: 24,
            ),
          ),
        )
      ],
    );
  }

}
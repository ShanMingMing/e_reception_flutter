import 'dart:convert';

import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/test_function/bean/sim_info_json_bean.dart';
import 'package:e_reception_flutter/ui/test_function/refresh_test_terminal_detail_page_event.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../app_main.dart';
import 'bean/sim_info_list_response_bean.dart';

class SimViewModel extends BaseViewModel{

  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  ValueNotifier<SimInfoListDataBean> simInfoListValueNotifier;
  SimViewModel(BaseState<StatefulWidget> state)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    simInfoListValueNotifier = ValueNotifier(null);
  }

  ///获取运营商 代理商 套餐规格信息
  ///type 00运营商 01代理商 02套餐规格
  void getSimInfoList(BuildContext context, String type){
    VgHttpUtils.get(NetApi.GET_SIM_INFO_LIST, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "type":type??"",
    },callback: BaseCallback(
        onSuccess: (val){
          SimInfoListResponseBean bean =
          SimInfoListResponseBean.fromMap(val);
          if(bean != null){
            if(!isStateDisposed){
              statusTypeValueNotifier?.value = null;
              simInfoListValueNotifier?.value = bean?.data;
            }
            loading(false);
          }
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///新增运营商 代理商 套餐规格信息
  ///type 00运营商 01代理商 02套餐规格
  void addSimInfo(BuildContext context, String content, String type){
    loading(true,);
    VgHttpUtils.post(NetApi.ADD_SIM_INFO_LIST, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "content":content??"",
      "type":type??"",
    },callback: BaseCallback(
        onSuccess: (val){
          loading(false);
          RouterUtils.pop(context, result: content);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///录入sim信息
  void saveSim(BuildContext context, String phone, String operator,
      String agent, String setMeal, String flowSharing, String hsn){
    if(StringUtils.isEmpty(phone)){
      return;
    }
    SimInfoJsonBean jsonBean = new SimInfoJsonBean();
    jsonBean.phone = phone;
    jsonBean.operator = operator;
    jsonBean.agent = agent;
    jsonBean.setMeal = setMeal;
    jsonBean.flowSharing = flowSharing;
    jsonBean.hsn = hsn;
    loading(true, msg: "正在保存");
    VgHttpUtils.post(NetApi.SAVE_SIM_INFO,
        params: {
          "authId":UserRepository.getInstance().authId ?? "",
        },
        body: jsonEncode(jsonBean),
        callback: BaseCallback(
            onSuccess: (val){
              VgToastUtils.toast(AppMain.context, "保存成功");
              VgEventBus.global.send(new RefreshTestTerminalDetailPageEvent());
              RouterUtils.pop(context);
            },
            onError: (msg){
              loading(false);
              VgToastUtils.toast(AppMain.context, msg);
            }
        ));
  }

  ///编辑sim信息
  void editSim(BuildContext context, String phone, String operator,
      String agent, String setMeal, String flowSharing, String hsn){
    if(StringUtils.isEmpty(phone)){
      return;
    }
    loading(true, msg: "正在保存");
    VgHttpUtils.post(NetApi.EDIT_SIM_INFO,
        params: {
          "authId":UserRepository.getInstance().authId ?? "",
          "phone":phone ?? "",
          "hsn":hsn ?? "",
          "operator":operator ?? "",
          "agent":agent ?? "",
          "set_meal":setMeal ?? "",
          "flow_sharing":flowSharing ?? "00",
        },
        callback: BaseCallback(
            onSuccess: (val){
              VgToastUtils.toast(AppMain.context, "保存成功");
              VgEventBus.global.send(new RefreshTestTerminalDetailPageEvent());
              RouterUtils.pop(context);
            },
            onError: (msg){
              loading(false);
              VgToastUtils.toast(AppMain.context, msg);
            }
        ));
  }
}
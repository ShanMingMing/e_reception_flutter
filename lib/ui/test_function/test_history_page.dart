import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/test_function/test_function_view_model.dart';
import 'package:e_reception_flutter/ui/test_function/test_scan_hsn_page.dart';
import 'package:e_reception_flutter/ui/test_function/test_terminal_detail_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import 'bean/test_record_bean.dart';

class TestHistoryPage extends StatefulWidget{

  static const String ROUTER = "TestHistoryPage";

  const TestHistoryPage({Key key,}) : super(key: key);

  @override
  _TestHistoryPageState createState() => _TestHistoryPageState();

  static Future<TestItemBean> navigatorPush(BuildContext context,) {
    return RouterUtils.routeForFutureResult(
      context,
      TestHistoryPage(
      ),
      routeName: TestHistoryPage.ROUTER,
    );
  }

}

class _TestHistoryPageState extends BasePagerState<TestItemBean, TestHistoryPage>
    with VgPlaceHolderStatusMixin{
  TestFunctionViewModel _viewModel;
  String _searchStr;
  @override
  void initState() {
    super.initState();
    _viewModel = TestFunctionViewModel(this, searchStrFunc: ()=>_searchStr);
    _viewModel.refresh();
  }

  @override
  void dispose() {
    _viewModel.onDisposed();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(
        child: Column(
          children: [
            _toTopBarWidget(),
            _toSearchWidget(),
            // _toPlaceHolderWidget(),
            Expanded(child: _toPlaceHolderWidget()),
          ],
        ),
      ),
    );
  }

  ///标题栏
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "出厂检测",
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  ///搜索
  Widget _toSearchWidget(){
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          CommonSearchBarWidget(
            hintText: "搜索设备ID",
            onSubmitted: (String searchStr) {
              _searchStr = searchStr;
              _viewModel?.refreshMultiPage();
            },
          ),
        ],
      ),
    );
  }


  Widget _toPlaceHolderWidget(){
    return Stack(
      children: [
        VgPlaceHolderStatusWidget(
          errorOnClick: () => _viewModel?.refresh(),
          emptyOnClick: () => _viewModel?.refresh(),
          loadingStatus: data == null,
          // emptyStatus: data?.length == 0,
          emptyStatus: false,
          child:_toListParentWidget(),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              RouterUtils.pop(context);
              TestScanHsnPage.navigatorPush(context);
            },
            child: Container(
              alignment: Alignment.center,
              width: 164,
              height: 40,
              margin: const EdgeInsets.only(bottom: 52),
              decoration: BoxDecoration(
                color: Color(0xFF1890FF),
                borderRadius: BorderRadius.circular(24),
              ),
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset(
                      'images/icon_scan_hint.png',
                      width: 20,
                      color: Colors.white,),
                    SizedBox(width: 5,),
                    Text(
                      "扫码检测新终端",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          height: 1.2,
                          fontWeight: FontWeight.w600
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _toListParentWidget() {
    return VgPullRefreshWidget.bind(
        state: this,
        viewModel: _viewModel,
        child: _toListWidget());
  }

  Widget _toListWidget() {
    return ListView.separated(
        itemCount: data?.length ?? 0,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(context, index, data[index]);
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            height: 1,
            color: Color(0xFF303546),
          );
        });
  }

  Widget _toListItemWidget(BuildContext context, int index, TestItemBean itemBean){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        TestTerminalDetailPage.navigatorPush(context, itemBean?.hsn, TestHistoryPage.ROUTER);
      },
      child: Stack(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "屏幕显示编码：",
                      style: TextStyle(
                          fontSize: 12,
                          color: ThemeRepository.getInstance().getTextColor_D0E0F7()
                      ),
                    ),
                    Spacer(),
                    Text(
                      "${itemBean?.nick??"-"}·${TimeUtil.format((itemBean?.updatedate??0)*1000)}",
                      style: TextStyle(
                          fontSize: 10,
                          color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                      ),
                    ),
                  ],
                ),
                Text(
                  "${itemBean?.hsn??"-"}",
                  textAlign: TextAlign.start,
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 16,
                      color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                      fontWeight: FontWeight.w600,
                      height: 1.2
                  ),
                ),
                SizedBox(height: 5,),
                Row(
                  children: [
                    Text(
                      "机身侧面编号：",
                      style: TextStyle(
                          fontSize: 12,
                          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                          height: 1.2
                      ),
                    ),
                    Text(
                      "${itemBean?.sideNumber??"-"}",
                      style: TextStyle(
                          fontSize: 12,
                          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                          height: 1.2
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "检测进度：",
                      style: TextStyle(
                          fontSize: 12,
                          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                          height: 1.1
                      ),
                    ),
                    Text(
                      "${itemBean?.passcnt??0}",
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF02B7B7),
                          height: 1.2
                      ),
                    ),
                    Text(
                      "/",
                      style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF02B7B7),
                          height: 1.2
                      ),
                    ),
                    Text(
                      "${itemBean?.allcnt??0}",
                      style: TextStyle(
                          fontSize: 12,
                          color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                          height: 1.2
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            right: 15,
            bottom: 15,
            child: ("01" == (itemBean?.status??"01"))?_toFinishWidget():_toUnCompleteWidget(),
          ),
        ],
      ),
    );
  }

  Widget _toUnCompleteWidget(){
    return Container(
      alignment: Alignment.center,
      width: 63,
      height: 28,
      decoration: BoxDecoration(
        color: ThemeRepository.getInstance().getMinorRedColor_F95355(),
        borderRadius: BorderRadius.circular(2),
      ),
      child: Text(
        "未完成",
        style: TextStyle(
            fontSize: 13,
            color: Colors.white
        ),
      ),
    );
  }

  Widget _toFinishWidget(){
    return Container(
      alignment: Alignment.center,
      width: 63,
      height: 28,
      decoration: BoxDecoration(
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        borderRadius: BorderRadius.circular(2),
        border:  Border.all(color: ThemeRepository.getInstance().getHintGreenColor_5E687C(), width: 1),
      ),
      child: Text(
        "已完成",
        style: TextStyle(
          fontSize: 13,
          color: Color(0xFF00C6C4),
        ),
      ),
    );
  }

}
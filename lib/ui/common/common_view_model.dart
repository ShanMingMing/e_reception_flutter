import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/common/update_diy_center_info_event.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import '../app_main.dart';
import 'bean/diy_center_info_response_bean.dart';

class CommonViewModel extends BaseViewModel{

  ValueNotifier<DiyCenterInfoData> diyCenterInfoValueNotifier;
  CommonViewModel(BaseState state) : super(state){
    diyCenterInfoValueNotifier = ValueNotifier(null);
  }


  ///获取媒体文件详情
  void getBusinessApplicationInfo(){
    VgHttpUtils.get(NetApi.GET_DIY_CENTER_INFO,params: {
      "authId":UserRepository.getInstance().authId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          DiyCenterInfoResponseBean bean =
          DiyCenterInfoResponseBean.fromMap(val);
          if(!isStateDisposed){
            diyCenterInfoValueNotifier?.value = bean?.data;
          }
          loading(false);
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///编辑行业应用
  void editBusinessApplicationInfo(BuildContext context, String type, String flag,
      String otype1, String oflag1, String otype2, String oflag2,
      String otype3, String oflag3,){
    VgHudUtils.show(context, "请稍后");
    VgHttpUtils.get(NetApi.EDIT_BUSINESS_APPLICATION_INFO,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      type:flag ?? "01",
      otype1:oflag1 ?? "01",
      otype2:oflag2 ?? "01",
      otype3:oflag3 ?? "01",
    },callback: BaseCallback(
        onSuccess: (val){
          VgEventBus.global.send(new UpdateDiyCenterInfoEvent());
          VgHudUtils.hide(context,);
          getBusinessApplicationInfo();
        },
        onError: (msg){
          VgHudUtils.hide(context,);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  @override
  void onDisposed() {
    diyCenterInfoValueNotifier?.dispose();
    super.onDisposed();
  }

}
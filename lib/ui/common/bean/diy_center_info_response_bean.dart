/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"comWBrandCnt":2,"smartHomeStoreCnt":1}
/// extra : null

class DiyCenterInfoResponseBean {
  bool success;
  String code;
  String msg;
  DiyCenterInfoData data;
  dynamic extra;

  static DiyCenterInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DiyCenterInfoResponseBean diyCenterInfoResponseBeanBean = DiyCenterInfoResponseBean();
    diyCenterInfoResponseBeanBean.success = map['success'];
    diyCenterInfoResponseBeanBean.code = map['code'];
    diyCenterInfoResponseBeanBean.msg = map['msg'];
    diyCenterInfoResponseBeanBean.data = DiyCenterInfoData.fromMap(map['data']);
    diyCenterInfoResponseBeanBean.extra = map['extra'];
    return diyCenterInfoResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// comWBrandCnt : 2
/// smartHomeStoreCnt : 1

class DiyCenterInfoData {
  int comWBrandCnt;
  int smartHomeStoreCnt;
  ComIndustryApplicationBean comIndustryApplication;

  static DiyCenterInfoData fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DiyCenterInfoData dataBean = DiyCenterInfoData();
    dataBean.comWBrandCnt = map['comWBrandCnt'];
    dataBean.smartHomeStoreCnt = map['smartHomeStoreCnt'];
    dataBean.comIndustryApplication = ComIndustryApplicationBean.fromMap(map['comIndustryApplication']);
    return dataBean;
  }

  Map toJson() => {
    "comWBrandCnt": comWBrandCnt,
    "smartHomeStoreCnt": smartHomeStoreCnt,
    "comIndustryApplication": comIndustryApplication,
  };
}

class ComIndustryApplicationBean {
  String companyid;
  String waterBrand;
  String smartHome;
  String goodsShow;
  String courseRecommend;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;
  String delflg;
  String bjtime;


  //是否开启楼层指示牌
  bool isOpenBuildingIndex(){
    return "01" == waterBrand;
  }

  //是否开启智能家居
  bool isOpenSmartHome(){
    return "01" == smartHome;
  }
  //是否开启商品展示
  bool isOpenGoodsShow(){
    return "01" == goodsShow;
  }

  //是否开启课程介绍
  bool isOpenCourseIntro(){
    return "01" == courseRecommend;
  }

  //01是使用中
  String getUseInfo(){
    int count = 0;
    if("01" == waterBrand){
      count++;
    }
    if("01" == smartHome){
      count++;
    }
    if("01" == goodsShow){
      count++;
    }
    if("01" == courseRecommend){
      count++;
    }
    return "使用中·" + count.toString();
  }

  static ComIndustryApplicationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComIndustryApplicationBean comIndustryApplicationBean = ComIndustryApplicationBean();
    comIndustryApplicationBean.companyid = map['companyid'];
    comIndustryApplicationBean.waterBrand = map['waterBrand'];
    comIndustryApplicationBean.smartHome = map['smartHome'];
    comIndustryApplicationBean.goodsShow = map['goodsShow'];
    comIndustryApplicationBean.courseRecommend = map['courseRecommend'];
    comIndustryApplicationBean.createuid = map['createuid'];
    comIndustryApplicationBean.createdate = map['createdate'];
    comIndustryApplicationBean.updatetime = map['updatetime'];
    comIndustryApplicationBean.updateuid = map['updateuid'];
    comIndustryApplicationBean.delflg = map['delflg'];
    comIndustryApplicationBean.bjtime = map['bjtime'];
    return comIndustryApplicationBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "waterBrand": waterBrand,
    "smartHome": smartHome,
    "goodsShow": goodsShow,
    "courseRecommend": courseRecommend,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
    "delflg": delflg,
    "bjtime": bjtime,
  };
}
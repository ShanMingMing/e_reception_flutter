import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

///首页管理员上传人脸处理
class IndexAdminUploadFaceViewModel {

  ///App上传预存照片
  static const String APP_SAVE_PU_PICURL_API =
     ServerApi.BASE_URL + "app/appSavePutPicurl";

  IndexAdminUploadFaceViewModel();

  ///APP上传预存照片
  void httpUploadPutPicurl(BuildContext context, String uploadLocalPic) async {
    if (StringUtils.isEmpty(uploadLocalPic)) {
      VgToastUtils.toast(context, "获取预存图片错误");
      return;
    }
    String netUrl =
        await VgMatisseUploadUtils.uploadSingleImageForFuture(uploadLocalPic, isFace: true);
    if (netUrl == null || netUrl.isEmpty) {
      VgToastUtils.toast(context, "图片上传失败");
      return;
    }
    VgHudUtils.show(context,"上传中");
    VgHttpUtils.post(APP_SAVE_PU_PICURL_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": UserRepository.getInstance().userData?.comUser?.fuid ?? "",
          "groupid": UserRepository.getInstance().userData?.comUser?.groupid ?? "",
          "putpicurl": netUrl ?? "",
        },
        callback: BaseCallback(
            onSuccess: (val) {
              VgHudUtils.hide(context);
              VgToastUtils.toast(context, "上传成功");
              UserRepository.getInstance().loginService().autoLogin();
            },
            onError: (msg) {
              VgHudUtils.hide(context);
              VgToastUtils.toast(context, msg);
            }));
  }
}

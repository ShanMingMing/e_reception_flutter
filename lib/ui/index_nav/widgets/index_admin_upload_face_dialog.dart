import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/ui/index_nav/view_model/index_admin_upload_face_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';

/// 首页管理员上传人脸弹窗
///
/// @author: zengxiangxi
/// @createTime: 3/3/21 4:05 PM
/// @specialDemand:
class IndexAdminUploadFaceDialog extends StatelessWidget {
  IndexAdminUploadFaceViewModel _viewModel = IndexAdminUploadFaceViewModel();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context) {
    final String putPicurl =
        UserRepository.getInstance().userData?.comUser?.putpicurl;
    if (putPicurl != null && putPicurl.isNotEmpty) {
      return null;
    }
    print("弹出人脸录入");
    // try{
    //   Navigator.of(context).popUntil(ModalRoute.withName("IndexAdminUploadFaceDialog"));
    // }catch(e){
    //
    // }

    return VgDialogUtils.showCommonDialog(
        context: context,
        child: IndexAdminUploadFaceDialog(),
        barrierDismissible: false,routeSettings:RouteSettings(name: 'IndexAdminUploadFaceDialog'));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: _toMainWidget(context),
      ),
    );
  }

  Widget _toMainWidget(BuildContext context) {
    return Container(
      width: 290,
      decoration: BoxDecoration(
          color: Color(0xFF21263C), borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 35),
          Image.asset(
            "images/index_admin_upload_face_ico.png",
            width: 168,
          ),
          SizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32),
            child: Text(
              "终端人脸识别模式已开启，请上传您的正面清晰人脸照片",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFFD0E0F7),
                fontSize: 14,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          _IndexFaceButtonWidget(
            text: "拍照/上传",
            backgroundColor:
                ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            textColor: Colors.white,
            onTap: () {
              RouterUtils.pop(context);
              _showCameraDialog(AppMain.context);
            },
          ),
          SizedBox(height: 10),
          _IndexFaceButtonWidget(
            text: "以后再说",
            backgroundColor: Colors.transparent,
            textColor: Color(0xFF5E687C),
            onTap: () {
              RouterUtils.pop(context);
            },
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  void _showCameraDialog(BuildContext context) {
    CommonMoreMenuDialog.navigatorPushDialog(context, {
      "上传图片": () async {
        _selectPhoto(context);
      },
      "拍照": () async {
        String path = await FrontCameraHeadPage.navigatorPush(context);
        if (path == null || path.isEmpty) {
          return;
        }
        _httpPic(path);
      },
    });
  }

  void _httpPic(String localPic) {
    _viewModel.httpUploadPutPicurl(AppMain.context, localPic);
  }

  void _selectPhoto(BuildContext context) async {
    String path = await ClipImageHeadBorderUtil.clipOneImage(context,
        scaleY: 1, scaleX: 1, maxAutoFinish: true,
        isShowHeadBorderWidget: true);
    if (path == null || path == "") {
      return;
    }
    _httpPic(path);
  }
}

/// 上传人脸弹窗按钮
///
/// @author: zengxiangxi
/// @createTime: 3/3/21 4:45 PM
/// @specialDemand:
class _IndexFaceButtonWidget extends StatelessWidget {
  final String text;

  final Color backgroundColor;

  final Color textColor;

  final VoidCallback onTap;

  const _IndexFaceButtonWidget(
      {Key key, this.text, this.backgroundColor, this.onTap, this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        height: 45,
        margin: const EdgeInsets.symmetric(horizontal: 32),
        decoration: BoxDecoration(
            color: backgroundColor, borderRadius: BorderRadius.circular(22.5)),
        child: Center(
          child: Text(
            text ?? "",
            style: TextStyle(
              color: textColor,
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }
}

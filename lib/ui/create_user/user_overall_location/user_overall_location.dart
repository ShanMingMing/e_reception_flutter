import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';

class UserOverallLocationUtil extends BaseViewModel{
  ValueNotifier<List<CompanyAddressListInfoBean>> valueNotifier;

  UserOverallLocationUtil(BaseState<StatefulWidget> state) : super(state){
    valueNotifier = ValueNotifier<List<CompanyAddressListInfoBean>>(null);
  }

  void userLocationInfoGps(String gps) {
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appComAddressList",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current": 1 ?? "",
          "gps": gps ?? "",
          "size": 1 ?? ""
        },
        callback: BaseCallback(onSuccess: (resp) {
          CompanyAddressBean bean = CompanyAddressBean.fromMap(resp);
          if(bean?.data?.page?.records!=null && (bean?.data?.page?.records?.length??0)!=0){
            valueNotifier?.value = bean?.data?.page?.records;
          }
          return bean;
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }
}

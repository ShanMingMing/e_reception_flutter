import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/relative_stu/bean/relative_stu_list_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_info_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/update_task_view_model.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SelectRelativeStuListViewModel
    extends BasePagerViewModel<StuBean, RelativeStuListResponseBean> {
  ///搜索关键词
  String keyword;
  GroupListBean stuListBean;

  SelectRelativeStuListViewModel(BaseState<StatefulWidget> state)
      : super(state);

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "groupid": stuListBean?.groupid??'',
      "groupType":stuListBean?.groupType ?? "",
      "current": page ?? 1,
      "size": 1000,
    };
    if(StringUtils.isNotEmpty(keyword)){
      map['searchName']=keyword;
    }
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appCompanyPages";
  }

  @override
  RelativeStuListResponseBean parseData(VgHttpResponse resp) {
    RelativeStuListResponseBean vo =
        RelativeStuListResponseBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  ///关联学员(需要更改)
  void saveParentUserHttp(BuildContext context, List<String> listFuids,String pfuid,{bool showDialog=true}) async{
    String fuids = VgStringUtils.getSplitStr(listFuids,symbol:",");
    if (fuids == "") {
      VgToastUtils.toast(context, "上传信息获取失败");
      return;
    }
    // FaceUserInfoBean faceUserInfo = editUserInfoDataBean?.data?.faceUserInfo;

    // VgHudUtils.show(context,"保存中");
    // StringBuffer classIds=StringBuffer();
    var params={
      "authId" : UserRepository.getInstance().authId ?? "",
      "pFuid" : pfuid ?? "",
      "fuids": fuids ??  "",

    };

    VgHttpUtils.post(ServerApi.BASE_URL + "app/appEditParentStudent",
        params: params,
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, "设置成功");
          VgEventBus.global.send(RefreshCompanyDetailPageEvent());
          VgEventBus.global.send(RefreshPersonDetailEvent());
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }
}

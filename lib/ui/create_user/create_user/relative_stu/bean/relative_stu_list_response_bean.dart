
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class RelativeStuListResponseBean extends BasePagerBean<StuBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static RelativeStuListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RelativeStuListResponseBean companyChooseTerminalResponseBeanBean = RelativeStuListResponseBean();
    companyChooseTerminalResponseBeanBean.success = map['success'];
    companyChooseTerminalResponseBeanBean.code = map['code'];
    companyChooseTerminalResponseBeanBean.msg = map['msg'];
    companyChooseTerminalResponseBeanBean.data = DataBean.fromMap(map['data']);
    companyChooseTerminalResponseBeanBean.extra = map['extra'];
    return companyChooseTerminalResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0
      ? 1
      : data.page.current;

  ///得到List列表
  @override
  List<StuBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() =>
      data?.page?.pages == null || data.page.pages <= 0 ? 1 : data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }

}
/// page : {"records":[{"hsn":"6b93696da4ea7c35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"00","manager":"李岁红","loginName":"王凡语"},{"terminalName":"台式机","terminalPicurl":"http://etpic.we17.com/test/20210122114419_4013.jpg","hsn":"5b63afb7016f1f00","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近润葳酒店","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"2021百事可乐呀","terminalPicurl":"http://etpic.we17.com/test/20210120170519_7532.jpg","hsn":"1ee7ba9ca715b8e3","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"摆放位置嘛","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝1","terminalPicurl":"http://etpic.we17.com/test/20210121101911_5050.jpg","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"3好楼2911","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210122175723_5242.jpg","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210121101755_0700.jpg","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210120213028_3276.jpg","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}],"total":8,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"hsn":"6b93696da4ea7c35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"00","manager":"李岁红","loginName":"王凡语"},{"terminalName":"台式机","terminalPicurl":"http://etpic.we17.com/test/20210122114419_4013.jpg","hsn":"5b63afb7016f1f00","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近润葳酒店","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"2021百事可乐呀","terminalPicurl":"http://etpic.we17.com/test/20210120170519_7532.jpg","hsn":"1ee7ba9ca715b8e3","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"摆放位置嘛","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝1","terminalPicurl":"http://etpic.we17.com/test/20210121101911_5050.jpg","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"3好楼2911","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210122175723_5242.jpg","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210121101755_0700.jpg","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210120213028_3276.jpg","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210120160400_9940.jpg","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}]
/// total : 8
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<StuBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
        (map['records'] as List ?? []).map((o) => StuBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

class StuBean{
  String nick;
  String number;
  String groupName;
  String uname;
  String phone;
  String roleid;
  String groupid;
  String name;
  String fuid;
  String napicurl;
  String type;
  String status;

  String fid;
  String userid;

  String putpicurl;

  String isSign;

  String pictureUrl;

  int lastPunchTime;

  String department;
  String projectname;
  String classname;
  String coursename;

  String city;

  bool isSelect;
  bool special;

  static StuBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StuBean recordsBean =
    StuBean();
    recordsBean.nick = map['nick'];
    recordsBean.number = map['number'];
    recordsBean.groupName = map['groupName'];
    recordsBean.uname = map['uname'];
    recordsBean.phone = map['phone'];
    recordsBean.roleid = showOriginEmptyStr(map['roleid']) ??
        CompanyAddUserEditInfoIdentityType.commonUser.getTypeToIdStr();
    recordsBean.groupid = map['groupid'];
    recordsBean.name = map['name'];
    recordsBean.fuid = map['fuid'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.type = map['type'];
    recordsBean.status = map['status'];
    recordsBean.lastPunchTime = map['lastPunchTime'];
    recordsBean.fid = map['fid'];
    recordsBean.userid = map['userid'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.isSign = map['isSign'];
    recordsBean.pictureUrl = map['pictureUrl'];
    recordsBean.department = map['department'];
    recordsBean.projectname = map['projectname'];
    recordsBean.classname = map['classname'];
    recordsBean.coursename = map['coursename'];
    recordsBean.city = map['city'];
    recordsBean.isSelect = map['isSelect'];
    recordsBean.special = map['special'];
    return recordsBean;
  }

  Map toJson() => {
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "uname": uname,
    "phone": phone,
    "roleid": roleid,
    "groupid": groupid,
    "name": name,
    "fuid": fuid,
    "napicurl": napicurl,
    "type": type,
    "status": status,
    "lastPunchTime": lastPunchTime,
    "fid": fid,
    "userid": userid,
    "pictureUrl": pictureUrl,
    "department": department,
    "projectname": projectname,
    "classname": classname,
    "coursename": coursename,
    "city": city,
    "isSelect": isSelect,
    "special": special,
  };


  ///`status` 用户状态00未激活 01激活',
  bool isAlive() {
    return status == "00";
  }


}
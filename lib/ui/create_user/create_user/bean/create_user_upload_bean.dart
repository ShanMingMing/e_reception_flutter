import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/course_class_bean.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class CreateUserUploadBean extends ChangeNotifier {
  String nick;

  CreateUserUploadBean() {
    //默认设为员工
    List<GroupListBean> groupList =
        UserRepository.getInstance().userData?.companyInfo?.groupList;
    if (groupList == null || groupList.isEmpty) {
      return;
    }

    //选择分类默认选择
    SharePreferenceUtil.getString(KEY_LAST_ADD_USER_TYPE +
            UserRepository.getInstance().getCacheKeySuffix())
        .then((groupid) {
          if(_groupBean==null){
            for (GroupListBean item in groupList) {
              if (groupid != null && item.groupid == groupid) {
                _groupBean = item;
                return;
              }
            }
            for (GroupListBean item in groupList) {
              //企业时添加员工
              if (UserRepository.getInstance().isCompany() && item.isStaff()) {
                _groupBean = item;
                return;
              }
              //培训时添加学员
              if (UserRepository.getInstance().isOrganization() && item.isStudent()) {
                _groupBean = item;
                return;
              }
            }
            if(groupList!=null&&groupList.length>0){
              _groupBean =groupList[0];

            }
          }
    });
  }

  ///判断非必填项是否允许并返回错误提示语
  String checkAllowConfirmForErrorMsg() {
    if (_name == null || _name.isEmpty) {
      return "名字不能为空";
    }
    if (_groupBean == null ||
        StringUtils.isEmpty(_groupBean.groupid) ||
        StringUtils.isEmpty(_groupBean.groupName)) {
      return "分类不能为空";
    }
    if (_isManagerTerminal) {
      if ((_identityId == null)) {
        return "请选择管理级别";
      }
      //超管默认全部
      // if(_identityId!=CompanyAddUserEditInfoIdentityType.superAdmin){
      //   if(UserRepository.getInstance().userData.companyInfo.isCompany()){
      //     //企业
      //     if((departmentList==null||departmentList.isEmpty)&&(terminalList==null||terminalList.isEmpty)){
      //       return '管理终端/部门至少填写一项';
      //     }
      //   }else{
      //     //机构
      //     if((departmentList==null||departmentList.isEmpty)&&(terminalList==null||terminalList.isEmpty)&&(classIdList==null||classIdList.isEmpty)){
      //       return '管理终端/部门/班级至少填写一项';
      //     }
      //   }
      // }

    }
    if (phoneRedStar() && StringUtils.isEmpty(phone)) {
      return "手机号不能为空";
    }
    if (phoneRedStar() && !VgToolUtils.isPhone(phone)) {
      return "手机号不正确";
    }

    return null;
  }

  ///是否是超级管理员和管理员
  bool isCommonAdminRole() {
    return identityType == CompanyAddUserEditInfoIdentityType.commonAdmin;
  }

  ///是否是超级管理员
  bool isSuperAdminRole() {
    return identityType == CompanyAddUserEditInfoIdentityType.superAdmin;
  }

  String getHsnSplitStr() {
    if (isSuperAdminRole()) {
      return null;
    }
    if (!isManagerTerminal) {
      return null;
    }
    if (identityType == CompanyAddUserEditInfoIdentityType.commonUser) {
      return null;
    }
    if (terminalList == null || terminalList.isEmpty) {
      return null;
    }
    String str = "";
    for (CompanyChooseTerminalListItemBean item in terminalList) {
      if (item == null || StringUtils.isEmpty(item?.hsn)) {
        continue;
      }
      if (str == null || str == "") {
        str = str + item.hsn;
      } else {
        str = str + "," + item.hsn;
      }
    }
    return str;
  }

  bool phoneRedStar() {
    return isManagerTerminal && (isSuperAdminRole() || isCommonAdminRole());
  }

  String getIdentityIdStr() {
    if (!isManagerTerminal) {
      return CompanyAddUserEditInfoIdentityType.commonUser?.getTypeToIdStr();
    }
    return identityType?.getTypeToIdStr() ?? "";
  }

  ///头像
  String _headFaceUrl;

  ///姓名
  String _name;

  //地址
  String _address;

  //企业地址caid
  String _caid;

  //企业分支cbid
  String _cbid;

  String _branchName;

  ///手机
  String _phone;

  ///头像
  String _headUrl;

  String locationGps;

  // ///分类名
  // String _classifyName;
  //
  // ///分类ID
  // String _classifyId;
  //
  // ///分类类型
  // String _classifyType;

  GroupListBean _groupBean;

  ///编号名称
  String _userNumber;

  ///部门名称
  String _departmentName;

  ///部门ID
  String _departmentId;

  ///项目名称
  String _projectName;

  ///项目ID
  String _projectId;

  ///FID
  String _fuid;

  ///城市名称
  String _cityName;

  ///城市ID
  String _cityId;

  ///是否管理显示屏
  bool _isManagerTerminal;

  ///身份名称
  String _identityName;

  ///身份ID
  CompanyAddUserEditInfoIdentityType _identityId;

  ///课程ID
  List<CourseClassListItemBean> _courseList;

  ///班级ID
  List<ClassCourseListItemBean> _classList;

  ///终端List
  List<CompanyChooseTerminalListItemBean> _terminalList;

  ///管理部门ID
  List<String> _departmentList;

  List<String> get departmentList => _departmentList;

  ///关联学员ID
  List<String> _relativeStuIds;

  List<String> get relativeStuIds => _relativeStuIds;

  set relativeStuIds(List<String> value) {
    _relativeStuIds = value;
  }

  set departmentList(List<String> value) {
    _departmentList = value;
    notifyListeners();
  }

  ///管理部门名
  String managerDepartmentName;

  ///管理班级ID
  List<String> _classIdList;

  String get headFaceUrl => _headFaceUrl;

  set headFaceUrl(String value) {
    _headFaceUrl = value;
    notifyListeners();
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  String get address => _address;

  set address(String value) {
    _address = value;
    notifyListeners();
  }

  String get caid => _caid;

  set caid(String value) {
    _caid = value;
    notifyListeners();
  }

  String get cbid => _cbid;

  set cbid(String value) {
    _cbid = value;
    notifyListeners();
  }

  String get branchName => _branchName;

  set branchName(String value) {
    _branchName = value;
    notifyListeners();
  }

  String get phone => _phone;

  set phone(String value) {
    _phone = value;
    notifyListeners();
  }

  // String get classifyName => _classifyName;
  //
  // set classifyName(String value) {
  //   _classifyName = value;
  //   notifyListeners();
  // }
  //
  // String get classifyId => _classifyId;
  //
  // set classifyId(String value) {
  //   _classifyId = value;
  //   notifyListeners();
  // }

  String get userNumber => _userNumber;

  set userNumber(String value) {
    _userNumber = value;
    notifyListeners();
  }

  String get departmentName => _departmentName;

  set departmentName(String value) {
    _departmentName = value;
    notifyListeners();
  }

  String get departmentId => _departmentId;

  set departmentId(String value) {
    _departmentId = value;
    notifyListeners();
  }

  String get projectName => _projectName;

  set projectName(String value) {
    _projectName = value;
    notifyListeners();
  }

  String get projectId => _projectId;

  set projectId(String value) {
    _projectId = value;
    notifyListeners();
  }

  String get fuid => _fuid;

  set fuid(String value) {
    _fuid = value;
    notifyListeners();
  }

  String get cityName => _cityName;

  set cityName(String value) {
    _cityName = value;
    notifyListeners();
  }

  String get cityId => _cityId;

  set cityId(String value) {
    _cityId = value;
    notifyListeners();
  }

  bool get isManagerTerminal => _isManagerTerminal;

  set isManagerTerminal(bool value) {
    _isManagerTerminal = value;
    notifyListeners();
  }

  String get identityName => _identityName;

  set identityName(String value) {
    _identityName = value;
    notifyListeners();
  }

  CompanyAddUserEditInfoIdentityType get identityType => _identityId;

  set identityType(CompanyAddUserEditInfoIdentityType value) {
    _identityId = value;
    notifyListeners();
  }

  List<CompanyChooseTerminalListItemBean> get terminalList => _terminalList;

  set terminalList(List<CompanyChooseTerminalListItemBean> value) {
    _terminalList = value;
    notifyListeners();
  }

  GroupListBean get groupBean => _groupBean;

  set groupBean(GroupListBean value) {
    _groupBean = value;
    notifyListeners();
  }

  List<CourseClassListItemBean> get courseList => _courseList;

  set courseList(List<CourseClassListItemBean> value) {
    _courseList = value;
    notifyListeners();
  }

  @override
  String toString() {
    return 'CreateUserUploadBean{_name: $_name, _phone: $_phone, _headUrl: $_headUrl, _userNumber: $_userNumber, _departmentName: $_departmentName, _departmentId: $_departmentId, _projectName: $_projectName, _projectId: $_projectId,_fuid:$_fuid, _cityName: $_cityName, _cityId: $_cityId, _isManagerTerminal: $_isManagerTerminal, _identityName: $_identityName, _identityId: $_identityId, _terminalList: $_terminalList}';
  }

  List<ClassCourseListItemBean> get classList => _classList;

  set classList(List<ClassCourseListItemBean> value) {
    _classList = value;
    notifyListeners();
  }

  String courseIds() {
    if (!_groupBean.isStudent()) {
      return null;
    }
    if (_courseList == null || _courseList.isEmpty) {
      return null;
    }
    List<String> list = List();
    for (CourseClassListItemBean item in _courseList) {
      if (item != null && item.id != null && item.id.isNotEmpty) {
        list.add(item.id);
      }
    }
    if (list == null || list.isEmpty) {
      return null;
    }
    return list.join(",");
  }

  String classIds() {
    if (!_groupBean.isStudent()) {
      return null;
    }
    if (_classList == null || _classList.isEmpty) {
      return null;
    }
    List<String> list = List();
    for (ClassCourseListItemBean item in _classList) {
      if (item != null && item.id != null && item.id.isNotEmpty) {
        list.add(item.id);
      }
    }
    if (list == null || list.isEmpty) {
      return null;
    }
    return list.join(",");
  }

  List<String> get classIdList => _classIdList;

  set classIdList(List<String> value) {
    _classIdList = value;
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : [{"name":"Ukell课程","id":"7101f9b256f74e13bbb2e59c7f4e5b86"},{"name":"书法正楷隶体微软雅黑草书颜真卿李白古诗苏轼打油诗课程","id":"21b46f50d3614cfb918425d703222344"},{"name":"素描课程","id":"416022aea46b4d4c8d3c4215d542ea5d"},{"name":"美术课呀","id":"87182cef28f94071b7c0b3bee9aef781"},{"name":"遛狗课程","id":"fbe98c36a8b74b708645b979c7257f0d"}]
/// extra : null

class ClassCourseBean extends BasePagerBean<ClassCourseListItemBean> {
  bool success;
  String code;
  String msg;
  List<ClassCourseListItemBean> data;
  dynamic extra;

  static ClassCourseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ClassCourseBean courseClassBeanBean = ClassCourseBean();
    courseClassBeanBean.success = map['success'];
    courseClassBeanBean.code = map['code'];
    courseClassBeanBean.msg = map['msg'];
    courseClassBeanBean.data = List()
      ..addAll((map['data'] as List ?? [])
          .map((o) => ClassCourseListItemBean.fromMap(o)));
    courseClassBeanBean.extra = map['extra'];
    return courseClassBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<ClassCourseListItemBean> getDataList() => data;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data = list.cast();
  }
}

/// name : "Ukell课程"
/// id : "7101f9b256f74e13bbb2e59c7f4e5b86"

class ClassCourseListItemBean {
  String name;
  String id;
  bool isSelect;
  bool isEdit;
  TextEditingController controller;


  ClassCourseListItemBean([this.name, this.id]);

  static ClassCourseListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ClassCourseListItemBean dataBean = ClassCourseListItemBean();
    dataBean.name = map['name'];
    dataBean.id = map['id'];
    dataBean.isSelect = map['isSelect'];
    dataBean.isEdit = map['isEdit'];
    return dataBean;
  }

  Map toJson() => {
    "name": name,
    "id": id,
    "isSelect": isSelect,
    "isEdit": isEdit,
  };
}

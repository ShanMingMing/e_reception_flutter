import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_edit_callback_widget/common_edit_callback_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/bean/user_select_sort_save_response_bean.dart';

// import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/course_class_bean.dart' hide ClassCourseBean;
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'course_class_bean/class_course_bean.dart';

class MultiSelectClassCourseWidget extends StatefulWidget {
  List<String> selectClassIds;

  MultiSelectClassCourseWidget(this.selectClassIds);

  @override
  _MultiSelectClassCourseWidgetState createState() =>
      _MultiSelectClassCourseWidgetState();

  ///跳转方法
  static Future<EditTextAndValue> navigatorPush(BuildContext context,
      List<String> selectClassIds) {
    return RouterUtils.routeForFutureResult<EditTextAndValue>(
      context,
      MultiSelectClassCourseWidget(selectClassIds),
    );
  }
}

class _MultiSelectClassCourseWidgetState
    extends State<MultiSelectClassCourseWidget> {
  List<ClassCourseListItemBean> mList;
  final String _classType = "03";
  CommonListPageWidgetState mState;
  List<String> selectClassIds;


  @override
  void initState() {
    selectClassIds=widget.selectClassIds??[];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: CommonPackUpKeyboardWidget(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            // color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            _toTopBarWidget(),
            Padding(
              padding: const EdgeInsets.only(left: 7, right: 7, top: 10),
              child: _searchDepartmentWidget(),
            ),
            SizedBox(
              height: 10,
            ),
            //课程list
            _courseClassListWidget(),
          ],
        ),
      ),
    );
  }

  Widget _courseClassListWidget() {
    return Expanded(
      child: CommonListPageWidget<ClassCourseListItemBean, ClassCourseBean>(
        enablePullUp: false,
        enablePullDown: true,
        queryMapFunc: (int page) =>
        {
          "authId": UserRepository
              .getInstance()
              .authId ?? "",
          "type": _classType,
        },
        parseDataFunc: (VgHttpResponse resp) {
          ClassCourseBean bean = ClassCourseBean.fromMap(resp?.data);
          bean?.data?.removeWhere((element) =>
          element == null ||
              StringUtils.isEmpty(element.id) ||
              StringUtils.isEmpty(element.name));
          return bean;
        },
        listFunc: (List<ClassCourseListItemBean> courseList) {
          mList = courseList;
          mList.forEach((element) {
            element.isSelect = false;
          });
        },
        stateFunc: (CommonListPageWidgetState state) {
          mState = state;
        },
        itemBuilder: (BuildContext context, int index,
            ClassCourseListItemBean itemBean) {
          if (itemBean == null) return Container();


          return _toListItemWidget(context, index, itemBean);
        },
        //分割器构造器
        separatorBuilder: (context, int index, _) {
          return Container(
            height: 0.5,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              color: Color(0xFF303546),
            ),
          );
          // return divider;
        },
        placeHolderFunc: (List<ClassCourseListItemBean> list, Widget child,
            VoidCallback onRefresh) {
          return VgPlaceHolderStatusWidget(
            emptyStatus: list == null || list.isEmpty,
            emptyOnClick: () => onRefresh(),
            child: child,
          );
        },
        netUrl:ServerApi.BASE_URL + "app/appChooseInfo",
        httpType: VgHttpType.post,
      ),
    );
  }


  Widget _toListItemWidget(BuildContext context, int index,
      ClassCourseListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (selectClassIds.contains(itemBean.id)) {
            selectClassIds.remove(itemBean.id);
        } else {
          selectClassIds.add(itemBean.id);
        }
        // print(selectCourseIndex.toString());
        setState(() {});
      },
      child: Container(
        height: 50,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 10,
            ),
            Image(
              image: selectClassIds.contains(itemBean.id) ?? false
                  ? AssetImage("images/common_selected_ico.png")
                  : AssetImage("images/common_unselect_ico.png"),
              width: 20,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              itemBean?.name ?? "",
              style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }


  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "班级",
      rightWidget: _buttonWidget(),
    );
  }

  Widget _buttonWidget() {
    return Container(
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: true,
        width: 48,
        height: 24,
        unSelectBgColor:
        ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
            fontWeight: FontWeight.w600),
        selectedTextStyle: TextStyle(
            color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
        text: "确定",
        onTap: () {
          StringBuffer name = StringBuffer();
          mList.forEach((element) {
            name.write(element.name + ",");
          });
          String edittext;
          if(name.length>1){
            edittext=name.toString().substring(0,name.length-1);
          }
          Navigator.pop(context, EditTextAndValue(editText:edittext,value: selectClassIds));
        },
      ),
    );
  }

  Widget _searchDepartmentWidget() {
    return Container(
      height: 30,
      color: Color(0xFF3A3F50),
      child: CommonEditCallbackWidget(
        contentPadding: EdgeInsets.only(top: 0, bottom: 15),
        hintText: "搜索",
        customIcon: "images/common_search_ico.png",
        iconColor: Colors.white54,
        onChanged: (String cityText) {
          //搜索
        },
      ),
    );
  }
}

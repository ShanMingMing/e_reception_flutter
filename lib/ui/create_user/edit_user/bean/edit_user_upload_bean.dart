import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'edit_user_info_response_bean.dart';

class EditUserUploadBean extends ChangeNotifier {

  EditUserUploadBean([this.oldNetImage]) {
    //默认设为员工
    List<GroupListBean> groupList =
        UserRepository.getInstance().userData?.companyInfo?.groupList;
    if (groupList == null || groupList.isEmpty) {
      return;
    }
    for (GroupListBean item in groupList) {
      if (item.isStaff()) {
        _groupBean = item;
      }
    }
  }

  ///判断非必填项是否允许并返回错误提示语
  String checkChangeMsg(EditUserInfoDataBean infoDataBean) {
    if (_name == null || _name.isEmpty) {
      return "名字不能为空";
    }
    if (_groupBean == null ||
        StringUtils.isEmpty(_groupBean.groupid) ||
        StringUtils.isEmpty(_groupBean.groupName)) {
      return "分类不能为空";
    }
    ///管理级别打开时，管理级别需要选择管理员以上
    if (_isManagerTerminal &&
        _identityId != CompanyAddUserEditInfoIdentityType.superAdmin &&
        _identityId != CompanyAddUserEditInfoIdentityType.commonAdmin) {
      return "请选择管理级别";
    }
    if (phoneRedStar() && StringUtils.isEmpty(phone)) {
      return "手机号不能为空";
    }
    if (phoneRedStar() && !VgToolUtils.isPhone(phone)) {
      return "手机号不正确";
    }
    if(infoDataBean?.name != _name) return null;
    if(infoDataBean?.phone != phone) return null;
    if(infoDataBean?.nick != nick) return null;
    if(infoDataBean?.department != departmentName) return null;
    if((infoDataBean?.classInfo?.map((e) => e.classname)?.toList()?.join('、')) != gradeName) return null;
    if(identityType != CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(infoDataBean?.roleid)) return null;
    if(headFaceUrl != VgStringUtils.getFirstNotEmptyStrByList([
      infoDataBean?.putpicurl,
      infoDataBean?.napicurl,
    ])) {return null;}


    // if(terminalList!=null){
    //   if(terminalList?.length != infoDataBean?.hsnInfo?.map((e) {
    //     return CompanyChooseTerminalListItemBean()
    //       ..hsn = e?.hsn
    //       ..terminalName = e?.terminalName;
    //   })?.toList()?.length){return null;}
    //   return "terminalList";
    // }
    // if(managerClassName!=null){
    //   if(managerClassName!=infoDataBean.MClassInfo.map((e) => e.classname).toList().join(','))return null;
    //   return "managerClassName";
    // }
    //
    // if(managerDepartmentName!=null){
    //   if(managerDepartmentName!=infoDataBean.MDepartmentInfo.map((e) => e.department).toList().join(',')) return null;
    //   return "managerDepartmentName";
    // }


    //当数据第一次加载返回0  有变动了返回1
    return "未改变";
  }

  ///判断非必填项是否允许并返回错误提示语
  String checkAllowConfirmForErrorMsg() {
    if (_name == null || _name.isEmpty) {
      return "名字不能为空";
    }
    if (_groupBean == null ||
        StringUtils.isEmpty(_groupBean.groupid) ||
        StringUtils.isEmpty(_groupBean.groupName)) {
      return "分类不能为空";
    }
    ///管理级别打开时，管理级别需要选择管理员以上
    if (_isManagerTerminal &&
        _identityId != CompanyAddUserEditInfoIdentityType.superAdmin &&
            _identityId != CompanyAddUserEditInfoIdentityType.commonAdmin) {
      return "请选择管理级别";
    }
    if (phoneRedStar() && StringUtils.isEmpty(phone)) {
      return "手机号不能为空";
    }
    if (phoneRedStar() && !VgToolUtils.isPhone(phone)) {
      return "手机号不正确";
    }
    return null;
  }

  String fuid;

  String fid;

  ///昵称
  String nick;

  List<StudentInfoBean> editStudentInfo;

  List<StudentInfoBean> studentInfo;

  String userid;

  ///旧头像
  final String oldNetImage;

  ///管理部门ID
  List<String> departmentList;

  ///管理部门名
  String managerDepartmentName;
  String splitDepartment(String managerDepartmentName){
    if(managerDepartmentName!=null && managerDepartmentName!=""){
      List<String>  departmentNames = List();
      if(managerDepartmentName?.contains(",")){
        departmentNames = managerDepartmentName?.split(",");
      }else{
        departmentNames = managerDepartmentName?.split("、");
      }

      if(departmentNames?.length ==1){
        return managerDepartmentName;
      }else{
        return managerDepartmentName = "${departmentNames?.length}个部门";
      }
    }
    return managerDepartmentName;
  }

  ///管理班级ID
  List<String> classIdList;

  ///管理班级名
  String managerClassName;
  String splitManagerClassName(String managerClassName){
    if(managerClassName==null || managerClassName=="")return managerClassName;
    List<String>  managerClassNames = List();
    if(managerClassName?.contains(",")){
      managerClassNames = managerClassName?.split(",");
    }else{
      managerClassNames = managerClassName?.split("、");
    }

    if(managerClassNames?.length ==1){
      return managerClassName;
    }else{
      return managerClassName = "${managerClassNames?.length}个班级";
    }
  }

  ///当前角色
  String role;

  ///创建人uid
  String createuid;


  ///是否是超级管理员和管理员
  bool isCommonAdminRole() {
    return identityType == CompanyAddUserEditInfoIdentityType.commonAdmin;
  }

  ///是否是超级管理员
  bool isSuperAdminRole() {
    return identityType == CompanyAddUserEditInfoIdentityType.superAdmin;
  }

  String getHsnSplitStr() {
    if (isSuperAdminRole()) {
      return null;
    }
    if (!isManagerTerminal) {
      return null;
    }
    if (identityType == CompanyAddUserEditInfoIdentityType.commonUser) {
      return null;
    }
    if (terminalList == null || terminalList.isEmpty) {
      return null;
    }
    String str = "";
    for (CompanyChooseTerminalListItemBean item in terminalList) {
      if (item == null || StringUtils.isEmpty(item?.hsn)) {
        continue;
      }
      if (str == null || str == "") {
        str = str + item.hsn;
      } else {
        str = str + "," + item.hsn;
      }
    }
    return str;
  }

  bool phoneRedStar() {
    return isManagerTerminal && (isSuperAdminRole() || isCommonAdminRole());
  }

  String getIdentityIdStr() {
    if (!isManagerTerminal) {
      return CompanyAddUserEditInfoIdentityType.commonUser?.getTypeToIdStr();
    }
    return identityType?.getTypeToIdStr() ?? "";
  }

  ///头像
  String _headFaceUrl;

  ///姓名
  String _name;

  ///手机
  String _phone;

  ///头像
  String _headUrl;

  // ///分类名
  // String _classifyName;
  //
  // ///分类ID
  // String _classifyId;
  //
  // ///分类类型
  // String _classifyType;

  GroupListBean _groupBean;

  ///编号名称
  String _userNumber;

  ///部门名称
  String _departmentName;

  ///部门ID
  String _departmentId;

  ///项目名称
  String _projectName;

  ///项目ID
  String _projectId;

  //地址
  String _address;

  //企业地址caid
  String _caid;

  List<CompanyAddressListInfoBean> _listAddressBean;

  // String _cbid;
  //
  String _addCbids;
  //
  String _removeCbids;

  String _branchName;

  List<CompanyBranchListInfoBean> _listBranchBean;

  ///城市名称
  String _cityName;

  ///城市ID
  String _cityId;

  ///学员身份所属班级
  List<ClassCourseListItemBean> _classList;

  ///是否管理显示屏
  bool _isManagerTerminal = false;

  ///身份名称
  String _identityName;

  ///身份ID
  CompanyAddUserEditInfoIdentityType _identityId;

  ///课程ID
  String _courseId;

  ///课程名称
  String _courseName;

  ///班级ID
  String _gradeId;

  ///班级名称
  String _gradeName;

  ///终端List
  List<CompanyChooseTerminalListItemBean> _terminalList;

  String get headFaceUrl => _headFaceUrl;

  set headFaceUrl(String value) {
    _headFaceUrl = value;
    notifyListeners();
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  String get phone => _phone;

  set phone(String value) {
    _phone = value;
    notifyListeners();
  }

  String get address => _address;

  set address(String value) {
    _address = value;
    notifyListeners();
  }

  String get caid => _caid;

  set caid(String value) {
    _caid = value;
    notifyListeners();
  }

  List<CompanyAddressListInfoBean> get listAddressBean => _listAddressBean;

  set listAddressBean(List<CompanyAddressListInfoBean> value) {
    _listAddressBean = value;
    notifyListeners();
  }
  //
  // String get cbid => _cbid;
  //
  // set cbid(String value) {
  //   _cbid = value;
  //   notifyListeners();
  // }

  String get addCbids => _addCbids;

  set addCbids(String value) {
    _addCbids = value;
    notifyListeners();
  }

  String get removeCbids => _removeCbids;

  set removeCbids(String value) {
    _removeCbids = value;
    notifyListeners();
  }

  String get branchName => _branchName;

  set branchName(String value) {
    _branchName = value;
    notifyListeners();
  }

  List<CompanyBranchListInfoBean> get listBranchBean => _listBranchBean;

  set listBranchBean(List<CompanyBranchListInfoBean> value) {
    _listBranchBean = value;
    notifyListeners();
  }

  // String get classifyName => _classifyName;
  //
  // set classifyName(String value) {
  //   _classifyName = value;
  //   notifyListeners();
  // }
  //
  // String get classifyId => _classifyId;
  //
  // set classifyId(String value) {
  //   _classifyId = value;
  //   notifyListeners();
  // }

  String get userNumber => _userNumber;

  set userNumber(String value) {
    _userNumber = value;
    notifyListeners();
  }

  String get departmentName => _departmentName;

  set departmentName(String value) {
    _departmentName = value;
    notifyListeners();
  }

  String get departmentId => _departmentId;

  set departmentId(String value) {
    _departmentId = value;
    notifyListeners();
  }

  String get projectName => _projectName;

  set projectName(String value) {
    _projectName = value;
    notifyListeners();
  }

  String get projectId => _projectId;

  set projectId(String value) {
    _projectId = value;
    notifyListeners();
  }

  String get cityName => _cityName;

  set cityName(String value) {
    _cityName = value;
    notifyListeners();
  }

  String get cityId => _cityId;

  set cityId(String value) {
    _cityId = value;
    notifyListeners();
  }

  bool get isManagerTerminal => _isManagerTerminal;

  set isManagerTerminal(bool value) {
    _isManagerTerminal = value;
    notifyListeners();
  }

  String get identityName => _identityName;

  set identityName(String value) {
    _identityName = value;
    notifyListeners();
  }

  CompanyAddUserEditInfoIdentityType get identityType => _identityId;

  set identityType(CompanyAddUserEditInfoIdentityType value) {
    _identityId = value;
    notifyListeners();
  }

  List<CompanyChooseTerminalListItemBean> get terminalList => _terminalList;

  set terminalList(List<CompanyChooseTerminalListItemBean> value) {
    _terminalList = value;
    notifyListeners();
  }

  GroupListBean get groupBean => _groupBean;

  set groupBean(GroupListBean value) {
    _groupBean = value;
    notifyListeners();
  }

  String get courseId => _courseId;

  set courseId(String value) {
    _courseId = value;
  }

  String get courseName => _courseName;

  set courseName(String value) {
    _courseName = value;
  }

  String get gradeId => _gradeId;

  set gradeId(String value) {
    _gradeId = value;
    notifyListeners();
  }

  String get gradeName => _gradeName;

  set gradeName(String value) {
    _gradeName = value;
    notifyListeners();
  }

  List<ClassCourseListItemBean> get classList => _classList;

  set classList(List<ClassCourseListItemBean> value) {
    _classList = value;
    notifyListeners();
  }

  ///是否是新图
  bool isNewPic() {
    return oldNetImage != headFaceUrl;
  }

  String classIds() {
    if (!_groupBean.isStudent()) {
      return null;
    }
    if (_classList == null || _classList.isEmpty) {
      return null;
    }
    List<String> list = List();
    for (ClassCourseListItemBean item in _classList) {
      if (item != null && item.id != null && item.id.isNotEmpty) {
        list.add(item.id);
      }
    }
    if (list == null || list.isEmpty) {
      return null;
    }
    return list.join(",");
  }

  @override
  String toString() {
    return 'CreateUserUploadBean{_name: $_name, _phone: $_phone, _headUrl: $_headUrl, _userNumber: $_userNumber, _departmentName: $_departmentName, _departmentId: $_departmentId, _projectName: $_projectName, _projectId: $_projectId, _cityName: $_cityName, _cityId: $_cityId, _isManagerTerminal: $_isManagerTerminal, _identityName: $_identityName, _identityId: $_identityId, _terminalList: $_terminalList}';
  }
}
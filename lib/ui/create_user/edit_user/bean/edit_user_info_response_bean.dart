import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"nick":"","number":"","groupName":"员工","phone":"13571059924","putpicurl":"http://etpic.we17.com/test/20210308103714_6525.jpg","roleid":"99","groupid":"fe49851d6dda41389a7156d386b0e1df","name":"新新义","napicurl":"","userid":"d8a05405e3024951840041b85cb70934","hsnInfo":[{"terminalName":"岁岁的终端","hsn":"a69ab51225adaf8b"},{"terminalName":"小米8SE","hsn":"e57477b75c986715"},{"terminalName":"北001","hsn":"15CC8E20433E81F2BF2480D51244431133"},{"terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0"}],"classInfo":[],"courseInfo":[]}
/// extra : null

class EditUserInfoResponseBean {
  bool success;
  String code;
  String msg;
  EditUserInfoDataBean data;
  List<StudentInfoBean> studentInfo;
  dynamic extra;

  static EditUserInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EditUserInfoResponseBean editUserInfoResponseBeanBean =
        EditUserInfoResponseBean();
    editUserInfoResponseBeanBean.success = map['success'];
    editUserInfoResponseBeanBean.code = map['code'];
    editUserInfoResponseBeanBean.msg = map['msg'];
    editUserInfoResponseBeanBean.data =
        EditUserInfoDataBean.fromMap(map['data']);
    editUserInfoResponseBeanBean.extra = map['extra'];
    return editUserInfoResponseBeanBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// nick : ""
/// number : ""
/// groupName : "员工"
/// phone : "13571059924"
/// putpicurl : "http://etpic.we17.com/test/20210308103714_6525.jpg"
/// roleid : "99"
/// groupid : "fe49851d6dda41389a7156d386b0e1df"
/// name : "新新义"
/// napicurl : ""
/// userid : "d8a05405e3024951840041b85cb70934"
/// hsnInfo : [{"terminalName":"岁岁的终端","hsn":"a69ab51225adaf8b"},{"terminalName":"小米8SE","hsn":"e57477b75c986715"},{"terminalName":"北001","hsn":"15CC8E20433E81F2BF2480D51244431133"},{"terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0"}]
/// classInfo : []
/// courseInfo : []

class EditUserInfoDataBean {
  String nick;
  String number;
  String groupName;
  String phone;
  String putpicurl;
  String roleid;
  String groupid;
  String name;
  String napicurl;
  String userid;
  String createuid;
  List<HsnInfoBean> hsnInfo;
  List<DepartmentInfo> MDepartmentInfo;
  List<ClassInfo> MClassInfo;
  List<ClassInfo> showClassInfo;//展示使用（为了区分权限展示）
  List<ClassInfo> classInfo;//编辑使用
  List<String> courseInfo;

  ///00员工 01访客 02家长 03学员 99普通
  String groupType;
  String department;
  String departmentid;
  String projectid;
  String projectname;
  String city;
  List<StudentInfoBean> studentInfo;
  List<StudentInfoBean> showStudentInfo;
  int monthPunchCnt;
  List<FuserBranchListBean> fuserBranchList;

  static EditUserInfoDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EditUserInfoDataBean dataBean = EditUserInfoDataBean();
    dataBean.nick = map['nick'];
    dataBean.number = map['number'];
    dataBean.groupName = map['groupName'];
    dataBean.phone = map['phone'];
    dataBean.createuid = map['createuid'];
    dataBean.putpicurl = map['putpicurl'];
    dataBean.roleid = map['roleid'];
    dataBean.groupid = map['groupid'];
    dataBean.name = map['name'];
    dataBean.napicurl = map['napicurl'];
    dataBean.userid = map['userid'];
    dataBean.monthPunchCnt = map['monthPunchCnt'];
    dataBean.studentInfo = List()..addAll(
        (map['studentInfo'] as List ?? []).map((o) => StudentInfoBean.fromMap(o))
    );
    dataBean.showStudentInfo = List()..addAll(
        (map['showStudentInfo'] as List ?? []).map((o) => StudentInfoBean.fromMap(o))
    );
    dataBean.hsnInfo = List()
      ..addAll(
          (map['hsnInfo'] as List ?? []).map((o) => HsnInfoBean.fromMap(o)));
    dataBean.classInfo = List()
      ..addAll(
          (map['classInfo'] as List ?? []).map((o) => ClassInfo.fromMap(o)));
    dataBean.showClassInfo = List()
      ..addAll(
          (map['classInfo'] as List ?? []).map((o) => ClassInfo.fromMap(o)));
    // dataBean.courseInfo = map['courseInfo'];
    dataBean.groupType = map['groupType'];
    dataBean.department = map['department'];
    dataBean.departmentid = map['departmentid'];
    dataBean.projectid = map['projectid'];
    dataBean.projectname = map['projectname'];
    dataBean.city = map['city'];
    dataBean.MDepartmentInfo = List()
      ..addAll((map['MDepartmentInfo'] as List ?? [])
          .map((o) => DepartmentInfo.fromMap(o)));
    dataBean.MClassInfo = List()
      ..addAll(
          (map['MClassInfo'] as List ?? []).map((o) => ClassInfo.fromMap(o)));
    dataBean.fuserBranchList = List()..addAll(
        (map['fuserBranchList'] as List ?? []).map((o) => FuserBranchListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
        "nick": nick,
        "number": number,
        "groupName": groupName,
        "phone": phone,
        "putpicurl": putpicurl,
        "roleid": roleid,
        "groupid": groupid,
        "name": name,
        "napicurl": napicurl,
        "userid": userid,
        "hsnInfo": hsnInfo,
        "createuid": createuid,
        "classInfo": classInfo,
    "showClassInfo": showClassInfo,
        "courseInfo": courseInfo,
        "groupType": groupType,
        "department": department,
        "departmentid": departmentid,
        "projectid": projectid,
        "projectname": projectname,
    "monthPunchCnt": monthPunchCnt,
    "studentInfo": studentInfo,
    "showStudentInfo": showStudentInfo,
      };
}

class ClassInfo {
  String classid;
  String classname;

  static ClassInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ClassInfo info = ClassInfo();
    info.classid = map['classid'];
    info.classname = map['classname'];
    return info;
  }
}

class DepartmentInfo {
  String departmentid;
  String department;

  static DepartmentInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentInfo info = DepartmentInfo();
    info.departmentid = map['departmentid'];
    info.department = map['department'];
    return info;
  }
}

/// terminalName : "岁岁的终端"
/// hsn : "a69ab51225adaf8b"

class HsnInfoBean {
  String terminalName;
  String hsn;

  static HsnInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnInfoBean hsnInfoBean = HsnInfoBean();
    hsnInfoBean.terminalName = map['terminalName'];
    hsnInfoBean.hsn = map['hsn'];
    return hsnInfoBean;
  }

  Map toJson() => {
        "terminalName": terminalName,
        "hsn": hsn,
      };
}
//
// class EditStudentInfoBean {
//   String nick;
//   String phone;
//   String putpicurl;
//   String name;
//   String fuid;
//   String napicurl;
//   String groupid;
//
//   static EditStudentInfoBean fromMap(Map<String, dynamic> map) {
//     if (map == null) return null;
//     EditStudentInfoBean studentInfoBean = EditStudentInfoBean();
//     studentInfoBean.nick = map['nick'];
//     studentInfoBean.phone = map['phone'];
//     studentInfoBean.putpicurl = map['putpicurl'];
//     studentInfoBean.name = map['name'];
//     studentInfoBean.fuid = map['fuid'];
//     studentInfoBean.napicurl = map['napicurl'];
//     studentInfoBean.groupid = map['groupid'];
//     return studentInfoBean;
//   }
//
//   Map toJson() => {
//     "nick": nick,
//     "phone": phone,
//     "putpicurl": putpicurl,
//     "name": name,
//     "fuid": fuid,
//     "napicurl": napicurl,
//     "groupid": groupid,
//   };
// }

class StudentInfoBean {
  String nick;
  String phone;
  String putpicurl;
  String name;
  String fuid;
  String napicurl;
  String groupid;

  static StudentInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StudentInfoBean studentInfoBean = StudentInfoBean();
    studentInfoBean.nick = map['nick'];
    studentInfoBean.phone = map['phone'];
    studentInfoBean.putpicurl = map['putpicurl'];
    studentInfoBean.name = map['name'];
    studentInfoBean.fuid = map['fuid'];
    studentInfoBean.napicurl = map['napicurl'];
    studentInfoBean.groupid = map['groupid'];
    return studentInfoBean;
  }

  Map toJson() => {
    "nick": nick,
    "phone": phone,
    "putpicurl": putpicurl,
    "name": name,
    "fuid": fuid,
    "napicurl": napicurl,
    "groupid": groupid,
  };
}


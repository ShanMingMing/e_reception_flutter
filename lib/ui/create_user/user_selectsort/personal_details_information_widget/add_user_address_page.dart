import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_address/company_create_or_edit_address_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_branch/company_create_or_edit_branch_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_address_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_branch_vied_model.dart';
import 'package:e_reception_flutter/utils/address_about_utils.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

class AddUserAddressPage extends StatefulWidget {
  final String fuid;
  final String fid;
  final bool createEdit;
  final String gps;
  final List<CompanyBranchListInfoBean> listBranchBean;
  const AddUserAddressPage({Key key, this.fuid, this.fid, this.createEdit, this.listBranchBean, this.gps}) : super(key: key);
  @override
  _AddUserAddressPageState createState() => _AddUserAddressPageState();

  static Future<CompanyBranchListInfoBean> navigatorPush(
      BuildContext context,List<CompanyBranchListInfoBean> listBranchBean,String gps,
      {String fuid, String fid, bool createEdit}) {
    return RouterUtils.routeForFutureResult<CompanyBranchListInfoBean>(
      context,
      AddUserAddressPage(
          fuid:fuid,
          fid:fid,
          createEdit:createEdit,
          listBranchBean:listBranchBean,
          gps:gps
      ),
    );
  }
}

class _AddUserAddressPageState extends BasePagerState<CompanyBranchListInfoBean,AddUserAddressPage> {
  CreateUserBranchViewModel viewModel;
  @override
  void initState() {
    super.initState();
    viewModel = CreateUserBranchViewModel(this,widget?.gps);
    viewModel.refresh();
  }
  @override
  Widget build(BuildContext context) {
    return CustomAnnotatedRegion(
      Scaffold(
        backgroundColor: Color(0xFF191E31),
        body: CommonPackUpKeyboardWidget(
          child: Stack(
            children: [
              Column(
                // mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  _toTopBarWidget(),
                  _toScreenWidget(),
                  Expanded(
                    child: _toPullRefreshWidget(),
                  )
                ],
              ),
              _toAddAddressButtonWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget _toPullRefreshWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: VgPullRefreshWidget.bind(
        viewModel: viewModel,
        state: this,
        child: ListView.separated(
            itemCount: data?.length ?? 0,
            padding: const EdgeInsets.only(bottom: 35),
            physics: BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return _toListItemWidget(context,index,data[index]);
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(height: 0.5);
            }),
      ),
    );
  }


  Widget _toScreenWidget() {
    return Container(
      height: 40,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 15, top: 12, bottom: 11),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Image.asset(
            "images/shai_xuan1.png",
            height: 16,
          ),
          SizedBox(width: 4),
          Text(
            "筛选",
            style: TextStyle(
                color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                fontSize: 12
            ),
          )
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "选择地址",
      // rightWidget: _buttonWidget(),
    );
  }

  Widget _toListItemWidget(
      BuildContext context, int index, CompanyBranchListInfoBean itemBean) {
    if((widget?.listBranchBean?.length??0)!=0){
      widget?.listBranchBean?.forEach((element) {
        if(element?.cbid?.contains(itemBean?.cbid)){
          itemBean?.selected = true;
        }
      });
    }
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if(!(itemBean?.selected??false)){
          if(!(widget?.createEdit??false)){
            if(widget?.fuid==null) return;
            viewModel.setUserBranch(widget?.fuid, widget?.fid, itemBean?.cbid, "");
            viewModel?.refresh();
          }
          RouterUtils.pop(context,result: itemBean);
        }else{
          VgToastUtils.toast(context, "已添加此地址");
        }

        setState(() {});
      },
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 12),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "${itemBean?.branchname}",
                style: TextStyle(
                  fontSize: 14,
                  color: itemBean?.selected ??false
                      ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                      : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                ),
              ),
            ),
            SizedBox(height: 2),
            Row(
              children: [
                Expanded(
                  child: Text(
                    AddressAboutUtils.getBranchSplicing(itemBean) ?? "",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 12,
                      color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      // child: Container(
      //   color: Color(0xFF21263C),
      //   height: 50,
      //   width: VgToolUtils.getScreenWidth()*3/5,
      //   alignment: Alignment.centerLeft,
      //   padding: const EdgeInsets.symmetric(horizontal: 15),
      //   child: Row(
      //     children: <Widget>[
      //       Expanded(
      //         child: Text(
      //           getBranchSplicing(itemBean) ?? "",
      //           maxLines: 1,
      //           overflow: TextOverflow.ellipsis,
      //           style: TextStyle(
      //             color: itemBean?.selected ??false ? Color(0xFF1890FF) :  ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
      //             fontSize: 14,
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }

  Widget _toAddAddressButtonWidget() {
    return Visibility(
      visible: AppOverallDistinguish.comefromAiOrWeStudy(),
      child: Positioned(
        right: 0,
        left: 0,
        bottom: ScreenUtils.getBottomBarH(context) + 50,
        child: Center(
          child: CommonFixedHeightConfirmButtonWidget(
            isAlive: true,
            width: 132,
            height: 40,
            margin: const EdgeInsets.symmetric(horizontal: 27),
            unSelectBgColor: Color(0xFF3A3F50),
            selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 15,
            ),
            selectedTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 15,
              // fontWeight: FontWeight.w600
            ),
            text: "添加地址",
            textLeftSelectedWidget: Padding(
              padding: const EdgeInsets.only(bottom: 1),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            onTap: (){
              CompanyCreateOrEditBranchPage.navigatorPush(context,"新增",null,widget?.gps,
                  UserRepository?.getInstance()?.userData?.companyInfo?.companynick,(){

                  }).then((value){
                setState(() {
                  viewModel.refresh();
                });
              });
            },
          ),
        ),
      ),
    );
  }
}

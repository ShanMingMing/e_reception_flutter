import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_edit_callback_widget/common_edit_callback_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/bean/project_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/bean/user_select_sort_save_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';

import '../update_task_view_model.dart';

class PersonalProjectWidget extends StatefulWidget {
  final String projectType;
  final FaceUserInfoBean infoBean;

  const PersonalProjectWidget(
      {Key key, this.projectType = "01", this.infoBean})
      : super(key: key);

  @override
  _PersonalProjectWidgetState createState() =>
      _PersonalProjectWidgetState();

  ///跳转方法
  static Future<EditTextAndValue> navigatorPush(
      BuildContext context, FaceUserInfoBean infoBean) {
    return RouterUtils.routeForFutureResult<EditTextAndValue>(
      context,
      PersonalProjectWidget(
        infoBean: infoBean,
      ),
    );
  }
}

class _PersonalProjectWidgetState
    extends BaseState<PersonalProjectWidget> {
  bool isLightUp = false;
  String mProjectText;

  String selectedId;
  
  String roleId = UserRepository.getInstance().userData.comUser.roleid;

  CommonListPageWidgetState<ProjectListItemBean, ProjectResponseBean>
  _mState;

  void setCommonListState(
      CommonListPageWidgetState<ProjectListItemBean, ProjectResponseBean>
      state) {
    _mState = state;
  }

  UpdateTaskViewModel viewModel;


  List<ProjectListItemBean> listProjectItem;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.infoBean?.projectid!=null && widget.infoBean?.projectid!=""){
      selectedId = widget.infoBean?.projectid;
    }
    viewModel = UpdateTaskViewModel(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      // resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xFF191E31),
      body: CommonPackUpKeyboardWidget(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _toTopBarWidget(),
            if (roleId == "99")
              _searchProjectrWidget(),
            //下划线
            if (roleId == "99")
              Container(
                height: 1,
                padding: const EdgeInsets.only(
                  left: 21,
                ),
                child: Divider(
                  height: 1.0,
                  indent: 0.0,
                  color: Color(0xFF303546),
                ),
              ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: _toCustomListWidget(),
            ),
          ],
        ),
      ),
    );
  }

  //封装的列表
  Widget _toCustomListWidget() {
    return CommonListPageWidget<ProjectListItemBean, ProjectResponseBean>(
      enablePullUp: false,
      enablePullDown: false,
      queryMapFunc: (int page) => {
        "authId": UserRepository.getInstance().authId ?? "",
        "name": "",
        "type": widget.projectType,
      },
      parseDataFunc: (VgHttpResponse resp) {
        ProjectResponseBean bean =
        ProjectResponseBean.fromMap(resp?.data);
        if(selectedId!=""){
          bean?.data?.forEach((element) {
            if(element?.id == selectedId){
              listProjectItem?.add(element);
            }
          });
          bean?.data?.forEach((element) {
            if(element?.id != selectedId){
              listProjectItem?.add(element);
            }
          });
        }
        // bean?.data = listProjectItem;
        return bean;
      },
      itemBuilder:
          (BuildContext context, int index, ProjectListItemBean itemBean) {
        return _toListItemWidget(context, index, itemBean);
      },
      //分割器构造器
      separatorBuilder: (context, int index, _) {
        return Container(
          height: 0.5,
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          padding: const EdgeInsets.only(left: 15),
          child: Container(
            color: Color(0xFF303546),
          ),
        );
        // return divider;
      },
      netUrl:ServerApi.BASE_URL + "app/appChooseInfo",
      httpType: VgHttpType.post,
      stateFunc: (CommonListPageWidgetState<ProjectListItemBean, ProjectResponseBean>
      state) {
        _mState = state;
      },
    );
  }

  Widget _toListItemWidget(
      BuildContext context, int index, ProjectListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        //判断字体是否选中
        selectedId = itemBean.id;
        setState(() {});
        //收键盘方法
        // FocusScope.of(context).requestFocus(FocusNode());
        _saveAddProjectName();
      },
      child: Container(
        color: Color(0xFF21263C),
        height: 50,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Text(
          itemBean?.name ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: itemBean.id != selectedId
                ? Color(0xFFD0E0F7)
                : Color(0xFF1890FF),
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  Widget _searchProjectrWidget() {
    return Container(
      height: 50,
      color: Color(0xFF3A3F50),
      child: CommonEditCallbackWidget(
        hintText: "请输入项目名称",
        onChanged: (String projectText) {
          mProjectText = projectText;
          if (projectText == null ||
              projectText == "" ||
              projectText.trim().isEmpty)
            isLightUp = false;
          else
            isLightUp = true;
          setState(() {});
        },
        onSubmitted: (String projectText) {
          mProjectText = projectText;
          if (projectText == null ||
              projectText == "" ||
              projectText.trim().isEmpty)
            isLightUp = false;
          else
            isLightUp = true;
          setState(() {});
        },
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "项目",
      rightWidget: _buttonWidget(),
    );
  }

  Widget _buttonWidget() {
    if (roleId == "99")
      return Container(
        child: CommonFixedHeightConfirmButtonWidget(
          isAlive: isLightUp,
          width: 48,
          height: 24,
          unSelectBgColor:
          ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
              fontWeight: FontWeight.w600),
          selectedTextStyle: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          text: "确定",
          onTap: () {
            //提交按钮
            if (isLightUp) {
              _httpSave();
            }
          },
        ),
      );
  }

  void _httpSave() {
    if (mProjectText == null || mProjectText.isEmpty) {
      VgToastUtils.toast(context, "请输入项目名称");
      return;
    }
    List<ProjectListItemBean> projectList = _mState?.data;
    if (projectList == null || projectList.isEmpty) {
      _saveProjectNameHttp();
      return;
    }
    for (ProjectListItemBean item in projectList) {
      if (item?.name == mProjectText) {
        _pop(item);
        return;
      }
    }
    _saveProjectNameHttp();
  }

  ///返回部门选中值
  void _pop(ProjectListItemBean itemBean) {
    if (itemBean == null) {
      return;
    }
    RouterUtils.pop(context,
        result: EditTextAndValue(value: itemBean.id, editText: itemBean.name));
  }

  ///保存接口
  void _saveProjectNameHttp() {
    if (mProjectText == null || mProjectText.isEmpty) {
      VgToastUtils.toast(context, "项目名称不能为空");
      return;
    }
    VgHudUtils.show(context, "保存中");
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appAddChooseInfo",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "name": mProjectText?.trim() ?? "",
          "type": widget.projectType,
        },
        callback: BaseCallback(onSuccess: (dynamic val) {
          VgHudUtils.hide(context);
          if (val == null) {
            return;
          }
          UserSelectSortSaveResponseBean successBean =
          UserSelectSortSaveResponseBean.fromMap(val);
          if (successBean == null ||
              successBean.data == null ||
              successBean.data.isEmpty) {
            VgToastUtils.toast(context, "项目添加返回值错误");
            return;
          }
          ProjectListItemBean projectListItemBean =
          new ProjectListItemBean();

          successBean.data.forEach((element) {
            if (element?.name != null &&
                element?.name != "" &&
                element?.name == mProjectText) {
              projectListItemBean.id = element.id;
            }
          });
          projectListItemBean.name = mProjectText;
          setState(() { });
          // _pop(projectListItemBean);
        }, onError: (String msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }


  void _saveAddProjectName(){
    VgToolUtils.removeAllFocus(context);
    viewModel?.batchAppUpdateChooseInfo(
        context, widget?.infoBean?.fuid, "", "", "", "", selectedId);
    VgToastUtils.toast(context, "设置成功");
  }
}

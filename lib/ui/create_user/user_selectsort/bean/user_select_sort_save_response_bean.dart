/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : [{"name":"-----","id":"1dc3264901624163954932148352703c"}]
/// extra : null

class UserSelectSortSaveResponseBean {
  bool success;
  String code;
  String msg;
  List<DataBean> data;
  dynamic extra;

  static UserSelectSortSaveResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UserSelectSortSaveResponseBean userSelectSortSaveResponseBeanBean = UserSelectSortSaveResponseBean();
    userSelectSortSaveResponseBeanBean.success = map['success'];
    userSelectSortSaveResponseBeanBean.code = map['code'];
    userSelectSortSaveResponseBeanBean.msg = map['msg'];
    userSelectSortSaveResponseBeanBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => DataBean.fromMap(o))
    );
    userSelectSortSaveResponseBeanBean.extra = map['extra'];
    return userSelectSortSaveResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// name : "-----"
/// id : "1dc3264901624163954932148352703c"

class DataBean {
  String name;
  String id;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.name = map['name'];
    dataBean.id = map['id'];
    return dataBean;
  }

  Map toJson() => {
    "name": name,
    "id": id,
  };
}
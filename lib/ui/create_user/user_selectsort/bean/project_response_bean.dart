import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : [{"name":"iPhone Watch 项目","id":"2aa8adef4bf74a19a0863e11c5934dc8"},{"name":"三星手表项目","id":"0bee61e77f484fa2956bc6e1f3d08b56"},{"name":"华为项目","id":"64972d9406184c12a6b89f5c26077ccc"},{"name":"小米项目","id":"256405892a984f898837c7d0d6327efa"},{"name":"苹果NFC项目","id":"ff7a7dd331b745a19c0ca74aebfcd872"},{"name":"苹果项目","id":"87b5d7c8e97c4e6dba0fa66ac1757391"}]
/// extra : null

class ProjectResponseBean extends BasePagerBean<ProjectListItemBean>{
  bool success;
  String code;
  String msg;
  List<ProjectListItemBean> data;
  dynamic extra;

  static ProjectResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProjectResponseBean projectResponseBeanBean = ProjectResponseBean();
    projectResponseBeanBean.success = map['success'];
    projectResponseBeanBean.code = map['code'];
    projectResponseBeanBean.msg = map['msg'];
    projectResponseBeanBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => ProjectListItemBean.fromMap(o))
    );
    projectResponseBeanBean.extra = map['extra'];
    return projectResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<ProjectListItemBean> getDataList() => data;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data = list.cast();
  }
}

/// name : "iPhone Watch 项目"
/// id : "2aa8adef4bf74a19a0863e11c5934dc8"

class ProjectListItemBean {
  String name;
  String id;

  static ProjectListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProjectListItemBean dataBean = ProjectListItemBean();
    dataBean.name = map['name'];
    dataBean.id = map['id'];
    return dataBean;
  }

  Map toJson() => {
    "name": name,
    "id": id,
  };
}
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : [{"name":"上海","id":""},{"name":"北京","id":""},{"name":"南京","id":""}]
/// extra : null

class CityResponseBean extends BasePagerBean<CityListItemBean>{
  bool success;
  String code;
  String msg;
  List<CityListItemBean> data;
  dynamic extra;

  static CityResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CityResponseBean cityResponseBeanBean = CityResponseBean();
    cityResponseBeanBean.success = map['success'];
    cityResponseBeanBean.code = map['code'];
    cityResponseBeanBean.msg = map['msg'];
    cityResponseBeanBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => CityListItemBean.fromMap(o))
    );
    cityResponseBeanBean.extra = map['extra'];
    return cityResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<CityListItemBean> getDataList() => data;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data = list.cast();
  }
}

/// name : "上海"
/// id : ""

class CityListItemBean {
  String name;
  String id;

  static CityListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CityListItemBean dataBean = CityListItemBean();
    dataBean.name = map['name'];
    dataBean.id = map['name'];
    return dataBean;
  }

  Map toJson() => {
    "name": name,
    "id": id,
  };
}
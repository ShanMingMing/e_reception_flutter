import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : [{"name":"上海开发1部","id":"01b7b9c523c346febab057b4d4180f11"},{"name":"伦敦开发1部","id":"6646b95f07894dfda18c1e1ae303aa22"},{"name":"北京开发1部","id":"505cbfbb157b469b9385da840498dbb8"},{"name":"南京开发1部","id":"db89d4ec87d541f7918364f8dd1b6b7e"},{"name":"广州开发1部","id":"d5a2261851f34dd984882e6633e1d45a"},{"name":"深圳开发1部","id":"349a2a2012a745599fbe6667b371adcb"},{"name":"硅谷开发1部","id":"ddf3ef46ac4f4408b8098c7546599fce"},{"name":"苏州开发1部","id":"2a09eb86e41e402b8c258184e993ed6e"},{"name":"西安开发1部","id":"de4ded34f9304b30bf68cf9a60e44ece"}]
/// extra : null

class DepartmentResponseBean extends BasePagerBean<DepartmentListItemBean>{
  bool success;
  String code;
  String msg;
  List<DepartmentListItemBean> data;
  dynamic extra;

  static DepartmentResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentResponseBean departmentResponseBeanBean = DepartmentResponseBean();
    departmentResponseBeanBean.success = map['success'];
    departmentResponseBeanBean.code = map['code'];
    departmentResponseBeanBean.msg = map['msg'];
    departmentResponseBeanBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => DepartmentListItemBean.fromMap(o))
    );
    departmentResponseBeanBean.extra = map['extra'];
    return departmentResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<DepartmentListItemBean> getDataList() => data;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data = list.cast();
  }
}

/// name : "上海开发1部"
/// id : "01b7b9c523c346febab057b4d4180f11"

class DepartmentListItemBean {
  String name;
  String id;
  bool selected;

  static DepartmentListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentListItemBean dataBean = DepartmentListItemBean();
    dataBean.name = map['name'];
    dataBean.id = map['id'];
    dataBean.selected = map['selected'];
    return dataBean;
  }

  Map toJson() => {
    "name": name,
    "id": id,
    "selected": selected,
  };
}
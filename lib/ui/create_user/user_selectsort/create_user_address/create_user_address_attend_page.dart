import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/company_address/company_create_or_edit_address_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_overall_location/user_overall_location.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_address_view_model.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/personal_details_information_widget/add_user_address_page.dart';
import 'package:e_reception_flutter/utils/address_about_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'create_user_branch_vied_model.dart';

class AddUserAddressAttendPage extends StatefulWidget {
  final String fuid;
  final String fid;
  final bool createEdit;
  // final String gps;
  final List<CompanyBranchListInfoBean> listBranchBean;

  const AddUserAddressAttendPage({Key key,this.fuid, this.fid, this.createEdit, this.listBranchBean}) : super(key: key);
  @override
  _AddUserAddressAttendPageState createState() => _AddUserAddressAttendPageState();

  static Future<List<CompanyBranchListInfoBean>> navigatorPush(
      BuildContext context,List<CompanyBranchListInfoBean> listBranchBean,
      String fuid, String fid,
      {bool createEdit}) {
    return RouterUtils.routeForFutureResult<List<CompanyBranchListInfoBean>>(
      context,
      AddUserAddressAttendPage(
          fuid:fuid,
          fid:fid,
          createEdit:createEdit,
          listBranchBean:listBranchBean
      ),
    );
  }
}

class _AddUserAddressAttendPageState extends BasePagerState<CompanyBranchListInfoBean,AddUserAddressAttendPage> {
  CreateUserBranchViewModel viewModel;
  List<CompanyBranchListInfoBean> showList = [];
  String locationGps;
  // UserOverallLocationUtil locationUtil;
  @override
  void initState() {
    super.initState();
    // locationUtil = UserOverallLocationUtil(this);
    viewModel = CreateUserBranchViewModel(this,"");
    // viewModel.refresh();
    showList = widget?.listBranchBean;

    VgLocation().requestSingleLocation((value) {
      Future<String> latlng = VgLocationUtils.getCacheLatLngString();
      latlng.then((value){
        print("刷脸地址最新gps:" + value);
        // _addressFristData(value);
        setState(() {locationGps = value;});
      });
    });
  }

  // void _addressFristData(String gps){
  //   if((lsitBeanData?.length??0)<1){
  //     locationUtil?.valueNotifier?.addListener(() {
  //       setState(() {
  //         lsitBeanData = locationUtil?.valueNotifier?.value;
  //       });
  //
  //     });
  //     locationUtil.userLocationInfoGps(gps);
  //
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        RouterUtils.pop(context,result: showList);
        return false;
      },
      child: CustomAnnotatedRegion(
        Scaffold(
          backgroundColor: Color(0xFF191E31),
          body: CommonPackUpKeyboardWidget(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _toTopBarWidget(),
                if((showList?.length??0)!=0)
                Expanded(
                  child: _toPullRefreshWidget(),
                ),
                // _addAddress()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _toPullRefreshWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: ListView.separated(
            itemCount: showList?.length ?? 0,
            padding: const EdgeInsets.all(0),
            physics: BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
                return _toListItemWidget(context,index,showList[index]);
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(height: 0.1);
            }),
    );
  }


  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "刷脸地址",
        navFunction:(){
          RouterUtils.pop(context,result: showList);
        },
      rightWidget: GestureDetector(
        onTap: (){
          AddUserAddressPage.navigatorPush(context,showList,locationGps,
              fuid: widget?.fuid, fid: widget?.fid, createEdit: widget?.createEdit)
              .then((value) {
            if(value==null){
              return;
            }
            setState(() {
              if(widget?.fuid!=""&&(widget?.createEdit??false)){
                value?.editSelected = true;
              }
              showList?.add(value);
            });
          });
          setState(() { });
        },
          child: Text(
        "+新增",
        style: TextStyle(
            fontSize: 14,
            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
      )),
    );
  }

  Widget _toListItemWidget(
      BuildContext context, int index, CompanyBranchListInfoBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      // color: Color(0xFF21263C),
      // height: 50,
      // width: VgToolUtils.getScreenWidth()*4/5,
      alignment: Alignment.centerLeft,
      // padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15,vertical: 12),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "${itemBean?.branchname}",
                      style: TextStyle(
                        fontSize: 14,
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      ),
                    ),
                  ),
                  SizedBox(height: 2),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      AddressAboutUtils.getBranchSplicing(itemBean) ?? "",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 12,
                        color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Spacer(),
          GestureDetector(
            onTap: () async {
              itemBean?.selected = false;
              if(!(widget?.createEdit??false)){
                viewModel.setUserBranch(widget?.fuid, widget?.fid, "", itemBean?.cbid);
              }
              showList?.remove(itemBean);
              setState(() { });
            },
            child: Container(
              height: 50,
              padding: EdgeInsets.only(right: 15, left: 100),
              child: Visibility(
                visible:(showList?.length??0)!=1 || (showList?.length??0) >1,
                child: Image.asset(
                  "images/icon_close_volume_dialog.png",
                  width: 11,
                  height: 11,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

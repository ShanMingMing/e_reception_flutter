

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../../app_main.dart';

///地址管理
class CreateUserAddressViewModel
    extends BasePagerViewModel<CompanyAddressListInfoBean,CompanyAddressBean> {

  final String gps;

  static const String SET_USER_ADDRESS_API =ServerApi.BASE_URL + "app/appEditFuserAddress";

  CreateUserAddressViewModel(BaseState<StatefulWidget> state, this.gps) : super(state);


  ///设置刷脸地址
  void setUserAddress(String fuid,String addCaids,String removeCaids) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(AppMain.context, "个人信息获取失败");
      return;
    }
    VgHttpUtils.post(SET_USER_ADDRESS_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
          "addCaids":addCaids??"",
          "removeCaids":removeCaids??"",
        },
        callback: BaseCallback(onSuccess: (val) {
          VgEventBus.global.send(RefreshPersonDetailEvent());
        }, onError: (msg) {
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "gps":gps??"",
      "current":1,
      "size":20
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.POST;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appComAddressList";
  }

  @override
  CompanyAddressBean parseData(VgHttpResponse resp) {
    CompanyAddressBean vo =
    CompanyAddressBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);

}

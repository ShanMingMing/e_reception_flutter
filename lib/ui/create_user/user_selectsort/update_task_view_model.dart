
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list_view.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';

class UpdateTaskViewModel extends BaseViewModel {
  UpdateTaskViewModel(BaseState<StatefulWidget> state) : super(state);


//  批量更改部门/城市/项目/班级信息
  void batchAppUpdateChooseInfo(
      BuildContext context,
      String fuids,
      String city,
      String classid,
      String courseid,
      String departmentid,
      String projectid,{bool pop=true,String pageNames=""}
      ) {
    if (StringUtils.isEmpty(fuids)) {
      VgToastUtils.toast(context, "个人信息获取失败");
      return;
    }

    VgHttpUtils.post(ServerApi.BASE_URL + "app/appUpdateChooseInfo",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuids": fuids ?? "",
          "city": city ?? "",
          "classid": classid ?? "",
          "courseid": courseid ?? "",
          "departmentid": departmentid ?? "",
          "projectid": projectid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          loading(false);
          if(pageNames == "PersonDetailPage"){
            VgEventBus.global.send(UpdateChooseInfoRefreshEvent());
            VgEventBus.global.send(new RefreshPersonDetailEvent());
            VgEventBus.global.send(new PersonDetailGroupRefreshEvent());
          }
          if(pageNames == "UnallocatedDepartmentStaffWidget"){
            VgEventBus.global.send(UpdateUnallocatedInfoRefreshEvent());
          }
          if(pageNames == "SpecialBatchOperationDialog"){//刷新企业主页列表
            VgEventBus.global.send(MultiDelegeRefreshEvent());
          }
          // if(departmentid!="" || projectid!="" ||city!=""){
          //   RouterUtils.pop(context);
          // }
          VgToastUtils.toast(context, "设置成功");
        }, onError: (msg) {
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  //  取消部门/城市/项目/班级信息
  void AppUpdateChooseInfo(
      BuildContext context,
      String fuid,
      String type
      ) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "个人信息获取失败");
      return;
    }
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appCancelCDP",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
          "type": type ?? "",// 00部门  01项目  04城市

        },
        callback: BaseCallback(onSuccess: (val) {
          VgEventBus.global.send(UpdateChooseInfoRefreshEvent());
          loading(false);
            RouterUtils.pop(context,
                result: EditTextAndValue(value: "", editText: ""));
            VgToastUtils.toast(context, "取消成功");
        }, onError: (msg) {
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

}

class UpdateChooseInfoRefreshEvent {}

class PersonDetailGroupRefreshEvent{}

class UpdateUnallocatedInfoRefreshEvent {}

class SpecialBatchUpdateInfoRefreshEvent {}

import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

///部门选择列表
class ClassSingleSelectWidget extends StatefulWidget {
  final String selectedId;
  final String fuid;
  final bool selectOnly;

  const ClassSingleSelectWidget(
      {Key key, this.selectedId, this.fuid, this.selectOnly})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ClassSingleSelectState();
  }
}

class ClassSingleSelectState
    extends BaseState<ClassSingleSelectWidget> {
  String selectedId;

  @override
  Widget build(BuildContext context) {
    return _toCustomListWidget();
  }

  @override
  void initState() {
    super.initState();
    selectedId = widget.selectedId;
  }

  //封装的列表
  Widget _toCustomListWidget() {
    return CommonListPageWidget<ClassCourseListItemBean, ClassCourseBean>(
      enablePullUp: false,
      enablePullDown: false,
      queryMapFunc: (int page) => {
        "authId": UserRepository.getInstance().authId ?? "",
        "name": "",
        "type": "03",
      },
      parseDataFunc: (VgHttpResponse resp) {
        ClassCourseBean bean = ClassCourseBean.fromMap(resp?.data);
        bean?.data?.removeWhere((element) =>
        element == null ||
            StringUtils.isEmpty(element.id) ||
            StringUtils.isEmpty(element.name));
        return bean;
      },
      itemBuilder:
          (BuildContext context, int index, ClassCourseListItemBean itemBean) {
        return _toListItemWidget(context, index, itemBean);
      },
      //分割器构造器
      separatorBuilder: (context, int index, _) {
        return Container(
          height: 0.5,
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          padding: const EdgeInsets.only(left: 15),
          child: Container(
            color: Color(0xFF303546),
          ),
        );
        // return divider;
      },
      netUrl:ServerApi.BASE_URL + "app/appChooseInfo",
      cacheKey:ServerApi.BASE_URL + "app/appChooseInfo"+UserRepository.getInstance().getCacheKeySuffix()+'Class',
      needCache: true,
      httpType: VgHttpType.post,
    );
  }

  Widget _toListItemWidget(
      BuildContext context, int index, ClassCourseListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        //判断字体是否选中
        // ClassText =
        selectedId = itemBean.id;
        setState(() {});
        //收键盘方法
        // FocusScope.of(context).requestFocus(FocusNode());
        VgToolUtils.removeAllFocus(context);
        Future.delayed(Duration(milliseconds: 10), () {
          _pop(itemBean);
        });
      },
      child: Container(
        color: Color(0xFF21263C),
        height: 50,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                itemBean?.name ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: itemBean.id != selectedId
                      ? Color(0xFFD0E0F7)
                      : Color(0xFF1890FF),
                  fontSize: 14,
                ),
              ),
            ),
            GestureDetector(
              onTap: () async {
                VgToolUtils.removeAllFocus(context);
                  RouterUtils.pop(context,
                      result: EditTextAndValue(editText: '', value: null));

              },
              child: Visibility(
                visible: itemBean.id == selectedId,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text(
                    "取消选择",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Color(0xFF5E687C),
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///返回部门选中值
  void _pop(ClassCourseListItemBean itemBean) {
    if (itemBean == null) {
      return;
    }
    RouterUtils.pop(context,
        result: EditTextAndValue(value: itemBean.id, editText: itemBean.name));
  }
}
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'common/bean/diy_center_info_response_bean.dart';
import 'common/common_view_model.dart';


/// 更多设置页面
class BusinessApplicationInfoPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "BusinessApplicationInfoPage";
  final ComIndustryApplicationBean businessData;
  const BusinessApplicationInfoPage({Key key, this.businessData,}) : super(key: key);

  @override
  _BusinessApplicationInfoPageState createState() => _BusinessApplicationInfoPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, ComIndustryApplicationBean businessData) {
    return RouterUtils.routeForFutureResult(
      context,
      BusinessApplicationInfoPage(businessData: businessData,),
      routeName: BusinessApplicationInfoPage.ROUTER,
    );
  }
}

class _BusinessApplicationInfoPageState extends BaseState<BusinessApplicationInfoPage> {

  CommonViewModel _viewModel;
  ComIndustryApplicationBean _businessData;
  @override
  void initState() {
    super.initState();
    _viewModel = CommonViewModel(this);
    _businessData = widget?.businessData;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _viewModel.diyCenterInfoValueNotifier.addListener(() {
        setState(() {
          _businessData = _viewModel?.diyCenterInfoValueNotifier?.value?.comIndustryApplication;
        });
      });
      _viewModel.getBusinessApplicationInfo();
    });

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body:Column(
        children: [
          _toTopBarWidget(),
          _toOperationsWidget("楼层指示牌", _businessData.isOpenBuildingIndex(), _businessData),
          _toOperationsWidget("智能家居", _businessData.isOpenSmartHome(), _businessData),
          _toOperationsWidget("商品展示", _businessData.isOpenGoodsShow(), _businessData),
          _toOperationsWidget("课程介绍", _businessData.isOpenCourseIntro(), _businessData),
        ],
      ),
    );
  }


  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "行业应用",
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  ///楼层指示牌
  Widget _toOperationsWidget(String title, bool isOpen, ComIndustryApplicationBean businessData){
    return Container(
      height: 50,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 15),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextColor_D0E0F7(),
                fontSize: 14
            ),
          ),
          Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: ()async{
              String type = "waterBrand";
              String otype1;
              String otype2;
              String otype3;
              String oflag1;
              String oflag2;
              String oflag3;
              if("楼层指示牌" == title){
                type = "waterBrand";

                otype1 = "smartHome";
                otype2 = "goodsShow";
                otype3 = "courseRecommend";

                oflag1 = businessData?.smartHome??"00";
                oflag2 = businessData?.goodsShow??"00";
                oflag3 = businessData?.courseRecommend??"00";
              }else if("智能家居" == title){
                type = "smartHome";

                otype1 = "waterBrand";
                otype2 = "goodsShow";
                otype3 = "courseRecommend";

                oflag1 = businessData?.waterBrand??"00";
                oflag2 = businessData?.goodsShow??"00";
                oflag3 = businessData?.courseRecommend??"00";
              }else if("商品展示" == title){
                type = "goodsShow";

                otype1 = "waterBrand";
                otype2 = "smartHome";
                otype3 = "courseRecommend";

                oflag1 = businessData?.waterBrand??"00";
                oflag2 = businessData?.smartHome??"00";
                oflag3 = businessData?.courseRecommend??"00";
              }else if("课程介绍" == title){
                type = "courseRecommend";

                otype1 = "waterBrand";
                otype2 = "smartHome";
                otype3 = "goodsShow";

                oflag1 = businessData?.waterBrand??"00";
                oflag2 = businessData?.smartHome??"00";
                oflag3 = businessData?.goodsShow??"00";
              }

              if(isOpen){
                bool result =
                await CommonConfirmCancelDialog.navigatorPushDialog(context,
                    title: "提示",
                    content: "确定关闭${title??""}应用？",
                    cancelText: "取消",
                    confirmText: "关闭",
                    confirmBgColor: ThemeRepository.getInstance()
                        .getMinorRedColor_F95355());
                if (result ?? false) {
                  _viewModel.editBusinessApplicationInfo(context, type, "00",
                      otype1, oflag1,
                      otype2, oflag2,
                      otype3, oflag3,
                  );
                }
              }else{
                //打开某个应用，关闭其他应用
                oflag1 = "00";
                oflag2 = "00";
                oflag3 = "00";
                _viewModel.editBusinessApplicationInfo(context, type, "01",
                  otype1, oflag1,
                  otype2, oflag2,
                  otype3, oflag3,
                );
              }

            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Image.asset(
                isOpen
                    ? "images/icon_open.png"
                    :"images/icon_close.png",
                width: 36,
              ),
            ),
          ),
        ],
      ),
    );
  }



}

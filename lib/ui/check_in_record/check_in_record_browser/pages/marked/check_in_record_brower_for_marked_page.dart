import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_refresh_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/marked/check_in_record_brower_for_marked_view_model.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/widgets/check_in_record_brower_grid_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/widgets/check_in_record_brower_list_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/widgets/check_in_record_brower_top_mode_tabs_widget.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import '../../check_in_record_brower_page.dart';

/// 打卡记录浏览-已标记
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 5:34 PM
/// @specialDemand:
class CheckInRecordBrowerForMarkedPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CheckInRecordBrowerForMarkedPage";

  @override
  _CheckInRecordBrowerForMarkedPageState createState() =>
      _CheckInRecordBrowerForMarkedPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CheckInRecordBrowerForMarkedPage(),
      routeName: CheckInRecordBrowerForMarkedPage.ROUTER,
    );
  }
}

class _CheckInRecordBrowerForMarkedPageState extends BasePagerState<
        CheckInRecordBrowerForTypeListItemBean,
        CheckInRecordBrowerForMarkedPage>
    with AutomaticKeepAliveClientMixin, VgPlaceHolderStatusMixin {
  CheckInRecordBrowerForMarkedViewModel _viewModel;

  PageController _pageController;

  TopModeTabsType _mode = TopModeTabsType.grid;

  StreamSubscription _streamSubscription;

  @override
  void initState() {
    super.initState();
    _viewModel = CheckInRecordBrowerForMarkedViewModel(this);
    _pageController =
        PageController(initialPage: _mode == TopModeTabsType.grid ? 0 : 1);
    _viewModel?.refresh();
    _streamSubscription = CheckInRecordBrowerPageState.of(context).multiRefreshStreamController?.stream?.listen((event) {
      _viewModel?.refreshMultiPage();
    });
  }

  @override
  void dispose() {
    _pageController?.dispose();
    _streamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        CheckInRecordBrowerTopModeTabsWidget(
          mode: _mode,
          onGridTap: (mode) {
            _animateToPage(0);
          },
          onListTap: (mode) {
            _animateToPage(1);
          },
        ),
        Expanded(
          child: _toPageViewWidget(),
        )
      ],
    );
  }

  void _animateToPage(int page) {
    _pageController?.animateToPage(page,
        duration: Duration(milliseconds: 200), curve: Curves.linear);
  }

  Widget _toPageViewWidget() {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        _toPullRefreshWidget(
          child: CheckInRecordBrowerGridWidget(
            itemCount: data?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
              return _toGridItemWidget(context, data?.elementAt(index));
            },
          ),
        ),
        _toPullRefreshWidget(
          child: CheckInRecordBrowerListWidget(
            itemCount: data?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
              return _toListItemWidget(context, data?.elementAt(index));
            },
          ),
        ),
      ],
    );
  }

  Widget _toPullRefreshWidget({Widget child}) {
    return MixinPlaceHolderStatusWidget(
      emptyOnClick: () => _viewModel.refresh(),
      errorOnClick: () => _viewModel.refresh(),
      // loadingOnClick: () => _viewModel?.refresh(),
      child: VgPullRefreshWidget.bind(
          state: this, viewModel: _viewModel, child: child),
    );
  }

  Widget _toGridItemWidget(BuildContext context, CheckInRecordBrowerForTypeListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        PersonDetailPage.navigatorPush(context,itemBean?.signFuid);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Stack(
          fit: StackFit.expand,
          children: [
            VgCacheNetWorkImage(
              itemBean?.napicurl ?? "",
              fit: BoxFit.cover,
              imageQualityType: ImageQualityType.middleDown,
              defaultPlaceType: ImagePlaceType.head,
              defaultErrorType: ImageErrorType.head,
            ),
            // Positioned(
            //   top: 0,
            //   right: 0,
            //   child: Container(
            //     width: 32,
            //     height: 17,
            //     decoration: BoxDecoration(
            //         color: ThemeRepository.getInstance().getMinorRedColor_F95355(),
            //         borderRadius: BorderRadius.only(topRight: Radius.circular(4),bottomLeft: Radius.circular(4))
            //     ),
            //     child: Center(
            //       child:  Text(
            //         "标记",
            //         maxLines: 1,
            //         overflow: TextOverflow.ellipsis,
            //         style: TextStyle(
            //             color: Colors.white,
            //             fontSize: 11,
            //             height: 1.15
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            //遮罩
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 45,
                decoration: BoxDecoration(

                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0x00000000),
                          Color(0x80000000)
                        ]
                    )
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 6, vertical: 6),
                alignment: Alignment.centerLeft,
                child: Text(
                 itemBean?.name ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _toListItemWidget(BuildContext context, CheckInRecordBrowerForTypeListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        PersonDetailPage.navigatorPush(context,itemBean?.signFuid);
      },
      child: Container(
        height: 60,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Container(
                width: 36,
                height: 36,
                child: VgCacheNetWorkImage(
                  itemBean?.napicurl ?? "",
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middleUp,
                  defaultPlaceType: ImagePlaceType.head,
                  defaultErrorType: ImageErrorType.head,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 36,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Text(
                          itemBean?.name ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: ThemeRepository.getInstance()
                                  .getTextMainColor_D0E0F7(),
                              fontSize: 15,
                              height: 1.2),
                        ),
                        // Text(
                        //   "/",
                        //   maxLines: 1,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(
                        //       color: ThemeRepository.getInstance()
                        //           .getTextMainColor_D0E0F7(),
                        //       fontSize: 15,
                        //       height: 1.2),
                        // ),
                        // Text(
                        //   "Sharon",
                        //   maxLines: 1,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(
                        //       color: ThemeRepository.getInstance()
                        //           .getTextMainColor_D0E0F7(),
                        //       fontSize: 12,
                        //       height: 1.2),
                        // ),
                        Spacer(),
                        Text(
                          _getGroupAndIdStr(itemBean?.groupName,itemBean?.number),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 12,
                              height: 1.2),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "${VgToolUtils.getTimestrampStr(itemBean?.punchTime) ?? ""}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 12,
                              height: 1.2),
                        ),
                        Spacer(),
                        Text(
                          StringUtils.isEmpty(itemBean?.uname) ? "":"${itemBean.uname}标记",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 10,
                              height: 1.2),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
      ),
    );
  }

  String _getGroupAndIdStr(String groupName,String number){
    if(StringUtils.isEmpty(groupName) && StringUtils.isEmpty(number)){
      return null;
    }
    if(StringUtils.isNotEmpty(groupName) && StringUtils.isEmpty(number)){
      return groupName;
    }
    if(StringUtils.isEmpty(groupName) && StringUtils.isNotEmpty(number)){
      return number;
    }
    return groupName+ "·" + number;
  }


  @override
  bool get wantKeepAlive => true;


  @override
  void setListData(List<CheckInRecordBrowerForTypeListItemBean> list) {
    // if(list != null && list.isNotEmpty){
    //   list = list.toSet().toSet()?.toList();
    // }
    super.setListData(list);
  }
}

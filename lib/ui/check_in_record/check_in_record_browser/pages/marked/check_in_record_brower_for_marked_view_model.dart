import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_total_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class CheckInRecordBrowerForMarkedViewModel extends BasePagerViewModel<
    CheckInRecordBrowerForTypeListItemBean,
    CheckInRecordBrowerForTypeResponseBean> {
  CheckInRecordBrowerForMarkedViewModel(BaseState<StatefulWidget> state)
      : super(state);

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "companyid": UserRepository.getInstance().companyId ?? "",
      "current": page ?? 1,
      "sign": "01",
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appFaceSignList";
  }

  @override
  CheckInRecordBrowerForTypeResponseBean parseData(VgHttpResponse resp) {
    CheckInRecordBrowerForTypeResponseBean vo =
        CheckInRecordBrowerForTypeResponseBean.fromMap(resp?.data);
    loading(false);
    VgEventBus.global.send(CheckTotalEven(totalBean: vo?.data?.eachSignCnt));
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

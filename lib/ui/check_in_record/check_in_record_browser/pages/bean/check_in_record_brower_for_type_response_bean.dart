import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"eachSignCnt":{"ignorecnt":0,"unsigncnt":71,"signcnt":2},"page":{"records":[{"fid":"4b5ce0a2e67447a0bf4c7318630751af","punchTime":1611023333505,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","temperature":"0.0","punchName":"用户4108","signFuid":"dfeae56b9b7041fc876ff39e162f1905","name":"123123","nick":"12321313123","phone":"21321321","number":"123213","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","groupName":"访客"},{"fid":"ce5674840359476692f8d029c1818421","punchTime":1611023622,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","temperature":"0.0","punchName":"用户4393","signFuid":"82398c6f8c6c44e1a5100e7f9ddfde80","name":"李岁红","nick":"z z z","phone":"13226332406","number":"","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","groupName":"访客"}],"total":2,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class CheckInRecordBrowerForTypeResponseBean extends BasePagerBean<CheckInRecordBrowerForTypeListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CheckInRecordBrowerForTypeResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordBrowerForTypeResponseBean checkInRecordBrowerForTypeResponseBeanBean = CheckInRecordBrowerForTypeResponseBean();
    checkInRecordBrowerForTypeResponseBeanBean.success = map['success'];
    checkInRecordBrowerForTypeResponseBeanBean.code = map['code'];
    checkInRecordBrowerForTypeResponseBeanBean.msg = map['msg'];
    checkInRecordBrowerForTypeResponseBeanBean.data = DataBean.fromMap(map['data']);
    checkInRecordBrowerForTypeResponseBeanBean.extra = map['extra'];
    return checkInRecordBrowerForTypeResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current ?? 1;

  ///得到List列表
  @override
  List<CheckInRecordBrowerForTypeListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages ?? 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// eachSignCnt : {"ignorecnt":0,"unsigncnt":71,"signcnt":2}
/// page : {"records":[{"fid":"4b5ce0a2e67447a0bf4c7318630751af","punchTime":1611023333505,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","temperature":"0.0","punchName":"用户4108","signFuid":"dfeae56b9b7041fc876ff39e162f1905","name":"123123","nick":"12321313123","phone":"21321321","number":"123213","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","groupName":"访客"},{"fid":"ce5674840359476692f8d029c1818421","punchTime":1611023622,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","temperature":"0.0","punchName":"用户4393","signFuid":"82398c6f8c6c44e1a5100e7f9ddfde80","name":"李岁红","nick":"z z z","phone":"13226332406","number":"","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","groupName":"访客"}],"total":2,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  EachSignCntBean eachSignCnt;
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.eachSignCnt = EachSignCntBean.fromMap(map['eachSignCnt']);
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "eachSignCnt": eachSignCnt,
    "page": page,
  };
}

/// records : [{"fid":"4b5ce0a2e67447a0bf4c7318630751af","punchTime":1611023333505,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","temperature":"0.0","punchName":"用户4108","signFuid":"dfeae56b9b7041fc876ff39e162f1905","name":"123123","nick":"12321313123","phone":"21321321","number":"123213","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg","groupName":"访客"},{"fid":"ce5674840359476692f8d029c1818421","punchTime":1611023622,"pictureUrl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","temperature":"0.0","punchName":"用户4393","signFuid":"82398c6f8c6c44e1a5100e7f9ddfde80","name":"李岁红","nick":"z z z","phone":"13226332406","number":"","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611023695880-1611023695693Unknown_2101191034553_.jpg","groupName":"访客"}]
/// total : 2
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CheckInRecordBrowerForTypeListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CheckInRecordBrowerForTypeListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// fid : "4b5ce0a2e67447a0bf4c7318630751af"
/// punchTime : 1611023333505
/// pictureUrl : "http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg"
/// temperature : "0.0"
/// punchName : "用户4108"
/// signFuid : "dfeae56b9b7041fc876ff39e162f1905"
/// name : "123123"
/// nick : "12321313123"
/// phone : "21321321"
/// number : "123213"
/// napicurl : "http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1610968492555-1610968492015Unknown_2101181914525_.jpg"
/// groupName : "访客"

class CheckInRecordBrowerForTypeListItemBean {
  String fid;
  int punchTime;
  String pictureUrl;
  String temperature;
  String punchName;
  String signFuid;
  String name;
  String nick;
  String phone;
  String number;
  String napicurl;
  String groupName;
  int vcnt;
  String groupid;

  String uname;

  static CheckInRecordBrowerForTypeListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordBrowerForTypeListItemBean recordsBean = CheckInRecordBrowerForTypeListItemBean();
    recordsBean.fid = map['fid'];
    recordsBean.punchTime = map['punchTime'];
    recordsBean.pictureUrl = map['pictureUrl'];
    recordsBean.temperature = map['temperature'];
    recordsBean.punchName = map['punchName'];
    recordsBean.signFuid = map['signFuid'];
    recordsBean.name = map['name'];
    recordsBean.nick = map['nick'];
    recordsBean.phone = map['phone'];
    recordsBean.number = map['number'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.groupName = map['groupName'];
    recordsBean.vcnt = map['vcnt'];
    recordsBean.groupid = map['groupid'];
    recordsBean.uname = map['uname'];
    return recordsBean;
  }

  Map toJson() => {
    "fid": fid,
    "punchTime": punchTime,
    "pictureUrl": pictureUrl,
    "temperature": temperature,
    "punchName": punchName,
    "signFuid": signFuid,
    "name": name,
    "nick": nick,
    "phone": phone,
    "number": number,
    "napicurl": napicurl,
    "groupName": groupName,
    "vcnt": vcnt,
    "groupid": groupid,
    "uname":uname,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckInRecordBrowerForTypeListItemBean &&
          runtimeType == other.runtimeType &&
          signFuid == other.signFuid;

  @override
  int get hashCode => signFuid.hashCode;
}

/// ignorecnt : 0
/// unsigncnt : 71
/// signcnt : 2

class EachSignCntBean {
  int ignorecnt;
  int unsigncnt;
  int signcnt;

  static EachSignCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EachSignCntBean eachSignCntBean = EachSignCntBean();
    eachSignCntBean.ignorecnt = map['ignorecnt'];
    eachSignCntBean.unsigncnt = map['unsigncnt'];
    eachSignCntBean.signcnt = map['signcnt'];
    return eachSignCntBean;
  }

  Map toJson() => {
    "ignorecnt": ignorecnt,
    "unsigncnt": unsigncnt,
    "signcnt": signcnt,
  };
}
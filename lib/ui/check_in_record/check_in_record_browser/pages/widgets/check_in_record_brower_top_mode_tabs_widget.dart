import 'dart:ffi';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/widgets.dart';

enum TopModeTabsType{
  grid,
  list
}
/// 顶部模式选择tab
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 4:35 PM
/// @specialDemand:
class CheckInRecordBrowerTopModeTabsWidget extends StatefulWidget {

  final TopModeTabsType mode;

  final ValueChanged<TopModeTabsType> onGridTap;

  final ValueChanged<TopModeTabsType> onListTap;

  final bool isShowIgnore;

  final VoidCallback onIgnore;

  const CheckInRecordBrowerTopModeTabsWidget({Key key, this.mode, this.onGridTap, this.onListTap, this.isShowIgnore = false, this.onIgnore}) :
        assert(mode != null),
        super(key: key);

  @override
  _CheckInRecordBrowerTopModeTabsWidgetState createState() => _CheckInRecordBrowerTopModeTabsWidgetState();
}

class _CheckInRecordBrowerTopModeTabsWidgetState extends State<CheckInRecordBrowerTopModeTabsWidget> {

  TopModeTabsType _mode;

  @override
  void initState() {
    super.initState();
    _mode = widget?.mode;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(
        height: 1.2
      ),
      child: Container(
        height: 47,
        child: Row(
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(widget.onGridTap != null){
                  widget.onGridTap(TopModeTabsType.grid);
                }
                _mode = TopModeTabsType.grid;
                setState(() {

                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 15,right: 8),
                child: Center(
                  child:  Text(
                    "照片模式",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: _mode == TopModeTabsType.grid ? ThemeRepository.getInstance().getPrimaryColor_1890FF() : Color(0xFF5E687C),
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 0.5,
              height: 12,
              color: VgColors.INPUT_BG_COLOR,
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(widget.onListTap != null){
                  widget.onListTap(TopModeTabsType.list);
                }
                _mode = TopModeTabsType.list;
                setState(() {

                });
              },
              child: Container(
                padding: const EdgeInsets.only(left: 8,right: 15),
                child: Center(
                  child:  Text(
                    "列表模式",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: _mode == TopModeTabsType.list ? ThemeRepository.getInstance().getPrimaryColor_1890FF() : Color(0xFF5E687C),
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
            Spacer(),
            Offstage(
              offstage: !(widget.isShowIgnore),
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () async {
                  bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,content: "确认忽略所有未标记人脸？");
                  if(result ?? false){
                    if(widget?.onIgnore != null){
                      widget.onIgnore();
                    }
                  }
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Center(
                    child:  Text(
                              "一键忽略",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color:ThemeRepository.getInstance().getTextBFC2CC_BFC2CC() ,
                                  fontSize: 12,
                                  ),
                            ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

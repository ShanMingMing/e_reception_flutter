import 'package:flutter/widgets.dart';

/// 表格列表
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 4:43 PM
/// @specialDemand:
class CheckInRecordBrowerGridWidget extends StatelessWidget {

  final IndexedWidgetBuilder itemBuilder;

  final int itemCount;

  const CheckInRecordBrowerGridWidget({Key key, this.itemBuilder, this.itemCount}) :
        assert(itemBuilder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        padding: EdgeInsets.only(left: 15, right: 15, top:3,bottom: 15),
        itemCount: itemCount ?? 0,
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          childAspectRatio: 1/1,
        ),
        itemBuilder: itemBuilder);
  }
}

import 'package:flutter/widgets.dart';

/// 打卡记录浏览-列表
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 5:03 PM
/// @specialDemand:
class CheckInRecordBrowerListWidget extends StatelessWidget {
  final IndexedWidgetBuilder itemBuilder;

  final int itemCount;

  const CheckInRecordBrowerListWidget(
      {Key key, this.itemBuilder, this.itemCount})
      : assert(itemBuilder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        itemCount: itemCount ?? 0,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: itemBuilder,
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 0,
          );
        });
  }
}

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/check_in_record_brower_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_refresh_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_total_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class CheckInRecordBrowerForUnmarkedViewModel extends BasePagerViewModel<CheckInRecordBrowerForTypeListItemBean,CheckInRecordBrowerForTypeResponseBean>{

  ///一键忽略
  static const String ALL_IGNORE_FACE_API =ServerApi.BASE_URL + "app/appIgnoreFaceUser";

  ///忽略或删除刷脸用户
  static const String SINGLE_IGNORE_OR_DELETE_API =ServerApi.BASE_URL + "app/appIgnoreDeleteOneFaceUser";

  CheckInRecordBrowerForUnmarkedViewModel(BaseState<StatefulWidget> state) : super(state);


  ///一键忽略
  void ignoreFaceHttp(BuildContext context){
    VgHttpUtils.post(ALL_IGNORE_FACE_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          CheckInRecordBrowerPageState.of(context).multiRefreshStreamController?.add(null);
          VgToastUtils.toast(context, "一键忽略成功");
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///忽略一张脸
  void ignoreSingleFaceHttp(BuildContext context,String fid){
    if(StringUtils.isEmpty(fid)){
      VgToastUtils.toast(context, "人脸数据获取失败");
      return;
    }
    VgHttpUtils.post(SINGLE_IGNORE_OR_DELETE_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "fid":fid ?? "",
      "flg":"00", //00忽略 01删除
    },callback: BaseCallback(
        onSuccess: (val){
          CheckInRecordBrowerPageState.of(context).multiRefreshStreamController?.add(null);
          VgToastUtils.toast(context, "忽略成功");
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  ///删除一张脸
  void deleteSingleFaceHttp(BuildContext context,String fid){
    if(StringUtils.isEmpty(fid)){
      VgToastUtils.toast(context, "人脸数据获取失败");
      return;
    }
    VgHttpUtils.post(SINGLE_IGNORE_OR_DELETE_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "fid":fid ?? "",
      "flg":"01", //00忽略 01删除
    },callback: BaseCallback(
        onSuccess: (val){
          CheckInRecordBrowerPageState.of(context).multiRefreshStreamController?.add(null);
          VgToastUtils.toast(context, "删除成功");
        },
        onError: (msg){
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "companyid": UserRepository.getInstance().companyId ?? "",
      "current": page ?? 1,
      "sign": "00",
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appFaceSignList";
  }

  @override
  CheckInRecordBrowerForTypeResponseBean parseData(VgHttpResponse resp) {
    CheckInRecordBrowerForTypeResponseBean vo = CheckInRecordBrowerForTypeResponseBean.fromMap(resp?.data);
    loading(false);
    VgEventBus.global.send(CheckTotalEven(totalBean: vo?.data?.eachSignCnt));

    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }


}
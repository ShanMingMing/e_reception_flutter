import 'dart:async';

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_total_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

import 'widgets/check_in_record_brower_pages_widget.dart';
import 'widgets/check_in_record_brower_top_tabs_widget.dart';

/// 打卡记录-浏览模式
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 3:41 PM
/// @specialDemand:
class CheckInRecordBrowerPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CheckInRecordBrowerPage";

  @override
  CheckInRecordBrowerPageState createState() => CheckInRecordBrowerPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      CheckInRecordBrowerPage(),
      routeName: CheckInRecordBrowerPage.ROUTER,
    );
  }
}

class CheckInRecordBrowerPageState extends State<CheckInRecordBrowerPage>
    with TickerProviderStateMixin {
  TabController _tabController;

  List<String> _tabsList;

  StreamSubscription streamSubscription;

  EachSignCntBean totalBean;

  StreamController<Null> multiRefreshStreamController;

  ///获取state
  static CheckInRecordBrowerPageState of(BuildContext context) {
    final CheckInRecordBrowerPageState result =
        context.findAncestorStateOfType<CheckInRecordBrowerPageState>();
    return result;
  }

  @override
  void initState() {
    super.initState();
    _tabsList = [
      "未标记",
      "已标记",
      "已忽略",
    ];
    multiRefreshStreamController = StreamController.broadcast();
    _tabController = TabController(length: _tabsList?.length ?? 0, vsync: this);

    ///todo：需重构，因快速实现功能直接用事件，
    streamSubscription = VgEventBus.global.on().listen((event) {
      if (event is CheckTotalEven) {
        if (event.totalBean == null) {
          return;
        }
        totalBean = event.totalBean;
        Future.delayed(Duration(milliseconds: 200),(){
          setState(() {

          });
        });
      }
    });
  }

  @override
  void dispose() {
    _tabController?.dispose();
    multiRefreshStreamController?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        CheckInRecordBrowerTopTabsWidget(
          tabsList: _tabsList,
          tabController: _tabController,
          totalBean: totalBean,
        ),
        Expanded(
          child: CheckInRecordBrowerPagesWidget(
              tabsList: _tabsList, tabController: _tabController),
        )
      ],
    );
  }
}

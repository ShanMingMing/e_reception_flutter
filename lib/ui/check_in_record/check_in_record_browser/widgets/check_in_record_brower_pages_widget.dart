import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/ignore/check_in_record_brower_for_ignore_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/marked/check_in_record_brower_for_marked_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/unmarked/check_in_record_brower_for_unmarked_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 打卡记录浏览-pageView
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 4:20 PM
/// @specialDemand:
class CheckInRecordBrowerPagesWidget extends StatelessWidget {

  final TabController tabController;

  final List<String> tabsList;

  const CheckInRecordBrowerPagesWidget({Key key, this.tabController, this.tabsList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: TabBarView(
        controller: tabController,
        children: [
          CheckInRecordBrowerForUnmarkedPage(),
          CheckInRecordBrowerForMarkedPage(),
          CheckInRecordBrowerForIgnorePage(),
        ],
      ),
    );
  }
}


import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 打卡记录预览-顶部tab栏
///
/// @author: zengxiangxi
/// @createTime: 1/12/21 3:44 PM 
/// @specialDemand:
class CheckInRecordBrowerTopTabsWidget extends StatelessWidget {

  final TabController tabController;

  final List<String> tabsList;

  final EachSignCntBean totalBean;

  List<int> totalList;

  CheckInRecordBrowerTopTabsWidget({Key key, this.tabController, this.tabsList, this.totalBean}) : super(key: key){
    totalList = List();
    totalList.add(totalBean?.unsigncnt ?? 0);
    totalList?.add(totalBean?.signcnt ?? 0);
    totalList?.add(totalBean?.ignorecnt ?? 0);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      padding: EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)),
      child: _toMainRowWidget(context),
    );
  }

  Widget _toMainRowWidget(BuildContext context){
    return Container(
      height: 44,
      child: Row(
        children: <Widget>[
          _toBackWidget(context),
          Expanded(
            child: Theme(
                data: ThemeData(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent),
                child: _toTabBarWidget()),
          ),
        ],
      ),
    );
  }

  Widget _toBackWidget(BuildContext context){
    return  ClickAnimateWidget(
      child: Container(
        width: 40,
        height: 44,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.only(left: 15.6),
        child: Image.asset(
          "images/top_bar_back_ico.png",
          width: 9,
          color: VgColors.INPUT_BG_COLOR,
        ),
      ),
      scale: 1.4,
      onClick: () {
        // VgNavigatorUtils.pop(context);
        FocusScope.of(context).requestFocus(FocusNode());
        Navigator.of(context).maybePop();
      },
    );
  }

  Widget _toTabBarWidget() {
    return TabBar(
      controller: tabController,
      isScrollable: false,
      indicatorSize: TabBarIndicatorSize.label,
      unselectedLabelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 15,
          height: 1.2),
      labelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 17,
          fontWeight: FontWeight.w600,
          height: 1.2),
      indicatorColor: Colors.transparent,
      tabs: List.generate(tabsList?.length ?? 0, (index) {
        return Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(tabsList?.elementAt(index)),
              SizedBox(
                width: 3,
              ),
              Text(
                "${totalList?.elementAt(index) ?? 0}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 12,
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}

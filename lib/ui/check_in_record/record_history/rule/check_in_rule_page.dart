import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_select_date_time_dialog/common_select_date_time_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/check_in_rec_by_month_response.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_date_picker/vg_date_picker.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import 'check_in_rule_view_model.dart';

class CheckInRulePage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CheckInRulePage";

  ///考勤规则
  final RuleTimeBean ruleTimeBean;

  CheckInRulePage(this.ruleTimeBean);

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, RuleTimeBean bean) {
    return RouterUtils.routeForFutureResult(
      context,
      CheckInRulePage(bean),
      routeName: CheckInRulePage.ROUTER,
    );
  }

  @override
  State<StatefulWidget> createState() {
    return CheckInRulePageState();
  }
}

class CheckInRulePageState extends BaseState {
  CheckInRuleViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = CheckInRuleViewModel(this);
    RuleTimeBean ruleTimeBean = (widget as CheckInRulePage).ruleTimeBean;
    if (ruleTimeBean != null) {
      print(ruleTimeBean.toJson());
      if(ruleTimeBean.ondutytime is double){
        _viewModel.startTime =
            DateTime.fromMillisecondsSinceEpoch(ruleTimeBean.ondutytime.round()*1000);
        _viewModel.endTime =
            DateTime.fromMillisecondsSinceEpoch(ruleTimeBean.offdutytime.round()*1000);
      }else{
        _viewModel.startTime =
            DateTime.fromMillisecondsSinceEpoch(ruleTimeBean.ondutytime*1000);
        _viewModel.endTime =
            DateTime.fromMillisecondsSinceEpoch(ruleTimeBean.offdutytime*1000);
      }


      print(ruleTimeBean.offdutytime.round());
      _viewModel.arid = ruleTimeBean.arid;
    }

    _viewModel.setRuleController.listen((t) {
      if(t){
        VgToastUtils.toast(context, "保存成功");
        //设置规则结果
        Navigator.pop(context, "");
      }

    });
    _viewModel.controller.listen((t) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: <Widget>[
          _toTopBarWidget(),
          Container(
            height: 50,
            color: Color(0xff282d44),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: <Widget>[
                Text("上班时间",
                    style: TextStyle(color: Color(0xff808388), fontSize: 14)),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      child: Container(
                        height: 50,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          _viewModel.startTime == null
                              ? "请选择"
                              : DateUtil.formatDate(_viewModel.startTime,
                                  format: "HH:mm"),
                          style: TextStyle(
                              color: _viewModel.startTime == null
                                  ? Color(0xff808388)
                                  : Colors.white,
                              fontSize: 14),
                        ),
                      ),
                      onTap: () async {
                        Map result = await CommonSelectDateTimeDialog
                            .navigatorPushDialog(
                                context, VgCupertinoDatePickerMode.time,
                                initDateTime: _viewModel.startTime??DateTime(2021,1,1,9,0),
                                maxDateTime: _viewModel.endTime,
                        showClear: true);
                        if(result==null){
                          return;
                        }
                        _viewModel.setStartTime(result[CommonSelectDateTimeDialog.RESULT_DATE_TIME]);
                      }),
                ),
                Image.asset("images/go_ico.png",
                    width: 5.3, height: 9.6, color: VgColors.INPUT_BG_COLOR)
              ],
            ),
          ),
          Container(
            height: 1,
            color: Color(0xff3a3f50),
          ),
          //下班时间
          Container(
            height: 50,
            color: Color(0xff282d44),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: <Widget>[
                Text("下班时间",
                    style: TextStyle(color: Color(0xff808388), fontSize: 14)),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      child: Container(
                        height: 50,
                        alignment: Alignment.centerLeft,
                        child: Text(
                            _viewModel.endTime == null
                                ? "请选择"
                                : DateUtil.formatDate(_viewModel.endTime,
                                    format: "HH:mm"),
                            style: TextStyle(
                                color: _viewModel.endTime == null
                                    ? Color(0xff808388)
                                    : Colors.white,
                                fontSize: 14)),
                      ),
                      onTap: () async {
                        Map result = await CommonSelectDateTimeDialog
                            .navigatorPushDialog(
                                context, VgCupertinoDatePickerMode.time,
                                initDateTime: _viewModel.endTime??DateTime(2021,1,1,18,0),
                                minDateTime: _viewModel.startTime,
                        showClear: true);
                        if(result==null){
                          return;
                        }
                        _viewModel.setEndTime(result[CommonSelectDateTimeDialog.RESULT_DATE_TIME]);
                      }),
                ),
                Image.asset("images/go_ico.png",
                    width: 5.3, height: 9.6, color: VgColors.INPUT_BG_COLOR)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      title: "考勤规则",
      rightWidget: _toSaveButtonWidget(),
    );
  }

  Widget _toSaveButtonWidget() {
    return CommonFixedHeightConfirmButtonWidget(
      isAlive: _viewModel.judgeCommit(),
      width: 48,
      height: 24,
      disableMillTime: 1000,
      unSelectBgColor:
          ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
      selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR, fontSize: 12, fontWeight: FontWeight.w600),
      selectedTextStyle: TextStyle(
          color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
      text: "保存",
      onTap: () {
        if (_viewModel.judgeCommit()) {
          _viewModel.commit();
        } else {
          if (_viewModel.startTime == null) {
            VgToastUtils.toast(context, "请选择上班时间");
          } else {
            VgToastUtils.toast(context, "请选择下班时间");
          }
        }
//        String msg = uploadBean?.checkVerify();
//        if(StringUtils.isEmpty(msg)){
//          _viewModel?.saveAddUserHttp(context, uploadBean);
//          return;
//        }
//        VgToastUtils.toast(context, msg);
      },
    );
  }
}

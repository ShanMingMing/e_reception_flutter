import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/check_in_rec_by_month_response.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/daily_check_in_detail_response.dart';
import 'package:e_reception_flutter/ui/mypage/daily_detail_list_page.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import 'daily_check_in_view_model.dart';

///单个某天考勤打卡记录
class DailyCheckInDetailPage extends StatefulWidget {
  final String dayTime;
  final ShowType type;
  final bool weekend;
  final String groupType;
  final String chooseId;

  DailyCheckInDetailPage(this.dayTime, this.type,
      {this.weekend, this.groupType, this.chooseId});

  @override
  State<StatefulWidget> createState() {
    return DailyCheckInDetailState();
  }
}

class DailyCheckInDetailState extends BaseState<DailyCheckInDetailPage>
    with SingleTickerProviderStateMixin {
  DailyCheckInViewModel viewModel;


  @override
  void initState() {
    viewModel = DailyCheckInViewModel(this);
    super.initState();
    viewModel.queryData(
        widget.dayTime, widget?.type, widget?.groupType, widget?.chooseId);
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: viewModel.dataNotifier,
      builder: (context, List<RecordsBean> dataList, child) {
        int count = dataList?.length ?? 0;
        return VgPlaceHolderStatusWidget(
            emptyStatus: dataList == null || dataList.isEmpty,
            child: ListView.separated(
                itemCount: count,
                separatorBuilder: (context, index) {
                  return Container(height: 1,
                    color: Color(0xff303546),
                    margin: EdgeInsets.only(left: 15),);
                },
                itemBuilder: (context, pos) {
                  RecordsBean record = dataList[pos];
                  return GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      if (record == null || record.fuid == null) {
                        return;
                      }
                      if(record?.delflg=="01"){
                        VgToastUtils.toast(context, "用户已删除");
                        return;
                      }
                      AttendRecordCalendarPage.navigatorPush(
                          context, record?.fuid,pages: "CheckInRecHistoryPage");
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 15, right: 15, bottom: 10, top: 10),
                      child: Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: Container(
                              height: 37,
                              width: 37,
                              child: VgCacheNetWorkImage(
                                record.napicurl ?? "",
                                fit: BoxFit.cover,
                                imageQualityType: ImageQualityType.middleDown,
                                defaultErrorType: ImageErrorType.head,
                                defaultPlaceType: ImagePlaceType.head,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    record.name,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: ThemeRepository.getInstance()
                                            .getTextMainColor_D0E0F7(),
                                        fontSize: 15,
                                        height: 1.2),
                                  ),
                                  _showCheckInTimeText(record)
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }));
      },
    );
  }

  ///第二行显示
  _showCheckInTimeText(RecordsBean record) {
    Color colorWhite = Color(0xff808388);
    Color colorRed = Color(0xfff95355);

    switch (widget.type) {
      case ShowType.NORMAL:
        return Text("${getFormatTime(record.firsttime)} 至 ${getFormatTime(
            record.lasttime)}",
            style: TextStyle(fontSize: 12, color: colorWhite));
        break;
      case ShowType.ABNORMAL:
        return RichText(
          text: TextSpan(children: [
            TextSpan(
                text: getFormatTime(record.firsttime),
                style: TextStyle(
                    fontSize: 12,
                    color: record.firsttime == null ||record.isAfterFirstTime()? colorRed : colorWhite)),
            TextSpan(
                text: " 至 ",
                style: TextStyle(
                    fontSize: 12,
                    color: colorWhite)),
            TextSpan(
                text: getFormatTime(record.lasttime),
                style: TextStyle(
                    fontSize: 12,
                    color: record.lasttime == null||record.isBeforeLastTime() ? colorRed : colorWhite))
          ]),
        );
        break;
      case ShowType.ABSENCE:
        return Text("缺勤",
            style: TextStyle(fontSize: 12, color: colorRed));
        break;
      case ShowType.NO_RULE:
        return Text(
            "${getFormatTime(record.firsttime)} 至 ${record.lasttime == null
                ? ''
                : DateUtil.formatDateMs(
                record.lasttime * 1000, format: "HH:mm")}",
            style: TextStyle(fontSize: 12, color:widget.weekend??false?Color(0xff00c6c4): colorWhite));
        break;
    }
  }

  String getFormatTime(int time) {
    if (time == null) {
      return "未知";
    }

    return DateUtil.formatDateMs(time * 1000, format: "HH:mm");
  }

}

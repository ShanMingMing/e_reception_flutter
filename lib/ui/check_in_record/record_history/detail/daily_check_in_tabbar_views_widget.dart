import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/check_in_record_by_type_list_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/check_in_rec_by_month_response.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/detail/daily_check_in_detail.dart';
import 'package:e_reception_flutter/ui/mypage/daily_detail_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

///可滑动的多个详情
class DailyCheckInTabBarViewsWidget extends StatefulWidget {
  final TabController tabController;

  final String dayTime;
  final bool weekend;
  final String groupType;
  final String chooseId;

  const DailyCheckInTabBarViewsWidget(
      {Key key,
      this.tabController,
      this.dayTime,
      this.weekend = false,
      this.groupType,
      this.chooseId})
      : super(key: key);

  @override
  DailyCheckInTabBarViewsWidgetState createState() =>
      DailyCheckInTabBarViewsWidgetState();
}

class DailyCheckInTabBarViewsWidgetState
    extends State<DailyCheckInTabBarViewsWidget> {
  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: TabBarView(
        controller: widget?.tabController,
        physics: NeverScrollableScrollPhysics(),
        children: List.generate(3, (index) {
          return DailyCheckInDetailPage(
            widget.dayTime,
            index == 0
                ? ShowType.NORMAL
                : index == 1 ? ShowType.ABNORMAL : ShowType.ABSENCE,
            weekend: widget.weekend,
            groupType: widget.groupType,
            chooseId: widget.chooseId,
          );
        }),
      ),
    );
  }
}

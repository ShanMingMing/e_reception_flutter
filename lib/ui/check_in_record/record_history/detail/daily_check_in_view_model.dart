import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/daily_check_in_detail_response.dart';
import 'package:e_reception_flutter/ui/mypage/daily_detail_list_page.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../../app_main.dart';

class DailyCheckInViewModel extends BaseViewModel {
  ValueNotifier<List<RecordsBean>> dataNotifier;

  DailyCheckInViewModel(BaseState<StatefulWidget> state) : super(state) {
    dataNotifier = ValueNotifier(null);
  }

  ///获取某一天的数据
  void queryData(
      String dayTime, ShowType type, String groupType, String chooseId) {
    loading(true);
    String dayTimeMillisecondsSinceEpoch =
        (DateTime.tryParse(dayTime).millisecondsSinceEpoch ~/ 1000).toString();
    VgHttpUtils.get(NetApi.DAILY_PUNCH_RECORD,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current": "1",
          "dayTime": dayTimeMillisecondsSinceEpoch,
          "size": "100",
          "type": getShowType(type),
          if (chooseId != null) "chooseId": chooseId,
          if (StringUtils.isNotEmpty(groupType)) "groupType": groupType
        },
        callback: BaseCallback(onSuccess: (val) {
          DailyCheckInDetailResponse bean =
              DailyCheckInDetailResponse.fromMap(val);
          dataNotifier?.value = bean?.data?.page?.records;
          loading(false);
        }, onError: (msg) {
          loading(false);
          dataNotifier?.value = null;
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  getShowType(ShowType type) {
    //00正常 01异常 02缺勤 03无
    if (type == ShowType.NORMAL) {
      return "00";
    } else if (type == ShowType.ABNORMAL) {
      return "01";
    } else if (type == ShowType.ABSENCE) {
      return "02";
    } else {
      return "03";
    }
  }
}

import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_list_page_widget/common_list_page_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_history_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/bean/department_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/class_single_select_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/departmentWidget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';

import 'package:e_reception_flutter/vg_widgets/vg_dialog_widget/pop_window.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

///部门参数
const TYPE_DEPARTMENT = '00';

///班级参数
const TYPE_CLASS = '01';

/// 选择部门或班级
///
///
typedef RoleChange=Function ( String roleType);
class ChooseFilterDepartmentClassPopWindow extends StatefulWidget {
  final String selectedId;
  final double offsetY;
  final String roleType;
  final bool hasRule;
  final RoleChange roleChange;
  const ChooseFilterDepartmentClassPopWindow(
      {Key key, this.selectedId, this.offsetY, this.roleType, this.hasRule, this.roleChange})
      : super(key: key);

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(
      BuildContext context, String selectId,
      {bool hasRule, String roleType,double offsetY,RoleChange roleChange}) {
    return Navigator.push(
        context,
        PopRoute(
            child: ChooseFilterDepartmentClassPopWindow(
          selectedId: selectId,
          hasRule: hasRule,
          roleType: roleType,
          roleChange: roleChange,
          offsetY: offsetY ?? 0,
        )));
  }

  @override
  State<StatefulWidget> createState() {
    return ChooseFilterDepartmentClassState();
  }
}

class ChooseFilterDepartmentClassState
    extends BaseState<ChooseFilterDepartmentClassPopWindow> {
  String selectedId;
  String selectType;

  @override
  void initState() {
    selectedId = widget.selectedId;
    if(widget.roleType!=null){
      selectType = (widget.roleType==TYPE_STAFF)?TYPE_DEPARTMENT:TYPE_CLASS;
    }else{
      selectType = TYPE_DEPARTMENT;
    }
    widget?.roleChange(selectType==TYPE_DEPARTMENT?TYPE_STAFF:TYPE_STU);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: _toMainWidget(context),
    );
  }

  _toMainWidget(BuildContext context) {
     bool showCheck=false;
     //机构 无规则  员工 学员  选择身份后 默认显示
    if(UserRepository.getInstance().isOrganization()){
       if(widget.hasRule??false){
         showCheck=false;
       }else{
         showCheck=true;
       }
    }else{
      showCheck=false;
    }
    return Container(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              RouterUtils.pop(context);
            },
            child: Container(
              color: Colors.transparent,
              height: ScreenUtils.getStatusBarH(context) +
                  44 +
                  (widget.offsetY ?? 0),
              child: Stack(
                children: [
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Container(
                      height: 10,
                      width: 45,
                      color: ColorConstants.BG_OR_SPLIT_COLOR,
                      alignment: Alignment.bottomRight,
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minHeight: 80, maxHeight: ScreenUtils.screenH(context) * 0.7),
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Visibility(
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.only(left: 15),
                      child: Row(
                        children: [
                          checkButton(selectType == TYPE_DEPARTMENT, "员工"),
                          SizedBox(
                            width: 10,
                          ),
                          checkButton(selectType == TYPE_CLASS, "学员")
                        ],
                      ),
                    ),

                    visible:showCheck ),
                Expanded(
                    child: selectType ==TYPE_CLASS?ClassSingleSelectWidget(selectedId: selectedId,
                        selectOnly: true):DepartmentSingleSelectWidget(
                  selectedId: selectedId,
                  selectOnly: true,
                ))
              ],
            ),
          ),
          Expanded(
            child: GestureDetector(
                onTap: () {
                  RouterUtils.pop(context);
                },
                child: Container(
                  color: Color(0x4d000000),
                )),
          )
        ],
      ),
    );
  }

  checkButton(bool checked, String text) {
    return GestureDetector(
      onTap: () {
        String type;
        if (text == '员工') {
          type = TYPE_DEPARTMENT;
        } else {
          type = TYPE_CLASS;
        }
        if (type != selectType) {
          selectType = type;
          widget?.roleChange(selectType==TYPE_DEPARTMENT?TYPE_STAFF:TYPE_STU);
          selectedId=null;
          setState(() {

          });
        }
      },
      child: Container(
        height: 28,
        width: 60,
        alignment: Alignment.center,
        child: Text(
          text,
          style: TextStyle(
              color: checked
                  ? Colors.white
                  : ThemeRepository.getInstance().getTextBFC2CC_BFC2CC()),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            color: checked
                ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                : ThemeRepository.getInstance().getBgOrSplitColor_191E31()),
      ),
    );
  }
}

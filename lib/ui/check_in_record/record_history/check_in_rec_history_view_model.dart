import 'dart:ffi';

import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_history_page.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_stream_lib.dart';

import '../../app_main.dart';
import 'bean/check_in_rec_by_month_response.dart';
import 'bean/has_rule_month_response.dart';

class CheckInRecHisViewModel extends BaseViewModel {
  ValueNotifier monthDataNotifier;

  ///日期变化
  VgStreamController<Void> dateStreamController;

  ///是否有规则
  ValueNotifier<bool> hasRuleNotifier;

  ///刷新页面组件
  VgStreamController<bool> refreshStreamController;

  ///筛选id
  ValueNotifier<EditTextAndValue> chooseIdNotifier;

  /// 身份类型和对应筛选
  Map<String,String> roleTypeChooseIdMap=Map();

  ///是否有考勤规则
  bool hasRule = false;

  /// 考勤规则
  RuleTimeBean rule;

  ///当前显示身份
  String roleType;

  int month;
  int year;



  CheckInRecHisViewModel(BaseState<StatefulWidget> state) : super(state) {
    monthDataNotifier = ValueNotifier(null);
    hasRuleNotifier=ValueNotifier(false);
    chooseIdNotifier=ValueNotifier(null);
    month = DateTime.now().month;
    year = DateTime.now().year;
    dateStreamController = newAutoReleaseBroadcast();
    refreshStreamController=newAutoReleaseBroadcast();
  }

  ///当月是否有规则
  void queryHasPunchInRule() {
    loading(true);
    final String sDateTime = getMonthMilliseconds();
    VgHttpUtils.get(NetApi.APP_HAVE_RULE_IN_MONTH,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "time": sDateTime,

        },
        callback: BaseCallback(onSuccess: (val) {
          HasRuleMonthResponse hasRuleMonthResponse =
              HasRuleMonthResponse.fromMap(val);
          if(hasRuleMonthResponse.success){
            loading(false);
            hasRuleNotifier.value=(hasRuleMonthResponse?.data?.ruleDayCnt??0)>0;
            hasRule = hasRuleNotifier.value;
            print('考勤规则：${hasRuleNotifier.value}');
            //刷新数据
            refreshSubPage();

          }else{
            loading(false);
            toast(hasRuleMonthResponse.msg);
          }

        }, onError: (msg) {
          loading(false);
          monthDataNotifier?.value = null;
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  ///获取某一月的数据
  void queryMonthRecData() {
    final String sDateTime = getMonthMilliseconds();
    loading(true);
    VgHttpUtils.get(NetApi.CHECK_IN_REC_BY_MONTH,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current": "1",
          "dayTime": sDateTime,
          "size": "31",
          "groupType": UserRepository.getInstance()
              .userData
              .companyInfo
              .groupList[0]
              .groupType
        },
        callback: BaseCallback(onSuccess: (val) {
          CheckInRecByMonthResponse bean =
              CheckInRecByMonthResponse.fromMap(val);
          hasRule = bean?.data?.punchRule ?? false;
          rule = bean?.data?.ruleTime;
          monthDataNotifier?.value = bean?.data;
          loading(false);
        }, onError: (msg) {
          loading(false);
          monthDataNotifier?.value = null;
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }



  String getMonthMilliseconds() {
    return ((DateTime(year, month, 1).millisecondsSinceEpoch) / 1000)
        .floor()
        .toString();
  }

  ///设置当前日期
  void setDate(int year, int month) {
    this.year = year;
    this.month = month;
    dateStreamController.add(null);
  }

  @override
  void onDisposed() {
    monthDataNotifier.dispose();
    super.onDisposed();
  }

  ///刷新数字页面
  void refreshSubPage() {
    if(refreshStreamController!=null&&!isStateDisposed){
      refreshStreamController.add(hasRuleNotifier.value);
    }
  }


}

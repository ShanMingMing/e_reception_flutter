import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/export_excel/select_class_or_department.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_page.dart';
import 'package:e_reception_flutter/ui/company/manager/edittext_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import '../check_in_rec_history_page.dart';

class ExportExcelSuccessPage extends StatelessWidget {
  final String email;

  ExportExcelSuccessPage(this.email);

  static Future<dynamic> navigatorPush(BuildContext context, String email) {
    return RouterUtils.routeForFutureResult(
      context,
      ExportExcelSuccessPage(email),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: Column(
        children: [
          TopBarWidget(
            title: '',
            backgroundColor:
                ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          ),
          SizedBox(
            height: 50,
          ),
          Image.asset(
            'images/tjcg.png',
            width: 100,
            height: 73,
          ),
          Text(
            '导出成功',
            style: TextStyle(fontSize: 17, color: Colors.white),
          ),
          Text(
            '请注意查收邮件',
            style: TextStyle(fontSize: 14, color: Colors.white),
          ),
          Text(
            '$email',
            style: TextStyle(fontSize: 14, color: Colors.white),
          ),
          SizedBox(
            height: 40,
          ),
          _confirmBtn(context)
        ],
      ),
    );
  }

  _confirmBtn(BuildContext context) {
    return GestureDetector(
        onTap: () {
          RouterUtils.popUntil(context, CheckInRecHistoryPage.ROUTER);
        },
        child: Container(
          width: double.infinity,
          height: 40,
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Center(
            child: Text(
              '我知道了',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
          ),
          decoration: BoxDecoration(
            color: ThemeRepository.getInstance()
                .getPrimaryColor_1890FF(),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(24)),
          ),
        ));
  }
}

import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_history_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_history_view_model.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_num_view_model.dart';
import 'package:e_reception_flutter/ui/mypage/daily_detail_list_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_time_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
import 'bean/check_in_rec_by_month_response.dart';

///正常颜色
const Color COLOR_NORMAL = Color(0xffbfc2cc);

///异常颜色
const Color COLOR_ABNORMAL = Color(0xffffb714);

///缺勤颜色
const Color COLOR_ABSENCE = Color(0xfff95355);

///休息颜色
const Color COLOR_REST = Color(0xff00c6c4);

///默认字体颜色
const Color COLOR_TEXT_DEFAULT = Color(0xff5e687c);

///背景色
const Color COLOR_BG = Color(0xff21263c);

///高亮的的背景色
const Color COLOR_BG_HIGHLIGHT = Color(0xff282d44);

///日期色
const Color COLOR_DATE = Color(0xff2a2f47);

///高亮的的日期色
const Color COLOR_DATE_SELECTED = Color(0xff343950);

class CheckInRecNumWidget extends StatefulWidget {
  ///有无规则
  final bool hasRule;

  ///筛选 员工家长学员
  final String roleType;

  CheckInRecNumWidget(this.roleType, this.hasRule);

  @override
  State<StatefulWidget> createState() {
    return CheckInRecNumState();
  }
}

class CheckInRecNumState extends BaseState<CheckInRecNumWidget>
    with AutomaticKeepAliveClientMixin {
  int year, month;

  ///有无规则
  bool hasRule = true;

  ScrollController _offsetController;

  CheckInRecNumViewModel viewModel;

  ///已选班级或部门id
  String chooseId;

  @override
  void initState() {
    super.initState();
    hasRule = widget.hasRule;
    viewModel = CheckInRecNumViewModel(this, widget.roleType);
    // viewModel.monthDataNotifier?.addListener(() {
    //   // _offsetController= ScrollController(initialScrollOffset: ((viewModel.monthDataNotifier?.value?.monthPunchRecord?.length ?? 0)*40).toDouble()); //定义ListView的controller
    // });
    CheckInRecHistoryPageState parentState =
        CheckInRecHistoryPageState.of(this.context);
    CheckInRecHisViewModel checkInRecHisViewModel = parentState.viewModel;
    year = checkInRecHisViewModel.year;
    month = checkInRecHisViewModel.month;
    viewModel.setDate(year, month);
    hasRule = checkInRecHisViewModel.hasRuleNotifier.value;
    checkInRecHisViewModel.refreshStreamController.listen((t) {
      year = checkInRecHisViewModel.year;
      month = checkInRecHisViewModel.month;
      hasRule = checkInRecHisViewModel.hasRuleNotifier.value;
      viewModel.setDate(year, month);
      viewModel.setRoleType(checkInRecHisViewModel.roleType);
      viewModel
          .setChooseId(checkInRecHisViewModel.chooseIdNotifier.value?.value);
      setState(() {});
      refresh();
    });

    refresh();
  }

  void refresh() {
    if (hasRule) {
      viewModel.queryRuleMonthRecData();
    } else {
      viewModel.queryNoRuleMonthRecData();
    }
  }

  @override
  Widget build(BuildContext context) {
    // _offsetController= ScrollController(initialScrollOffset: ((DateTime.now().day)*40).toDouble());
    return ValueListenableBuilder(
      valueListenable: viewModel.monthDataNotifier,
      builder: (BuildContext context, dynamic bean, Widget child) {
        return _content(bean);
      },
    );
  }

  ///内容
  Widget _content(DataBean bean) {
    //当前月份数加1 末尾加了间距
    int itemCount = DateTime(year, month + 1, 0).day + 1;
    int day = DateTime.now().day;
    int offset = 0;
    if(day > 8){
      offset = (day-8)*40;
    }
    return Column(
      children: <Widget>[
        //条目
        Container(
          height: 30,
          child: getSubjectBar(),
          color: COLOR_BG,
        ),
        //日期列表
        Expanded(
          child: ScrollConfiguration(
            behavior: MyBehavior(),
            child: ListView.builder(
              controller: ScrollController(initialScrollOffset: offset.toDouble()),
              itemBuilder: (context, position) {
                if (position == itemCount - 1) {
                  //底部间距
                  return SizedBox(
                    height: 130,
                  );
                }
                if (bean != null &&
                    bean.monthPunchRecord != null &&
                    position < bean.monthPunchRecord.length) {
                  return _listItem(bean.monthPunchRecord[position]);
                } else {
                  MonthPunchRecordBean nextDaysBean = MonthPunchRecordBean();
                  nextDaysBean.date = DateUtil.formatDate(
                      DateTime(year, month, position + 1),
                      format: "yyyy-MM-dd");
                  return _listItem(nextDaysBean);
                }
              },
              itemCount: itemCount,
              padding: EdgeInsets.all(0),
            ),
          ),
        ),
      ],
    );
  }

  ///列表项
  Widget _listItem(MonthPunchRecordBean monthPunchRecord) {
    return Container(
      height: 40,
      color: COLOR_BG,
      child: Row(
        children: <Widget>[
          //日期
          getDateWidget(monthPunchRecord),
          //详情
          monthPunchRecord.nextDays()
              ? Spacer()
              : Expanded(
                  child: _detailItem(monthPunchRecord),
                )
        ],
      ),
    );
  }

  ///日期 04/星期一
  Container getDateWidget(MonthPunchRecordBean monthPunchRecord) {
    return Container(
      width: 80,
      height: 40,
      padding: EdgeInsets.only(left: 14),
      color: monthPunchRecord.isWeekend() && !monthPunchRecord.nextDays()
          ? COLOR_DATE_SELECTED
          : COLOR_DATE,
      child: Row(
        children: [
          Text(monthPunchRecord.getDateFormat(),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  color: monthPunchRecord.nextDays()
                      ? COLOR_TEXT_DEFAULT
                      : Colors.white)),
          //星期
          Text("/" + monthPunchRecord.getDateWeek(),
              style: TextStyle(
                  fontSize: 9,
                  color: monthPunchRecord.nextDays()
                      ? COLOR_TEXT_DEFAULT
                      : Color(0xff808388),
                  height: 1.5))
        ],
      ),
    );
  }

  ///某一条详情
  _detailItem(MonthPunchRecordBean monthPunchRecord) {
    return Container(
      color: monthPunchRecord.isWeekend() && !monthPunchRecord.nextDays()
          ? COLOR_BG_HIGHLIGHT
          : COLOR_BG,
      child: Row(children: [
        Expanded(
            child: DefaultTextStyle.merge(
                style: TextStyle(fontSize: 12),
                child: getNumberWidget(monthPunchRecord))),
        //详情列
        Container(
          width: 55,
          child: Visibility(
            visible: canShowDetail(monthPunchRecord),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              child: Container(
                  width: 55,
                  child: Center(
                    child: Image.asset(
                      "images/go_ico.png",
                      width: 5.3,
                      height: 9.6,
                      color: VgColors.INPUT_BG_COLOR,
                    ),
                  )),
              onTap: () {
                if (!canToDetail(monthPunchRecord)) {
                  return;
                }
                if (viewModel.roleType != null) {
                  if (viewModel.roleType != TYPE_STAFF) {
                    hasRule = false;
                  }
                  DailyDetailListPage.navigatorPush(
                      context, monthPunchRecord,
                      hasRule, viewModel.roleType, viewModel.chooseId,
                      defaultType: monthPunchRecord.isWeekend()
                          ? ShowType.NO_RULE
                          : ShowType.NORMAL);
                }
              },
            ),
          ),
        )
      ]),
    );
  }

  ///列表无数据text -
  _noDataText() {
    return Container(
      width: 7,
      height: 1,
      color: COLOR_TEXT_DEFAULT,
    );
  }

  ///正常
  _normalText(MonthPunchRecordBean monthPunchRecord) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: monthPunchRecord.isWeekend()
            ? Container()
            : monthPunchRecord.normalcnt > 0
                ? Text(
                    "${monthPunchRecord.normalcnt}人",
                    style: TextStyle(color: COLOR_NORMAL, fontSize: 12),
                  )
                : _noDataText(),
      ),
      onTap: () {
        if (monthPunchRecord == null) {
          return;
        }
        if (monthPunchRecord.isWeekend()) {
          if (!canToDetail(monthPunchRecord)) {
            return;
          }
        } else {
          if (monthPunchRecord.normalcnt == 0) {
            return;
          }
        }
        DailyDetailListPage.navigatorPush(context, monthPunchRecord, hasRule,
            viewModel.roleType, viewModel.chooseId,
            defaultType: monthPunchRecord.isWeekend()
                ? ShowType.NO_RULE
                : ShowType.NORMAL);
      },
    );
  }

  ///异常
  _abnormalText(MonthPunchRecordBean monthPunchRecord) {
    int cnt = 0;
    if (monthPunchRecord.isWeekend()) {
      cnt = monthPunchRecord.facecnt;
    } else {
      cnt = monthPunchRecord.abnormalcnt;
    }
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: cnt > 0
            ? Text(
                "$cnt人",
                style: TextStyle(
                    color: monthPunchRecord.isWeekend()
                        ? COLOR_REST
                        : COLOR_ABNORMAL,
                    fontSize: 12),
              )
            : _noDataText(),
      ),
      onTap: () {
        if (monthPunchRecord == null) {
          return;
        }
        if (monthPunchRecord.isWeekend()) {
          if (!canToDetail(monthPunchRecord)) {
            return;
          }
        } else {
          if (monthPunchRecord.abnormalcnt == 0) {
            return;
          }
        }
        DailyDetailListPage.navigatorPush(context, monthPunchRecord, hasRule,
            viewModel.roleType, viewModel.chooseId,
            defaultType: monthPunchRecord.isWeekend()
                ? ShowType.NO_RULE
                : ShowType.ABNORMAL);
      },
    );
  }

  ///缺勤
  _absenceText(MonthPunchRecordBean monthPunchRecord) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: monthPunchRecord.isWeekend()
            ? Container()
            : monthPunchRecord.absencecnt > 0
                ? Text(
                    "${monthPunchRecord.absencecnt}人",
                    style: TextStyle(color: COLOR_ABSENCE, fontSize: 12),
                  )
                : _noDataText(),
      ),
      onTap: () {
        if (monthPunchRecord == null) {
          return;
        }
        if (monthPunchRecord.isWeekend()) {
          if (!canToDetail(monthPunchRecord)) {
            return;
          }
        } else {
          if (monthPunchRecord.absencecnt == 0) {
            return;
          }
        }
        DailyDetailListPage.navigatorPush(
          context,
          monthPunchRecord,
          hasRule,
          viewModel.roleType,
          viewModel.chooseId,
          defaultType: monthPunchRecord.isWeekend()
              ? ShowType.NO_RULE
              : ShowType.ABSENCE,
        );
      },
    );
  }

  ///无规则时
  _noRuleText(MonthPunchRecordBean monthPunchRecord, String roleType) {
    int num = 0;
    switch (roleType) {
      case TYPE_STAFF:
        num = monthPunchRecord?.staffcnt ?? 0;
        break;
      case TYPE_STU:
        num = monthPunchRecord?.stdcnt ?? 0;
        break;
      case TYPE_PARENT:
        num = monthPunchRecord?.parentcnt ?? 0;
        break;
      default:
        num = monthPunchRecord?.staffcnt ?? 0;
        break;
    }
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: monthPunchRecord == null
            ? 0
            : (num > 0)
                ? Text(
                    "$num人",
                    style: TextStyle(
                        color: monthPunchRecord.isWeekend()
                            ? COLOR_REST
                            : COLOR_NORMAL,
                        fontSize: 12),
                  )
                : monthPunchRecord.isWeekend() ? SizedBox() : _noDataText(),
      ),
      onTap: () {
        if (!canToDetail(monthPunchRecord)) {
          return;
        }
        DailyDetailListPage.navigatorPush(context, monthPunchRecord, hasRule,
            viewModel.roleType, viewModel.chooseId,
            defaultType: ShowType.NO_RULE);
      },
    );
  }

  ///多身份时的刷脸数
  _multiFaceCntText(MonthPunchRecordBean monthPunchRecord, String roleType) {
    int num = 0;
    switch (roleType) {
      case TYPE_STAFF:
        num = monthPunchRecord?.staffcnt ?? 0;
        break;
      case TYPE_STU:
        num = monthPunchRecord?.stdcnt ?? 0;
        break;
      case TYPE_PARENT:
        num = monthPunchRecord?.parentcnt ?? 0;
        break;
    }
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: num > 0
            ? Text(
                "$num人",
                style: TextStyle(color: COLOR_NORMAL, fontSize: 12),
              )
            : monthPunchRecord.isWeekend() ? SizedBox() : _noDataText(),
      ),
      onTap: () {
        if (monthPunchRecord == null) {
          return;
        }
        if (monthPunchRecord.isWeekend()) {
          if (!canToDetail(monthPunchRecord)) {
            return;
          }
        } else {
          if (monthPunchRecord.normalcnt == 0) {
            return;
          }
        }
        DailyDetailListPage.navigatorPush(
            context, monthPunchRecord, hasRule, roleType, viewModel.chooseId,
            defaultType: monthPunchRecord.isWeekend()
                ? ShowType.NO_RULE
                : ShowType.NORMAL);
      },
    );
  }

  ///是否能进详情
  bool canToDetail(MonthPunchRecordBean monthPunchRecord) {
    if (monthPunchRecord == null) {
      return false;
    }
    if(hasRule){
      //有规则
      if (monthPunchRecord.isWeekend()) {
        return monthPunchRecord.facecnt > 0;
      }
      return (monthPunchRecord.abnormalcnt > 0 ||
          monthPunchRecord.normalcnt > 0 ||
          monthPunchRecord.absencecnt > 0);
    }else {
      //无规则
      if (UserRepository.getInstance().isOrganization()) {
        if (viewModel.roleType != null) {
          //已筛选
          if (viewModel.roleType == TYPE_STAFF) {
            return monthPunchRecord.staffcnt > 0;
          }
          if (viewModel.roleType == TYPE_PARENT) {
            return monthPunchRecord.parentcnt > 0;
          }
          if (viewModel.roleType == TYPE_STU) {
            return monthPunchRecord.stdcnt > 0;
          }
        }
      } else {
        return monthPunchRecord.staffcnt > 0;
      }
    }
    return false;

  }

  ///标题条目栏
  getSubjectBar() {
    if (hasRule) {
      // 有规则
      if (UserRepository.getInstance().isOrganization()) {
        //机构
        //未筛选
        if (viewModel.roleType == null) {
          return _subjBarHasRule();
        } else {
          if (TYPE_PARENT == viewModel.roleType ||
              TYPE_STU == viewModel.roleType) {
            return _subjBarHasRuleHasFilter();
          }
          return _subjBarHasRule();
        }
      } else {
        //企业
        return _subjBarHasRule();
      }
    } else {
      //无规则
      if (UserRepository.getInstance().isOrganization()) {
        if (viewModel.roleType == null) {
          //未筛选
          // 员工 学员 家长  详情
          return _subjBarNoRule();
        } else {
          //已筛选
          //刷脸人数
          return _subjBarNoRuleHasFilter(
              title: viewModel.roleType == TYPE_STAFF
                  ? '员工'
                  : viewModel.roleType == TYPE_PARENT ? '家长' : '学员');
        }
      } else {
        return _subjBarNoRuleHasFilter();
      }
    }
  }

  ///显示数字栏
  getNumberWidget(MonthPunchRecordBean monthPunchRecord) {
    if (!hasRule) {
      //无规则
      if (UserRepository.getInstance().isOrganization()) {
        //机构
        if (viewModel.roleType != null) {
          //无规则已筛选 某一类型的刷脸数
          return _noRuleText(monthPunchRecord, viewModel.roleType);
        } else {
          //无规则未筛选 员工 学员 家长 刷脸数
          return _noRuleMultiItem(monthPunchRecord);
        }
      } else {
        //企业 仅员工
        return _noRuleText(monthPunchRecord, viewModel.roleType);
      }
    } else {
      //有规则
      if (UserRepository.getInstance().isOrganization()) {
        //机构
        if (TYPE_PARENT == viewModel.roleType ||
            TYPE_STU == viewModel.roleType) {
          return _hasRuleNoRuleTodayItem(monthPunchRecord);
        }
        if (monthPunchRecord.hasRuleToday) {
          if (monthPunchRecord.isToday()) {
            return _hasRuleIsTodayItem(monthPunchRecord);
          }
          return _hasRuleItem(monthPunchRecord);
        } else {
          //有规则且这一天没规则
          return _hasRuleNoRuleTodayItem(monthPunchRecord);
        }
      } else {
        //企业
        if (monthPunchRecord.hasRuleToday) {
          if (monthPunchRecord.isToday()) {
            return _hasRuleIsTodayItem(monthPunchRecord);
          }
          return _hasRuleItem(monthPunchRecord);
        } else {
          //有规则且这一天没规则
          return _hasRuleNoRuleTodayItem(monthPunchRecord);
        }
      }
    }
  }

  ///无规则未筛选的标题内容  日期｜员工｜学员｜ 家长 ｜ 详情
  _subjBarNoRule() {
    return DefaultTextStyle.merge(
      style: TextStyle(
          fontSize: 12,
          color: ThemeRepository.getInstance().getHintGreenColor_5E687C()),
      child: Row(
        children: <Widget>[
          Container(
            width: 80,
            height: 30,
            color: COLOR_DATE,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 14),
            child: Text(
              "日期",
            ),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Text(
                    "员工",
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "学员",
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "家长",
                  ),
                ),
              )
            ],
          )),
          //详情列
          Container(
            width: 55,
            alignment: Alignment.center,
            child: Text("详情"),
          )
        ],
      ),
    );
  }

  ///无规则 已筛选的标题内容 日期｜刷脸人数｜详情
  _subjBarNoRuleHasFilter({String title}) {
    return DefaultTextStyle.merge(
      style: TextStyle(
          fontSize: 12,
          color: ThemeRepository.getInstance().getHintGreenColor_5E687C()),
      child: Row(
        children: <Widget>[
          Container(
            width: 80,
            height: 30,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 14),
            color: COLOR_DATE,
            child: Text(
              "日期",
            ),
          ),
          Expanded(
              child: Center(
            child: Text(title ?? "刷脸人数"),
          )),
          //详情列
          Container(
            width: 55,
            alignment: Alignment.center,
            child: Text("详情"),
          )
        ],
      ),
    );
  }

  ///无规则 已筛选的标题内容 日期｜刷脸人数｜详情
  _subjBarHasRuleHasFilter({String title}) {
    return DefaultTextStyle.merge(
      style: TextStyle(
          fontSize: 12,
          color: ThemeRepository.getInstance().getHintGreenColor_5E687C()),
      child: Row(
        children: <Widget>[
          Container(
            width: 80,
            height: 30,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 14),
            color: COLOR_DATE,
            child: Text(
              "日期",
            ),
          ),
          Expanded(
              child: Center(
            child: Text(title ?? "刷脸人数"),
          )),
          //详情列
          Container(
            width: 55,
            alignment: Alignment.center,
            child: Text("详情"),
          )
        ],
      ),
    );
  }

  ///有规则时的标题内容
  _subjBarHasRule() {
    return DefaultTextStyle.merge(
      style: TextStyle(fontSize: 12, color: COLOR_TEXT_DEFAULT),
      child: Row(
        children: <Widget>[
          Container(
            width: 80,
            height: 30,
            color: COLOR_DATE,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 14),
            child: Text(
              "日期",
            ),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Text(
                    "正常",
                    style: TextStyle(color: COLOR_NORMAL),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "异常",
                    style: TextStyle(color: COLOR_ABNORMAL),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "缺勤",
                    style: TextStyle(color: COLOR_ABSENCE),
                  ),
                ),
              )
            ],
          )),
          //详情列
          Container(
            width: 55,
            alignment: Alignment.center,
            child: Text("详情"),
          )
        ],
      ),
    );
  }

  ///机构无规则未筛选  刷脸数 员工 学员 家长
  _noRuleMultiItem(MonthPunchRecordBean monthPunchRecord) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        //员工
        Expanded(child: _multiFaceCntText(monthPunchRecord, TYPE_STAFF)),
        //学员
        Expanded(
          child: _multiFaceCntText(monthPunchRecord, TYPE_STU),
        ),
        //家长
        Expanded(child: _multiFaceCntText(monthPunchRecord, TYPE_PARENT))
      ],
    );
  }

  ///有规则 且这一天有规则
  _hasRuleItem(MonthPunchRecordBean monthPunchRecord) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        //正常
        Expanded(child: _normalText(monthPunchRecord)),
        //异常
        Expanded(
          child: _abnormalText(monthPunchRecord),
        ),
        //缺勤
        Expanded(child: _absenceText(monthPunchRecord))
      ],
    );
  }

  ///有规则但今日无规则 123人刷脸
  _hasRuleNoRuleTodayItem(MonthPunchRecordBean monthPunchRecord) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: monthPunchRecord == null
            ? 0
            : (monthPunchRecord.facecnt > 0)
                ? Text(
                    "${monthPunchRecord.facecnt}人刷脸",
                    style: TextStyle(
                        color: monthPunchRecord.isWeekend()
                            ? COLOR_REST
                            : COLOR_NORMAL,
                        fontSize: 12),
                  )
                : _noDataText(),
      ),
      onTap: () {
        if (!canToDetail(monthPunchRecord)) {
          return;
        }
        DailyDetailListPage.navigatorPush(context, monthPunchRecord, hasRule,
            viewModel.roleType, viewModel.chooseId,
            defaultType: ShowType.NO_RULE);
      },
    );
  }

  ///有规则且今日  123人刷脸
  _hasRuleIsTodayItem(MonthPunchRecordBean monthPunchRecord) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Center(
        child: monthPunchRecord == null
            ? 0
            : (monthPunchRecord.getTotalCnt() > 0)
                ? Text(
                    "${monthPunchRecord.getTotalCnt()}人刷脸",
                    style: TextStyle(
                        color: monthPunchRecord.isWeekend()
                            ? COLOR_REST
                            : COLOR_NORMAL,
                        fontSize: 12),
                  )
                : _noDataText(),
      ),
      onTap: () {
        if (!canToDetail(monthPunchRecord)) {
          return;
        }
        DailyDetailListPage.navigatorPush(context, monthPunchRecord, false,
            viewModel.roleType, viewModel.chooseId,
            defaultType: ShowType.NO_RULE);
      },
    );
  }

  @override
  bool get wantKeepAlive => true;

  ///是否可以显示详情
  canShowDetail(MonthPunchRecordBean bean) {
    if (hasRule) {
      ///有考勤规则
      ///当天有考勤规则
    } else {
      ///无考勤规则
      if (bean.isWeekend()) {
        ///休息日
        if (viewModel.roleType != null) {
          return (bean?.staffcnt ?? 0) > 0;
        } else {
          return ((bean?.staffcnt ?? 0) > 0) ||
              ((bean?.parentcnt ?? 0) > 0) ||
              ((bean?.stdcnt ?? 0) > 0);
        }
      }
    }

    return true;
  }
}

import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/has_rule_date_punch_in_response.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_log_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';
import 'bean/check_in_rec_by_month_response.dart' as CheckInMonthData;
import 'bean/no_rule_date_punch_in_response.dart';

class CheckInRecNumViewModel extends BaseViewModel {
  ValueNotifier<CheckInMonthData.DataBean> monthDataNotifier;
  String roleType;
  String chooseId;
  int year, month;

  CheckInRecNumViewModel(BaseState<StatefulWidget> state, this.roleType)
      : super(state) {
    monthDataNotifier = ValueNotifier(null);
  }

  ///获取无规则的某一月的数据
  void queryNoRuleMonthRecData() {
    int queryYear = year;
    int queryMonth = month;
    final String sDateTime =
        ((DateTime(queryYear, queryMonth, 1).millisecondsSinceEpoch) / 1000)
            .floor()
            .toString();
    loading(true);
    LogUtils.e('CheckInRecNumViewModel :获取无规则的某一月的数据\n$queryYear $queryMonth $roleType $chooseId');
    VgHttpUtils.get(NetApi.APP_NO_RULE_PUNCH_IN_MONTH_DATA,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "time": sDateTime,
          if (StringUtils.isNotEmpty(chooseId)) "chooseId": chooseId,
          if (StringUtils.isNotEmpty(chooseId)) "type": roleType ?? '',
        },
        callback: BaseCallback(onSuccess: (val) {
          NoRuleDatePunchInResponse bean =
              NoRuleDatePunchInResponse.fromMap(val);

          CheckInMonthData.DataBean monthDataBean = CheckInMonthData.DataBean();

          if (bean?.data?.noRuleMonthRecord != null) {
            monthDataBean.monthPunchRecord = bean.data.noRuleMonthRecord;
          }

          monthDataNotifier?.value = monthDataBean;
          loading(false);
        }, onError: (msg) {
          loading(false);
          monthDataNotifier?.value = null;
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  ///获取有规则的某一月的数据
  void queryRuleMonthRecData() {
    final String sDateTime = getMonthMilliseconds();
    loading(true);
    int queryYear = year;
    int queryMonth = month;
    LogUtils.e('CheckInRecNumViewModel :获取有规则的某一月的数据\n$queryYear $queryMonth $roleType $chooseId');

    VgHttpUtils.get(NetApi.APP_HAS_RULE_PUNCH_IN_MONTH_DATA,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "time": sDateTime,
          "type": roleType ?? '',
          if (StringUtils.isNotEmpty(chooseId)) "chooseId": chooseId,
        },
        callback: BaseCallback(onSuccess: (val) {
          CheckInMonthData.DataBean monthDataBean = CheckInMonthData.DataBean();
          HasRuleDatePunchInResponse bean =
          HasRuleDatePunchInResponse.fromMap(val);

          if (bean?.data?.haveRuleMonthRecord != null) {
            monthDataBean.monthPunchRecord = bean.data.haveRuleMonthRecord;
          }
          monthDataBean.roleType=roleType;
          monthDataNotifier?.value = monthDataBean;
          loading(false);
        }, onError: (msg) {
          loading(false);
          monthDataNotifier?.value = null;
          VgToastUtils.toast(AppMain.context, msg);
        }));
  }

  String getMonthMilliseconds() {
    return ((DateTime(year, month, 1).millisecondsSinceEpoch) / 1000)
        .floor()
        .toString();
  }

  ///设置当前日期
  void setDate(int year, int month) {
    this.year = year;
    this.month = month;
  }

  ///设置选择id
  void setChooseId(String chooseId) {
    this.chooseId = chooseId;
  }

  ///设置当前身份 无规则是按机构 显示三种身份 此时为null 当筛选后显示特定如员工
  void setRoleType(String type) {
    this.roleType = type;
  }
}

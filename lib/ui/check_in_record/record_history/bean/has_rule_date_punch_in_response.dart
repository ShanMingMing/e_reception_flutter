import 'check_in_rec_by_month_response.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"haveRuleMonthRecord":[{"date":"2021-04-01","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-02","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-03","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-04","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-05","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-06","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-07","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-08","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-09","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-10","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-11","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-12","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-13","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-14","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-15","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-16","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-17","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-18","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-19","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-20","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-21","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-22","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-23","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-24","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-25","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-26","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-27","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-28","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-29","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-30","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"}]}
/// extra : null

///有考勤规则的打卡记录
class HasRuleDatePunchInResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static HasRuleDatePunchInResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HasRuleDatePunchInResponse hasRuleDatePunchInResponseBean = HasRuleDatePunchInResponse();
    hasRuleDatePunchInResponseBean.success = map['success'];
    hasRuleDatePunchInResponseBean.code = map['code'];
    hasRuleDatePunchInResponseBean.msg = map['msg'];
    hasRuleDatePunchInResponseBean.data = DataBean.fromMap(map['data']);
    hasRuleDatePunchInResponseBean.extra = map['extra'];
    return hasRuleDatePunchInResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// haveRuleMonthRecord : [{"date":"2021-04-01","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-02","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-03","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-04","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-05","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-06","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-07","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-08","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-09","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-10","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-11","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-12","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-13","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-14","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-15","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-16","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-17","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-18","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-19","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-20","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-21","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-22","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-23","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-24","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"00"},{"date":"2021-04-25","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-26","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-27","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-28","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-29","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"},{"date":"2021-04-30","offdutycnt":0,"normalcnt":0,"rule":"01","abnormalcnt":0,"rest":"01"}]

class DataBean {
  List<MonthPunchRecordBean> haveRuleMonthRecord;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.haveRuleMonthRecord = List()..addAll(
      (map['haveRuleMonthRecord'] as List ?? []).map((o) => MonthPunchRecordBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "haveRuleMonthRecord": haveRuleMonthRecord,
  };
}

/// date : "2021-04-01"
/// offdutycnt : 0
/// normalcnt : 0
/// rule : "01"
/// abnormalcnt : 0
/// rest : "01"

class HaveRuleMonthRecordBean {
  String date;
  int offdutycnt;
  int normalcnt;
  String rule;
  int abnormalcnt;
  String rest;

  static HaveRuleMonthRecordBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HaveRuleMonthRecordBean haveRuleMonthRecordBean = HaveRuleMonthRecordBean();
    haveRuleMonthRecordBean.date = map['date'];
    haveRuleMonthRecordBean.offdutycnt = map['offdutycnt'];
    haveRuleMonthRecordBean.normalcnt = map['normalcnt'];
    haveRuleMonthRecordBean.rule = map['rule'];
    haveRuleMonthRecordBean.abnormalcnt = map['abnormalcnt'];
    haveRuleMonthRecordBean.rest = map['rest'];
    return haveRuleMonthRecordBean;
  }

  Map toJson() => {
    "date": date,
    "offdutycnt": offdutycnt,
    "normalcnt": normalcnt,
    "rule": rule,
    "abnormalcnt": abnormalcnt,
    "rest": rest,
  };
}
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"monthPunchRecord":[{"date":"2021-03-01","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-02","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-03","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-04","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-05","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-06","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-07","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-08","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-09","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-10","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-11","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-12","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-13","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-14","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-15","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-16","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-17","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-18","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-19","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-20","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-21","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-22","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-23","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0}],"punchRule":false}
/// extra : null

class CheckInRecByMonthResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CheckInRecByMonthResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecByMonthResponse checkInRecByMonthResponseBean =
        CheckInRecByMonthResponse();
    checkInRecByMonthResponseBean.success = map['success'];
    checkInRecByMonthResponseBean.code = map['code'];
    checkInRecByMonthResponseBean.msg = map['msg'];
    checkInRecByMonthResponseBean.data = DataBean.fromMap(map['data']);
    checkInRecByMonthResponseBean.extra = map['extra'];
    return checkInRecByMonthResponseBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// monthPunchRecord : [{"date":"2021-03-01","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-02","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-03","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-04","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-05","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-06","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-07","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-08","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-09","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-10","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-11","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-12","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-13","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-14","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-15","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-16","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-17","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-18","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-19","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-20","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-21","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-22","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0},{"date":"2021-03-23","absencecnt":0,"normalcnt":0,"facecnt":0,"abnormalcnt":0}]
/// punchRule : false

class DataBean {
  List<MonthPunchRecordBean> monthPunchRecord;
  bool punchRule;
  RuleTimeBean ruleTime;
  //页面角色类型
  String roleType;

  //当前月
  int month;

  //当前年
  int year;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.monthPunchRecord = List()
      ..addAll((map['monthPunchRecord'] as List ?? [])
          .map((o) => MonthPunchRecordBean.fromMap(o)));
    dataBean.punchRule = map['punchRule'];
    dataBean.ruleTime = RuleTimeBean.fromMap(map['ruleTime']);
    return dataBean;
  }

  Map toJson() => {
        "monthPunchRecord": monthPunchRecord,
        "punchRule": punchRule,
      };
}

class RuleTimeBean {
  var offdutytime;
  var ondutytime;
  String arid;

  static RuleTimeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RuleTimeBean ruleTimeBean = RuleTimeBean();
    ruleTimeBean.offdutytime = map['offdutytime'];
    ruleTimeBean.ondutytime = map['ondutytime'];
    ruleTimeBean.arid = map['arid'];
    return ruleTimeBean;
  }

  Map toJson() =>
      {"offdutytime": offdutytime, "ondutytime": ondutytime, "arid": arid};
}

/// date : "2021-03-01"
/// absencecnt : 0
/// normalcnt : 0
/// facecnt : 0
/// abnormalcnt : 0

class MonthPunchRecordBean {
  String date;

  ///有规则时显示出勤请假缺勤
  int absencecnt = 0;
  int normalcnt = 0;
  int facecnt = 0;
  int abnormalcnt = 0;
  String rest;

  /// 无规则时显示的学员、家长、员工考勤数
  int staffcnt;
  int stdcnt;
  int parentcnt;

  ///今日有规则 00 有
  bool hasRuleToday = false;

  ///全部数量
  getTotalCnt() {
    DateTime dateTime = DateTime.tryParse(date);
    if (DateUtil.isToday(dateTime.millisecondsSinceEpoch)) {
      return normalcnt ?? 0;
    }
    return (normalcnt ?? 0) + (abnormalcnt ?? 0);
  }

  ///是否是今天之后的日期
  bool nextDays() {
    DateTime dateTime = DateTime.tryParse(date);
    return dateTime?.isAfter(DateTime.now());
  }

  ///该日期是周末
  bool isWeekend() {
    if (rest == null) {
      DateTime dateTime = DateTime.tryParse(date);
      return dateTime.weekday == 6 || dateTime.weekday == 7;
    } else {
      return "00" == rest;
    }
  }

  /// 日期 转 日
  getDateFormat() {
    return DateUtil.formatDateStr(date, format: "dd");
  }

  /// 日期 转 月日
  getDateMMddFormat() {
    return DateUtil.formatDateStr(date, format: "MM/dd");
  }

  /// 日期转星期
  getDateWeek() {
    DateTime dateTime = DateTime.tryParse(date);
    return DateUtil.getZHWeekDay(dateTime);
  }

  static MonthPunchRecordBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MonthPunchRecordBean monthPunchRecordBean = MonthPunchRecordBean();
    monthPunchRecordBean.date = map['date'];
    monthPunchRecordBean.absencecnt = map['absencecnt'];
    monthPunchRecordBean.normalcnt = map['normalcnt'];
    monthPunchRecordBean.abnormalcnt = map['abnormalcnt'];
    monthPunchRecordBean.rest = map['rest'];
    monthPunchRecordBean.staffcnt = map['staffcnt'];
    monthPunchRecordBean.stdcnt = map['stdcnt'];
    monthPunchRecordBean.parentcnt = map['parentcnt'];
    monthPunchRecordBean.hasRuleToday=(map['rule']?.toString()?.contains('00')??false);
    ///刷脸数默认取出勤的
    monthPunchRecordBean.facecnt= map['normalcnt'];

    return monthPunchRecordBean;
  }

  Map toJson() => {
        "date": date,
        "absencecnt": absencecnt,
        "normalcnt": normalcnt,
        "facecnt": facecnt,
        "abnormalcnt": abnormalcnt,
        "rest": rest,
        "staffcnt": staffcnt,
        "stdcnt": stdcnt,
        "parentcnt": parentcnt,
      };

  bool isToday() {
    DateTime dateTime = DateTime.tryParse(date);
    return DateUtil.isToday(dateTime.millisecondsSinceEpoch);
  }
}

import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/check_in_rec_by_month_response.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"noRuleMonthRecord":[{"date":"2021-05-01","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-02","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-03","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-04","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-05","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-06","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-07","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-08","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-09","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-10","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-11","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-12","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-13","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-14","staffcnt":0,"stdcnt":0,"parentcnt":0}]}
/// extra : null
///无规则单月考勤记录
class NoRuleDatePunchInResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static NoRuleDatePunchInResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    NoRuleDatePunchInResponse noRuleDatePunchInResponseBean =
        NoRuleDatePunchInResponse();
    noRuleDatePunchInResponseBean.success = map['success'];
    noRuleDatePunchInResponseBean.code = map['code'];
    noRuleDatePunchInResponseBean.msg = map['msg'];
    noRuleDatePunchInResponseBean.data = DataBean.fromMap(map['data']);
    noRuleDatePunchInResponseBean.extra = map['extra'];
    return noRuleDatePunchInResponseBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// noRuleMonthRecord : [{"date":"2021-05-01","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-02","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-03","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-04","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-05","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-06","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-07","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-08","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-09","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-10","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-11","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-12","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-13","staffcnt":0,"stdcnt":0,"parentcnt":0},{"date":"2021-05-14","staffcnt":0,"stdcnt":0,"parentcnt":0}]

class DataBean {
  List<MonthPunchRecordBean> noRuleMonthRecord;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.noRuleMonthRecord = List()
      ..addAll((map['noRuleMonthRecord'] as List ?? [])
          .map((o) => MonthPunchRecordBean.fromMap(o)));
    return dataBean;
  }

  Map toJson() => {
        "noRuleMonthRecord": noRuleMonthRecord,
      };
}

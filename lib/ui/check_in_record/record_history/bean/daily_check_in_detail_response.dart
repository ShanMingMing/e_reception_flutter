import 'package:e_reception_flutter/ui/punch_in_rule/bean/punch_in_rule_bean.dart';

import 'check_in_rec_by_month_response.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"firsttime":1616460069,"lasttime":1616496364,"putpicurl":"","name":"赵丽颖","fuid":"c408631dabbe402a8ab47cc2878b21fe","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396440082-Unknown_2f5f124c-f058-49a7-8ad8-fd51e284addf_.jpg"},{"firsttime":1616460072,"lasttime":1616496367,"putpicurl":"","name":"杨幂","fuid":"3bb3c187e9bf41ceb3d7e4064e650e62","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396447018-Unknown_bfe926f8-f247-4d95-8c04-6e98efafc5e5_.jpg"},{"firsttime":1616460075,"lasttime":1616496369,"putpicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg","name":"陈伟霆","fuid":"1ca2069c98be43728135abf44f2e56b9","napicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg"},{"firsttime":1616460052,"lasttime":1616496362,"putpicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","name":"李大红呀","fuid":"28d27e67a4f44984924d6b13237bb300","napicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg"},{"firsttime":1616460077,"lasttime":1616496370,"putpicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg","name":"易烊千玺24号改成非员工","fuid":"0fb7f9c4efda4f9f8833c22268da802f","napicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg"},{"firsttime":1616460079,"lasttime":1616496372,"putpicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg","name":"刘亦菲","fuid":"f5ac98cd88f2404a936bfe9729a656ff","napicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg"}],"total":6,"size":100,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class DailyCheckInDetailResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static DailyCheckInDetailResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DailyCheckInDetailResponse dailyCheckInDetailResponseBean =
        DailyCheckInDetailResponse();
    dailyCheckInDetailResponseBean.success = map['success'];
    dailyCheckInDetailResponseBean.code = map['code'];
    dailyCheckInDetailResponseBean.msg = map['msg'];
    dailyCheckInDetailResponseBean.data = DataBean.fromMap(map['data']);
    dailyCheckInDetailResponseBean.extra = map['extra'];
    return dailyCheckInDetailResponseBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };
}

/// page : {"records":[{"firsttime":1616460069,"lasttime":1616496364,"putpicurl":"","name":"赵丽颖","fuid":"c408631dabbe402a8ab47cc2878b21fe","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396440082-Unknown_2f5f124c-f058-49a7-8ad8-fd51e284addf_.jpg"},{"firsttime":1616460072,"lasttime":1616496367,"putpicurl":"","name":"杨幂","fuid":"3bb3c187e9bf41ceb3d7e4064e650e62","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396447018-Unknown_bfe926f8-f247-4d95-8c04-6e98efafc5e5_.jpg"},{"firsttime":1616460075,"lasttime":1616496369,"putpicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg","name":"陈伟霆","fuid":"1ca2069c98be43728135abf44f2e56b9","napicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg"},{"firsttime":1616460052,"lasttime":1616496362,"putpicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","name":"李大红呀","fuid":"28d27e67a4f44984924d6b13237bb300","napicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg"},{"firsttime":1616460077,"lasttime":1616496370,"putpicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg","name":"易烊千玺24号改成非员工","fuid":"0fb7f9c4efda4f9f8833c22268da802f","napicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg"},{"firsttime":1616460079,"lasttime":1616496372,"putpicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg","name":"刘亦菲","fuid":"f5ac98cd88f2404a936bfe9729a656ff","napicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg"}],"total":6,"size":100,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
        "page": page,
      };
}

/// records : [{"firsttime":1616460069,"lasttime":1616496364,"putpicurl":"","name":"赵丽颖","fuid":"c408631dabbe402a8ab47cc2878b21fe","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396440082-Unknown_2f5f124c-f058-49a7-8ad8-fd51e284addf_.jpg"},{"firsttime":1616460072,"lasttime":1616496367,"putpicurl":"","name":"杨幂","fuid":"3bb3c187e9bf41ceb3d7e4064e650e62","napicurl":"http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396447018-Unknown_bfe926f8-f247-4d95-8c04-6e98efafc5e5_.jpg"},{"firsttime":1616460075,"lasttime":1616496369,"putpicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg","name":"陈伟霆","fuid":"1ca2069c98be43728135abf44f2e56b9","napicurl":"http://etpic.we17.com/test/20210322151431_3803.jpg"},{"firsttime":1616460052,"lasttime":1616496362,"putpicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg","name":"李大红呀","fuid":"28d27e67a4f44984924d6b13237bb300","napicurl":"http://etpic.we17.com/test/20210226095738_2383.jpg"},{"firsttime":1616460077,"lasttime":1616496370,"putpicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg","name":"易烊千玺24号改成非员工","fuid":"0fb7f9c4efda4f9f8833c22268da802f","napicurl":"http://etpic.we17.com/test/20210322150542_7181.jpg"},{"firsttime":1616460079,"lasttime":1616496372,"putpicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg","name":"刘亦菲","fuid":"f5ac98cd88f2404a936bfe9729a656ff","napicurl":"http://etpic.we17.com/test/20210322150516_1205.jpg"}]
/// total : 6
/// size : 100
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<RecordsBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()
      ..addAll(
          (map['records'] as List ?? []).map((o) => RecordsBean.fromMap(o)));
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
        "records": records,
        "total": total,
        "size": size,
        "current": current,
        "orders": orders,
        "searchCount": searchCount,
        "pages": pages,
      };
}

/// firsttime : 1616460069
/// lasttime : 1616496364
/// putpicurl : ""
/// name : "赵丽颖"
/// fuid : "c408631dabbe402a8ab47cc2878b21fe"
/// napicurl : "http://etpic.we17.com/2a1de165b3754d1c8f6ab762a81a9d3c_1616396440082-Unknown_2f5f124c-f058-49a7-8ad8-fd51e284addf_.jpg"

class RecordsBean {
  int firsttime;
  int lasttime;
  String putpicurl;
  String name;
  String fuid;
  String napicurl;
  String delflg;

  //考勤规则
  String dayRuleStatus;
  String dutytime;
  String offdutytime;

  //上班弹性 秒
  int onFlextime;
  String daystatus;

  //下班弹性 秒
  int offFlextime;
  bool hasRule;

  ///早退
  bool isBeforeLastTime({RuleTimeBean ruleTimeBean}) {
    if (ruleTimeBean == null || ruleTimeBean.offdutytime == null) {
      return false;
    }
    DateTime lastTimeDateTime =
        DateTime.fromMillisecondsSinceEpoch(lasttime.round() * 1000);
   //下班时间
    DateTime offdutyDateTime =
        PunchInRuleBean.getFormatDateTime5min(offdutytime);
    //弹性
    if(offFlextime!=null&&offFlextime>0){
      offdutyDateTime=offdutyDateTime.subtract(Duration(seconds: offFlextime));
    }
    return lastTimeDateTime.hour < offdutyDateTime.hour ||
        (lastTimeDateTime.hour == offdutyDateTime.hour &&
            lastTimeDateTime.minute < offdutyDateTime.minute);
  }

  ///迟到
  bool isAfterFirstTime() {
    if (!(hasRule ?? false)) {
      return false;
    }
    DateTime firstTimeDateTime =
        DateTime.fromMillisecondsSinceEpoch(firsttime.round() * 1000);
    //上班时间
    DateTime ondutyDateTime = PunchInRuleBean.getFormatDateTime5min(dutytime);
    if (onFlextime != null && onFlextime > 0) {
      //加上弹性
      ondutyDateTime = ondutyDateTime.add(Duration(seconds: onFlextime));
    }
    return firstTimeDateTime.hour > ondutyDateTime.hour ||
        (firstTimeDateTime.hour == ondutyDateTime.hour &&
            firstTimeDateTime.minute > ondutyDateTime.minute);
  }

  static RecordsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RecordsBean recordsBean = RecordsBean();
    recordsBean.firsttime = map['firsttime'];
    recordsBean.lasttime = map['lasttime'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.name = map['name'];
    recordsBean.fuid = map['fuid'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.dayRuleStatus = map['dayRuleStatus'];
    recordsBean.dutytime = map['dutytime'];
    recordsBean.offdutytime = map['offdutytime'];
    recordsBean.onFlextime = map['onFlextime'];
    recordsBean.daystatus = map['daystatus'];
    recordsBean.offFlextime = map['offFlextime'];
    recordsBean.delflg = map['delflg'];
    //判断是否有规则
    recordsBean.hasRule = (map['dayRuleStatus'] == '00');
    return recordsBean;
  }

  Map toJson() => {
    "delflg":delflg,
        "firsttime": firsttime,
        "lasttime": lasttime,
        "putpicurl": putpicurl,
        "name": name,
        "fuid": fuid,
        "napicurl": napicurl,
      };
}

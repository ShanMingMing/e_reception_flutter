/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"ruleDayCnt":0}
/// extra : null

class HasRuleMonthResponse {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static HasRuleMonthResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HasRuleMonthResponse hasRuleMonthResponseBean = HasRuleMonthResponse();
    hasRuleMonthResponseBean.success = map['success'];
    hasRuleMonthResponseBean.code = map['code'];
    hasRuleMonthResponseBean.msg = map['msg'];
    hasRuleMonthResponseBean.data = DataBean.fromMap(map['data']);
    hasRuleMonthResponseBean.extra = map['extra'];
    return hasRuleMonthResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// ruleDayCnt : 0

class DataBean {
  int ruleDayCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.ruleDayCnt = map['ruleDayCnt'];
    return dataBean;
  }

  Map toJson() => {
    "ruleDayCnt": ruleDayCnt,
  };
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef Listener = Function(int index);

class TabLayoutWidget extends StatefulWidget {
  final int tabCount;
  final List<Widget> tabs;
  final Listener listener;
  final int index;
  final TextStyle selectTextStyle;
  final TextStyle normalTextStyle;
  final Decoration normalDecoration;
  final Decoration selectDecoration;
  final TabController controller;

  /// 是否显示指示器
  final bool showIndicator;

  TabLayoutWidget(
      {this.tabCount,
      this.tabs,
      this.listener,
      this.index,
      this.selectTextStyle,
      this.normalTextStyle,
      this.normalDecoration,
      this.selectDecoration,
      this.controller,
      this.showIndicator});

  @override
  State<StatefulWidget> createState() {
    return TabLayoutWidgetState();
  }
}

class TabLayoutWidgetState extends State<TabLayoutWidget> {
  int selectPos;

  @override
  void initState() {
    super.initState();
    selectPos = widget.index ?? 0;
//    widget.controller.animation.addListener(() {
//      print("animation:"+widget.controller.animation.value.toString());
//    });
    widget.controller?.addListener(() {
      setState(() {
        selectPos = widget?.controller?.index ?? 0;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: widget.tabs.map((child) {
            int index = widget.tabs.indexOf(child);
            return Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                child: DefaultTextStyle(
                  style: selectPos == index
                      ? widget.selectTextStyle
                      : widget.normalTextStyle,
                  child: Container(
                    alignment: Alignment.center,
                    decoration: selectPos == index
                        ? widget.selectDecoration
                        : widget.normalDecoration,
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: child,
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: (widget.showIndicator??false)
                              ? _indicator(selectPos == index)
                              : SizedBox(
                                  height: 1,
                                ),
                        )
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  selectPos = index;
                  setState(() {
                    if (widget.listener != null) {
                      widget.listener(index);
                    }
                  });
                },
              ),
            );
          }).toList()),
    );
  }

  void setSelect(int pos) {
    setState(() {
      selectPos = pos;
    });
  }

  _indicator(bool selected) {
    return Container(
      width: 20,
      height: 2,
      padding: EdgeInsets.only(bottom: 3),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: selected
              ? widget?.selectTextStyle?.color ?? Colors.white
              : Colors.transparent,
          borderRadius: BorderRadius.all(Radius.circular(2))),
    );
  }
}

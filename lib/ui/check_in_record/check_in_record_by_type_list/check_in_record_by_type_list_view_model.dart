import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/bean/check_in_record_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/check_in_record_index_page.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'check_in_record_by_type_list_widget.dart';

class CheckInRecordByTypeListViewModel extends BasePagerViewModel<
    CheckInRecordByTypeListItemBean, CheckInRecordByTypeListResponseBean> {
  final GroupInfoBean groupInfoBean;

  ValueNotifier<CheckInRecordByTypeListDataBean> infoValueNotifier;

  final CheckInRecordByTypeListWidgetState state;

  CheckInRecordByTypeListViewModel(this.state, this.groupInfoBean)
      : super(state) {
    infoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    infoValueNotifier?.dispose();
    super.onDisposed();
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    DateTime currentDateTime = CheckInRecordIndexPageState.of(state.context).currentDateTime;
    final String  sDateTime=(( DateTime(currentDateTime.year,currentDateTime.month,currentDateTime.day).millisecondsSinceEpoch )/ 1000).floor().toString();
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "groupid": groupInfoBean?.groupid ?? "",
      "hsn": state?.getHsn() ?? "",
      "isVisitor": groupInfoBean.visitor ?? "",
      "dayTime": sDateTime,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.POST;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appTodayGroupList";
  }

  @override
  CheckInRecordByTypeListResponseBean parseData(VgHttpResponse resp) {
    CheckInRecordByTypeListResponseBean vo =
        CheckInRecordByTypeListResponseBean.fromMap(resp?.data);
    loading(false);
    infoValueNotifier?.value = vo?.data;
    return vo;
  }
}

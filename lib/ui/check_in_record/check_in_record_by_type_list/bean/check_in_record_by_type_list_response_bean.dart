import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"groupPerson":"2","todayPerson":1,"page":{"records":[{"mintime":1611126889,"temperature":"0.0","signFuid":"dfb4d324a5d54726b5c566bac57fd8a3","maxtime":1611126983,"name":"李岁红","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1},"todayRecordCnt":"2"}
/// extra : null

class CheckInRecordByTypeListResponseBean
    extends BasePagerBean<CheckInRecordByTypeListItemBean> {
  bool success;
  String code;
  String msg;
  CheckInRecordByTypeListDataBean data;
  dynamic extra;

  static CheckInRecordByTypeListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordByTypeListResponseBean
        checkInRecordByTypeListResponseBeanBean =
        CheckInRecordByTypeListResponseBean();
    checkInRecordByTypeListResponseBeanBean.success = map['success'];
    checkInRecordByTypeListResponseBeanBean.code = map['code'];
    checkInRecordByTypeListResponseBeanBean.msg = map['msg'];
    checkInRecordByTypeListResponseBeanBean.data =
        CheckInRecordByTypeListDataBean.fromMap(map['data']);
    checkInRecordByTypeListResponseBeanBean.extra = map['extra'];
    return checkInRecordByTypeListResponseBeanBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0
      ? 1
      : data.page.current;

  ///得到List列表
  @override
  List<CheckInRecordByTypeListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() =>
      data?.page?.pages == null || data.page.pages <= 0 ? 1 : data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// groupPerson : "2"
/// todayPerson : 1
/// page : {"records":[{"mintime":1611126889,"temperature":"0.0","signFuid":"dfb4d324a5d54726b5c566bac57fd8a3","maxtime":1611126983,"name":"李岁红","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}
/// todayRecordCnt : "2"

class CheckInRecordByTypeListDataBean {
  String groupPerson;
  int todayPerson;
  PageBean page;
  String todayRecordCnt;

  static CheckInRecordByTypeListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordByTypeListDataBean dataBean =
        CheckInRecordByTypeListDataBean();
    dataBean.groupPerson = map['groupPerson'];
    dataBean.todayPerson = map['todayPerson'];
    dataBean.page = PageBean.fromMap(map['page']);
    dataBean.todayRecordCnt = map['todayRecordCnt'];
    return dataBean;
  }

  Map toJson() => {
        "groupPerson": groupPerson,
        "todayPerson": todayPerson,
        "page": page,
        "todayRecordCnt": todayRecordCnt,
      };
}

/// records : [{"mintime":1611126889,"temperature":"0.0","signFuid":"dfb4d324a5d54726b5c566bac57fd8a3","maxtime":1611126983,"name":"李岁红","napicurl":"http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg"}]
/// total : 1
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CheckInRecordByTypeListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()
      ..addAll((map['records'] as List ?? [])
          .map((o) => CheckInRecordByTypeListItemBean.fromMap(o)));
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
        "records": records,
        "total": total,
        "size": size,
        "current": current,
        "orders": orders,
        "searchCount": searchCount,
        "pages": pages,
      };
}

/// mintime : 1611126889
/// temperature : "0.0"
/// signFuid : "dfb4d324a5d54726b5c566bac57fd8a3"
/// maxtime : 1611126983
/// name : "李岁红"
/// napicurl : "http://etpic.we17.com/55a438b79b7c4cd3bf7fec1f603e774a_1611043053131-1611043052954Unknown_2101191557324_.jpg"

class CheckInRecordByTypeListItemBean {
  String fid;
  int mintime;
  String temperature;
  String signFuid;
  int maxtime;
  String name;
  String napicurl;
  String isSign;
  String putpicurl;

  static CheckInRecordByTypeListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordByTypeListItemBean recordsBean =
        CheckInRecordByTypeListItemBean();
    recordsBean.fid = map['fid'];
    recordsBean.mintime = map['mintime'];
    recordsBean.temperature = map['temperature'];
    recordsBean.signFuid = map['signFuid'];
    recordsBean.maxtime = map['maxtime'];
    recordsBean.name = map['name'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.isSign = map['isSign'];
    recordsBean.putpicurl = map['putpicurl'];
    return recordsBean;
  }

  Map toJson() => {
        "mintime": mintime,
        "temperature": temperature,
        "signFuid": signFuid,
        "maxtime": maxtime,
        "name": name,
        "napicurl": napicurl,
        "fid": fid,
        "isSign": isSign,
        "putpicurl":putpicurl,
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckInRecordByTypeListItemBean &&
          runtimeType == other.runtimeType &&
          signFuid == other.signFuid;

  @override
  int get hashCode => signFuid.hashCode;

  ///00
  bool isUnknow() {
    return isSign == "00";
  }

  @override
  String toString() {
    return 'CheckInRecordByTypeListItemBean{fid: $fid, mintime: $mintime, temperature: $temperature, signFuid: $signFuid, maxtime: $maxtime, name: $name, napicurl: $napicurl, isSign: $isSign}';
  }
}

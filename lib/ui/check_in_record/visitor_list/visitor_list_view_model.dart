import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:upgrade_flutter/upgrade_flutter.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class VisitorListViewModel extends BaseViewModel {
  ///忽略或删除刷脸用户
  static const String SINGLE_IGNORE_OR_DELETE_API =
     ServerApi.BASE_URL + "app/appIgnoreDeleteOneFaceUser";

  VisitorListViewModel(BaseState<StatefulWidget> state) : super(state);

  ///忽略一张脸
  void ignoreSingleFaceHttp(BuildContext context, String fid,VoidCallback onRefresh) {
    if (StringUtils.isEmpty(fid)) {
      VgToastUtils.toast(context, "人脸数据获取失败");
      return;
    }
    VgHttpUtils.post(SINGLE_IGNORE_OR_DELETE_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fid": fid ?? "",
          "flg": "00", //00忽略 01删除
        },
        callback: BaseCallback(onSuccess: (val) {
//刷新
          VgToastUtils.toast(context, "忽略成功");
          onRefresh?.call();
        }, onError: (msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  ///删除一张脸
  void deleteSingleFaceHttp(BuildContext context, String fid,VoidCallback onRefresh) {
    if (StringUtils.isEmpty(fid)) {
      VgToastUtils.toast(context, "人脸数据获取失败");
      return;
    }
    VgHttpUtils.post(SINGLE_IGNORE_OR_DELETE_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fid": fid ?? "",
          "flg": "01", //00忽略 01删除
        },
        callback: BaseCallback(onSuccess: (val) {
//刷新
          VgToastUtils.toast(context, "删除成功");
          onRefresh?.call();
        }, onError: (msg) {
          VgToastUtils.toast(context, msg);
        }));
  }
}

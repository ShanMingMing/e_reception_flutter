import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"visitorList":[{"firsttime":1616728799,"lasttime":1616728799,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728799205-Unknown_1ca74e7a-f0a2-4173-be3a-d420f4fc228d_.jpg","punchName":"用户9080"},{"firsttime":1616728370,"lasttime":1616728370,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728370066-Unknown_30c49a86-8806-460c-b7ea-61c9de3cdb69_.jpg","punchName":"用户2833"},{"firsttime":1616728346,"lasttime":1616728346,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728346010-Unknown_a4d26a2c-c124-4dad-906d-b71b7195dd47_.jpg","punchName":"用户2402"},{"firsttime":1616727991,"lasttime":1616727992,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727991970-Unknown_2a081d32-49c0-4d31-87b2-ea69be79582f_.jpg","punchName":"用户7408"},{"firsttime":1616727871,"lasttime":1616727948,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727871077-Unknown_b216e98e-89a3-4ece-8bd6-342744a9ae04_.jpg","punchName":"用户2970"},{"firsttime":1616727736,"lasttime":1616727736,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727736976-Unknown_9e3dfcc8-6709-4a62-9e61-3994e848c146_.jpg","punchName":"用户0740"},{"firsttime":1616118389,"lasttime":1616121604,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616118389632-Unknown_c3f32581-cfec-485f-88a1-433045bf097d_.jpg","punchName":"用户0694"},{"firsttime":1615973349,"lasttime":1616039848,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1615973166987-Unknown_6b6ee450-b051-4007-a176-7ab15d486f95_.jpg","punchName":"用户7673"},{"firsttime":1614578522,"lasttime":1614580013,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1614578522979-Unknown_38007b6e-4ff3-46c0-a37c-0fd7c117444a_.jpg","punchName":"用户4972"}]}
/// extra : null

class VisitorListResponseBean extends BasePagerBean<VisitorListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static VisitorListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    VisitorListResponseBean visitorListResponseBeanBean = VisitorListResponseBean();
    visitorListResponseBeanBean.success = map['success'];
    visitorListResponseBeanBean.code = map['code'];
    visitorListResponseBeanBean.msg = map['msg'];
    visitorListResponseBeanBean.data = DataBean.fromMap(map['data']);
    visitorListResponseBeanBean.extra = map['extra'];
    return visitorListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<VisitorListItemBean> getDataList() => data.visitorList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.visitorList = list?.cast();
  }
}

/// visitorList : [{"firsttime":1616728799,"lasttime":1616728799,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728799205-Unknown_1ca74e7a-f0a2-4173-be3a-d420f4fc228d_.jpg","punchName":"用户9080"},{"firsttime":1616728370,"lasttime":1616728370,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728370066-Unknown_30c49a86-8806-460c-b7ea-61c9de3cdb69_.jpg","punchName":"用户2833"},{"firsttime":1616728346,"lasttime":1616728346,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728346010-Unknown_a4d26a2c-c124-4dad-906d-b71b7195dd47_.jpg","punchName":"用户2402"},{"firsttime":1616727991,"lasttime":1616727992,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727991970-Unknown_2a081d32-49c0-4d31-87b2-ea69be79582f_.jpg","punchName":"用户7408"},{"firsttime":1616727871,"lasttime":1616727948,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727871077-Unknown_b216e98e-89a3-4ece-8bd6-342744a9ae04_.jpg","punchName":"用户2970"},{"firsttime":1616727736,"lasttime":1616727736,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616727736976-Unknown_9e3dfcc8-6709-4a62-9e61-3994e848c146_.jpg","punchName":"用户0740"},{"firsttime":1616118389,"lasttime":1616121604,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616118389632-Unknown_c3f32581-cfec-485f-88a1-433045bf097d_.jpg","punchName":"用户0694"},{"firsttime":1615973349,"lasttime":1616039848,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1615973166987-Unknown_6b6ee450-b051-4007-a176-7ab15d486f95_.jpg","punchName":"用户7673"},{"firsttime":1614578522,"lasttime":1614580013,"pictureUrl":"http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1614578522979-Unknown_38007b6e-4ff3-46c0-a37c-0fd7c117444a_.jpg","punchName":"用户4972"}]

class DataBean {
  List<VisitorListItemBean> visitorList;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.visitorList = List()..addAll(
      (map['visitorList'] as List ?? []).map((o) => VisitorListItemBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "visitorList": visitorList,
  };
}

/// firsttime : 1616728799
/// lasttime : 1616728799
/// pictureUrl : "http://etpic.we17.com/076cca3a75a84944aae0be750c9b629c_1616728799205-Unknown_1ca74e7a-f0a2-4173-be3a-d420f4fc228d_.jpg"
/// punchName : "用户9080"

class VisitorListItemBean {
  int firsttime;
  int lasttime;
  String pictureUrl;
  String punchName;

  String fid;

  static VisitorListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    VisitorListItemBean visitorListBean = VisitorListItemBean();
    visitorListBean.firsttime = map['firsttime'];
    visitorListBean.lasttime = map['lasttime'];
    visitorListBean.pictureUrl = map['pictureUrl'];
    visitorListBean.punchName = map['punchName'];
    visitorListBean.fid = map['fid'];
    return visitorListBean;
  }

  Map toJson() => {
    "firsttime": firsttime,
    "lasttime": lasttime,
    "pictureUrl": pictureUrl,
    "punchName": punchName,
    "fid": fid,
  };
}
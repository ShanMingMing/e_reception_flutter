import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_record/visitor_list/bean/visitor_list_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/visitor_list/visitor_list_page.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 访客列表项
///
/// @author: zengxiangxi
/// @createTime: 3/29/21 2:36 PM
/// @specialDemand:
class VisitorListItemWidget extends StatelessWidget {
  final VisitorListItemBean itemBean;
  final VoidCallback onTap;

  final ValueChanged<VisitorListItemBean> onFaceIgnore;
  final ValueChanged<VisitorListItemBean> onFaceDelete;

  const VisitorListItemWidget({Key key, this.itemBean, this.onTap, this.onFaceIgnore, this.onFaceDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        onTap?.call();
      },
      onLongPress: (){
        _pushMoreMenuDialog(context,itemBean);
      },
      child: Container(
        height: VISITOR_LIST_ITEM_HEIGHT,
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: _toMainRowWidget(context),
      ),
    );
  }

  Widget _toMainRowWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(StringUtils.isEmpty(itemBean?.pictureUrl)){
          return;
        }
        VgPhotoPreview.single(context, itemBean?.pictureUrl,loadingImageQualityType: ImageQualityType.middleDown);

      },
      child: Row(
        children: <Widget>[
          SizedBox(width: 15),
          _toHeadPicWidget(context),
          SizedBox(width: 9),
          Expanded(child: _toColumnWidget()),
          Image.asset(
            "images/go_ico.png",
            width: 6,
            color: Color(0xFF5E687C),
          ),
          SizedBox(width: 10),
          //标记按钮
          // _toMarkWidget(),
        ],
      ),
    );
  }

  Widget _toHeadPicWidget(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: VgCacheNetWorkImage(
        showOriginEmptyStr(itemBean?.pictureUrl) ?? "",
        width: 37,
        height: 37,
        imageQualityType: ImageQualityType.middleUp,
        defaultPlaceType: ImagePlaceType.head,
        defaultErrorType: ImageErrorType.head,
      ),
    );
  }

  Widget _toColumnWidget() {
    final bool isShowMax =
        VgToolUtils.isShowMaxTime(itemBean.firsttime, itemBean?.lasttime);

    return Container(
      height: 37,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CommonConstraintMaxWidthWidget(
                maxWidth: VgToolUtils.getScreenWidth() / 2,
                child: Text(
                  itemBean?.punchName ?? "-",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 15,
                      height: 1.2),
                ),
              ),
              Spacer(),
            ],
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text.rich(
              TextSpan(children: [
                TextSpan(
                    text:VgDateTimeUtils.getFormatTimeStr(itemBean?.firsttime,
                    yearOrMonthOrDaySplit: "-",
                    isShowCompleteYear: true,
                    isShowCompleteMonth: true,
                    isShowCompleteDay: true,
                    isShowCompleteHour: true,isShowCompleteMinute: true) ?? ""),
                TextSpan(
                    text: isShowMax ? " 至 " : "",
                    style: TextStyle(height: 1.3)),
                TextSpan(
                    text: isShowMax
                        ? "${VgToolUtils.getHourAndMinuteStr(itemBean?.lasttime) ?? "-"}"
                        : ""),
              ]),
              style: TextStyle(
                  color: (VgDateTimeUtils.isToday(itemBean?.firsttime) || VgDateTimeUtils.isToday(itemBean?.lasttime))
                      ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                      : Color(0xFF5E687C),
                  fontSize: 12),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toMarkWidget() {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: true,
        width: 60,
        height: 28,
        unSelectBgColor: Colors.transparent,
        selectedBgColor: Colors.transparent,
        // unSelectBoxBorder: Border.all(color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),width: 1),
        selectedBoxBorder: Border.all(
            color: ThemeRepository.getInstance()
                .getTextMinorGreyColor_808388()
                .withOpacity(0.5),
            width: 1),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 13,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 13,
        ),
        text: "标记",
      ),
    );
  }

  // String replaceName(){
  //   return itemBean?.punchName?.replaceFirst(new RegExp(r'用户'),"访客");
  // }

  ///弹出更多菜单弹窗
  void _pushMoreMenuDialog(
      BuildContext context, VisitorListItemBean itemBean) {
    Map<String, VoidCallback> menuMap = Map();
      menuMap['忽略'] = () {
        onFaceIgnore?.call(itemBean);
      };
      menuMap['删除'] = () {
        onFaceDelete?.call(itemBean);
      };

    CommonMoreMenuDialog.navigatorPushDialog(context, menuMap);
  }
}

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';

import '../../app_main.dart';
import 'bean/check_in_record_index_group_info_response_bean.dart';

class CheckInRecordIndexViewModel extends BaseViewModel{

  ValueNotifier<PlaceHolderStatusType> statusValueNotifier;
  ValueNotifier<CheckInRecordIndexGroupInfoDataBean> groupInfoValueNotifier;

  static const String TOP_GROUP_INFO_API = ServerApi.BASE_URL + "app/appTodayPunchVisitor";

  CheckInRecordIndexViewModel(BaseState<StatefulWidget> state) : super(state){
    groupInfoValueNotifier = ValueNotifier(null);
    statusValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
  }

  @override
  void onDisposed() {
    groupInfoValueNotifier?.dispose();
    super.onDisposed();
  }

  ///获取顶部信息请求
  void getTopInfoHttp(String hsn,DateTime currentDateTime){
    final String  sDateTime=(( DateTime(currentDateTime.year,currentDateTime.month,currentDateTime.day).millisecondsSinceEpoch )/ 1000).floor().toString();
    VgHttpUtils.post(TOP_GROUP_INFO_API,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "dayTime": sDateTime,
    },callback: BaseCallback(
        onSuccess: (val){
          CheckInRecordIndexGroupInfoResponseBean bean = CheckInRecordIndexGroupInfoResponseBean.fromMap(val);
          statusValueNotifier.value = null;
          groupInfoValueNotifier?.value = bean?.data;
        },
        onError: (msg){
          VgToastUtils.toast(AppMain.context, msg);
          statusValueNotifier.value = PlaceHolderStatusType.error;
        }
    ));
  }
}
/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"hsnInfo":[{"terminalName":"南京的终端呀呀呀哈哈","hsn":"ed5d18608a872ce8"},{"terminalName":"北京搜宝的终端","hsn":"lisuihongtest0113"},{"terminalName":"南京的终端12321呀呀呀哈哈","hsn":"4bcc02ed94667d35"},{"terminalName":"北京的终端233333333","hsn":"a69ab51225adaf8b"},{"terminalName":"北京搜宝1","hsn":"e57477b75c986715"},{"terminalName":"","hsn":"1ee7ba9ca715b8e3"}],"unSignCnt":57,"groupInfo":[{"groupName":"员工","groupid":"11e2bec33bf743aaae4522b51c022f32","cnt":0},{"groupName":"访客","groupid":"377750b3c2f14647b1be20bd3cad3ff9","cnt":0}],"faceCnt":0}
/// extra : null

class CheckInRecordIndexGroupInfoResponseBean {
  bool success;
  String code;
  String msg;
  CheckInRecordIndexGroupInfoDataBean data;
  dynamic extra;

  static CheckInRecordIndexGroupInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordIndexGroupInfoResponseBean checkInRecordIndexGroupInfoResponseBeanBean = CheckInRecordIndexGroupInfoResponseBean();
    checkInRecordIndexGroupInfoResponseBeanBean.success = map['success'];
    checkInRecordIndexGroupInfoResponseBeanBean.code = map['code'];
    checkInRecordIndexGroupInfoResponseBeanBean.msg = map['msg'];
    checkInRecordIndexGroupInfoResponseBeanBean.data = CheckInRecordIndexGroupInfoDataBean.fromMap(map['data']);
    checkInRecordIndexGroupInfoResponseBeanBean.extra = map['extra'];
    return checkInRecordIndexGroupInfoResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// hsnInfo : [{"terminalName":"南京的终端呀呀呀哈哈","hsn":"ed5d18608a872ce8"},{"terminalName":"北京搜宝的终端","hsn":"lisuihongtest0113"},{"terminalName":"南京的终端12321呀呀呀哈哈","hsn":"4bcc02ed94667d35"},{"terminalName":"北京的终端233333333","hsn":"a69ab51225adaf8b"},{"terminalName":"北京搜宝1","hsn":"e57477b75c986715"},{"terminalName":"","hsn":"1ee7ba9ca715b8e3"}]
/// unSignCnt : 57
/// groupInfo : [{"groupName":"员工","groupid":"11e2bec33bf743aaae4522b51c022f32","cnt":0},{"groupName":"访客","groupid":"377750b3c2f14647b1be20bd3cad3ff9","cnt":0}]
/// faceCnt : 0

class CheckInRecordIndexGroupInfoDataBean {
  List<HsnInfoBean> hsnInfo;
  int unSignCnt;
  List<GroupInfoBean> groupInfo;
  int faceCnt;
  int shouldAttendanceNum;

  static CheckInRecordIndexGroupInfoDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInRecordIndexGroupInfoDataBean dataBean = CheckInRecordIndexGroupInfoDataBean();
    dataBean.hsnInfo = List()..addAll(
      (map['hsnInfo'] as List ?? []).map((o) => HsnInfoBean.fromMap(o))
    );
    dataBean.unSignCnt = map['unSignCnt'];
    dataBean.groupInfo = List()..addAll(
      (map['groupInfo'] as List ?? []).map((o) => GroupInfoBean.fromMap(o))
    );
    dataBean.faceCnt = map['faceCnt'];
    dataBean.shouldAttendanceNum = map['shouldAttendanceNum'];
    return dataBean;
  }

  Map toJson() => {
    "hsnInfo": hsnInfo,
    "unSignCnt": unSignCnt,
    "groupInfo": groupInfo,
    "faceCnt": faceCnt,
    "shouldAttendanceNum": shouldAttendanceNum,
  };
}

/// groupName : "员工"
/// groupid : "11e2bec33bf743aaae4522b51c022f32"
/// cnt : 0

class GroupInfoBean {
  String groupName;
  String groupid;
  int cnt;
  String visitor;

  static GroupInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GroupInfoBean groupInfoBean = GroupInfoBean();
    groupInfoBean.groupName = map['groupName'];
    groupInfoBean.groupid = map['groupid'];
    groupInfoBean.cnt = map['cnt'];
    groupInfoBean.visitor = map['visitor'];
    return groupInfoBean;
  }

  Map toJson() => {
    "groupName": groupName,
    "groupid": groupid,
    "cnt": cnt,
    "visitor": visitor,
  };

  ///是否是访客
  ///访客：00；非访客 01;
  bool isVisitor(){
    return visitor == "00";
  }
}

/// terminalName : "南京的终端呀呀呀哈哈"
/// hsn : "ed5d18608a872ce8"

class HsnInfoBean {
  String terminalName;
  String hsn;

  static HsnInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnInfoBean hsnInfoBean = HsnInfoBean();
    hsnInfoBean.terminalName = map['terminalName'];
    hsnInfoBean.hsn = map['hsn'];
    return hsnInfoBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
  };

  @override
  String toString() {
    return 'HsnInfoBean{terminalName: $terminalName, hsn: $hsn}';
  }
}
import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 今日/访客-切换设备弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/22/21 2:00 PM
/// @specialDemand:
///

const String CHECK_IN_RCORD_DEFAULT_TERMINAL_NAME = "全部终端";
const String CHECK_IN_RCORD_DEFAULT_TERMINAL_HSN = "ALL";

class CheckInRecordSwitchDialog extends StatelessWidget {
  final List<HsnInfoBean> list;
  final HsnInfoBean selectedHsnBean;

  const CheckInRecordSwitchDialog({Key key, this.list, this.selectedHsnBean})
      : super(key: key);

  static HsnInfoBean getAllTerminalItemBean() {
    return HsnInfoBean()
      ..terminalName = CHECK_IN_RCORD_DEFAULT_TERMINAL_NAME
      ..hsn = CHECK_IN_RCORD_DEFAULT_TERMINAL_HSN;
  }

  ///弹出弹窗
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      List<HsnInfoBean> list, HsnInfoBean selectedHsnBean) {
    if (list == null || list.isEmpty) {
      VgToastUtils.toast(context, "无可切换设备");
      return null;
    }
    List<HsnInfoBean> copyList =
        list.map((HsnInfoBean element) => element).toList();
    if (copyList == null || copyList.isEmpty) {
      return null;
    }
    copyList?.insert(0, getAllTerminalItemBean());

    return VgDialogUtils.showCommonDialog<HsnInfoBean>(
        context: context,
        barrierColor: Colors.white.withOpacity(0),
        child: CheckInRecordSwitchDialog(
          list: copyList,
          selectedHsnBean: selectedHsnBean,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context);
      },
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          child: _toMainColumnWidget(context),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: ScreenUtils.getStatusBarH(context) + 44 + 35,
        ),
        _toListWidget(context),
        Expanded(
          child: Container(
            color: ColorConstants.BOTTOM_SHEET_BARRIER_COLOR,
          ),
        )
      ],
    );
  }

  Widget _toListWidget(BuildContext context) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      constraints: BoxConstraints(
        maxHeight: ScreenUtils.screenH(context) / 2,
      ),
      child: ScrollConfiguration(
        behavior: MyBehavior(),
        child: ListView.separated(
            itemCount: list?.length ?? 0,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            padding: const EdgeInsets.all(0),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return _toListItemWidget(context,list?.elementAt(index));
            },
            separatorBuilder: (BuildContext context, int index) {
              return Container(
                height: 0.5,
                color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                margin: const EdgeInsets.only(left: 15, right: 1),
              );
            }),
      ),
    );
  }

  Widget _toListItemWidget(BuildContext context,HsnInfoBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap:(){
        RouterUtils.pop(context,result: itemBean);
      },
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        alignment: Alignment.centerLeft,
        child: Text(
          itemBean?.terminalName ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: getTextColor(itemBean),
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  Color getTextColor(HsnInfoBean itemBean) {
    if (selectedHsnBean == null ||
        selectedHsnBean.hsn == null ||
        selectedHsnBean.hsn.isEmpty ||
        itemBean == null ||
        itemBean.hsn == null ||
        itemBean.hsn.isEmpty) {
      return ThemeRepository.getInstance().getTextMainColor_D0E0F7();
    }
    if(selectedHsnBean.hsn == itemBean.hsn){
      return ThemeRepository.getInstance().getPrimaryColor_1890FF();
    }
    return ThemeRepository.getInstance().getTextMainColor_D0E0F7();

  }
}

// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
//
// /// 打卡记录-topbar
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/8/21 2:50 PM
// /// @specialDemand:
// class CheckInRecordIndexTopBarWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return VgTopBarWidget(
//       isShowBack: false,
//       backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
//       centerWidget: Row(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//            Text(
//                      "全部4台终端",
//                      maxLines: 1,
//                      overflow: TextOverflow.ellipsis,
//                      style: TextStyle(
//                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                          fontSize: 17,
//                        height: 1.2
//                          ),
//                    ),
//           Icon(
//             Icons.keyboard_arrow_down,
//             size: 19,
//             color:ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//           )
//
//         ],
//       ),
//     );
//   }
// }

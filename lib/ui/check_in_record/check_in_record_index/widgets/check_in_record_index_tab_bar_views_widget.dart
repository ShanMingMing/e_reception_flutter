import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/check_in_record_by_type_list_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 打卡记录-项列表
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 6:11 PM
/// @specialDemand:
class CheckInRecordIndexTabBarViewsWidget extends StatefulWidget {
  final TabController tabController;
  final List<GroupInfoBean> tabsList;
  final String hsn;

  const CheckInRecordIndexTabBarViewsWidget({Key key, this.tabController, this.tabsList, this.hsn, }) : super(key: key);

  @override
  _CheckInRecordIndexTabBarViewsWidgetState createState() => _CheckInRecordIndexTabBarViewsWidgetState();
}

class _CheckInRecordIndexTabBarViewsWidgetState extends State<CheckInRecordIndexTabBarViewsWidget> {
  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: TabBarView(
        controller: widget?.tabController,
        physics: BouncingScrollPhysics(),
        children: List.generate(widget?.tabsList?.length ?? 0, (index){
          return CheckInRecordByTypeListWidget(groupInfoBean:widget?.tabsList?.elementAt(index),hsn:widget?.hsn);
        }),
      ),
    );
  }
}

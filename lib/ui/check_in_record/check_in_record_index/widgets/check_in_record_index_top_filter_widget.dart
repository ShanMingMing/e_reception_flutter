import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'check_in_record_switch_dialog.dart';

/// 打卡记录-过滤
///
/// @author: zengxiangxi
/// @createTime: 1/11/21 5:00 PM
/// @specialDemand:
class CheckInRecordIndexTopFilterWidget extends StatelessWidget {
  final CheckInRecordIndexGroupInfoDataBean groupInfoBean;
  final ValueChanged<HsnInfoBean> onChanged;
  final HsnInfoBean selectedHsnBean;
  CheckInRecordIndexTopFilterWidget({this.groupInfoBean, this.onChanged, this.selectedHsnBean});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async{
        HsnInfoBean result = await CheckInRecordSwitchDialog.navigatorPushDialog(context,groupInfoBean?.hsnInfo,selectedHsnBean);
        if(result == null){
          return;
        }
        if(onChanged == null){
          return;
        }
        onChanged(result);
      },
      child: Container(
        height: 35,
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 14,
            ),
             Text(
                       selectedHsnBean?.terminalName ?? "",
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                       style: TextStyle(
                           color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                           fontSize: 12,
                         height: 1.2
                           ),
                     ),
            Icon(
              Icons.arrow_drop_down,size: 16,
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
            ),
            Spacer(),
             Text(
                       "应出勤${groupInfoBean?.shouldAttendanceNum ?? 0}人",
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                       style: TextStyle(
                           color: VgColors.INPUT_BG_COLOR,
                           fontSize: 12,
                         height: 1.2
                           ),
                     ),
            SizedBox(width: 15,),
          ],
        ),
      ),
    );
  }
}

// import 'package:e_reception_flutter/common_widgets/common_red_num_widget/common_red_num_widget.dart';
// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:flutter/widgets.dart';
//
// /// 打卡记录-顶部数据统计（已标记-未标记）
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/8/21 2:57 PM
// /// @specialDemand:
// class CheckInRecordIndexTopStatisticsCardWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.symmetric(horizontal: 15),
//       child: _toMainRowWidget(),
//     );
//   }
//
//   Widget _toMainRowWidget() {
//     return Row(
//       children: <Widget>[
//           Expanded(
//            child: _toHasLabelCardWidget(),
//           ),
//         SizedBox(
//           width: 8,
//         ),
//         Expanded(
//           child: _toNoLabelCardWidget(),
//         )
//       ],
//     );
//   }
//
//   Widget _toHasLabelCardWidget(){
//     return Container(
//       height: 50,
//        padding: const EdgeInsets.symmetric(horizontal: 12),
//        decoration: BoxDecoration(
//          color: Color(0xFF253952),
//          borderRadius: BorderRadius.circular(8)
//        ),
//       child: Row(
//         children: <Widget>[
//            Text(
//                      "已标记",
//                      maxLines: 1,
//                      overflow: TextOverflow.ellipsis,
//                      style: TextStyle(
//                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                          fontSize: 17,
//                          height: 1.24
//                          ),
//                    ),
//           Spacer(),
//            Text(
//                      "4组/123人",
//                      maxLines: 1,
//                      overflow: TextOverflow.ellipsis,
//                      style: TextStyle(
//                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                          fontSize: 13,
//                        height: 1.24
//                          ),
//                    ),
//           SizedBox(width: 6,),
//           Image.asset("images/go_ico.png",width: 7
//             ,)
//         ],
//       ),
//     );
//   }
//
//
//   Widget _toNoLabelCardWidget(){
//     return Container(
//       height: 50,
//       padding: const EdgeInsets.symmetric(horizontal: 12),
//       decoration: BoxDecoration(
//           color: Color(0xFF34365B),
//           borderRadius: BorderRadius.circular(8)
//       ),
//       child: Row(
//         children: <Widget>[
//           Text(
//             "未标记",
//             maxLines: 1,
//             overflow: TextOverflow.ellipsis,
//             style: TextStyle(
//               color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//               fontSize: 17,
//                 height: 1.24
//             ),
//           ),
//           Spacer(),
//           CommonRedNumWidget(124),
//           SizedBox(width: 6,),
//           Image.asset("images/go_ico.png",width: 7
//             ,)
//         ],
//       ),
//     );
//   }
// }

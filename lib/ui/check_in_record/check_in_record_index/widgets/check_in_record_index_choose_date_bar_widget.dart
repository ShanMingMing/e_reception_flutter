import 'package:e_reception_flutter/common_widgets/common_select_date_time_dialog/common_select_date_time_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_date_picker/vg_date_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// 打卡记录-选择日期组件
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 3:26 PM
/// @specialDemand:
class CheckInRecordIndexChooseDateBarWidget extends StatefulWidget {
  @override
  _CheckInRecordIndexChooseDateBarWidgetState createState() =>
      _CheckInRecordIndexChooseDateBarWidgetState();
}

class _CheckInRecordIndexChooseDateBarWidgetState
    extends State<CheckInRecordIndexChooseDateBarWidget> {
  DateTime _currentDateTime;

  @override
  void initState() {
    super.initState();
    _currentDateTime = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        Map<String, dynamic> resultMap =
            await CommonSelectDateTimeDialog.navigatorPushDialog(
          context,
          VgCupertinoDatePickerMode.date,
          initDateTime: _currentDateTime,
          maxDateTime: DateTime.now(),
        );
        if (resultMap == null) {
          return;
        }
        DateTime resultDateTime=resultMap[CommonSelectDateTimeDialog.RESULT_DATE_TIME];
        _currentDateTime = resultDateTime;
        setState(() {});
      },
      child: Container(
        height: 50,
        child: _toMainRowWidget(),
      ),
    );
  }

  Widget _toMainRowWidget() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 15,
        ),
        Text(
          "刷脸记录",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextBFC2CC_BFC2CC(),
              fontSize: 17,
              fontWeight: FontWeight.w600,
              height: 1.2),
        ),
        Spacer(),
        Text(
          _getDateTimeStr() ?? "",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance()
                  .getPrimaryColor_1890FF()
                  .withOpacity(0.6),
              fontSize: 12,
              height: 1.1),
        ),
        Icon(
          Icons.keyboard_arrow_down,
          size: 19,
          color: ThemeRepository.getInstance()
              .getPrimaryColor_1890FF()
              .withOpacity(0.6),
        ),
        SizedBox(
          width: 4,
        )
      ],
    );
  }

  String _getDateTimeStr() {
    if (_currentDateTime == null) {
      return null;
    }
    String timeStr =
        "${_currentDateTime.year}-${VgToolUtils.twoDigits(_currentDateTime.month)}-${VgToolUtils.twoDigits(_currentDateTime.day)}";
    return timeStr;
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/bean/check_in_record_index_group_info_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 打卡记录-tabs栏
///
/// @author: zengxiangxi
/// @createTime: 1/8/21 5:48 PM
/// @specialDemand:
class CheckInRecordIndexTabsWidget extends StatefulWidget {
  final List<GroupInfoBean> tabsList;

  final TabController tabController;

  const CheckInRecordIndexTabsWidget(
      {Key key, this.tabsList, this.tabController})
      : super(key: key);

  @override
  _CheckInRecordIndexTabsWidgetState createState() =>
      _CheckInRecordIndexTabsWidgetState();
}

class _CheckInRecordIndexTabsWidgetState
    extends State<CheckInRecordIndexTabsWidget> with AutomaticKeepAliveClientMixin{
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      height: 40,
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.only(bottom: 5),
      child: ScrollConfiguration(
          behavior: MyBehavior(),
          child: Theme(
              data: ThemeData(
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent),
              child: _toTabBarWidget())),
    );
  }

  Widget _toTabBarWidget() {
    return TabBar(
      controller: widget?.tabController,
      isScrollable: true,
      indicatorSize: TabBarIndicatorSize.label,
      unselectedLabelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 15,
          height: 1.2),
      labelStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 15,
          fontWeight: FontWeight.w600,
          height: 1.2),
      indicatorColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
      indicatorPadding: EdgeInsets.only(
        left: 10,
        right: 24,
      ),
      labelPadding: EdgeInsets.only(left: 15,right: 0),

      tabs: List.generate(widget?.tabsList?.length ?? 0, (index) {
        return Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 5),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(widget?.tabsList?.elementAt(index)?.groupName ?? ""),
              SizedBox(
                width: 3,
              ),
              Container(
                width: 24,
                child: Text(
                  "${widget?.tabsList?.elementAt(index)?.cnt ?? 0}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

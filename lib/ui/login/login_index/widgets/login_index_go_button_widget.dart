import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/index/index_page.dart';
import 'package:e_reception_flutter/ui/login/login_auth/login_auth_page.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_sp_lib.dart';

/// 登录-点击获取验证码
///
/// @author: zengxiangxi
/// @createTime: 1/6/21 3:44 PM
/// @specialDemand:
class LoginIndexGoButtonWidget extends StatefulWidget {
  final ValueNotifier<String> phoneValueNotifier;
  final ValueChanged<String> onPhoneTap;
  final bool selectionBox;
  const LoginIndexGoButtonWidget(
    this.phoneValueNotifier, {
    Key key, this.onPhoneTap, this.selectionBox,
  }) : super(key: key);

  @override
  _LoginIndexGoButtonWidgetState createState() => _LoginIndexGoButtonWidgetState();
}

class _LoginIndexGoButtonWidgetState extends State<LoginIndexGoButtonWidget> {
  //是否启用灰色

  final Color disableBgColor = Color(0xFF3A3F50);
  final Color disableTextColor = Color(0xFF5E687C);

  // int obtained = 0;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.phoneValueNotifier,
      builder: (BuildContext context, String phone, Widget child) {
        final bool isDisable = phone == null ||
            phone == "" ||
            phone.length != DEFAULT_PHONE_LENGTH_LIMIT;
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: !isDisable,
          height: 50,
          margin: const EdgeInsets.symmetric(horizontal: 46),
          unSelectBgColor: disableBgColor,
          selectedBgColor:  ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color:  disableTextColor,
            fontSize: 17,
          ),
          selectedTextStyle: TextStyle(
            color:  Colors.white,
            fontSize: 17,
          ),
          text: "获取验证码",
          textSize: 17,
          onTap: (){
            if(isDisable){
              VgToastUtils.toast(context, "手机号不正确～");
              return;
            }
            // if(obtained==1){
            //   VgToastUtils.toast(context, "验证码已发送～");
            //   return;
            // }
            if(!widget.selectionBox){
              VgToastUtils.toast(context, "请同意下方服务协议与隐私政策");
              FocusScope.of(context).requestFocus(FocusNode());
              return;
            }
            // obtained = 1;
            //移除焦点
            VgToolUtils.removeAllFocus(context);
            if(widget.onPhoneTap != null){
              widget.onPhoneTap(widget.phoneValueNotifier?.value);
            }
            setState(() {

            });
          },
        );
      },
    );
  }
}


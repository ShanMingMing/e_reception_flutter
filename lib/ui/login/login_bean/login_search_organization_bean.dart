/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"already":false,"orgInfoList":[{"address":"绿地启航","uname":"刘肖峰","usercnt":0,"city":"北京市","isimport":"00","admincnt":0,"orgid":"c31511e463fe46a1957acd8b7c3b38a6","lanme":"油画","logosurl":"http://static.map8.com/matter/06draw.png","familycnt":0,"provincecode":"110000","citycode":"110100","province":"北京市","oname":"直接登记机构，不设置偏好","phone":"19900001111","district":"大兴区","studentcnt":0,"isboss":"00","logo":"http://static.map8.com/matter/06draw.png","otype":"A0093","region":"北京市北京市大兴区","districtcode":"110115","clacnt":0,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"},{"address":"胜太西路9号7幢206室","uname":"刘肖峰","usercnt":14,"city":"南京市","isimport":"00","admincnt":3,"orgid":"c8b82b1b6e364a86976313c28c335781","lanme":"钢管舞","logosurl":"http://static.verygrow.com/bigc/image/20181107/0_111340182007.jpg","familycnt":2,"provincecode":"320000","citycode":"320100","province":"江苏省","oname":"封腾钢管舞专业培训测试","phone":"19900001111","district":"江宁区","studentcnt":9,"isboss":"01","logo":"http://static.verygrow.com/bigc/image/20181107/0_111340182007.jpg","otype":"A0112","region":"江苏省南京市江宁区","districtcode":"320115","clacnt":2,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"},{"address":"胜利西路9号7幢206室","uname":"刘肖峰","usercnt":0,"city":"南京市","isimport":"00","admincnt":0,"orgid":"edfdf981714e4bd5bbe27a210e72974a","lanme":"钢管舞,交谊舞","logosurl":"http://static.we17.com/matter/04dance.png","familycnt":0,"provincecode":"320000","citycode":"320100","province":"江苏省","oname":"峰腾钢管舞指导","phone":"19900001111","district":"江宁区","studentcnt":0,"isboss":"00","logo":"http://static.we17.com/matter/04dance.png","otype":"A0112,A0114","region":"江苏省南京市江宁区","districtcode":"320115","clacnt":0,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"}]}
/// extra : null

class LoginSearchOrganizationBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static LoginSearchOrganizationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LoginSearchOrganizationBean loginSearchOrganizationBeanBean = LoginSearchOrganizationBean();
    loginSearchOrganizationBeanBean.success = map['success'];
    loginSearchOrganizationBeanBean.code = map['code'];
    loginSearchOrganizationBeanBean.msg = map['msg'];
    loginSearchOrganizationBeanBean.data = DataBean.fromMap(map['data']);
    loginSearchOrganizationBeanBean.extra = map['extra'];
    return loginSearchOrganizationBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// already : false
/// orgInfoList : [{"address":"绿地启航","uname":"刘肖峰","usercnt":0,"city":"北京市","isimport":"00","admincnt":0,"orgid":"c31511e463fe46a1957acd8b7c3b38a6","lanme":"油画","logosurl":"http://static.map8.com/matter/06draw.png","familycnt":0,"provincecode":"110000","citycode":"110100","province":"北京市","oname":"直接登记机构，不设置偏好","phone":"19900001111","district":"大兴区","studentcnt":0,"isboss":"00","logo":"http://static.map8.com/matter/06draw.png","otype":"A0093","region":"北京市北京市大兴区","districtcode":"110115","clacnt":0,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"},{"address":"胜太西路9号7幢206室","uname":"刘肖峰","usercnt":14,"city":"南京市","isimport":"00","admincnt":3,"orgid":"c8b82b1b6e364a86976313c28c335781","lanme":"钢管舞","logosurl":"http://static.verygrow.com/bigc/image/20181107/0_111340182007.jpg","familycnt":2,"provincecode":"320000","citycode":"320100","province":"江苏省","oname":"封腾钢管舞专业培训测试","phone":"19900001111","district":"江宁区","studentcnt":9,"isboss":"01","logo":"http://static.verygrow.com/bigc/image/20181107/0_111340182007.jpg","otype":"A0112","region":"江苏省南京市江宁区","districtcode":"320115","clacnt":2,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"},{"address":"胜利西路9号7幢206室","uname":"刘肖峰","usercnt":0,"city":"南京市","isimport":"00","admincnt":0,"orgid":"edfdf981714e4bd5bbe27a210e72974a","lanme":"钢管舞,交谊舞","logosurl":"http://static.we17.com/matter/04dance.png","familycnt":0,"provincecode":"320000","citycode":"320100","province":"江苏省","oname":"峰腾钢管舞指导","phone":"19900001111","district":"江宁区","studentcnt":0,"isboss":"00","logo":"http://static.we17.com/matter/04dance.png","otype":"A0112,A0114","region":"江苏省南京市江宁区","districtcode":"320115","clacnt":0,"createuid":"13e6ea9c59804ab2a82f3d06067e3ea1"}]

class DataBean {
  bool already;
  List<OrgInfoListBean> orgInfoList;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.already = map['already'];
    dataBean.orgInfoList = List()..addAll(
      (map['orgInfoList'] as List ?? []).map((o) => OrgInfoListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "already": already,
    "orgInfoList": orgInfoList,
  };
}

/// address : "绿地启航"
/// uname : "刘肖峰"
/// usercnt : 0
/// city : "北京市"
/// isimport : "00"
/// admincnt : 0
/// orgid : "c31511e463fe46a1957acd8b7c3b38a6"
/// lanme : "油画"
/// logosurl : "http://static.map8.com/matter/06draw.png"
/// familycnt : 0
/// provincecode : "110000"
/// citycode : "110100"
/// province : "北京市"
/// oname : "直接登记机构，不设置偏好"
/// phone : "19900001111"
/// district : "大兴区"
/// studentcnt : 0
/// isboss : "00"
/// logo : "http://static.map8.com/matter/06draw.png"
/// otype : "A0093"
/// region : "北京市北京市大兴区"
/// districtcode : "110115"
/// clacnt : 0
/// createuid : "13e6ea9c59804ab2a82f3d06067e3ea1"

class OrgInfoListBean {
  String address;
  String uname;
  int usercnt;
  String city;
  String isimport;
  int admincnt;
  String orgid;
  String lanme;
  String logosurl;
  int familycnt;
  String provincecode;
  String citycode;
  String province;
  String oname;
  String phone;
  String district;
  int studentcnt;
  String isboss;
  String logo;
  String otype;
  String region;
  String districtcode;
  int clacnt;
  String createuid;
  String gps;

  static OrgInfoListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    OrgInfoListBean orgInfoListBean = OrgInfoListBean();
    orgInfoListBean.gps = map['gps'];
    orgInfoListBean.address = map['address'];
    orgInfoListBean.uname = map['uname'];
    orgInfoListBean.usercnt = map['usercnt'];
    orgInfoListBean.city = map['city'];
    orgInfoListBean.isimport = map['isimport'];
    orgInfoListBean.admincnt = map['admincnt'];
    orgInfoListBean.orgid = map['orgid'];
    orgInfoListBean.lanme = map['lanme'];
    orgInfoListBean.logosurl = map['logosurl'];
    orgInfoListBean.familycnt = map['familycnt'];
    orgInfoListBean.provincecode = map['provincecode'];
    orgInfoListBean.citycode = map['citycode'];
    orgInfoListBean.province = map['province'];
    orgInfoListBean.oname = map['oname'];
    orgInfoListBean.phone = map['phone'];
    orgInfoListBean.district = map['district'];
    orgInfoListBean.studentcnt = map['studentcnt'];
    orgInfoListBean.isboss = map['isboss'];
    orgInfoListBean.logo = map['logo'];
    orgInfoListBean.otype = map['otype'];
    orgInfoListBean.region = map['region'];
    orgInfoListBean.districtcode = map['districtcode'];
    orgInfoListBean.clacnt = map['clacnt'];
    orgInfoListBean.createuid = map['createuid'];
    return orgInfoListBean;
  }

  Map toJson() => {
    "gps": gps,
    "address": address,
    "uname": uname,
    "usercnt": usercnt,
    "city": city,
    "isimport": isimport,
    "admincnt": admincnt,
    "orgid": orgid,
    "lanme": lanme,
    "logosurl": logosurl,
    "familycnt": familycnt,
    "provincecode": provincecode,
    "citycode": citycode,
    "province": province,
    "oname": oname,
    "phone": phone,
    "district": district,
    "studentcnt": studentcnt,
    "isboss": isboss,
    "logo": logo,
    "otype": otype,
    "region": region,
    "districtcode": districtcode,
    "clacnt": clacnt,
    "createuid": createuid,
  };
}
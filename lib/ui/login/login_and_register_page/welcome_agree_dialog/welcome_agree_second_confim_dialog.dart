import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/login/login_and_register_page/welcome_agree_dialog/welcome_agree_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_screen_lib.dart';

///针对IOS不同意隐私弹出二次确认框
class WelcomeAgreeSecondConfimDialog extends StatelessWidget {

  static void navigatorPushDialog(BuildContext context){
    VgDialogUtils.showCommonBottomDialog(context: context, enableDrag: false,isDismissible:false,child: WelcomeAgreeSecondConfimDialog());
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 280 + ScreenUtils.getBottomBarH(context),
      decoration: BoxDecoration(
          color: ColorConstants.CARD_BG_COLOR,
          borderRadius: BorderRadius.vertical(top: Radius.circular(8))),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25,
          ),
          Text(
            "您需要同意本隐私政策才\n能继续使用AI前台",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: TextColorConstants.TEXT_MAIN_COLOR,
                fontWeight: FontWeight.w600,
                fontSize: 17),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
              child: Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    "若您不同意本隐私政策，很遗憾我们将无法为您提供服务。",
                    style: TextStyle(
                        color: TextColorConstants.TEXT_MAIN_COLOR,
                        fontSize: 14
                    ),
                  )
            ),
          ),
          SizedBox(height: 20,),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              RouterUtils.pop(context);
              WelcomeAgreeDialog.navigatorPushDialog(context);
            },
            child: Container(
              height: 45,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  borderRadius: BorderRadius.circular(22.5)
              ),
              child:  Text(
                "查看协议",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17),
              ),
            ),
          ),
          SizedBox(height: 10,),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => VgToolUtils.exitApp(),
            child: Container(
              height: 45,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(22.5)
              ),
              child:  Text(
                "仍不同意",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: TextColorConstants.TEXT_EDIT_HINT_COLOR,
                    fontSize: 17),
              ),
            ),
          ),
          SizedBox(
            height: 30 + ScreenUtils.getBottomBarH(context),
          )
        ],
      ),
    );
  }
}

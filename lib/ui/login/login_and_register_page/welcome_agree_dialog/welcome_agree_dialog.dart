import 'dart:io';

import 'package:e_reception_flutter/common_widgets/common_web_page/common_web_page.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/main_delegate.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/share_repository/share_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/color_constants.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/login/login_and_register_page/welcome_agree_dialog/welcome_agree_second_confim_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_const.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_sp_lib.dart';

///欢迎同意隐私协议弹窗
class WelcomeAgreeDialog extends StatelessWidget {
  
  static Future<dynamic> navigatorPushDialog(BuildContext context){
    return VgDialogUtils.showCommonBottomDialog(context: context, enableDrag: false,isDismissible:false,child: WelcomeAgreeDialog());
  }

  bool _isEnableClose = false;

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder:(BuildContext context)=> Container(
        height: 280 + ScreenUtils.getBottomBarH(context),
        decoration: BoxDecoration(
          color: ColorConstants.CARD_BG_COLOR,
          borderRadius: BorderRadius.vertical(top: Radius.circular(8))
        ),
        child: WillPopScope(
          onWillPop: ()async{
            return _isEnableClose ?? false;
          },
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 25,
              ),
               Text(
                         "欢迎使用AI前台APP",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             color: TextColorConstants.TEXT_MAIN_COLOR,
                             fontWeight: FontWeight.w600,
                             fontSize: 17),
                       ),
              SizedBox(
                height: 15,
              ),
               Expanded(
                 child: DefaultTextStyle.merge(
                   maxLines: 3,
                   overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                   ),
                   child: Container(
                             alignment: Alignment.topLeft,
                             margin: const EdgeInsets.symmetric(horizontal: 20),
                             child: Text.rich(
                               TextSpan(
                                 children: [
                                    TextSpan(
                                      text: "我们非常重视隐私和个人信息保护，请您认真阅读",
                                    ),
                                   TextSpan(
                                     text: "《隐私政策》",
                                     style: TextStyle(
                                       color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                                     ),
                                     recognizer:TapGestureRecognizer(
                                     )..onTap = (){
                                       Navigator.of(context)
                                           .push(new MaterialPageRoute(builder: (_) {
                                         return CommonWebPage(
                                           title: "隐私政策",
                                           url: PRIVACY_H5,
                                         );
                                       }));
                                     }
                                   ),
                                   TextSpan(
                                       text: "和",
                                   ),
                                   TextSpan(
                                       text: "《用户协议》",
                                       style: TextStyle(
                                           color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                                       ),
                                       recognizer:TapGestureRecognizer(
                                       )..onTap = (){
                                         Navigator.of(context)
                                             .push(new MaterialPageRoute(builder: (_) {
                                           return CommonWebPage(
                                             title: "用户协议",
                                             url: USER_H5,
                                           );
                                         }));
                                       }
                                   ),
                                   TextSpan(
                                     text: "的全部内容，在充分理解并同意后开始使用我们的服务。感谢您的支持与关注！",
                                   )
                                 ]
                               ),
                               style: TextStyle(
                                 color: TextColorConstants.TEXT_MAIN_COLOR,
                                 fontSize: 14
                               ),
                             )
                           ),
                 ),
               ),
              SizedBox(height: 20,),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: (){
                  _isEnableClose = true;
                  Navigator.of(context).maybePop();
                  SharePreferenceUtil.putBool(SP_FIRST_INSTALL,false);
                  MainDelegate.initWithCondition();
                  ConstantRepository.of().initUmeng();
                  // ShareRepository.getInstance().submitPrivacyGrantResult(context, null);
                },
                child: Container(
                  height: 45,
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    borderRadius: BorderRadius.circular(22.5)
                  ),
                  child:  Text(
                            "同意并继续",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17),
                          ),
                ),
              ),
              SizedBox(height: 10,),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: (){
                  RouterUtils.pop(context);
                  if(Platform.isIOS){
                    WelcomeAgreeSecondConfimDialog.navigatorPushDialog(context);
                    return;
                  }
                  VgToolUtils.exitApp();
                },
                child: Container(
                  height: 45,
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(22.5)
                  ),
                  child:  Text(
                    "不同意，退出",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: TextColorConstants.TEXT_EDIT_HINT_COLOR,
                        fontSize: 17),
                  ),
                ),
              ),
              SizedBox(
                height: 30 + ScreenUtils.getBottomBarH(context),
              )
            ],
          ),
        ),
      ),
    );
  }
}

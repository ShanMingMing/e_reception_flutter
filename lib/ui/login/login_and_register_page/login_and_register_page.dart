import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_register/company_category_selection_page.dart';
import 'package:e_reception_flutter/ui/company/company_register/company_register_page.dart';
import 'package:e_reception_flutter/ui/login/login_index/login_page.dart';
import 'package:flutter/material.dart';

/// 登录和注册页面
///
/// @author: zengxiangxi
/// @createTime: 3/24/21 6:28 PM 
/// @specialDemand:
class LoginAndRegisterPage extends StatefulWidget {
  @override
  _LoginAndRegisterPageState createState() => _LoginAndRegisterPageState();
}

class _LoginAndRegisterPageState extends State<LoginAndRegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: _toMainWidget(),
    );
  }

  Widget _toMainWidget() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Image.asset("images/login_and_register_bg.png",fit: BoxFit.cover,),
        Align(
          alignment: Alignment.bottomCenter,
          child: _toTwoButtonWidget(),
        )
      ],
    );
  }

  Widget _toTwoButtonWidget(){
    return Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              LoginPage.navigatorPush(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 28,right: 10,top: 50,bottom: 50),
              child: CommonFixedHeightConfirmButtonWidget(
                isAlive: true,
                height: 45,
                unSelectBgColor: Color(0xFF3A3F50),
                selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                unSelectTextStyle: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 17,
                ),
                selectedTextStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                  fontWeight: FontWeight.w600
                ),
                text: "登录",
                textSize: 17,
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: (){
              // CompanyRegisterPage.navigatorPush(context);
              CompanyCategorySelectionPage.navigatorPush(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10,right: 28,top: 50,bottom: 50),
              child: CommonFixedHeightConfirmButtonWidget(
                isAlive: true,
                height: 45,
                unSelectBgColor: Color(0xFF3A3F50),
                selectedBgColor: Colors.white,
                unSelectTextStyle: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 17,
                ),
                selectedTextStyle: TextStyle(
                    color: Color(0xFF21263C),
                    fontSize: 17,
                    fontWeight: FontWeight.w600
                ),
                text: "注册",
                textSize: 17,
              ),
            ),
          ),
        )
      ],
    );
  }

}

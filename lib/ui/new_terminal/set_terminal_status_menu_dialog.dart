import 'dart:core';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_is_know_dialog.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/index/widgets/terminal_operations_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:e_reception_flutter/ui/new_terminal/set_auto_onoff_page_by_each_day.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'binding_terminal/binding_terminal_view_model.dart';
import 'new_terminal_list/bean/common_terminal_settings_response_bean.dart';

/// 设置设备状态弹窗
class SetTerminalStatusMenuDialog extends StatefulWidget {

  final ValueChanged<SetTerminalStateType> onOpen;
  final ValueChanged<SetTerminalStateType> screenOff;
  final ValueChanged<SetTerminalStateType> onClose;
  final ValueChanged<String> timeChange;
  final ValueChanged<String> weekChange;
  final ValueChanged<String> autoOnOffChange;
  final SetTerminalStateType currentState;
  final String terminalName;
  final String hsn;
  final int rcaid;
  final ComAutoSwitchBean autoBean;

  const SetTerminalStatusMenuDialog({Key key, this.onOpen, this.screenOff,
    this.onClose, this.timeChange, this.currentState, this.hsn,
    this.rcaid, this.terminalName, this.weekChange, this.autoOnOffChange,
    this.autoBean}) : super(key: key);

  @override
  _SetTerminalStatusMenuDialogState createState() =>
      _SetTerminalStatusMenuDialogState();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      ValueChanged<SetTerminalStateType> onOpen,
      ValueChanged<SetTerminalStateType> screenOff,
      ValueChanged<SetTerminalStateType> onClose,
      ValueChanged<String> timeChange,
      SetTerminalStateType currentState,
      String onOffTime,
      String week,
      String hsn,
      int rcaid,
      String terminalName,
      String autoOnOff,
      ComAutoSwitchBean autoBean,
      {ValueChanged<String> weekChange, ValueChanged<String> autoOnOffChange}
      ) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      child: SetTerminalStatusMenuDialog(
        onOpen:onOpen,
        screenOff:screenOff,
        onClose:onClose,
        timeChange:timeChange,
        currentState:currentState,
        hsn:hsn,
        rcaid:rcaid,
        terminalName:terminalName,
        weekChange:weekChange,
        autoOnOffChange:autoOnOffChange,
        autoBean:autoBean,
      ),
    );
  }

}

class _SetTerminalStatusMenuDialogState extends BaseState<SetTerminalStatusMenuDialog>{
  SetTerminalStateType _currentState;
  BindingTerminalViewModel _viewModel;
  ComAutoSwitchBean _autoBean;
  @override
  void initState() {
    super.initState();
    _viewModel = BindingTerminalViewModel(this);
    _currentState = widget?.currentState;
    _autoBean = widget?.autoBean;
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
      child: Container(
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getCardBgColor_21263C()),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 12),
              height: 35,
              child: Text(
                "${widget?.terminalName??"-"}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 12,
                    color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                ),
              ),
            ),
            Container(
              height: 0.5,
              color: Color(0xFF303546),
            ),
            _toTerminalOpenWidget(),
            _toTerminalCloseWidget(),
            Container(
              height: 0.5,
              color: Color(0xFF303546),
            ),
            _toAutoOnOffWidget(),
            // _toScreenOffWidget(),
            Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            ),
            _toCancelButtonWidget(context),
          ],
        ),
      ),
    );
  }

  ///正常开机
  Widget _toTerminalOpenWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: ()async{
        if(_currentState == SetTerminalStateType.open){
          RouterUtils.pop(context);
          return;
        }
        if(IGNORE_HSN == (widget?.hsn??"")){
          VgToastUtils.toast(context, "该设备暂不支持远程开关");
          return;
        }
        bool result =
        await TerminalOperationsDialog.navigatorPushDialog(context,
            title: "确定开启该设备？",
            cancelText: "取消",
            confirmText: "开启",
            confirmBgColor: ThemeRepository.getInstance()
                .getHintGreenColor_00C6C4());
        if(result??false){
          RouterUtils.pop(context);
          widget?.onOpen?.call(_currentState);
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12),
                height: 55,
                alignment: Alignment.centerLeft,
                child: Text(
                  _currentState == SetTerminalStateType.open?"开机状态":"开启设备",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: _currentState == SetTerminalStateType.open?
                      ThemeRepository.getInstance().getPrimaryColor_1890FF()
                          :ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 14,
                      height: 1.2),
                ),
              ),
              SizedBox(width: 4,),
              Visibility(
                visible: _currentState == SetTerminalStateType.open,
                child: Image(
                  image: AssetImage("images/icon_swoosh_select.png"),
                  width: 11,
                ),
              ),
            ],
          ),
          Container(
            height: 0.5,
            color: Color(0xFF303546),
          )
        ],
      ),
    );
  }

  // ///息屏开机
  // Widget _toScreenOffWidget(){
  //   return GestureDetector(
  //     behavior: HitTestBehavior.translucent,
  //     onTap: (){
  //       if(_currentState == SetTerminalStateType.screenOff){
  //         return;
  //       }
  //       RouterUtils.pop(context);
  //       widget?.screenOff?.call(_currentState);
  //     },
  //     child: Column(
  //       mainAxisSize: MainAxisSize.min,
  //       children: [
  //         Row(
  //           children: [
  //             Container(
  //               margin: EdgeInsets.symmetric(horizontal: 12),
  //               height: 55,
  //               alignment: Alignment.centerLeft,
  //               child: Text(
  //                 (_currentState == SetTerminalStateType.open) ?"关闭屏幕":"息屏开机",
  //                 maxLines: 1,
  //                 overflow: TextOverflow.ellipsis,
  //                 style: TextStyle(
  //                     color: _currentState == SetTerminalStateType.screenOff?
  //                     ThemeRepository.getInstance().getPrimaryColor_1890FF()
  //                         :ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
  //                     fontSize: 14,
  //                     height: 1.2),
  //               ),
  //             ),
  //             SizedBox(width: 4,),
  //             Visibility(
  //               visible: _currentState == SetTerminalStateType.screenOff,
  //               child: Image(
  //                 image: AssetImage("images/icon_swoosh_select.png"),
  //                 width: 11,
  //               ),
  //             ),
  //             Spacer(),
  //             GestureDetector(
  //               onTap: ()async{
  //                 if(_currentState == SetTerminalStateType.off){
  //                   CommonISeeDialog.navigatorPushDialog(context, title: "提示",
  //                       content: "当前终端已关机，无法设置或修改定时开关屏幕的时间");
  //                   return;
  //                 }
  //
  //                 SetAutoOnOffPage.navigatorPushDialog(context,
  //                   _startTime, _endTime,
  //                   widget?.week, (time, week){
  //                       _viewModel.setAutoOnOffTime(widget?.rcaid,
  //                           widget?.hsn, "01", time, "01", week, needPop: true);
  //                       widget?.timeChange?.call(time);
  //                     });
  //
  //                 // Map<String, dynamic> resultMap =
  //                 // await BindingTerminalAutoSwitchDialog.navigatorPushDialog(
  //                 //     context,
  //                 //     _startTime,
  //                 //     _endTime,
  //                 //     false, hideBottom: true);
  //                 // if (resultMap == null || resultMap.isEmpty) {
  //                 //   return;
  //                 // }
  //                 // _startTime =
  //                 // resultMap[BINDING_TERMINAL_POP_START_DATE_TIME_KEY];
  //                 // _endTime =
  //                 // resultMap[BINDING_TERMINAL_POP_END_DATE_TIME_KEY];
  //                 //
  //                 // _viewModel.setAutoOnOffTime(widget?.rcaid,
  //                 //     widget?.hsn, "01", _getAutoTimeStr("-"), "01", "1,2,3,4,5", needPop: true);
  //                 // widget?.timeChange?.call(_getAutoTimeStr("-"));
  //               },
  //               child: Container(
  //                 alignment: Alignment.center,
  //                 height: 30,
  //                 padding: EdgeInsets.symmetric(horizontal: 14),
  //                 decoration: BoxDecoration(
  //                     color: Color(0xFF303546),
  //                     borderRadius: BorderRadius.circular(15)),
  //                 child: Row(
  //                   children: [
  //                     Text(
  //                       "定时开关：",
  //                       style: TextStyle(
  //                           fontSize: 12,
  //                           color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
  //                       ),
  //                     ),
  //                     Text(
  //                       _getAutoTimeStr("~")??"未设置",
  //                       style: TextStyle(
  //                           fontSize: 12,
  //                           color:
  //                           (_getAutoTimeStr("~") == null)?ThemeRepository.getInstance().getTextMinorGreyColor_808388()
  //                               :ThemeRepository.getInstance().getTextMainColor_D0E0F7()
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             SizedBox(width: 12,)
  //           ],
  //         ),
  //         Container(
  //           height: 0.5,
  //           color: Color(0xFF303546),
  //         )
  //       ],
  //     ),
  //   );
  // }

  ///关闭设备
  Widget _toTerminalCloseWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: ()async{
        if(_currentState == SetTerminalStateType.off){
          RouterUtils.pop(context);
          return;
        }
        if(IGNORE_HSN == (widget?.hsn??"")){
          VgToastUtils.toast(context, "该设备暂不支持远程开关");
          return;
        }
        bool result =
        await TerminalOperationsDialog.navigatorPushDialog(context,
            title: "确定关闭该设备？",
            cancelText: "取消",
            confirmText: "关闭",
            confirmBgColor: ThemeRepository.getInstance()
                .getMinorRedColor_F95355());
        if(result??false){
          RouterUtils.pop(context);
          widget?.onClose?.call(_currentState);
        }
      },
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 12),
            height: 55,
            alignment: Alignment.centerLeft,
            child: Text(
              _currentState == SetTerminalStateType.off?"关机状态":"关闭设备",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: _currentState == SetTerminalStateType.off?
                  ThemeRepository.getInstance().getPrimaryColor_1890FF()
                      :ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          SizedBox(width: 4,),
          Visibility(
            visible: _currentState == SetTerminalStateType.off,
            child: Image(
              image: AssetImage("images/icon_swoosh_select.png"),
              width: 11,
            ),
          ),
        ],
      ),
    );
  }

  ///定时开关机
  Widget _toAutoOnOffWidget(){
    String autoOnOffTime = getWeekAutoOnOffTimeStr();
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        if(StringUtils.isEmpty(autoOnOffTime) || ("00" == _autoBean?.autoOnoff)){
          return;
        }
        // if(_currentState == SetTerminalStateType.off){
        //   CommonISeeDialog.navigatorPushDialog(context, title: "提示",
        //       content:"当前终端已关机，无法设置或修改定时开关屏幕的时间");
        //   return;
        // }
        SetAutoOnOffByEachDayPage.navigatorPush(context, _autoBean, (autoBean){
          RouterUtils.pop(context);
          _viewModel.saveAutoOnOffByWeek(context, widget?.hsn, widget?.rcaid, autoBean);
          if(widget?.autoOnOffChange != null){
            widget?.autoOnOffChange?.call(autoBean.autoOnoff);
          }
          if(_currentState == SetTerminalStateType.off){
            CommonISeeDialog.navigatorPushDialog(context, title: "提示",
                content:"当前显示屏已关机，定时设置将在开机后生效");
          }
        },
            hsn: widget?.hsn,
            rcaid: widget?.rcaid,
            callback: (){
              RouterUtils.pop(context);
              if(_currentState == SetTerminalStateType.off){
                CommonISeeDialog.navigatorPushDialog(context, title: "提示",
                    content:"当前显示屏已关机，定时设置将在开机后生效");
              }
            }
        );

        // SetAutoOnOffPage.navigatorPushDialog(context, _startTime, _endTime,
        //     widget?.week, (time, week){
        //   RouterUtils.pop(context);
        //       if(StringUtils.isEmpty(time)){
        //         //自动开关机时间为空，代表未设置
        //         _viewModel.setAutoOnOffTime(widget?.rcaid,
        //             widget?.hsn, "01", widget?.onOffTime, "00", widget?.week, needPop: false);
        //         if(widget?.autoOnOffChange != null){
        //           widget?.autoOnOffChange?.call("00");
        //         }
        //       }else{
        //         _viewModel.setAutoOnOffTime(widget?.rcaid,
        //             widget?.hsn, "01", time, "01", week, needPop: false);
        //         if(widget?.autoOnOffChange != null){
        //           widget?.autoOnOffChange?.call("01");
        //         }
        //       }
        //       widget?.timeChange?.call(time);
        //       if(widget?.weekChange != null){
        //         widget?.weekChange?.call(week);
        //       }
        //       if(_currentState == SetTerminalStateType.off){
        //           CommonISeeDialog.navigatorPushDialog(context, title: "提示",
        //               content:"当前显示屏已关机，定时设置将在开机后生效");
        //       }
        //     });
        // _toSetAutoOnOffTime();
      },
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 12),
            height: 55,
            alignment: Alignment.centerLeft,
            child: Text(
              "定时开关机",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                // color: _currentState == SetTerminalStateType.off?
                // ThemeRepository.getInstance().getPrimaryColor_1890FF()
                //     :ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Spacer(),
          Visibility(
            // visible: StringUtils.isNotEmpty(widget?.onOffTime) && ("01" == widget?.autoOnOff),
            visible: StringUtils.isNotEmpty(autoOnOffTime) && ("01" == _autoBean?.autoOnoff),
            child: Row(
              children: [
                Text(
                  autoOnOffTime,
                  style: TextStyle(
                    fontSize: 12,
                    // color:
                    // (_getAutoTimeStr("~") == null)?ThemeRepository.getInstance().getTextMinorGreyColor_808388()
                    //     :ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  ),
                ),
                SizedBox(width: 11,),
                Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                ),
              ],
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              //auto_onoff 00未设置 01已设置
              if(StringUtils.isNotEmpty(autoOnOffTime)){
                // RouterUtils.pop(context);
                // _viewModel.setAutoOnOffTime(widget?.rcaid,
                //     widget?.hsn, "01", widget?.onOffTime, "01", "1,2,3,4,5", needPop: false);

                setState(() {
                  _autoBean?.autoOnoff = "01";
                });
                _viewModel.saveAutoOnOffByWeek(context, widget?.hsn, widget?.rcaid,
                    _autoBean,
                    //打开之后进入设置页面
                    callback: (){
                      RouterUtils.pop(context);
                      SetAutoOnOffByEachDayPage.navigatorPush(context,
                          _autoBean,
                          (autoBean){
                            RouterUtils.pop(context);
                            _viewModel.saveAutoOnOffByWeek(context, widget?.hsn, widget?.rcaid, autoBean);
                            if(widget?.autoOnOffChange != null){
                              widget?.autoOnOffChange?.call(autoBean.autoOnoff);
                            }
                          },
                          hsn: widget?.hsn,
                          rcaid: widget?.rcaid,
                          callback: (){
                            RouterUtils.pop(context);
                            // RouterUtils.pop(context);
                            // if(_currentState == SetTerminalStateType.off){
                            CommonISeeDialog.navigatorPushDialog(context, title: "提示",
                                content:"当前显示屏已关机，定时设置将在开机后生效");
                            // }
                          },
                          showOffHint: _currentState == SetTerminalStateType.off
                      );
                    }, noToast: true);

                if(widget?.autoOnOffChange != null){
                  widget?.autoOnOffChange?.call("01");
                }



              }else{

                SetAutoOnOffByEachDayPage.navigatorPush(context, _autoBean, (autoBean){
                  RouterUtils.pop(context);
                  _viewModel.saveAutoOnOffByWeek(context, widget?.hsn, widget?.rcaid, autoBean);
                  if(widget?.autoOnOffChange != null){
                    widget?.autoOnOffChange?.call(autoBean.autoOnoff);
                  }
                  if(_currentState == SetTerminalStateType.off){
                    CommonISeeDialog.navigatorPushDialog(context, title: "提示",
                        content:"当前显示屏已关机，定时设置将在开机后生效");
                  }
                },
                    hsn: widget?.hsn,
                    rcaid: widget?.rcaid,
                    callback: (){
                      // RouterUtils.pop(context);
                    }
                );


                // SetAutoOnOffPage.navigatorPushDialog(context, _startTime, _endTime,
                //     widget?.week, (time, week){
                //       RouterUtils.pop(context);
                //       if(StringUtils.isEmpty(time)){
                //         //自动开关机时间为空，代表未设置
                //         _viewModel.setAutoOnOffTime(widget?.rcaid,
                //             widget?.hsn, "01", widget?.onOffTime, "00", widget?.week, needPop: false);
                //         if(widget?.autoOnOffChange != null){
                //           widget?.autoOnOffChange?.call("00");
                //         }
                //       }else{
                //         _viewModel.setAutoOnOffTime(widget?.rcaid,
                //             widget?.hsn, "01", time, "01", week, needPop: false);
                //         if(widget?.autoOnOffChange != null){
                //           widget?.autoOnOffChange?.call("01");
                //         }
                //       }
                //       widget?.timeChange?.call(time);
                //       if(widget?.weekChange != null){
                //         widget?.weekChange?.call(week);
                //       }
                //       if(_currentState == SetTerminalStateType.off){
                //         CommonISeeDialog.navigatorPushDialog(context, title: "提示",
                //             content:"当前显示屏已关机，定时设置将在开机后生效");
                //       }
                //     });
                // _toSetAutoOnOffTime();
              }
            },
            child: Visibility(
              // visible: StringUtils.isEmpty(widget?.onOffTime) || ("00" == widget?.autoOnOff),
              visible: StringUtils.isEmpty(autoOnOffTime) || ("00" == _autoBean?.autoOnoff),
              child: Image.asset(
                "images/icon_close.png",
                width: 36,
              ),
            ),
          ),
          SizedBox(width: 12,)
        ],
      ),
    );
  }

  String getWeekAutoOnOffTimeStr(){
    if(_autoBean== null){
      return "";
    }
    if("01" == (_autoBean?.autoMon??"") && StringUtils.isNotEmpty(_autoBean?.monTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoTues??"") && StringUtils.isNotEmpty(_autoBean?.tuesTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoWed??"") && StringUtils.isNotEmpty(_autoBean?.wedTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoThur??"") && StringUtils.isNotEmpty(_autoBean?.thurTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoFri??"") && StringUtils.isNotEmpty(_autoBean?.friTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoSat??"") && StringUtils.isNotEmpty(_autoBean?.satTime)){
      return "已启用";
    }
    if("01" == (_autoBean?.autoSun??"") && StringUtils.isNotEmpty(_autoBean?.sunTime)){
      return "已启用";
    }
    return "";
  }

  // void _toSetAutoOnOffTime()async{
  //   Map<String, dynamic> resultMap =
  //   await BindingTerminalAutoSwitchDialog.navigatorPushDialog(
  //       context,
  //       _startTime,
  //       _endTime,
  //       false, hideBottom: true);
  //   if (resultMap == null || resultMap.isEmpty) {
  //     return;
  //   }
  //   _startTime =
  //   resultMap[BINDING_TERMINAL_POP_START_DATE_TIME_KEY];
  //   _endTime =
  //   resultMap[BINDING_TERMINAL_POP_END_DATE_TIME_KEY];
  //   //auto_onoff 00未设置 01已设置
  //   String autoOnOffTime = _getAutoTimeStr("-");
  //   if(StringUtils.isEmpty(autoOnOffTime)){
  //     //自动开关机时间为空，代表未设置
  //     _viewModel.setAutoOnOffTime(widget?.rcaid,
  //         widget?.hsn, "01", widget?.onOffTime, "00", "1,2,3,4,5", needPop: true);
  //   }else{
  //     _viewModel.setAutoOnOffTime(widget?.rcaid,
  //         widget?.hsn, "01", autoOnOffTime, "01", "1,2,3,4,5", needPop: true);
  //   }
  //   widget?.timeChange?.call(_getAutoTimeStr("-"));
  // }


  Widget _toCancelButtonWidget(BuildContext context) {
    final double bottomStatusHeight =  ScreenUtils.getBottomBarH(context);
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 55 + bottomStatusHeight,
        padding: EdgeInsets.only(bottom: bottomStatusHeight),
        child: Center(
          child: Text(
            "取消",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
        ),
      ),
    );
  }



  String getText(String index){
    switch (index){
      case "1":
        return "一";
      case "2":
        return "二";
      case "3":
        return "三";
      case "4":
        return "四";
      case "5":
        return "五";
      case "6":
        return "六";
      case "7":
        return "日";
    }
  }

  DateTime getStartOrEndTime(String autoOffTime,int index){
    if(StringUtils.isEmpty(autoOffTime) || index == null ||index <0 || index > 1){
      return null;
    }
    List<String> timeList = autoOffTime.split("-");
    if(timeList == null || timeList.length != 2){
      return null;
    }
    String time = timeList.elementAt(index);
    if(StringUtils.isEmpty(time)){
      return null;
    }
    List<String> hourMinList = time.split(":");
    if(hourMinList == null || hourMinList.length != 2){
      return null;
    }
    final DateTime nowDateTime = DateTime.now();

    return DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,int.tryParse(hourMinList.elementAt(0)),int.tryParse(hourMinList.elementAt(1)));
  }
}

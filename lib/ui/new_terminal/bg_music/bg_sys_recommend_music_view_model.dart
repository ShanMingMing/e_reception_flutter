import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/bean/bg_terminal_music_list_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/refresh_terminal_bg_music_list_event.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';
import 'bean/bg_sys_recommend_music_response_bean.dart';

class BgSysRecommendMusicViewModel extends BasePagerViewModel<
    MusicListBean,
    BgSysRecommendMusicResponseBean> {

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  ValueNotifier<List<MusicListBean>> musicListValueNotifier;
  final String hsn;

  BgSysRecommendMusicViewModel(BaseState<StatefulWidget> state, this.hsn)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);
    musicListValueNotifier = ValueNotifier(null);
  }


  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();
  }

  void getBgByHsnWithoutCallback(BuildContext context, String hsn,){
    VgHttpUtils.get(NetApi.GET_SYS_MUSIC_LIST,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "current": 1,
      "size": 1000,
      "hsn": hsn ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          BgSysRecommendMusicResponseBean vo = BgSysRecommendMusicResponseBean.fromMap(val);
          if(vo != null && vo.data != null && vo.data.page != null && vo.data.page.records != null){
            musicListValueNotifier.value = vo.data.page.records;
          }else{
            VgToastUtils.toast(AppMain.context, "暂无音乐数据");
          }
        },
        onError: (msg){
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  void getBgByHsn(BuildContext context, String hsn, Function(List<MusicListBean>) callback){
    VgHudUtils.show(context,"请稍后");
    VgHttpUtils.get(NetApi.GET_SYS_MUSIC_LIST,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "current": 1,
      "size": 1000,
      "hsn": hsn ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          BgSysRecommendMusicResponseBean vo = BgSysRecommendMusicResponseBean.fromMap(val);
          if(vo != null && vo.data != null && vo.data.page != null && vo.data.page.records != null){
            callback.call(vo.data.page.records);
          }else{
            VgToastUtils.toast(AppMain.context, "暂无音乐数据");
          }
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }

  ///从终端移除某个bg
  void setBgByHsn(BuildContext context, String hsn, String mid){
    if(StringUtils.isEmpty(hsn)){
      return;
    }
    VgHudUtils.show(context,"设置中");
    VgHttpUtils.get(NetApi.SET_BG_MUSIC,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "mid": mid ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          VgEventBus.global.send(new RefreshTerminalBgMusicListEvent());
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }


  @override
  bool isNeedCache() {
    return true;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "hsn": hsn,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return NetApi.GET_SYS_MUSIC_LIST;
  }

  @override
  BgSysRecommendMusicResponseBean parseData(VgHttpResponse resp) {
    BgSysRecommendMusicResponseBean vo = BgSysRecommendMusicResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import '../set_terminal_volume_dialog.dart';

class PositionSeekWidget extends StatefulWidget {
  final Duration currentPosition;
  final Duration duration;
  final Function(Duration) seekTo;
  final List<Color> gradientColorList;
  final Color indicatorColor;
  final Color inactiveTrackColor;
  final Color activeTrackColor;

  const PositionSeekWidget({
    @required this.currentPosition,
    @required this.duration,
    @required this.seekTo,
    this.gradientColorList,
    this.indicatorColor,
    this.inactiveTrackColor,
    this.activeTrackColor,
  });

  @override
  _PositionSeekWidgetState createState() => _PositionSeekWidgetState();
}

class _PositionSeekWidgetState extends State<PositionSeekWidget> {
  Duration _visibleValue;
  bool listenOnlyUserInterraction = false;
  double get percent => widget.duration.inMilliseconds == 0
      ? 0
      : _visibleValue.inMilliseconds / widget.duration.inMilliseconds;

  @override
  void initState() {
    super.initState();
    _visibleValue = widget.currentPosition;
  }

  @override
  void didUpdateWidget(PositionSeekWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!listenOnlyUserInterraction) {
      _visibleValue = widget.currentPosition;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      width: ScreenUtils.screenW(context),
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 15),
      child: SliderTheme(
        data: SliderThemeData(
            overlayShape: SliderComponentShape.noOverlay,
            thumbShape: RingThumbShape(
                16, 16, 8, 6,
                indicatorColor: widget?.indicatorColor??Colors.white,
              ringColor: Colors.white.withOpacity(0.6)
            ),
            trackHeight: 4,
            trackShape: CustomRoundedRectSliderTrackShape(activeGradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: widget?.gradientColorList??[
                  Color(0xFFCA45FF),
                  Color(0xFF0365FF),
                ]
            )),
            inactiveTickMarkColor: Colors.transparent,
            activeTickMarkColor: Colors.transparent,
            inactiveTrackColor: widget?.inactiveTrackColor??ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            activeTrackColor: widget?.activeTrackColor??ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            valueIndicatorColor: Colors.transparent),
        child: Slider(
          min: 0.0,
          max: widget.duration.inMilliseconds.toDouble(),
          value: getValue(0.0, widget.duration.inMilliseconds.toDouble()),
          divisions: 1000,
          onChangeStart: (_) {
            setState(() {
              listenOnlyUserInterraction = true;
            });
          },
          onChanged: (newValue) {
            setState(() {
              final to = Duration(milliseconds: newValue.floor());
              _visibleValue = to;
            });
          },
          onChangeEnd: (value){
            setState(() {
              listenOnlyUserInterraction = false;
              widget.seekTo(_visibleValue);
            });
          },
        ),
      ),
    );
  }

  double getValue(double min, double max){
    double value = percent * max;
    if(value < min){
      value = min;
    }
    if(value > max){
      value = max;
    }
    return value;
  }
}

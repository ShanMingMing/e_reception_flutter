import 'dart:convert';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_terminal_list_response_bean.dart';
import 'package:e_reception_flutter/ui/building_index/change_to_diy_or_module_page_event.dart';
import 'package:e_reception_flutter/ui/building_index/create_building_index_response_bean.dart';
import 'package:e_reception_flutter/ui/building_index/refresh_building_index_terminal_settings_event.dart';
import 'package:e_reception_flutter/ui/building_index/refresh_diy_page_event.dart';
import 'package:e_reception_flutter/ui/building_index/show_set_module_type_bubble_event.dart';
import 'package:e_reception_flutter/ui/common/update_diy_center_info_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/refresh_terminal_bg_music_list_event.dart';
import 'package:e_reception_flutter/utils/file_utils.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../../app_main.dart';


class BgMusicViewModel extends BaseViewModel {

  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;
  ValueNotifier<BuildingIndexDetailBean> buildingIndexDetailValueNotifier;
  ValueNotifier<BuildingIndexTerminalDataBean> buildingIndexTerminalValueNotifier;

  BgMusicViewModel(BaseState<StatefulWidget> state)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    buildingIndexDetailValueNotifier = ValueNotifier(null);
    buildingIndexTerminalValueNotifier = ValueNotifier(null);
  }

  ///上传音频
  void uploadAudio(BuildContext context, String author, String hsn,
      String mname, String msize, String mtime, String murl,){
    VgHudUtils.show(context, "请稍后");
    VgHttpUtils.get(NetApi.UPLOAD_BG_MUSIC, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "author": author ?? "",
      "hsn": hsn ?? "",
      "mname": mname ?? "",
      "msize": msize ?? "",
      "mtime": mtime ?? "",
      "murl": murl ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          VgEventBus.global.send(new RefreshTerminalBgMusicListEvent());
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, msg);
        }
    ));
  }


  @override
  void onDisposed() {
    statusTypeValueNotifier?.dispose();
    buildingIndexDetailValueNotifier?.dispose();
    super.onDisposed();
  }
}

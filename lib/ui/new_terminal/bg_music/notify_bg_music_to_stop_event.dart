///自己添加的音乐以及系统推荐音乐播放器互相通知停止对方的播放状态
class NotifyBgMusicToStopEvent {
  final int page;
  NotifyBgMusicToStopEvent(this.page);
}
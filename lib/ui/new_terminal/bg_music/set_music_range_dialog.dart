import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 背景音乐添加方式
class SetMusicRangeDialog extends StatefulWidget {

  final Function(bool single) onConfirm;

  const SetMusicRangeDialog({Key key, this.onConfirm,}) : super(key: key);

  static Future<Map<String, dynamic>> navigatorPushDialog(BuildContext context,
      Function onConfirm){
    return VgDialogUtils.showCommonDialog<Map<String, dynamic>>(context: context, child: SetMusicRangeDialog(
      onConfirm: onConfirm,
    ),barrierDismissible: false);
  }

  @override
  _SetPlayIntervalDialogState createState() => _SetPlayIntervalDialogState();

}

class _SetPlayIntervalDialogState extends BaseState<SetMusicRangeDialog>{
  bool _currentTerminal = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: ()=> RouterUtils.pop(context),
      child: UnconstrainedBox(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){},
          child: Material(
            type: MaterialType.transparency,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                width: 290,
                decoration: BoxDecoration(
                    color: ThemeRepository.getInstance().getCardBgColor_21263C()
                ),
                child: _toMainColumnWidget(context),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _toMainColumnWidget(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 25,),
        Text(
          "请选择添加方式",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 16,
              fontWeight: FontWeight.w600
          ),
        ),
        SizedBox(height: 25,),
        _toTerminalRangeWidget(),
        SizedBox(height: 25,),
        _toTwoButtonWidget(context),
      ],
    );
  }


  ///设置终端范围
  Widget _toTerminalRangeWidget(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20,),
          GestureDetector(
            onTap: (){
              setState(() {
                _currentTerminal = true;
              });
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _currentTerminal
                        ?"images/icon_terminal_select.png"
                        :"images/icon_terminal_unselect.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "仅应用于当前显示屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
          SizedBox(height: 20,),
          GestureDetector(
            onTap: (){
              setState(() {
                _currentTerminal = false;
              });
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _currentTerminal
                        ?"images/icon_terminal_unselect.png"
                        :"images/icon_terminal_select.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "应用于全部显示屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///取消确定
  Widget _toTwoButtonWidget(BuildContext context){
    return Container(
      height: 40,
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                RouterUtils.pop(context);
              },
              child: Container(
                color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                child: Center(
                  child:  Text(
                    "取消",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: VgColors.INPUT_BG_COLOR,
                        fontSize: 14,
                        height: 1.2
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                if(widget?.onConfirm != null){
                  widget?.onConfirm?.call(_currentTerminal);
                }
                RouterUtils.pop(context);
              },
              child: Container(
                color:ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                child: Center(
                  child:  Text(
                    "确定",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        height: 1.2
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

}

import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"musicflg":"01","musicList":[{"murl":"sdcard/data","author":"1","mid":"b70b57bee08b4180ad102dc64ebf007b","mname":"1","msize":200,"id":"0f6b3cf18827c73d244cf8fca30998f5","mtime":"1:30"},{"murl":"sdcard/data","author":"1","mid":"d1ccbb7fc3354edfa49ef58fa50fe52e","mname":"1","msize":200,"id":"ce58d1388b4e0577ad24378091bd328c","mtime":"1:30"},{"murl":"Mi9 Pro 5G\\内部存储设备\\Download","author":"周杰伦","mid":"f26d2c22be194031aa4d444bfee98a34","mname":"夜曲","msize":3667,"id":"6f082b3158336cd988100be4afaeed8e","mtime":"4:26"}]}
/// extra : null

class BgTerminalMusicListResponseBean extends BasePagerBean<MusicListBean>{
  bool success;
  String code;
  String msg;
  MusicListDataBean data;
  dynamic extra;

  static BgTerminalMusicListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BgTerminalMusicListResponseBean bgTerminalMusicListResponseBeanBean = BgTerminalMusicListResponseBean();
    bgTerminalMusicListResponseBeanBean.success = map['success'];
    bgTerminalMusicListResponseBeanBean.code = map['code'];
    bgTerminalMusicListResponseBeanBean.msg = map['msg'];
    bgTerminalMusicListResponseBeanBean.data = MusicListDataBean.fromMap(map['data']);
    bgTerminalMusicListResponseBeanBean.extra = map['extra'];
    return bgTerminalMusicListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<MusicListBean> getDataList() => data?.musicList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// musicflg : "01"
/// musicList : [{"murl":"sdcard/data","author":"1","mid":"b70b57bee08b4180ad102dc64ebf007b","mname":"1","msize":200,"id":"0f6b3cf18827c73d244cf8fca30998f5","mtime":"1:30"},{"murl":"sdcard/data","author":"1","mid":"d1ccbb7fc3354edfa49ef58fa50fe52e","mname":"1","msize":200,"id":"ce58d1388b4e0577ad24378091bd328c","mtime":"1:30"},{"murl":"Mi9 Pro 5G\\内部存储设备\\Download","author":"周杰伦","mid":"f26d2c22be194031aa4d444bfee98a34","mname":"夜曲","msize":3667,"id":"6f082b3158336cd988100be4afaeed8e","mtime":"4:26"}]

class MusicListDataBean {
  String musicflg;
  List<MusicListBean> musicList;

  bool isOpen(){
    return "00" == musicflg;
  }

  static MusicListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MusicListDataBean dataBean = MusicListDataBean();
    dataBean.musicflg = map['musicflg'];
    dataBean.musicList = List()..addAll(
      (map['musicList'] as List ?? []).map((o) => MusicListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "musicflg": musicflg,
    "musicList": musicList,
  };
}

/// murl : "sdcard/data"
/// author : "1"
/// mid : "b70b57bee08b4180ad102dc64ebf007b"
/// mname : "1"
/// msize : 200
/// id : "0f6b3cf18827c73d244cf8fca30998f5"
/// mtime : "1:30"

class MusicListBean {
  String murl;
  String author;
  String mid;
  String mname;
  int msize;
  String id;
  String mtime;
  //01使用中
  String useflg;

  bool isInUse(){
    return "01" == useflg;
  }

  String getAuthorAndTime(){
    String authorAndTime = "";
    if(StringUtils.isNotEmpty(author)){
      authorAndTime += author;
      authorAndTime += " ";
    }
    if(StringUtils.isNotEmpty(mtime)){
      authorAndTime += mtime;
    }
    return authorAndTime;
  }

  String getTimeAndSize(){
    String timeAndSize = "";
    if(StringUtils.isNotEmpty(mtime)){
      timeAndSize += mtime;
      timeAndSize += " ";
    }
    if(msize != null){
      timeAndSize += getStorageStr(msize);
    }else{
      timeAndSize += "0kb";
    }
    return timeAndSize;
  }

  String getStorageStr(int size){
    if(size <= 1024){
      return size.toStringAsPrecision(3) + "Kb";
    }else if(size > 1024 && size <= 1024*1024){
      return (size/1024).toStringAsPrecision(3) + "M";
    }else{
      return (size/(1024*1024)).toStringAsPrecision(3) + "G";
    }
  }

  static MusicListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MusicListBean musicListBean = MusicListBean();
    musicListBean.murl = map['murl'];
    musicListBean.author = map['author'];
    musicListBean.mid = map['mid'];
    musicListBean.mname = map['mname'];
    musicListBean.msize = map['msize'];
    musicListBean.id = map['id'];
    musicListBean.mtime = map['mtime'];
    musicListBean.useflg = map['useflg'];
    return musicListBean;
  }

  Map toJson() => {
    "murl": murl,
    "author": author,
    "mid": mid,
    "mname": mname,
    "msize": msize,
    "id": id,
    "mtime": mtime,
    "useflg": useflg,
  };
}
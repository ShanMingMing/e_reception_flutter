import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'bg_terminal_music_list_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"murl":"https://etpic.we17.com/music/meet_piano.mp3","author":"纯音乐","id":"5","mname":"遇见 (钢琴版)","msize":3267,"mtime":"3:24"},{"murl":"https://etpic.we17.com/music/quiet%20afternoon.flac","author":"Pianoboy高至豪","id":"4","mname":"安静的午后","msize":6810,"mtime":"2:28"},{"murl":"https://etpic.we17.com/music/Dusk.flac","author":"Peter Jeremias","id":"3","mname":"Dusk.flac","msize":6451,"mtime":"2:05"},{"murl":"https://etpic.we17.com/music/Outro.mp3","author":"Mili (ミリー）","id":"2","mname":"Outro","msize":8909,"mtime":"3:45"},{"murl":"https://etpic.we17.com/music/Butterfly%20Waltz.flac","author":"Brian Crain","id":"1","mname":"Butterfly Waltz","msize":15586,"mtime":"3:28"}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class BgSysRecommendMusicResponseBean extends BasePagerBean<MusicListBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static BgSysRecommendMusicResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BgSysRecommendMusicResponseBean bgSysRecommendMusicResponseBeanBean = BgSysRecommendMusicResponseBean();
    bgSysRecommendMusicResponseBeanBean.success = map['success'];
    bgSysRecommendMusicResponseBeanBean.code = map['code'];
    bgSysRecommendMusicResponseBeanBean.msg = map['msg'];
    bgSysRecommendMusicResponseBeanBean.data = DataBean.fromMap(map['data']);
    bgSysRecommendMusicResponseBeanBean.extra = map['extra'];
    return bgSysRecommendMusicResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<MusicListBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// page : {"records":[{"murl":"https://etpic.we17.com/music/meet_piano.mp3","author":"纯音乐","id":"5","mname":"遇见 (钢琴版)","msize":3267,"mtime":"3:24"},{"murl":"https://etpic.we17.com/music/quiet%20afternoon.flac","author":"Pianoboy高至豪","id":"4","mname":"安静的午后","msize":6810,"mtime":"2:28"},{"murl":"https://etpic.we17.com/music/Dusk.flac","author":"Peter Jeremias","id":"3","mname":"Dusk.flac","msize":6451,"mtime":"2:05"},{"murl":"https://etpic.we17.com/music/Outro.mp3","author":"Mili (ミリー）","id":"2","mname":"Outro","msize":8909,"mtime":"3:45"},{"murl":"https://etpic.we17.com/music/Butterfly%20Waltz.flac","author":"Brian Crain","id":"1","mname":"Butterfly Waltz","msize":15586,"mtime":"3:28"}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"murl":"https://etpic.we17.com/music/meet_piano.mp3","author":"纯音乐","id":"5","mname":"遇见 (钢琴版)","msize":3267,"mtime":"3:24"},{"murl":"https://etpic.we17.com/music/quiet%20afternoon.flac","author":"Pianoboy高至豪","id":"4","mname":"安静的午后","msize":6810,"mtime":"2:28"},{"murl":"https://etpic.we17.com/music/Dusk.flac","author":"Peter Jeremias","id":"3","mname":"Dusk.flac","msize":6451,"mtime":"2:05"},{"murl":"https://etpic.we17.com/music/Outro.mp3","author":"Mili (ミリー）","id":"2","mname":"Outro","msize":8909,"mtime":"3:45"},{"murl":"https://etpic.we17.com/music/Butterfly%20Waltz.flac","author":"Brian Crain","id":"1","mname":"Butterfly Waltz","msize":15586,"mtime":"3:28"}]
/// total : 5
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<MusicListBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => MusicListBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}
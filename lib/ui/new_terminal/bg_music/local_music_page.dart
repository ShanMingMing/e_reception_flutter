import 'dart:async';
import 'dart:convert';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/bg_music_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/position_seek_widget.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/set_music_range_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/terminal_bg_music_name_list_event.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_oss_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:local_audio/audio_info.dart';
import 'package:local_audio/local_audio.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:flare_flutter/flare_actor.dart';

import 'notify_bg_music_to_stop_event.dart';

/// 本地音乐
class LocalMusicPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "LocalMusicPage";
  static const String SP_LOCAL_MUSIC_LIST = "sp_local_music_list";
  final String hsn;
  final String hsns;
  final List<String> musicNameList;
  const LocalMusicPage({Key key, this.hsn, this.hsns, this.musicNameList}) : super(key: key);


  @override
  LocalMusicPageState createState() => LocalMusicPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn,
      String hsns, List<String> musicNameList) {
    return RouterUtils.routeForFutureResult(
      context,
      LocalMusicPage(
        hsn: hsn,
        hsns: hsns,
        musicNameList: musicNameList,
      ),
      routeName: LocalMusicPage.ROUTER,
    );
  }
}

class LocalMusicPageState
    extends BaseState<LocalMusicPage> with SingleTickerProviderStateMixin{

  BgMusicViewModel _viewModel;
  bool _dataLoaded = false;
  List<AudioInfo> mLocalAudio;

  AssetsAudioPlayer _assetsAudioPlayer;
  final List<StreamSubscription> _subscriptions = [];
  bool _isPlaying = false;
  Audio _playingAudio;
  String _currentMusicId;
  AnimationController _controller;
  StreamSubscription _terminalBgMusicUpdateStreamSubscription;
  List<String> _musicNameList;

  @override
  void initState(){
    super.initState();
    _musicNameList = new List();
    if(widget?.musicNameList != null){
      _musicNameList.addAll(widget?.musicNameList);
    }
    _viewModel = new BgMusicViewModel(this);
    mLocalAudio = new List();
    _terminalBgMusicUpdateStreamSubscription =
        VgEventBus.global.on<TerminalBgMusicNameListEvent>()?.listen((event) {
          if(event?.musicNameList != null){
            setState(() {
              _musicNameList.clear();
              _musicNameList.addAll(event?.musicNameList);
            });
          }
        });
    Future<String> audioJson = SharePreferenceUtil.getString(LocalMusicPage.SP_LOCAL_MUSIC_LIST);
    audioJson.then((value)  {
      if(StringUtils.isNotEmpty(value)){
        List list = json.decode(value);
        List<AudioInfo> audioList = list.map((m) => AudioInfo.fromMap(m)).toList();
        mLocalAudio.clear();
        mLocalAudio.addAll(audioList);
        setState(() {
          _dataLoaded = true;
        });
      }else{
        _scanMusic();
      }
    });


    _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
    _subscriptions.add(_assetsAudioPlayer.playlistAudioFinished.listen((data) {
      print('playlistAudioFinished : $data');
    }));
    _subscriptions.add(_assetsAudioPlayer.audioSessionId.listen((sessionId) {
      print('audioSessionId : $sessionId');
    }));
    _subscriptions.add(_assetsAudioPlayer.isPlaying.listen((isPlay) {
      setState(() {
        _isPlaying = isPlay??false;
      });
    }));
    _subscriptions.add(_assetsAudioPlayer.current.listen((playing) {
      if(playing != null){
        setState(() {
          _currentMusicId = playing?.audio?.audio?.metas?.id??"";
          print('_currentMusicId : $_currentMusicId');
        });
      }
    }));
    _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //动画从 controller.forward() 正向执行 结束时会回调此方法
        //重置起点
        _controller.reset();
        //开启
        _controller.forward();
      } else if (status == AnimationStatus.dismissed) {
        //动画从 controller.reverse() 反向执行 结束时会回调此方法
      } else if (status == AnimationStatus.forward) {
        //执行 controller.forward() 会回调此状态
      } else if (status == AnimationStatus.reverse) {
        //执行 controller.reverse() 会回调此状态
      }
    });
  }

  @override
  void dispose() {
    _assetsAudioPlayer?.dispose();
    _controller?.dispose();
    _terminalBgMusicUpdateStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: [
          _toTopBarWidget(),
          Expanded(
            child: Stack(
              children: [
                _toListPage(),
                _emptyCustomWidget("本地暂无音乐"),
                _toLoadingPage(),
                _toUploadWidget(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "本地音乐",
      titleColor: ThemeRepository.getInstance().getTextColor_D0E0F7(),
    );
  }

  Widget _toListPage(){
    return Visibility(
      visible: _dataLoaded,
      child: ListView.separated(
          padding: EdgeInsets.only(
              left: 0,
              right: 0,
              top: 0,
              bottom: getNavHeightDistance(context)
          ),
          itemCount: mLocalAudio?.length ?? 0,
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            return _toListItemWidget(index, mLocalAudio?.elementAt(index));
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(height: 0);
          }),
    );
  }

  Widget _emptyCustomWidget(String msg) {
    return Visibility(
      visible: _dataLoaded && (mLocalAudio?.length??0) == 0,
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              "images/empty_icon.png",
              width: 120,
              gaplessPlayback: true,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              msg??"暂无内容",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 13,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _toLoadingPage(){
    return Visibility(
      visible: !_dataLoaded,
      child: Container(
        height: ScreenUtils.screenH(context),
        child: Container(
          width: 130,
          height: 130,
          child: FlareActor("assets/loading.flr",
              alignment: Alignment.center,
              fit: BoxFit.contain,
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              animation: "loading"),
        ),
      ),
    );
  }


  ///重新扫描文件布局
  Widget _toUploadWidget(){
    return Positioned(
      bottom: 50 + ScreenUtils.getBottomBarH(context),
      left: (ScreenUtils.screenW(context) - 100)/2,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: throttle(()  async {
          loading(true, msg: "正在扫描");
          _scanMusic();
          await Future.delayed(Duration(milliseconds: 2000));
        }),
        child: Container(
          width: 100,
          height: 40,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            borderRadius: BorderRadius.circular(24),
          ),
          child: Text(
            "重新扫描",
            style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                height: 1.2,
                fontWeight: FontWeight.w600
            ),
          ),
        ),
      ),
    );
  }

  void _scanMusic()async{
    bool isPermiss =  await PermissionUtil.requestPermission();
    if(isPermiss) {
      Future<List<AudioInfo>> audioList = LocalAudio.scanLocalMusic();
      audioList.then((value) {
        loading(false);
        if(value != null){
          mLocalAudio.clear();
          mLocalAudio.addAll(value);
          SharePreferenceUtil.putString(LocalMusicPage.SP_LOCAL_MUSIC_LIST, json.encode(mLocalAudio));
          toast("扫描完成");
          setState(() {
            _dataLoaded = true;
          });
        }
      });
    }else{
      toast("无文件读写权限");
    }

  }

  Function throttle(
      Future Function() func,
      ) {
    if (func == null) {
      return func;
    }
    bool enable = true;
    Function target = () {
      if (enable == true) {
        enable = false;
        func().then((_) {
          enable = true;
        });
      }
    };
    return target;
  }

  Widget _toListItemWidget(int index, AudioInfo itemBean){
    return Column(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: (){
            openPlayer(itemBean);
          },
          child: Container(
            height: 70,
            padding: EdgeInsets.only(right: 15),
            alignment: Alignment.centerLeft,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            child: Row(
              children: [
                _toMusicLogoWidget(itemBean),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        itemBean?.mname??"-",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15,
                            color: ThemeRepository.getInstance().getTextColor_D0E0F7()
                        ),
                      ),
                      SizedBox(height: 2,),
                      Text(
                        getAuthorAndTime(itemBean?.author, itemBean?.mtime),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 12,
                            color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                        ),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                _toUseWidget(itemBean),
              ],
            ),
          ),
        ),
        _toSeekWidget(itemBean),
      ],
    );
  }

  Widget _toMusicLogoWidget(AudioInfo itemBean){
    if(_isPlaying && itemBean.id == _currentMusicId){
      _controller.forward();
      return Container(
        width: 64,
        alignment: Alignment.center,
        child: RotationTransition(
          alignment: Alignment.center,
          turns: _controller,
          child: Image.asset(
            "images/icon_music_play.png",
            width: 46,
            height: 46,
          ),
        ),
      );
    }else{
      return Container(
        width: 64,
        alignment: Alignment.center,
        child: Image.asset(
          "images/icon_music_play.png",
          width: 34,
          height: 34,
        ),
      );
    }
  }

  Widget _toSeekWidget(AudioInfo itemBean){
    return Visibility(
      visible: _isPlaying && itemBean.id == _currentMusicId,
      child: Container(
        height: 50,
        alignment: Alignment.center,
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: _assetsAudioPlayer.builderRealtimePlayingInfos(
            builder: (context, infos) {
              if (infos == null) {
                return SizedBox();
              }
              return Column(
                children: [
                  PositionSeekWidget(
                    currentPosition: infos.currentPosition,
                    duration: infos.duration,
                    seekTo: (to) {
                      _assetsAudioPlayer.seek(to);
                    },
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          durationToString(infos.currentPosition),
                          style: TextStyle(
                              fontSize: 10,
                              color: Colors.white
                          ),
                        ),
                        Spacer(),
                        Text(
                          durationToString(infos.duration),
                          style: TextStyle(
                              fontSize: 10,
                              color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              );
            }),
      ),
    );
  }

  String durationToString(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes =
    twoDigits(duration.inMinutes.remainder(Duration.minutesPerHour));
    String twoDigitSeconds =
    twoDigits(duration.inSeconds.remainder(Duration.secondsPerMinute));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }


  Widget _toUseWidget(AudioInfo itemBean){
    if(_musicNameList?.contains(itemBean?.mname)??false){
      return Container(
        width: 60,
        height: 28,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            color: Color(0xFF3A3F50)
        ),
        child: Text(
          "已添加",
          style: TextStyle(
              fontSize: 13,
              color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
          ),
        ),
      );
    }else{
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: (){
          // _viewModel.setBgByHsn(context, widget?.hsn, mid);
          String hsn = widget?.hsn;
          if((widget?.hsns??"").split(",").length > 1){
              SetMusicRangeDialog.navigatorPushDialog(context, (isCurrent){
                  if(!isCurrent){
                    hsn = "";
                  }
                  VgHudUtils.show(context, "上传中");
                  VgOssUtils.uploadSingleAudio(
                    path: itemBean?.filePath,
                    ext: itemBean?.fileExt,
                    callback: BaseCallback(
                        onSuccess: (netAudio){
                          if (StringUtils.isNotEmpty(netAudio)) {
                            _viewModel.uploadAudio(context, itemBean?.author,
                                hsn, itemBean?.mname,
                                ((itemBean?.msize/1024).toInt())?.toString(),
                                itemBean?.mtime, netAudio);
                          } else {
                            VgToastUtils.toast(context, "音频上传失败");
                          }
                        },
                        onError: (msg){
                          VgToastUtils.toast(context, msg);
                        }
                    ),
                  );
              });
          }else{
            VgHudUtils.show(context, "上传中");
            VgOssUtils.uploadSingleAudio(
              path: itemBean?.filePath,
              ext: itemBean?.fileExt,
              callback: BaseCallback(
                  onSuccess: (netAudio){
                    if (StringUtils.isNotEmpty(netAudio)) {
                      _viewModel.uploadAudio(context, itemBean?.author,
                          hsn, itemBean?.mname,
                          ((itemBean?.msize/1024).toInt())?.toString(),
                          itemBean?.mtime, netAudio);
                    } else {
                      VgToastUtils.toast(context, "音频上传失败");
                    }
                  },
                  onError: (msg){
                    VgToastUtils.toast(context, msg);
                  }
              ),
            );
          }
        },
        child: Container(
          width: 60,
          height: 28,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14),
            border: Border.all(
                color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                width: 0.5),
          ),
          child: Text(
            "添加",
            style: TextStyle(
                fontSize: 13,
                color: Colors.white
            ),
          ),
        ),
      );
    }
  }

  void openPlayer(AudioInfo itemBean) async {
    VgEventBus.global.send(new NotifyBgMusicToStopEvent(2));
    if(itemBean?.id == _currentMusicId){
      if(_isPlaying){
        _assetsAudioPlayer.pause();
      }else{
        _assetsAudioPlayer.play();
      }
      return;
    }
    _playingAudio = Audio.file(
      itemBean.filePath,
      metas: Metas(
        id: itemBean.id,
        title: itemBean.mname,
        artist: itemBean.author,
        album: itemBean.author,
      ),
    );
    setState(() {});
    try {
      await _assetsAudioPlayer.open(
        _playingAudio,
        autoStart: true,
        showNotification: false,
        playInBackground: PlayInBackground.disabledRestoreOnForeground,
        audioFocusStrategy: AudioFocusStrategy.request(
            resumeAfterInterruption: true,
            resumeOthersPlayersAfterDone: true),
        headPhoneStrategy: HeadPhoneStrategy.pauseOnUnplug,
      );
    } catch (e) {
      print(e);
    }
  }

  String getAuthorAndTime(String author, String time){
    String authorAndTime = "";
    if(StringUtils.isNotEmpty(author)){
      authorAndTime += author;
      authorAndTime += " ";
    }
    if(StringUtils.isNotEmpty(time)){
      authorAndTime += time;
    }
    return authorAndTime;
  }


}

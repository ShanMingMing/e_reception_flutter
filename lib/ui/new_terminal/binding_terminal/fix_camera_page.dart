import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'bean/binding_terminal_edit_upload_bean.dart';
import 'bean/binding_terminal_response_bean.dart';
import 'bind_terminal_more_settings_page.dart';

/// 摄像头校正页面
class FixCameraPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "FixCameraPage";
  final String hsn;
  final String router;
  final BindingTermialEditUploadBean uploadBean;
  const FixCameraPage({Key key, this.hsn, this.uploadBean, this.router})
      : super(key: key);

  @override
  _FixCameraPageState createState() => _FixCameraPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, String hsn, BindingTermialEditUploadBean uploadBean, String router) {
    return RouterUtils.routeForFutureResult(
      context,
      FixCameraPage(
        hsn: hsn,
        router: router,
        uploadBean:uploadBean,
      ),
      routeName: FixCameraPage.ROUTER,
    );
  }
}

class _FixCameraPageState extends BaseState<FixCameraPage> {

  BindingTerminalViewModel _viewModel;
  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;
  int angle;///角度
  String cameraType;///镜像

  @override
  void initState() {
    super.initState();
    _viewModel = BindingTerminalViewModel(this);
    _viewModel.notifyTerminal(widget?.hsn);
    _uploadValueNotifier = ValueNotifier(null);
    angle = widget?.uploadBean?.correctCamera;
    cameraType = widget?.uploadBean?.cameratype;
    Future((){
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toInfoWidget(),
    );
  }

  Widget _toInfoWidget(){
    return ValueListenableBuilder(
      valueListenable: _viewModel?.infoValueNotifier,
      builder: (BuildContext context, BindingTerminalDataBean dataBean, Widget child){
        return Column(
          children: <Widget>[
            _toTopBarWidget(),
            Container(
              alignment: Alignment.center,
              child: Text(
                "调整摄像头至合理拍摄角度",
                style: TextStyle(
                  fontSize: 14,
                  color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
                ),
              ),
            ),
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    ///00->01->02
                    if(cameraType == "00"){
                      cameraType = "01";
                    }else if(cameraType == "01"){
                      cameraType = "02";
                    }else {
                      cameraType = "00";
                    }
                    widget?.uploadBean?.cameratype = cameraType;
                    _viewModel.fixCamera(widget?.uploadBean?.mid, widget?.uploadBean?.rcaid,
                        widget?.uploadBean?.hsn, "00",
                        widget?.uploadBean?.correctCamera, widget?.uploadBean?.cameratype);
                    print("镜像调整");
                  },
                  child: Column(
                    children: [
                      Image.asset(
                        "images/icon_mirror.png",
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(height: 8,),
                      Text(
                        "镜像",
                        style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 60,
                ),
                GestureDetector(
                  onTap: (){
                    print("旋转摄像头");
                    ///0->90->180->270
                    if(angle == 0){
                      angle = 90;
                    }else if(angle == 90){
                      angle = 180;
                    }else if(angle == 180){
                      angle = 270;
                    }else{
                      angle = 0;
                    }
                    widget?.uploadBean?.correctCamera = angle;
                    _viewModel.fixCamera(widget?.uploadBean?.mid, widget?.uploadBean?.rcaid,
                        widget?.uploadBean?.hsn, "00",
                        widget?.uploadBean?.correctCamera, widget?.uploadBean?.cameratype);
                  },
                  child: Column(
                    children: [
                      Image.asset(
                        "images/icon_fix_camera.png",
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(height: 8,),
                      Text(
                        "旋转",
                        style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Spacer(),
            GestureDetector(
              child: Container(
                height: 40,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "完成",
                    style: TextStyle(
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 15,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                ),
              ),
              onTap: (){
                widget?.uploadBean?.faceSize = 1;
                widget?.uploadBean?.cameraStartDistance = 2.0;
                widget?.uploadBean?.punchInInterval = 5;
                widget?.uploadBean?.startDateTime = null;
                String msg = widget?.uploadBean?.checkVerify();
                if(StringUtils.isEmpty(msg)){
                  _viewModel?.saveTerminalInfo(context, widget?.uploadBean, router: widget?.router);
                  return;
                }
                VgToastUtils.toast(context, msg);
                // // _viewModel.notifyTerminal(widget?.hsn);
                // BindTerminalMoreSettingsPage.navigatorPush(context, widget?.hsn, widget?.uploadBean);
              },
            ),
            SizedBox(
              height: 30 + ScreenUtils.getBottomBarH(context),
            ),
          ],
        );
      },
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "摄像头校正",
      isShowBack: true,
    );
  }
}

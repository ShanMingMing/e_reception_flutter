import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:e_reception_flutter/ui/company/company_detail/bean/branch_detail_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'bean/binding_terminal_response_bean.dart';

class TerminalAddressMarkerToImage extends StatefulWidget{
  final ComBranchBean branchBean;
  // 组件的key
  final GlobalKey globalKeys;
  TerminalAddressMarkerToImage({this.branchBean, this.globalKeys,});
  @override
  _TerminalAddressMarkerToImageState createState() => _TerminalAddressMarkerToImageState();

}

class _TerminalAddressMarkerToImageState extends State<TerminalAddressMarkerToImage>{

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: widget.globalKeys,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[

          Container(
            // width: (ScreenUtils.screenW(context)*2)/3 - 15,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 12, top: 8, right: 12, bottom: 8),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.7),
              borderRadius: BorderRadius.circular(6),
            ),
            child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: (ScreenUtils.screenW(context)*2)/3 - 15),
              child: Text(
                (widget?.branchBean?.gpsaddress == null || widget?.branchBean?.gpsaddress.isEmpty)
                    ? widget?.branchBean?.address
                    : widget?.branchBean?.gpsaddress,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    height: 1.2
                ),
              ),
            ),
          ),
          Image.asset(
            "images/icon_location_down.png",
            width: 12,
          ),
          SizedBox(
            height: 3,
          ),
          Image.asset(
            "images/icon_location.png",
            width: 24,
          ),
        ],
      ),
    );
  }
}

// 单例 用于获取图片
class GetWidgetToImage{
  static GlobalKey _globalKeys;
  static GetWidgetToImage _etWidgetToImage;
  // 存放图片的路径
  String _path = "/widget to img/";
  static GetWidgetToImage getInstance(globalKey){
    if (_etWidgetToImage == null) {
      _etWidgetToImage = new GetWidgetToImage();
    }
    _globalKeys = globalKey;
    return _etWidgetToImage;
  }

  // 获取路径
  Future<String> _getPath() async {
    String _filePath = (await getExternalStorageDirectory()).path + _path;
    // 路径不存在则创建
    if (!await Directory(_filePath).exists()) {
      Directory(_filePath).create();
    }
    return _filePath;
  }

  // 获取unit8
  Future<Uint8List> getUint8List() async{
    RenderRepaintBoundary boundary = _globalKeys.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: window.devicePixelRatio,);
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      return byteData.buffer.asUint8List();
  }

  // 返回 文件
  Future<File>  getFile() async {
    String _filePath  = await _getPath();
    return File(_filePath + DateTime.now().millisecondsSinceEpoch.toString() + ".png").writeAsBytes(await getUint8List());
  }

  // 返回Base64流
  Future<String> getByte64() async{
    return base64Encode(await getUint8List());
  }
}
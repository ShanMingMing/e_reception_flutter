import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class SetTerminalPositionDialog extends StatefulWidget {
  final String position;
  final Function(String position) onConfirm;
  final Function() onReset;

  const SetTerminalPositionDialog({Key key,this.position, this.onConfirm, this.onReset,})
      : super(key: key);

  @override
  _SetTerminalPositionDialogState createState() =>
      _SetTerminalPositionDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context,
      {String position, Function onConfirm, Function onReset}) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: SetTerminalPositionDialog(
          position: position,
          onConfirm: onConfirm,
          onReset: onReset,
        )
    );
  }
}

class _SetTerminalPositionDialogState
    extends BaseState<SetTerminalPositionDialog> {
  TextEditingController _positionController;
  bool isAlive = false;
  String _hint = "添加字幕";
  @override
  void initState() {
    super.initState();
    _positionController = TextEditingController();
    if(StringUtils.isNotEmpty(widget?.position)){
      _positionController?.text = widget?.position;
    }
    isAlive = judgeAlive();
    if(isAlive){
      _hint = "修改字幕";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              borderRadius:
              BorderRadius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS)),
          child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                "摆放位置",
                style: TextStyle(
                    fontSize: 17,
                    color: ThemeRepository.getInstance()
                        .getTextMainColor_D0E0F7(),fontWeight: FontWeight.w600
                ),
              ),
            ),
            Spacer(),
            _resetWidget(),
            _buttonWidget(),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15),
          height: 80,
          decoration: BoxDecoration(
              color:
              ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
              borderRadius: BorderRadius.circular(8)
          ),
          child: VgTextField(
            controller: _positionController,
            maxLines: 3,
            minLines: 1,
            maxLength: 30,
            autofocus: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              counterText: "",
              contentPadding: const EdgeInsets.only(left: 15, top:10, bottom: 10, right: 15),
              hintText: "30字以内",
              hintStyle: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextEditHintColor_3A3F50(),
                  fontSize: 14),
            ),
            style: TextStyle(
                fontSize: 14,
                color: ThemeRepository.getInstance()
                    .getTextMainColor_D0E0F7()),
            onChanged: (String str) {
              isAlive = judgeAlive();
              setState(() {});
            },
          ),
        ),
        SizedBox(height: 20,),
      ],
    );
  }

  Widget _buttonWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isAlive ?? false,
        width: 76,
        height: 30,
        unSelectBgColor:
        ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 14,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
          height: 1.2,
        ),
        text: "保存",
        onTap: () {
          if (isAlive) {
            widget?.onConfirm?.call(_positionController.text,);
            RouterUtils.pop(context);
          }
        },
      ),
    );
  }

  Widget _resetWidget(){
    return Opacity(
        opacity: (StringUtils.isNotEmpty(widget?.position??"")) ? 1 : 0,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            if(!judgeAlive()){
              return;
            }
            bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                title: "提示",
                content: "确认重置终端摆放位置？",
                cancelText: "取消",
                confirmText: "重置",
                confirmBgColor:
                ThemeRepository.getInstance().getPrimaryColor_1890FF());
            if (result ?? false) {
              VgToolUtils.removeAllFocus(context);
              widget?.onReset?.call();
              RouterUtils.pop(context);
            }
          },
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 10,),
            child: Text(
              "重置",
              style: TextStyle(
                  fontSize: 14,
                  height: 1.2,
                  color: ThemeRepository.getInstance()
                      .getHintGreenColor_5E687C()),
            ),
          ),
        ));
  }

  bool judgeAlive(){
    return StringUtils.isNotEmpty(_positionController.text);
  }
}

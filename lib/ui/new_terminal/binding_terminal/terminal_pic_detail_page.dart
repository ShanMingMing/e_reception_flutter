import 'dart:io';

import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// @author: pengboboer
/// @createDate: 4/25/21
/*
公共的编辑logo的页面
url: 图片的地址，可以是网络或者本地地址
selectMode: 正常模式、裁剪带有人像边框的模式
clipCompleteCallback: 裁剪完成的回调，可以做一些上传图片至服务器的操作，此回调传入了取消加载框等操作

*/

enum SelectMode {
  Normal,
  HeadBorder,
}

typedef ClipCompleteCallback = void Function(String path, Function cancelLoadingCallback);

class TerminalPicDetailPage extends StatefulWidget {
  static const ROUTER = "TerminalPicDetailPage";

  final String url;
  final SelectMode selectMode;
  final ClipCompleteCallback clipCompleteCallback;
  final bool isClipCompleteShowLoading;

  const TerminalPicDetailPage({
    Key key, 
    this.url, 
    this.selectMode, 
    this.clipCompleteCallback,
    this.isClipCompleteShowLoading = false,
  }) : assert(url != null && url != "", "url cannot be empty");


  @override
  State<StatefulWidget> createState() => _TerminalLogoState();

  static Future<dynamic> navigatorPush(BuildContext context, {String url, SelectMode selectMode, ClipCompleteCallback clipCompleteCallback}) {
    return RouterUtils.routeForFutureResult(context,
        TerminalPicDetailPage(url: url,
            selectMode: selectMode,
            clipCompleteCallback: clipCompleteCallback),
        routeName: TerminalPicDetailPage.ROUTER);
  }
}

class _TerminalLogoState extends BaseState<TerminalPicDetailPage> {

  String _path;

  @override
  void initState() {
    super.initState();
    _path = widget?.url;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(children: <Widget>[
        _toTopBarWidget(context),
        _buildImageLogo(),
        Container(height: ScreenUtils.getBottomBarH(context))
      ]),
    );
  }

  Widget _toTopBarWidget(BuildContext context) {
    return VgTopBarWidget(
      title: "",
      isShowBack: true,
      backImgColor: Color(0xff5e687c),
      isShowGrayLine: false,
      backgroundColor: Colors.transparent,
      rightPadding: 0,
      rightWidget: Offstage(
        offstage: true,
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Image.asset("images/more_white_dark.png", width: 20, height: 20)
        ),
      ),
    );
  }

  Widget _buildImageLogo() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          VgCacheNetWorkImage(
            _path,
            width: ScreenUtils.screenW(context),
            height: ScreenUtils.screenW(context),
            placeWidget: Container(color: Colors.black),

          ),
          SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildHandleBtn("更换图片", () => onClickEdit(false))
            ],
          ),
          SizedBox(height: 20)
        ],
      ),
    );
  }

  Future<void> onClickEdit(bool isEdit) async {
    String path = await ClipImageHeadBorderUtil.clipOneImage(context,
        path: isEdit ? _path : null, scaleY: 1, scaleX: 1, maxAutoFinish: true,
        isShowHeadBorderWidget: widget?.selectMode == SelectMode.HeadBorder,
        isShowTextPrompt: false);
    if (path == null || path == "") {
      return;
    }
    _path = path;
    setState(() {});
    RouterUtils.pop(context);
    if (widget?.isClipCompleteShowLoading ?? false) loading(true, msg: "上传中");
    if (widget?.clipCompleteCallback != null) widget?.clipCompleteCallback(_path, () {
      loading(false);
      toast("上传成功");
    });
  }


  Widget _buildHandleBtn(String text, VoidCallback onClickCallback) {
    return ClickAnimateWidget(
      scale: 1.05,
      onClick: onClickCallback ?? (){},
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: Container(
          width: 112,
          height: 40,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(
                  width: 0.5,
                  color: Color(0xff808388)
              )
          ),
          child: Text(
            "${text ?? ""}",
            style: TextStyle(fontSize: 15,
                color: Color(0xffd0e0f7)
            ),
          ),
        ),
      ),
    );
  }
}
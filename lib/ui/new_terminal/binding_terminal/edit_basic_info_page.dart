
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/bind_terminal_info_confirm_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_edit_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/edit_terminal_basic_info_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'bean/binding_terminal_response_bean.dart';
import 'bean/binding_terminal_type.dart';
import 'fix_camera_page.dart';

///编辑基本信息页面
class EditBasicInfoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "EditBasicInfoPage";
  final TerminalInfoBean terminalInfoBean;


  const EditBasicInfoPage({Key key, this.terminalInfoBean})
      : super(key: key);


  @override
  _EditBasicInfoPageState createState() => _EditBasicInfoPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, TerminalInfoBean terminalInfoBean) {
    return RouterUtils.routeForFutureResult(
      context,
      EditBasicInfoPage(
        terminalInfoBean: terminalInfoBean,
      ),
      routeName: EditBasicInfoPage.ROUTER,
    );
  }
}

class _EditBasicInfoPageState extends BaseState<EditBasicInfoPage> {
  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;
  BindingTerminalViewModel _viewModel;
  @override
  void initState() {
    super.initState();
    _uploadValueNotifier = ValueNotifier(null);
    _viewModel = BindingTerminalViewModel(this);
    Future(() {
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(
          isEnableVerical: true,
          child: Column(children: [
            _toTopBarWidget(),
            Expanded(child: _toMainColumnWidget())
          ])),
    );
  }


  Widget _toMainColumnWidget() {
    return EditTerminalBasicInfoWidget(
        widget?.terminalInfoBean, _uploadValueNotifier);

  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
        title: "基本信息", isShowBack: true, rightWidget: _toSaveButtonWidget());
  }

  bool _compareData(BindingTermialEditUploadBean uploadBean){
    if(uploadBean?.terminal_name != widget?.terminalInfoBean?.terminalName){
      return true;
    }
    if(uploadBean?.position != widget?.terminalInfoBean?.position){
      return true;
    }
    if(uploadBean?.screenSizeType != BindingTerminalScreenSizeTypeExtension.getStrToType(widget?.terminalInfoBean?.screenSpecs)){
      return true;
    }
    if(uploadBean?.screenOrientationType != BindingTerminalScreenOrientationTypeExtension.getStrToType(widget?.terminalInfoBean?.screenDirection)){
      return true;
    }
    if(uploadBean?.deviceType != BindingTerminalDeviceTypeExtension.getStrByType(widget?.terminalInfoBean?.type)){
      return true;
    }
    if(uploadBean?.faceRecognitionType != FaceRecognitionTypeExtension.getStrToType(widget?.terminalInfoBean?.enableFaceRecognition)){
      return true;
    }
    return false;
  }

  Widget _toSaveButtonWidget() {
    return ValueListenableBuilder(
      valueListenable: _uploadValueNotifier,
      builder: (BuildContext context, BindingTermialEditUploadBean uploadBean,
          Widget child) {
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: ((uploadBean?.isAlive() ?? false) && (_compareData(uploadBean))),
          width: 48,
          height: 24,
          unSelectBgColor:
          ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
              fontWeight: FontWeight.w600),
          selectedTextStyle: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          text: "保存",
          onTap: (){
            print("点击");
            String msg = uploadBean?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              _viewModel.setBasicInfo(context, _uploadValueNotifier.value);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

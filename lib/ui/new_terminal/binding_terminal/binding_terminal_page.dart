import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_add_admin_list/binding_add_admin_list_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_edit_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/new_terminal_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'bean/binding_terminal_response_bean.dart';
import 'widgets/binding_terminal_admin_bar_widget.dart';
import 'widgets/binding_terminal_admin_list_widget.dart';
import 'widgets/binding_terminal_edit_setting_widget.dart';

/// 绑定终端页面
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 9:32 AM
/// @specialDemand:
class BindingTerminalPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "BindingTerminalPage";

  final NewTerminalListItemBean terminalPreviousBean;

  const BindingTerminalPage({Key key, this.terminalPreviousBean})
      : super(key: key);

  @override
  _BindingTerminalPageState createState() => _BindingTerminalPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, NewTerminalListItemBean terminalBean) {
    return RouterUtils.routeForFutureResult(
      context,
      BindingTerminalPage(
        terminalPreviousBean: terminalBean,
      ),
      routeName: BindingTerminalPage.ROUTER,
    );
  }
}

class _BindingTerminalPageState extends BaseState<BindingTerminalPage> {
  BindingTerminalViewModel _viewModel;

  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;

  @override
  void initState() {
    super.initState();
    _viewModel = BindingTerminalViewModel(this);
    _viewModel.getBindingTerminalInfo(widget?.terminalPreviousBean?.hsn);
    _uploadValueNotifier = ValueNotifier(null);
    Future(() {
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(
          isEnableVerical: true,
          child: Column(children: [
            _toTopBarWidget(),
            Expanded(child: _toPlaceHolderWidget())
          ])),
    );
  }

  Widget _toPlaceHolderWidget() {
    return ValueListenableBuilder(
        valueListenable: _viewModel?.statusTypeValueNotifier,
        builder:
            (BuildContext context, PlaceHolderStatusType value, Widget child) {
          return VgPlaceHolderStatusWidget(
            loadingStatus: value == PlaceHolderStatusType.loading,
            errorStatus: value == PlaceHolderStatusType.error,
            errorOnClick: () => _viewModel
                .getBindingTerminalInfo(widget?.terminalPreviousBean?.hsn),
            loadingOnClick: () => _viewModel
                .getBindingTerminalInfo(widget?.terminalPreviousBean?.hsn),
            child: child,
          );
        },
        child: _toMainColumnWidget());
  }

  Widget _toMainColumnWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.infoValueNotifier,
      builder: (BuildContext context, BindingTerminalDataBean infoBean,
          Widget child) {
        return Column(
          children: <Widget>[
            BindingTerminalEditSettingWidget(infoBean?.terminalInfo,
                widget?.terminalPreviousBean, _uploadValueNotifier),
            Container(
              height: 8,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            ),
            GestureDetector(
                // behavior: HitTestBehavior.translucent,
                // onTap: () {
                //   BindingAddAdminListPage.navigatorPush(context);
                // },
                // child: BindingTerminalAdminBarWidget(
                //   total: infoBean?.adminList?.length ?? 0,
                //   hsn:  infoBean?.terminalInfo?.hsn ?? "",
                // )
              child: Container(),
            ),
            Expanded(
              child: BindingTerminalAdminListWidget(infoBean?.adminList),
            )
          ],
        );
      },
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
        title: widget?.terminalPreviousBean?.bindflg == "00"? "绑定" : "管理", isShowBack: true, rightWidget: _toSaveButtonWidget());
  }

  Widget _toSaveButtonWidget() {
    return ValueListenableBuilder(
      valueListenable: _uploadValueNotifier,
      builder: (BuildContext context, BindingTermialEditUploadBean uploadBean,
          Widget child) {
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: uploadBean?.isAlive() ?? false,
          width: 48,
          height: 24,
          unSelectBgColor:
              ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
              fontWeight: FontWeight.w600),
          selectedTextStyle: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          text: "保存",
          onTap: (){
            print("点击");
            String msg = uploadBean?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              _viewModel?.saveTerminalOrBind(context, uploadBean);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

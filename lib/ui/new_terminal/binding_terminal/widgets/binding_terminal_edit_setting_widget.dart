import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_edit_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_type.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/new_terminal_response_bean.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'binding_terminal_auto_switch_dialog.dart';

/// 绑定终端-设置编辑组件
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 9:47 AM
/// @specialDemand:
class BindingTerminalEditSettingWidget extends StatefulWidget {
  final ValueNotifier<BindingTermialEditUploadBean> onChanged;
  final TerminalInfoBean terminalInfoBean;

  final NewTerminalListItemBean terminalPreviousBean;

  const BindingTerminalEditSettingWidget(
      this.terminalInfoBean, this.terminalPreviousBean, this.onChanged);

  @override
  _BindingTerminalEditSettingWidgetState createState() =>
      _BindingTerminalEditSettingWidgetState();
}

class _BindingTerminalEditSettingWidgetState
    extends BaseState<BindingTerminalEditSettingWidget> {
  ///名字编辑控制器
  TextEditingController _nameEditingController;

  ///摆放位置编辑控制器
  TextEditingController _positionEditingController;

  ///备注说明控制器
  TextEditingController _descEditingController;

  BindingTermialEditUploadBean _uploadBean;

  ///是否启用人脸显示屏
  bool isOpenFaceRecognition = true;

  @override
  void initState() {
    super.initState();
    _uploadBean = BindingTermialEditUploadBean();
    _uploadBean
      ..address = widget?.terminalInfoBean?.position
      ..cameraStatusType =
      BindingTerminalCameraStatusTypeExtension.getStrToType(
          widget?.terminalInfoBean?.closeCamera)
      ..startDateTime = _uploadBean.getStartOrEndTime(
          widget?.terminalInfoBean?.autoOnoffTime, 0)
      ..endDateTime = _uploadBean.getStartOrEndTime(
          widget?.terminalInfoBean?.autoOnoffTime, 1)
      ..backup = widget?.terminalInfoBean?.backup
      ..gps = ""
      ..hsn = widget?.terminalInfoBean?.hsn
      ..is_bind = widget?.terminalPreviousBean?.getEditsBindType() ?? ""
      ..mid = widget?.terminalInfoBean?.mid
      ..rcaid = widget?.terminalInfoBean?.rcaid
      ..terminal_name = widget?.terminalInfoBean?.terminalName
      ..terminal_picurl = widget?.terminalInfoBean?.terminalPicurl;

    _uploadBean.autoOffStatusType =
    _uploadBean.startDateTime == null || _uploadBean.endDateTime == null
        ? BindingTerminalAutoOffStatusType.unSetting
        : BindingTerminalAutoOffStatusType.setting;

    _uploadBean.deviceType = BindingTerminalDeviceType.vertical;
    _uploadBean.screenSizeType = BindingTerminalScreenSizeType.small;
    _uploadBean.screenOrientationType = BindingTerminalScreenOrientationType.vertical;

    ///这块代码冗余 容易出bug 小心
    _nameEditingController = TextEditingController()
      ..addListener(() => _notifyEditsChange())
      ..text = widget?.terminalInfoBean?.terminalName;
    _positionEditingController = TextEditingController()
      ..addListener(() => _notifyEditsChange())
      ..text = widget?.terminalInfoBean?.position;
    _descEditingController = TextEditingController()
      ..addListener(() => _notifyEditsChange())
      ..text = widget?.terminalInfoBean?.backup;

    _notifyEditsChange();
  }

  @override
  void dispose() {
    _nameEditingController?.dispose();
    _positionEditingController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 10,),
        _toAboutDeviceWidget(),//摆放照片
        _toDriverWidget(),
        _toNameSettingWidget(),//终端命名
        _toDriverWidget(),
        _toPositionSettingWidget(),//摆放位置
        _toDriverWidget(),
        _toScreenTypeWidget(),//屏幕规格
        _toDriverWidget(),
        _toScreenOrientationWidget(),//屏幕方向
        _toDriverWidget(),
        _toDeviceTypeWidget(),//设备类型
        _toDriverWidget(),
        _toFaceRecognitionWidget(),//启用人脸识别
        // _toDriverWidget(),
        // _toDescSettingWidget(),
        // _toDriverWidget(),
        // _toAutoSwitchWidget(),
      ],
    );
  }

  ///通知编辑框更新
  _notifyEditsChange() {
    if (widget?.onChanged == null) {
      return;
    }
    if (_uploadBean == null) {
      _uploadBean = BindingTermialEditUploadBean();
    }
    _uploadBean
      ..terminal_name = _nameEditingController?.text?.trim()
      ..address = _positionEditingController?.text?.trim()
      ..backup = _descEditingController?.text?.trim();
    Future(() {
      widget?.onChanged?.value = _uploadBean;
      print(_uploadBean?.toString());
    });
  }

  ///通知
  _notifyChange() {
    if (widget?.onChanged == null) {
      return;
    }
    Future(() {
      widget?.onChanged?.value = _uploadBean;
      print(_uploadBean?.toString());
    });
  }

  ///分割线
  Widget _toDriverWidget() {
    return SizedBox(
        height: 15
    );
  }

  Widget _toAboutDeviceWidget() {
    return Container(
      height: 90,
      margin: EdgeInsets.only(left: 15),
      child: Row(
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => _chooseSinglePicAndClip(),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Container(
                width: 90,
                height: 90,
                child: VgCacheNetWorkImage(
                  _uploadBean?.terminal_picurl ?? "",
                  emptyWidget:
                  Image.asset("images/binding_terminal_add_pic_ico.png"),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: Text(
              "上传终端显示屏摆放场景照片（选填）",
              style: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _chooseSinglePicAndClip() async {
    String fileUrl =
    await MatisseUtil.clipOneImage(context, scaleX: 1, scaleY: 1,               onRequestPermission: (msg)async{
      bool result =
      await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: msg,
        cancelText: "取消",
        confirmText: "去授权",
      );
      if (result ?? false) {
        PermissionUtil.openAppSettings();
      }
    });
    if (fileUrl == null || fileUrl == "") {
      return null;
    }
    _uploadBean?.terminal_picurl = fileUrl;
    setState(() {});
    _notifyChange();
  }

  ///名字设置
  Widget _toNameSettingWidget() {
    return Container(
      height: 45,
      margin: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      ),
      child: Row(
        children: <Widget>[
          Container(
            width: 15,
            alignment: Alignment(2 / 5, 0),
            padding: const EdgeInsets.only(top: 5),
            child: Text(
              "*",
              style: TextStyle(
                  color:
                  ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "终端命名",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Expanded(
            child: VgTextField(
              controller: _nameEditingController,
              maxLines: 1,
              maxLimitLength: 10,
              limitCallback: (int maxLimitLength) {
                VgToastUtils.toast(context, "最多${maxLimitLength ?? 0}个字～");
              },
              scrollPhysics: BouncingScrollPhysics(),
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(bottom: 2),
                hintText: "请输入（10个字以内）" ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  color:
                  ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
            ),
          )
        ],
      ),
    );
  }

  ///摆放位置设置
  Widget _toPositionSettingWidget() {
    return Container(
      height: 45,
      margin: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      ),
      child: Row(
        children: <Widget>[
          Container(
            width: 15,
            alignment: Alignment(2 / 5, 0),
            padding: const EdgeInsets.only(top: 5),
            child: Text(
              "*",
              style: TextStyle(
                  color:
                  ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "摆放位置",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Expanded(
            child: VgTextField(
              controller: _positionEditingController,
              minLines: 1,
              maxLines: 2,
              // maxLimitLength: 10,
              // limitCallback: (int maxLimitLength){
              //   VgToastUtils.toast(context, "最多${maxLimitLength ?? 0}个字～");
              // },
              scrollPhysics: BouncingScrollPhysics(),
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(bottom: 2),
                hintText: "请输入" ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  height: 1.2,
                  color:
                  ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
            ),
          ),
        ],
      ),
    );
  }

  ///屏幕规格
  Widget _toScreenTypeWidget() {
    return Container(
      height: 45,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      ),
      child: Row(
        children: <Widget>[
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "屏幕规格",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          GestureDetector(
            onTap: (){
              _uploadBean.screenSizeType = BindingTerminalScreenSizeType.big;
              setState(() {});
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _uploadBean.screenSizeType == BindingTerminalScreenSizeType.big
                        ?"images/icon_white_selected.png"
                    :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "大屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: _uploadBean.screenSizeType == BindingTerminalScreenSizeType.big
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 40,
          ),
          GestureDetector(
            onTap: (){
              _uploadBean.screenSizeType = BindingTerminalScreenSizeType.small;
              setState(() {});
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _uploadBean.screenSizeType == BindingTerminalScreenSizeType.small
                        ?"images/icon_white_selected.png"
                        :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "小屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: _uploadBean.screenSizeType.getParamsValue() == BindingTerminalScreenSizeType.small
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///屏幕方向
  Widget _toScreenOrientationWidget() {
    return Container(
      height: 45,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      ),
      child: Row(
        children: <Widget>[
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "屏幕方向",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          GestureDetector(
            onTap: (){
              _uploadBean.screenOrientationType = BindingTerminalScreenOrientationType.vertical;
              setState(() {});
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _uploadBean.screenOrientationType == BindingTerminalScreenOrientationType.vertical
                        ?"images/icon_white_selected.png"
                        :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "竖屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: _uploadBean.screenOrientationType == BindingTerminalScreenOrientationType.vertical
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 40,
          ),
          GestureDetector(
            onTap: (){
              _uploadBean.screenOrientationType = BindingTerminalScreenOrientationType.horizontal;
              setState(() {});
            },
            child: Row(
              children: <Widget>[
                AnimatedSwitcher(
                  duration: DEFAULT_ANIM_DURATION,
                  child: Image.asset(
                    _uploadBean.screenOrientationType == BindingTerminalScreenOrientationType.horizontal
                        ?"images/icon_white_selected.png"
                        :"images/icon_white_unselected.png",
                    width: 20,
                    height: 20,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "横屏",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    // ignore: unrelated_type_equality_checks
                      color: _uploadBean.screenOrientationType == BindingTerminalScreenOrientationType.horizontal
                          ? ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7()
                          : Color(0XFF5E687C),
                      fontSize: 14,
                      height: 1.2),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///设备类型
  Widget _toDeviceTypeWidget(){
    return                 //自动开关机
      GestureDetector(
        onTap: (){
          return SelectUtil.showListSelectDialog(
            context: context,
            title: "设备类型",
            positionStr: _uploadBean.deviceType.getTypeToStr(),
            textList: BindingTerminalDeviceTypeExtension.getListStr(),
              onSelect: (dynamic value) {
                _uploadBean.deviceType = BindingTerminalDeviceTypeExtension.getStrToType(value);
                setState(() {});
              }
          );
        },
        child: Container(
          height: 45,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          ),
          child: Row(
            children: <Widget>[
              Text(
                "设备类型",
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                    fontSize: 14
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Text(
                _uploadBean.deviceType.getTypeToStr(),
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 14
                ),
              ),
              Spacer(),
              Image.asset(
                "images/index_arrow_ico.png",
                width: 6,
                height: 11,
              ),
            ],
          ),
        ),
      );
  }

  ///启用人脸识别
  Widget _toFaceRecognitionWidget(){
    return
      GestureDetector(
        onTap: (){
          isOpenFaceRecognition = !isOpenFaceRecognition;
          _uploadBean?.faceRecognitionType = isOpenFaceRecognition?FaceRecognitionType.open:FaceRecognitionType.close;
          _notifyChange();
          setState(() {});
        },
        child: Container(
          height: 45,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          ),
          child: Row(
            children: <Widget>[
              Text(
                "启用人脸识别",
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                    fontSize: 14
                ),
              ),
              Spacer(),
              Image.asset(
                isOpenFaceRecognition
                    ? "images/icon_open.png"
                    :"images/icon_close.png",
                width: 36,
              ),
            ],
          ),
        ),
      );
  }

  ///备注设置说明
  Widget _toDescSettingWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 15, bottom: 15, right: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Opacity(
            opacity: 0,
            child: Container(
              width: 15,
              alignment: Alignment(2 / 5, 0),
              padding: const EdgeInsets.only(top: 5),
              child: Text(
                "*",
                style: TextStyle(
                    color:
                    ThemeRepository.getInstance().getMinorRedColor_F95355(),
                    fontSize: 14,
                    height: 1.2),
              ),
            ),
          ),
          Container(
            width: 71,
            alignment: Alignment.centerLeft,
            child: Text(
              "备注说明",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 14,
                  height: 1.2),
            ),
          ),
          Expanded(
            child: VgTextField(
              controller: _descEditingController,
              maxLines: 3,
              minLines: 3,
              maxLimitLength: 100,
              scrollPhysics: BouncingScrollPhysics(),
              limitCallback: (int maxLimitLength) {
                VgToastUtils.toast(context, "最多${maxLimitLength ?? 0}个字～");
              },
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(bottom: 0),
                hintText: "请输入（100字以内）" ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  height: 1.2,
                  color:
                  ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
            ),
          )
        ],
      ),
    );
  }

  ///自动开关设置组件
  Widget _toAutoSwitchWidget() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        Map<String, dynamic> resultMap =
        await BindingTerminalAutoSwitchDialog.navigatorPushDialog(
            context,
            _uploadBean.startDateTime,
            _uploadBean.endDateTime,
            _uploadBean.cameraStatusType ==
                BindingTerminalCameraStatusType.open);
        if (resultMap == null || resultMap.isEmpty) {
          return;
        }
        _uploadBean.startDateTime =
        resultMap[BINDING_TERMINAL_POP_START_DATE_TIME_KEY];
        _uploadBean.endDateTime =
        resultMap[BINDING_TERMINAL_POP_END_DATE_TIME_KEY];
        _uploadBean.autoOffStatusType =
        _uploadBean.startDateTime == null || _uploadBean.endDateTime == null
            ? BindingTerminalAutoOffStatusType.unSetting
            : BindingTerminalAutoOffStatusType.setting;
        _uploadBean.cameraStatusType =
        resultMap[BINDING_TERMINAL_POP_IS_ENABLE_SYNC_KEY]
            ? BindingTerminalCameraStatusType.open
            : BindingTerminalCameraStatusType.close;
        _notifyChange();
        setState(() {});
      },
      child: Container(
        height: 50,
        child: Row(
          children: <Widget>[
            Opacity(
              opacity: 0,
              child: Container(
                width: 15,
                alignment: Alignment(2 / 5, 0),
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  "*",
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getMinorRedColor_F95355(),
                      fontSize: 14,
                      height: 1.2),
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "自动开关机",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextMinorGreyColor_808388(),
                    fontSize: 14,
                    height: 1.2),
              ),
            ),
            Spacer(),
            Text(
              _getAutoTimeStr() ?? "暂未设置",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: _uploadBean.startDateTime == null ||
                      _uploadBean.endDateTime == null
                      ? ThemeRepository.getInstance()
                      .getTextEditHintColor_3A3F50()
                      : Colors.white,
                  fontSize: 14,
                  height: 1.2),
            ),
            // SizedBox(width: 11,),
            Container(
              margin: const EdgeInsets.only(left: 3),
              child: Icon(
                Icons.keyboard_arrow_down,
                color: VgColors.INPUT_BG_COLOR,
              ),
            ),
            SizedBox(
              width: 4,
            )
          ],
        ),
      ),
    );
  }

  String _getAutoTimeStr() {
    if (_uploadBean.endDateTime == null || _uploadBean.startDateTime == null) {
      return null;
    }

    return "${VgToolUtils.twoDigits(_uploadBean.startDateTime.hour)}:${VgToolUtils.twoDigits(_uploadBean.startDateTime.minute)}" +
        "至" +
        "${VgToolUtils.twoDigits(_uploadBean.endDateTime.hour)}:${VgToolUtils.twoDigits(_uploadBean.endDateTime.minute)}";
  }
}

/// 标题和内容行组件
///
/// @author: zengxiangxi
/// @createTime: 1/15/21 4:56 PM
/// @specialDemand:
class _TitleAndContentWidget extends StatelessWidget {
  final String title;

  final String content;

  const _TitleAndContentWidget({
    Key key,
    @required this.title,
    @required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 64,
          alignment: Alignment.centerLeft,
          child: Text(
            title ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color:
              ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          width: 2,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            child: Text(
              content ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ),
        )
      ],
    );
  }
}

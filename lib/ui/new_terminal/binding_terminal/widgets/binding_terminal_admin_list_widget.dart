import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_name_and_nick_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_label_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 绑定终端-管理员列表
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 10:59 AM 
/// @specialDemand:
class BindingTerminalAdminListWidget extends StatelessWidget {

  final List<AdminListBean> list;
  const BindingTerminalAdminListWidget(this.list);

  @override
  Widget build(BuildContext context) {
    return _toListWidget();
  }

  Widget _toListWidget() {
    return ListView.separated(
        itemCount:list?.length ?? 0,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(list?.elementAt(index));
        },
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(height: 0,);
        }
    );
  }

  Widget _toListItemWidget(AdminListBean itemBean) {
    return Container(
      height: 64,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: _toMainRowWidget(itemBean),
    );
  }

  Widget _toMainRowWidget(AdminListBean itemBean) {
    return Row(
      children: <Widget>[
        ClipOval(
          child: Container(
            width: 40,
            height: 40,
            child: VgCacheNetWorkImage(
              itemBean?.napicurl ?? "",
              fit: BoxFit.cover,
              imageQualityType: ImageQualityType.middleDown,
              defaultErrorType: ImageErrorType.head,
              defaultPlaceType: ImagePlaceType.head,
            ),
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          child:  _toColumnWidget(itemBean),
        )
      ],
    );
  }

  Widget _toColumnWidget(AdminListBean itemBean) {
    return Container(
      height: 40,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CommonNameAndNickWidget(
                name: itemBean?.nick ??"-",
              ),
              SizedBox(width: 4,),
              VgLabelUtils.getRoleLabel(itemBean?.roleid) ?? Container(),
              VgLabelUtils.getCurrentLoginDeviceLabel(itemBean?.loginflg) ?? Container(),
              Spacer(),
            ],
          ),
          Row(
            children: <Widget>[
               Text(
                         itemBean?.loginphone ?? "-",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                           color: VgColors.INPUT_BG_COLOR,
                             fontSize: 12,
                             ),
                       ),
              Spacer(),
               Text(
                         "登录APP·3天前",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                           color: VgColors.INPUT_BG_COLOR,
                           fontSize: 11,
                             ),
                       )
            ],
          )
        ],
      ),
    );
  }
}

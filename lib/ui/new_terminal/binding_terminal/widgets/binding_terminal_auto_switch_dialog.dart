import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_date_picker/vg_date_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_widget_lib.dart';

///pop返回值Map开始日期Key
const String BINDING_TERMINAL_POP_START_DATE_TIME_KEY = "startDateTime";

///pop返回值Map截止日期Key
const String BINDING_TERMINAL_POP_END_DATE_TIME_KEY = "endDateTime";

///pop返回值Map是否开启同步Key
const String BINDING_TERMINAL_POP_IS_ENABLE_SYNC_KEY = "isEnableSync";

const int DEFAULT_START_HOUR = 8;
const int DEFAULT_END_HOUR = 20;
const bool DEFAULT_IS_ENABLE_SYNC = false;

/// 绑定终端-自动开关设置弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 11:33 AM
/// @specialDemand:
class BindingTerminalAutoSwitchDialog extends StatefulWidget {
  final DateTime startDateTime;

  final DateTime endDateTime;

  final bool isEnableSync;

  final bool hideBottom;

  const BindingTerminalAutoSwitchDialog({
    Key key,
    this.startDateTime,
    this.endDateTime, this.isEnableSync = DEFAULT_IS_ENABLE_SYNC,
   this.hideBottom,
  }) : super(key: key);

  @override
  _BindingTerminalAutoSwitchDialogState createState() =>
      _BindingTerminalAutoSwitchDialogState();

  ///跳转方法
  ///返回Map
  static Future<Map<String,dynamic>> navigatorPushDialog(
      BuildContext context, DateTime startDateTime, DateTime endDateTime,
      bool isEnableSync, {bool hideBottom}) {
    VgToolUtils.removeAllFocus(context);
    return VgDialogUtils.showCommonBottomDialog<Map<String,dynamic>>(
      context: context,
      child: BindingTerminalAutoSwitchDialog(
        startDateTime: startDateTime,
        endDateTime: endDateTime,
        isEnableSync: isEnableSync,
        hideBottom: hideBottom,
      ),
    );
  }
}

class _BindingTerminalAutoSwitchDialogState
    extends State<BindingTerminalAutoSwitchDialog> {
  DateTime _startDateTime;

  DateTime _endDateTime;

  bool _isEnableSync;

  @override
  void initState() {
    super.initState();
    _isEnableSync = widget?.isEnableSync;
    final DateTime nowDateTime = DateTime.now();
    _startDateTime = widget?.startDateTime ??
        DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,
            DEFAULT_START_HOUR);
    _endDateTime = widget?.endDateTime ??
        DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,
            DEFAULT_END_HOUR);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS))),
      child: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _toTopFunctionWidget(),
        Container(
          height: 175,
          child: _toTimeChooseWidget(),
        ),
        // _toBottomSwitchWidget(),
        SizedBox(
          height: ScreenUtils.getBottomBarH(context),
        )
      ],
    );
  }

  Widget _toTopFunctionWidget() {
    return Container(
      height: 54,
      child: Row(
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => RouterUtils.pop(context),
            child: Container(
              width: 58,
              child: Center(
                child: Text(
                  "取消",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: 0.5,
            height: 12,
            color: ThemeRepository.getInstance().getLineColor_3A3F50(),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => _processClearPop(),
            child: Container(
              width: 86,
              child: Center(
                child: Text(
                  "清除设置",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
          Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => _processConfirmPop(),
            child: Container(
              width: 60,
              child: Center(
                child: Text(
                  "确定",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toBottomSwitchWidget() {
    return Visibility(
      visible: !(widget?.hideBottom??false),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: (){
          _isEnableSync = !_isEnableSync;
          setState(() {

          });
        },
        child: Container(
          height: 50,
          child: _RadioWithTextWidget(
            text: "同步开启/关闭摄像头",
            isSelected: _isEnableSync ?? false,
          ),
        ),
      ),
    );
  }

  ///时间选择
  Widget _toTimeChooseWidget() {
    return CupertinoTheme(
      data: CupertinoThemeData(
          textTheme: CupertinoTextThemeData(
              dateTimePickerTextStyle: TextStyle(color: Colors.white))),
      child: Row(
        children: <Widget>[
          Expanded(
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.time,
              use24hFormat: true,
              initialDateTime: _startDateTime,
              onDateTimeChanged: (DateTime newStartDateTime) {
                _startDateTime = newStartDateTime;
              },
            ),
          ),
          Text(
            "至",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white, fontSize: 18, height: 1.2),
          ),
          Expanded(
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.time,
              use24hFormat: true,
              initialDateTime: _endDateTime,
              onDateTimeChanged: (DateTime newEndDateTime) {
                _endDateTime = newEndDateTime;
              },
            ),
          ),
        ],
      ),
    );
  }

  void _processConfirmPop() {
    if (_startDateTime == null || _endDateTime == null) {
      VgToastUtils.toast(context, "保存时间错误！");
      Future.delayed(Duration(milliseconds: 1000), () {
        RouterUtils.pop(context);
      });
      return;
    }
    if (_startDateTime.isAfter(_endDateTime)) {
      VgToastUtils.toast(context, "起止时间不正确～");
      return;
    }

    if (_startDateTime.compareTo(_endDateTime) == 0) {
      VgToastUtils.toast(context, "起止时间不能相同～");
      return;
    }

    RouterUtils.pop(context, result: {
      BINDING_TERMINAL_POP_START_DATE_TIME_KEY: _startDateTime,
      BINDING_TERMINAL_POP_END_DATE_TIME_KEY: _endDateTime,
      BINDING_TERMINAL_POP_IS_ENABLE_SYNC_KEY:_isEnableSync
    });
  }

  void _processClearPop() {
    RouterUtils.pop(context, result: {
      BINDING_TERMINAL_POP_START_DATE_TIME_KEY: null,
      BINDING_TERMINAL_POP_END_DATE_TIME_KEY: null,
      BINDING_TERMINAL_POP_IS_ENABLE_SYNC_KEY:DEFAULT_IS_ENABLE_SYNC,
    });
  }
}

/// 单选带文字的组件
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 11:45 AM
/// @specialDemand:
class _RadioWithTextWidget extends StatelessWidget {
  final String text;

  final bool isSelected;

  const _RadioWithTextWidget({Key key, this.text, this.isSelected = DEFAULT_IS_ENABLE_SYNC})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CommonSingleChoiceButtonWidget(
              isSelected ? CommonSingleChoiceStatus.selected:CommonSingleChoiceStatus.unSelect
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              text ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 13,
                  height: 1.2),
            )
          ],
        ),
      ),
    );
  }
}

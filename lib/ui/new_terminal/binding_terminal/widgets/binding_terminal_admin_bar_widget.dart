import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/add_device_administrator_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 绑定终端-管理员栏
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 10:49 AM
/// @specialDemand:
class BindingTerminalAdminBarWidget extends StatelessWidget {
  final int total;
  // final String hsn;

  const BindingTerminalAdminBarWidget({Key key, this.total}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
           Text(
                     "管理员·${total ?? 0}",
                     maxLines: 1,
                     overflow: TextOverflow.ellipsis,
                     style: TextStyle(
                         color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                         fontSize: 15,
                       fontWeight: FontWeight.w600,
                       height: 1.2
                         ),
                   ),
          Spacer(),
          // Row(
          //   mainAxisSize: MainAxisSize.min,
          //   children: <Widget>[
          //     Icon(
          //       Icons.add,
          //       size: 15,
          //       color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          //     ),
          //      Text(
          //                "添加管理员",
          //                maxLines: 1,
          //                overflow: TextOverflow.ellipsis,
          //                style: TextStyle(
          //                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          //                    fontSize: 14,
          //                  height: 1.2
          //                    ),
          //              )
          //   ],
          // )
        ],
      ),
    );
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/show_terminal_address_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/terminal_basic_info_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'bean/bind_terminal_info_confirm_response_bean.dart';

/// 终端信息确认页面
class BindTerminalInfoConfirmPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "BindTerminalInfoConfirmPage";

  final String hsn;
  final String defaultName;
  final String defaultPosition;
  final String router;

  const BindTerminalInfoConfirmPage({Key key, this.hsn, this.defaultName, this.defaultPosition, this.router}) : super(key: key);

  @override
  _BindTerminalInfoConfirmPageState createState() =>
      _BindTerminalInfoConfirmPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn, {String defaultName,
    String defaultPosition, String router}) {
    return RouterUtils.routeForFutureResult(
      context,
      BindTerminalInfoConfirmPage(
        hsn: hsn,
        defaultName: defaultName,
        defaultPosition: defaultPosition,
        router: router,
      ),
      routeName: BindTerminalInfoConfirmPage.ROUTER,
    );
  }
}

class _BindTerminalInfoConfirmPageState
    extends BaseState<BindTerminalInfoConfirmPage> {
  BindingTerminalViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = BindingTerminalViewModel(this);
    _viewModel.getBindingTerminalConfirmInfo(context, widget?.hsn);
    Future(() {
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toInfoWidget(),
    );
  }

  Widget _toInfoWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.confirmInfoValueNotifier,
      builder: (BuildContext context, BindTerminalInfoConfirmBean dataBean,
          Widget child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _toTopBarWidget(),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                "以下终端显示屏请求绑定，请确认信息：",
                maxLines: 1,
                style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 13,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                "${VgStringUtils.setHiddenSeparatorForStr("设备ID：${dataBean?.hsn??""}")}",
                style: TextStyle(
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 20,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Flexible(
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: <Widget>[
                    _TitleAndContentWidget(
                      title: "登录账号",
                      content:
                      "${dataBean?.phone ?? ""} ${dataBean?.name ?? "-"}",
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    _TitleAndContentWidget(
                      title: "所属公司",
                      content: dataBean?.companynick ?? "-",
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Expanded(
                          child: _TitleAndContentWidget(
                            title: "GPS位置",
                            content: dataBean?.address ?? "-",
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Container(
                          color: ThemeRepository.getInstance()
                              .getLineColor_3A3F50(),
                          width: 0.5,
                          height: 25,
                        ),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: (){
                            if(StringUtils.isNotEmpty((dataBean?.gps??""))){
                              TerminalInfoBean terminalInfoBean = new TerminalInfoBean();
                              terminalInfoBean.address = dataBean.address??"";
                              terminalInfoBean.gps = dataBean.gps??"";
                              terminalInfoBean.terminalName = dataBean.terminalName??"新终端";
                              ShowTerminalAddressPage.navigatorPush(context, terminalInfoBean);
                            }else{
                              VgToastUtils.toast(context, "gps位置信息异常");
                            }

                          },
                          child: Container(
                            width: 56,
                            child: Image.asset(
                              "images/binding_terminal_edit_setting_location_ico.png",
                              width: 24,
                              height: 24,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            GestureDetector(
              child: Container(
                height: 40,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "绑定",
                    style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 15,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color:
                    ThemeRepository.getInstance().getPrimaryColor_1890FF()),
              ),
              onTap: () async{
                _viewModel.beginBindTerminal(widget?.hsn);
                dataBean.flg = "00";
                dataBean.terminalName = widget?.defaultName??"";
                dataBean.position = widget?.defaultPosition??"";
                dynamic result = await TerminalBasicInfoPage.navigatorPush(context, dataBean, widget?.router);
                if(result == null){
                  _viewModel.exitBindTerminal(widget?.hsn);
                }
              },
            ),
            SizedBox(
              height: 30 + ScreenUtils.getBottomBarH(context),
            ),
          ],
        );
      },
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "",
      isShowBack: true,
    );
  }
}

/// 标题和内容行组件
class _TitleAndContentWidget extends StatelessWidget {
  final String title;

  final String content;

  const _TitleAndContentWidget(
      {Key key, @required this.title, @required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: 64,
          alignment: Alignment.centerLeft,
          child: Text(
            title ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color:
              ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 14,
            ),
          ),
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            child: Text(
              content ?? "",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ),
        )
      ],
    );
  }
}

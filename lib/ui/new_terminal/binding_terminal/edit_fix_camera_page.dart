import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'bean/binding_terminal_edit_upload_bean.dart';
import 'bean/binding_terminal_response_bean.dart';
import 'bind_terminal_more_settings_page.dart';

/// 编辑摄像头校正页面
class EditFixCameraPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "FixCameraPage";
  final TerminalInfoBean terminalInfoBean;
  const EditFixCameraPage({Key key, this.terminalInfoBean})
      : super(key: key);

  @override
  _EditFixCameraPageState createState() => _EditFixCameraPageState();

  ///跳转方法
  static Future<bool> navigatorPush(
      BuildContext context, TerminalInfoBean terminalInfoBean) {
    return RouterUtils.routeForFutureResult(
      context,
      EditFixCameraPage(
        terminalInfoBean: terminalInfoBean,
      ),
      routeName: EditFixCameraPage.ROUTER,
    );
  }
}

class _EditFixCameraPageState extends BaseState<EditFixCameraPage> {

  BindingTerminalViewModel _viewModel;
  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;

  @override
  void initState() {
    super.initState();
    _viewModel = BindingTerminalViewModel(this);
    _viewModel.notifyTerminal(widget?.terminalInfoBean?.hsn);
    _uploadValueNotifier = ValueNotifier(null);

    Future((){
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toInfoWidget(),
    );
  }

  Widget _toInfoWidget(){
    return ValueListenableBuilder(
      valueListenable: _viewModel?.infoValueNotifier,
      builder: (BuildContext context, BindingTerminalDataBean dataBean, Widget child){
        return Column(
          children: <Widget>[
            _toTopBarWidget(),
            Container(
              alignment: Alignment.center,
              child: Text(
                "调整摄像头至合理拍摄角度",
                style: TextStyle(
                    fontSize: 14,
                    color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
                ),
              ),
            ),
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                    ///00->01->02
                    if(widget?.terminalInfoBean?.cameratype == "00"){
                      widget?.terminalInfoBean?.cameratype = "01";
                    }else if(widget?.terminalInfoBean?.cameratype == "01"){
                      widget?.terminalInfoBean?.cameratype = "02";
                    }else {
                      widget?.terminalInfoBean?.cameratype = "00";
                    }
                    _viewModel.fixCamera(widget?.terminalInfoBean?.mid, widget?.terminalInfoBean?.id,
                        widget?.terminalInfoBean?.hsn, "01",
                        widget?.terminalInfoBean?.correctCamera, widget?.terminalInfoBean?.cameratype);
                    print("镜像调整");
                  },
                  child: Column(
                    children: [
                      Image.asset(
                        "images/icon_mirror.png",
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(height: 8,),
                      Text(
                        "镜像",
                        style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 60,
                ),
                GestureDetector(
                  onTap: (){
                    print("旋转摄像头");
                    ///0->90->180->270
                    if(widget?.terminalInfoBean?.correctCamera == 0){
                      widget?.terminalInfoBean?.correctCamera = 90;
                    }else if(widget?.terminalInfoBean?.correctCamera == 90){
                      widget?.terminalInfoBean?.correctCamera = 180;
                    }else if(widget?.terminalInfoBean?.correctCamera == 180){
                      widget?.terminalInfoBean?.correctCamera = 270;
                    }else{
                      widget?.terminalInfoBean?.correctCamera = 0;
                    }
                    _viewModel.fixCamera(widget?.terminalInfoBean?.mid, widget?.terminalInfoBean?.id,
                        widget?.terminalInfoBean?.hsn, "01",
                        widget?.terminalInfoBean?.correctCamera, widget?.terminalInfoBean?.cameratype);
                  },
                  child: Column(
                    children: [
                      Image.asset(
                        "images/icon_fix_camera.png",
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(height: 8,),
                      Text(
                        "旋转",
                        style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Spacer(),
            GestureDetector(
              child: Container(
                height: 40,
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "保存",
                    style: TextStyle(
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 15,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                ),
              ),
              onTap: (){
                _viewModel.setCameraSettings(context, widget?.terminalInfoBean?.mid, widget?.terminalInfoBean?.id,
                    widget?.terminalInfoBean?.hsn, "01",
                    widget?.terminalInfoBean?.correctCamera, widget?.terminalInfoBean?.cameratype);
              },
            ),
            SizedBox(
              height: 30 + ScreenUtils.getBottomBarH(context),
            ),
          ],
        );
      },
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "摄像头校正",
      isShowBack: true,
      // rightWidget: CommonFixedHeightConfirmButtonWidget(
      //   isAlive: true,
      //   width: 48,
      //   height: 24,
      //   unSelectBgColor:
      //   ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
      //   selectedBgColor:
      //   ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      //   unSelectTextStyle: TextStyle(
      //       color: VgColors.INPUT_BG_COLOR,
      //       fontSize: 12,
      //       fontWeight: FontWeight.w600),
      //   selectedTextStyle: TextStyle(
      //       color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
      //   text: "保存",
      //   onTap: (){
      //
      //     _viewModel.setCameraSettings(context, widget?.terminalInfoBean?.mid, widget?.terminalInfoBean?.id,
      //         widget?.terminalInfoBean?.hsn, "01",
      //         widget?.terminalInfoBean?.correctCamera, widget?.terminalInfoBean?.cameratype);
      //   },
      // ),
    );
  }
}

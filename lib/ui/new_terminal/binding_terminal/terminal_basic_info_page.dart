import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/bind_terminal_info_confirm_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_edit_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/binding_terminal_confirm_edit_setting_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'fix_camera_page.dart';

///显示屏基本信息页面

class TerminalBasicInfoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "TerminalBasicInfoPage";

  final BindTerminalInfoConfirmBean confirmBean;
  final String router;

  const TerminalBasicInfoPage({Key key, this.confirmBean, this.router})
      : super(key: key);


  @override
  _TerminalBasicInfoPageState createState() => _TerminalBasicInfoPageState();

  ///跳转方法
  static Future<bool> navigatorPush(
      BuildContext context, BindTerminalInfoConfirmBean confirmBean, String router) {
    return RouterUtils.routeForFutureResult(
      context,
      TerminalBasicInfoPage(
        confirmBean: confirmBean,
        router: router,
      ),
      routeName: TerminalBasicInfoPage.ROUTER,
    );
  }
}

class _TerminalBasicInfoPageState extends BaseState<TerminalBasicInfoPage> {
  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;

  @override
  void initState() {
    super.initState();
    _uploadValueNotifier = ValueNotifier(null);
    Future(() {
      VgToolUtils.removeAllFocus(context);
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(
          isEnableVerical: true,
          child: Column(children: [
            _toTopBarWidget(),
            Expanded(child: _toMainColumnWidget())
          ])),
    );
  }


  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        BindingTerminalConfirmEditSettingWidget(
            widget?.confirmBean, _uploadValueNotifier),
        Spacer(),
        _toSaveButtonWidget(),
        SizedBox(
          height: 30 + ScreenUtils.getBottomBarH(context),
        ),
      ],
    );
  }

  Widget _toSaveButtonWidget(){
    return ValueListenableBuilder(
      valueListenable: _uploadValueNotifier,
      builder: (BuildContext context, BindingTermialEditUploadBean uploadBean,
          Widget child) {
        return GestureDetector(
          onTap: (){
            String msg = uploadBean.checkVerify();
            if(StringUtils.isEmpty(msg)){
              FixCameraPage.navigatorPush(context, widget?.confirmBean?.hsn, _uploadValueNotifier.value, widget?.router);
            }
            VgToastUtils.toast(context, msg);
          },
          child: Container(
            height: 40,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: uploadBean?.isAlive()??false
                    ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    : ThemeRepository.getInstance().getLineColor_3A3F50()
            ),
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "下一步",
                style: TextStyle(
                  color: uploadBean?.isAlive()??false
                      ?ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                      :ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C(),
                  fontSize: 15,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
        title: "显示屏基本信息", isShowBack: true);
  }
}

import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"adminList":[{"nick":"岁0225改","hsn":"f76e9ae227c727b4","lasttime":1615166497,"hsncnt":1,"roleid":"90","napicurl":"","loginphone":"15100000000","userid":"5d43c01e163b48c39e2991efaa5e44c9"},{"nick":"大兴","lasttime":1616064033,"hsncnt":10,"roleid":"99","napicurl":"http://etpic.we17.com/d05dbaca8bac4cb7832bf7eda98025cb_1614217486102-Unknown_3e1ee4fa-c69f-4b21-8f39-72533a9c73b5_.jpg","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"岁","lasttime":1616118841,"hsncnt":3,"roleid":"99","napicurl":"","loginphone":"18665289540","userid":"d5647a27c47547f38051e3cba92f5f46"},{"nick":"李新","lasttime":1615531544,"hsncnt":3,"roleid":"99","loginphone":"13571059924","userid":"d8a05405e3024951840041b85cb70934"},{"nick":"岁","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","lasttime":1612418157,"hsncnt":1,"roleid":"99","loginphone":"02935851546","userid":"dd7fe6f607344d1694c52b6b8d959f6c"},{"nick":"丹姐","lasttime":1614305311,"hsncnt":2,"roleid":"99","loginphone":"17777777777","userid":"fad6c0c9c69f4c97bad57485de3a6c99"}]}
/// extra : null

class AddDeviceAdministratorBean extends BasePagerBean<AdminListBean>{
  bool success;
  String code;
  String msg;
  DeviceAdminListDataBean data;
  dynamic extra;

  static AddDeviceAdministratorBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AddDeviceAdministratorBean addDeviceAdministratorBeanBean = AddDeviceAdministratorBean();
    addDeviceAdministratorBeanBean.success = map['success'];
    addDeviceAdministratorBeanBean.code = map['code'];
    addDeviceAdministratorBeanBean.msg = map['msg'];
    addDeviceAdministratorBeanBean.data = DeviceAdminListDataBean.fromMap(map['data']);
    addDeviceAdministratorBeanBean.extra = map['extra'];
    return addDeviceAdministratorBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<AdminListBean> getDataList() => data.adminList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data.adminList = list.cast();
  }
}

/// adminList : [{"nick":"岁0225改","hsn":"f76e9ae227c727b4","lasttime":1615166497,"hsncnt":1,"roleid":"90","napicurl":"","loginphone":"15100000000","userid":"5d43c01e163b48c39e2991efaa5e44c9"},{"nick":"大兴","lasttime":1616064033,"hsncnt":10,"roleid":"99","napicurl":"http://etpic.we17.com/d05dbaca8bac4cb7832bf7eda98025cb_1614217486102-Unknown_3e1ee4fa-c69f-4b21-8f39-72533a9c73b5_.jpg","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"岁","lasttime":1616118841,"hsncnt":3,"roleid":"99","napicurl":"","loginphone":"18665289540","userid":"d5647a27c47547f38051e3cba92f5f46"},{"nick":"李新","lasttime":1615531544,"hsncnt":3,"roleid":"99","loginphone":"13571059924","userid":"d8a05405e3024951840041b85cb70934"},{"nick":"岁","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","lasttime":1612418157,"hsncnt":1,"roleid":"99","loginphone":"02935851546","userid":"dd7fe6f607344d1694c52b6b8d959f6c"},{"nick":"丹姐","lasttime":1614305311,"hsncnt":2,"roleid":"99","loginphone":"17777777777","userid":"fad6c0c9c69f4c97bad57485de3a6c99"}]

class DeviceAdminListDataBean {
  List<AdminListBean> adminList;

  static DeviceAdminListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DeviceAdminListDataBean dataBean = DeviceAdminListDataBean();
    dataBean.adminList = List()..addAll(
      (map['adminList'] as List ?? []).map((o) => AdminListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "adminList": adminList,
  };
}

/// nick : "岁0225改"
/// hsn : "f76e9ae227c727b4"
/// lasttime : 1615166497
/// hsncnt : 1
/// roleid : "90" 90管理员  10普通用户  99超级管理员
/// napicurl : ""
/// loginphone : "15100000000"
/// userid : "5d43c01e163b48c39e2991efaa5e44c9"

class AdminListBean {
  String nick;
  String hsn;
  int applogintime;
  int hsncnt;
  String roleid;
  String napicurl;
  String loginphone;
  String userid;
  String fuid;

  static AdminListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AdminListBean adminListBean = AdminListBean();
    adminListBean.nick = map['nick'];
    adminListBean.hsn = map['hsn'];
    adminListBean.applogintime = map['applogintime'];
    adminListBean.hsncnt = map['hsncnt'];
    adminListBean.roleid = map['roleid'];
    adminListBean.napicurl = map['napicurl'];
    adminListBean.loginphone = map['loginphone'];
    adminListBean.userid = map['userid'];
    adminListBean.fuid = map['fuid'];
    return adminListBean;
  }

  Map toJson() => {
    "nick": nick,
    "hsn": hsn,
    "applogintime": applogintime,
    "hsncnt": hsncnt,
    "roleid": roleid,
    "napicurl": napicurl,
    "loginphone": loginphone,
    "userid": userid,
    "fuid": fuid,
  };
}
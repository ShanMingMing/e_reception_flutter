/// status : 0
/// message : "query ok"
/// request_id : "76371572-a5bf-11eb-951c-28c13c97997f"
/// result : {"location":{"lat":31.95266,"lng":118.84002},"address":"江苏省南京市江宁区上元大街","formatted_addresses":{"recommend":"南京市江宁区人民政府(上元大街北)","rough":"南京市江宁区人民政府(上元大街北)"},"address_component":{"nation":"中国","province":"江苏省","city":"南京市","district":"江宁区","street":"上元大街","street_number":"上元大街"},"ad_info":{"nation_code":"156","adcode":"320115","city_code":"156320100","name":"中国,江苏省,南京市,江宁区","location":{"lat":31.75,"lng":118.597328},"nation":"中国","province":"江苏省","city":"南京市","district":"江宁区"},"address_reference":{"street_number":{"id":"","title":"","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"},"business_area":{"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"},"famous_area":{"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"},"crossroad":{"id":"4008397","title":"上元大街/府前东路(路口)","location":{"lat":31.952761,"lng":118.841652},"_distance":149.1,"_dir_desc":"西"},"town":{"id":"320115001","title":"东山街道","location":{"lat":31.962538,"lng":118.784637},"_distance":0,"_dir_desc":"内"},"street":{"id":"11747961910109150138","title":"上元大街","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"},"landmark_l2":{"id":"15226929800080506919","title":"南京市江宁区人民政府","location":{"lat":31.953566,"lng":118.83976},"_distance":0,"_dir_desc":"内"}}}

class TerminalDetailLocationInfo {
  int status;
  String message;
  String requestId;
  ResultBean result;

  static TerminalDetailLocationInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalDetailLocationInfo terminalDetailLocationInfoBean = TerminalDetailLocationInfo();
    terminalDetailLocationInfoBean.status = map['status'];
    terminalDetailLocationInfoBean.message = map['message'];
    terminalDetailLocationInfoBean.requestId = map['request_id'];
    terminalDetailLocationInfoBean.result = ResultBean.fromMap(map['result']);
    return terminalDetailLocationInfoBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "request_id": requestId,
    "result": result,
  };
}

/// location : {"lat":31.95266,"lng":118.84002}
/// address : "江苏省南京市江宁区上元大街"
/// formatted_addresses : {"recommend":"南京市江宁区人民政府(上元大街北)","rough":"南京市江宁区人民政府(上元大街北)"}
/// address_component : {"nation":"中国","province":"江苏省","city":"南京市","district":"江宁区","street":"上元大街","street_number":"上元大街"}
/// ad_info : {"nation_code":"156","adcode":"320115","city_code":"156320100","name":"中国,江苏省,南京市,江宁区","location":{"lat":31.75,"lng":118.597328},"nation":"中国","province":"江苏省","city":"南京市","district":"江宁区"}
/// address_reference : {"street_number":{"id":"","title":"","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"},"business_area":{"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"},"famous_area":{"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"},"crossroad":{"id":"4008397","title":"上元大街/府前东路(路口)","location":{"lat":31.952761,"lng":118.841652},"_distance":149.1,"_dir_desc":"西"},"town":{"id":"320115001","title":"东山街道","location":{"lat":31.962538,"lng":118.784637},"_distance":0,"_dir_desc":"内"},"street":{"id":"11747961910109150138","title":"上元大街","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"},"landmark_l2":{"id":"15226929800080506919","title":"南京市江宁区人民政府","location":{"lat":31.953566,"lng":118.83976},"_distance":0,"_dir_desc":"内"}}

class ResultBean {
  LocationBean location;
  String address;
  Formatted_addressesBean formattedAddresses;
  Address_componentBean addressComponent;
  Ad_infoBean adInfo;
  Address_referenceBean addressReference;

  static ResultBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ResultBean resultBean = ResultBean();
    resultBean.location = LocationBean.fromMap(map['location']);
    resultBean.address = map['address'];
    resultBean.formattedAddresses = Formatted_addressesBean.fromMap(map['formatted_addresses']);
    resultBean.addressComponent = Address_componentBean.fromMap(map['address_component']);
    resultBean.adInfo = Ad_infoBean.fromMap(map['ad_info']);
    resultBean.addressReference = Address_referenceBean.fromMap(map['address_reference']);
    return resultBean;
  }

  Map toJson() => {
    "location": location,
    "address": address,
    "formatted_addresses": formattedAddresses,
    "address_component": addressComponent,
    "ad_info": adInfo,
    "address_reference": addressReference,
  };
}

/// street_number : {"id":"","title":"","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"}
/// business_area : {"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"}
/// famous_area : {"id":"5495285438494454608","title":"上元大街","location":{"lat":31.9548,"lng":118.842003},"_distance":0,"_dir_desc":"内"}
/// crossroad : {"id":"4008397","title":"上元大街/府前东路(路口)","location":{"lat":31.952761,"lng":118.841652},"_distance":149.1,"_dir_desc":"西"}
/// town : {"id":"320115001","title":"东山街道","location":{"lat":31.962538,"lng":118.784637},"_distance":0,"_dir_desc":"内"}
/// street : {"id":"11747961910109150138","title":"上元大街","location":{"lat":31.954544,"lng":118.848328},"_distance":22.3,"_dir_desc":"北"}
/// landmark_l2 : {"id":"15226929800080506919","title":"南京市江宁区人民政府","location":{"lat":31.953566,"lng":118.83976},"_distance":0,"_dir_desc":"内"}

class Address_referenceBean {
  Street_numberBean streetNumber;
  Business_areaBean businessArea;
  Famous_areaBean famousArea;
  CrossroadBean crossroad;
  TownBean town;
  StreetBean street;
  Landmark_l2Bean landmarkL2;

  static Address_referenceBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Address_referenceBean address_referenceBean = Address_referenceBean();
    address_referenceBean.streetNumber = Street_numberBean.fromMap(map['street_number']);
    address_referenceBean.businessArea = Business_areaBean.fromMap(map['business_area']);
    address_referenceBean.famousArea = Famous_areaBean.fromMap(map['famous_area']);
    address_referenceBean.crossroad = CrossroadBean.fromMap(map['crossroad']);
    address_referenceBean.town = TownBean.fromMap(map['town']);
    address_referenceBean.street = StreetBean.fromMap(map['street']);
    address_referenceBean.landmarkL2 = Landmark_l2Bean.fromMap(map['landmark_l2']);
    return address_referenceBean;
  }

  Map toJson() => {
    "street_number": streetNumber,
    "business_area": businessArea,
    "famous_area": famousArea,
    "crossroad": crossroad,
    "town": town,
    "street": street,
    "landmark_l2": landmarkL2,
  };
}

/// id : "15226929800080506919"
/// title : "南京市江宁区人民政府"
/// location : {"lat":31.953566,"lng":118.83976}
/// _distance : 0
/// _dir_desc : "内"

class Landmark_l2Bean {
  String id;
  String title;
  LocationBean location;
  int Distance;
  String DirDesc;

  static Landmark_l2Bean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Landmark_l2Bean landmark_l2Bean = Landmark_l2Bean();
    landmark_l2Bean.id = map['id'];
    landmark_l2Bean.title = map['title'];
    landmark_l2Bean.location = LocationBean.fromMap(map['location']);
    landmark_l2Bean.Distance = map['_distance'];
    landmark_l2Bean.DirDesc = map['_dir_desc'];
    return landmark_l2Bean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}

/// lat : 31.953566
/// lng : 118.83976

class LocationBean {
  double lat;
  double lng;

  static LocationBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    LocationBean locationBean = LocationBean();
    locationBean.lat = map['lat'];
    locationBean.lng = map['lng'];
    return locationBean;
  }

  Map toJson() => {
    "lat": lat,
    "lng": lng,
  };
}

/// id : "11747961910109150138"
/// title : "上元大街"
/// location : {"lat":31.954544,"lng":118.848328}
/// _distance : 22.3
/// _dir_desc : "北"

class StreetBean {
  String id;
  String title;
  LocationBean location;
  double Distance;
  String DirDesc;

  static StreetBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StreetBean streetBean = StreetBean();
    streetBean.id = map['id'];
    streetBean.title = map['title'];
    streetBean.location = LocationBean.fromMap(map['location']);
    streetBean.Distance = map['_distance'];
    streetBean.DirDesc = map['_dir_desc'];
    return streetBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}



/// id : "320115001"
/// title : "东山街道"
/// location : {"lat":31.962538,"lng":118.784637}
/// _distance : 0
/// _dir_desc : "内"

class TownBean {
  String id;
  String title;
  LocationBean location;
  int Distance;
  String DirDesc;

  static TownBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TownBean townBean = TownBean();
    townBean.id = map['id'];
    townBean.title = map['title'];
    townBean.location = LocationBean.fromMap(map['location']);
    townBean.Distance = map['_distance'];
    townBean.DirDesc = map['_dir_desc'];
    return townBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}


/// id : "4008397"
/// title : "上元大街/府前东路(路口)"
/// location : {"lat":31.952761,"lng":118.841652}
/// _distance : 149.1
/// _dir_desc : "西"

class CrossroadBean {
  String id;
  String title;
  LocationBean location;
  double Distance;
  String DirDesc;

  static CrossroadBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CrossroadBean crossroadBean = CrossroadBean();
    crossroadBean.id = map['id'];
    crossroadBean.title = map['title'];
    crossroadBean.location = LocationBean.fromMap(map['location']);
    crossroadBean.Distance = map['_distance'];
    crossroadBean.DirDesc = map['_dir_desc'];
    return crossroadBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}


/// id : "5495285438494454608"
/// title : "上元大街"
/// location : {"lat":31.9548,"lng":118.842003}
/// _distance : 0
/// _dir_desc : "内"

class Famous_areaBean {
  String id;
  String title;
  LocationBean location;
  int Distance;
  String DirDesc;

  static Famous_areaBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Famous_areaBean famous_areaBean = Famous_areaBean();
    famous_areaBean.id = map['id'];
    famous_areaBean.title = map['title'];
    famous_areaBean.location = LocationBean.fromMap(map['location']);
    famous_areaBean.Distance = map['_distance'];
    famous_areaBean.DirDesc = map['_dir_desc'];
    return famous_areaBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}


/// id : "5495285438494454608"
/// title : "上元大街"
/// location : {"lat":31.9548,"lng":118.842003}
/// _distance : 0
/// _dir_desc : "内"

class Business_areaBean {
  String id;
  String title;
  LocationBean location;
  int Distance;
  String DirDesc;

  static Business_areaBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Business_areaBean business_areaBean = Business_areaBean();
    business_areaBean.id = map['id'];
    business_areaBean.title = map['title'];
    business_areaBean.location = LocationBean.fromMap(map['location']);
    business_areaBean.Distance = map['_distance'];
    business_areaBean.DirDesc = map['_dir_desc'];
    return business_areaBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}


/// id : ""
/// title : ""
/// location : {"lat":31.954544,"lng":118.848328}
/// _distance : 22.3
/// _dir_desc : "北"

class Street_numberBean {
  String id;
  String title;
  LocationBean location;
  double Distance;
  String DirDesc;

  static Street_numberBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Street_numberBean street_numberBean = Street_numberBean();
    street_numberBean.id = map['id'];
    street_numberBean.title = map['title'];
    street_numberBean.location = LocationBean.fromMap(map['location']);
    street_numberBean.Distance = map['_distance'];
    street_numberBean.DirDesc = map['_dir_desc'];
    return street_numberBean;
  }

  Map toJson() => {
    "id": id,
    "title": title,
    "location": location,
    "_distance": Distance,
    "_dir_desc": DirDesc,
  };
}


/// nation_code : "156"
/// adcode : "320115"
/// city_code : "156320100"
/// name : "中国,江苏省,南京市,江宁区"
/// location : {"lat":31.75,"lng":118.597328}
/// nation : "中国"
/// province : "江苏省"
/// city : "南京市"
/// district : "江宁区"

class Ad_infoBean {
  String nationCode;
  String adcode;
  String cityCode;
  String name;
  LocationBean location;
  String nation;
  String province;
  String city;
  String district;

  static Ad_infoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Ad_infoBean ad_infoBean = Ad_infoBean();
    ad_infoBean.nationCode = map['nation_code'];
    ad_infoBean.adcode = map['adcode'];
    ad_infoBean.cityCode = map['city_code'];
    ad_infoBean.name = map['name'];
    ad_infoBean.location = LocationBean.fromMap(map['location']);
    ad_infoBean.nation = map['nation'];
    ad_infoBean.province = map['province'];
    ad_infoBean.city = map['city'];
    ad_infoBean.district = map['district'];
    return ad_infoBean;
  }

  Map toJson() => {
    "nation_code": nationCode,
    "adcode": adcode,
    "city_code": cityCode,
    "name": name,
    "location": location,
    "nation": nation,
    "province": province,
    "city": city,
    "district": district,
  };
}


/// nation : "中国"
/// province : "江苏省"
/// city : "南京市"
/// district : "江宁区"
/// street : "上元大街"
/// street_number : "上元大街"

class Address_componentBean {
  String nation;
  String province;
  String city;
  String district;
  String street;
  String streetNumber;

  static Address_componentBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Address_componentBean address_componentBean = Address_componentBean();
    address_componentBean.nation = map['nation'];
    address_componentBean.province = map['province'];
    address_componentBean.city = map['city'];
    address_componentBean.district = map['district'];
    address_componentBean.street = map['street'];
    address_componentBean.streetNumber = map['street_number'];
    return address_componentBean;
  }

  Map toJson() => {
    "nation": nation,
    "province": province,
    "city": city,
    "district": district,
    "street": street,
    "street_number": streetNumber,
  };
}

/// recommend : "南京市江宁区人民政府(上元大街北)"
/// rough : "南京市江宁区人民政府(上元大街北)"

class Formatted_addressesBean {
  String recommend;
  String rough;

  static Formatted_addressesBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    Formatted_addressesBean formatted_addressesBean = Formatted_addressesBean();
    formatted_addressesBean.recommend = map['recommend'];
    formatted_addressesBean.rough = map['rough'];
    return formatted_addressesBean;
  }

  Map toJson() => {
    "recommend": recommend,
    "rough": rough,
  };
}

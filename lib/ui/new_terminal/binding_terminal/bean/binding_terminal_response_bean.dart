
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/common_terminal_settings_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"adminList":[{"nick":"李岁红","roleid":"99","loginflg":"01","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"李岁红","roleid":"99","loginflg":"00","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"郑哥","roleid":"90","loginflg":"00","loginphone":"15510095356","userid":"020932c9da55460eb694cfe9ce86be06"},{"nick":"郑哥","roleid":"90","loginflg":"00","loginphone":"15510095356","userid":"020932c9da55460eb694cfe9ce86be06"},{"nick":"牛哈哈","loginflg":"00","loginphone":"13470016025","userid":"808b26b631564518b1802a0984745f11"},{"nick":"牛哈哈","loginflg":"00","loginphone":"13470016025","userid":"808b26b631564518b1802a0984745f11"},{"nick":"王凡语","loginflg":"00","loginphone":"18611999116","userid":"bd7ca4fcdfc442049622761636b2f0a4"},{"nick":"王凡语","loginflg":"00","loginphone":"18611999116","userid":"bd7ca4fcdfc442049622761636b2f0a4"}],"terminalInfo":{"autoOnoff":"00","terminalName":"终端名字嘛","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","hsn":"lisuihongtest0113","backup":"这个机器安在搜宝3号楼2909前台哈哈哈哈","screenDirection":"竖屏","address":"北京搜宝","mid":"a48f0893466842c6ab117f36b336d318","closeCamera":" ","autoOnoffTime":" ","screenSpecs":"大屏","rcaid":7}}
/// extra : null

class BindingTerminalResponseBean {
  bool success;
  String code;
  String msg;
  BindingTerminalDataBean data;
  dynamic extra;

  static BindingTerminalResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindingTerminalResponseBean bindingTerminalResponseBeanBean = BindingTerminalResponseBean();
    bindingTerminalResponseBeanBean.success = map['success'];
    bindingTerminalResponseBeanBean.code = map['code'];
    bindingTerminalResponseBeanBean.msg = map['msg'];
    bindingTerminalResponseBeanBean.data = BindingTerminalDataBean.fromMap(map['data']);
    bindingTerminalResponseBeanBean.extra = map['extra'];
    return bindingTerminalResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// adminList : [{"nick":"李岁红","roleid":"99","loginflg":"01","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"李岁红","roleid":"99","loginflg":"00","loginphone":"13226332406","userid":"5f0980b626d446d2badbfb62c6fc473e"},{"nick":"郑哥","roleid":"90","loginflg":"00","loginphone":"15510095356","userid":"020932c9da55460eb694cfe9ce86be06"},{"nick":"郑哥","roleid":"90","loginflg":"00","loginphone":"15510095356","userid":"020932c9da55460eb694cfe9ce86be06"},{"nick":"牛哈哈","loginflg":"00","loginphone":"13470016025","userid":"808b26b631564518b1802a0984745f11"},{"nick":"牛哈哈","loginflg":"00","loginphone":"13470016025","userid":"808b26b631564518b1802a0984745f11"},{"nick":"王凡语","loginflg":"00","loginphone":"18611999116","userid":"bd7ca4fcdfc442049622761636b2f0a4"},{"nick":"王凡语","loginflg":"00","loginphone":"18611999116","userid":"bd7ca4fcdfc442049622761636b2f0a4"}]
/// terminalInfo : {"autoOnoff":"00","terminalName":"终端名字嘛","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","hsn":"lisuihongtest0113","backup":"这个机器安在搜宝3号楼2909前台哈哈哈哈","screenDirection":"竖屏","address":"北京搜宝","mid":"a48f0893466842c6ab117f36b336d318","closeCamera":" ","autoOnoffTime":" ","screenSpecs":"大屏","rcaid":7}

class BindingTerminalDataBean {
  List<AdminListBean> adminList;
  TerminalInfoBean terminalInfo;
  SmartHomeBean smartHome;
  EachFaceCntBean eachFaceCnt;
  String branchname;
  String address;
  String gpsaddress;

  static BindingTerminalDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindingTerminalDataBean dataBean = BindingTerminalDataBean();
    dataBean.adminList = List()..addAll(
        (map['adminList'] as List ?? []).map((o) => AdminListBean.fromMap(o))
    );
    dataBean.terminalInfo = TerminalInfoBean.fromMap(map['terminalInfo']);
    dataBean.smartHome = SmartHomeBean.fromMap(map['smartHome']);
    dataBean.eachFaceCnt = EachFaceCntBean.fromMap(map['eachFaceCnt']);
    dataBean.branchname = map['branchname'];
    dataBean.address = map['address'];
    dataBean.gpsaddress = map['gpsaddress'];
    return dataBean;
  }

  Map toJson() => {
    "adminList": adminList,
    "terminalInfo": terminalInfo,
    "smartHome": smartHome,
    "eachFaceCnt":eachFaceCnt,
    "branchname":branchname,
    "address":address,
    "gpsaddress":gpsaddress
  };
}

/// autoOnoff : "00"
/// terminalName : "终端名字嘛"
/// terminalPicurl : "http://etpic.we17.com/test/20210115134601_3102.jpg"
/// hsn : "lisuihongtest0113"
/// backup : "这个机器安在搜宝3号楼2909前台哈哈哈哈"
/// screenDirection : "竖屏"
/// address : "北京搜宝"
/// mid : "a48f0893466842c6ab117f36b336d318"
/// closeCamera : " "
/// autoOnoffTime : " "
/// screenSpecs : "大屏"
/// rcaid : 7

class TerminalInfoBean {
  static const _START_END_SPLIT = "-";

  static const _TIME_SPLIT = ":";
  String autoOnoff;
  String terminalName;
  String terminalPicurl;
  String hsn;
  String backup;
  String screenDirection;
  String address;
  String gps;
  String mid;
  String closeCamera;
  String autoOnoffTime;
  String autoOnoffWeek;//"1,2,3,4,5"
  String screenSpecs;
  int rcaid;
  int id;
  String position;
  String phone;
  String name;
  String type;
  String versionNo;
  String osversion;
  String enableFaceRecognition;//00开启，01关闭
  String soundonoff;//00开 01关
  int volume;
  int correctCamera;
  double startCameraDistance;
  int frame;
  int timeInterval;
  String cameratype;
  String haveCamera;
  int picCnt;
  int videoCnt;
  String reportHsnLastTime;
  String screenStatus;
  String screenTouch;//00无 01有
  String endday;
  String pushflg;
  String logout;//00正常 01登出
  String licenseflg;
  String sideNumber;
  String cbid;
  String appedition;//00基础版、01海报版、02人脸版、03智慧版
  ComAutoSwitchBean comAutoSwitch;
  String musicflg;//00打开
  int musicNum;

  bool isMusicOpen(){
    return "00" == musicflg;
  }

  String getEnableFaceRecognition(){
    if(StringUtils.isEmpty(enableFaceRecognition)){
      return "01";
    }
    return enableFaceRecognition;
  }

  bool isShowFaceRecognition(){
    if(StringUtils.isEmpty(appedition)){
      return false;
    }
    if(("00" == appedition) || ("01" == appedition)){
      return false;
    }
    return true;
  }

  //基础版 和 人脸版企业 去掉播放管理整个模块
  bool isHidePlayManage(){
    if(StringUtils.isEmpty(appedition)){
      return false;
    }
    if(("00" == appedition) || ("02" == appedition)){
      return true;
    }
    return false;
  }

  //是否展示有效期，这个有效期指海报有效期
  bool isShowEndDay(){
    if(StringUtils.isEmpty(appedition)){
      return false;
    }
    if(("01" == appedition) || ("03" == appedition)){
      return true;
    }
    return false;
  }

  bool getAutoOnOffStatus(){
    if(StringUtils.isNotEmpty(autoOnoffTime)){
      return true;
    }
    return false;
  }

  String getTerminalName(){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏";
  }




  static TerminalInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalInfoBean terminalInfoBean = TerminalInfoBean();
    terminalInfoBean.autoOnoff = map['autoOnoff'];
    terminalInfoBean.terminalName = map['terminalName'];
    terminalInfoBean.terminalPicurl = map['terminalPicurl'];
    terminalInfoBean.hsn = map['hsn'];
    terminalInfoBean.backup = map['backup'];
    terminalInfoBean.screenDirection = map['screenDirection'];
    terminalInfoBean.address = map['address'];
    terminalInfoBean.gps = map['gps'];
    terminalInfoBean.mid = map['mid'];
    terminalInfoBean.closeCamera = map['closeCamera'];
    terminalInfoBean.autoOnoffTime = map['autoOnoffTime'];
    terminalInfoBean.autoOnoffWeek = map['autoOnoffWeek'];
    terminalInfoBean.screenSpecs = map['screenSpecs'];
    terminalInfoBean.rcaid = map['rcaid'];
    terminalInfoBean.id = map['id'];
    //这里是基本信息的摆放位置；
    // if(StringUtils.isNotEmpty(terminalInfoBean.position)){
      terminalInfoBean.position = map['position'];
    // }
    terminalInfoBean.phone = map['phone'];
    terminalInfoBean.name = map['name'];
    terminalInfoBean.type = map['type'];
    terminalInfoBean.versionNo = map['versionNo'];
    terminalInfoBean.osversion = map['osversion'];
    terminalInfoBean.enableFaceRecognition = map['enableFaceRecognition'];
    terminalInfoBean.soundonoff = map['soundonoff'];
    terminalInfoBean.correctCamera = map['correctCamera'];
    terminalInfoBean.startCameraDistance = map['startCameraDistance'];
    terminalInfoBean.frame = map['frame'];
    terminalInfoBean.timeInterval = map['timeInterval'];
    terminalInfoBean.cameratype = map['cameratype'];
    terminalInfoBean.haveCamera = map['haveCamera'];
    terminalInfoBean.picCnt = map['picCnt'];
    terminalInfoBean.videoCnt = map['videoCnt'];
    terminalInfoBean.reportHsnLastTime = map['reportHsnLastTime'];
    terminalInfoBean.screenStatus = map['screenStatus'];
    terminalInfoBean.screenTouch = map['screenTouch'];
    terminalInfoBean.endday = map['endday'];
    terminalInfoBean.volume = map['volume'];
    terminalInfoBean.pushflg = map['pushflg'];
    terminalInfoBean.logout = map['logout'];
    terminalInfoBean.licenseflg = map['licenseflg'];
    terminalInfoBean.sideNumber = map['sideNumber'];
    terminalInfoBean.cbid = map['cbid'];
    terminalInfoBean.appedition = map['appedition'];
    terminalInfoBean.musicNum = map['musicNum'];
    terminalInfoBean.musicflg = map['musicflg'];
    terminalInfoBean.comAutoSwitch = ComAutoSwitchBean.fromMap(map);
    return terminalInfoBean;
  }

  String getEndDayString(){
    if(StringUtils.isEmpty(endday)){
      return "-";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "";
    }
    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      return endday.replaceAll("-", "/") + "到期";
      // if((((endDayDateTime.millisecondsSinceEpoch - currentDay.millisecondsSinceEpoch)~/(1000*3600*24))) >= 100){
      //   return endday.replaceAll("-", "/") + "到期";
      // }
      // return endday.replaceAll("-", "/") + "到期";
    }
  }

  String getNotifyString(){
    if(StringUtils.isEmpty(endday)){
      return "";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "已到期";
    }

    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "已到期" +  (((currentDay.millisecondsSinceEpoch - endDayDateTime.millisecondsSinceEpoch)~/(1000*3600*24))).toString() + "天";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      int interval = (((endDayDateTime.millisecondsSinceEpoch - currentDay.millisecondsSinceEpoch)~/(1000*3600*24)));
      if(interval >= 100){
        return "";
      }
      return "（${interval}天后）";
    }
  }

  String getFaceSizeStr(){
    if(0 == frame){
      return "小";
    }
    if(1 == frame){
      return "中";
    }
    if(2 == frame){
      return "大";
    }
    return "中";
    // if(3 == frame){
    //   return "较大";
    // }
    // if(4 == frame){
    //   return "大";
    // }
    // return "适中";
  }

  double getFaceSizeDouble(){
    if(0 == frame){
      return 0.0;
    }
    if(1 == frame){
      return 1.0;
    }
    if(2 == frame){
      return 2.0;
    }
    // if(3 == frame){
    //   return 3.0;
    // }
    // if(4 == frame){
    //   return 4.0;
    // }
    return 1.0;
  }

  bool isTerminalOff(){
    if(StringUtils.isEmpty(reportHsnLastTime??"") || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    String currentTimeStr = DateUtil.getNowDateStr();
    int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
    int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
    int interval = (currentTime - time)~/(24*60*60*1000);
    if(currentTime - time <= 10*60*1000){
      return false;
    }else{
      return true;
    }
  }

  String getTerminalStatus(){
    if(StringUtils.isEmpty(reportHsnLastTime??"") || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      if(StringUtils.isNotEmpty(reportHsnLastTime??"")){
        String currentTimeStr = DateUtil.getNowDateStr();
        int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
        int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
        int interval = (currentTime - time)~/(24*60*60*1000);
        if(interval > 2){
          return "已关机${interval}天";
        }else{
          return "已关机";
        }
      }
      return "已关机";
    }
    String currentTimeStr = DateUtil.getNowDateStr();
    int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
    int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
    int interval = (currentTime - time)~/(24*60*60*1000);
    if(currentTime - time <= 10*60*1000){
      if("01" == screenStatus){
        return "已关机";
        // return "息屏开机";
      }else{
        return "正常开机";
      }
    }else{
      if(interval > 2){
        return "已关机${interval}天";
      }else{
        return "已关机";
      }
    }
  }

  SetTerminalStateType getTerminalState(){

    if(StringUtils.isEmpty(reportHsnLastTime??"") || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return SetTerminalStateType.off;
    }
    String currentTimeStr = DateUtil.getNowDateStr();
    int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
    int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
    if(currentTime - time <= 10*60*1000){
      if("01" == screenStatus){
        return SetTerminalStateType.screenOff;
      }else{
        return SetTerminalStateType.open;
      }
    }else{
      return SetTerminalStateType.off;
    }
  }


  String getCameraStartDistanceStr(){
    if(1.0 == startCameraDistance){
      return "最近";
    }
    if(1.5 == startCameraDistance){
      return "近";
    }
    if(2.0 == startCameraDistance){
      return "适中";
    }
    if(2.5 == startCameraDistance){
      return "远";
    }
    if(3.0 == startCameraDistance){
      return "最远";
    }
    return "适中";
  }

  double getCameraStartDistanceValue(){
    if(1.0 == startCameraDistance){
      return 0.0;
    }
    if(1.5 == startCameraDistance){
      return 1.0;
    }
    if(2.0 == startCameraDistance){
      return 2.0;
    }
    if(2.5 == startCameraDistance){
      return 3.0;
    }
    if(3.0 == startCameraDistance){
      return 4.0;
    }
    return 2.0;
  }

  String getPunchInIntervalStr(){
    if(timeInterval > 30){
      double interval = timeInterval/60.0;
      return interval.toString().replaceAll(".0", "") + "小时";
    }else{
      return timeInterval.toString() + "分钟";
    }
  }

  String getWeekStr(){
    if(StringUtils.isEmpty(autoOnoffWeek??"")){
      return "";
    }
    List<String> weekList = autoOnoffWeek.split(",");
    String str = "";
    weekList.forEach((element) {
      str += getText(element);
    });
    return str;
  }


  String getText(String index){
    switch (index){
      case "1":
        return "一";
      case "2":
        return "二";
      case "3":
        return "三";
      case "4":
        return "四";
      case "5":
        return "五";
      case "6":
        return "六";
      case "7":
        return "日";
    }
  }

  DateTime startDateTime;
  DateTime endDateTime;

  String getAutoOffTimeStr(){
    if(startDateTime == null || endDateTime == null){
      return null;
    }
    return "${startDateTime.hour}$_TIME_SPLIT${VgToolUtils.twoDigits(startDateTime.minute)}"
        "$_START_END_SPLIT"
        "${endDateTime.hour}$_TIME_SPLIT${VgToolUtils.twoDigits(endDateTime.minute)}";
  }

  DateTime getStartOrEndTime(String autoOffTime,int index){
    if(StringUtils.isEmpty(autoOffTime) || index == null ||index <0 || index > 1){
      return null;
    }
    List<String> timeList = autoOffTime.split("$_START_END_SPLIT");
    if(timeList == null || timeList.length != 2){
      return null;
    }
    String time = timeList.elementAt(index);
    if(StringUtils.isEmpty(time)){
      return null;
    }
    List<String> hourMinList = time.split("$_TIME_SPLIT");
    if(hourMinList == null || hourMinList.length != 2){
      return null;
    }
    final DateTime nowDateTime = DateTime.now();

    return DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,int.tryParse(hourMinList.elementAt(0)),int.tryParse(hourMinList.elementAt(1)));
  }

  Map toJson() => {
    "autoOnoff": autoOnoff,
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "backup": backup,
    "screenDirection": screenDirection,
    "address": address,
    "gps": gps,
    "mid": mid,
    "closeCamera": closeCamera,
    "autoOnoffTime": autoOnoffTime,
    "autoOnoffWeek": autoOnoffWeek,
    "screenSpecs": screenSpecs,
    "rcaid": rcaid,
    "id": id,

    "position": position,
    "phone": phone,
    "name": name,
    "type": type,
    "versionNo": versionNo,
    "osversion": osversion,
    "enableFaceRecognition": enableFaceRecognition,
    "soundonoff": soundonoff,
    "correctCamera": correctCamera,
    "startCameraDistance": startCameraDistance,
    "timeInterval": timeInterval,
    "cameratype": cameratype,
    "haveCamera": haveCamera,
    "picCnt": picCnt,
    "videoCnt": videoCnt,
    "reportHsnLastTime": reportHsnLastTime,
    "screenStatus": screenStatus,
    "screenTouch": screenTouch,
    "endday": endday,
    "volume": volume,
    "pushflg": pushflg,
    "logout": logout,
    "licenseflg": licenseflg,
    "sideNumber": sideNumber,
    "cbid": cbid,
    "appedition": appedition,
    "comAutoSwitch": comAutoSwitch,
    "musicNum": musicNum,
    "musicflg": musicflg,
  };
}

class SmartHomeBean {
  String stopflg;
  String watermark;
  String shid;
  String name;
  String rasid;
  String type;
  String brand;

  String getTitle(){
    return "${brand??""}（${name??""}）";
  }

  static SmartHomeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeBean smartHomeBean = SmartHomeBean();
    smartHomeBean.stopflg = map['stopflg'];
    smartHomeBean.watermark = map['watermark'];
    smartHomeBean.shid = map['shid'];
    smartHomeBean.name = map['name'];
    smartHomeBean.rasid = map['rasid'];
    smartHomeBean.type = map['type'];
    smartHomeBean.brand = map['brand'];
    return smartHomeBean;
  }

  Map toJson() => {
    "stopflg": stopflg,
    "watermark": watermark,
    "shid": shid,
    "name": name,
    "rasid": rasid,
    "type": type,
    "brand": brand,
  };
}

/// facecnt : 6
/// unfacecnt : 0

class EachFaceCntBean {
  int facecnt;
  int unfacecnt;

  static EachFaceCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EachFaceCntBean eachFaceCntBean = EachFaceCntBean();
    eachFaceCntBean.facecnt = map['facecnt'];
    eachFaceCntBean.unfacecnt = map['unfacecnt'];
    return eachFaceCntBean;
  }

  Map toJson() => {
    "facecnt": facecnt,
    "unfacecnt": unfacecnt,
  };
}

/// nick : "李岁红"
/// roleid : "99"
/// loginflg : "01"
/// loginphone : "13226332406"
/// userid : "5f0980b626d446d2badbfb62c6fc473e"

class AdminListBean {
  String nick;
  String roleid;
  String loginflg;
  String loginphone;
  String userid;
  String napicurl;

  static AdminListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AdminListBean adminListBean = AdminListBean();
    adminListBean.nick = map['nick'];
    adminListBean.roleid = map['roleid'];
    adminListBean.loginflg = map['loginflg'];
    adminListBean.loginphone = map['loginphone'];
    adminListBean.userid = map['userid'];
    adminListBean.napicurl = map['napicurl'];
    return adminListBean;
  }

  Map toJson() => {
    "nick": nick,
    "roleid": roleid,
    "loginflg": loginflg,
    "loginphone": loginphone,
    "userid": userid,
    "napicurl":napicurl,
  };

  ///当前登录
  ///00-不是
  ///01-当前登录
  bool isLocalLogin(){
    return loginflg == "01";
  }
}
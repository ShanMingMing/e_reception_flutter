import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_type.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class BindingTermialEditUploadBean{

  static const _START_END_SPLIT = "-";

  static const _TIME_SPLIT = ":";
  
  ///地址信息
  String address;

  // ///自动关机 00未设置 01已设置
  // String auto_onoff;
  //
  // ///backup
  // String auto_onoff_time;

  ///备注（没有传空）
  String backup;

  // ///00正常 01同步关闭摄像头
  // String close_camera;

  ///gps(31.930648,118.830942)
  String gps;

  ///终端编码
  String hsn;

  ///00保存 01绑定
  String is_bind;

  ///终端机id
  String mid;

  ///企业终端关系id（没有传0）
  int rcaid;

  ///终端名称
  String terminal_name;

  ///terminal_picurl
  String terminal_picurl;

  BindingTerminalCameraStatusType cameraStatusType;

  BindingTerminalAutoOffStatusType autoOffStatusType;

  DateTime startDateTime;
  DateTime endDateTime;

  ///屏幕规格
  BindingTerminalScreenSizeType screenSizeType;
  ///屏幕方向
  BindingTerminalScreenOrientationType screenOrientationType;
  ///设备类型
  BindingTerminalDeviceType deviceType;
  ///启用人脸识别
  FaceRecognitionType faceRecognitionType;

  ///人脸框识别比例
  int faceSize = 2;
  ///摄像头启动距离
  double cameraStartDistance = 2.0;
  ///刷脸记录间隔
  int punchInInterval = 5;

  ///00绑定 01编辑
  String flg;

  ///摆放位置
  String position;

  ///相机镜像 00无 01 02
  String cameratype;

  ///是否触屏 00无 01有
  String screenTouch = "00";

  ///矫正摄像头
  int correctCamera;

  String caid;

  String cbid;

  double getCameraStartDistanceValue(){
    if(1.0 == cameraStartDistance){
      return 0.0;
    }
    if(1.5 == cameraStartDistance){
      return 1.0;
    }
    if(2.0 == cameraStartDistance){
      return 2.0;
    }
    if(2.5 == cameraStartDistance){
      return 3.0;
    }
    if(3.0 == cameraStartDistance){
      return 4.0;
    }
    return 2.0;
  }

  String getCameraStartDistanceStr(){
    if(1.0 == cameraStartDistance){
      return "最近";
    }
    if(1.5 == cameraStartDistance){
      return "近";
    }
    if(2.0 == cameraStartDistance){
      return "适中";
    }
    if(2.5 == cameraStartDistance){
      return "远";
    }
    if(3.0 == cameraStartDistance){
      return "最远";
    }
    return "适中";
  }


  double getFaceSizeDistanceValue(){
    if(0 == faceSize){
      return 0.0;
    }
    if(1 == faceSize){
      return 1.0;
    }
    if(2 == faceSize){
      return 2.0;
    }
    if(3 == faceSize){
      return 3.0;
    }
    if(4 == faceSize){
      return 4.0;
    }
    return 2.0;
  }

  String getFaceSizeStr(){
    if(0 == faceSize){
      return "小";
    }
    if(1 == faceSize){
      return "较小";
    }
    if(2 == faceSize){
      return "适中";
    }
    if(3 == faceSize){
      return "较大";
    }
    if(4 == faceSize){
      return "大";
    }
    return "适中";
  }

  String getPunchInIntervalStr(){
    return punchInInterval.toString() + "分钟";
  }


  @override
  bool operator ==(Object other) => false;

  @override
  int get hashCode => super.hashCode;


  ///是否检查通过
  bool isAlive() {
    // return true;
    //demand  全称非必填
    // if (StringUtils.isEmpty(terminal_name) || StringUtils.isEmpty(position)) {
    if (StringUtils.isEmpty(terminal_name)) {
      return false;
    }
    return true;
  }

  ///检查校验
  //demand  全称非必填
  String checkVerify(){
    if(StringUtils.isEmpty(terminal_name)){
      return "设备名称不能为空";
    }
    // if(StringUtils.isEmpty(position)){
    //   return "摆放位置不能为空";
    // }
    return null;
  }

  @override
  String toString() {
    return 'BindingTermialEditUploadBean{adress: $address, backup: $backup, gps: $gps, hsn: $hsn, is_bind: $is_bind, mid: $mid, rcaid: $rcaid, terminal_name: $terminal_name, terminal_picurl: $terminal_picurl, caid: $caid}';
  }

  String getAutoOffTimeStr(){
    if(startDateTime == null || endDateTime == null){
      return null;
    }
    return "${startDateTime.hour}$_TIME_SPLIT${VgToolUtils.twoDigits(startDateTime.minute)}"
        "$_START_END_SPLIT"
        "${endDateTime.hour}$_TIME_SPLIT${VgToolUtils.twoDigits(endDateTime.minute)}";
  }
  
  DateTime getStartOrEndTime(String autoOffTime,int index){
    if(StringUtils.isEmpty(autoOffTime) || index == null ||index <0 || index > 1){
      return null;
    }
    List<String> timeList = autoOffTime.split("$_START_END_SPLIT");
    if(timeList == null || timeList.length != 2){
      return null;
    }
    String time = timeList.elementAt(index);
    if(StringUtils.isEmpty(time)){
      return null;
    }
    List<String> hourMinList = time.split("$_TIME_SPLIT");
    if(hourMinList == null || hourMinList.length != 2){
      return null;
    }
    final DateTime nowDateTime = DateTime.now();

    return DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,int.tryParse(hourMinList.elementAt(0)),int.tryParse(hourMinList.elementAt(1)));
  }

}
enum BindingTerminalCameraStatusType {
  open,
  close,
}

extension BindingTerminalCameraStatusTypeExtension
    on BindingTerminalCameraStatusType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalCameraStatusType.open:
        return "00";
      case BindingTerminalCameraStatusType.close:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static BindingTerminalCameraStatusType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return BindingTerminalCameraStatusType.open;
      case "01" :return BindingTerminalCameraStatusType.close;
    }
    return null;
  }

  ///字符转类型
  static String getTypeToStr(BindingTerminalCameraStatusType type){
    if(type == null){
      return null;
    }
    switch(type){
      case BindingTerminalCameraStatusType.open:return"00";
      case BindingTerminalCameraStatusType.close:return"01";
    }
    return null;
  }
}

enum BindingTerminalAutoOffStatusType{
  unSetting,
  setting,
}

extension BindingTerminalAutoOffStatusTypeExtension on BindingTerminalAutoOffStatusType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalAutoOffStatusType.unSetting:
        return "00";
      case BindingTerminalAutoOffStatusType.setting:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static BindingTerminalAutoOffStatusType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return BindingTerminalAutoOffStatusType.unSetting;
      case "01" :return BindingTerminalAutoOffStatusType.setting;
    }
    return null;
  }
}

/// 屏幕规格
enum BindingTerminalScreenSizeType{
  big,
  small,
}

extension BindingTerminalScreenSizeTypeExtension on BindingTerminalScreenSizeType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalScreenSizeType.big:
        return "00";
      case BindingTerminalScreenSizeType.small:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static BindingTerminalScreenSizeType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return BindingTerminalScreenSizeType.big;
      case "01" :return BindingTerminalScreenSizeType.small;
    }
    return null;
  }

  static String getStrByType(String type){
    if(type == null || type == ""){
      return "-";
    }
    switch(type){
      case "00" :return "大屏";
      case "01" :return "小屏";
    }
    return "-";
  }
}

/// 屏幕方向
enum BindingTerminalScreenOrientationType{
  vertical,
  horizontal,
}

extension BindingTerminalScreenOrientationTypeExtension on BindingTerminalScreenOrientationType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalScreenOrientationType.horizontal:
        return "00";
      case BindingTerminalScreenOrientationType.vertical:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static BindingTerminalScreenOrientationType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return BindingTerminalScreenOrientationType.horizontal;
      case "01" :return BindingTerminalScreenOrientationType.vertical;
    }
    return null;
  }

  static String getStrByType(String type){
    if(type == null || type == ""){
      return "-";
    }
    switch(type){
      case "00" :return "横屏";
      case "01" :return "竖屏";
    }
    return "-";
  }
}

/// 设备类型
enum BindingTerminalDeviceType{
  vertical,//直立式
  desktop,//台式
  suspension,//悬挂
  incline,//斜立式
}

extension BindingTerminalDeviceTypeExtension on BindingTerminalDeviceType {

  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalDeviceType.vertical:
        return "00";
      case BindingTerminalDeviceType.desktop:
        return "01";
      case BindingTerminalDeviceType.suspension:
        return "02";
      case BindingTerminalDeviceType.incline:
        return "03";
    }
    return null;
  }

  ///获取默认类型
  static BindingTerminalDeviceType getDefaultType(){
    return BindingTerminalDeviceType.vertical;
  }

  ///或者选择项
  static List<String> getListStr() {
    List<String> list = List();
    for(BindingTerminalDeviceType item in BindingTerminalDeviceType.values){
      list?.add(item?.getTypeToStr());
    }
    return list;
  }

  ///获取类型对应字符串
  String getDefaultTypeStr() {
        return "直立式";
  }

  ///获取类型对应字符串
  String getTypeToStr() {
    if (this == null) {
      return getDefaultTypeStr();
    }
    switch (this) {
      case BindingTerminalDeviceType.vertical:
        return "直立式";
      case BindingTerminalDeviceType.desktop:
        return "台式";
      case BindingTerminalDeviceType.suspension:
        return "悬挂式";
      case BindingTerminalDeviceType.incline:
        return "斜立式";
    }
    return getDefaultTypeStr();
  }

  ///字符串转类型
  static BindingTerminalDeviceType getStrToType(String str) {
    if (str == null || str.isEmpty) {
      return BindingTerminalDeviceType.vertical;
    }
    switch (str) {
      case "直立式":
        return BindingTerminalDeviceType.vertical;
      case "台式":
        return BindingTerminalDeviceType.desktop;
      case "悬挂式":
        return BindingTerminalDeviceType.suspension;
      case "斜立式":
        return BindingTerminalDeviceType.incline;
    }
    return null;
  }

  static String getShowStrByType(String type){
    if(type == null || type == ""){
      return "直立式";
    }
    switch(type){
      case "00" : return "直立式";
      case "01" : return "台式";
      case "02" : return "悬挂式";
      case "03" : return "斜立式";
    }
    return "直立式";
  }

  static BindingTerminalDeviceType getStrByType(String type){
    if(type == null || type == ""){
      return BindingTerminalDeviceType.vertical;
    }
    switch(type){
      case "00" : return BindingTerminalDeviceType.vertical;
      case "01" : return BindingTerminalDeviceType.desktop;
      case "02" : return BindingTerminalDeviceType.suspension;
      case "03" : return BindingTerminalDeviceType.incline;
    }
    return BindingTerminalDeviceType.vertical;
  }

  static String getTypeByStr(String str){
    if(str == null || str == ""){
      return "-";
    }
    switch(str){
      case "00" :return "直立式";
      case "01" :return "台式";
      case "02" :return "悬挂式";
      case "03" :return "斜立式";
    }
    return "-";
  }

  ///获取类型对应Id字符串
  String getTypeToIdStr() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case BindingTerminalDeviceType.vertical:
        return "00";
      case BindingTerminalDeviceType.desktop:
        return "01";
      case BindingTerminalDeviceType.suspension:
        return "02";
      case BindingTerminalDeviceType.incline:
        return "03";
    }
    return null;
  }

  ///字符转类型
  static BindingTerminalDeviceType getIdToTypeStr(String id){
    if(id == null || id == ""){
      return null;
    }
    switch(id){
      case "00" :return BindingTerminalDeviceType.vertical;
      case "01" :return BindingTerminalDeviceType.desktop;
      case "02" :return BindingTerminalDeviceType.suspension;
      case "03" :return BindingTerminalDeviceType.incline;
    }
    return null;
  }
}

///有无摄像头
enum HaveCameraType{
  exist,
  unExist,
}

extension HaveCameraTypeExtension on HaveCameraType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case HaveCameraType.exist:
        return "00";
      case HaveCameraType.unExist:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static HaveCameraType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return HaveCameraType.exist;
      case "01" :return HaveCameraType.unExist;
    }
    return null;
  }
}

///启用人脸识别
enum FaceRecognitionType{
  open,
  close,
}

extension FaceRecognitionTypeExtension on FaceRecognitionType {
  ///获取参数值
  String getParamsValue() {
    if (this == null) {
      return null;
    }
    switch (this) {
      case FaceRecognitionType.open:
        return "00";
      case FaceRecognitionType.close:
        return "01";
    }
    return null;
  }

  ///字符转类型
  static FaceRecognitionType getStrToType(String str){
    if(str == null || str == ""){
      return null;
    }
    switch(str){
      case "00" :return FaceRecognitionType.open;
      case "01" :return FaceRecognitionType.close;
    }
    return null;
  }
}


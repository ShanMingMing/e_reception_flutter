/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"terminalName":"小米pad0315","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(搜宝商务中心支行)","phone":"18611999119","name":"超管119","position":"壮壮座位","type":"立柜式","companynick":"湛腾科技02"}
/// extra : null

class BindTerminalInfoConfirmResponseBean {
  bool success;
  String code;
  String msg;
  BindTerminalInfoConfirmBean data;
  dynamic extra;

  static BindTerminalInfoConfirmResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindTerminalInfoConfirmResponseBean bindTerminalInfoConfirmResponseBeanBean = BindTerminalInfoConfirmResponseBean();
    bindTerminalInfoConfirmResponseBeanBean.success = map['success'];
    bindTerminalInfoConfirmResponseBeanBean.code = map['code'];
    bindTerminalInfoConfirmResponseBeanBean.msg = map['msg'];
    bindTerminalInfoConfirmResponseBeanBean.data = BindTerminalInfoConfirmBean.fromMap(map['data']);
    bindTerminalInfoConfirmResponseBeanBean.extra = map['extra'];
    return bindTerminalInfoConfirmResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// terminalName : "小米pad0315"
/// terminalPicurl : ""
/// hsn : "11140C1F12FEEB2C52DFBE67ED3E634AA0"
/// address : "北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(搜宝商务中心支行)"
/// phone : "18611999119"
/// name : "超管119"
/// position : "壮壮座位"
/// type : "立柜式"
/// companynick : "湛腾科技02"

class BindTerminalInfoConfirmBean {
  String terminalName;
  String terminalPicurl;
  String hsn;
  String address;
  String phone;
  String name;
  String mid;
  String position;
  String type;
  String companynick;
  int rcaid;
  String screenSpecs;//屏幕规格 00大屏 01小屏
  String screenDirection;//屏幕方向 00横屏 01竖屏
  String enableFaceRecognition;//启用人脸识别 00启用 01关闭
  String haveCamera;//有无摄像头 00有 01无
  String flg;//00绑定 01编辑
  String gps;

  static BindTerminalInfoConfirmBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindTerminalInfoConfirmBean dataBean = BindTerminalInfoConfirmBean();
    dataBean.terminalName = map['terminalName'];
    dataBean.terminalPicurl = map['terminalPicurl'];
    dataBean.hsn = map['hsn'];
    dataBean.address = map['address'];
    dataBean.phone = map['phone'];
    dataBean.name = map['name'];
    dataBean.mid = map['mid'];
    dataBean.position = map['position'];
    dataBean.type = map['type'];
    dataBean.companynick = map['companynick'];
    dataBean.rcaid = map['rcaid'];
    dataBean.screenSpecs = map['screenSpecs'];
    dataBean.screenDirection = map['screenDirection'];
    dataBean.enableFaceRecognition = map['enableFaceRecognition'];
    dataBean.haveCamera = map['haveCamera'];
    dataBean.flg = map['flg'];
    dataBean.gps = map['gps'];
    return dataBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "address": address,
    "phone": phone,
    "name": name,
    "mid": mid,
    "position": position,
    "type": type,
    "companynick": companynick,
    "rcaid": rcaid,
    "screenSpecs": screenSpecs,
    "screenDirection": screenDirection,
    "enableFaceRecognition": enableFaceRecognition,
    "haveCamera": haveCamera,
    "flg": flg,
    "gps": gps,
  };

  ///获取编辑保存时的类型
  ///If(isBind) return "00";else return "01";
  ///此时都是未绑定
  String getEditsBindType(){
      return "01";
  }
}
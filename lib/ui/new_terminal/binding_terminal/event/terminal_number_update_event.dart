class TerminalNumberUpdateEvent {
  String message;
  String hsn;

  TerminalNumberUpdateEvent(this.message, {this.hsn}){
    print('发送${message??'TerminalNumberUpdateEvent'}');
  }

  @override
  String toString() {
    return "收到${message??'TerminalNumberUpdateEvent'}事件";
  }
}
class TerminalInfoUpdateEvent {
  String message;

  TerminalInfoUpdateEvent(this.message){
    print('发送${message??'TerminalInfoUpdateEvent'}');
  }

  @override
  String toString() {
    return "收到${message??'TerminalInfoUpdateEvent'}事件";
  }
}
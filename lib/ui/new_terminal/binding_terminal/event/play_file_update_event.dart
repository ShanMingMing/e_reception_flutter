class PlayFileUpdateEvent {
  String message;

  PlayFileUpdateEvent(this.message){
    print('发送${message??'PlayFileUpdateEvent'}');
  }

  @override
  String toString() {
    return "收到${message??'PlayFileUpdateEvent'}事件";
  }
}
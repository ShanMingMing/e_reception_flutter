import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_bubble_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_detail_update.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/choose_city/bean/choose_city_list_item_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/bean/bg_terminal_music_list_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/bg_terminal_music_list_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/music_index_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/bg_music/refresh_terminal_bg_music_list_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/close_ai_poster_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_add_admin_list/binding_add_admin_list_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_edit_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/bean/binding_terminal_type.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/binding_terminal_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/edit_basic_info_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/edit_fix_camera_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/edit_more_settings_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/terminal_number_update_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/fix_camera_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/set_terminal_backup_dailog.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/show_terminal_address_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/terminal_pic_detail_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/add_device_administrator_widget.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/binding_terminal_auto_switch_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/bubble_widget.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/camera_distance_widget.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/widgets/terminal_face_brushing_record_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/channel_org_list_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_auto_on_off_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_list_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_volume_refresh_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/widgets/terminal_state_menu_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/set_terminal_volume_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/single_terminal_manage_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/terminal_detail_page.dart';
import 'package:e_reception_flutter/ui/smart_home/event/refresh_smart_home_index_event.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_simple_store_list_page.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';
import '../set_terminal_status_menu_dialog.dart';
import 'bean/binding_terminal_response_bean.dart';
import 'event/play_file_update_event.dart';
import 'event/terminal_info_update_event.dart';
import 'package:vg_base/vg_permission_lib.dart';

///设备详情页面
class TerminalDetailInfoPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "TerminalDetailInfoPage";

  final String hsn;
  final String router;
  final bool popflg;
  final int terminalCount;
  const TerminalDetailInfoPage({Key key, this.hsn, this.popflg, this.terminalCount, this.router})
      : super(key: key);


  @override
  _TerminalDetailInfoPageState createState() => _TerminalDetailInfoPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context, String hsn, {bool popflg, int terminalCount, String router}) {
    return RouterUtils.routeForFutureResult(
      context,
      TerminalDetailInfoPage(
        hsn: hsn,
        popflg: popflg,
        terminalCount: terminalCount,
        router: router,
      ),
      routeName: TerminalDetailInfoPage.ROUTER,
    );
  }
}

class _TerminalDetailInfoPageState extends BaseState<TerminalDetailInfoPage> with SingleTickerProviderStateMixin{
  BindingTerminalViewModel _viewModel;
  NewTerminalListViewModel _terminalListViewModel;
  ValueNotifier<BindingTermialEditUploadBean> _uploadValueNotifier;
  BgTerminalMusicListViewModel _bgMusicViewModel;

  StreamSubscription _terminalBgMusicUpdateStreamSubscription;
  AnimationController _controller;

  bool showMore = false;
  bool showBubble = false;
  bool _openPoster = true;
  TerminalInfoBean _terminalInfoBean;

  VgLocation _locationPlugin;
  Map<String, ChooseCityListItemBean> _resultLocationMap;
  String _gps;
  String _address;

  String _hsns;

  @override
  void initState() {
    super.initState();
    _initLocation();
    _viewModel = BindingTerminalViewModel(this);
    _terminalListViewModel = NewTerminalListViewModel(this);
    _bgMusicViewModel = BgTerminalMusicListViewModel(this);
    _viewModel.getTerminalDetailInfoWithCache(widget?.hsn);
    String cacheKey = NetApi.GET_BG_MUSIC_LIST + (UserRepository.getInstance().getCacheKeySuffix()?? "")
        + widget?.hsn;
    _bgMusicViewModel.getBgMusicListOnLine(widget?.hsn, cacheKey, null);
    _terminalBgMusicUpdateStreamSubscription =
        VgEventBus.global.on<RefreshTerminalBgMusicListEvent>()?.listen((event) {
          _bgMusicViewModel.getBgMusicListOnLine(widget?.hsn, cacheKey, null);
        });

    _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        //动画从 controller.forward() 正向执行 结束时会回调此方法
        //重置起点
        _controller.reset();
        //开启
        _controller.forward();
      } else if (status == AnimationStatus.dismissed) {
        //动画从 controller.reverse() 反向执行 结束时会回调此方法
      } else if (status == AnimationStatus.forward) {
        //执行 controller.forward() 会回调此状态
      } else if (status == AnimationStatus.reverse) {
        //执行 controller.reverse() 会回调此状态
      }
    });

    _uploadValueNotifier = ValueNotifier(null);
    VgEventBus.global.streamController.stream.listen((event) {
      if (event is TerminalListRefreshEvent
          || event is MediaDetailUpdateEvent || event is MediaLibraryRefreshEven
          || event is TerminalVolumeRefreshEvent || event is TerminalAutoOnOffRefreshEvent
          || event is TerminalInfoUpdateEvent || event is RefreshTerminalBgMusicListEvent
          || event is RefreshSmartHomeIndexEvent) {
        print('_TerminalDetailInfoPageState' + event.toString());
        _viewModel.getTerminalDetailInfo(widget?.hsn);
      }
      if(event is PlayFileUpdateEvent && "终端照片更新" != event.message){
        _viewModel.getTerminalDetailInfo(widget?.hsn);
      }
    });
    Future(() {
      VgToolUtils.removeAllFocus(context);
    });

    SharePreferenceUtil.getString(SP_ALL_TERMINAL_HSNS).then((value) {
      _hsns = value??"";
    });
  }

  _initLocation(){
    _locationPlugin = VgLocation();
    _locationPlugin.requestSingleLocation((locationMap) {
      Future<String> latlng = VgLocationUtils.getCacheLatLngString();
      latlng.then((value){
        print("获取到了最新gps:" + value);
        _gps = value;
      });
      print("能不能收到");
      var transResult = VgLocationUtils.transLocationToMap(locationMap);
      if (transResult == null) {
        return;
      }
      _resultLocationMap = transResult;
      // _setLocationEditingValue();
      // _setAddressGps();
      // _notifyChange();
    });
  }

  @override
  void dispose() {
    _uploadValueNotifier?.dispose();
    _terminalBgMusicUpdateStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: CommonPackUpKeyboardWidget(
          isEnableVerical: true,
          child: Stack(
            children: [
              GestureDetector(

                child: Column(children: [
                  _toTopBarWidget(),
                  if(UserRepository.getInstance().userData.companyInfo.isShowMusicLayout())_toMusicPlayWidget(),
                  Expanded(
                      child: GestureDetector(
                          onTapDown: (details){
                            setState(() {
                              showBubble = false;
                            });
                          },
                          onTapUp: (details){
                            setState(() {
                              showBubble = false;
                            });
                          },
                          onTapCancel: (){
                            setState(() {
                              showBubble = false;
                            });
                          },
                          onPanDown: (details){
                            setState(() {
                              showBubble = false;
                            });
                          },
                          child: _toPlaceHolderWidget()
                      )
                  )
                ]),
              ),
              Visibility(
                visible: showBubble,
                child: Positioned(
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: (){
                      setState(() {
                        showBubble = !showBubble;
                      });
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: Stack(
                        children: [
                          Positioned(
                              top: ScreenUtils.getStatusBarH(context) + 44 - 6,
                              right: 15,
                              child: GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: ()async{
                                  setState(() {
                                    showBubble = !showBubble;
                                  });
                                  if(_terminalInfoBean == null){
                                    return;
                                  }
                                  // if(_terminalInfoBean.isTerminalOff()){
                                  //   VgToastUtils.toast(context, "请检测对应终端是否在线");
                                  //   return;
                                  // }
                                  bool result =
                                  await CommonConfirmCancelDialog.navigatorPushDialog(context,
                                      title: "提示",
                                      content: "终端显示屏将退出登录状态，可再次扫描二维码登录显示屏，确认继续？",
                                      cancelText: "取消",
                                      confirmText: "确定",
                                      confirmBgColor: ThemeRepository.getInstance()
                                          .getPrimaryColor_1890FF());
                                  if (result ?? false) {
                                    _viewModel?.terminalLoginOut(context, widget?.hsn, (_terminalInfoBean?.isTerminalOff()??false)?"01":"00");
                                  }
                                },
                                child: BubbleWidget(
                                    120.0,
                                    65.0,
                                    ThemeRepository.getInstance().getLineColor_3A3F50(),
                                    BubbleArrowDirection.top,
                                    arrAngle: 85.0,
                                    length: 84.0,
                                    radius: 12.0,
                                    arrHeight: 6.0,
                                    strokeWidth: 0.0,
                                    child: Text(
                                        '退出登录',
                                        style: TextStyle(
                                            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(), fontSize: 14.0
                                        )
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }

  Widget _toPlaceHolderWidget() {
    return ValueListenableBuilder(
        valueListenable: _viewModel?.statusTypeValueNotifier,
        builder:
            (BuildContext context, PlaceHolderStatusType value, Widget child) {
          return VgPlaceHolderStatusWidget(
            loadingStatus: value == PlaceHolderStatusType.loading,
            errorStatus: value == PlaceHolderStatusType.error,
            errorOnClick: () => _viewModel
                .getTerminalDetailInfo(widget?.hsn),
            loadingOnClick: () => _viewModel
                .getTerminalDetailInfo(widget?.hsn),
            child: child,
          );
        },
        child: _toMainColumnWidget());
  }

  Widget _toMainColumnWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.infoValueNotifier,
      builder: (BuildContext context, BindingTerminalDataBean infoBean,
          Widget child) {
        _terminalInfoBean = infoBean?.terminalInfo;
        _address = infoBean?.address;
        SetTerminalStateType currentType = infoBean?.terminalInfo?.getTerminalState();
        return ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            _toTerminalCompanyInfoWidget(infoBean?.branchname, infoBean?.smartHome),
            SizedBox(
              height: 10,
            ),
            Container(
              decoration: BoxDecoration(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _toDeviceInfoWidget(infoBean.terminalInfo),
                  _toDevicePhotoWidget(infoBean.terminalInfo),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              child: Column(
                children: [
                  Visibility(
                      visible: !showMore,
                      child: SizedBox(height: 9,)),
                  Visibility(
                    visible: !showMore,
                    child: Container(
                      child: GestureDetector(
                        onTap: (){
                          showMore = !showMore;
                          setState(() {});
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Spacer(),
                            Text(
                              "展开更多",
                              style: TextStyle(
                                  color: VgColors.INPUT_BG_COLOR,
                                  fontSize: 14
                              ),
                            ),
                            SizedBox(width: 3,),
                            Image.asset(
                              "images/icon_show_more.png",
                              width: 9,
                              height: 20,
                            ),
                            Spacer(),
                          ],
                        ),
                      ),
                    ),

                  ),
                  Visibility(
                    visible: showMore,
                    child: Column(
                      children: <Widget>[
                        _TitleAndContentWidget(
                          title: "屏幕规格",
                          content: BindingTerminalScreenSizeTypeExtension.getStrByType(_terminalInfoBean?.screenSpecs),
                        ),
                        _TitleAndContentWidget(
                          title: "屏幕方向",
                          content: BindingTerminalScreenOrientationTypeExtension.getStrByType(_terminalInfoBean?.screenDirection),
                        ),
                        _TitleAndContentWidget(
                          title: "是否触屏",
                          content: ("01" == (_terminalInfoBean?.screenTouch??"00"))?"是":"否",
                        ),
                        _TitleAndContentWidget(
                          title: "设备类型",
                          content: BindingTerminalDeviceTypeExtension.getShowStrByType(_terminalInfoBean?.type),
                        ),
                        _TitleAndContentWidget(
                            title: "软件版本",
                            content: (_terminalInfoBean?.osversion??"")
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width: 71,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color:
                                    ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                                    fontSize: 14,
                                    height: 1.22
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top: 1),
                                child: Text(
                                  "APP " + (_terminalInfoBean?.versionNo??""),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                                      fontSize: 14,
                                      height: 1.2
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),


                        // SizedBox(
                        //   height: 12,
                        // ),
                        // Row(
                        //   children: <Widget>[
                        //     Container(
                        //       width: 64,
                        //       alignment: Alignment.centerLeft,
                        //       child: Text(
                        //         "人脸识别",
                        //         maxLines: 1,
                        //         overflow: TextOverflow.ellipsis,
                        //         style: TextStyle(
                        //           color:
                        //           ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        //           fontSize: 14,
                        //         ),
                        //       ),
                        //     ),
                        //     SizedBox(width: 15,),
                        //     Text(
                        //       FaceRecognitionTypeExtension.getStrToType(terminalInfoBean?.enableFaceRecognition) == FaceRecognitionType.open
                        //           ?"已启用"
                        //           :"未启用",
                        //       maxLines: 1,
                        //       overflow: TextOverflow.ellipsis,
                        //       style: TextStyle(
                        //         color:
                        //         FaceRecognitionTypeExtension.getStrToType(terminalInfoBean?.enableFaceRecognition) == FaceRecognitionType.open
                        //             ?ThemeRepository.getInstance().getHintGreenColor_00C6C4()
                        //             :ThemeRepository.getInstance().getMinorRedColor_F95355(),
                        //         fontSize: 14,
                        //       ),
                        //     ),
                        //
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            // _toDeviceStatusWidget(infoBean.terminalInfo),
            // _toEnableFaceRecognizeAndVolumeWidget(infoBean.terminalInfo),
            SizedBox(
              height: 10,
            ),
            // _toBasicInfoWidget(infoBean.terminalInfo),
            // SizedBox(
            //   height: 10,
            // ),
            Visibility(
                visible: !(infoBean?.terminalInfo?.isHidePlayManage()??false),
                child: _toManagePlayListWidget(infoBean.terminalInfo)
            ),
            Visibility(
              visible: !(infoBean?.terminalInfo?.isHidePlayManage()??false),
              child: SizedBox(
                height: 10,
              ),
            ),
            _toFaceBrushingRecordWidget(infoBean.terminalInfo,infoBean?.eachFaceCnt),
            Visibility(
              visible: StringUtils.isNotEmpty(infoBean?.terminalInfo?.appedition)?infoBean?.terminalInfo?.isShowFaceRecognition():
              UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition(),
              child: SizedBox(
                height: 10,
              ),
            ),
            _toAdminListWidget(infoBean.adminList,infoBean.terminalInfo),
            SizedBox(
              height: 10,
            ),
            _moreSettingsIndexWidget(infoBean.terminalInfo),
            SizedBox(height: 54 + NAV_HEIGHT,),
            // _moreSettingsWidget(infoBean.terminalInfo),
          ],
        );
      },
    );
  }

  ///topbar
  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "设备详情", isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      rightWidget: Visibility(
        visible: VgRoleUtils.isSuperAdmin(UserRepository.getInstance().getCurrentRoleId())
            || VgRoleUtils.isCommonAdmin(UserRepository.getInstance().getCurrentRoleId()),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          child: Container(
            width: 120,
            height: 44,
            alignment: Alignment.centerRight,
            child: Image.asset(
              "images/icon_more_operations.png",
              width: 20,
            ),
          ),
          onTap: (){
            setState(() {
              showBubble = !showBubble;
            });
          },
        ),
      ),
    );
  }

  ///音乐
  Widget _toMusicPlayWidget(){
    return ValueListenableBuilder(
        valueListenable: _bgMusicViewModel.musicListAndFlagValueNotifier,
        builder:
            (BuildContext context, MusicListDataBean listDataBean, Widget child) {
          return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                MusicIndexPage.navigatorPush(context, _terminalInfoBean?.terminalName, widget?.hsn, _hsns);
              },
              child: Container(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                child: Container(
                  margin: EdgeInsets.only(left: 15, top: 6, right: 15),
                  height: 34,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color(0xFFCA45FF).withOpacity(0.2),
                          Color(0xFF0365FF).withOpacity(0.2),
                        ]
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(17)),
                  ),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      SizedBox(width: 5,),
                      _toMusicLogoWidget(listDataBean),
                      SizedBox(width: 6.5,),
                      _toMusicNameWidget(listDataBean),
                      Spacer(),
                      _toMusicStatusWidget(listDataBean),
                      SizedBox(width: 12,),
                      Image.asset(
                        "images/icon_arrow_right_grey.png",
                        width: 6,
                      ),
                      SizedBox(width: 12,),
                    ],
                  ),
                ),
              )
          );
        }
    );

  }

  ///音乐logo布局
  Widget _toMusicLogoWidget(MusicListDataBean listDataBean){
    String asset = "images/icon_music_stop.png";
    if(listDataBean != null && (listDataBean?.musicList?.isNotEmpty??false) && (listDataBean?.isOpen()??false)){
      asset = "images/icon_music_play.png";
    }
    if((listDataBean?.isOpen()??false) && (listDataBean?.musicList?.isNotEmpty??false)){
      _controller.forward();
      return RotationTransition(
        alignment: Alignment.center,
        turns: _controller,
        child: Image.asset(
          asset,
          width: 24,
          height: 24,
        ),
      );
    }
    return Image.asset(
      asset,
      width: 24,
      height: 24,
    );
  }

  ///音乐名布局
  Widget _toMusicNameWidget(MusicListDataBean listDataBean){
    String content = "-";
    Color textColor = ThemeRepository.getInstance().getTextColor_D0E0F7();
    if(listDataBean == null || (listDataBean?.musicList?.isEmpty??true)){
      content = "暂无背景音乐";
      textColor = ThemeRepository.getInstance().getHintGreenColor_5E687C();
    }else{
      if(!(listDataBean?.isOpen()??false)){
        //未打开背景音乐
        content = "背景音乐";
      }else{
        //打开了背景音乐
        if(listDataBean.musicList.length == 1){
          content = listDataBean.musicList.elementAt(0)?.mname??"";
        }else{
          content = "${listDataBean.musicList.length}首背景音乐";
        }
      }
    }

    return Text(
      content,
      style: TextStyle(
        fontSize: 12,
        color: textColor,
      ),
    );
  }

  ///背景音乐状态
  Widget _toMusicStatusWidget(MusicListDataBean listDataBean){
    if(listDataBean == null || (listDataBean?.musicList?.isEmpty??true)){
      return SizedBox();
    }
    String text = "已关闭";
    Color textColor = ThemeRepository.getInstance().getMinorRedColor_F95355();
    if(listDataBean?.isOpen()??false){
      text = "已开启";
      textColor = Color(0XFF00C6C4);
    }
    return Text(
      text,
      style: TextStyle(
        fontSize: 12,
        color: textColor,
      ),
    );
  }

  ///企业信息
  Widget _toTerminalCompanyInfoWidget(String branchName, SmartHomeBean smartHomeBean){
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(left: 15, right: 15, top: 9, bottom: 17),
      child: Column(
        children: [
          _toSmartHomeStoreWidget(smartHomeBean),
          Visibility(
            visible: !UserRepository.getInstance().isSmartHomeCompany(),
            child: Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "所属单位",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      UserRepository.getInstance().userData.companyInfo.companynick,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14,
                          height: 1.22
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Visibility(
            visible: ("00" == UserRepository.getInstance().userData.companyInfo.comefrom) && !UserRepository.getInstance().isSmartHomeCompany(),
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: (){
                ChannelOrgListPage.navigatorPush(context, _gps, null,
                    cbid: _terminalInfoBean?.cbid,
                    onSetCbid: (cbid){
                      _viewModel.setCbid(_terminalInfoBean?.id, _terminalInfoBean?.hsn, "01", cbid);
                    }
                );
              },
              child: Row(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    height: 32,
                    width: 71,
                    child: Text(
                      "所属地址",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                          ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                          fontSize: 14,
                          height: 1.22
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 32,
                      alignment: Alignment.centerLeft,
                      child: Text(
                        StringUtils.isNotEmpty(branchName)?"${branchName??""}/${_address??""}":"暂未设置",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: StringUtils.isNotEmpty(branchName)?ThemeRepository.getInstance().getTextMainColor_D0E0F7():ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                            fontSize: 14,
                            height: 1.22
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 3,),
                  Image.asset(
                    "images/index_arrow_ico.png",
                    width: 6,
                  )
                ],
              ),
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              if(StringUtils.isNotEmpty((_terminalInfoBean?.gps??""))){
                ShowTerminalAddressPage.navigatorPush(context, _terminalInfoBean);
              }else{
                VgToastUtils.toast(context, "gps位置信息异常");
              }
            },
            child: Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "摆放位置",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      StringUtils.isNotEmpty(_terminalInfoBean?.position??"")?
                      _terminalInfoBean?.position:_terminalInfoBean?.address??"-",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: StringUtils.isNotEmpty(_terminalInfoBean?.position)
                              ?ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                              :ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                          fontSize: 14,
                          height: 1.22
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 3,),
                Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                )
              ],
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              SetTerminalBackupDialog.navigatorPushDialog(context,
                  backup: _terminalInfoBean?.terminalName,
                  onConfirm: (backup){
                    _viewModel.setBackup(_terminalInfoBean?.id, _terminalInfoBean?.hsn, "01", backup);
                  },
                  onDelete: (){
                    _viewModel.setBackup(_terminalInfoBean?.id, _terminalInfoBean?.hsn, "01", "");
                  }
              );
            },
            child: Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "备注说明",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      StringUtils.isNotEmpty(_terminalInfoBean?.terminalName??"")?_terminalInfoBean?.terminalName:"暂无",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: StringUtils.isNotEmpty(_terminalInfoBean?.terminalName)
                              ?ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                              :ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 3,),
                Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                )
              ],
            ),
          ),
          SizedBox(height: 9,),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTapUp: (detail){
                    // if(_terminalInfoBean.isTerminalOff()){
                    //   return;
                    // }
                    SetTerminalStatusMenuDialog.navigatorPushDialog(context,
                          (currentState){
                        _onOpen?.call(currentState);
                      },
                          (currentState){
                        _screenOff?.call(currentState);
                      },
                          (currentState){
                        _off?.call(currentState);
                      },
                          (time){

                      },
                      _terminalInfoBean?.getTerminalState(),
                      _terminalInfoBean.autoOnoffTime,
                      _terminalInfoBean.autoOnoffWeek,
                      _terminalInfoBean.hsn,
                      _terminalInfoBean.id,
                      _terminalInfoBean?.getTerminalName(),
                      _terminalInfoBean?.autoOnoff,
                      _terminalInfoBean?.comAutoSwitch,
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 40,
                    decoration: BoxDecoration(
                      color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          (_terminalInfoBean?.isTerminalOff()??true)?"images/icon_terminal_off_small.png":"images/icon_terminal_on_small.png",
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(width: 6,),
                        Text(
                          _terminalInfoBean?.getTerminalStatus()??"未知",
                          style: TextStyle(
                            fontSize: 14,
                            color:(_terminalInfoBean?.isTerminalOff()??false)
                                ?ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                                :ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                          ),
                        ),
                        Visibility(
                          // visible: !(_terminalInfoBean?.isTerminalOff()??true),
                          visible: true,
                          child: Row(
                            children: [
                              SizedBox(width: 4,),
                              Image(
                                image: AssetImage("images/icon_shape_arrow_down.png"),
                                width: 7,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 7,),
              Expanded(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: (){
                    SetTerminalVolumeDialog.navigatorPushDialog(context, _terminalInfoBean.soundonoff, _terminalInfoBean.hsn, _terminalInfoBean.volume);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 40,
                    decoration: BoxDecoration(
                      color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "音量：",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color:
                            ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                            fontSize: 14,
                          ),
                        ),
                        Text(
                          ("01" == _terminalInfoBean?.soundonoff??"")?"0%":"${_terminalInfoBean?.volume??50}%",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color:ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                            fontSize: 14,
                          ),
                        ),
                        SizedBox(width: 4,),
                        Image(
                          image: AssetImage("images/icon_shape_arrow_down.png"),
                          width: 7,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  ///所属门店
  Widget _toSmartHomeStoreWidget(SmartHomeBean smartHomeBean){
    return Visibility(
      visible: UserRepository.getInstance().isSmartHomeCompany(),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: (){
          SmartHomeSimpleStoreListPage.navigatorPush(context, smartHomeBean?.shid, widget?.hsn, smartHomeBean?.rasid);
        },
        child: Row(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              height: 32,
              width: 71,
              child: Text(
                "所属门店",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color:
                    ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                    fontSize: 14,
                    height: 1.22
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: 32,
                alignment: Alignment.centerLeft,
                child: Text(
                  smartHomeBean?.getTitle()??"",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 14,
                      height: 1.22
                  ),
                ),
              ),
            ),
            SizedBox(width: 3,),
            Image.asset(
              "images/index_arrow_ico.png",
              width: 6,
            )
          ],
        ),
      ),
    );
  }

  ///设备id 登录账号 GPS位置信息
  Widget _toDeviceInfoWidget(TerminalInfoBean terminalInfoBean){
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 15, right: 5, top: 15,),
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "机身编号",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      terminalInfoBean?.sideNumber ?? "-",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "硬件ID",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      terminalInfoBean?.hsn ?? "-",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  height: 32,
                  width: 71,
                  child: Text(
                    "登录账号",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                        fontSize: 14,
                        height: 1.22
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "${terminalInfoBean?.phone ?? ""} ${terminalInfoBean?.name ?? "-"}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                )
              ],
            ),
            Visibility(
              visible: StringUtils.isNotEmpty(terminalInfoBean?.appedition)?terminalInfoBean.isShowEndDay():
              UserRepository.getInstance().userData.companyInfo.isShowEndDay(),
              child: Row(
                children: <Widget>[
                  Container(
                    height: 32,
                    width: 71,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "账号状态",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                          ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                          fontSize: 14,
                          height: 1.22
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (_terminalInfoBean == null)?"":_terminalInfoBean.getEndDayString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                  Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (_terminalInfoBean == null)?"":_terminalInfoBean.getNotifyString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getMinorRedColor_F95355(),
                          fontSize: 14,
                          height: 1.2
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///设备状态
  // Widget _toDeviceStatusWidget(TerminalInfoBean terminalInfoBean){
  //   return Container(
  //     padding: EdgeInsets.only(left: 15, top: 12, right: 10),
  //     decoration: BoxDecoration(
  //       color: ThemeRepository.getInstance().getCardBgColor_21263C(),
  //     ),
  //     child: Row(
  //       children: <Widget>[
  //         Container(
  //           width: 79,
  //           alignment: Alignment.centerLeft,
  //           child: Text(
  //             "设备状态",
  //             maxLines: 1,
  //             overflow: TextOverflow.ellipsis,
  //             style: TextStyle(
  //               color:
  //               ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
  //               fontSize: 14,
  //             ),
  //           ),
  //         ),
  //         GestureDetector(
  //           behavior: HitTestBehavior.translucent,
  //           onTapUp: (detail){
  //             if(terminalInfoBean.isTerminalOff()){
  //               return;
  //             }
  //             SetTerminalStatusMenuDialog.navigatorPushDialog(context,
  //                   (currentState){
  //                 _onOpen?.call(currentState);
  //               },
  //                   (currentState){
  //                 _screenOff?.call(currentState);
  //               },
  //                   (currentState){
  //                 _off?.call(currentState);
  //               },
  //                   (time){
  //
  //               },
  //               terminalInfoBean?.getTerminalState(),
  //               terminalInfoBean.autoOnoffTime,
  //               terminalInfoBean.autoOnoffWeek,
  //               terminalInfoBean.hsn,
  //               terminalInfoBean.id,
  //               terminalInfoBean.getTerminalName(),
  //               terminalInfoBean?.autoOnoff,
  //             );
  //             // TerminalStateMenuDialogUtils.showMenuDialog(
  //             //     context: context,
  //             //     tabUpDetails: detail,
  //             //     itemMap: _getMenuMap(context, terminalInfoBean),
  //             //     sortType: currentType
  //             // );
  //           },
  //           child: Row(
  //             children: [
  //               Text(
  //                 terminalInfoBean?.getTerminalStatus()??"未知",
  //                 maxLines: 1,
  //                 overflow: TextOverflow.ellipsis,
  //                 style: TextStyle(
  //                   color:(terminalInfoBean?.isTerminalOff()??false)
  //                       ?ThemeRepository.getInstance().getTextMainColor_D0E0F7()
  //                       :ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
  //                   fontSize: 14,
  //                 ),
  //               ),
  //               Visibility(
  //                 visible: !(terminalInfoBean?.isTerminalOff()),
  //                 child: Row(
  //                   children: [
  //                     SizedBox(width: 5,),
  //                     Image(
  //                       image: AssetImage("images/icon_shape_arrow_down.png"),
  //                       width: 7,
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  ///人脸识别和开启声音
  Widget _toEnableFaceRecognizeAndVolumeWidget(TerminalInfoBean terminalInfoBean){
    return Container(
      padding: EdgeInsets.only(left: 15, top: 12, right: 10, bottom: 20),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(
        children: [
          _enableVolumeWidget(terminalInfoBean),
          _enableFaceRecognizeWidget(terminalInfoBean),
        ],
      ),
    );
  }


  ///人脸识别布局
  Widget _enableFaceRecognizeWidget(TerminalInfoBean terminalInfoBean){
    return Visibility(
      // visible: "01" == terminalInfoBean?.licenseflg,
      visible: true,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: ()async{
          if("00" == terminalInfoBean?.licenseflg){
            VgToastUtils.toast(context, "此设备暂不支持人脸识别");
            return;
          }
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
              title: "提示",
              content: ("00" == terminalInfoBean?.enableFaceRecognition)?"确定关闭该显示屏的人脸识别功能？":"确定启用该显示屏的人脸识别功能？",
              cancelText: "取消",
              confirmText: ("00" == terminalInfoBean?.enableFaceRecognition)?"关闭":"开启",
              confirmBgColor: ("00" == terminalInfoBean?.enableFaceRecognition)?ThemeRepository.getInstance()
                  .getMinorRedColor_F95355(): ThemeRepository.getInstance()
                  .getPrimaryColor_1890FF());
          if (result ?? false) {
            //00启用 01关闭
            _viewModel.changeFace(context, terminalInfoBean?.hsn, ("00" == terminalInfoBean?.enableFaceRecognition)?"01":"00");
          }
        },
        child: Row(
          children: [
            //licenseflg 00 未激活 01已激活
            Text(
              "人脸识别",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color:
                (("01" == (terminalInfoBean?.getEnableFaceRecognition()??"01")) || "00" == terminalInfoBean?.licenseflg)?
                ThemeRepository.getInstance().getTextMinorGreyColor_808388()
                    :ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                fontSize: 14,
              ),
            ),
            SizedBox(width: 10,),
            Image.asset(
              (("01" == (terminalInfoBean?.getEnableFaceRecognition()??"01")) || "00" == terminalInfoBean?.licenseflg)
                  ? "images/icon_close.png"
                  :"images/icon_open.png",
              width: 36,
            ),
          ],
        ),
      ),
    );
  }

  ///开启声音布局
  Widget _enableVolumeWidget(TerminalInfoBean terminalInfoBean){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        SetTerminalVolumeDialog.navigatorPushDialog(context, terminalInfoBean.soundonoff, terminalInfoBean.hsn, terminalInfoBean.volume);
      },
      child: Row(
        children: [
          Container(
            width: 79,
            child: Text(
              "音量设置",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color:
                ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Container(
            width: 77,
            child: Row(
              children: [
                Text(
                  ("01" == terminalInfoBean?.soundonoff??"")?"0%":"${terminalInfoBean?.volume??50}%",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color:ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 14,
                  ),
                ),
                SizedBox(width: 4,),
                Image(
                  image: AssetImage("images/icon_shape_arrow_down.png"),
                  width: 7,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget toPositionAndAddressWidget(TerminalInfoBean terminalInfoBean){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: 64,
          child: Text(
            "摆放位置",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:
                ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
                height: 1.2
            ),
          ),
        ),
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: Text(
            StringUtils.isNotEmpty(terminalInfoBean?.position??"")?terminalInfoBean?.position: "暂未设置",
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: (StringUtils?.isNotEmpty(terminalInfoBean?.position??"") ??false)
                    ? ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                    : ThemeRepository.getInstance().getMinorRedColor_F95355(),
                fontSize: 14,
                height: 1.2
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        GestureDetector(
          onTap: (){
            if(StringUtils.isNotEmpty((terminalInfoBean?.gps??""))){
              ShowTerminalAddressPage.navigatorPush(context, terminalInfoBean);
            }else{
              VgToastUtils.toast(context, "gps位置信息异常");
            }

          },
          child: Text((StringUtils?.isNotEmpty(terminalInfoBean?.position) ??false)
              ? "GPS定位"
              : "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:
                ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                fontSize: 14,
                height: 1.2
            ),
          ),
        ),
      ],
    );
  }

  void _onOpen(SetTerminalStateType currentState){
    print("开机");
    // if(widget?.currentState == currentState){
    //   return;
    // }
    _terminalListViewModel.terminalOffScreen(context, widget?.hsn, "00", callback: (){
    });
  }

  void _screenOff(SetTerminalStateType currentState){
    print("息屏开机");
    // if(widget?.currentState == currentState){
    //   return;
    // }
    _terminalListViewModel.terminalOffScreen(context, widget?.hsn, "01", callback: (){
    });
  }

  void _off(SetTerminalStateType currentState){
    print("关机");
    _terminalListViewModel.terminalOff(context, widget?.hsn, callback: (){
    });
  }

  // void _onOpen(TerminalInfoBean itemBean){
  //   print("开机");
  //   if("00" == itemBean.screenStatus){
  //     return;
  //   }
  //   _terminalListViewModel.terminalOffScreen(context, itemBean?.hsn, "00");
  // }
  //
  // void _screenOff(TerminalInfoBean itemBean){
  //   print("息屏开机");
  //   if("01" == itemBean.screenStatus){
  //     return;
  //   }
  //   _terminalListViewModel.terminalOffScreen(context, itemBean?.hsn, "01");
  // }
  //
  // void _off(TerminalInfoBean itemBean){
  //   print("关机");
  //   _terminalListViewModel.terminalOff(context, itemBean?.hsn);
  // }

  // Map<SetTerminalStateType,ValueChanged> _getMenuMap(BuildContext context, TerminalInfoBean itemBean){
  //   //开机状态，
  //   if(!itemBean.isTerminalOff()){
  //     return {
  //       SetTerminalStateType.open:(_){
  //         _onOpen?.call(itemBean);
  //       },
  //       SetTerminalStateType.screenOff:(_){
  //         _screenOff?.call(itemBean);
  //       },
  //       SetTerminalStateType.off:(_){
  //         _off?.call(itemBean);
  //       },
  //     };
  //   }
  //
  // }


  ///图片展示与选择
  Widget _toDevicePhotoWidget(TerminalInfoBean terminalInfoBean) {
    return Container(
      height: 85,
      margin: EdgeInsets.only(left: 9, top:21, right: 15),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => StringUtils.isEmpty(terminalInfoBean?.terminalPicurl ?? "")
            ?_chooseSinglePicAndClip(terminalInfoBean)
            :TerminalPicDetailPage.navigatorPush(context,
            url: terminalInfoBean?.terminalPicurl,
            selectMode: SelectMode.Normal,
            clipCompleteCallback: (path, cancelLoadingCallback) {
              terminalInfoBean?.terminalPicurl = path;
              // setState(() {});
              _viewModel.setTerminalPic(context, terminalInfoBean?.id, terminalInfoBean?.hsn,
                  "01", terminalInfoBean?.terminalPicurl);
            }),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Container(
            width: 85,
            height: 85,
            child: VgCacheNetWorkImage(
              terminalInfoBean?.terminalPicurl ?? "",
              placeWidget: Container(color: Colors.transparent,),
              emptyWidget:
              Image.asset("images/binding_terminal_add_pic_ico.png"),
            ),
          ),
        ),
      ),
    );
  }

  void _chooseSinglePicAndClip(TerminalInfoBean terminalInfoBean) async {
    String fileUrl =
    await MatisseUtil.clipOneImage(context, scaleX: 1, scaleY: 1,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        });
    if (fileUrl == null || fileUrl == "") {
      return null;
    }
    terminalInfoBean?.terminalPicurl = fileUrl;
    // setState(() {
    //   terminalInfoBean?.terminalPicurl = fileUrl;
    // });
    _viewModel.setTerminalPic(context, terminalInfoBean?.id, terminalInfoBean?.hsn,
        "01", terminalInfoBean?.terminalPicurl);
  }

  ///基本信息
  Widget _toBasicInfoWidget(TerminalInfoBean terminalInfoBean){
    return Container(
      padding: EdgeInsets.only(left: 15, top: 16, right: 16, bottom: 21),
      decoration: BoxDecoration(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      ),
      child: Column(
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () async{
              bool success = await EditBasicInfoPage.navigatorPush(context, terminalInfoBean);
              if(success != null && success){
                _viewModel.loading(true);
                _viewModel.getTerminalDetailInfo(widget?.hsn);
              }
            },
            child: Row(
              children: <Widget>[
                Text(
                  "基本信息",
                  style: TextStyle(
                      fontSize: 16,
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Text(
                      "修改",
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                          fontSize: 14
                      ),
                    ),
                    SizedBox(width: 9,),
                    Image.asset(
                      "images/index_arrow_ico.png",
                      width: 6,
                    )
                  ],
                ),
              ],
            ),
          ),

          SizedBox(
            height: 16,
          ),
          _TitleAndContentWidget(
            title: "设备名称",
            content: terminalInfoBean?.terminalName?? "-",
          ),
          SizedBox(
            height: 12,
          ),
          toPositionAndAddressWidget(terminalInfoBean),
          SizedBox(
            height: 12,
          ),
          Visibility(
            visible: !showMore,
            child: Container(
              child: GestureDetector(
                onTap: (){
                  showMore = !showMore;
                  setState(() {});
                },
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Spacer(),
                    Text(
                      "展开更多",
                      style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR,
                          fontSize: 14
                      ),
                    ),
                    SizedBox(width: 3,),
                    Image.asset(
                      "images/icon_show_more.png",
                      width: 9,
                      height: 20,
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),

          ),
          Visibility(
            visible: showMore,
            child: Column(
              children: <Widget>[
                _TitleAndContentWidget(
                  title: "屏幕规格",
                  content: BindingTerminalScreenSizeTypeExtension.getStrByType(terminalInfoBean?.screenSpecs),
                ),
                SizedBox(
                  height: 12,
                ),
                _TitleAndContentWidget(
                  title: "屏幕方向",
                  content: BindingTerminalScreenOrientationTypeExtension.getStrByType(terminalInfoBean?.screenDirection),
                ),
                SizedBox(
                  height: 12,
                ),
                _TitleAndContentWidget(
                  title: "是否触屏",
                  content: ("01" == (terminalInfoBean?.screenTouch??"00"))?"是":"否",
                ),
                SizedBox(
                  height: 12,
                ),
                _TitleAndContentWidget(
                  title: "设备类型",
                  content: BindingTerminalDeviceTypeExtension.getShowStrByType(terminalInfoBean?.type),
                ),
                SizedBox(
                  height: 12,
                ),
                _TitleAndContentWidget(
                    title: "终端版本",
                    content: (terminalInfoBean?.osversion??"")
                ),
                SizedBox(
                  height: 6,
                ),
                _TitleAndContentWidget(
                  title: "",
                  content: "APP " + (terminalInfoBean?.versionNo??""),
                ),

                // SizedBox(
                //   height: 12,
                // ),
                // Row(
                //   children: <Widget>[
                //     Container(
                //       width: 64,
                //       alignment: Alignment.centerLeft,
                //       child: Text(
                //         "人脸识别",
                //         maxLines: 1,
                //         overflow: TextOverflow.ellipsis,
                //         style: TextStyle(
                //           color:
                //           ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                //           fontSize: 14,
                //         ),
                //       ),
                //     ),
                //     SizedBox(width: 15,),
                //     Text(
                //       FaceRecognitionTypeExtension.getStrToType(terminalInfoBean?.enableFaceRecognition) == FaceRecognitionType.open
                //           ?"已启用"
                //           :"未启用",
                //       maxLines: 1,
                //       overflow: TextOverflow.ellipsis,
                //       style: TextStyle(
                //         color:
                //         FaceRecognitionTypeExtension.getStrToType(terminalInfoBean?.enableFaceRecognition) == FaceRecognitionType.open
                //             ?ThemeRepository.getInstance().getHintGreenColor_00C6C4()
                //             :ThemeRepository.getInstance().getMinorRedColor_F95355(),
                //         fontSize: 14,
                //       ),
                //     ),
                //
                //   ],
                // ),
              ],
            ),
          ),
        ],
      ),
    );
  }


  ///刷脸记录
  Widget _toFaceBrushingRecordWidget(TerminalInfoBean terminalInfoBean,EachFaceCntBean eachFaceCnt){
    return Visibility(
      visible: StringUtils.isNotEmpty(terminalInfoBean?.appedition)?terminalInfoBean.isShowFaceRecognition():
      UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition(),
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        padding: EdgeInsets.only(left: 15, right: 16, top: 16, bottom: 20),
        child:  GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            TerminalFaceBrushingRecordPage.navigatorPush(context,terminalInfoBean?.terminalName?? "终端名称",widget?.hsn);
          },
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Text(
                    "刷脸记录",
                    style: TextStyle(
                        fontSize: 16,
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  Spacer(),
                  _enableFaceRecognizeWidget(terminalInfoBean),
                  // _toPosterWidget(terminalInfoBean),
                ],
              ),
              //关闭人脸识别后，今日刷脸行隐藏
              //启用人脸识别 00启用 01关闭
              Visibility(
                  visible: ("00" == terminalInfoBean?.enableFaceRecognition),
                  child: SizedBox(height: 16,)
              ),
              Visibility(
                visible: ("00" == terminalInfoBean?.enableFaceRecognition),
                child: Row(
                  children: [
                    Text(
                      "今日刷脸${eachFaceCnt?.facecnt??0}人",
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                          fontSize: 14
                      ),
                    ),
                    Spacer(),
                    Image.asset(
                      "images/index_arrow_ico.png",
                      width: 6,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///播放管理
  Widget _toManagePlayListWidget(TerminalInfoBean terminalInfoBean){
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(left: 15, right: 16, top: 16, bottom: 20),
      child:  GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          if(widget?.popflg??false){
            RouterUtils.pop(context);
          }else{
            SingleTerminalMangePage.navigatorPush(
                context,
                widget?.hsn,
                _hsns,
                terminalInfoBean?.getTerminalName(),
                terminalInfoBean?.position,
                terminalInfoBean?.cbid,
                terminalInfoBean?.getTerminalState(),
                terminalInfoBean?.rcaid,
                onOffTime: terminalInfoBean?.autoOnoffTime,
                onOffWeek: terminalInfoBean?.autoOnoffWeek,
                autoOnOff: terminalInfoBean?.autoOnoff,
                autoBean: terminalInfoBean?.comAutoSwitch
            );
          }
        },
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Text(
                  "播放管理",
                  style: TextStyle(
                      fontSize: 16,
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                _toPosterWidget(terminalInfoBean),
              ],
            ),
            SizedBox(height: 16,),
            Row(
              children: [
                Text(
                  "${terminalInfoBean?.picCnt ?? 0}图片， ${terminalInfoBean?.videoCnt ?? 0}视频",
                  style: TextStyle(
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 14
                  ),
                ),
                Spacer(),
                _toMusicWidget(terminalInfoBean),
                SizedBox(width: 10,),
                Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _toMusicWidget(TerminalInfoBean terminalInfoBean){
    String text = "暂无背景音乐";
    Color textColor = VgColors.INPUT_BG_COLOR;
    if(terminalInfoBean == null || (terminalInfoBean?.musicNum??0) == 0){
      return Text(
        text,
        style: TextStyle(
          fontSize: 14,
          color: textColor,
        ),
      );
    }
    text = "背景音乐关闭";
    textColor = ThemeRepository.getInstance().getMinorRedColor_F95355();
    if(terminalInfoBean?.isMusicOpen()??false){
      text = "${terminalInfoBean?.musicNum??0}背景音乐";
      textColor = ThemeRepository.getInstance().getTextMainColor_D0E0F7();
    }
    return Text(
      text,
      style: TextStyle(
        fontSize: 14,
        color: textColor,
      ),
    );
  }

  ///AI海报推送开关
  Widget _toPosterWidget(TerminalInfoBean terminalInfoBean){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: ()async{
        //00启用 01关闭一天 02永久关闭
        if(("00" == terminalInfoBean?.pushflg)){
          CloseAiPosterDialog.navigatorPushDialog(context,
                  () {
                _viewModel.changeAiPoster(context, terminalInfoBean?.hsn, "01");
              },
                  () {
                _viewModel.changeAiPoster(context, terminalInfoBean?.hsn, "02");
              });
        }else{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
              title: "提示",
              content: "确定启用AI海报推送功能？",
              cancelText: "取消",
              confirmText: "开启",
              confirmBgColor: ThemeRepository.getInstance()
                  .getPrimaryColor_1890FF());
          if (result ?? false) {
            _viewModel.changeAiPoster(context, terminalInfoBean?.hsn, "00");
          }
        }
      },
      child: Row(
        children: [
          Text(
            "AI海报推送",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color:
              ("00" == terminalInfoBean?.pushflg)?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                  :ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
              fontSize: 14,
            ),
          ),
          SizedBox(width: 10,),
          Image.asset(
            ("00" == terminalInfoBean?.pushflg)
                ? "images/icon_open.png"
                :"images/icon_close.png",
            width: 36,
          ),
        ],
      ),
    );

  }

  ///管理员
  Widget _toAdminListWidget(List<AdminListBean> adminList,TerminalInfoBean terminalInfoBean){
    var count=(adminList.length>4?4:adminList.length)+1;
    // var count =2;
    return  GestureDetector(
      onTap: (){
        AddDeviceAdministratorWidget.navigatorPush(context, terminalInfoBean?.hsn);
      },
      child: Container(
        padding: EdgeInsets.only( top: 16, right: 16, bottom: 15),
        decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                SizedBox(width: 15,),
                Text(
                  "设备管理",
                  style: TextStyle(
                      fontSize: 16,
                      color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    Text(
                      adminList.length.toString() + "人",
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                          fontSize: 14
                      ),
                    ),
                    SizedBox(width: 9,),
                    Image.asset(
                      "images/index_arrow_ico.png",
                      width: 6,
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              height: ScreenUtils.screenW(context)/5,
              child: GridView.builder(
                  padding: EdgeInsets.all(0),
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                  ),
                  scrollDirection: Axis.vertical,
                  itemCount: count,
                  // separatorBuilder: (context, index){
                  //   return SizedBox(width: 20,);
                  // },
                  itemBuilder: (context, index){
                    if(index==count-1){
                      return _addAdminWidget();
                    }
                    return _toAdminInfo(adminList[index]);
                  }
              ),
            ),
          ],
        ),

      ),
    );
  }

  ///管理员列表item
  Widget _toAdminInfo(AdminListBean adminListBean){
    return Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(shape: BoxShape.circle),
            child: VgCacheNetWorkImage(
              showOriginEmptyStr(adminListBean.napicurl)??"",
              defaultErrorType: ImageErrorType.head,
              defaultPlaceType: ImagePlaceType.head,
            ),
          ),
        ),
        SizedBox(height: 5,),
        Container(
          width: 60,
          alignment: Alignment.center,
          child: Text(
            adminListBean?.nick??"-",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14
            ),
          ),
        ),
      ],
    );
  }

  ///添加管理员布局
  Widget _addAdminWidget(){
    return GestureDetector(
      onTap: (){
        BindingAddAdminListPage.navigatorPush(context,widget?.hsn);
      },
      child: Column(
        children: <Widget>[
          Image.asset(
            "images/icon_add_admin.png",
            width: 40,
            height: 40,
          ),
          SizedBox(height: 5,),
          Text(
            "添加",
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14
            ),
          ),
        ],
      ),
    );
  }

  Widget _toSplitLineWidget() {
    return Container(
      height: 0.5,
      margin: const EdgeInsets.only(left: 0, right: 0),
      color: ThemeRepository.getInstance().getLineColor_3A3F50(),
    );
  }

  ///更多设置
  Widget _moreSettingsIndexWidget(TerminalInfoBean terminalInfoBean){
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      height: 54,
      padding: EdgeInsets.only(left: 15, right: 16,),
      child:  GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          EditMoreSettingsPage.navigatorPush(context, widget?.hsn, terminalInfoBean,
              terminalCount: widget?.terminalCount, router: widget?.router);
        },
        child: Row(
          children: <Widget>[
            Text(
              "更多设置",
              style: TextStyle(
                  fontSize: 16,
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontWeight: FontWeight.bold
              ),
            ),
            Spacer(),
            Image.asset(
              "images/index_arrow_ico.png",
              width: 6,
            ),
          ],
        ),
      ),
    );
  }

//   ///更多设置布局
//   Widget _moreSettingsWidget(TerminalInfoBean terminalInfoBean){
//     terminalInfoBean?.startDateTime = terminalInfoBean?.getStartOrEndTime(
//         terminalInfoBean?.autoOnoffTime, 0);
//     terminalInfoBean?.endDateTime = terminalInfoBean?.getStartOrEndTime(
//         terminalInfoBean?.autoOnoffTime, 1);
//     return Container(
//       padding: EdgeInsets.only(left: 15, top: 13, right: 16),
//       decoration: BoxDecoration(
//         color: ThemeRepository.getInstance().getCardBgColor_21263C(),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Text(
//             "更多设置",
//             style: TextStyle(
//                 fontSize: 16,
//                 color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                 fontWeight: FontWeight.bold
//             ),
//           ),
//           ///摄像头校正
//           GestureDetector(
//             behavior: HitTestBehavior.opaque,
//             onTap: ()async{
//               dynamic result = await EditFixCameraPage.navigatorPush(context, terminalInfoBean);
//               if(result == null){
//                 _viewModel.exitBindTerminal(widget?.hsn);
//               }
//             },
//             child: Container(
//               height: 50,
//               child: Row(
//                 children: <Widget>[
//                   Text(
//                     "摄像头校正",
//                     style: TextStyle(
//                         color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//                         fontSize: 14
//                     ),
//                   ),
//                   Spacer(),
//                   Image.asset(
//                     "images/index_arrow_ico.png",
//                     width: 6,
//                   ),
//                   // Row(
//                   //   children: <Widget>[
//                   //     Text(
//                   //     terminalInfoBean?.correctCamera == null || terminalInfoBean?.correctCamera == -1 ?"-":terminalInfoBean?.correctCamera.toString() + "°",
//                   //       style: TextStyle(
//                   //           color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                   //           fontSize: 14
//                   //       ),
//                   //     ),
//                   //     SizedBox(width: 10,),
//                   //     Image.asset(
//                   //       "images/index_arrow_ico.png",
//                   //       width: 6,
//                   //     )
//                   //   ],
//                   // ),
//                 ],
//               ),
//             ),
//           ),
//           _toSplitLineWidget(),
//           ///摄像头启动距离
//           GestureDetector(
//             behavior: HitTestBehavior.opaque,
//             onTap: ()async{
//               double result= await CameraDistanceWidget.navigatorPush(context, terminalInfoBean?.getCameraStartDistanceValue());
//               if(result!=null){
//                 terminalInfoBean?.startCameraDistance = _getDistance(result);
//                 _viewModel.infoValueNotifier.value.terminalInfo = terminalInfoBean;
//                 setState(() {});
//                 _viewModel.setCameraStartDistance(terminalInfoBean.mid, terminalInfoBean.id,
//                     terminalInfoBean.hsn, "01", terminalInfoBean.startCameraDistance);
//                 print("摄像头启动距离$result");
//               }
// //              return SelectUtil.showListSelectDialog(
// //                  context: context,
// //                  title: "摄像头启动距离",
// //                  positionStr: terminalInfoBean?.getCameraStartDistanceStr(),
// //                  textList: ConstantRepository.of().getCameraStartDistance(),
// //                  onSelect: (dynamic value) {
// //                    terminalInfoBean?.startCameraDistance = double.parse(value.toString().replaceAll("米", ""));
// //                    _viewModel.infoValueNotifier.value.terminalInfo = terminalInfoBean;
// //                    setState(() {});
// //                    _viewModel.setCameraStartDistance(terminalInfoBean.mid, terminalInfoBean.id,
// //                        terminalInfoBean.hsn, "01", terminalInfoBean.startCameraDistance);
// //                  }
// //              );
//             },
//             child: Container(
//               height: 50,
//               child: Row(
//                 children: <Widget>[
//                   Text(
//                     "摄像头启动距离",
//                     style: TextStyle(
//                         color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//                         fontSize: 14
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     children: <Widget>[
//                       Text(
//                         terminalInfoBean?.startCameraDistance == null?"-":terminalInfoBean?.getCameraStartDistanceStr(),
//                         style: TextStyle(
//                             color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                             fontSize: 14
//                         ),
//                       ),
//                       SizedBox(width: 10,),
//                       Image.asset(
//                         "images/index_arrow_ico.png",
//                         width: 6,
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//           _toSplitLineWidget(),
//           ///刷脸记录间隔
//           GestureDetector(
//             behavior: HitTestBehavior.opaque,
//             onTap: (){
//               return SelectUtil.showListSelectDialog(
//                   context: context,
//                   title: "刷脸记录间隔",
//                   positionStr: terminalInfoBean?.getPunchInIntervalStr(),
//                   textList: ConstantRepository.of().getPunchInInterval(),
//                   onSelect: (dynamic value) {
//                     terminalInfoBean?.timeInterval = int.parse(value.toString().replaceAll("分钟", ""));
//                     setState(() {});
//                     _viewModel.setPunchInInterval(terminalInfoBean.mid, terminalInfoBean.id,
//                         terminalInfoBean.hsn, "01", terminalInfoBean.timeInterval);
//                   }
//               );
//             },
//             child: Container(
//               height: 50,
//               child: Row(
//                 children: <Widget>[
//                   Text(
//                     "刷脸记录间隔",
//                     style: TextStyle(
//                         color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//                         fontSize: 14
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     children: <Widget>[
//                       Text(
//                         terminalInfoBean?.timeInterval == null?"-":terminalInfoBean?.getPunchInIntervalStr(),
//                         style: TextStyle(
//                             color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                             fontSize: 14
//                         ),
//                       ),
//                       SizedBox(width: 10,),
//                       Image.asset(
//                         "images/index_arrow_ico.png",
//                         width: 6,
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//           _toSplitLineWidget(),
//           ///自动开关机
//           GestureDetector(
//             behavior: HitTestBehavior.opaque,
//             onTap: () async {
//               BindingTerminalAutoOffStatusType autoOffStatusType;
//               Map<String, dynamic> resultMap =
//               await BindingTerminalAutoSwitchDialog.navigatorPushDialog(
//                   context,
//                   terminalInfoBean?.startDateTime,
//                   terminalInfoBean?.endDateTime,
//                   BindingTerminalCameraStatusTypeExtension.getStrToType(terminalInfoBean?.closeCamera) ==
//                       BindingTerminalCameraStatusType.open);
//               if (resultMap == null || resultMap.isEmpty) {
//                 return;
//               }
//               terminalInfoBean?.startDateTime =
//               resultMap[BINDING_TERMINAL_POP_START_DATE_TIME_KEY];
//               terminalInfoBean?.endDateTime =
//               resultMap[BINDING_TERMINAL_POP_END_DATE_TIME_KEY];
//               autoOffStatusType =
//               terminalInfoBean?.startDateTime == null || terminalInfoBean?.endDateTime == null
//                   ? BindingTerminalAutoOffStatusType.unSetting
//                   : BindingTerminalAutoOffStatusType.setting;
//               terminalInfoBean?.closeCamera =
//                   BindingTerminalCameraStatusTypeExtension.getTypeToStr(
//                       resultMap[BINDING_TERMINAL_POP_IS_ENABLE_SYNC_KEY]
//                           ? BindingTerminalCameraStatusType.open
//                           : BindingTerminalCameraStatusType.close);
//               terminalInfoBean.autoOnoffTime = terminalInfoBean.getAutoOffTimeStr();
//               setState(() {});
//               _viewModel.setAutoOnOffTime(terminalInfoBean.id,
//                   terminalInfoBean.hsn, "01",
//                   terminalInfoBean.autoOnoffTime,
//                   terminalInfoBean?.closeCamera,
//                   "1,2,3,4,5",
//               );
//             },
//             child: Container(
//               height: 50,
//               child: Row(
//                 children: <Widget>[
//                   Text(
//                     "自动开关机",
//                     style: TextStyle(
//                         color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//                         fontSize: 14
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     children: <Widget>[
//                       Text(
//                         terminalInfoBean?.getAutoOffTimeStr()??"暂未设置",
//                         style: TextStyle(
//                             color: terminalInfoBean.startDateTime == null ||
//                                 terminalInfoBean.endDateTime == null
//                                 ? ThemeRepository.getInstance()
//                                 .getTextEditHintColor_3A3F50()
//                                 : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//                             fontSize: 14
//                         ),
//                       ),
//                       SizedBox(width: 10,),
//                       Image.asset(
//                         "images/index_arrow_ico.png",
//                         width: 6,
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

  double _getDistance(double value){
    if(0.0 == value){
      return 1.0;
    }
    if(1.0 == value){
      return 1.5;
    }
    if(2.0 == value){
      return 2.0;
    }
    if(3.0 == value){
      return 2.5;
    }
    if(4.0 == value){
      return 3.0;
    }
    return 2.0;
  }
}

/// 标题和内容行组件
class _TitleAndContentWidget extends StatelessWidget{

  final String title;

  final String content;
  final int contentLines;

  const _TitleAndContentWidget({
    Key key,
    @required this.title,
    @required this.content,
    this.contentLines,
  }):super(key:key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 71,
          height: 32,
          alignment: Alignment.centerLeft,
          child: Text(
            title ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:
                ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 14,
                height: 1.22
            ),
          ),
        ),
        Expanded(
          child: Container(
            height: 32,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 1),
            child: Text(
              content ?? "-",
              maxLines: contentLines??3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 14,
                  height: 1.2
              ),
            ),
          ),
        )
      ],
    );
  }
}

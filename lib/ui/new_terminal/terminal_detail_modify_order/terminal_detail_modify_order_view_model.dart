import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/event/terminal_list_refresh_event.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

class TerminalDetailModifyOrderViewModel extends BaseViewModel{
  TerminalDetailModifyOrderViewModel(BaseState<StatefulWidget> state) : super(state);

  ///保存终端顺序（逗号分割id）
  void saveTerminalOrder(BuildContext context,String ids, String hsn){
    if(ids == null || ids.isEmpty){
      VgToastUtils.toast(context, "顺序失败");
      return;
    }
    VgHudUtils.show(context,"保存中");
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appComAttPicOrderby",params:{
      "authId": UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "ids":ids ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          RouterUtils.pop(context,result: true);
          VgEventBus.global.send(MediaLibraryRefreshEven());
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }
  //添加管理员
  void saveTerminalAddAdmin(BuildContext context,String hsn,String userIdArr){
    // VgHudUtils.show(context,"保存中");
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appTerminalAddAdmin",params:{
      "authId": UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "userIdArr":userIdArr ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgToastUtils.toast(context, "添加成功");
          VgHudUtils.hide(context);
          RouterUtils.pop(context);
          VgEventBus.global.send(AddAdminRefreshEvent());
          VgEventBus.global.send(new TerminalListRefreshEvent());
          // VgEventBus.global.send(new TerminalListRefreshEvent());
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }

  //移除管理员
  void removeTerminalAdmin(BuildContext context,String hsn,String removeUId){
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appTerminalRemoveAdmin",params:{
      "authId": UserRepository.getInstance().authId ?? "",
      "hsn": hsn ?? "",
      "removeUId":removeUId ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgToastUtils.toast(context, "移除成功");
          VgHudUtils.hide(context);
          // RouterUtils.pop(context);
          VgEventBus.global.send(AddAdminRefreshEvent());
          VgEventBus.global.send(new TerminalListRefreshEvent());
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }
}
class AddAdminRefreshEvent{}
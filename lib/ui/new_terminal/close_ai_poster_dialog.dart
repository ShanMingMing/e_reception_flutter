import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';


///AI海报推送开关弹窗
class CloseAiPosterDialog extends StatefulWidget{


  final  VoidCallback onCloseOneDay;
  final  VoidCallback onCloseForever;

  const CloseAiPosterDialog({Key key, this.onCloseOneDay, this.onCloseForever}) : super(key: key);

  @override
  _CloseAiPosterDialogState createState() => _CloseAiPosterDialogState();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context,
      VoidCallback onCloseOneDay,
      VoidCallback onCloseForever) {
    return VgDialogUtils.showCommonBottomDialog<DateTime>(
      context: context,
      child: CloseAiPosterDialog(
        onCloseOneDay:onCloseOneDay,
        onCloseForever:onCloseForever,
      ),
    );
  }


}

class _CloseAiPosterDialogState extends BaseState<CloseAiPosterDialog> {

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
      child: Container(
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getCardBgColor_21263C()),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Center(
              child: Container(
                height: 35,
                alignment: Alignment.center,
                child: Text(
                  "关闭AI海报推送",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 12,
                      color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                  ),
                ),
              ),
            ),
            Container(
              height: 0.5,
              color: Color(0xFF303546),
            ),
            _toCloseOneDayWidget(),
            Container(
              height: 0.5,
              color: Color(0xFF303546),
            ),
            _toCloseForeverWidget(),
            // _toScreenOffWidget(),
            Container(
              height: 10,
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            ),
            _toCancelButtonWidget(context),
          ],
        ),
      ),
    );
  }


  Widget _toCloseOneDayWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        widget?.onCloseOneDay?.call();
        RouterUtils.pop(context);
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 12),
        height: 55,
        alignment: Alignment.center,
        child: Text(
          "仅当日关闭",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
              height: 1.2),
        ),
      ),
    );
  }

  Widget _toCloseForeverWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        widget?.onCloseForever?.call();
        RouterUtils.pop(context);
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 12),
        height: 55,
        alignment: Alignment.center,
        child: Text(
          "永久关闭",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
              height: 1.2),
        ),
      ),
    );
  }

  Widget _toCancelButtonWidget(BuildContext context) {
    final double bottomStatusHeight =  ScreenUtils.getBottomBarH(context);
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        RouterUtils.pop(context);
      },
      child: Container(
        height: 55 + bottomStatusHeight,
        padding: EdgeInsets.only(bottom: bottomStatusHeight),
        child: Center(
          child: Text(
            "取消",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
        ),
      ),
    );
  }

}
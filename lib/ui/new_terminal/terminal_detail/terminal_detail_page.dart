import 'dart:io';
import 'dart:math';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_edit_with_title_dialog/common_edit_with_title_dialog.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_detail_update.dart';
import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/media_detail_page.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/media_settings_page.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/poster_detail_page.dart';
import 'package:e_reception_flutter/media_library/media_library_index/service/media_library_index_service.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_terminal/event/play_file_update_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/new_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/terminal_detail_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/widgets/terminal_detail_add_button_widget.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_add_folder/terminal_detail_add_folder_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_add_folder/terminal_detail_add_folder_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/material.dart';
import 'package:matisse_android_plugin/export_matisse.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_permission_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import 'bean/terminal_detail_response_bean.dart';
import 'widgets/terminal_detail_list_item_widget.dart';
import 'widgets/terminal_detail_page_view_widget.dart';
import 'widgets/terminal_detail_setting_bar_widget.dart';
import 'widgets/terminal_detail_top_bar_widget.dart';

/// 终端详情
///
/// @author: zengxiangxi
/// @createTime: 2/4/21 2:02 PM
/// @specialDemand:
class TerminalDetailPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "TerminalDetailPage";

  final String hsn;

  const TerminalDetailPage({Key key, this.hsn,}) : super(key: key);

  @override
  _TerminalDetailPageState createState() => _TerminalDetailPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, String hsn,) {
    if (StringUtils.isEmpty(hsn)) {
      VgToastUtils.toast(context, "终端设备获取失败");
      return null;
    }
    return RouterUtils.routeForFutureResult(
      context,
      TerminalDetailPage(
        hsn: hsn,
      ),
      routeName: TerminalDetailPage.ROUTER,
    );
  }
}

class _TerminalDetailPageState
    extends BasePagerState<TerminalDetailListItemBean, TerminalDetailPage>
    with VgPlaceHolderStatusMixin {
  ValueNotifier<bool> _isShowAddButtonValueNotifier;

  TerminalDetailViewModel _viewModel;
  TerminalDetailAddFolderViewModel _addFolderViewModel;

  String _jumpToFolderId;

  ValueNotifier<String> _jumpToFolderIdValueNotifier;
  ValueNotifier<List<TerminalDetailListItemBean>> _dataUpdateValueNotifier;

  @override
  void initState() {
    super.initState();
    VgEventBus.global.streamController.stream.listen((event) {
      if (event is MediaLibraryRefreshEven || event is MediaDetailUpdateEvent) {
        _viewModel.refresh();
      }
    });
    _viewModel = TerminalDetailViewModel(this, widget.hsn);
    _addFolderViewModel = TerminalDetailAddFolderViewModel(this, widget.hsn,);
    _isShowAddButtonValueNotifier = ValueNotifier(true);
    _jumpToFolderIdValueNotifier = ValueNotifier(null);
    _dataUpdateValueNotifier = ValueNotifier(null);
    _viewModel.refresh();
  }

  @override
  void dispose() {
    _isShowAddButtonValueNotifier?.dispose();
    _jumpToFolderIdValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainStackWidget(),
    );
  }

  Widget _toMainStackWidget() {
    return Stack(
      children: <Widget>[
        _toMainColumnWidget(),
        Align(
          alignment: Alignment.bottomRight,
          child: ValueListenableBuilder(
              valueListenable: _isShowAddButtonValueNotifier,
              builder: (BuildContext context, bool isShow, Widget child) {
                //避免无数据隐藏掉添加按钮
                if (data == null || data.isEmpty) {
                  isShow = true;
                }
                return AnimatedOpacity(
                  opacity: isShow ? 1 : 0,
                  duration: DEFAULT_ANIM_DURATION,
                  child: child,
                );
              },
              child: TerminalDetailAddButtonWidget(
                onTap: () async {
                  _addFolderViewModel.getList(context, widget?.hsn, (size) async {
                    if(size < 1){
                      _processUpload();
                    }else{
                      String result =
                      await TerminalDetailAddFolderPage.navigatorPush(
                          context, widget.hsn, "");
                      if (StringUtils.isNotEmpty(result)) {
                        _jumpToFolderId = result;
                        _viewModel?.refreshMultiPage();
                        VgEventBus.global.send(PlayFileUpdateEvent('播放文件更新'));
                      } else {
                        _jumpToFolderId = null;
                      }
                    }
                  });

                },
              )),
        )
      ],
    );
  }

  void _processUpload() async {
    List<String> resultList = await MatisseUtil.selectAll(
      context: context,
      isCanMixSelect: false,
      maxImageSize: 9,
      maxVideoSize: 1,
      videoMemoryLimit: 500*1024,
      videoDurationLimit: 10*60,
      videoDurationMinLimit: 10,
      maxAutoFinish: false,
        onRequestPermission: (msg)async{
          bool result =
          await CommonConfirmCancelDialog.navigatorPushDialog(context,
            title: "提示",
            content: msg,
            cancelText: "取消",
            confirmText: "去授权",
          );
          if (result ?? false) {
            PermissionUtil.openAppSettings();
          }
        }
    );
    if (resultList == null || resultList.isEmpty) {
      return;
    }
    List<UploadMediaLibraryItemBean> _uploadList = List();
    Map<String, ImageInfo> _imageInfoMap = new Map();
    String localUrl = resultList[0];
    if (StringUtils.isEmpty(localUrl)) {
      return;
    }

    if(resultList.length == 1 && MatisseUtil.isVideo(localUrl)){
      //视频
      loading(true, msg: "视频处理中");
      handleVideo(localUrl, _uploadList);
    }else {
      if(resultList.length > 1){
        loading(true, msg: "图片处理中");
        int needClipCount = 0;
        int computeCount = 0;
        resultList.forEach((element) {
          FileImage image = FileImage(File(element));
          image.resolve(new ImageConfiguration())
              .addListener(
              new ImageStreamListener((ImageInfo info, bool _) async {
                _imageInfoMap[element] = info;
                double width = info.image.width + 0.0;
                double height = info.image.height + 0.0;
                if (width / height != 9.0 / 16.0) {
                  needClipCount++;
                }
                computeCount++;
                if (computeCount == resultList.length) {
                  loading(false);
                  if(needClipCount > 0){
                    bool result =
                    await CommonConfirmCancelDialog.navigatorPushDialog(context,
                        title: "提示",
                        content: "检测到${needClipCount}张图片长宽比不符合16:9，是否逐个裁剪处理？",
                        cancelText: "保持原比例",
                        confirmText: "去裁剪",
                        confirmBgColor: ThemeRepository.getInstance()
                            .getPrimaryColor_1890FF());
                    //去裁剪
                    if(result == null){
                      RouterUtils.pop(context);
                      return;
                    }
                    if (result ?? false) {
                      handleImage(localUrl, resultList, _uploadList);
                    } else {
                      //保持原图比例
                      resultList.forEach((element) {
                        _uploadList.add(UploadMediaLibraryItemBean(
                          localUrl: element,
                          mediaType: MediaType.image,
                          folderWidth: _imageInfoMap[element].image.width,
                          folderHeight: _imageInfoMap[element].image.height,));
                      });


                      String result = await MediaSettingsPage.navigatorPushString(
                          context, _uploadList[0],
                          singleflg: true,
                          needUpload: true,
                          currentHsn: widget?.hsn,
                          hsns: widget?.hsn,
                          itemList: _uploadList, branchname: "");
                      if (StringUtils.isNotEmpty(result)) {
                        RouterUtils.pop(context);
                      }
                    }
                  }else{
                    //保持原图比例
                    resultList.forEach((element) {
                      _uploadList.add(UploadMediaLibraryItemBean(
                        localUrl: element,
                        mediaType: MediaType.image,
                        folderWidth: _imageInfoMap[element].image.width,
                        folderHeight: _imageInfoMap[element].image.height,));
                    });

                    String result = await MediaSettingsPage.navigatorPushString(
                        context, _uploadList[0],
                        singleflg: true,
                        needUpload: true,
                        currentHsn: widget?.hsn,
                        hsns: widget?.hsn,
                        itemList: _uploadList, branchname: "");
                    if (StringUtils.isNotEmpty(result)) {
                      RouterUtils.pop(context);
                    }
                  }

                }
              }));
        });
      }else{
        handleImage(localUrl, resultList, _uploadList);
      }


    }
  }

  ///处理视频的逻辑
  void handleVideo(String localUrl, List<UploadMediaLibraryItemBean> _uploadList)async{
    //视频
    UploadMediaLibraryItemBean item = UploadMediaLibraryItemBean(
      localUrl: localUrl, mediaType: MediaType.video,);

    File file = File(item.localUrl);
    Directory rootPath = await getTemporaryDirectory();
    Directory tempDirectory = new Directory('${rootPath.path}/imageTemp');
    if (!tempDirectory.existsSync()) {
      tempDirectory.createSync();
    }

    String tmpVideoCover = await VideoThumbnail.thumbnailFile(
      video: item.localUrl,
      thumbnailPath: tempDirectory?.path,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 1920,
      quality: 100,
    );
    item.videoLocalCover = tmpVideoCover;
    item.videoLocalCover = tmpVideoCover;
    //
    VideoPlayerController videoController = VideoPlayerController.file(file);
    await videoController.initialize();
    item.videoDuration = videoController?.value?.duration?.inSeconds;
    item.folderWidth = videoController?.value?.size?.width?.floor();
    item.folderHeight = videoController?.value?.size?.height?.floor();
    item.folderStorage = file.lengthSync();

    _uploadList.add(item);
    videoController.dispose();
    loading(false);
    String result = await MediaSettingsPage.navigatorPushString(context, _uploadList[0],
        singleflg: true, needUpload:true, currentHsn: widget?.hsn, hsns: widget?.hsn,
        itemList: _uploadList, branchname: "");
    if(StringUtils.isNotEmpty(result)){
      RouterUtils.pop(context);
    }
  }

  ///处理图片
  void handleImage(String localUrl, List<String> resultList, List<UploadMediaLibraryItemBean> _uploadList){
    //图片
    List<Future> taskList = resultList.reversed.map((element) {
      FileImage image = FileImage(File(element));
      // 预先获取图片信息
      image
          .resolve(new ImageConfiguration())
          .addListener(new ImageStreamListener((ImageInfo info,
          bool _) async {
        double width = info.image.width + 0.0;
        double height = info.image.height + 0.0;

        if (width / height != 9.0 / 16.0) {
          String clipPath = await MatisseUtil.clipOneImage(context,
            path: element, scaleX: 9, scaleY: 16,
            isCanMixRadio: true,
            topLeftWidget: _topLeftWidget(),
            keepRadioWidget: _keepWidget(info, element, _uploadList, resultList.length),
            rightTopClipWidget: _topRightWidget(),
              onRequestPermission: (msg)async{
                bool result =
                await CommonConfirmCancelDialog.navigatorPushDialog(context,
                  title: "提示",
                  content: msg,
                  cancelText: "取消",
                  confirmText: "去授权",
                );
                if (result ?? false) {
                  PermissionUtil.openAppSettings();
                }
              } );
          if(clipPath == null){
            return;
          }
          if (StringUtils.isNotEmpty(clipPath)) {
            FileImage clipImage = FileImage(File(clipPath));
            clipImage
                .resolve(new ImageConfiguration())
                .addListener(new ImageStreamListener((ImageInfo info, bool _) async {
              _uploadList.add(UploadMediaLibraryItemBean(
                localUrl: clipPath,
                mediaType: MediaType.image,
                folderWidth: info.image.width,
                folderHeight: info.image.height,
              ));
              if (_uploadList.length == resultList.length) {
                String result = await MediaSettingsPage
                    .navigatorPushString(context, _uploadList[0],
                    singleflg: true,
                    needUpload: true,
                    currentHsn: widget?.hsn,
                    hsns: widget?.hsn,
                    itemList: _uploadList, branchname: "");
                if (StringUtils.isNotEmpty(result)) {
                  RouterUtils.pop(context);
                }
              }
            }));



          } else {
            if (_uploadList.length == resultList.length) {
              //判断是否是重复数据
              int count = 0;
              _uploadList.forEach((element) {
                if(localUrl == element.localUrl){
                  count++;
                }
              });
              if(count == 0){
                _uploadList.add(UploadMediaLibraryItemBean(
                  localUrl: localUrl,
                  mediaType: MediaType.image,
                  folderWidth: info.image.width,
                  folderHeight: info.image.height,));
              }
            }
          }
        }

        else {
          _uploadList.add(UploadMediaLibraryItemBean(
            localUrl: element,
            mediaType: MediaType.image,
            folderWidth: info.image.width,
            folderHeight: info.image.height,));

          if (_uploadList.length == resultList.length) {
            String result = await MediaSettingsPage.navigatorPushString(
                context, _uploadList[0],
                singleflg: true,
                needUpload: true,
                currentHsn: widget?.hsn,
                hsns: widget?.hsn,
                itemList: _uploadList, branchname: "");
            if (StringUtils.isNotEmpty(result)) {
              RouterUtils.pop(context);
            }
          }
        }
      }));

      return new Future(() {
        print(element);
      });
    }).toList();

  }

  Widget _topLeftWidget(){
    return ClickAnimateWidget(
      child: Container(
        width: 40,
        height: 44,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.only(left: 15.6),
        child: Image.asset(
          "images/top_bar_back_ico.png",
          width: 9,
          color: VgColors.INPUT_BG_COLOR,
        ),
      ),
      scale: 1.4,
      onClick: () {
        // VgNavigatorUtils.pop(context);
        FocusScope.of(context).requestFocus(FocusNode());
        Navigator.of(context).maybePop();
      },
    );
  }

  Widget _keepWidget(ImageInfo info, String localUrl, List<UploadMediaLibraryItemBean> _uploadList, int totalSize){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: ()async{
        if(StringUtils.isNotEmpty(localUrl)){
          Navigator.of(context).pop();
          _uploadList.add(UploadMediaLibraryItemBean(
            localUrl: localUrl,
            mediaType: MediaType.image,
            folderWidth: info.image.width,
            folderHeight: info.image.height,));
          // bool result = await MediaSettingsPage.navigatorPush(context, _uploadList[0]);
          // if(result??false){
          //   _viewModel?.refreshMultiPage();
          // }
          if(_uploadList.length == totalSize){
            String result = await MediaSettingsPage.navigatorPushString(context, _uploadList[0],
                singleflg: true, needUpload:true,  currentHsn: widget?.hsn,
                hsns: widget?.hsn, itemList: _uploadList, branchname: "");
            if(StringUtils.isNotEmpty(result)) {
              RouterUtils.pop(context);
            }
          }

        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 22),
        child: Container(
          height: 21.5,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
          alignment: Alignment.center,
          child: Text(
            "原始比例",
            style: TextStyle(color: Color(0xffaaaaaa), fontSize: 12, height: Platform.isIOS ? 1.2 : 1.15),
          ),
        ),
      ),
    );
  }

  Widget _topRightWidget(){
    return Container(
      height: 23,
      width: 48,
      alignment: Alignment.center,
      margin: EdgeInsets.only(left: 15, right: 15, top: 11, bottom: 10),
      decoration: BoxDecoration(
        color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        borderRadius: BorderRadius.circular(11.5),
      ),
      child: Text(
        "确定",
        style: TextStyle(
            fontSize: 12,
            color: Colors.white
        ),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        TerminalDetailTopBarWidget(
          hsn: widget.hsn,
          hsnInfoValueNotifier: _viewModel?.hsnInfoValueNotifier,
          onRefresh:() {
            _viewModel.refresh();
          },
        ),
        Offstage(
            offstage: data == null || data.isEmpty, child: _toSwiperWidget()),
        Offstage(
            offstage: data == null || data.isEmpty,
            child: TerminalDetailSettingBarWidget(
              total: data?.length ?? 0,
              hsn: widget.hsn,
              onRefresh:() {
                _viewModel.refresh();
              },
            )),
        Expanded(
          child: MixinPlaceHolderStatusWidget(
              emptyOnClick: () => _viewModel?.refresh(),
              errorOnClick: () => _viewModel?.refresh(),
              // errorCustomSetting: (bool isError){
              //   isEmptyStatus = true;
              //   isErrorStatus = false;
              //   Future((){
              //     setState(() { });
              //   });
              // },
              child: _toListWidget()),
        )
      ],
    );
  }

  Widget _toSwiperWidget() {
    return ValueListenableBuilder(
      valueListenable: _viewModel?.hsnInfoValueNotifier,
      builder: (BuildContext context, HsnMapBean hsnInfoBean, Widget child) {
        Axis axis;
        if (StringUtils.isEmpty(hsnInfoBean?.screenDirection) ||
            hsnInfoBean.screenDirection == "01") {
          axis = Axis.vertical;
        } else {
          axis = Axis.horizontal;
        }
        return TerminalDetailPageViewWidget(
          axis: axis,
          list: data,
          jumpToFolderIdValueNotifier: _jumpToFolderIdValueNotifier,
          dataUpdateValueNotifier: _dataUpdateValueNotifier,
          onRepeat: _onRepeat,
          onTap: _onTap,
          onDeleteTap: _onDeleteTap,
          onDeleteForever: _onDeleteForver,
          onUpdateName: _onUpdateName,
        );
      },
    );
  }

  Widget _toListWidget() {
    return NotificationListener(
      onNotification: (Notification notification) {
        if (notification is ScrollStartNotification) {
          _isShowAddButtonValueNotifier?.value = false;
        }
        if (notification is ScrollUpdateNotification) {
          _isShowAddButtonValueNotifier?.value = false;
        }
        if (notification is ScrollEndNotification) {
          _isShowAddButtonValueNotifier?.value = true;
        }
        return false;
      },
      child: ScrollConfiguration(
        behavior: MyBehavior(),
        child: VgPullRefreshWidget.bind(
          viewModel: _viewModel,
          state: this,
          enablePullDown: true,
          enablePullUp: false,
          child: ListView.separated(
              itemCount: data?.length ?? 0,
              padding: const EdgeInsets.all(0),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              itemBuilder: (BuildContext context, int index) {
                return TerminalDetailListItemWidget(
                    itemBean: data?.elementAt(index),
                    onTap: _onTap,
                    onDeleteTap: _onDeleteTap,
                    onDeleteForever: _onDeleteForver,
                    onUpdateName: _onUpdateName);
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 0,
                );
              }),
        ),
      ),
    );
  }

  _onRepeat(TerminalDetailListItemBean itemBean) {
    if (StringUtils.isEmpty(itemBean?.picid)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    _viewModel?.repeatPlay(context, itemBean.picid, itemBean.editStopFlg, itemBean.id);
  }

  _onTap(TerminalDetailListItemBean itemBean) {
    if(itemBean.isHtml()){
      PosterDetailPage.navigatorPush(context, itemBean.htmlurl, itemBean.id,
          itemBean.likeflg, itemBean.cretype,
          itemBean.hsn, false, itemBean.logoflg, itemBean.addressflg, itemBean.diyflg, "01",
          itemBean.addressdiy, itemBean.namediy, itemBean.logodiy,
          itemBean.splpicurl, itemBean.pictype);
    }else{
      MediaDetailPage.navigatorPush(context, itemBean?.picid, "01", id: itemBean?.id);
    }
    // VgPhotoPreview.listForPage(
    //   context,
    //   data,
    //   initUrl: itemBean?.picurl,
    //   urlTrasformFunc: (item) => item?.picurl,
    //   loadingTransformFunc: (item) => VgPhotoPreview.getImageQualityStr(
    //       item.getCoverUrl(), ImageQualityType.middleDown),
    // );
  }

  _onDeleteTap(TerminalDetailListItemBean itemBean) async{
    if (StringUtils.isEmpty(itemBean?.picid)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: "仅在当前终端显示屏取消播放，确认不再播放？",
        cancelText: "取消",
        confirmText: "确定",
        confirmBgColor:
        ThemeRepository.getInstance().getMinorRedColor_F95355());
    if (result ?? false) {
      _viewModel?.deleteFolder(context, itemBean.picid);
    }
  }

  void _onDeleteForver(TerminalDetailListItemBean itemBean) async {
    if (StringUtils.isEmpty(itemBean?.picid)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: "所有终端显示屏中该文件都会删除，且不可找回，确认继续？",
        cancelText: "取消",
        confirmText: "删除",
        confirmBgColor:
        ThemeRepository.getInstance().getMinorRedColor_F95355());
    if (result ?? false) {
      _viewModel?.deleteForeverFolder(context, itemBean.picid, itemBean?.videoid, itemBean?.getToDeletePicUrl(), (itemBean?.isVideo()??false)?(itemBean?.picurl??""):"");
    }
  }

  void _onUpdateName(TerminalDetailListItemBean itemBean) {
    if (StringUtils.isEmpty(itemBean?.id)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    CommonEditWithTitleDialog.navigatorPushDialog(context,
        title: "重命名",
        confirmText: "确定",
        oldContent: itemBean.picname,
        onConfirm: (String content, VoidCallback popCallBack) {
          if (StringUtils.isEmpty(content)) {
            VgToastUtils.toast(context, "文件名不能为空");
            return;
          }
          VgToolUtils.removeAllFocus(context);
          _viewModel?.updateFolderName(
              context, itemBean?.picid, content, popCallBack);
        });
  }

  @override
  void setListData(List<TerminalDetailListItemBean> list) {
    super.setListData(list);
    _dataUpdateValueNotifier.value = list;
  }

  @override
  void loading(bool show, {String msg}) {
    ///有更新请求更新
    if (StringUtils.isNotEmpty(_jumpToFolderId)) {
      _jumpToFolderIdValueNotifier.value = _jumpToFolderId;
      _jumpToFolderId = null;
    }
    super.loading(show, msg: msg);
  }
}

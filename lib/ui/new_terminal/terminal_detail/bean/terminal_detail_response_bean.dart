import 'dart:convert';

import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"hsnMap":{"autoOnoff":"00","terminalName":"三星横屏pad","terminalPicurl":"http://etpic.we17.com/test/20210201192530_5305.jpg","hsn":"96e4a7e9ab42d713","backup":"","screenDirection":"横屏","address":"北京市丰台区南三环西路辅路3号楼靠近中国工商银行(搜宝商务中心支行)","mid":"c04cc50eabea411c8b8d6d83f870a295","closeCamera":"","autoOnoffTime":"","id":57,"rcaid":57},"list":[{"palytime":0,"createtime":1612594762,"picsize":"474*683","playnum":0,"picstorage":"7333","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/picture/20210109114719_7269.jpg","picname":"20210109114719_7269.jpg","id":"9e5a644d93554ab5a35a12e4e0e50188","picid":"cb3a4f63baa04ab6b8a8d1ac172a0ac8","multiples":1},{"palytime":0,"createtime":1612594762,"picsize":"474*683","playnum":0,"picstorage":"4500","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/picture/20210109114420_0385.jpg","picname":"20210109114420_0385.jpg","id":"aad9f2ea10084a7fb0dabc68cd3db0ae","picid":"dedf31c5f56b403d87c2efd737adb8ea","multiples":1},{"palytime":0,"createtime":1612594612,"picsize":"826*466","playnum":0,"picstorage":"15000","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/test/20210206103419_1684.jpg","picname":"hengtu.jpg","id":"9e670aba09fa477087f7df8ea31de400","picid":"c0f36fda3705400a994a44b31ea5ed19","multiples":1}]}
/// extra : null

class TerminalDetailResponseBean extends BasePagerBean<TerminalDetailListItemBean>{
  bool success;
  String code;
  String msg;
  TerminalDetailDataBean data;
  dynamic extra;

  static TerminalDetailResponseBean fromMap(Map<String, dynamic> map, {bool isCache}) {
    if (map == null) return null;
    TerminalDetailResponseBean terminalDetailResponseBeanBean = TerminalDetailResponseBean();
    terminalDetailResponseBeanBean.success = map['success'];
    terminalDetailResponseBeanBean.code = map['code'];
    terminalDetailResponseBeanBean.msg = map['msg'];
    terminalDetailResponseBeanBean.data = TerminalDetailDataBean.fromMap(map['data'], isCache: isCache);
    terminalDetailResponseBeanBean.extra = map['extra'];
    return terminalDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<TerminalDetailListItemBean> getDataList() => data?.list;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.list = list.cast();
  }
}

/// hsnMap : {"autoOnoff":"00","terminalName":"三星横屏pad","terminalPicurl":"http://etpic.we17.com/test/20210201192530_5305.jpg","hsn":"96e4a7e9ab42d713","backup":"","screenDirection":"横屏","address":"北京市丰台区南三环西路辅路3号楼靠近中国工商银行(搜宝商务中心支行)","mid":"c04cc50eabea411c8b8d6d83f870a295","closeCamera":"","autoOnoffTime":"","id":57,"rcaid":57}
/// list : [{"palytime":0,"createtime":1612594762,"picsize":"474*683","playnum":0,"picstorage":"7333","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/picture/20210109114719_7269.jpg","picname":"20210109114719_7269.jpg","id":"9e5a644d93554ab5a35a12e4e0e50188","picid":"cb3a4f63baa04ab6b8a8d1ac172a0ac8","multiples":1},{"palytime":0,"createtime":1612594762,"picsize":"474*683","playnum":0,"picstorage":"4500","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/picture/20210109114420_0385.jpg","picname":"20210109114420_0385.jpg","id":"aad9f2ea10084a7fb0dabc68cd3db0ae","picid":"dedf31c5f56b403d87c2efd737adb8ea","multiples":1},{"palytime":0,"createtime":1612594612,"picsize":"826*466","playnum":0,"picstorage":"15000","videotime":"0","type":"01","comattid":57,"videopic":" ","picurl":"https://pic.008box.com/test/20210206103419_1684.jpg","picname":"hengtu.jpg","id":"9e670aba09fa477087f7df8ea31de400","picid":"c0f36fda3705400a994a44b31ea5ed19","multiples":1}]

class TerminalDetailDataBean {
  HsnMapBean hsnMap;
  HsnwbrandMapBean hsnwbrandMap;
  List<UpListBean> upList;
  List<DownListBean> downList;
  List<TerminalDetailListItemBean> list;
  List<SmartHomeListBean> smartHomeList;
  List<TerminalDetailListItemBean> eventList;
  List<TerminalDetailListItemBean> festivalList;

  static TerminalDetailDataBean fromMap(Map<String, dynamic> map, {bool isCache}) {
    if (map == null) return null;
    TerminalDetailDataBean dataBean = TerminalDetailDataBean();
    dataBean.hsnMap = HsnMapBean.fromMap(map['hsnMap']);
    dataBean.hsnwbrandMap = HsnwbrandMapBean.fromMap(map['hsnwbrandMap']);
    dataBean.upList = List()..addAll(
        (map['upList'] as List ?? []).map((o) => UpListBean.fromMap(o))
    );
    dataBean.downList = List()..addAll(
        (map['downList'] as List ?? []).map((o) => DownListBean.fromMap(o))
    );
    dataBean.smartHomeList = List()..addAll(
        (map['smartHomeList'] as List ?? []).map((o) => SmartHomeListBean.fromMap(o))
    );
    dataBean.list = List()..addAll(
        (map['list'] as List ?? []).map((o) => TerminalDetailListItemBean.fromMap(o))
    );
    dataBean.eventList = List()..addAll(
        (map['eventList'] as List ?? []).map((o) => TerminalDetailListItemBean.fromMap(o))
    );
    dataBean.festivalList = List()..addAll(
        (map['festivalList'] as List ?? []).map((o) => TerminalDetailListItemBean.fromMap(o))
    );
    if(dataBean.festivalList != null && dataBean.festivalList.length > 0){
      dataBean.list.add(dataBean.festivalList[0]);
    }
    if(dataBean.eventList != null && dataBean.eventList.length > 0){
      dataBean.list.add(dataBean.eventList[0]);
    }
    if(dataBean.hsnwbrandMap != null && !(isCache??false)){
      BuildingIndexJsonData jsonData = BuildingIndexJsonData.fromMap(map);
      String dataJson = json.encode(jsonData);
      //containerFlg 00大屏机 01app
      String url = dataBean.hsnwbrandMap.htmlurl + "?containerFlg=01&contJson=" + Uri.encodeComponent(dataJson);
      TerminalDetailListItemBean terminalDetailListItemBean = new TerminalDetailListItemBean();
      terminalDetailListItemBean.type = "01";
      terminalDetailListItemBean.picurl = dataBean.hsnwbrandMap.picurl;
      terminalDetailListItemBean.htmlurl = url;
      terminalDetailListItemBean.isBuildingIndex = true;
      terminalDetailListItemBean.comwid = dataBean.hsnwbrandMap.comwid;
      terminalDetailListItemBean.picid = dataBean.hsnwbrandMap.id;
      terminalDetailListItemBean.buildingIndexId = dataBean.hsnwbrandMap.id;
      terminalDetailListItemBean.stopflg = dataBean.hsnwbrandMap.stopflg;
      dataBean.list.insert(0, terminalDetailListItemBean);
    }
    if(dataBean.smartHomeList != null && !(isCache??false) && dataBean.smartHomeList.length > 0){
      dataBean.smartHomeList.forEach((element) {
        TerminalDetailListItemBean terminalDetailListItemBean = new TerminalDetailListItemBean();
        terminalDetailListItemBean.isSmartHome = true;
        terminalDetailListItemBean.type = "01";
        terminalDetailListItemBean.stopflg = element.stopflg;
        terminalDetailListItemBean.shid = element.shid;
        terminalDetailListItemBean.name = element.name;
        terminalDetailListItemBean.logo = element.logo;
        terminalDetailListItemBean.rasid = element.rasid;
        terminalDetailListItemBean.picList = element.picList;
        dataBean.list.add(terminalDetailListItemBean);
      });

    }
    return dataBean;
  }

  Map toJson() => {
    "hsnMap": hsnMap,
    "list": list,
    "eventList": eventList,
    "festivalList": festivalList,
    "hsnwbrandMap": hsnwbrandMap,
    "upList": upList,
    "downList": downList,
    "smartHomeList": smartHomeList,
  };
}

class BuildingIndexJsonData{
  HsnwbrandMapBean hsnwbrandMap;
  List<UpListBean> upList;
  List<DownListBean> downList;

  static BuildingIndexJsonData fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexJsonData dataBean = BuildingIndexJsonData();
    dataBean.hsnwbrandMap = HsnwbrandMapBean.fromMap(map['hsnwbrandMap']);
    dataBean.upList = List()..addAll(
        (map['upList'] as List ?? []).map((o) => UpListBean.fromMap(o))
    );
    dataBean.downList = List()..addAll(
        (map['downList'] as List ?? []).map((o) => DownListBean.fromMap(o))
    );
    return dataBean;
  }
  Map toJson() => {
    "hsnwbrandMap": hsnwbrandMap,
    "upList": upList,
    "downList": downList,
  };
}

class HsnwbrandMapBean {
  String stopflg;//01重复播放 00取消重复
  String wtitle;
  String syswid;
  String htmlurl;
  String picurl;
  String id;
  String comwid;

  static HsnwbrandMapBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnwbrandMapBean hsnwbrandMapBean = HsnwbrandMapBean();
    hsnwbrandMapBean.stopflg = map['stopflg'];
    hsnwbrandMapBean.wtitle = map['wtitle'];
    hsnwbrandMapBean.syswid = map['syswid'];
    hsnwbrandMapBean.htmlurl = map['htmlurl'];
    hsnwbrandMapBean.picurl = map['picurl'];
    hsnwbrandMapBean.id = map['id'];
    hsnwbrandMapBean.comwid = map['comwid'];
    return hsnwbrandMapBean;
  }

  Map toJson() => {
    "stopflg": stopflg,
    "wtitle": wtitle,
    "syswid": syswid,
    "htmlurl": htmlurl,
    "picurl": picurl,
    "id": id,
    "comwid": comwid,
  };
}

class DownListBean {
  String fnum;
  String floorflg;
  String company;
  String id;
  String comwid;
  String hnum;

  static DownListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DownListBean downListBean = DownListBean();
    downListBean.fnum = map['fnum'];
    downListBean.floorflg = map['floorflg'];
    downListBean.company = map['company'];
    downListBean.id = map['id'];
    downListBean.comwid = map['comwid'];
    downListBean.hnum = map['hnum'];
    return downListBean;
  }

  Map toJson() => {
    "fnum": fnum,
    "floorflg": floorflg,
    "company": company,
    "id": id,
    "comwid": comwid,
    "hnum": hnum,
  };
}

class UpListBean {
  String fnum;
  String floorflg;
  String company;
  String id;
  String comwid;
  String hnum;

  static UpListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpListBean upListBean = UpListBean();
    upListBean.fnum = map['fnum'];
    upListBean.floorflg = map['floorflg'];
    upListBean.company = map['company'];
    upListBean.id = map['id'];
    upListBean.comwid = map['comwid'];
    upListBean.hnum = map['hnum'];
    return upListBean;
  }

  Map toJson() => {
    "fnum": fnum,
    "floorflg": floorflg,
    "company": company,
    "id": id,
    "comwid": comwid,
    "hnum": hnum,
  };
}

/// palytime : 0
/// createtime : 1612594762
/// picsize : "474*683"
/// playnum : 0
/// picstorage : "7333"
/// videotime : "0"
/// type : "01"
/// comattid : 57
/// videopic : " "
/// picurl : "https://pic.008box.com/picture/20210109114719_7269.jpg"
/// picname : "20210109114719_7269.jpg"
/// id : "9e5a644d93554ab5a35a12e4e0e50188"
/// picid : "cb3a4f63baa04ab6b8a8d1ac172a0ac8"
/// multiples : 1

class TerminalDetailListItemBean {
  int palytime;
  int createtime;
  String picsize;
  String splpicsize;
  int playnum;
  String picstorage;
  String videotime;
  String type;
  int comattid;
  String videopic;
  String videoid;
  String picurl;
  String htmlurl;
  String picname;
  String id;
  String picid;
  int multiples;
  String stopflg;//00 不重复 01重复
  String editStopFlg;//01 重复 02不重复
  String hsn;
  String startday;
  String endday;
  String uploadflg;//00自传 01 推送
  int reday;//有效天数
  int suday;//剩余播放天数 已播天数=suday -1   推送的不显示剩余天数
  int playDay;//已播天数
  String likeflg;//01喜欢 02不喜欢 03不喜欢删除
  String cretype;//00:什么都没有 01:logo 02:名称 03:地址 04:logo+名称 05:logo+地址 06:名称+地址 07:全部都有
  String logoflg;//00显示 01不显示
  String addressflg;//00显示 01不显示
  String diyflg;//00不可diy 01diy
  String calflg;// 00没有日历 01有日历
  String calx;//日历坐标
  String caly;//日历坐标
  String addressdiy;
  String namediy;
  String logodiy;
  String splpicurl;
  String pictureurl;
  String pictype;
  String cbid;
  //水牌
  bool isBuildingIndex;
  String comwid;
  String buildingIndexId;

  //智能家居
  bool isSmartHome;
  String shid;//门店id
  String name;//门店名
  String logo;//门店logo
  String rasid;
  List<PicListBean> picList;//门店图片

  String getPicSize(){
    if(isHtml()){
      return splpicsize;
    }
    return picsize;
  }


  String getPicUrl(){
    //之前是合成海报，并且自传文件地址不为空，就展示自传地址
    //现在改为只要自传地址不为空，就展示
    // if("09" == pictype && StringUtils.isNotEmpty(splpicurl)){
    if(StringUtils.isNotEmpty(splpicurl)){
      return splpicurl;
    }
    if(StringUtils.isNotEmpty(picurl) && !picurl.endsWith(".html")){
      return picurl;
    }
    return pictureurl;
  }


  String getPlayTimeString(){
    if(isIndex() || isSmart()){
      return "长期有效";
    }
    //系统推送文件
    if("01" == uploadflg){
      if(playDay == null){
        return "暂无播放周期";
      }
      return "已播${playDay??0}天";
      // return "已播${reday-suday+1}天";
    }
    //异常数据
    if(StringUtils.isEmpty(startday) || StringUtils.isEmpty(endday)){
      return "暂无播放周期";
    }

    DateTime startDate = DateTime.parse(startday);
    DateTime endDate = DateTime.parse(endday);
    DateTime currentDate = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //待播放
    if(startDate.millisecondsSinceEpoch > currentDate.millisecondsSinceEpoch){
      int interval = (((startDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
      return interval.toString() + "天后播放";
    }
    //已结束
    if(endDate.millisecondsSinceEpoch < currentDate.millisecondsSinceEpoch){
      int interval = (((endDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
      return "已播放" + interval.toString() + "天";
    }
    //播放中
    //1.永久播放
    if("9999-99-99" == endday || "10007-06-0" == endday){
      return "永久播放";
    }
    //2.已播多少天
    int alreadyPlay = (((currentDate.millisecondsSinceEpoch - startDate.millisecondsSinceEpoch)~/(1000*3600*24)) + 1);
    int resumePlay = (((endDate.millisecondsSinceEpoch - currentDate.millisecondsSinceEpoch)~/(1000*3600*24)));
    return "已播" + alreadyPlay.toString() + "天 剩余" + resumePlay.toString() + "天";

  }

  static TerminalDetailListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalDetailListItemBean listBean = TerminalDetailListItemBean();
    listBean.palytime = map['palytime'];
    listBean.createtime = map['createtime'];
    listBean.picsize = map['picsize'];
    listBean.splpicsize = map['splpicsize'];
    listBean.playnum = map['playnum'];
    listBean.picstorage = map['picstorage'];
    listBean.videotime = map['videotime'];
    listBean.type = map['type'];
    listBean.comattid = map['comattid'];
    listBean.videopic = map['videopic'];
    listBean.videoid = map['videoid'];
    listBean.picurl = map['picurl'];
    listBean.htmlurl = map['htmlurl'];
    listBean.picname = map['picname'];
    listBean.id = map['id'];
    listBean.picid = map['picid'];
    listBean.multiples = map['multiples'];
    listBean.stopflg = map['stopflg'];
    listBean.hsn = map['hsn'];
    listBean.startday = map['startday'];
    listBean.endday = map['endday'];
    listBean.uploadflg = map['uploadflg'];
    listBean.reday = map['reday'];
    listBean.suday = map['suday'];
    listBean.likeflg = map['likeflg'];
    listBean.cretype = map['cretype'];
    listBean.playDay = map['playDay'];
    listBean.logoflg = map['logoflg'];
    listBean.addressflg = map['addressflg'];
    listBean.addressdiy = map['addressdiy'];
    listBean.namediy = map['namediy'];
    listBean.logodiy = map['logodiy'];
    listBean.diyflg = map['diyflg'];
    listBean.calflg = map['calflg'];
    listBean.calx = map['calx'];
    listBean.caly = map['caly'];
    listBean.splpicurl = map['splpicurl'];
    listBean.pictureurl = map['pictureurl'];
    listBean.pictype = map['pictype'];
    listBean.cbid = map['cbid'];
    //水牌
    listBean.isBuildingIndex = map['isBuildingIndex'];
    listBean.comwid = map['comwid'];
    listBean.buildingIndexId = map['buildingIndexId'];

    //智能家居
    listBean.isSmartHome = map['isSmartHome'];
    listBean.shid = map['shid'];
    listBean.name = map['name'];
    listBean.logo = map['logo'];
    listBean.rasid = map['rasid'];
    if(map['picList'] != null){
      listBean.picList = List()..addAll(
          (map['picList'] as List ?? []).map((o) => PicListBean.fromMap(o))
      );
    }
    return listBean;
  }

  Map toJson() => {
    "palytime": palytime,
    "createtime": createtime,
    "picsize": picsize,
    "splpicsize": splpicsize,
    "playnum": playnum,
    "picstorage": picstorage,
    "videotime": videotime,
    "type": type,
    "comattid": comattid,
    "videopic": videopic,
    "videoid": videoid,
    "picurl": picurl,
    "htmlurl": htmlurl,
    "picname": picname,
    "id": id,
    "picid": picid,
    "multiples": multiples,
    "stopflg": stopflg,
    "hsn": hsn,
    "startday": startday,
    "endday": endday,
    "uploadflg": uploadflg,
    "reday": reday,
    "suday": suday,
    "likeflg": likeflg,
    "cretype": cretype,
    "playDay": playDay,
    "logoflg": logoflg,
    "addressflg": addressflg,
    "diyflg": diyflg,
    "calflg": calflg,
    "calx": calx,
    "caly": caly ,
    "addressdiy": addressdiy ,
    "namediy": namediy ,
    "logodiy": logodiy ,
    "splpicurl": splpicurl ,
    "pictureurl": pictureurl ,
    "pictype": pictype ,
    "cbid": cbid ,
    "isBuildingIndex": isBuildingIndex ,
    "comwid": comwid ,
    "buildingIndexId": buildingIndexId ,

    "isSmartHome": isSmartHome ,
    "shid": shid ,
    "name": name ,
    "logo": logo ,
    "rasid": rasid ,
    "picList": picList ,

  };

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  bool isHtml(){
    if("01" == type && StringUtils.isNotEmpty(htmlurl)){
      return true;
    }
    return false;
  }

  ///是否是水牌
  bool isIndex(){
    return isBuildingIndex??false;
  }

  ///是否是智能家居
  bool isSmart(){
    return isSmartHome??false;
  }

  ///是否有终端使用
  bool isUserFolder(){
    // return playnum != null && playnum > 0;
    return false;
  }

  ///获取可删除的图片地址
  String getToDeletePicUrl(){
    if(isVideo()){
      return videopic;
    }
    if("01" == (uploadflg?? "")){
      //是推送海报，不删除图片地址
      return "";
    }
    return getPicUrl();
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return getPicUrl();
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is TerminalDetailListItemBean &&
              runtimeType == other.runtimeType &&
              id == other.id;

  @override
  int get hashCode => id.hashCode;
}

/// autoOnoff : "00"
/// terminalName : "三星横屏pad"
/// terminalPicurl : "http://etpic.we17.com/test/20210201192530_5305.jpg"
/// hsn : "96e4a7e9ab42d713"
/// backup : ""
/// screenDirection : "横屏"
/// address : "北京市丰台区南三环西路辅路3号楼靠近中国工商银行(搜宝商务中心支行)"
/// mid : "c04cc50eabea411c8b8d6d83f870a295"
/// closeCamera : ""
/// autoOnoffTime : ""
/// id : 57
/// rcaid : 57

class HsnMapBean {
  String autoOnoff;
  String terminalName;
  String terminalPicurl;
  String hsn;
  String backup;
  String screenDirection;
  String address;
  String mid;
  String closeCamera;
  String autoOnoffTime;
  String autoOnoffWeek;//"1,2,3,4,5"
  int id;
  int rcaid;
  String position;

  static HsnMapBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnMapBean hsnMapBean = HsnMapBean();
    hsnMapBean.autoOnoff = map['autoOnoff'];
    hsnMapBean.terminalName = map['terminalName'];
    hsnMapBean.terminalPicurl = map['terminalPicurl'];
    hsnMapBean.hsn = map['hsn'];
    hsnMapBean.backup = map['backup'];
    hsnMapBean.screenDirection = map['screenDirection'];
    hsnMapBean.address = map['address'];
    hsnMapBean.mid = map['mid'];
    hsnMapBean.closeCamera = map['closeCamera'];
    hsnMapBean.autoOnoffTime = map['autoOnoffTime'];
    hsnMapBean.autoOnoffWeek = map['autoOnoffWeek'];
    hsnMapBean.id = map['id'];
    hsnMapBean.rcaid = map['rcaid'];
    hsnMapBean.position = map['position'];
    return hsnMapBean;
  }

  Map toJson() => {
    "autoOnoff": autoOnoff,
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "backup": backup,
    "screenDirection": screenDirection,
    "address": address,
    "mid": mid,
    "closeCamera": closeCamera,
    "autoOnoffTime": autoOnoffTime,
    "autoOnoffWeek": autoOnoffWeek,
    "id": id,
    "rcaid": rcaid,
    "position": position,
  };
}

class SmartHomeListBean {
  String stopflg;
  String shid;
  String name;
  String logo;
  String rasid;
  List<PicListBean> picList;

  static SmartHomeListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeListBean smartHomeListBean = SmartHomeListBean();
    smartHomeListBean.stopflg = map['stopflg'];
    smartHomeListBean.shid = map['shid'];
    smartHomeListBean.name = map['name'];
    smartHomeListBean.logo = map['logo'];
    smartHomeListBean.rasid = map['rasid'];
    smartHomeListBean.picList = List()..addAll(
        (map['picList'] as List ?? []).map((o) => PicListBean.fromMap(o))
    );
    return smartHomeListBean;
  }

  Map toJson() => {
    "stopflg": stopflg,
    "shid": shid,
    "name": name,
    "logo": logo,
    "rasid": rasid,
    "picList": picList,
  };
}

class PicListBean {
  String picurl;
  String backup;
  String picsize;
  String picstorage;
  int videotime;
  String type;
  String videopic;
  String coverurl;
  String videoid;

  String getPicUrl(){
    if(isVideo()){
      return videopic;
    }
    if(is3DLink()){
      return coverurl;
    }
    return picurl;
  }

  bool is3DLink(){
    return type == "03";
  }

  bool isVideo(){
    return type == "02";
  }

  static PicListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PicListBean picListBean = PicListBean();
    picListBean.picurl = map['picurl'];
    picListBean.backup = map['backup'];
    picListBean.picsize = map['picsize'];
    picListBean.picstorage = map['picstorage'];
    picListBean.videotime = map['videotime'];
    picListBean.type = map['type'];
    picListBean.videopic = map['videopic'];
    picListBean.videoid = map['videoid'];
    picListBean.coverurl = map['coverurl'];
    return picListBean;
  }

  Map toJson() => {
    "picurl": picurl,
    "backup": backup,
    "picsize": picsize,
    "picstorage": picstorage,
    "videotime": videotime,
    "type": type,
    "videopic": videopic,
    "videoid": videoid,
    "coverurl": coverurl,
  };
}
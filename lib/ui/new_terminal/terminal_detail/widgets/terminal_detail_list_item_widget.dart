import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/bean/terminal_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/click_back_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/menu_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 终端详情-列表项
///
/// @author: zengxiangxi
/// @createTime: 2/4/21 4:21 PM
/// @specialDemand:
class TerminalDetailListItemWidget extends StatelessWidget {

  final TerminalDetailListItemBean itemBean;

  final ValueChanged<TerminalDetailListItemBean> onTap;

  final ValueChanged<TerminalDetailListItemBean> onDeleteTap;

  final ValueChanged<TerminalDetailListItemBean> onDeleteForever;

  final ValueChanged<TerminalDetailListItemBean> onUpdateName;

  const TerminalDetailListItemWidget({Key key, this.itemBean, this.onTap, this.onDeleteTap, this.onDeleteForever, this.onUpdateName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClickBackgroundWidget(
      onTap: (){
        onTap?.call(itemBean);
      },
      onLongPress: (detail){
        return MenuDialogUtils.showMenuDialog(context: context,longPressDetails: detail,itemMap: _getMenuMap(context));
      },
      child: Container(
        height: 60,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: _toMainRowWidget(context),
      ),
    );
  }

  Map<String,ValueChanged> _getMenuMap(BuildContext context){
    return {
      "重命名":(_){
        onUpdateName?.call(itemBean);
      },
      "取消播放":(_){
        onDeleteTap?.call(itemBean);
      },
      "删除文件":(_){
        onDeleteForever?.call(itemBean);
      }
    };
  }

  Widget _toMainRowWidget(BuildContext context) {
    return Row(
      children: <Widget>[
        _toPicWidget(),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
            height: 36,
            child: _toColumnWidget(context),
          ),
        )
      ],
    );
  }

  Widget _toColumnWidget(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            CommonConstraintMaxWidthWidget(
              maxWidth: ScreenUtils.screenW(context) / 2,
              child: Text(
                itemBean?.picname ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                  fontSize: 15,
                  height: 1.2
                ),
              ),
            ),
            Spacer(),
            Opacity(
              opacity: (itemBean?.isVideo() ?? false) ? 1:0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 3),
                child: Icon(
                  Icons.access_time,
                  color: VgColors.INPUT_BG_COLOR,
                  size: 14,
                ),
              ),
            ),
             Opacity(
               opacity: (itemBean?.isVideo() ?? false) ? 1:0,

               child: Text(
                        VgDateTimeUtils.getSecondsToMinuteStr(int.tryParse(itemBean?.videotime)) ?? "00:00",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                             fontSize: 12,
                           height: 1.2
                             ),
                       ),
             )
          ],
        ),
        DefaultTextStyle.merge(
          style: TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 12, height: 1.2),
          child: Builder(
            builder:(BuildContext context){
              final String folderSize = _getFolderSize();
              final String folderStorage = itemBean?.picstorage;
              return Row(
                children: <Widget>[
                  Text(
                    "${folderSize ?? ""}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Offstage(
                    offstage:StringUtils.isEmpty(folderSize) || StringUtils.isEmpty(folderStorage),
                    child: Container(
                      width: 0.5,
                      height: 10,
                      margin: const EdgeInsets.symmetric(horizontal: 6),
                      color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                    ),
                  ),
                  Text(
                    VgToolUtils.showMemoryStr(int.tryParse(folderStorage)) ?? "0KB",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Spacer(),
                  Visibility(
                    visible:false,
                    child: Text(
                      "播放频率：",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Visibility(
                    visible:false,
                    child: Text(
                      "${itemBean?.multiples ?? 1}倍",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF()
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        )
      ],
    );
  }

  Widget _toPicWidget() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        width: 36,
        height: 36,
        color: Colors.black,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            VgCacheNetWorkImage(itemBean?.getCoverUrl() ?? "",imageQualityType: ImageQualityType.middleDown,),
            Offstage(
              offstage: !(itemBean?.isVideo() ?? false),
              child: Image.asset(
                "images/video_play_ico.png",
                width: 12,
              ),
            )
          ],
        ),
      ),
    );
  }

  String _getFolderSize(){
    if(StringUtils.isEmpty(itemBean?.picsize)){
      return null;
    }
    return itemBean?.picsize?.replaceAll("*", " × ");
  }
}

import 'dart:async';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

class OperationSuccessTipsDialog extends StatefulWidget {

  final String title;
  const OperationSuccessTipsDialog({Key key, this.title})
      : super(key: key);

  @override
  _OperationSuccessTipsDialogState createState() => _OperationSuccessTipsDialogState();


  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context, {String title}) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: OperationSuccessTipsDialog(title: title,));
  }

}


class _OperationSuccessTipsDialogState extends BaseState<OperationSuccessTipsDialog>{
  Timer _timer;
  int count = 3;
  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      count--;
      if(count == 0){
        _timer.cancel();
        RouterUtils.pop(context);
      }
      setState(() { });
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Material(
            type: MaterialType.transparency,
            child: smsSendOutSuccessTips()));
  }


  Widget smsSendOutSuccessTips() {
    return Container(
      width: 290,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Text(
            widget?.title??"操作成功",
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 30,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              RouterUtils.pop(context);
            },
            child: Container(
              width: 250,
              height: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              ),
              child: Text(
                count>0?"我知道了(${count}s)":"我知道了",
                style: TextStyle(color: Colors.white, fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: 21,
          ),
        ],
      ),
    );
  }

}

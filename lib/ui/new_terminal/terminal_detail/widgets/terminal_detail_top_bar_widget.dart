import 'package:e_reception_flutter/common_widgets/common_top_bar_menu_dialog/bean/common_top_bar_menu_list_item_bean.dart';
import 'package:e_reception_flutter/common_widgets/common_top_bar_menu_dialog/common_top_bar_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/bean/terminal_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_modify_order/terminal_detail_modify_order_page.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';

/// 终端详情-topbar
///
/// @author: zengxiangxi
/// @createTime: 2/4/21 2:12 PM
/// @specialDemand:
class TerminalDetailTopBarWidget extends StatefulWidget {

  final ValueNotifier<HsnMapBean> hsnInfoValueNotifier;

  final String hsn;

  final VoidCallback onRefresh;

  const TerminalDetailTopBarWidget({Key key, this.hsnInfoValueNotifier, this.hsn, this.onRefresh}) : super(key: key);

  @override
  _TerminalDetailTopBarWidgetState createState() =>
      _TerminalDetailTopBarWidgetState();
}

class _TerminalDetailTopBarWidgetState
    extends State<TerminalDetailTopBarWidget> {

  List<CommonTopBarMenuListItemBean> dialogMenuList;
  CommonTopBarMenuListItemBean dialogMenuValue;

  @override
  void initState() {
    super.initState();
    dialogMenuList = [
      CommonTopBarMenuListItemBean(
          text: "刷脸优先",
          onTap: setMenu,
          value: 1
      ),
      CommonTopBarMenuListItemBean(
          text: "播放优先",
          onTap: setMenu,
          value: 2
      ),
      CommonTopBarMenuListItemBean(
          text: "人脸注册",
          onTap: setMenu,
          value: 3
      ),
      CommonTopBarMenuListItemBean(
          text: "仅播放",
          onTap: setMenu,
          value: 4
      ),
    ];
    dialogMenuValue = dialogMenuList.elementAt(0);

  }

  void setMenu(CommonTopBarMenuListItemBean itemBean){
    dialogMenuValue = itemBean;
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return VgTopBarWidget(
      isShowBack: true,
      centerWidget: _toCenterWidget(context),
      rightWidget: _toDeleteWidget(),
    );
  }

  Widget _toDeleteWidget() {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
//      onTap: () async{
//        bool result = await TerminalDetailModifyOrderPage.navigatorPush(context,widget.hsn);
//        if(result ?? false){
//          widget?.onRefresh?.call();
//        }
//      },
      child: Text(
        "属性",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 13,
        ),
      ),
    );
  }

  Widget _toCenterWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        CommonTopBarMenuDialog.navigatorPushDialog(context, dialogMenuList, dialogMenuValue?.value);
      },
      child: Container(
        constraints: BoxConstraints(maxWidth: ScreenUtils.screenW(context)),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ValueListenableBuilder(

                valueListenable: widget?.hsnInfoValueNotifier,
                builder: (BuildContext context, HsnMapBean hsnInfoBean, Widget child) {
                  return Text(
                    hsnInfoBean?.terminalName ?? "-",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color:
                        ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                        fontSize: 17,
                        height: 1.2,
                        fontWeight: FontWeight.w600),
                  );
                },
              ),
              _toSwitchWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _toSwitchWidget() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          dialogMenuValue?.text ?? "-",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
              fontSize: 10,
              height: 1.2
          ),
        ),
        Transform.translate(
          offset: Offset(-3, 0),
          child: Icon(
            Icons.arrow_drop_down,
            size: 16,
            color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
          ),
        )
      ],
    );
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_modify_order/terminal_detail_modify_order_page.dart';
import 'package:flutter/widgets.dart';

/// 企业详情-播放顺序bar
///
/// @author: zengxiangxi
/// @createTime: 2/4/21 3:59 PM
/// @specialDemand:
class TerminalDetailSettingBarWidget extends StatelessWidget {


  final int total;

  final String hsn;

  final VoidCallback onRefresh;

  const TerminalDetailSettingBarWidget({Key key, this.total, this.hsn, this.onRefresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 62,
      child: DefaultTextStyle.merge(
          style: TextStyle(
            height: 1.2
          ),
          child: _toMainRowWidget(context)),
    );
  }

  Widget _toMainRowWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async{
        bool result = await TerminalDetailModifyOrderPage.navigatorPush(context, hsn, "");
        if(result ?? false){
          onRefresh?.call();
        }
      },
      child: Row(
        children: <Widget>[
          SizedBox(width: 15,),
          Expanded(
            child: _toColumnWidget(),
          ),
          Text(
            "设置",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              fontSize: 12,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8,right: 15),
            child: Image.asset("images/go_ico.png",width: 6,color: VgColors.INPUT_BG_COLOR,),
          )
        ],
      ),
    );
  }

  Widget _toColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "播放序列设置",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
            fontSize: 14,
            fontWeight: FontWeight.w600,
          ),
        ),
        Text(
          "共${total ?? 0}个文件，播放一轮需要10分0秒",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
            fontSize: 11,
          ),
        )
      ],
    );
  }
}

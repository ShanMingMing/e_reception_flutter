import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 终端详情-添加按钮
///
/// @author: zengxiangxi
/// @createTime: 2/5/21 10:47 AM
/// @specialDemand:
class TerminalDetailAddButtonWidget extends StatelessWidget {

  final VoidCallback onTap;

  const TerminalDetailAddButtonWidget({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        onTap?.call();
      },
      child: Container(
        alignment: Alignment.center,
        width: 148,
        height: 40,
        margin: const EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          color: Color(0xFFFF7A7A),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset("images/add_icon.png",width: 14,color: Colors.white,),
               SizedBox(width: 4,),
               Text(
                         "添加播放文件",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             color: Colors.white,
                             fontSize: 15,
                           height: 1.2
                             ),
                       )
            ],
          ),
        ),
      ),
    );
  }
}

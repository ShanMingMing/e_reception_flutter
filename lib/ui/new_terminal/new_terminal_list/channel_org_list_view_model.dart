
import 'dart:convert';

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/arch/base_state.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class ChannelOrgListViewModel extends BasePagerViewModel<CompanyAddressListInfoBean, CompanyAddressBean>{
  ValueNotifier<int> totalValueNotifier;
  final String gps;

  ChannelOrgListViewModel(BaseState<StatefulWidget> state, {this.gps}) : super(state){
    totalValueNotifier = ValueNotifier(0);
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    super.onDisposed();
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "gps":gps??"",
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.POST;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appComBranchList";
  }
  @override
  bool isNeedCache() {
    return true;
  }

  @override
  String getCacheKey() {
    return getUrl() + (UserRepository.getInstance().companyId??"");
  }

  @override
  CompanyAddressBean parseData(VgHttpResponse resp) {
    CompanyAddressBean vo = CompanyAddressBean.fromMap(resp?.data);
    loading(false);
    totalValueNotifier?.value = vo?.data?.page?.total ?? 0;
    return vo;
  }

}
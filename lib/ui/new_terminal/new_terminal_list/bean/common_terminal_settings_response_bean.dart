import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"volume":20,"enableFaceRecognition":"","autoOnoffTime":"10:00-22:00","licenseActCnt":0}
/// extra : null

class CommonTerminalSettingsResponseBean {
  bool success;
  String code;
  String msg;
  CommonTerminalSettingsBean data;
  dynamic extra;

  static CommonTerminalSettingsResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CommonTerminalSettingsResponseBean commonTerminalSettingsResponseBeanBean = CommonTerminalSettingsResponseBean();
    commonTerminalSettingsResponseBeanBean.success = map['success'];
    commonTerminalSettingsResponseBeanBean.code = map['code'];
    commonTerminalSettingsResponseBeanBean.msg = map['msg'];
    commonTerminalSettingsResponseBeanBean.data = CommonTerminalSettingsBean.fromMap(map['data']);
    commonTerminalSettingsResponseBeanBean.extra = map['extra'];
    return commonTerminalSettingsResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// volume : 20
/// enableFaceRecognition : ""
/// autoOnoffTime : "10:00-22:00"
/// licenseActCnt : 0

class CommonTerminalSettingsBean {
  static const START_END_SPLIT = "-";

  static const TIME_SPLIT = ":";
  int volume;
  String enableFaceRecognition;
  String autoOnoffTime;
  String autoOnoff;//00未设置 01已设置
  String appedition;//00基础版、01海报版、02人脸版、03智慧版
  int licenseActCnt;
  String autoOnoffWeek;
  ComAutoSwitchBean comAutoSwitch;

  bool isShowFaceRecognition(){
    if(StringUtils.isEmpty(appedition)){
      return false;
    }
    if(("00" == appedition) || ("01" == appedition)){
      return false;
    }
    return true;
  }

  DateTime getStartOrEndTime(int index){
    if(StringUtils.isEmpty(autoOnoffTime) || index == null ||index <0 || index > 1){
      return null;
    }
    List<String> timeList = autoOnoffTime.split("$START_END_SPLIT");
    if(timeList == null || timeList.length != 2){
      return null;
    }
    String time = timeList.elementAt(index);
    if(StringUtils.isEmpty(time)){
      return null;
    }
    List<String> hourMinList = time.split("$TIME_SPLIT");
    if(hourMinList == null || hourMinList.length != 2){
      return null;
    }
    final DateTime nowDateTime = DateTime.now();

    return DateTime(nowDateTime.year, nowDateTime.month, nowDateTime.day,int.tryParse(hourMinList.elementAt(0)),int.tryParse(hourMinList.elementAt(1)));
  }

  static CommonTerminalSettingsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CommonTerminalSettingsBean dataBean = CommonTerminalSettingsBean();
    dataBean.volume = map['volume'];
    dataBean.enableFaceRecognition = map['enableFaceRecognition'];
    dataBean.autoOnoffTime = map['autoOnoffTime'];
    dataBean.autoOnoffWeek = map['autoOnoffWeek'];
    dataBean.licenseActCnt = map['licenseActCnt'];
    dataBean.appedition = map['appedition'];
    dataBean.autoOnoff = map['autoOnoff'];
    dataBean.comAutoSwitch = ComAutoSwitchBean.fromMap(map['comAutoSwitch']);
    return dataBean;
  }

  Map toJson() => {
    "volume": volume,
    "enableFaceRecognition": enableFaceRecognition,
    "autoOnoffTime": autoOnoffTime,
    "autoOnoffWeek": autoOnoffWeek,
    "licenseActCnt": licenseActCnt,
    "appedition": appedition,
    "autoOnoff": autoOnoff,
    "comAutoSwitch": comAutoSwitch,
  };
}

class ComAutoSwitchBean {
  String companyid;
  String type;
  String autoOnoff;
  String autoMon;
  String monTime;
  String autoTues;
  String tuesTime;
  String autoWed;
  String wedTime;
  String autoThur;
  String thurTime;
  String autoFri;
  String friTime;
  String autoSat;
  String satTime;
  String autoSun;
  String sunTime;
  String restflg;//休息日不自动开机 00开启 01关闭

  static ComAutoSwitchBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComAutoSwitchBean comAutoSwitchBean = ComAutoSwitchBean();
    comAutoSwitchBean.companyid = map['companyid'];
    comAutoSwitchBean.type = map['type'];
    comAutoSwitchBean.autoOnoff = map['autoOnoff'];
    comAutoSwitchBean.autoMon = map['autoMon'];
    comAutoSwitchBean.monTime = map['monTime'];
    comAutoSwitchBean.autoTues = map['autoTues'];
    comAutoSwitchBean.tuesTime = map['tuesTime'];
    comAutoSwitchBean.autoWed = map['autoWed'];
    comAutoSwitchBean.wedTime = map['wedTime'];
    comAutoSwitchBean.autoThur = map['autoThur'];
    comAutoSwitchBean.thurTime = map['thurTime'];
    comAutoSwitchBean.autoFri = map['autoFri'];
    comAutoSwitchBean.friTime = map['friTime'];
    comAutoSwitchBean.autoSat = map['autoSat'];
    comAutoSwitchBean.satTime = map['satTime'];
    comAutoSwitchBean.autoSun = map['autoSun'];
    comAutoSwitchBean.sunTime = map['sunTime'];
    comAutoSwitchBean.restflg = map['restflg'];
    return comAutoSwitchBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "type": type,
    "autoOnoff": autoOnoff,
    "autoMon": autoMon,
    "monTime": monTime,
    "autoTues": autoTues,
    "tuesTime": tuesTime,
    "autoWed": autoWed,
    "wedTime": wedTime,
    "autoThur": autoThur,
    "thurTime": thurTime,
    "autoFri": autoFri,
    "friTime": friTime,
    "autoSat": autoSat,
    "satTime": satTime,
    "autoSun": autoSun,
    "sunTime": sunTime,
    "restflg": restflg,
  };
}
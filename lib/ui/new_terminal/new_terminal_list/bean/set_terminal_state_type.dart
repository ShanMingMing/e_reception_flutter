///排序方式
enum SetTerminalStateType{
  //正常开机，
  open,
  //息屏开机，
  screenOff,
  //远程关机
  off,
  autoOnOff,

}

String parseStateType(SetTerminalStateType type) {
  switch (type.index) {
    case 0:
      return '开启设备';
    case 1:
      return '关闭设备';
    case 2:
      return '远程关机';
    case 3:
      return '定时开关机';
    default:
      return '开启设备';
  }
}

String parseStateTypeParams(SetTerminalStateType type) {
  //排序类型00正常开机 01息屏开机 02远程关机
  switch (type.index) {
    case 0:
      return '01';
    case 1:
      return '00';
    case 2:
      return '02';
    case 3:
      return '03';
    default:
      return '01';
  }
}

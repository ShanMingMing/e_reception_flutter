import 'new_terminal_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"pichsnList":["e1bb6956d91c7a6c","5OUB0K130W"],"terminalList":[{"terminalName":"立式机","terminalPicurl":"","hsn":"155AD31F093C1CD733B7E775C3E0140B9A","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路110号靠近江宁开发区总部基地","bindflg":"01","position":"小走道","manager":"陈喆","loginName":""},{"terminalName":"小米手机","terminalPicurl":"","hsn":"e1bb6956d91c7a6c","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路108号靠近润葳酒店","bindflg":"01","manager":"陈喆","loginName":""},{"terminalName":"哈哈哈哇","terminalPicurl":"","hsn":"95a53f9dcbc8eead","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","manager":"陈喆","loginName":""},{"terminalName":"大柜子","terminalPicurl":"","hsn":"5OUB0K130W","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"南京总部基地","bindflg":"01","manager":"陈喆","loginName":""}]}
/// extra : null

class TerminalByPicResponseBean {
  bool success;
  String code;
  String msg;
  TerminalByPicDataBean data;
  dynamic extra;

  static TerminalByPicResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalByPicResponseBean terminalByPicResponseBeanBean = TerminalByPicResponseBean();
    terminalByPicResponseBeanBean.success = map['success'];
    terminalByPicResponseBeanBean.code = map['code'];
    terminalByPicResponseBeanBean.msg = map['msg'];
    terminalByPicResponseBeanBean.data = TerminalByPicDataBean.fromMap(map['data']);
    terminalByPicResponseBeanBean.extra = map['extra'];
    return terminalByPicResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// pichsnList : ["e1bb6956d91c7a6c","5OUB0K130W"]
/// terminalList : [{"terminalName":"立式机","terminalPicurl":"","hsn":"155AD31F093C1CD733B7E775C3E0140B9A","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路110号靠近江宁开发区总部基地","bindflg":"01","position":"小走道","manager":"陈喆","loginName":""},{"terminalName":"小米手机","terminalPicurl":"","hsn":"e1bb6956d91c7a6c","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路108号靠近润葳酒店","bindflg":"01","manager":"陈喆","loginName":""},{"terminalName":"哈哈哈哇","terminalPicurl":"","hsn":"95a53f9dcbc8eead","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","manager":"陈喆","loginName":""},{"terminalName":"大柜子","terminalPicurl":"","hsn":"5OUB0K130W","companyid":"ad7ce5517c0e400fa048f9a37326d69b","address":"南京总部基地","bindflg":"01","manager":"陈喆","loginName":""}]

class TerminalByPicDataBean {
  List<NewTerminalListItemBean> usehsnList;//正在使用和即将使用这个海报的终端
  List<NewTerminalListItemBean> terminalList;//是用户管理的全部终端

  static TerminalByPicDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalByPicDataBean dataBean = TerminalByPicDataBean();
    dataBean.usehsnList = List()..addAll(
        (map['usehsnList'] as List ?? []).map((o) => NewTerminalListItemBean.fromMap(o))
    );
    dataBean.terminalList = List()..addAll(
      (map['terminalList'] as List ?? []).map((o) => NewTerminalListItemBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "usehsnList": usehsnList,
    "terminalList": terminalList,
  };
}

import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import 'common_terminal_settings_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"terminalName":"嘻嘻BeiJing北京的终端2343545233333333","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class NewTerminalResponseBean extends BasePagerBean<NewTerminalListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static NewTerminalResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    NewTerminalResponseBean newTerminalResponseBeanBean = NewTerminalResponseBean();
    newTerminalResponseBeanBean.success = map['success'];
    newTerminalResponseBeanBean.code = map['code'];
    newTerminalResponseBeanBean.msg = map['msg'];
    newTerminalResponseBeanBean.data = DataBean.fromMap(map['data']);
    newTerminalResponseBeanBean.extra = map['extra'];
    return newTerminalResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current ?? 1;

  ///得到List列表
  @override
  List<NewTerminalListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages ?? 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"terminalName":"嘻嘻BeiJing北京的终端2343545233333333","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}],"total":5,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"terminalName":"嘻嘻BeiJing北京的终端2343545233333333","hsn":"e57477b75c986715","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京的终端233333333","hsn":"a69ab51225adaf8b","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端12321呀呀呀哈哈","hsn":"4bcc02ed94667d35","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"江苏省南京市江宁区天元中路辅路114号靠近江宁开发区总部基地","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"南京的终端呀呀呀哈哈","hsn":"ed5d18608a872ce8","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","power":"","manager":"李岁红","loginName":"王凡语"},{"terminalName":"北京搜宝的终端","hsn":"lisuihongtest0113","companyid":"55a438b79b7c4cd3bf7fec1f603e774a","address":"北京搜宝","bindflg":"01","manager":"李岁红","loginName":""}]
/// total : 5
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<NewTerminalListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
        (map['records'] as List ?? []).map((o) => NewTerminalListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// terminalName : "嘻嘻BeiJing北京的终端2343545233333333"
/// hsn : "e57477b75c986715"
/// companyid : "55a438b79b7c4cd3bf7fec1f603e774a"
/// address : "北京搜宝"
/// bindflg : "01"
/// manager : "李岁红"
/// loginName : "王凡语"

class NewTerminalListItemBean {
  String terminalName;
  String terminalPicurl;
  String hsn;
  int id;
  String companyid;
  String address;
  String bindflg;//00未绑定 01已绑定
  String manager;
  String loginName;
  String position;
  bool selectStatus;
  int onOff;//0关机 1开机
  String screenStatus;//00开 01息屏 02关机
  String logout;
  String endday;//
  String reportHsnLastTime;
  String autoOnoffTime;
  String autoOnoffWeek;
  String sideNumber;
  String autoOnoff;//00未设置 01已设置
  ComAutoSwitchBean comAutoSwitch;
  String appedition;

  bool getAutoOnOffStatus(){
    if(StringUtils.isNotEmpty(autoOnoffTime)){
      return true;
    }
    return false;
  }

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }

  String getEndDayString(){
    if(StringUtils.isEmpty(endday)){
      return "-";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "";
    }
    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      return endday.replaceAll("-", "/") + "到期";
      // if((((endDayDateTime.millisecondsSinceEpoch - currentDay.millisecondsSinceEpoch)~/(1000*3600*24))) >= 100){
      //   return endday.replaceAll("-", "/") + "到期";
      // }
      // return endday.replaceAll("-", "/") + "到期";
    }
  }

  String getNotifyString(){
    if(StringUtils.isEmpty(endday)){
      return "";
    }
    DateTime endDayDateTime = DateTime.parse(endday);
    if(DateUtil.isToday(endDayDateTime.millisecondsSinceEpoch)){
      return "已到期";
    }

    DateTime currentDay = DateTime.parse(DateUtil.getDateStrByDateTime(DateTime.now(), format: DateFormat.YEAR_MONTH_DAY));
    //已到期xx天
    if(currentDay.millisecondsSinceEpoch > endDayDateTime.millisecondsSinceEpoch){
      return "已到期" +  (((currentDay.millisecondsSinceEpoch - endDayDateTime.millisecondsSinceEpoch)~/(1000*3600*24))).toString() + "天";
    }
    //未到期
    if(currentDay.millisecondsSinceEpoch < endDayDateTime.millisecondsSinceEpoch){
      int interval = (((endDayDateTime.millisecondsSinceEpoch - currentDay.millisecondsSinceEpoch)~/(1000*3600*24)));
      if(interval >= 100){
        return "";
      }
      return "（${interval}天后）";
    }
  }

  //是否展示有效期，这个有效期指海报有效期
  bool isShowEndDay(){
    if(StringUtils.isEmpty(appedition)){
      return false;
    }
    if(("01" == appedition) || ("03" == appedition)){
      return true;
    }
    return false;
  }

  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }

  SetTerminalStateType getTerminalState(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return SetTerminalStateType.off;
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return SetTerminalStateType.screenOff;
      }else{
        return SetTerminalStateType.open;
      }
    }
  }

  String getTerminalStateString(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      if(StringUtils.isNotEmpty(reportHsnLastTime??"")){
        String currentTimeStr = DateUtil.getNowDateStr();
        int currentTime = DateTime.parse(currentTimeStr).millisecondsSinceEpoch;
        int time = DateTime.parse(reportHsnLastTime).millisecondsSinceEpoch;
        int interval = (currentTime - time)~/(24*60*60*1000);
        if(interval > 2){
          return "已关机${interval}天";
        }else{
          return "已关机";
        }
      }
      return "已关机";
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return "已关机";
        // return "息屏开机";
      }else{
        return "正常开机";
      }
    }
  }

  static NewTerminalListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    NewTerminalListItemBean recordsBean = NewTerminalListItemBean();
    recordsBean.terminalName = map['terminalName'];
    recordsBean.terminalPicurl = map['terminalPicurl'];
    recordsBean.hsn = map['hsn'];
    recordsBean.id = map['id'];
    recordsBean.companyid = map['companyid'];
    recordsBean.address = map['address'];
    recordsBean.bindflg = map['bindflg'];
    recordsBean.manager = map['manager'];
    recordsBean.loginName = map['loginName'];
    recordsBean.position = map['position'];
    recordsBean.selectStatus = false;
    recordsBean.onOff = map['onOff'];
    recordsBean.screenStatus = map['screenStatus'];
    recordsBean.endday = map['endday'];
    recordsBean.reportHsnLastTime = map['reportHsnLastTime'];
    recordsBean.autoOnoffTime = map['autoOnoffTime'];
    recordsBean.autoOnoffWeek = map['autoOnoffWeek'];
    recordsBean.logout = map['logout'];
    recordsBean.autoOnoff = map['autoOnoff'];
    recordsBean.sideNumber = map['sideNumber'];
    recordsBean.comAutoSwitch = ComAutoSwitchBean.fromMap(map);
    return recordsBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "id": id,
    "hsn": hsn,
    "companyid": companyid,
    "address": address,
    "bindflg": bindflg,
    "manager": manager,
    "loginName": loginName,
    "position": position,
    "selectStatus": selectStatus,
    "onOff": onOff,
    "screenStatus": screenStatus,
    "endday": endday,
    "reportHsnLastTime": reportHsnLastTime,
    "autoOnoffTime": autoOnoffTime,
    "autoOnoffWeek": autoOnoffWeek,
    "logout": logout,
    "autoOnoff": autoOnoff,
    "sideNumber": sideNumber,
    "comAutoSwitch": comAutoSwitch,
  };

  ///获取编辑保存时的类型
  ///If(isBind) return "00";else return "01";
  String getEditsBindType(){
    if(bindflg == "01"){
      return "00";
    }else if(bindflg == "00"){
      return "01";
    }
    return null;
  }
}


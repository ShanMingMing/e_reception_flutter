/// success : true
/// code : 200
/// msg : "处理成功"
/// data : {"have":false}
/// extra : null

class ScanResponseBean {
  bool success;
  String code;
  String msg;
  ScanResultBean data;
  dynamic extra;

  static ScanResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ScanResponseBean scanResponseBeanBean = ScanResponseBean();
    scanResponseBeanBean.success = map['success'];
    scanResponseBeanBean.code = map['code'];
    scanResponseBeanBean.msg = map['msg'];
    scanResponseBeanBean.data = ScanResultBean.fromMap(map['data']);
    scanResponseBeanBean.extra = map['extra'];
    return scanResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// have : false

class ScanResultBean {
  //终端是否已绑定 00扫码登录  01扫码登录并绑定 02扫码绑定
  String have;
  int rcaid;
  String terName;
  String position;
  String key;
  String caid;
  String gps;

  static ScanResultBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ScanResultBean dataBean = ScanResultBean();
    dataBean.have = map['have'];
    dataBean.rcaid = map['rcaid'];
    dataBean.terName = map['terName'];
    dataBean.position = map['position'];
    dataBean.key = map['key'];
    dataBean.caid = map['caid'];
    dataBean.gps = map['gps'];
    return dataBean;
  }

  Map toJson() => {
    "have": have,
    "rcaid": rcaid,
    "terName": terName,
    "position": position,
    "key": key,
    "caid": caid,
    "gps": gps,
  };
}
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';

class NewTerminalListUtils {

  static String getTerminalListCacheKey() {
    return "app/appTerminalList" + (UserRepository.getInstance().authId??"")
          + (UserRepository.getInstance().companyId??"");
  }
}
import 'dart:async';
import 'package:amap_flutter_map/amap_flutter_map.dart';
import 'package:amap_flutter_base/amap_flutter_base.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

/// 查看终端位置页面
class ShowTerminalGpsPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "ShowTerminalGpsPage";
  final String gps;
  const ShowTerminalGpsPage({Key key, this.gps})
      : super(key: key);

  @override
  _ShowTerminalGpsPageState createState() => _ShowTerminalGpsPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(
      BuildContext context,  String gps) {
    return RouterUtils.routeForFutureResult(
      context,
      ShowTerminalGpsPage(
        gps:gps,
      ),
      routeName: ShowTerminalGpsPage.ROUTER,
    );
  }
}

class _ShowTerminalGpsPageState extends BaseState<ShowTerminalGpsPage> {

  LatLng _latLng;
  Set<Marker> _markerList;
  bool _hasInitMarker = false;
  void _initMarker(BuildContext context) async {
    if (_hasInitMarker) {
      return;
    }
    Marker marker = Marker(
        position: _latLng,
        icon: BitmapDescriptor.fromIconPath("images/icon_terminal_position.png"));
    setState(() {
      _hasInitMarker = true;
      _markerList.add(marker);
    });
  }

  @override
  void initState() {
    super.initState();
    _latLng = new LatLng(double.parse(widget?.gps?.split(",")[0]),
        double.parse(widget?.gps?.split(",")[1]));
    _markerList = new Set();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _initMarker(context);
    return LightAnnotatedRegion(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _toInfoWidget(),
      ),
    );
  }

  Widget _toInfoWidget(){
    return Stack(
      children:[
        Column(
          children: <Widget>[
            _toTopBarWidget(),
            _toMapWidget(),
          ],
        ),
      ],
    );
  }

  Widget _toTopBarWidget(){
    return  VgTopBarWidget(
      title: "摆放位置",
      isShowBack: true,
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      backgroundColor: Colors.white,
    );
  }

  Widget _toMapWidget(){
    return Expanded(
      child: Container(
        child: AMapWidget(
          initialCameraPosition: CameraPosition(
            target: _latLng,
            zoom: 17,
          ),
          markers: _markerList,
          scaleEnabled: false,
        ),
      ),
    );

  }


  onMapMoveEnd(){
    setState(() { });
  }
}

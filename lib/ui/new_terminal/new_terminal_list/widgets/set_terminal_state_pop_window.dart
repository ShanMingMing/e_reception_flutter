import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:flutter/cupertino.dart';

typedef OnClick=Function(SetTerminalStateType type);

class SetTerminalStatePopWindow extends StatelessWidget {
  final SetTerminalStateType sortType;
  final OnClick onClick;
  const SetTerminalStatePopWindow({Key key, this.sortType,this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 22, horizontal: 14),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
              color: Color(0xff303546)),
          child: Column(
            children: [
              _itemWidget(context,SetTerminalStateType.open),
              _itemWidget(context,SetTerminalStateType.off),
              _itemWidget(context,SetTerminalStateType.autoOnOff)],
          ),
          width: 120,
        ),
      ],
    );
  }

  _itemWidget(BuildContext context, SetTerminalStateType type) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: (){
          onClick(type);
          Navigator.pop(context);
        },
        child: Container(
          height: 45,
          child: Row(
            children: [
              Text(
                parseStateType(type),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: type == sortType
                      ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                      : Color(0xFFD0E0F7),
                  fontSize: 14,
                ),
              ),
              SizedBox(width: 20,),
              Visibility(
                visible: type == sortType,
                child: Image(
                  image: AssetImage("images/icon_swoosh_select.png"),
                  width: 11,
                ),
              ),
            ],
          ),
        ));
  }
}

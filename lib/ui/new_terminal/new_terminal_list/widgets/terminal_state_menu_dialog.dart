import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/vg_menu_widget.dart' as vgMenu;


class TerminalStateMenuDialogUtils {

  static Future<dynamic> showMenuDialog({BuildContext context,
    TapUpDetails tabUpDetails,Map<SetTerminalStateType,ValueChanged<SetTerminalStateType>> itemMap, SetTerminalStateType sortType}) {
    assert(itemMap != null, "items is not empty");

    return vgMenu.showMenu(
      elevation: 1,
      color: Color(0xFF303546),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12)
      ),
      useRootNavigator: true,
        captureInheritedThemes: false,
        context: context,
        position: RelativeRect.fromLTRB(
          tabUpDetails.globalPosition.dx,
          tabUpDetails.globalPosition.dy,
          tabUpDetails.globalPosition.dx,
          tabUpDetails.globalPosition.dy,
        ),

      items: itemMap.keys.map((SetTerminalStateType key){
        return vgMenu.PopupMenuItem<SetTerminalStateType>(
          value: key,
          onTap: itemMap[key],
          child: Container(
            color: Color(0xFF303546),
            padding: EdgeInsets.symmetric(horizontal: 14),
            height: 45,
            child: Row(
              children: [
                Text(
                  parseStateType(key),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: key == sortType
                        ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                        : Color(0xFFD0E0F7),
                    fontSize: 14,
                  ),
                ),
                SizedBox(width: 20,),
                Visibility(
                  visible: key == sortType,
                  child: Image(
                    image: AssetImage("images/icon_swoosh_select.png"),
                    width: 11,
                  ),
                ),
              ],
            ),
          ),
        );

      }).toList()
    );
  }


}
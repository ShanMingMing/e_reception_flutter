import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/binding_add_admin_list/widgets/binding_add_admin_list_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_modify_order/terminal_detail_modify_order_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'binding_add_admin_list_bean.dart';
import 'widgets/binding_add_admin_list_widget.dart';

/// 终端添加管理员页面
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 4:30 PM
/// @specialDemand:
class BindingAddAdminListPage extends StatefulWidget {

 ///路由名称
  static const String ROUTER = "BindingAddAdminListPage";
  final String hsn;

  const BindingAddAdminListPage({Key key, this.hsn}) : super(key: key);
  @override
  _BindingAddAdminListPageState createState() => _BindingAddAdminListPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,String hsn){
    return RouterUtils.routeForFutureResult(context,
        BindingAddAdminListPage(hsn:hsn,),
        routeName: BindingAddAdminListPage.ROUTER,);
  }
}

class _BindingAddAdminListPageState extends BaseState<BindingAddAdminListPage> {
  TerminalDetailModifyOrderViewModel _viewModel;
  bool isUp = false;
  List<SelectAddRecordsBean> _list;
  ValueNotifier<String> searchPageValueNotifier;

  @override
  void initState() {
    super.initState();
    _viewModel = TerminalDetailModifyOrderViewModel(this);
    searchPageValueNotifier = ValueNotifier(null);
  }
  @override
  void dispose() {
    searchPageValueNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget(){
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        CommonSearchBarWidget(
          hintText: "搜索",
          autoFocus: true,
          onChanged: (String searchStr){
            searchPageValueNotifier.value = searchStr?.trim();
          },
          onSubmitted: (String searchStr){
            searchPageValueNotifier.value = searchStr?.trim();
          }),
        // _toAddBarWidget(),
        Expanded(
          child: BindingAddAdminListWidget(listClickCallback:(list){
            _list = list;
            // todo 判断颜色
            if(list?.length==0){
              isUp = false;
            }else{
              isUp = true;
            }
            setState(() {
            });
          },hsn:widget?.hsn,searchPageValueNotifier: searchPageValueNotifier,),
        )
      ],
    );
  }

  ///topbar
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "请选择",
      isShowBack: true,
      rightWidget: CommonFixedHeightConfirmButtonWidget(
        isAlive: isUp,
        width: 48,
        height: 24,
        unSelectBgColor: Color(0xff3a3f50),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 12,
            fontWeight: FontWeight.w600
        ),
        selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 12,
            fontWeight: FontWeight.w600
        ),
        text: "确定",
        onTap: (){
             if(isUp){
              String userIdArr = VgStringUtils.getSplitStr(_list.map((e) => e.userid).toList(),symbol: ",");
              _viewModel?.saveTerminalAddAdmin(context, widget?.hsn, userIdArr);
             }
        },
      ),
    );
  }

  Widget _toAddBarWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        BindingAddAdminListDialog.navigatorPushDialog(context);
      },
      child: Container(),
      // child: Container(
      //   height: 50,
      //   alignment: Alignment.centerLeft,
      //   margin: const EdgeInsets.symmetric(horizontal: 15),
      //   child: Row(
      //     mainAxisSize: MainAxisSize.min,
      //     children: <Widget>[
      //       Icon(
      //         Icons.add,
      //         size: 15,
      //         color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      //       ),
      //       Text(
      //         "添加管理员",
      //         maxLines: 1,
      //         overflow: TextOverflow.ellipsis,
      //         style: TextStyle(
      //             color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      //             fontSize: 14,
      //             height: 1.2
      //         ),
      //       )
      //     ],
      //   ),
      // ),
    );
  }
}

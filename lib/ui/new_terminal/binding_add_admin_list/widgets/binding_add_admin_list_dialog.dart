import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

/// 绑定添加管理员-添加弹窗
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 5:09 PM
/// @specialDemand:
class BindingAddAdminListDialog extends StatefulWidget {
  @override
  _BindingAddAdminListDialogState createState() => _BindingAddAdminListDialogState();

  ///跳转方法
  static Future<dynamic> navigatorPushDialog(BuildContext context) {
    VgToolUtils.removeAllFocus(context);
    return VgDialogUtils.showCommonBottomDialog(
      context: context,
      child: BindingAddAdminListDialog(),
    );
  }
}

class _BindingAddAdminListDialogState extends State<BindingAddAdminListDialog> {
  TextEditingController _nameEditingController;

  TextEditingController _phoneEditingController;

  @override
  void initState() {
    super.initState();
    _nameEditingController = TextEditingController();
    _phoneEditingController = TextEditingController();
  }

  @override
  void dispose() {
    _nameEditingController?.dispose();
    _phoneEditingController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            borderRadius: BorderRadius.vertical(
                top: Radius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS))),
        child: _toMainColumnWidget(),
      ),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _toTopTitleAndConfirmWidget(),
        _BindingAddAdminDialogEditWidget(
          text: "姓名",
          hintText: "请输入",
          editingController: _nameEditingController,
        ),
        SizedBox(
          height: 12,
        ),
        _BindingAddAdminDialogEditWidget(
          text: "手机",
          hintText: "请输入",
          editingController: _phoneEditingController,
          isPhone: true,
        ),
        SizedBox(
          height:12,
        ),
         Container(
           margin: const EdgeInsets.symmetric(horizontal: 15),
                   alignment: Alignment.centerLeft,
                   child:  Text(
                     "注：添加后，对方将收到短信通知～",
                     maxLines: 1,
                     overflow: TextOverflow.ellipsis,
                     style: TextStyle(
                       color: VgColors.INPUT_BG_COLOR,
                         fontSize: 12,
                         ),
                   ),
                 ),
        SizedBox(
          height: 30 + ScreenUtils.getBottomBarH(context),
        )
      ],
    );
  }

  Widget _toTopTitleAndConfirmWidget() {
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 15,
          ),
          Text(
            "新建管理员",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 16,
                height: 1.2),
          ),
          Spacer(),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
            child: CommonFixedHeightConfirmButtonWidget(
              isAlive: false,
              width: 76,
              height: 30,
              unSelectBgColor:
              ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              unSelectTextStyle: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 14,
              ),
              selectedTextStyle: TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
              text: "确定",
              onTap: (){
                RouterUtils.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}

/// 新增管理员弹窗编辑组件
///
/// @author: zengxiangxi
/// @createTime: 1/7/21 5:18 PM
/// @specialDemand:
class _BindingAddAdminDialogEditWidget extends StatelessWidget {
  final String text;

  final String hintText;

  final Widget rightWidget;

  final TextEditingController editingController;


  final bool isPhone;

  const _BindingAddAdminDialogEditWidget(
      {Key key,
      this.hintText,
      this.rightWidget,
      this.text,
      this.editingController, this.isPhone = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          borderRadius: BorderRadius.circular(4)),
      child: Row(
        children: <Widget>[
          Opacity(
            opacity: 1,
            child: Container(
              width: 15,
              alignment: Alignment(2 / 5, 0),
              padding: const EdgeInsets.only(top: 7),
              child: Text(
                "*",
                style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getMinorRedColor_F95355(),
                    fontSize: 14,
                    height: 1.2),
              ),
            ),
          ),
          Container(
            width: 43,
            alignment: Alignment.centerLeft,
            child: Text(
              text ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance()
                    .getTextMinorGreyColor_808388(),
                fontSize: 14,
              ),
            ),
          ),
          Expanded(
            child: VgTextField(
              maxLines: 1,
              controller: editingController,
              keyboardType: isPhone ? TextInputType.number : null,
              inputFormatters: isPhone ? [
                WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
              ] : null,
              decoration: InputDecoration(
                border: InputBorder.none,
                counterText: "",
                contentPadding: const EdgeInsets.only(bottom: 2),
                hintText: hintText ?? "",
                hintStyle: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getTextEditHintColor_3A3F50(),
                    fontSize: 14),
              ),
              style: TextStyle(
                  fontSize: 14,
                  color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
            ),
          ),
          rightWidget ?? SizedBox(width: 15)
        ],
      ),
    );
  }
}

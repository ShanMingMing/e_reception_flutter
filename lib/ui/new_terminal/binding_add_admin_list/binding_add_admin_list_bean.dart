import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:e_reception_flutter/media_library/media_library_index/bean/media_library_index_response_bean.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"adminList":[{"nick":"Lily","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","lasttime":1618293956,"hsncnt":1,"roleid":"99","loginphone":"13200000000","userid":"4895ada8d22749159caebcb838387513"},{"nick":"丽颖","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","hsncnt":1,"napicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","loginphone":"13255556666","userid":"e5783831e6ac481caca500f69f5dff35"}]}
/// extra : null

class BindingAddAdminListBean extends BasePagerBean<SelectAddRecordsBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static BindingAddAdminListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindingAddAdminListBean bindingSelectAddAdminBean = BindingAddAdminListBean();
    bindingSelectAddAdminBean.success = map['success'];
    bindingSelectAddAdminBean.code = map['code'];
    bindingSelectAddAdminBean.msg = map['msg'];
    bindingSelectAddAdminBean.data = DataBean.fromMap(map['data']);
    bindingSelectAddAdminBean.extra = map['extra'];
    return bindingSelectAddAdminBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<SelectAddRecordsBean> getDataList() => data?.adminList;

  ///最后页码
  @override
  int getMaxPage() =>1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    // data?.page?.records = list.cast();
  }
}

/// adminList : [{"nick":"Lily","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","lasttime":1618293956,"hsncnt":1,"roleid":"99","loginphone":"13200000000","userid":"4895ada8d22749159caebcb838387513"},{"nick":"丽颖","terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","hsncnt":1,"napicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","loginphone":"13255556666","userid":"e5783831e6ac481caca500f69f5dff35"}]

class DataBean {
  List<SelectAddRecordsBean> adminList;
  PageBean page;
  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.adminList = List()..addAll(
        (map['adminList'] as List ?? []).map((o) => SelectAddRecordsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "adminList": adminList,
  };
}

/// nick : "Lily"
/// terminalName : "小米pad"
/// hsn : "11140C1F12FEEB2C52DFBE67ED3E634AA0"
/// lasttime : 1618293956
/// hsncnt : 1
/// roleid : "99"
/// loginphone : "13200000000"
/// userid : "4895ada8d22749159caebcb838387513"

class SelectAddRecordsBean {
  String nick;
  String terminalName;
  String hsn;
  int lasttime;
  int hsncnt;
  String roleid;
  String loginphone;
  String userid;
  CommonSingleChoiceStatus statusType;
  String napicurl;

  static SelectAddRecordsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SelectAddRecordsBean adminListBean = SelectAddRecordsBean();
    adminListBean.nick = map['nick'];
    adminListBean.terminalName = map['terminalName'];
    adminListBean.hsn = map['hsn'];
    adminListBean.lasttime = map['lasttime'];
    adminListBean.hsncnt = map['hsncnt'];
    adminListBean.roleid = map['roleid'];
    adminListBean.loginphone = map['loginphone'];
    adminListBean.userid = map['userid'];
    adminListBean.statusType = map['statusType'];
    adminListBean.napicurl = map['napicurl'];
    return adminListBean;
  }

  Map toJson() => {
    "nick": nick,
    "terminalName": terminalName,
    "hsn": hsn,
    "lasttime": lasttime,
    "hsncnt": hsncnt,
    "roleid": roleid,
    "loginphone": loginphone,
    "userid": userid,
    "statusType": statusType,
    "napicurl": napicurl,
  };
}

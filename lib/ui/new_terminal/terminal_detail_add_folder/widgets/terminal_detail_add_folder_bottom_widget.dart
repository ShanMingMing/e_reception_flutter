import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_add_folder/service/upload_media_library_service.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matisse_android_plugin/export_matisse.dart';

/// 终端添加文件-底部按钮
///
/// @author: zengxiangxi
/// @createTime: 2/5/21 12:19 PM
/// @specialDemand:
class TerminalDetailAddFolderBottomWidget extends StatelessWidget {

  final VoidCallback onUploadTap;

  final VoidCallback onConfrimTap;

  final bool isAliveConfirm;

  const TerminalDetailAddFolderBottomWidget({Key key, this.onUploadTap, this.isAliveConfirm, this.onConfrimTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double bottomH = ScreenUtils.getBottomBarH(context);
    return Container(
      height: 60 + bottomH,
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                offset: Offset(0, -4),
                blurRadius: 10)
          ]),
      padding: EdgeInsets.only(bottom: bottomH),
      child: Center(child: _toMainWidget(context)),
    );
  }


  /// 函数节流
  ///
  /// [func]: 要执行的方法
  Function throttle(
      Future Function() func,
      ) {
    if (func == null) {
      return func;
    }
    bool enable = true;
    Function target = () {
      if (enable == true) {
        enable = false;
        func().then((_) {
          enable = true;
        });
      }
    };
    return target;
  }

  Widget _toMainWidget(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 15,
        ),
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap:
            throttle(()  async {
                onUploadTap?.call();
                await Future.delayed(Duration(milliseconds: 2000));
              }),
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                      color: ThemeRepository.getInstance()
                          .getMinorYellowColor_FFB714()
                          .withOpacity(0.5),
                      width: 0.5)),
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4, bottom: 2),
                      child: Image.asset(
                        "images/add_icon.png",
                        width: 14,
                        color: ThemeRepository.getInstance()
                            .getMinorYellowColor_FFB714(),
                      ),
                    ),
                    Text(
                      "上传手机本地文件",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getMinorYellowColor_FFB714(),
                        fontSize: 15,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        CommonFixedHeightConfirmButtonWidget(
          isAlive: isAliveConfirm,
          height: 40,
          width: 90,
          margin: const EdgeInsets.symmetric(horizontal: 0),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          text: "确定",
          onTap: (){
            onConfrimTap?.call();
          },
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }


}

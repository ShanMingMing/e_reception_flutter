import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_add_folder/bean/terminal_detail_add_folder_response_bean.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 终端详情添加表格
///
/// @author: zengxiangxi
/// @createTime: 2/5/21 12:15 PM
/// @specialDemand:
class TerminalDetailAddFolderGridItemWidget extends StatelessWidget {
  final TerminalDetailAddFolderListItemBean itemBean;

  final ValueChanged<TerminalDetailAddFolderListItemBean> onTap;

  const TerminalDetailAddFolderGridItemWidget(
      {Key key, this.itemBean, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          if(itemBean == null){
            return null;
          }
          VgPhotoPreview.single(context, itemBean?.picurl,loadingCoverUrl: itemBean?.getCoverUrl(),loadingImageQualityType: ImageQualityType.middleDown);
        },
        // onLongPress: (){
        //   if(itemBean == null){
        //     return null;
        //   }
        //   VgPhotoPreview.single(context, itemBean?.picurl,loadingCoverUrl: itemBean?.getCoverUrl(),loadingImageQualityType: ImageQualityType.middleDown);
        // },
        child: _toGridItemWidget());
  }

  Widget _toGridItemWidget() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: AspectRatio(
        aspectRatio: 1,
        child: Container(
          color: Colors.black,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              VgCacheNetWorkImage(
                itemBean?.getCoverUrl() ?? "",
                imageQualityType: ImageQualityType.middleDown,
              ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   right: 0,
              //   child: Container(
              //     alignment: Alignment.centerLeft,
              //     padding: const EdgeInsets.only(
              //         left: 8, right: 8, bottom: 6, top: 10),
              //     decoration: BoxDecoration(
              //         gradient: LinearGradient(
              //             begin: Alignment.topCenter,
              //             end: Alignment.bottomCenter,
              //             colors: [Color(0x00000000), Color(0x80000000)])),
              //     child: Text(
              //       itemBean?.picname ?? "",
              //       maxLines: 1,
              //       overflow: TextOverflow.ellipsis,
              //       style: TextStyle(
              //         color: Colors.white,
              //         fontSize: 10,
              //       ),
              //     ),
              //   ),
              // ),
              Offstage(
                offstage: !(itemBean?.isVideo() ?? false),
                child: Opacity(
                  opacity: 0.5,
                  child: Image.asset(
                    "images/video_play_ico.png",
                    width: 40,
                  ),
                ),
              ),
              AnimatedOpacity(
                duration: DEFAULT_ANIM_DURATION,
                opacity:
                    itemBean?.selectedNum == null || itemBean.selectedNum <= 0
                        ? 0
                        : 0.3,
                child: Container(
                  color: Colors.black,
                ),
              ),
              Positioned(
                top: 0,
                right: 0,
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: (){
                    onTap?.call(itemBean);
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    padding: EdgeInsets.only(top: 8, right: 8),
                    alignment: Alignment.topRight,
                    child: _toSelectedNumberWidget(itemBean?.selectedNum),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _toSelectedNumberWidget(int number) {
    return Container(
      width: 20,
      height: 20,
      decoration: BoxDecoration(
          color: Colors.transparent,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white, width: 1)),
      child: AnimatedSwitcher(
        duration: DEFAULT_ANIM_DURATION,
        child: number == null || number <= 0
            ? SizedBox()
            : Container(
                width: 18,
                height: 18,
                decoration: BoxDecoration(
                  color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Text(
                    "${number ?? 0}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.white, fontSize: 13, height: 1.2),
                  ),
                ),
              ),
      ),
    );
  }
}

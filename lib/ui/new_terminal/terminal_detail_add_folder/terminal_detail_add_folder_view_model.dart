import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail_add_folder/bean/terminal_detail_add_folder_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_widget_lib.dart';

class TerminalDetailAddFolderViewModel extends BasePagerViewModel<
    TerminalDetailAddFolderListItemBean, TerminalDetailAddFolderResponseBean> {
  ///App终端从资源库添加视频图片
  static const String SAVE_MEDIA_FOR_HSN_API =
     ServerApi.BASE_URL + "app/appSaveCompicByhsn";

  static const String GET_NO_COM_PIC_LIST =
      ServerApi.BASE_URL + "app/appGetNoComAttPicListByhsn";

  final String hsn;

  final String Function() getSearchStrFunc;

  TerminalDetailAddFolderViewModel(
      BaseState<StatefulWidget> state, this.hsn, {this.getSearchStrFunc})
      : super(state);

  ///添加选择项
  void saveListHttp(BuildContext context, List<String> selectedList) {
    if (selectedList == null || selectedList.isEmpty) {
      VgToastUtils.toast(context, "请选择添加项");

      return;
    }
    String picids = selectedList.join(",");
    if (StringUtils.isEmpty(picids)) {
      VgToastUtils.toast(context, "选择格式化异常");
      return;
    }
    VgHudUtils.show(context, "正在保存");
    VgHttpUtils.get(SAVE_MEDIA_FOR_HSN_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "hsn": hsn ?? "",
          "picids": picids ?? ""
        },
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          String tmpFirstPic = selectedList?.first;
          RouterUtils.pop(context,result: tmpFirstPic);
          VgToastUtils.toast(AppMain.context, "添加成功");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }

  void getList(BuildContext context, String hsn, Function(int size) onGetList){
    loading(true);
    VgHttpUtils.get(GET_NO_COM_PIC_LIST,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current": 1,
          "size": 20,
          "hsn": hsn,
        },
        callback: BaseCallback(onSuccess: (val) {
          loading(false);
          TerminalDetailAddFolderResponseBean vo =
          TerminalDetailAddFolderResponseBean.fromMap(val);
          if(vo != null && vo.getDataList()!= null){
            onGetList.call(vo.getDataList().length);
          }else{
            onGetList.call(0);
          }
        }, onError: (msg) {
          loading(false);
          onGetList.call(1);
          VgToastUtils.toast(context, msg);
        }));
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "hsn": hsn,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return GET_NO_COM_PIC_LIST;
  }

  @override
  TerminalDetailAddFolderResponseBean parseData(VgHttpResponse resp) {
    TerminalDetailAddFolderResponseBean vo =
        TerminalDetailAddFolderResponseBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

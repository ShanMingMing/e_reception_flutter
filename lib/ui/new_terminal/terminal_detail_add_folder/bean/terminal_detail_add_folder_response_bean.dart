import 'package:e_reception_flutter/common_widgets/common_single_choice_button_widget/common_single_choice_button_widget.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"comPictureList":[{"createtime":1612578367,"picsize":"474*683","playnum":0,"picstorage":"4000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114245_3647.jpg","bjtime":"2021-02-06 10:26:07","picname":"20210109114245_3647.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"37c68e7e483e45d39ea4ff7ab7710c72"},{"createtime":1612578781,"picsize":"830*830","playnum":0,"picstorage":"15000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"http://etpic.we17.com/test/20210206103125_4799.jpg","bjtime":"2021-02-06 10:33:01","picname":"zhengfangxingtu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"3c1a43bc0fe94866be7e050e0f6bf7e1"},{"createtime":1612582368,"picsize":"1279*722","playnum":0,"picstorage":"40960","videotime":49,"playtime":0,"type":"02","videopic":"https://pic.008box.com/test/20210206113049_1415.jpg","createname":"王凡语","picurl":"https://pic.008box.com/test/20210206113050_4980.mp4","bjtime":"2021-02-06 11:32:48","picname":"video_heng.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"6318147b6de4473893ff11fc88d67014"},{"createtime":1612578270,"picsize":"474*683","playnum":0,"picstorage":"4555","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114745_5526.jpg","bjtime":"2021-02-06 10:24:30","picname":"20210109114745_5526.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"811412ee6d484898b62efcf9922fbdfe"},{"createtime":1612578922,"picsize":"826*466","playnum":0,"picstorage":"15000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/test/20210206103419_1684.jpg","bjtime":"2021-02-06 10:35:22","picname":"hengtu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"c0f36fda3705400a994a44b31ea5ed19"},{"createtime":1612578284,"picsize":"474*683","playnum":0,"picstorage":"7333","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114719_7269.jpg","bjtime":"2021-02-06 10:24:44","picname":"20210109114719_7269.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"cb3a4f63baa04ab6b8a8d1ac172a0ac8"},{"createtime":1612578296,"picsize":"474*683","playnum":0,"picstorage":"4500","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114420_0385.jpg","bjtime":"2021-02-06 10:24:56","picname":"20210109114420_0385.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"dedf31c5f56b403d87c2efd737adb8ea"},{"createtime":1612582032,"picsize":"474*683","playnum":0,"picstorage":"409600","videotime":136,"playtime":0,"type":"02","videopic":"https://pic.008box.com/picture/20210109114511_2038.jpg","createname":"王凡语","picurl":"http://pic.008box.com/video/20200706133551_6825.mp4","bjtime":"2021-02-06 11:27:12","picname":"video_shu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"e618b821bb79447dab5dd03386bcd424"}]}
/// extra : null

class TerminalDetailAddFolderResponseBean extends BasePagerBean<TerminalDetailAddFolderListItemBean> {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static TerminalDetailAddFolderResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalDetailAddFolderResponseBean terminalDetailAddFolderResponseBeanBean = TerminalDetailAddFolderResponseBean();
    terminalDetailAddFolderResponseBeanBean.success = map['success'];
    terminalDetailAddFolderResponseBeanBean.code = map['code'];
    terminalDetailAddFolderResponseBeanBean.msg = map['msg'];
    terminalDetailAddFolderResponseBeanBean.data = DataBean.fromMap(map['data']);
    terminalDetailAddFolderResponseBeanBean.extra = map['extra'];
    return terminalDetailAddFolderResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<TerminalDetailAddFolderListItemBean> getDataList() => data?.comPictureList;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.comPictureList = list.cast();
  }
}

/// comPictureList : [{"createtime":1612578367,"picsize":"474*683","playnum":0,"picstorage":"4000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114245_3647.jpg","bjtime":"2021-02-06 10:26:07","picname":"20210109114245_3647.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"37c68e7e483e45d39ea4ff7ab7710c72"},{"createtime":1612578781,"picsize":"830*830","playnum":0,"picstorage":"15000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"http://etpic.we17.com/test/20210206103125_4799.jpg","bjtime":"2021-02-06 10:33:01","picname":"zhengfangxingtu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"3c1a43bc0fe94866be7e050e0f6bf7e1"},{"createtime":1612582368,"picsize":"1279*722","playnum":0,"picstorage":"40960","videotime":49,"playtime":0,"type":"02","videopic":"https://pic.008box.com/test/20210206113049_1415.jpg","createname":"王凡语","picurl":"https://pic.008box.com/test/20210206113050_4980.mp4","bjtime":"2021-02-06 11:32:48","picname":"video_heng.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"6318147b6de4473893ff11fc88d67014"},{"createtime":1612578270,"picsize":"474*683","playnum":0,"picstorage":"4555","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114745_5526.jpg","bjtime":"2021-02-06 10:24:30","picname":"20210109114745_5526.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"811412ee6d484898b62efcf9922fbdfe"},{"createtime":1612578922,"picsize":"826*466","playnum":0,"picstorage":"15000","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/test/20210206103419_1684.jpg","bjtime":"2021-02-06 10:35:22","picname":"hengtu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"c0f36fda3705400a994a44b31ea5ed19"},{"createtime":1612578284,"picsize":"474*683","playnum":0,"picstorage":"7333","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114719_7269.jpg","bjtime":"2021-02-06 10:24:44","picname":"20210109114719_7269.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"cb3a4f63baa04ab6b8a8d1ac172a0ac8"},{"createtime":1612578296,"picsize":"474*683","playnum":0,"picstorage":"4500","videotime":0,"playtime":0,"type":"01","videopic":" ","createname":"王凡语","picurl":"https://pic.008box.com/picture/20210109114420_0385.jpg","bjtime":"2021-02-06 10:24:56","picname":"20210109114420_0385.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"dedf31c5f56b403d87c2efd737adb8ea"},{"createtime":1612582032,"picsize":"474*683","playnum":0,"picstorage":"409600","videotime":136,"playtime":0,"type":"02","videopic":"https://pic.008box.com/picture/20210109114511_2038.jpg","createname":"王凡语","picurl":"http://pic.008box.com/video/20200706133551_6825.mp4","bjtime":"2021-02-06 11:27:12","picname":"video_shu.jpg","createuserid":"be97b4557acc49a782c2e52b53bb77a1","id":"e618b821bb79447dab5dd03386bcd424"}]

class DataBean {
  List<TerminalDetailAddFolderListItemBean> comPictureList;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.comPictureList = List()..addAll(
      (map['comPictureList'] as List ?? []).map((o) => TerminalDetailAddFolderListItemBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "comPictureList": comPictureList,
  };
}

/// createtime : 1612578367
/// picsize : "474*683"
/// playnum : 0
/// picstorage : "4000"
/// videotime : 0
/// playtime : 0
/// type : "01"
/// videopic : " "
/// createname : "王凡语"
/// picurl : "https://pic.008box.com/picture/20210109114245_3647.jpg"
/// bjtime : "2021-02-06 10:26:07"
/// picname : "20210109114245_3647.jpg"
/// createuserid : "be97b4557acc49a782c2e52b53bb77a1"
/// id : "37c68e7e483e45d39ea4ff7ab7710c72"

class TerminalDetailAddFolderListItemBean {
  int createtime;
  String picsize;
  int playnum;
  String picstorage;
  int videotime;
  int playtime;
  String type;
  String videopic;
  String createname;
  String picurl;
  String bjtime;
  String picname;
  String createuserid;
  String id;
  //字幕
  String subtitle;
  //字幕标题
  String title;
  //水印
  String waterflg;//00没有水印 01有水印
  //机构logo
  String logoflg;//00有 01无logo

  ///选中的序号
  int selectedNum;

  static TerminalDetailAddFolderListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    TerminalDetailAddFolderListItemBean comPictureListBean = TerminalDetailAddFolderListItemBean();
    comPictureListBean.createtime = map['createtime'];
    comPictureListBean.picsize = map['picsize'];
    comPictureListBean.playnum = map['playnum'];
    comPictureListBean.picstorage = map['picstorage'];
    comPictureListBean.videotime = map['videotime'];
    comPictureListBean.playtime = map['playtime'];
    comPictureListBean.type = map['type'];
    comPictureListBean.videopic = map['videopic'];
    comPictureListBean.createname = map['createname'];
    comPictureListBean.picurl = map['picurl'];
    comPictureListBean.bjtime = map['bjtime'];
    comPictureListBean.picname = map['picname'];
    comPictureListBean.createuserid = map['createuserid'];
    comPictureListBean.id = map['id'];
    comPictureListBean.subtitle = map['subtitle'];
    comPictureListBean.title = map['title'];
    comPictureListBean.waterflg = map['waterflg'];
    comPictureListBean.logoflg = map['logoflg'];
    return comPictureListBean;
  }

  Map toJson() => {
    "createtime": createtime,
    "picsize": picsize,
    "playnum": playnum,
    "picstorage": picstorage,
    "videotime": videotime,
    "playtime": playtime,
    "type": type,
    "videopic": videopic,
    "createname": createname,
    "picurl": picurl,
    "bjtime": bjtime,
    "picname": picname,
    "createuserid": createuserid,
    "id": id,
    "subtitle": subtitle,
    "title": title,
    "waterflg": waterflg,
    "logoflg": logoflg,
  };


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TerminalDetailAddFolderListItemBean &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;

  ///01 图片 02 视频
  bool isVideo(){
    return type == "02";
  }

  ///是否有终端使用
  bool isUserFolder(){
    // return playnum != null && playnum > 0;
    return false;
  }

  ///根据类型获取封面
  String getCoverUrl() {
    if(isVideo()){
      return videopic;
    }
    return picurl;
  }

  @override
  String toString() {
    return 'TerminalDetailAddFolderListItemBean{picname: $picname, id: $id, selectedNum: $selectedNum}\n';
  }
}
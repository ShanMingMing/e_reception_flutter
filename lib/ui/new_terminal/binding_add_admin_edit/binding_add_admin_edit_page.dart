import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_detail_add_admin_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 绑定添加管理员页面
///
/// @author: zengxiangxi
/// @createTime: 1/18/21 3:44 PM
/// @specialDemand:
class BindingAddAdminEditPage extends StatefulWidget {
  static const String ROUTER = "BindingAddAdminEditPage";

  @override
  _BindingAddAdminEditPageState createState() => _BindingAddAdminEditPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context) {
    return RouterUtils.routeForFutureResult(
      context,
      BindingAddAdminEditPage(),
      routeName: BindingAddAdminEditPage.ROUTER,
    );
  }
}

class _BindingAddAdminEditPageState extends State<BindingAddAdminEditPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
    );
  }

  ///主列
  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        CompanyDetailAddAdminDialog()
      ],
    );
  }

  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "添加管理员",
      isShowBack:  true,
    );
  }
}

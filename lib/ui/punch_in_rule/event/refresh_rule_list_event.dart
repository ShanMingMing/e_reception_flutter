class RefreshRuleListEvent{
 String msg;

 RefreshRuleListEvent(this.msg);

  @override
  String toString() {
    return "${msg??''}";
  }
}
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/punch_in_rule/bean/punch_in_rule_bean.dart';
import 'package:e_reception_flutter/ui/punch_in_rule/bean/punch_in_rule_list_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/src/http/vg_http_response.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'event/refresh_rule_list_event.dart';

class PunchInRuleListViewModel extends BasePagerViewModel<PunchInRuleBean,PunchInRuleListResponse> {
  final String type;

  ValueNotifier<RuleCntBean> cntNotifier;


  PunchInRuleListViewModel(BaseState<StatefulWidget> state,this.type) : super(state){
    cntNotifier=ValueNotifier(null);
    VgEventBus.global.on().listen((event) {
      if(event is RefreshRuleListEvent){
        print("考勤规则列表收到刷新事件：$event");
        if(!isStateDisposed){
          refresh();
        }
      }
    });
  }



  @override
  String getBodyData(int page) {
     return null;
  }


  @override
  bool isNeedCache() {
    return true;
  }


  @override
  String getCacheKey() {
    return NetApi.RULE_LIST+UserRepository.getInstance().getCacheKeySuffix()+type;
  }

  @override
  Map<String,String> getQuery(int page) {
    return {
      'authId': UserRepository.getInstance().authId,
      'groupType': type,
  };
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return NetApi.RULE_LIST;
  }

  @override
  PunchInRuleListResponse parseData(VgHttpResponse resp) {
    PunchInRuleListResponse response=PunchInRuleListResponse.fromMap(resp.data);
    if(!isStateDisposed){
      cntNotifier.value=response?.data?.ruleCnt;
    }

    return response;
  }

  @override
  void onDisposed() {
    cntNotifier?.dispose();
    super.onDisposed();
  }
}

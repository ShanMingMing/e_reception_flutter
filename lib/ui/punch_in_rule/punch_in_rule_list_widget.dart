import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/punch_in_rule/bean/punch_in_rule_bean.dart';
import 'package:e_reception_flutter/ui/punch_in_rule/punch_in_rule_list_viewmodel.dart';
import 'package:e_reception_flutter/ui/punch_in_rule/set_rule/set_punch_in_rule_page.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'bean/punch_in_rule_list_response.dart';
typedef CntCallback=Function(int cnt);
class PunchInRuleListWidget extends StatefulWidget {
  ///员工
  static const String TYPE_STAFF = "00";

  ///学员
  static const String TYPE_STU = "01";

  final String type;

  final CntCallback cntCallback;


  const PunchInRuleListWidget({Key key, @required this.type,this.cntCallback}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PunchInRuleListState();
  }
}

class PunchInRuleListState
    extends BasePagerState<PunchInRuleBean, PunchInRuleListWidget>
    with SingleTickerProviderStateMixin {
  PunchInRuleListViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = PunchInRuleListViewModel(this, widget.type);
    _viewModel?.refresh();
    _viewModel.cntNotifier.addListener(() {
      RuleCntBean cntBean=_viewModel.cntNotifier.value;
      if(cntBean!=null){
        widget.cntCallback((widget.type=='01')?cntBean.stdCnt:cntBean.staffCnt);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      child: VgPlaceHolderStatusWidget(
        loadingStatus: data==null,
        emptyStatus:data==null||( data!=null&&data.length==0),
        child: VgPullRefreshWidget.bind(
            state: this,
            enablePullDown: true,
            viewModel: _viewModel,
            child: ListView.separated(
              itemBuilder: (context, position) {
                return _itemBuilder(position);
              },
              physics: BouncingScrollPhysics(),
              itemCount: data?.length ?? 0,
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              separatorBuilder: (context, index) {
                return SizedBox(
                  height: 10,
                );
              },
            )),
      ),
    );
  }

  _itemBuilder(int position) {
    PunchInRuleBean punchInRuleBean=data[position];
    if(punchInRuleBean==null){
      return SizedBox();
    }
    bool isGeneral =punchInRuleBean.isGeneral() ;

    return GestureDetector(
      onTap: () {

        SetPunchInRulePage.navigatorPush(
            context, widget.type, isGeneral, punchInRuleBean);
      },
      child: Container(
        color: ThemeRepository.getInstance().getCardBgColor_21263C(),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            //条目
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Text(
                    isGeneral ? "通用规则" : (StringUtils.isEmpty(punchInRuleBean.name)?"自定义规则":punchInRuleBean.name),
                    style: TextStyle(
                        fontSize: 15,
                        color: isGeneral
                            ? ThemeRepository.getInstance()
                                .getPrimaryColor_1890FF()
                            : Color(0xffd0e0f7)),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  if(!punchInRuleBean.isGeneral())
                    Column(
                      children: <Widget>[
                        _subWidget("适用对象",punchInRuleBean.suitableObjectName),
                        SizedBox(
                          height: 6,
                        ),
                      ],
                    ),

                  _subWidget("考勤日期", "国家规定工作日（周一二三四五）"),
                  // if (!isGeneral)
                    SizedBox(
                      height: 6,
                    ),


                  _subWidget("上班时间", (punchInRuleBean.onDutyTime??'')+getFlexDuration(punchInRuleBean.onDutyFlexibleTime)),
                  SizedBox(
                    height: 6,
                  ),
                  _subWidget("下班时间", (punchInRuleBean.offDutyTime??'')+getFlexDuration(punchInRuleBean.offDutyFlexibleTime)),
                  SizedBox(
                    height: 6,
                  ),
                  _subWidget("时长要求", PunchInRuleBean.formatDurationTimeString(punchInRuleBean.duration)),
                ],
              ),
            ),
            //箭头
            Image.asset(
              "images/index_arrow_ico.png",
              width: 6,
              color: Color(0xff5e687c),
            )
          ],
        ),
      ),
    );
  }

  _subWidget(String title, String content) {
    return DefaultTextStyle(
      style: TextStyle(fontSize: 12,height: 1.1),
      child: Container(
        child: Row(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(color: Color(0xff808388)),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: StringUtils.isEmpty(content)
                  ? Text(
                      "未设置",
                      style: TextStyle(color: Color(0xff5e687c)),
                    )
                  : Text(
                      content,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Color(0xffd0e0f7)),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  String getFlexDuration(int flexibleTime) {
    if(flexibleTime==null){
      return '';
    }
    return '（弹性${flexibleTime~/60}分钟）';
  }
}

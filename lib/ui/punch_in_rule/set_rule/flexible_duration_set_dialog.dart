import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

const List<String> timeData = [
  "5分钟",
  "10分钟",
  "15分钟",
  "20分钟",
  "30分钟",
  "45分钟",
  "60分钟",
  "90分钟",
  "120分钟",
  "150分钟",
  "180分钟"
];

///
/// 设置弹性时长确认弹窗
class FlexibleDurationSetDialog extends StatefulWidget {
  final String title;

  final int initValue;

  const FlexibleDurationSetDialog({Key key, this.title, this.initValue})
      : super(key: key);

  static Future<int> navigatorPushDialog(BuildContext context,
      {String title, int initValue}) {
    return VgDialogUtils.showCommonDialog<int>(
        context: context,
        child: FlexibleDurationSetDialog(title: title, initValue: initValue),
        barrierDismissible: false);
  }

  @override
  State<StatefulWidget> createState() {
    return FlexibleDurationSetDialogState();
  }
}

class FlexibleDurationSetDialogState extends State<FlexibleDurationSetDialog> {
  String durationStr;

  @override
  void initState() {
    durationStr = widget.initValue==null?null:widget.initValue.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => RouterUtils.pop(context),
      child: UnconstrainedBox(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {},
          child: Material(
            type: MaterialType.transparency,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Container(
                width: 290,
                height: 244,
                decoration: BoxDecoration(
                    color:
                    ThemeRepository.getInstance().getCardBgColor_21263C()),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        widget.title,
                        style: TextStyle(
                            fontSize: 16,
                            color: ThemeRepository.getInstance()
                                .getTextMainColor_D0E0F7()),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        widget.title=='下班弹性时长'?'在该时间范围内提前下班不视为异常':'在该时间范围内迟到不视为异常',
                        style: TextStyle(
                            fontSize: 12,
                            color: ThemeRepository.getInstance()
                                .getTextMainColor_D0E0F7()),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          SelectUtil.showListSelectDialog(
                              context: context,
                              title: widget.title,
                              positionStr: durationStr,
                              textList: timeData,
                              rightText: '分钟',
                              onSelect: (dynamic value) {
                                setState(() {
                                  durationStr = value;
                                });
                              });
                        },
                        child: Container(
                          height: 45,
                          padding:
                          EdgeInsets.symmetric(horizontal: 14, vertical: 12),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              color: ThemeRepository.getInstance()
                                  .getBgOrSplitColor_191E31()),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(durationStr?? '请选择', style: TextStyle(
                                    color: durationStr == null ? ThemeRepository
                                        .getInstance()
                                        .getDefaultGrayColor_FF5E687C() :ThemeRepository.getInstance().getTextMainColor_D0E0F7()),),
                              ),
                              Text('分钟', style: TextStyle(
                                fontSize: 14,
                                  color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),)
                            ],
                          ),
                        ),
                      ),
                      Spacer(),
                      CommonFixedHeightConfirmButtonWidget(
                        isAlive: durationStr!=null,
                        height: 45,
                        radius:BorderRadius.all(Radius.circular(4)),
                        unSelectBgColor: Color(0xff3a3f50),
                        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                        unSelectTextStyle: TextStyle(
                            color: VgColors.INPUT_BG_COLOR,
                            fontSize: 12,
                            fontWeight: FontWeight.w600
                        ),
                        selectedTextStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.w600
                        ),
                        text: "保存",
                        onTap: (){
                          int result;
                            if(durationStr!=null){
                              result=int.parse(durationStr);
                            }
                           RouterUtils.pop(context,result: result);
                        },
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }


  ///时间秒转字符串
  static String getFormatTimeZnString(int sub) {
    if (sub == null) {
      return null;
    }
    sub = sub * 1000;
    if (sub == null || sub < 0) {
      return null;
    }
    int minute = (sub / 1000 / 60).floor();
    if (minute < 1) {
      minute = 1;
    }
    return '$minute分钟';
  }
}

import 'package:e_reception_flutter/ui/punch_in_rule/bean/punch_in_rule_bean.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"attRuleList":[{"duration":0,"delflg":"00","arid":"c131a8fc010446d79c47cd6f1f6eaebb","groupType":"00","companyid":"248989548cc84252878e1cb1b248c36d","bjtime":"2021-04-30T13:48:07.000+0000","onFlextime":0,"ruleName":"通用规则","createdate":1619761686,"offFlextime":0,"type":"00","createuid":"4895ada8d22749159caebcb838387513"}],"ruleCnt":{"staffCnt":1,"stdCnt":0}}
/// extra : null

class PunchInRuleListResponse extends BasePagerBean<PunchInRuleBean> {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static PunchInRuleListResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PunchInRuleListResponse punchInRuleListResponseBean =
        PunchInRuleListResponse();
    punchInRuleListResponseBean.success = map['success'];
    punchInRuleListResponseBean.code = map['code'];
    punchInRuleListResponseBean.msg = map['msg'];
    punchInRuleListResponseBean.data = DataBean.fromMap(map['data']);
    punchInRuleListResponseBean.extra = map['extra'];
    return punchInRuleListResponseBean;
  }

  Map toJson() => {
        "success": success,
        "code": code,
        "msg": msg,
        "data": data,
        "extra": extra,
      };

  @override
  int getCurrentPage() {
    return 1;
  }

  @override
  List<PunchInRuleBean> getDataList() {
    return data.attRuleList;
  }

  @override
  int getMaxPage() {
    return 1;
  }



  @override
  bool isSucceed() {
    return success;
  }

  @override
  String getMessage() {
    return msg;
  }
}

/// attRuleList : [{"duration":0,"delflg":"00","arid":"c131a8fc010446d79c47cd6f1f6eaebb","groupType":"00","companyid":"248989548cc84252878e1cb1b248c36d","bjtime":"2021-04-30T13:48:07.000+0000","onFlextime":0,"ruleName":"通用规则","createdate":1619761686,"offFlextime":0,"type":"00","createuid":"4895ada8d22749159caebcb838387513"}]
/// ruleCnt : {"staffCnt":1,"stdCnt":0}

class DataBean {
  List<PunchInRuleBean> attRuleList;
  RuleCntBean ruleCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.attRuleList = List()
      ..addAll((map['attRuleList'] as List ?? [])
          .map((o) => PunchInRuleBean.fromMap(o)));
    dataBean.ruleCnt = RuleCntBean.fromMap(map['ruleCnt']);
    return dataBean;
  }

  Map toJson() => {
        "attRuleList": attRuleList,
        "ruleCnt": ruleCnt,
      };
}

/// staffCnt : 1
/// stdCnt : 0

class RuleCntBean {
  int staffCnt;
  int stdCnt;

  static RuleCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RuleCntBean ruleCntBean = RuleCntBean();
    ruleCntBean.staffCnt = map['staffCnt'];
    ruleCntBean.stdCnt = map['stdCnt'];
    return ruleCntBean;
  }

  Map toJson() => {
        "staffCnt": staffCnt,
        "stdCnt": stdCnt,
      };
}

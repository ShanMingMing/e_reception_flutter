/// success : false
/// code : "200"
/// msg : "处理成功"
/// data : [{"departmentid":"8c625557e64241b7b9edf6cb0e9feafd","department":"呀哈哈"}]
/// extra : null

class SetPunchRuleResponse {
  bool success;
  String code;
  String msg;
  List<RepeatBean> data;
  dynamic extra;

  static SetPunchRuleResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SetPunchRuleResponse setPunchRuleResponseBean = SetPunchRuleResponse();
    setPunchRuleResponseBean.success = map['success'];
    setPunchRuleResponseBean.code = map['code'];
    setPunchRuleResponseBean.msg = map['msg'];
    if(map['data'] is List){
      setPunchRuleResponseBean.data = List()..addAll(
          (map['data'] as List ?? []).map((o) => RepeatBean.fromMap(o))
      );
    }

    setPunchRuleResponseBean.extra = map['extra'];
    return setPunchRuleResponseBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// departmentid : "8c625557e64241b7b9edf6cb0e9feafd"
/// department : "呀哈哈"

class RepeatBean {
  String departmentid;
  String department;

  static RepeatBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RepeatBean dataBean = RepeatBean();
    dataBean.departmentid = map['departmentid'];
    dataBean.department = map['department'];
    return dataBean;
  }

  Map toJson() => {
    "departmentid": departmentid,
    "department": department,
  };
}
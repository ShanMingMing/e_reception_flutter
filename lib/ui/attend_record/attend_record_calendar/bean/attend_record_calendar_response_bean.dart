/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"overtimeCnt":0,"punchRecord":[{"date":"2021-03-01","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-02","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-03","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-04","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-05","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-06","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-07","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-08","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-09","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-10","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-11","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-12","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-13","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-14","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-15","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-16","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-17","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-18","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-19","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"}],"absenceCnt":15,"faceDayCnt":0,"unFaceDayCnt":15,"normalCnt":0,"punchRule":false,"faceUserInfo":{"groupType":"00","projectname":"特色项目","city":"北京","roleid":"10","groupid":"8f007208d7cb4ac78fbd0975d5852cb2","departmentid":"f8d96b99b8e14c7db4862973a4947378","napicurl":"http://etpic.we17.com/test/20210319173141_0671.jpg","userid":"d5647a27c47547f38051e3cba92f5f46","nick":"","number":"","groupName":"员工","phone":"18665289540","putpicurl":"http://etpic.we17.com/test/20210319173141_0671.jpg","name":"186assd","department":"he","projectid":"1f1d8807aced4d9fbf648d84c62e9016"},"abnormalCnt":0}
/// extra : null

class AttendRecordCalendarResponseBean {
  bool success;
  String code;
  String msg;
  AttendRecordCalendarDataBean data;
  dynamic extra;

  static AttendRecordCalendarResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AttendRecordCalendarResponseBean attendRecordCalendarResponseBeanBean = AttendRecordCalendarResponseBean();
    attendRecordCalendarResponseBeanBean.success = map['success'];
    attendRecordCalendarResponseBeanBean.code = map['code'];
    attendRecordCalendarResponseBeanBean.msg = map['msg'];
    attendRecordCalendarResponseBeanBean.data = AttendRecordCalendarDataBean.fromMap(map['data']);
    attendRecordCalendarResponseBeanBean.extra = map['extra'];
    return attendRecordCalendarResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// overtimeCnt : 0
/// punchRecord : [{"date":"2021-03-01","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-02","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-03","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-04","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-05","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-06","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-07","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-08","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-09","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-10","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-11","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-12","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-13","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-14","mintime":0,"pcnt":0,"maxtime":0,"rest":"00","overtime":"01"},{"date":"2021-03-15","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-16","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-17","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-18","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"},{"date":"2021-03-19","mintime":0,"pcnt":0,"maxtime":0,"rest":"01","absence":"00"}]
/// absenceCnt : 15
/// faceDayCnt : 0
/// unFaceDayCnt : 15
/// normalCnt : 0
/// punchRule : false
/// faceUserInfo : {"groupType":"00","projectname":"特色项目","city":"北京","roleid":"10","groupid":"8f007208d7cb4ac78fbd0975d5852cb2","departmentid":"f8d96b99b8e14c7db4862973a4947378","napicurl":"http://etpic.we17.com/test/20210319173141_0671.jpg","userid":"d5647a27c47547f38051e3cba92f5f46","nick":"","number":"","groupName":"员工","phone":"18665289540","putpicurl":"http://etpic.we17.com/test/20210319173141_0671.jpg","name":"186assd","department":"he","projectid":"1f1d8807aced4d9fbf648d84c62e9016"}
/// abnormalCnt : 0

class AttendRecordCalendarDataBean {
  int overtimeCnt;
  List<PunchRecordBean> punchRecord;
  int absenceCnt;
  int faceDayCnt;
  int unFaceDayCnt;
  int normalCnt;
  bool punchRule;
  FaceUserInfoBean faceUserInfo;
  int abnormalCnt;
  int restCnt;

  int leaveCnt;
  int outCnt;
  bool ruleFlg;

  static AttendRecordCalendarDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AttendRecordCalendarDataBean dataBean = AttendRecordCalendarDataBean();
    dataBean.overtimeCnt = map['overtimeCnt'];
    dataBean.punchRecord = List()..addAll(
      (map['punchRecord'] as List ?? []).map((o) => PunchRecordBean.fromMap(o))
    );
    dataBean.absenceCnt = map['absenceCnt'];
    dataBean.faceDayCnt = map['faceDayCnt'];
    dataBean.unFaceDayCnt = map['unFaceDayCnt'];
    dataBean.normalCnt = map['normalCnt'];
    dataBean.punchRule = map['punchRule'];
    dataBean.faceUserInfo = FaceUserInfoBean.fromMap(map['faceUserInfo']);
    dataBean.abnormalCnt = map['abnormalCnt'];
    dataBean.restCnt = map['restCnt'];
    dataBean.leaveCnt = map['leaveCnt'];
    dataBean.outCnt = map['outCnt'];
    dataBean.ruleFlg = map['ruleFlg'];
    return dataBean;
  }

  Map toJson() => {
    "overtimeCnt": overtimeCnt,
    "punchRecord": punchRecord,
    "absenceCnt": absenceCnt,
    "faceDayCnt": faceDayCnt,
    "unFaceDayCnt": unFaceDayCnt,
    "normalCnt": normalCnt,
    "punchRule": punchRule,
    "faceUserInfo": faceUserInfo,
    "abnormalCnt": abnormalCnt,
    "restCnt": restCnt,
    "leaveCnt": leaveCnt,
    "outCnt": outCnt,
    "ruleFlg": ruleFlg,
  };
}

/// groupType : "00"
/// projectname : "特色项目"
/// city : "北京"
/// roleid : "10"
/// groupid : "8f007208d7cb4ac78fbd0975d5852cb2"
/// departmentid : "f8d96b99b8e14c7db4862973a4947378"
/// napicurl : "http://etpic.we17.com/test/20210319173141_0671.jpg"
/// userid : "d5647a27c47547f38051e3cba92f5f46"
/// nick : ""
/// number : ""
/// groupName : "员工"
/// phone : "18665289540"
/// putpicurl : "http://etpic.we17.com/test/20210319173141_0671.jpg"
/// name : "186assd"
/// department : "he"
/// projectid : "1f1d8807aced4d9fbf648d84c62e9016"

class FaceUserInfoBean {
  String groupType;
  String projectname;
  String city;
  String roleid;
  String groupid;
  String departmentid;
  String napicurl;
  String userid;
  String nick;
  String number;
  String groupName;
  String phone;
  String putpicurl;
  String name;
  String department;
  String projectid;
  String createuid;

  static FaceUserInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceUserInfoBean faceUserInfoBean = FaceUserInfoBean();
    faceUserInfoBean.groupType = map['groupType'];
    faceUserInfoBean.projectname = map['projectname'];
    faceUserInfoBean.city = map['city'];
    faceUserInfoBean.roleid = map['roleid'];
    faceUserInfoBean.groupid = map['groupid'];
    faceUserInfoBean.departmentid = map['departmentid'];
    faceUserInfoBean.napicurl = map['napicurl'];
    faceUserInfoBean.userid = map['userid'];
    faceUserInfoBean.nick = map['nick'];
    faceUserInfoBean.number = map['number'];
    faceUserInfoBean.groupName = map['groupName'];
    faceUserInfoBean.phone = map['phone'];
    faceUserInfoBean.putpicurl = map['putpicurl'];
    faceUserInfoBean.name = map['name'];
    faceUserInfoBean.department = map['department'];
    faceUserInfoBean.projectid = map['projectid'];
    faceUserInfoBean.createuid = map['createuid'];
    return faceUserInfoBean;
  }

  Map toJson() => {
    "groupType": groupType,
    "projectname": projectname,
    "city": city,
    "roleid": roleid,
    "groupid": groupid,
    "departmentid": departmentid,
    "napicurl": napicurl,
    "userid": userid,
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "phone": phone,
    "putpicurl": putpicurl,
    "name": name,
    "department": department,
    "projectid": projectid,
    "createuid": createuid,
  };
}

/// date : "2021-03-01"
/// mintime : 0
/// pcnt : 0
/// maxtime : 0
/// rest : "01"
/// absence : "00"

class PunchRecordBean {

  // String date;
  // String punchstatus;
  // int firsttime;
  // int lasttime;
  String daystatus;
  // String rule;
  // int pcnt;


  String date;
  String punchstatus; // 上班状态 00出勤01缺勤 02纠正 03加班 04非加班 05请假 06外出 07确定加班 99异常'
  int mintime;
  int pcnt;
  int maxtime;
  String rule; // 01无规则，00有规则
  String rest;
  // String absence;
  // String normal;
  // String overtime;
  int day;
  String cpid;
  String backup;

  String position;

  static PunchRecordBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PunchRecordBean punchRecordBean = PunchRecordBean();
    punchRecordBean.date = map['date'];
    punchRecordBean.mintime = map['firsttime'];
    punchRecordBean.pcnt = map['pcnt'];
    punchRecordBean.maxtime = map['lasttime'];
    punchRecordBean.rest = map['rest'];
    // punchRecordBean.absence = map['absence'];
    punchRecordBean.day = _getDayByDate(map['date']);
    // punchRecordBean.normal = map['normal'];
    // punchRecordBean.overtime = map['overtime'];
    punchRecordBean.cpid = map['cpid'];
    punchRecordBean.punchstatus = map['punchstatus'];
    punchRecordBean.position = map['position'];
    punchRecordBean.rule = map['rule'];
    punchRecordBean.backup = map['backup'];
    punchRecordBean.daystatus = map['daystatus'];
    return punchRecordBean;
  }

  Map toJson() => {
    "date": date,
    "firsttime": mintime,
    "pcnt": pcnt,
    "lasttime": maxtime,
    "rest": rest,
    // "absence": absence,
    "day": day,
    // "normal": normal,
    // "overtime": overtime,
    "cpid": cpid,
    "punchstatus": punchstatus,
    "position": position,
    "rule": rule,
    "barkup": backup,
    "daystatus": daystatus,
  };


  static int _getDayByDate(String date){
    if(date == null || date.isEmpty){
      return null;
    }
    return DateTime.tryParse(date)?.day;
  }

  @override
  String toString() {
    return 'PunchRecordBean{day: $day}\n';
  }

  // rest  00  休息日  01工作日  02次数
  // normal  00 正常  01异常
  // absence  00 缺勤  01出勤
  // overtime  00 加班 01不加班
  //上班状态 00出勤01缺勤 02纠正 03加班 04非加班 05请假 06外出 07确定加班 99异常',

  // ///是否是正常
  bool isNormal(){
    // return normal == "00";
    return punchstatus == "00" || punchstatus == "02";
  }
  //
  // ///是否异常
  bool isException() {
    // return normal == "01";
    return punchstatus == "99";
  }
  //
  // ///是否是休息
  bool isRest(){
    return rest == "00";
    // return punchstatus == "04";
  }
  //
  // ///是否显示次数
  bool isRestNum(){
    return rule == "01" || rule == "02" && pcnt != 0;
  }
  // ///是否外出
  bool isLeave(){
    return punchstatus == "06";
  }
  //
  // ///是否缺勤
  bool isAbsence(){
    // return absence == "00" && !isRest();
    return punchstatus == "01";
  }
  //
  // ///是否加班
  bool isOverTime(){
    return punchstatus == "03" && isRest();
  }

  // ///是加班
  bool isConfirmOverTime(){
    return punchstatus == "07" && isRest();
  }

  // ///否认加班
  bool isNotOverTime(){
    return punchstatus == "04" && isRest();
  }

  // ///是否请假
  bool isNotify(){
    return punchstatus == "05";
  }


}
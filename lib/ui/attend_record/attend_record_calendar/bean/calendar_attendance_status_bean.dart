import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"operatorInfo":{"punchstatus":"00","name":"岁岁啊","type":"00"},"dayUserPunchList":[{"punchTime":1617069720,"position":"小队座位"}],"dayStatus":{"rest":"01","normal":"00","absence":"01"},"punchRule":true}
/// extra : null

class CalendarAttendanceStatusBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CalendarAttendanceStatusBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CalendarAttendanceStatusBean calendarAttendanceStatusBeanBean = CalendarAttendanceStatusBean();
    calendarAttendanceStatusBeanBean.success = map['success'];
    calendarAttendanceStatusBeanBean.code = map['code'];
    calendarAttendanceStatusBeanBean.msg = map['msg'];
    calendarAttendanceStatusBeanBean.data = DataBean.fromMap(map['data']);
    calendarAttendanceStatusBeanBean.extra = map['extra'];
    return calendarAttendanceStatusBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// operatorInfo : {"punchstatus":"00","name":"岁岁啊","type":"00"}
/// dayUserPunchList : [{"punchTime":1617069720,"position":"小队座位"}]
/// dayStatus : {"rest":"01","normal":"00","absence":"01"}
/// punchRule : true

class DataBean {
  FaceuserBean faceuser;
  OperatorInfoBean operatorInfo;
  List<DayUserPunchListBean> dayUserPunchList;
  DayStatusBean dayStatus;
  bool punchRule;
  String cpid;
  PunchuserBean punchuser;
  int monthPunchCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.operatorInfo = OperatorInfoBean.fromMap(map['operatorInfo']);
    dataBean.faceuser = FaceuserBean.fromMap(map['faceuser']);
    dataBean.dayUserPunchList = List()..addAll(
      (map['dayUserPunchList'] as List ?? []).map((o) => DayUserPunchListBean.fromMap(o))
    );
    dataBean.dayStatus = DayStatusBean.fromMap(map['dayStatus']);
    dataBean.punchRule = map['punchRule'];
    dataBean.cpid = map['cpid'];
    dataBean.punchuser = PunchuserBean.fromMap(map['punchuser']);
    dataBean.monthPunchCnt = map['monthPunchCnt'];
    return dataBean;
  }

  Map toJson() => {
    "operatorInfo": operatorInfo,
    "dayUserPunchList": dayUserPunchList,
    "dayStatus": dayStatus,
    "punchRule": punchRule,
    "cpid": cpid,
    "punchuser": punchuser,
    "monthPunchCnt": monthPunchCnt,
  };
}

/// rest : "01"
/// normal : "00"
/// absence : "01"

class DayStatusBean {
  // rest  00  休息日  01工作日
  // normal  00 正常  01异常
  // absence  00 缺勤  01出勤
  // overtime  00 加班 01不加班
  String rest;
  String normal;
  String absence;
  String overtime;

  static DayStatusBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DayStatusBean dayStatusBean = DayStatusBean();
    dayStatusBean.rest = map['rest'];
    dayStatusBean.normal = map['normal'];
    dayStatusBean.absence = map['absence'];
    dayStatusBean.overtime = map['overtime'];
    return dayStatusBean;
  }

  Map toJson() => {
    "rest": rest,
    "normal": normal,
    "absence": absence,
    "overtime": overtime,
  };
}

/// id : "164ddffd754113b02ffb6d0c303baf90"
/// fuid : "247d0b9cea4543c7b91c187bb27b7101"
/// companyid : "cbb16e2724ca46a3a518914d181f9b71"
/// punchdate : 1620662400
/// firsttime : 1620727740
/// lasttime : null
/// lasthsn : null
/// punchcnt : 1
/// punchstatus : "00"
/// daystatus : "00"
/// dayRuleStatus : "00"
/// groupType : "04"
/// recorded : "00"
/// departmentid : null
/// dutytime : null
/// onFlextime : 0
/// offdutytime : null
/// offFlextime : 0
/// duration : 0
/// backup : null
/// backupuid : null
/// backuptime : null

class PunchuserBean {
  String id;
  String fuid;
  String companyid;
  int punchdate;
  int firsttime;
  dynamic lasttime;
  dynamic lasthsn;
  int punchcnt;
  String punchstatus;
  String daystatus;
  String dayRuleStatus;
  String groupType;
  String recorded;
  dynamic departmentid;
  dynamic dutytime;//上班时间规则
  int onFlextime;//上班弹性时间
  dynamic offdutytime;//下班时间
  int offFlextime;//下班弹性时间
  int duration;//时长要求
  dynamic backup;
  dynamic backupuid;
  dynamic backuptime;
  dynamic backupname;

  static PunchuserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PunchuserBean punchuserBean = PunchuserBean();
    punchuserBean.id = map['id'];
    punchuserBean.fuid = map['fuid'];
    punchuserBean.companyid = map['companyid'];
    punchuserBean.punchdate = map['punchdate'];
    punchuserBean.firsttime = map['firsttime'];
    punchuserBean.lasttime = map['lasttime'];
    punchuserBean.lasthsn = map['lasthsn'];
    punchuserBean.punchcnt = map['punchcnt'];
    punchuserBean.punchstatus = map['punchstatus'];
    punchuserBean.daystatus = map['daystatus'];
    punchuserBean.dayRuleStatus = map['dayRuleStatus'];
    punchuserBean.groupType = map['groupType'];
    punchuserBean.recorded = map['recorded'];
    punchuserBean.departmentid = map['departmentid'];
    punchuserBean.dutytime = map['dutytime'];
    punchuserBean.onFlextime = map['onFlextime'];
    punchuserBean.offdutytime = map['offdutytime'];
    punchuserBean.offFlextime = map['offFlextime'];
    punchuserBean.duration = map['duration'];
    punchuserBean.backup = map['backup'];
    punchuserBean.backupuid = map['backupuid'];
    punchuserBean.backuptime = map['backuptime'];
    punchuserBean.backupname = map['backupname'];
    return punchuserBean;
  }

  Map toJson() => {
    "id": id,
    "fuid": fuid,
    "companyid": companyid,
    "punchdate": punchdate,
    "firsttime": firsttime,
    "lasttime": lasttime,
    "lasthsn": lasthsn,
    "punchcnt": punchcnt,
    "punchstatus": punchstatus,
    "daystatus": daystatus,
    "dayRuleStatus": dayRuleStatus,
    "groupType": groupType,
    "recorded": recorded,
    "departmentid": departmentid,
    "dutytime": dutytime,
    "onFlextime": onFlextime,
    "offdutytime": offdutytime,
    "offFlextime": offFlextime,
    "duration": duration,
    "backup": backup,
    "backupuid": backupuid,
    "backuptime": backuptime,
    "backupname": backupname,
  };

  bool isRule(){
    bool rule = true;
    if(duration<=0){
      rule = false;
    }else if(StringUtils.isEmpty(dutytime)&&StringUtils.isEmpty(offdutytime)){
      rule = false;
    }
      return rule;

  }
}

/// punchTime : 1617069720
/// position : "小队座位"

class DayUserPunchListBean {
  int punchTime;
  String position;

  String backup;
  int createdate;
  String name;
  String temperature;
  String pictureUrl;
  String napicurl;
  int fpid;
  String type;//00终端刷脸 01手动,


  static DayUserPunchListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DayUserPunchListBean dayUserPunchListBean = DayUserPunchListBean();
    dayUserPunchListBean.punchTime = map['punchTime'];
    dayUserPunchListBean.position = map['position'];
    dayUserPunchListBean.backup = map['backup'];
    dayUserPunchListBean.createdate = map['createdate'];
    dayUserPunchListBean.name = map['name'];
    dayUserPunchListBean.temperature = map['temperature'];
    dayUserPunchListBean.pictureUrl = map['pictureUrl'];
    dayUserPunchListBean.napicurl = map['napicurl'];
    dayUserPunchListBean.fpid = map['fpid'];
    dayUserPunchListBean.type = map['type'];
    return dayUserPunchListBean;
  }

  Map toJson() => {
    "punchTime": punchTime,
    "position": position,
    "backup": backup,
    "createdate": createdate,
    "name": name,
    "temperature": temperature,
    "pictureUrl": pictureUrl,
    "napicurl": napicurl,
    "fpid": fpid,
    "type": type,
  };
}

/// punchstatus : "00"
/// name : "岁岁啊"
/// type : "00"

class OperatorInfoBean {
  String punchstatus;
  String name;
  String type;

  static OperatorInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    OperatorInfoBean operatorInfoBean = OperatorInfoBean();
    operatorInfoBean.punchstatus = map['punchstatus'];
    operatorInfoBean.name = map['name'];
    operatorInfoBean.type = map['type'];
    return operatorInfoBean;
  }

  Map toJson() => {
    "punchstatus": punchstatus,
    "name": name,
    "type": type,
  };
}

/// fuid : "3b6d7096767d46ef868f15e2766cd0d3"
/// type : "01"
/// number : ""
/// name : "北开李岁红"
/// nick : ""
/// phone : "18200000000"
/// napicurl : "http://etpic.we17.com/test/20210514151004_3759.jpg"
/// putpicurl : "http://etpic.we17.com/test/20210514151004_3759.jpg"
/// companyid : "0126e97aed8146c785dc9be2d6a4c41c"
/// createdate : 1619336254
/// createuid : "4a3d21386acb4f3d923227515c130470"
/// groupid : "aaa9c030de1a42959fc8d43438951cb1"
/// status : "01"
/// userid : "4a3d21386acb4f3d923227515c130470"
/// roleid : "99"
/// city : ""
/// projectid : ""
/// departmentid : "d6e48324b86c424ea1d83f048285c347"
/// delflg : "00"
/// punchDayCnt : 0

class FaceuserBean {
  String fuid;
  String type;
  String number;
  String name;
  String nick;
  String phone;
  String napicurl;
  String putpicurl;
  String companyid;
  int createdate;
  String createuid;
  String groupid;
  String status;
  String userid;
  String roleid;
  String city;
  String projectid;
  String departmentid;
  String delflg;
  int punchDayCnt;


  static FaceuserBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceuserBean faceuserBean = FaceuserBean();
    faceuserBean.fuid = map['fuid'];
    faceuserBean.type = map['type'];
    faceuserBean.number = map['number'];
    faceuserBean.name = map['name'];
    faceuserBean.nick = map['nick'];
    faceuserBean.phone = map['phone'];
    faceuserBean.napicurl = map['napicurl'];
    faceuserBean.putpicurl = map['putpicurl'];
    faceuserBean.companyid = map['companyid'];
    faceuserBean.createdate = map['createdate'];
    faceuserBean.createuid = map['createuid'];
    faceuserBean.groupid = map['groupid'];
    faceuserBean.status = map['status'];
    faceuserBean.userid = map['userid'];
    faceuserBean.roleid = map['roleid'];
    faceuserBean.city = map['city'];
    faceuserBean.projectid = map['projectid'];
    faceuserBean.departmentid = map['departmentid'];
    faceuserBean.delflg = map['delflg'];
    faceuserBean.punchDayCnt = map['punchDayCnt'];
    return faceuserBean;
  }

  Map toJson() =>
      {
        "fuid": fuid,
        "type": type,
        "number": number,
        "name": name,
        "nick": nick,
        "phone": phone,
        "napicurl": napicurl,
        "putpicurl": putpicurl,
        "companyid": companyid,
        "createdate": createdate,
        "createuid": createuid,
        "groupid": groupid,
        "status": status,
        "userid": userid,
        "roleid": roleid,
        "city": city,
        "projectid": projectid,
        "departmentid": departmentid,
        "delflg": delflg,
        "punchDayCnt": punchDayCnt,

      };
}
import 'dart:async';

import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_view_model.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/attend_record_calendar_response_bean.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/attend_record_calendar_page_view_widget.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/attend_record_calendar_statistics_card_widget.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/calendar_attendance_status_statistics_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'attendance_record_today_viewmodel.dart';
import 'widgets/attend_record_calendar_title_bar_widget.dart';
import 'widgets/attend_record_calendar_top_bar_widget.dart';

/// 考勤记录日历页
///
/// @author: zengxiangxi
/// @createTime: 3/15/21 2:21 PM
/// @specialDemand:
class AttendRecordCalendarPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "AttendRecordCalendarPage";

  final String fuid;
  final String pages;

  const AttendRecordCalendarPage({Key key, this.fuid, this.pages}) : super(key: key);

  @override
  AttendRecordCalendarPageState createState() =>
      AttendRecordCalendarPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,String fuid,{String pages}) {
    return RouterUtils.routeForFutureResult(
      context,
      AttendRecordCalendarPage(fuid:fuid,pages: pages,
      ),
      routeName: AttendRecordCalendarPage.ROUTER,
    ).then((value){
      //点击返回时设置为0 下次可以继续点击
      SharePreferenceUtil.putInt(fuid??"", 0);
    });
  }
}

class AttendRecordCalendarPageState extends BaseState<AttendRecordCalendarPage> {

  int _lastReportedPage;
  PageController _pageController;
  final int initPostion = 50;

  ///当前日期
  final DateTime initDateTime = DateTime.now();

  ValueNotifier<int> _pageValueNotifier;
  ValueNotifier<int> get pageValueNotifier => _pageValueNotifier;

  AttendRecordCalendarViewModel _viewModel;
  AttendRecordCalendarViewModel get viewModel => _viewModel;

  PunchRecordBean currentPunchRecordBean;

  static AttendRecordCalendarPageState of(BuildContext context) {
    return context?.findAncestorStateOfType<AttendRecordCalendarPageState>();
  }

  @override
  void initState() {
    super.initState();
    _viewModel = AttendRecordCalendarViewModel(this,widget.fuid);
    _pageValueNotifier = ValueNotifier(initPostion);
    _pageController =
        PageController(initialPage: initPostion, keepPage: true);
    _pageController.addListener(_changePage);


  }

  void _changePage() {
    final int currentPage = _pageController?.page?.round();
    if (currentPage != _lastReportedPage) {
      _lastReportedPage = currentPage;
      _pageValueNotifier.value = currentPage;
    }
  }

  @override
  void dispose() {
    _pageController.removeListener(_changePage);
    _pageController?.dispose();
    _pageValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: Column(
        children: <Widget>[
          AttendRecordCalendarTopBarWidget(
            dateTimeValueNotifier: _pageValueNotifier,
            dateInfoValueNotifier: _viewModel.dateInfoValueNotifier,
            fuid: widget.fuid,
            onPrevious: () {
              _pageController?.previousPage(duration: const Duration(milliseconds: 300), curve: Curves.linearToEaseOut);
            },
            onNext: () {
              _pageController?.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.linearToEaseOut);
            },
            pages: widget?.pages,
          ),
          Expanded(
            child: _toSingleScrollView(),
          ),
        ],
      ),
    );
  }


  Widget _toSingleScrollView(){
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AttendRecordCalendarPageViewWidget(
              pageController: _pageController,
              itemCount: 51,
            ),
            CalendarAttendanceStatusStatisticsWidget(dayPunchRecordBeanValue: currentPunchRecordBean,fuid: widget?.fuid,dateTimeValueNotifier: _pageValueNotifier,),
            AttendRecordCalendarStatisticsCardWidget(
              dateInfoValueNotifier: _viewModel.dateInfoValueNotifier,
            ),
          ],
        ),
      ),
    );
  }

  setPunchRecordBean(PunchRecordBean punchRecordBean){
    this.currentPunchRecordBean = punchRecordBean;
    setState(() { });
  }
}

import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/attend_record_calendar_grid_widget.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 考勤记录日历切换页组件
///
/// @author: zengxiangxi
/// @createTime: 3/15/21 2:43 PM
/// @specialDemand:
class AttendRecordCalendarPageViewWidget extends StatelessWidget {

  final PageController pageController;

  final int itemCount;

  const AttendRecordCalendarPageViewWidget({Key key, this.pageController, this.itemCount}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: (ScreenUtils.screenW(context) -90) * 1.27+68+30,
      child: PageView.builder(
        controller: pageController,
        itemCount: itemCount ?? 0,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index){
          return AttendRecordCalendarGridWidget(index: index,);
        },
      ),
    );
  }
}

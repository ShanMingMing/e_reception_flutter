import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:flutter/widgets.dart';

/// 考勤记录日期标题bar
///
/// @author: zengxiangxi
/// @createTime: 3/16/21 11:26 AM
/// @specialDemand:
class AttendRecordCalendarTitleBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 9),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(1.5),
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            ),
            width: 3,
            height: 14,
          ),
          SizedBox(width: 7),
          Text(
            "自动统计",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 15,
                fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}

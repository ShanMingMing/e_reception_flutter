import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';

import '../attendance_record_today_viewmodel.dart';

class AttendRecordRemarkDialog extends StatefulWidget {
  final String cpid;
  final String backup;

  const AttendRecordRemarkDialog({Key key, this.cpid, this.backup})
      : super(key: key);

  @override
  _AttendRecordRemarkDialogState createState() =>
      _AttendRecordRemarkDialogState();

  ///跳转方法
  static Future navigatorPushDialog(BuildContext context, String cpid,
      {String backup}) {
    return VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: AttendRecordRemarkDialog(
          cpid: cpid,
          backup: backup,
        ));
  }
}

class _AttendRecordRemarkDialogState
    extends BaseState<AttendRecordRemarkDialog> {
  TextEditingController _editingController;
  bool isAlive = false;
  AttendanceRecordTodayViewModel viewModel;

  @override
  void initState() {
    super.initState();
    _editingController = TextEditingController();
    viewModel = AttendanceRecordTodayViewModel(this, null);
    if(widget?.backup!=null && widget?.backup!=""){
      _editingController?.text = widget?.backup;
      isAlive = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: MediaQuery.of(context).viewInsets, //边距（必要）
      duration: const Duration(milliseconds: 100), //时常 （必要）
      child: Container(
          decoration: BoxDecoration(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              borderRadius:
                  BorderRadius.circular(DEFAULT_BOTTOM_CARD_DIALOG_RADIUS)),
          child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                "本日备注",
                style: TextStyle(
                    fontSize: 17,
                    color: ThemeRepository.getInstance()
                        .getTextMainColor_D0E0F7(),fontWeight: FontWeight.w600),
              ),
            ),
            Spacer(),
            Opacity(
                opacity: widget?.backup!=null && widget?.backup!="" ? 1 : 0,
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () async {
                    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                        title: "提示",
                        content: "确定删除本日备注？",
                        cancelText: "取消",
                        confirmText: "删除",
                        confirmBgColor:
                        ThemeRepository.getInstance().getMinorRedColor_F95355());
                    if (result ?? false) {
                      _editingController?.text = "";
                      viewModel?.addRemarksDayAttendanceData(
                          context, widget?.cpid, _editingController?.text, "01");
                    }

                    setState(() {});
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      "删除",
                      style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance()
                              .getHintGreenColor_5E687C()),
                    ),
                  ),
                )),
            _buttonWidget(),
          ],
        ),
        Container(
          padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
          alignment: Alignment.topLeft,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 120,
                  color:
                      ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  child: VgTextField(
                    controller: _editingController,
                    maxLines: 4,
                    minLines: 4,
                    maxLength: 200,
                    autofocus: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      counterText: "",
                      contentPadding: const EdgeInsets.only(left: 15, top: 12),
                      hintText: "请输入...",
                      hintStyle: TextStyle(
                          color: ThemeRepository.getInstance()
                              .getTextEditHintColor_3A3F50(),
                          fontSize: 14),
                    ),
                    style: TextStyle(
                        fontSize: 14,
                        color: ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7()),
                    onChanged: (String str) {
                      if (str != "" && str != null) {
                        isAlive = true;
                      } else {
                        isAlive = false;
                      }
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buttonWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: CommonFixedHeightConfirmButtonWidget(
        isAlive: isAlive ?? false,
        width: 76,
        height: 30,
        unSelectBgColor:
            ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
        selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
        unSelectTextStyle: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 14,
        ),
        selectedTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
        ),
        text: "保存",
        onTap: () {
          if (isAlive) {
            viewModel?.addRemarksDayAttendanceData(
                context, widget?.cpid, _editingController?.text, "00");
            setState(() { });
          }
        },
      ),
    );
  }
}

import 'dart:ffi';

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/attend_record_calendar_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_page.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// 考勤记录日历Topbar
///
/// @author: zengxiangxi
/// @createTime: 3/15/21 2:23 PM
/// @specialDemand:
class AttendRecordCalendarTopBarWidget extends StatelessWidget {
  final ValueNotifier<int> dateTimeValueNotifier;

  ///具体信息
  final ValueNotifier<AttendRecordCalendarDataBean> dateInfoValueNotifier;

  final VoidCallback onPrevious;
  final VoidCallback onNext;

  final String fuid;
  final String pages;


  const AttendRecordCalendarTopBarWidget(
      {Key key, this.dateTimeValueNotifier, this.onPrevious, this.onNext, this.dateInfoValueNotifier, this.fuid, this.pages})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      padding: EdgeInsets.only(top: ScreenUtils.getStatusBarH(context)),
      child: Container(
        height: 44,
        child: _toMainRowWidget(context),
      ),
    );
  }

  Widget _toMainRowWidget(BuildContext context) {
    DateTime dateTime;
    return ValueListenableBuilder(
      valueListenable: dateInfoValueNotifier,

      builder: (BuildContext context, AttendRecordCalendarDataBean dateInfoBean, Widget child) {
       return Row(
         children: <Widget>[
           ClickAnimateWidget(
             child: Container(
               width: 40,
               height: 44,
               alignment: Alignment.centerLeft,
               padding: const EdgeInsets.only(left: 15.6),
               child: Image.asset(
                 "images/top_bar_back_ico.png",
                 width: 9,
               ),
             ),
             scale: 1.4,
             onClick: () {
               //点击返回时设置为0 下次可以继续点击
               SharePreferenceUtil.putInt(fuid??"", 0);
               FocusScope.of(context).requestFocus(FocusNode());
               Navigator.of(context).maybePop();
             },
           ),
           Expanded(
             child: GestureDetector(
               behavior: HitTestBehavior.translucent,
               onTap: (){
                 // PersonDetailPage.navigatorPush(context, fuid,pages: "AttendRecordCalendarPage");
                 PersonDetailPage.navigatorPush(context, fuid, pages: pages);
               },
               child: Container(
                 child: Center(
                   child: Row(
                     children: <Widget>[
                       ClipOval(
                         child: Container(
                           height: 28,
                           width: 28,
                           child: VgCacheNetWorkImage(
                             showOriginEmptyStr(dateInfoBean?.faceUserInfo?.putpicurl) ?? (dateInfoBean?.faceUserInfo?.napicurl ?? ""),
                             fit: BoxFit.cover,
                             defaultErrorType: ImageErrorType.head,
                             defaultPlaceType: ImagePlaceType.head,
                             imageQualityType: ImageQualityType.middleDown,
                           ),
                         ),
                       ),
                       SizedBox(
                         width: 8,
                       ),
                       Text(
                         "${VgStringUtils.subStringAndAppendSymbol(dateInfoBean?.faceUserInfo?.name ?? "用户名", 8,symbol: "...")}",
                         maxLines: 1,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),
                       ),
                     ],
                   ),
                 ),
               ),
             ),
           ),
           // SizedBox(width: 30,),
           GestureDetector(
             behavior: HitTestBehavior.translucent,
             onTap: () {
               onPrevious?.call();
             },
             child: Container(
                 height: 44,
                 padding: const EdgeInsets.only(left: 16, right: 10),
                 child: Center(
                     child: Image.asset(
                       "images/calendar_previous_ico.png",
                       width: 16,
                     ))),
           ),
           ValueListenableBuilder(
             valueListenable: dateTimeValueNotifier,
             builder: (BuildContext context, int pageValue, Widget child) {

               if(pageValue != null && pageValue >=0){
                 final DateTime initDateTime =
                     AttendRecordCalendarPageState.of(context).initDateTime;
                 final int initPositon =
                     AttendRecordCalendarPageState.of(context).initPostion;
                 final int diff = pageValue - initPositon;
                 dateTime = DateTime(initDateTime.year,
                     initDateTime.month + (diff), 1);
               }

               return Container(
                 width: 85,
                 child: Center(
                   child: Text(
                     "${dateTime?.year ?? "-"}年${dateTime?.month ?? "-"}月",
                     maxLines: 1,
                     overflow: TextOverflow.ellipsis,
                     style: TextStyle(color: Colors.white, fontSize: 15, height: 1.2),
                   ),
                 ),
               );
             },
           ),
           GestureDetector(
             behavior: HitTestBehavior.translucent,
             onTap: () {
               if(isEquealDateTime(dateTime)){
                 return;
               }
               onNext?.call();
             },
             child: Opacity(
               opacity: isEquealDateTime(dateTime) ? 0.3:1,
               child: Container(
                   height: 44,
                   padding: const EdgeInsets.only(left: 10, right: 16),
                   child: Center(
                       child: Image.asset(
                         "images/calendar_next_ico.png",
                         width: 16,
                       ))),
             ),
           )
         ],
       );
      },
    );
  }
  static bool isEquealDateTime(DateTime dateTime){
    if(dateTime?.year == DateTime.now().year && dateTime?.month == DateTime.now().month){
      return true;
    }else{
      return false;
    }
  }
}

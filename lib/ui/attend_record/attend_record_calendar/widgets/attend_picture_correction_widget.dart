import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/calendar_attendance_status_bean.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/picture_correction_search_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import '../attend_record_calendar_view_model.dart';

class AttendPictureCorrectionWidget extends StatefulWidget {
  final DayUserPunchListBean itemBean;
  final FaceuserBean faceuserBean;
  final String Cpid;

  const AttendPictureCorrectionWidget(
      {Key key, this.itemBean, this.faceuserBean, this.Cpid})
      : super(key: key);

  @override
  _AttendPictureCorrectionWidgetState createState() =>
      _AttendPictureCorrectionWidgetState();

  static Future<dynamic> navigatorPush(BuildContext context,
      DayUserPunchListBean itemBean, FaceuserBean faceuserBean,String Cpid) {
    return RouterUtils.routeForFutureResult(
      context,
      AttendPictureCorrectionWidget(
        itemBean: itemBean,
        faceuserBean: faceuserBean,
          Cpid:Cpid
      ),
    );
  }
}

class _AttendPictureCorrectionWidgetState
    extends BaseState<AttendPictureCorrectionWidget> {
  ValueNotifier<String> _searchPageValueNotifier;
  AttendRecordCalendarViewModel viewModel;
  bool hide = true;
  bool insideSelected = true;
  // bool externalSelected = false;
  bool determine = false;

  String editStr;

  TextEditingController _editingController;

  CompanyDetailByTypeListItemBean selectItemBean;

  String name = "请选择";

  @override
  void initState() {
    super.initState();
    selectItemBean = CompanyDetailByTypeListItemBean();
    _searchPageValueNotifier = ValueNotifier(null);
    viewModel = AttendRecordCalendarViewModel(this,widget?.faceuserBean?.fuid);
    _editingController = TextEditingController();
  }

  @override
  void dispose() {
      _editingController?.dispose();
      _searchPageValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _attendPictureCorrection(),
    );
  }

  Widget _attendPictureCorrection() {
    return ListView(
      children: [
         Column(
          children: [
            _toTopBarWidget(),
            //查看大头像
            Container(
                // height: VgToolUtils.getScreenHeight()/2,
                // width: VgToolUtils.getScreenWidth(),
                child: VgCacheNetWorkImage(
                  widget?.itemBean?.pictureUrl ?? "",
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middleDown,
                  defaultErrorType: ImageErrorType.head,
                  defaultPlaceType: ImagePlaceType.head,
                )),
            // SizedBox(height: 30,),
            hide ? _pictureCorrectionButton() : _pictureCorrectionWidget(),
          ],
        ),
      ],

    );
  }

  Widget _pictureCorrectionWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          Container(
              padding: const EdgeInsets.only(top: 20, bottom: 12),
              alignment: Alignment.centerLeft,
              child: Text(
                "请标注正确的人员信息：",
                style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getMinorYellowColor_FFB714(),
                    fontSize: 12),
              )),
          _selectType(),
          SizedBox(height: 12,),
          insideSelected ? _writeName() :_writeNotes(),
          SizedBox(height: 30,),
          _buttonDetermination()
        ],
      ),
    );
  }

  Widget _buttonDetermination(){
    return Row(
      children: [
        Expanded(
          child: Container(
            alignment: Alignment.center,
            height: 40,
            decoration: BoxDecoration(
                border: Border.all(
                    color: ThemeRepository.getInstance().getLineColor_3A3F50(),
                    width: 1),
                color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                borderRadius: BorderRadius.circular(22.5)),
            child: Text(
              "取消",
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 15),
            )
          ),
        ),
        SizedBox(width: 10,),
        Expanded(
          child: GestureDetector(
            onTap: (){
              if(!determine){
                return;
              }
              String type = "00";//00内部  01外部
              insideSelected ?type = "00" :type = "01";
              String fuid = "";
              if(type == "00"){
                if(selectItemBean?.fuid!=null&&selectItemBean?.fuid!=""){
                  if(selectItemBean?.fuid != widget?.faceuserBean?.fuid){
                    fuid = selectItemBean?.fuid;
                  }else{
                    VgToastUtils.toast(context, "修改人不允许是当前用户");
                    return;
                  }
                }
              }else{
                fuid = "";
              }
              viewModel.faceRecordUpdate(context, widget?.itemBean?.fpid ?? 0, type, editStr,fuid,widget?.Cpid,widget?.itemBean?.punchTime);
            },
            child: Container(
                alignment: Alignment.center,
                height: 40,
                // padding: const EdgeInsets.symmetric(horizontal: 14),
                decoration: BoxDecoration(
                    color: determine ?ThemeRepository.getInstance().getPrimaryColor_1890FF() : ThemeRepository.getInstance().getLineColor_3A3F50(),
                    borderRadius: BorderRadius.circular(22.5)),
                child: Text(
                  "确定",
                  style: TextStyle(
                      color:determine ?ThemeRepository.getInstance().getTextMainColor_D0E0F7() : ThemeRepository.getInstance()
                          .getTextMinorGreyColor_808388(),
                      fontSize: 15),
                )
            ),
          ),
        )
      ],
    );
  }

  Widget _writeNotes(){
    return Container(
        height: 45,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
            borderRadius: BorderRadius.circular(4)),
        child: VgTextField(
          controller: _editingController,
          maxLines: 1,
          maxLength: 10,
          decoration: InputDecoration(
            border: InputBorder.none,
            counterText: "",
            contentPadding: const EdgeInsets.only(bottom: 2),
            hintText: "备注说明",
            hintStyle: TextStyle(
                color:
                ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
                fontSize: 14),
          ),
          style: TextStyle(
              fontSize: 14,
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),

          onChanged: (String value){
            if(value!=null &&value!=""){
              determine = true;
              editStr  = value;
            }else{
              determine = false;
              editStr  = value;
            }
            setState(() {

            });
          },
        ));
  }

  Widget _writeName(){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        CorrectionSearchPage.navigatorPush(context,widget?.faceuserBean).then((value){
          selectItemBean = value;
          if(selectItemBean ==null){
            name = "请选择";
            determine = false;
          }else{
            name = selectItemBean?.name??"";
            determine = true;
          }
          setState(() { });
        });
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
          height: 45,
          alignment: Alignment.centerLeft,
          child: Row(
            children: [
              Container(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Text(
                    "姓名",
                    style: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getTextMinorGreyColor_808388(),
                        fontSize: 14),
                  )),
              Container(
                  child: Text(
                    name ?? "请选择",
                    style: TextStyle(
                        color: name!="请选择" ? ThemeRepository.getInstance().getTextMainColor_D0E0F7() : ThemeRepository.getInstance()
                            .getHintGreenColor_5E687C(),
                        fontSize: 14),
                  )),
              Spacer(),
              Image.asset(
                "images/index_arrow_ico.png",
                width: 6,
              ),
              SizedBox(width: 15,)
            ],
          ),
        ),
      ),
    );
  }

  Widget _selectType(){
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
        height: 45,
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  "类型",
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMinorGreyColor_808388(),
                      fontSize: 14),
                )),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                insideSelected =!insideSelected;
                setState(() {

                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Image.asset(
                      insideSelected
                          ? 'images/select_w.png'
                          : 'images/unselect_w.png',
                      width: 20,
                      height: 20,
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.only(left: 8, right: 20),
                      height: 45,
                      alignment: Alignment?.center,
                      child: Text(
                        "内部人员",
                        style: TextStyle(
                            color: insideSelected
                                ? ThemeRepository.getInstance()
                                .getTextMainColor_D0E0F7()
                                : ThemeRepository.getInstance()
                                .getTextMinorGreyColor_808388(),
                            fontSize: 14),
                      )),
                ],
              ),
            ),
            SizedBox(width: 20,),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                insideSelected = !insideSelected;
                setState(() {

                });
              },
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Image.asset(
                      !insideSelected
                          ? 'images/select_w.png'
                          : 'images/unselect_w.png',
                      width: 20,
                      height: 20,
                    ),
                  ),
                  Container(
                      height: 45,
                      alignment: Alignment?.center,
                      padding: const EdgeInsets.only(left: 8, right: 40),
                      child: Text(
                        "外部访客",
                        style: TextStyle(
                            color: !insideSelected
                                ? ThemeRepository.getInstance()
                                .getTextMainColor_D0E0F7()
                                : ThemeRepository.getInstance()
                                .getTextMinorGreyColor_808388(),
                            fontSize: 14),
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _pictureCorrectionButton() {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            hide = !hide;
            setState(() {});
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Container(
              height: 40,
              width: 200,
              alignment: Alignment.center,
              color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
              child: Text(
                "识别错误？去修改",
                style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getMinorYellowColor_FFB714(),
                    fontSize: 15),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      isShowBackRightWdiget: true,
      backRightWidget: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {},
        child: Container(
          child: Center(
            child: Row(
              children: <Widget>[
                ClipOval(
                  child: Container(
                    height: 28,
                    width: 28,
                    child: VgCacheNetWorkImage(
                      showOriginEmptyStr(widget?.faceuserBean?.putpicurl) ??
                          showOriginEmptyStr(widget?.faceuserBean?.napicurl) ??
                          "",
                      fit: BoxFit.cover,
                      defaultErrorType: ImageErrorType.head,
                      defaultPlaceType: ImagePlaceType.head,
                      imageQualityType: ImageQualityType.middleDown,
                    ),
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        "${VgStringUtils.subStringAndAppendSymbol(widget?.faceuserBean?.name, 8, symbol: "...")}",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "${VgToolUtils.getHourAndMinuteStr(widget?.itemBean?.punchTime) ?? ""} ",
                          style: TextStyle(fontSize: 11, color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()),
                        ),
                        backupOrPositionText(),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget backupOrPositionText(){
    if(widget?.itemBean?.createdate!=null && widget?.itemBean?.createdate!=""){
      return Row(
        children: [
          Container(
            width: 200,
            child: Text(
              "${widget?.itemBean?.name ?? ""}",
              style: TextStyle(fontSize: 11, color: Color(0xFF808388),height: 1.3),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      );
    }else{
      return Container(
        width: 200,
        child: Text(
          "${widget?.itemBean?.position ?? ""}",
          style: TextStyle(fontSize: 11, color: Color(0xFF808388),height: 1.3),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      );
    }
  }
}

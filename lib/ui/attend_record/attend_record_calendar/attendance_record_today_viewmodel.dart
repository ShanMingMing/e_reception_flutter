import 'dart:convert';

import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/calendar_attendance_status_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import '../../app_main.dart';

class AttendanceRecordTodayViewModel extends BaseViewModel{
  ValueNotifier<CalendarAttendanceStatusBean> valueNotifier;

  final int Function() getDateTime;
  AttendanceRecordTodayViewModel(BaseState<StatefulWidget> state, this.getDateTime) : super(state){
    valueNotifier = ValueNotifier(null);
  }

  //重新计算
  void userPunchRecordRecalculate(String cpid,BaseCallback callback){
    loading(false);
    VgHttpUtils.get(ServerApi.BASE_URL + "app/userPunchRecordRecalculate",
      params:{
        "authId": UserRepository.getInstance().authId ?? "",
        "cpid":cpid,
      },
      callback: BaseCallback(onSuccess: (val){
        // recallModifyData = val;
        callback?.onSuccess(val);
        // VgEventBus.global.send(ModifyAttendanceRefreshEvent());
      },onError: (msg){
        callback?.onError(msg);
      }),
    );
  }

//点击纠正更改数据
  void modifyAttendanceData(String cpid,String punchstatus,String type,BaseCallback callback){
    loading(false);
        VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayPunchCorrect",
            params:{
              "authId": UserRepository.getInstance().authId ?? "",
              "cpid":cpid,
              "punchstatus":punchstatus,
              "type":type
            },
            callback: BaseCallback(onSuccess: (val){
              // recallModifyData = val;
              callback?.onSuccess(val);
              // VgEventBus.global.send(ModifyAttendanceRefreshEvent());
            },onError: (msg){
              callback?.onError(msg);
            }),
    );
  }
//点击获取每一天的数据
  void choiceDayAttendanceData(int dayTime,String fuid){
    String cacheKey = "app/appDayPunchRecordForFaceUser" + (UserRepository.getInstance().authId??"")
        + fuid;
    ///获取缓存数据
    Future<String> future = SharePreferenceUtil.getString(cacheKey);
    future.then((jsonStr){
      if(!StringUtils.isEmpty(jsonStr)){
        Map map = json.decode(jsonStr);
        CalendarAttendanceStatusBean bean = CalendarAttendanceStatusBean.fromMap(map);
        if(bean != null){
          if(bean.data != null){
            valueNotifier?.value = bean;
          }
        }
      }
    });
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayPunchRecordForFaceUser",
      params:{
        "authId": UserRepository.getInstance().authId ?? "",
        "dayTime":dayTime,
        "fuid":fuid

      },
      callback: BaseCallback(onSuccess: (val){
        // RouterUtils.pop(context,result: val);
        final int getTime = getDateTime?.call();
        if(getTime == dayTime && val != null){
          CalendarAttendanceStatusBean response = CalendarAttendanceStatusBean.fromMap(val);
          valueNotifier?.value = response;
          SharePreferenceUtil.putString("app/appDayPunchRecordForFaceUser" + (UserRepository.getInstance().authId??"")
              + fuid,json.encode(response));
        }else{
          return;
        }

        // VgToastUtils.toast(AppMain.context, "");
      }, onError: (msg) {
        // valueNotifier?.value = null;
        VgToastUtils.toast(AppMain.context, "请检查网络连接");
      }),
    );
  }

  //是否加班
  void setDayPunchOvertime(String cpid,String type,BaseCallback callback){
    loading(false);
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayPunchOvertime",
      params:{
        "authId": UserRepository.getInstance().authId ?? "",
        "cpid":cpid,
        "type":type
      },
      callback: BaseCallback(onSuccess: (val){
        callback?.onSuccess(val);
      },onError: (msg){
        callback?.onError(msg);
      }),
    );
  }

  //添加当天备注
  void addRemarksDayAttendanceData(BuildContext context,String cpid,String backup,String type){
    loading(false);
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayPunchBackUp",
      params:{
        "authId": UserRepository.getInstance().authId ?? "",
        "cpid":cpid,
        "backup":backup ?? "",
        "type":type  //00保存 01删除s

      },
      callback: BaseCallback(onSuccess: (val){
        VgEventBus.global.send(ModifyAttendanceRefreshEvent());
        if(type == "00"){
          VgToastUtils.toast(AppMain.context, "添加成功");
        }else{
          VgToastUtils.toast(AppMain.context, "删除成功");
        }

        RouterUtils.pop(context);
      }, onError: (msg) {
        // valueNotifier?.value = null;
        // VgToastUtils.toast(AppMain.context, "msg");
      }),
    );
  }

}

class ModifyAttendanceRefreshEvent{

}
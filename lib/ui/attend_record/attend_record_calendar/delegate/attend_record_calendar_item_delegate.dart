import 'dart:ffi';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/attend_record_calendar_response_bean.dart';
import 'package:flutter/material.dart';

import 'attend_record_calendar_item_base_delegate.dart';

class AttendRecordCalendarItemDelegate
    extends AttendRecordCalendarItemBaseDelegate {
  AttendRecordCalendarItemDelegate({
    @required BuildContext context,
    @required DateTime initDateTime,
    @required DateTime currentDateTime,
    @required int emptyCount,
    @required StateSetter setState,
    @required VoidCallback onRefresh,
  }) : super(
          context: context,
          initDateTime: initDateTime,
          currentDateTime: currentDateTime,
          emptyCount: emptyCount,
          setState: setState,
          onRefresh: onRefresh,
        );

  //根据状态获取对应组件
  @override
  Widget getItemByNormal(int day, PunchRecordBean itemBean) {
    if (itemBean == null) {
      return _getDefaultWidget(day, itemBean);
    }
    if(itemBean.isRestNum() || itemBean.isNotOverTime()){
      return _getRestNumWidget(day, itemBean);
    }
    if (itemBean.isNormal() && !itemBean.isRest()) {
      return _getNormalWidget(day, itemBean);
    }
    if (itemBean.isLeave()) {
      return _getLesveWidget(day, itemBean);
    }
    if (itemBean.isException()) {
      return _getExceptionWidget(day, itemBean);
    }
    if (itemBean.isAbsence()) {
      return Builder(builder: (BuildContext context) {
        return _getAbsenceWidget(day, itemBean);
      });
    }
    if (itemBean.isNotify()) {
      return Builder(builder: (BuildContext context) {
        return _getNotifyWidget(day, itemBean);
      });
    }
    if (itemBean.isOverTime()||itemBean.isConfirmOverTime()) {
      return _getOvertimeWidget(day, itemBean);
    }
    if (itemBean.isRest()) {
      return _getRestWidget(day, itemBean);
    }
    return _getDefaultWidget(day, itemBean);
  }

  ///正常
  Widget _getNormalWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:itemBean.isRest() ? Color(0xFF5E687C) : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                (itemBean?.isNormal() ?? false) ? "正常" : "异常",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///异常
  Widget _getExceptionWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                (itemBean?.isNormal() ?? false) ? "正常" : "异常",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color:itemBean.isRest() ? Color(0xFF5E687C) : Color(0xFFFFB714),
                  // color:
                  //     ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///q请假
  Widget _getAbsenceWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:itemBean.isRest() ? Color(0xFF5E687C) : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                "缺勤",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color:
                  ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///缺勤
  Widget _getNotifyWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:itemBean.isRest() ? Color(0xFF5E687C) : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                "请假",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color:
                      ThemeRepository.getInstance().getMinorRedColor_F95355(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///加班
  Widget _getOvertimeWidget(int day, PunchRecordBean itemBean) {
    String text = "加班?";
    if(itemBean.isConfirmOverTime()){
      text = "加班";
    }
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                //如果是休息日并且加班，字体为灰色
                color: itemBean.isRest() ? Color(0xFF5E687C) : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                text,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color:
                      ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///休息日
  Widget _getRestWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: VgColors.INPUT_BG_COLOR,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///外出
  Widget _getLesveWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color:itemBean.isRest() ? Color(0xFF5E687C) : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text("外出",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  ///显示次数
  Widget _getRestNumWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: itemBean.isRest() ? VgColors.INPUT_BG_COLOR : Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),

          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Opacity(
                opacity: (itemBean?.pcnt ?? 0) <= 0 ? 0:1,
                child: Text(
                  "${itemBean?.pcnt ?? "0"}次",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance()
                        .getPrimaryColor_1890FF(),
                    fontSize: 11,
                  ),
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  @override
  Widget getItemBySelected(int day, PunchRecordBean value) {
    return Container(
        decoration: BoxDecoration(
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            border: Border.all(
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                width: 1)),
        child: getItemByNormal(day, value));
  }

  //选中今天
  @override
  Widget getItemByToday(int day, PunchRecordBean value, int selectedDay) {
    Color dayCardColors = ThemeRepository.getInstance().getPrimaryColor_1890FF();
    Color dayCardFontColors = Colors.white;
    Color cardBorderColor = Colors.transparent;
    if(selectedDay == null){
      dayCardColors = Colors.blue;
    }
    // if(selectedDay == day){
    //   dayCardColors = ThemeRepository.getInstance().getCardBgColor_21263C();
    //   cardBorderColor = ThemeRepository.getInstance().getPrimaryColor_1890FF();
    // }
    // if(selectedDay != day && selectedDay != null){注释
    //   dayCardColors = ThemeRepository.getInstance().getCardBgColor_21263C();
    //   dayCardFontColors = ThemeRepository.getInstance().getPrimaryColor_1890FF();
    // }
    BoxDecoration selectedDecoration = BoxDecoration(
        color: dayCardColors,
        border: Border.all(
            color: cardBorderColor,
            width: 1));
    return Container(
      decoration: selectedDecoration,
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: dayCardFontColors,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text((value?.pcnt??0)==0 ?"" :"${value?.pcnt ?? "0"}次",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color:Colors.white,
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  @override
  Widget getItemByDisable(int day, PunchRecordBean value) {
    return Container(
      color: Colors.transparent,
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text("${day ?? "-"}",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 17,
                  fontWeight: FontWeight.w600,
                  height: 1.2)),
          Spacer(flex: 2),
          Container(
            height: 18,
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  @override
  Widget getItemByEmpty(int day, PunchRecordBean value) {
    return Container();
  }

  ///默认显示
  Widget _getDefaultWidget(int day, PunchRecordBean itemBean) {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Column(
        children: <Widget>[
          Spacer(flex: 8),
          Text(
            "${day ?? "-"}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: Colors.white,
                fontSize: 17,
                fontWeight: FontWeight.w600,
                height: 1.2),
          ),
          Spacer(flex: 2),
          Container(
            height: 18,
            child: Center(
              child: Text(
                "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: ThemeRepository.getInstance()
                      .getTextMinorGreyColor_808388(),
                  fontSize: 11,
                ),
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }
}

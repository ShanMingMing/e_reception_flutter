import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/bean/attend_record_calendar_response_bean.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/even/attend_record_calendar_refresh_even.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/calendar_attendance_status_statistics_widget.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/modify_personal_clock_record_widget.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

import 'attend_record_calendar_item_delegate.dart';


// enum AttendRecordCalendarItemType {
//   normal,
//   error,
//   disable,
//   empty,
//   selected,
// }

abstract class AttendRecordCalendarItemBaseDelegate{

  AttendRecordCalendarItemBaseDelegate({this.context,this.onRefresh, this.initDateTime, this.currentDateTime,this.emptyCount,this.setState});
  // BuildContext get context => _context;
  // BuildContext _context;
  // set context(BuildContext ctx) => _context = ctx;

  final BuildContext context;
  ///Stateful更新
  final StateSetter setState;

  ///接口请求）
  final VoidCallback onRefresh;

  ///当前日期
  final DateTime initDateTime;

  ///所在日期
  final DateTime currentDateTime;

  ///占位数
  final int emptyCount;

  int selectedDay;

  // int clickTimes = 1;


  ///显示各状态规则
  Widget getItemWidget(
      int index,PunchRecordBean value) {
    final int day = index - emptyCount + 1;
    //占位
    if(index < emptyCount){
      return getItemByEmpty(day,value);
    }
    //是否是今天
    if(_isToday(day)){
      return _ontapCard(getItemByToday(day,value,selectedDay),value,today: true);
    }
    //选中项
    if(selectedDay == day){
      return _ontapCard(getItemBySelected(day,value),value);
    }
    //是否禁用
    if(_isDisableByOrder(day)){
      return getItemByDisable(day,value);
    }
    //正常模式
    return _ontapCard(getItemByNormal(day,value),value);
  }

  Widget _ontapCard(Widget child,PunchRecordBean value,{bool today}){
  return  Stack(
    children: [
      GestureDetector(
        onTap: (){
          // if(selectedDay==null && (today??false)){//默认当天一次进入
            //单人整日数据列表
            // ModifyPersonalClockRecordWidget.navigatorPush(
            //     context,
            //     // smallCardValue,
            //     // smallStatusCardColor,
            //     VgDateTimeUtils.getStringToIntMillisecond(value?.date?.toString()),
            //     AttendRecordCalendarPageState.of(context).widget?.fuid,
            //     DateTime?.parse(value?.date?.toString()),
            //         () {
            //       VgEventBus.global.send(AttendRecordCalendarRefreshEven());
            //     });
          // }
          // if(selectedDay!=value?.day){
          //   clickTimes = 1;
          // }
          selectedDay = value?.day;
          //选中项的值传入到小横条
          AttendRecordCalendarPageState.of(context).setPunchRecordBean(value);
          // if(clickTimes>=2){
            //单人整日数据列表
            ModifyPersonalClockRecordWidget.navigatorPush(
                context,
                // smallCardValue,
                // smallStatusCardColor,
                VgDateTimeUtils.getStringToIntMillisecond(value?.date?.toString()),
                AttendRecordCalendarPageState.of(context).widget?.fuid,
                DateTime?.parse(value?.date?.toString()),
                    () {
                  VgEventBus.global.send(AttendRecordCalendarRefreshEven());
                });
          // }
          // if(clickTimes<2)clickTimes++;
          setState((){});
        },
        child:  child,
      ),
      showRemarksCorner(value,today:today ?? false)
    ],
  );
  }

  //展示角标
  Widget showRemarksCorner(PunchRecordBean value,{bool today}){
    if(today == null){today =false;}
    bool barkup = false;
    if(value?.backup!=""&&value?.backup!=null){
      barkup = true;
      if(today){
        return Positioned(
            top: 0,
            right: 0,
            child: Image.asset("images/remarks_corner.png",width: 8,color:barkup??false ? Colors.white : Colors.transparent )
        );
      } else{
        return Positioned(
            top: 0,
            right: 0,
            child: Image.asset("images/remarks_corner.png",width: 8,color:barkup??false ? Colors.blue : Colors.transparent )
        );
      }
    }else{
      return Positioned(
          top: 0,
          right: 0,
          child: Image.asset("images/remarks_corner.png",width: 8,color:Colors.transparent )
      );
    }

  }


  ///获取正常状态项
  Widget getItemByNormal(int day,PunchRecordBean value);
  
  ///获取禁止状态项
  Widget getItemByDisable(int day,PunchRecordBean value);
  
  ///获取空状态项
  Widget getItemByEmpty(int day,PunchRecordBean value);

  ///获取选中状态
  Widget getItemBySelected(int day,PunchRecordBean value);


  Widget getItemByToday(int day,PunchRecordBean value,int selectedDay);

  ///是否是禁用状态通过日期比对
  bool _isDisableByOrder(int day){
    if(initDateTime == null || currentDateTime == null){
      return true;
    }
    //未来年
    if(initDateTime.year < currentDateTime.year){
      return true;
    }
    if(initDateTime.year == currentDateTime.year){
      //同年 未来月
      if(initDateTime.month < currentDateTime.month){
        return true;
      }
      //同年 同月 未来日
      if(initDateTime.month == currentDateTime.month && initDateTime.day < day){
        return true;
      }

    }
    return false;
  }

  ///是否是今天
  bool _isToday(int day){
    if(initDateTime == null || currentDateTime == null){
      return false;
    }
    if(initDateTime.year == currentDateTime.year && initDateTime.month == currentDateTime.month && initDateTime.day == day){
      return true;
    }
    return false;
  }


}
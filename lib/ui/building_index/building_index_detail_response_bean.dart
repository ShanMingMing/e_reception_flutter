/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"comWbrand":{"id":"c291fbf5f989471c854181eaa5a3dd43","companyid":"f7a8f0bc664c4b91af42c11057bf08cb","syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","picurl":"http://etpic.we17.com/template/pic/20220110095548.png","wtitle":"哈哈哈","upflg":"01","usnum":3,"uenum":33,"dflg":"01","dsnum":1,"denum":4,"createuid":"53f3513b69464e9b826516be611acbb6","createtime":"2022-01-11 16:09:29","updatetime":"2022-01-11 16:09:29"},"upList":[],"downList":[]}
/// extra : null

class BuildingIndexDetailResponseBean {
  bool success;
  String code;
  String msg;
  BuildingIndexDetailBean data;
  dynamic extra;

  static BuildingIndexDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexDetailResponseBean buildingIndexDetailResponseBeanBean = BuildingIndexDetailResponseBean();
    buildingIndexDetailResponseBeanBean.success = map['success'];
    buildingIndexDetailResponseBeanBean.code = map['code'];
    buildingIndexDetailResponseBeanBean.msg = map['msg'];
    buildingIndexDetailResponseBeanBean.data = BuildingIndexDetailBean.fromMap(map['data']);
    buildingIndexDetailResponseBeanBean.extra = map['extra'];
    return buildingIndexDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// comWbrand : {"id":"c291fbf5f989471c854181eaa5a3dd43","companyid":"f7a8f0bc664c4b91af42c11057bf08cb","syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","picurl":"http://etpic.we17.com/template/pic/20220110095548.png","wtitle":"哈哈哈","upflg":"01","usnum":3,"uenum":33,"dflg":"01","dsnum":1,"denum":4,"createuid":"53f3513b69464e9b826516be611acbb6","createtime":"2022-01-11 16:09:29","updatetime":"2022-01-11 16:09:29"}
/// upList : []
/// downList : []

class BuildingIndexDetailBean {
  ComWbrandBean comWbrand;
  List<FloorCompanyListBean> upList;
  List<FloorCompanyListBean> downList;

  static BuildingIndexDetailBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexDetailBean dataBean = BuildingIndexDetailBean();
    dataBean.comWbrand = ComWbrandBean.fromMap(map['comWbrand']);
    dataBean.upList = List()..addAll(
        (map['upList'] as List ?? []).map((o) => FloorCompanyListBean.fromMap(o))
    );
    dataBean.downList = List()..addAll(
        (map['downList'] as List ?? []).map((o) => FloorCompanyListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "comWbrand": comWbrand,
    "upList": upList,
    "downList": downList,
  };
}

class FloorCompanyListBean {
  String fnum;//楼层号
  String floorflg;//01地下 02地上
  String company;//公司名
  String id;
  String comwid;
  String hnum = "";
  FloorCompanyListBean({this.fnum, this.floorflg}); //门牌号

  static FloorCompanyListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FloorCompanyListBean downListBean = FloorCompanyListBean();
    downListBean.fnum = map['fnum'];
    downListBean.floorflg = map['floorflg'];
    downListBean.company = map['company'];
    downListBean.id = map['id'];
    downListBean.comwid = map['comwid'];
    downListBean.hnum = map['hnum'];
    return downListBean;
  }

  Map toJson() => {
    "fnum": fnum,
    "floorflg": floorflg,
    "company": company,
    "id": id,
    "comwid": comwid,
    "hnum": hnum,
  };
}

/// id : "c291fbf5f989471c854181eaa5a3dd43"
/// companyid : "f7a8f0bc664c4b91af42c11057bf08cb"
/// syswid : "1"
/// htmlurl : "http://etpic.we17.com/test/20220110094429_8649.html"
/// picurl : "http://etpic.we17.com/template/pic/20220110095548.png"
/// wtitle : "哈哈哈"
/// upflg : "01"
/// usnum : 3
/// uenum : 33
/// dflg : "01"
/// dsnum : 1
/// denum : 4
/// createuid : "53f3513b69464e9b826516be611acbb6"
/// createtime : "2022-01-11 16:09:29"
/// updatetime : "2022-01-11 16:09:29"

class ComWbrandBean {
  String id;
  String companyid;
  String syswid;
  String htmlurl;
  String picurl;
  String wtitle;
  String upflg;
  int usnum;
  int uenum;
  String dflg;
  int dsnum;
  int denum;
  String createuid;
  String createtime;
  String updatetime;

  static ComWbrandBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComWbrandBean comWbrandBean = ComWbrandBean();
    comWbrandBean.id = map['id'];
    comWbrandBean.companyid = map['companyid'];
    comWbrandBean.syswid = map['syswid'];
    comWbrandBean.htmlurl = map['htmlurl'];
    comWbrandBean.picurl = map['picurl'];
    comWbrandBean.wtitle = map['wtitle'];
    comWbrandBean.upflg = map['upflg'];
    comWbrandBean.usnum = map['usnum'];
    comWbrandBean.uenum = map['uenum'];
    comWbrandBean.dflg = map['dflg'];
    comWbrandBean.dsnum = map['dsnum'];
    comWbrandBean.denum = map['denum'];
    comWbrandBean.createuid = map['createuid'];
    comWbrandBean.createtime = map['createtime'];
    comWbrandBean.updatetime = map['updatetime'];
    return comWbrandBean;
  }

  Map toJson() => {
    "id": id,
    "companyid": companyid,
    "syswid": syswid,
    "htmlurl": htmlurl,
    "picurl": picurl,
    "wtitle": wtitle,
    "upflg": upflg,
    "usnum": usnum,
    "uenum": uenum,
    "dflg": dflg,
    "dsnum": dsnum,
    "denum": denum,
    "createuid": createuid,
    "createtime": createtime,
    "updatetime": updatetime,
  };
}
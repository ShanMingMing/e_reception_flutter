import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'building_index_diy_detail_page.dart';
import 'building_index_terminal_list_response_bean.dart';
import 'building_index_view_model.dart';

///楼层指示牌选择播放终端页面
class BuildingIndexSelectTerminalPage extends StatefulWidget{
  ///路由名称
  static const String ROUTER = "BuildingIndexSelectTerminalPage";
  final String buildingIndexId;
  final String hsns;
  final List<HsnListBean> terminalList;

  const BuildingIndexSelectTerminalPage({Key key, this.buildingIndexId,
    this.hsns, this.terminalList}):super(key: key);

  @override
  _BuildingIndexSelectTerminalPageState createState() => _BuildingIndexSelectTerminalPageState();

  ///跳转方法
  static Future<String> navigatorPush(
      BuildContext context, String buildingIndexId, String hsns, List<HsnListBean> terminalList) {
    return RouterUtils.routeForFutureResult(
      context, BuildingIndexSelectTerminalPage(
      buildingIndexId: buildingIndexId,
      hsns: hsns,
      terminalList: terminalList,
    ),
      routeName: BuildingIndexSelectTerminalPage.ROUTER,
    );
  }
}

class _BuildingIndexSelectTerminalPageState extends BaseState<BuildingIndexSelectTerminalPage>{
  bool _selectAll = false;
  List<HsnListBean> data;
  String _buildingIndexId;
  String _hsns;
  String _originHsns;
  BuildingIndexViewModel _viewModel;
  String _searchStr;
  @override
  void initState() {
    super.initState();
    _viewModel = BuildingIndexViewModel(this);
    _viewModel.getTerminalList(_searchStr);
    _buildingIndexId = widget?.buildingIndexId;
    data = new List();
    _hsns = widget.hsns??"";
    _originHsns = _hsns;
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: false,
          backgroundColor: Color(0xFFF6F7F9),
          body: _toPlaceHolderWidget(),
        )
    );
  }

  ///标题栏
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "请选择",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      backgroundColor: Colors.white,
    );
  }

  ///搜索
  Widget _toSearchWidget(){
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          SizedBox(height: 10),
          CommonSearchBarWidget(
            hintText: "搜索",
            onSubmitted: (String searchStr) {
              _searchStr = searchStr;
              _viewModel.getTerminalList(_searchStr);
            },
            bgColor: Color(0xFFF1F4F8),
            textColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          ),
        ],
      ),
    );
  }

  Widget _toPlaceHolderWidget(){
    return ValueListenableBuilder(
        valueListenable: _viewModel?.statusTypeValueNotifier,
        builder:
            (BuildContext context, PlaceHolderStatusType value, Widget child) {
          return VgPlaceHolderStatusWidget(
            loadingStatus: value == PlaceHolderStatusType.loading,
            errorStatus: value == PlaceHolderStatusType.error,
            errorOnClick: () => _viewModel.getTerminalList(_searchStr),
            loadingOnClick: () => _viewModel.getTerminalList(_searchStr),
            child: child,
          );
        },
        child: _toBodyWidget());
  }

  Widget _toBodyWidget(){
    return ValueListenableBuilder(
        valueListenable: _viewModel?.buildingIndexTerminalValueNotifier,
        builder: (BuildContext context, BuildingIndexTerminalDataBean detailBean, Widget child){
          if(detailBean != null && detailBean.hsnList != null){
            data.clear();
            data.addAll(detailBean.hsnList);
          }
          if(StringUtils.isNotEmpty(_hsns??"")){
            List<String> hsnList = _hsns.split(",");
            int count = 0;
            if(hsnList != null && hsnList.length > 0){
              data.forEach((element) {
                if(hsnList.contains(element.hsn)){
                  element.selectStatus = true;
                  count++;
                }
              });
            }
            if(count == data?.length??0){
              _selectAll = true;
            }
          }
          return CommonPackUpKeyboardWidget(
            child: Column(
              children: <Widget>[
                _toTopBarWidget(),
                _toSearchWidget(),
                Expanded(
                  child: _toListWidget(),
                ),
                _toConfirmWidget(),
              ],
            ),
          );
        }
    );
  }

  ///图片展示栏
  Widget _toListWidget(){

    return ListView.separated(
      padding: EdgeInsets.only(top: 0, bottom: 50),
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        return _toListItemWidget(context, index, data?.elementAt(index));
      },
      separatorBuilder: (BuildContext context, int index){
        return SizedBox();
      },
      itemCount: data?.length??0,
    );
  }

  ///item
  Widget _toListItemWidget(BuildContext context, int index, HsnListBean itemBean){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: ()  {
        setState(() {
          itemBean.selectStatus = !(itemBean.selectStatus??false);
          _selectAll = _getSelectedCount() == data.length;
          _hsns = _getHsns();
        });
      },
      child: Container(
        height: 87,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Image.asset(
                itemBean.selectStatus??false
                    ? "images/common_selected_ico.png"
                    : "images/common_unselect_ico.png",
                height: 20,
              ),
            ),

            Expanded(child: _toContentWidget(index, itemBean)),
          ],
        ),
      ),
    );
  }

  Widget _toContentWidget(int index, HsnListBean itemBean) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: [
              Visibility(
                visible: (StringUtils.isEmpty(itemBean?.terminalName) && StringUtils.isEmpty(itemBean?.sideNumber)&& StringUtils.isNotEmpty(itemBean?.hsn)),
                child: Text(
                  // "硬件ID：",
                  "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                    fontSize: 15,
                    // fontWeight: FontWeight.w600
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  itemBean?.getTerminalName(index),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                    fontSize: 15,
                    // fontWeight: FontWeight.w600
                  ),
                ),
              ),
              Spacer(),
              Visibility(
                child: Container(
                  alignment: Alignment.centerRight,
                  width: 60,
                  child: Text(
                    (itemBean.onOff == null || itemBean.onOff !=1 || itemBean.getTerminalIsOff())?"已关机":"开机状态",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: (itemBean.onOff == null || itemBean.onOff !=1 || itemBean.getTerminalIsOff())
                          ?Color(0xFF8B93A5)
                          :ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            width: 236,
            child: Text(
              showOriginEmptyStr(itemBean?.position) ?? itemBean?.address ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Color(0xFF8B93A5),
                fontSize: 12,
              ),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            "管理员：${itemBean?.manager ?? "-"}${_getLoginStr(itemBean?.loginName) ?? ""}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Color(0xFF8B93A5),
              fontSize: 12,
            ),
          )
        ],
      ),
    );
  }

  String _getLoginStr(String loginName) {
    if (loginName == null || loginName == "") {
      return null;
    }
    return "（$loginName账号登录）";
  }

  int _getSelectedCount(){
    int count = 0;
    if(data != null){
      data.forEach((element) {
        if(element.selectStatus??false){
          count++;
        }
      });
    }
    return count;
  }

  ///确定
  Widget _toConfirmWidget(){
    double bottomH = ScreenUtils.getBottomBarH(context);
    return Container(
      height: 56 + bottomH,
      padding: EdgeInsets.only(bottom: bottomH),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: (){
              setState(() {
                data.forEach((element) {
                  element.selectStatus = !_selectAll;
                });
                _selectAll = !_selectAll;
                _hsns = _getHsns();
              });
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                _selectAll?"全不选":"全选",
                style: TextStyle(
                  fontSize: 15,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                ),
              ),
            ),
          ),
          Visibility(
            visible: _getSelectedCount() > 0,
            child: Text(
              "已选${_getSelectedCount()}终端",
              style: TextStyle(
                fontSize: 12,
                color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              ),
            ),
          ),
          Spacer(),
          CommonFixedHeightConfirmButtonWidget(
            isAlive: _originHsns != _hsns,
            height: 36,
            width: 90,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            unSelectBgColor: Color(0xFFCFD4DB),
            selectedBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            unSelectTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
            selectedTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 15,
              // fontWeight: FontWeight.w600
            ),
            text: "确定",
            onTap: ()async{
              if(_originHsns == _hsns){
                return;
              }
              if(StringUtils.isNotEmpty(_hsns) && _needHint()){
                bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                    title: "提示",
                    content: "显示屏原有指示牌将被替换，确定继续？",
                    cancelText: "取消",
                    confirmText: "确定",
                  confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  cancelBgColor: Color(0xFFF6F7F9),
                  titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  widgetBgColor: Colors.white,
                );
                if (result ?? false) {
                  _viewModel.pushBuildingIndex(context, _buildingIndexId, _hsns, (){
                    RouterUtils.popUntil(context, BuildingIndexDiyDetailPage.ROUTER);
                  });
                }
                return;
              }
              _viewModel.pushBuildingIndex(context, _buildingIndexId, _hsns, (){
                RouterUtils.popUntil(context, BuildingIndexDiyDetailPage.ROUTER);
              });
              // RouterUtils.pop(context, result: _getHsns());
            },
          ),
        ],
      ),
    );
  }

  String _getHsns(){
    String hsns = "";
    data.forEach((element) {
      if(element.selectStatus??false){
        hsns += element.hsn;
        hsns += ",";
      }
    });
    if(StringUtils.isNotEmpty(hsns)){
      hsns = hsns.substring(0, hsns.length-1);
    }
    return hsns;
  }

  bool _needHint(){
    for(int i = 0; i < data.length; i++){
      if((data[i].selectStatus??false)
          && StringUtils.isNotEmpty(data[i].comwid)
          && _buildingIndexId != data[i].comwid){
        return true;
      }
    }
    return false;
  }


}
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'building_index_detail_response_bean.dart';

class FloorCompanyItemWidget extends StatefulWidget{
  FloorCompanyListBean companyListBean;
  Function(FloorCompanyListBean companyListBean) onDelete;
  Function(FloorCompanyListBean companyListBean, TapUpDetails details) onTapUp;
  String resource;
  FloorCompanyItemWidget({Key key, this.companyListBean, this.onDelete, this.onTapUp, this.resource}) : super(key: key);

  @override
  _FloorCompanyItemWidgetState createState() => _FloorCompanyItemWidgetState();
}

class _FloorCompanyItemWidgetState extends State<FloorCompanyItemWidget>{
  TextEditingController _houseNumberController;
  TextEditingController _companyNameController;
  VoidCallback _listener;
  @override
  void initState() {
    super.initState();
    // _listener = () => _notifyChange();


  }


  ///通知更新
  _notifyChange() {
    if(widget == null){
      return;
    }
    // widget?.companyListBean?.hnum = _houseNumberController?.text?.trim();
    // widget?.companyListBean?.company = _companyNameController?.text?.trim();
    setState(() { });
  }

  @override
  Widget build(BuildContext context) {
    _houseNumberController = TextEditingController(text: widget?.companyListBean?.hnum??"");
    _companyNameController = TextEditingController(text: widget?.companyListBean?.company??"");
    return Container(
      height: 50,
      color: Colors.white,
      alignment: Alignment.center,
      child: Row(
        children: [
          Container(
            width: 110,
            height: 50,
            padding: EdgeInsets.only(left: 15),
            alignment: Alignment.center,
            child: VgTextField(
              controller: _houseNumberController,
              keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
              style: TextStyle(
                  color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  fontSize: 14),
              decoration: new InputDecoration(
                  counterText: "",
                  hintText: "门牌号/选填",
                  contentPadding: EdgeInsets.all(0),
                  border: InputBorder.none,
                  hintStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0XFFB0B3BF))),
              // inputFormatters: <TextInputFormatter> [
              //   LengthLimitingTextInputFormatter(10)
              // ],
              maxLines: 2,
              minLines: 1,
              onChanged: (value){
                widget?.companyListBean?.hnum = value.trim();
              },
              maxLimitLength: 10,
              zhCountEqEnCount: true,
              limitCallback:(int){
                VgToastUtils.toast(context, "最多可输入10字");
              },
              // minLines: 1,
            ),
          ),
          Container(
            height: 20,
            width: 1,
            color: Color(0xFFEEEEEE),
          ),
          SizedBox(width: 15,),
          Expanded(
            child: VgTextField(
              controller: _companyNameController,
              keyboardType: TextInputType.text,
              style: TextStyle(
                color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                fontSize: 14,
              ),
              decoration: new InputDecoration(
                  counterText: "",
                  hintText: "企业名称",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(0),
                  hintStyle: TextStyle(
                      fontSize: 14,
                      color: Color(0XFFB0B3BF))
              ),
              // inputFormatters: <TextInputFormatter> [
              //   LengthLimitingTextInputFormatter(30)
              // ],
              maxLines: 2,
              minLines: 1,
              onChanged: (value){
                widget?.companyListBean?.company = value.trim();
              },
              maxLimitLength: 30,
              zhCountEqEnCount: true,
              limitCallback:(int){
                VgToastUtils.toast(context, "最多可输入30字");
              },
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: (){
              if(widget?.onDelete != null){
                widget.onDelete.call(widget?.companyListBean);
                widget?.companyListBean?.hnum = "";
                widget?.companyListBean?.company = "";
              }
            },
            onTapUp: (details){
              if(widget?.onTapUp != null){
                widget.onTapUp.call(widget?.companyListBean, details);
                // widget?.companyListBean?.hnum = "";
                // widget?.companyListBean?.company = "";
              }
            },
            child: Container(
              height: 50,
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Container(
                width: 9,
                height: 9,
                alignment: Alignment.center,
                child: Image.asset(
                  'images/${widget?.resource??"login_close_ico"}.png',
                  color: Color(0xFF959CAC),
                  width: ("icon_edit_floor_company_info" == widget?.resource)?3:9,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // _companyNameController?.removeListener(_listener);
    // _houseNumberController?.removeListener(_listener);
    // _companyNameController?.clear();
    // _houseNumberController?.clear();
    // _companyNameController?.dispose();
    // _houseNumberController?.dispose();
    super.dispose();
  }

}
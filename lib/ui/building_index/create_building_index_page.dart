import 'dart:async';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/singleton/constant_repository/constant_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_view_model.dart';
import 'package:e_reception_flutter/ui/building_index/create_floor_company_page.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/system_ui_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/light_annotated_region_style.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'building_index_detail_response_bean.dart';
import 'building_index_diy_floor_settings_page.dart';

/// 创建楼层指示牌
class CreateBuildingIndexPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "CreateBuildingIndexPage";
  //创建还是编辑 00创建 01编辑
  final String createOrEdit;
  //模板id
  final String moduleId;
  //编辑用的企业信息
  final ComWbrandBean comWbrand;
  final List<String> upFloorNumList;
  final List<String> downFloorNumList;
  const CreateBuildingIndexPage(
      {Key key, this.createOrEdit, this.moduleId, this.comWbrand,
        this.upFloorNumList, this.downFloorNumList}) : super(key:key);

  @override
  _CreateBuildingIndexPageState createState() => _CreateBuildingIndexPageState();
  static Future<dynamic> navigatorPush(BuildContext context,
      String createOrEdit, String moduleId, {ComWbrandBean comWbrand,
        List<String> upFloorNumList, List<String> downFloorNumList}){
    return RouterUtils.routeForFutureResult(
        context,
        CreateBuildingIndexPage(
          createOrEdit: createOrEdit,
          moduleId: moduleId,
          comWbrand: comWbrand,
          upFloorNumList: upFloorNumList,
          downFloorNumList: downFloorNumList,
        ),
        routeName: CreateBuildingIndexPage.ROUTER
    );
  }
}

class _CreateBuildingIndexPageState
    extends BaseState<CreateBuildingIndexPage> {

  TextEditingController _buildingIndexTitleController;
  BuildingIndexViewModel _viewModel;

  //指示牌标题
  String _title;
  //地上层打开标志 默认打开
  String _upOpenFlag = "01";
  //地下层打开标志 默认关闭
  String _downOpenFlag = "00";
  //地上层默认起始楼数 1F
  String _upStartFloor = "1F";
  //地上层默认结束楼数 空
  String _upEndFloor;
  //地下层默认起始楼数 空
  String _downStartFloor = "B1";
  //地下层默认结束楼数 空
  String _downEndFloor;
  //已有指示牌信息
  ComWbrandBean _comWbrand;

  @override
  void initState() {
    super.initState();
    _comWbrand = widget?.comWbrand;
    _viewModel = BuildingIndexViewModel(this);
    if(_comWbrand != null){
      _buildingIndexTitleController = TextEditingController(text: _comWbrand.wtitle??"");
      _buildingIndexTitleController.addListener(() {
        _notifyChange();
      });
      if(StringUtils.isNotEmpty(_comWbrand.wtitle)){
        _title = _comWbrand.wtitle;
      }
      if(StringUtils.isNotEmpty(_comWbrand.upflg)){
        _upOpenFlag = _comWbrand.upflg;
      }
      if(StringUtils.isNotEmpty(_comWbrand.dflg)){
        _downOpenFlag = _comWbrand.dflg;
      }
      if(_comWbrand.usnum != null && _comWbrand.usnum != 0){
        _upStartFloor = "${_comWbrand.usnum}F";
      }
      if(_comWbrand.uenum != null && _comWbrand.uenum != 0 ){
        _upEndFloor = "${_comWbrand.uenum}F";
      }
      if(_comWbrand.dsnum != null && _comWbrand.dsnum != 0){
        _downStartFloor = "B${_comWbrand.dsnum}";
      }
      if(_comWbrand.denum != null && _comWbrand.denum != 0){
        _downEndFloor = "B${_comWbrand.denum}";
      }
    }else{
      _buildingIndexTitleController = TextEditingController()
        ..addListener(() => _notifyChange());
    }

  }

  @override
  void dispose() {
    _buildingIndexTitleController.clear();
    _buildingIndexTitleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LightAnnotatedRegion(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Color(0xFFF6F7F9),
          body: _toMainColumnWidget(),
        )
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: [
        _toTopBarWidget(),
        _toBuildingIndexTitleWidget(),
        SizedBox(height: 10,),
        _toBuildingSettingsWidget(),
        SizedBox(height: 30,),
        _toCreateWidget(),
      ],
    );
  }

  ///页面标题
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      isShowBack: true,
      title: "楼层指示牌",
      titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
    );
  }

  ///指示牌标题
  Widget _toBuildingIndexTitleWidget(){
    return Container(
      height: 76,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 26,
            margin: EdgeInsets.only(left: 15),
            alignment: Alignment.bottomLeft,
            child: Text(
              "指示牌标题：",
              style: TextStyle(
                  fontSize: 11,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  fontWeight: FontWeight.w600
              ),
            ),
          ),
          Container(
            height: 50,
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Container(
              alignment: Alignment.centerLeft,
              child: VgTextField(
                controller: _buildingIndexTitleController,
                keyboardType: TextInputType.text,
                autofocus: true,
                style: TextStyle(
                    color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                    fontSize: 14),
                decoration: new InputDecoration(
                    counterText: "",
                    hintText: "如：XX科技大厦A栋（30字内）",
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                        fontSize: 14,
                        color: Color(0XFFB0B3BF))),
                inputFormatters: <TextInputFormatter> [
                  LengthLimitingTextInputFormatter(30)
                ],
                maxLines: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///楼层设置
  Widget _toBuildingSettingsWidget(){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          Container(
            height: 26,
            alignment: Alignment.bottomLeft,
            child: Text(
              "楼层设置：",
              style: TextStyle(
                  fontSize: 11,
                  color: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  fontWeight: FontWeight.w600
              ),
            ),
          ),
          Container(
            height: 60,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: (){
                    setState(() {
                      if("01" == _upOpenFlag){
                        _upOpenFlag = "00";
                      }else{
                        _upOpenFlag = "01";
                      }
                    });
                  },
                  child: Image.asset(
                    ("01" == _upOpenFlag)
                        ? "images/icon_open.png"
                        :"images/icon_close_setting_floor.png",
                    width: 36,
                  ),
                ),
                SizedBox(width: 10,),
                Text(
                  "地上层",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                    fontSize: 14,
                  ),
                ),
                Visibility(
                  visible: "01" == _upOpenFlag,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(width: 15,),
                      _toSelectUpStartFloor(),
                      SizedBox(width: 10,),
                      Text(
                        "至",
                        style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                        ),
                      ),
                      SizedBox(width: 10,),
                      _toSelectUpEndFloor(),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 60,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: (){
                    setState(() {
                      if("01" == _downOpenFlag){
                        _downOpenFlag = "00";
                      }else{
                        _downOpenFlag = "01";
                      }
                    });
                  },
                  child: Image.asset(
                    ("01" == _downOpenFlag)
                        ? "images/icon_open.png"
                        :"images/icon_close_setting_floor.png",
                    width: 36,
                  ),
                ),
                SizedBox(width: 10,),
                Text(
                  "地下层",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
                    fontSize: 14,
                  ),
                ),
                Visibility(
                  visible: "01" == _downOpenFlag,
                  child: Row(
                    children: [
                      SizedBox(width: 15,),
                      _toSelectDownStartFloor(),
                      SizedBox(width: 10,),
                      Text(
                        "至",
                        style: TextStyle(
                            fontSize: 14,
                            color: ThemeRepository.getInstance().getHintGreenColor_5E687C()
                        ),
                      ),
                      SizedBox(width: 10,),
                      _toSelectDownEndFloor(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  //动态获取楼上层起始楼层的最小值
  int getUpStartMinIndex(){
    int startMinIndex = 1;
    // if(_comWbrand != null && _comWbrand.usnum != null && startMinIndex < _comWbrand.usnum){
    //   startMinIndex = _comWbrand.usnum;
    // }
    return startMinIndex;
  }

  //动态获取楼上层起始楼层的最大值
  int getUpStartMaxValue(){
    int startMaxIndex = 120;
    // if(_comWbrand != null && _comWbrand.uenum != null && startMaxIndex > _comWbrand.uenum){
    //   startMaxIndex = _comWbrand.uenum;
    // }
    if(StringUtils.isNotEmpty(_upEndFloor)){
      startMaxIndex = int.parse(_upEndFloor.replaceAll("F", ""));
    }
    return startMaxIndex;
  }


  ///选择地上开始层
  Widget _toSelectUpStartFloor(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        return SelectUtil.showListSelectDialog(
            context: context,
            title: "选择楼层",
            positionStr: _upStartFloor,
            textList: getUpFloor(getUpStartMinIndex(), getUpStartMaxValue()),
            onSelect: (String value) {
              int floor = int.parse(value.replaceAll("F", ""));
              int end = 0;
              if(StringUtils.isNotEmpty(_upEndFloor)){
                end = int.parse(_upEndFloor.replaceAll("F", ""));
              }
              if(end >0 && floor > end){
                VgToastUtils.toast(context, "请选择正确的起始楼层");
                return;
              }
              _upStartFloor = value;
              setState(() {});
            },
          bgColor: Colors.white,
          titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          itemColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        );
      },
      child: Container(
        width: 90,
        height: 60,
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      StringUtils.isNotEmpty(_upStartFloor)?_upStartFloor:"请选择",
                      style: TextStyle(
                          fontSize: 14,
                          color: StringUtils.isNotEmpty(_upStartFloor)
                              ?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                              :Color(0xFFB0B3BF)
                      ),
                    ),
                  ),
                ),
                Image.asset("images/icon_arrow_down.png",width: 11,)
              ],
            ),
            Container(
              width: 90,
              height: 1,
              color: Color(0xFFCFD4D8),
            ),
          ],
        ),
      ),
    );
  }


  //动态获取楼上层结束楼层的最小值
  int getUpEndMinIndex(){
    int startMinIndex = 1;
    // if(_comWbrand != null && _comWbrand.usnum != null && startMinIndex < _comWbrand.usnum){
    //   startMinIndex = _comWbrand.usnum;
    // }
    if(StringUtils.isNotEmpty(_upStartFloor)){
      startMinIndex = int.parse(_upStartFloor.replaceAll("F", ""));
    }
    return startMinIndex;
  }

  //动态获取楼上层结束楼层的最大值
  int getUpEndMaxValue(){
    int startMaxIndex = 120;
    // if(_comWbrand != null && _comWbrand.uenum != null && startMaxIndex > _comWbrand.uenum){
    //   startMaxIndex = _comWbrand.uenum;
    // }
    return startMaxIndex;
  }

  ///选择地上结束层
  Widget _toSelectUpEndFloor(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        return SelectUtil.showListSelectDialog(
            context: context,
            title: "选择楼层",
            positionStr: StringUtils.isNotEmpty(_upEndFloor)?_upEndFloor:"20F",
            textList: getUpFloor(getUpEndMinIndex(), getUpEndMaxValue()).reversed.toList(),
            onSelect: (dynamic value) {
              int floor = int.parse(value.replaceAll("F", ""));
              int start = 0;
              if(StringUtils.isNotEmpty(_upStartFloor)){
                start = int.parse(_upStartFloor.replaceAll("F", ""));
              }
              if(start>0 && floor < start){
                VgToastUtils.toast(context, "请选择正确的结束楼层");
                return;
              }
              _upEndFloor = value;
              setState(() {});
            },
          bgColor: Colors.white,
          titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          itemColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        );
      },
      child: Container(
        width: 90,
        height: 60,
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      StringUtils.isNotEmpty(_upEndFloor)?_upEndFloor:"请选择",
                      style: TextStyle(
                          fontSize: 14,
                          color: StringUtils.isNotEmpty(_upEndFloor)
                              ?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                              :Color(0xFFB0B3BF)
                      ),
                    ),
                  ),
                ),
                Image.asset("images/icon_arrow_down.png",width: 11,)
              ],
            ),
            Container(
              width: 90,
              height: 1,
              color: Color(0xFFCFD4D8),
            ),
          ],
        ),
      ),
    );
  }

  //动态获取楼下层起始楼层的最小值
  int getDownStartMinIndex(){
    int startMinIndex = 1;
    // if(_comWbrand != null && _comWbrand.dsnum != null && startMinIndex < _comWbrand.dsnum){
    //   startMinIndex = _comWbrand.dsnum;
    // }
    return startMinIndex;
  }

  //动态获取楼下层起始楼层的最大值
  int getDownStartMaxValue(){
    int startMaxIndex = 4;
    // if(_comWbrand != null && _comWbrand.denum != null && startMaxIndex > _comWbrand.denum){
    //   startMaxIndex = _comWbrand.denum;
    // }
    if(StringUtils.isNotEmpty(_downEndFloor)){
      startMaxIndex = int.parse(_downEndFloor.replaceAll("B", ""));
    }
    return startMaxIndex;
  }

  ///选择地下开始层
  Widget _toSelectDownStartFloor(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        return SelectUtil.showListSelectDialog(
            context: context,
            title: "选择楼层",
            positionStr: _downStartFloor,
            textList: getDownFloor(getDownStartMinIndex(), getDownStartMaxValue()),
            onSelect: (dynamic value) {
              int floor = int.parse(value.replaceAll("B", ""));
              int end = 0;
              if(StringUtils.isNotEmpty(_downEndFloor)){
                end = int.parse(_downEndFloor.replaceAll("B", ""));
              }
              if(end > 0 && floor > end){
                VgToastUtils.toast(context, "请选择正确的起始楼层");
                return;
              }
              _downStartFloor = value;
              setState(() {});
            },
          bgColor: Colors.white,
          titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          itemColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        );
      },
      child: Container(
        width: 90,
        height: 60,
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      StringUtils.isNotEmpty(_downStartFloor)?_downStartFloor:"请选择",
                      style: TextStyle(
                          fontSize: 14,
                          color: StringUtils.isNotEmpty(_downStartFloor)
                              ?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                              :Color(0xFFB0B3BF)
                      ),
                    ),
                  ),
                ),
                Image.asset("images/icon_arrow_down.png",width: 11,)
              ],
            ),
            Container(
              width: 90,
              height: 1,
              color: Color(0xFFCFD4D8),
            ),
          ],
        ),
      ),
    );
  }


  //动态获取楼下层结束楼层的最小值
  int getDownEndMinIndex(){
    int startMinIndex = 1;
    // if(_comWbrand != null && _comWbrand.dsnum != null && startMinIndex < _comWbrand.dsnum){
    //   startMinIndex = _comWbrand.dsnum;
    // }
    if(StringUtils.isNotEmpty(_downStartFloor)){
      startMinIndex = int.parse(_downStartFloor.replaceAll("B", ""));
    }
    return startMinIndex;
  }

  //动态获取楼下层结束楼层的最大值
  int getDownEndMaxValue(){
    int startMaxIndex = 4;
    // if(_comWbrand != null && _comWbrand.denum != null && startMaxIndex > _comWbrand.denum){
    //   startMaxIndex = _comWbrand.denum;
    // }
    return startMaxIndex;
  }

  ///选择地下结束层
  Widget _toSelectDownEndFloor(){
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        return SelectUtil.showListSelectDialog(
            context: context,
            title: "选择楼层",
            positionStr: StringUtils.isNotEmpty(_downEndFloor)?_downEndFloor:"B4",
            textList: getDownFloor(getDownEndMinIndex(), getDownEndMaxValue()),
            onSelect: (dynamic value) {
              int floor = int.parse(value.replaceAll("B", ""));
              int start = 0;
              if(StringUtils.isNotEmpty(_downStartFloor)){
                start = int.parse(_downStartFloor.replaceAll("B", ""));
              }
              if(start> 0 && floor < start){
                VgToastUtils.toast(context, "请选择正确的结束楼层");
                return;
              }
              _downEndFloor = value;
              setState(() {});
            },
          bgColor: Colors.white,
          titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
          itemColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
        );
      },
      child: Container(
        width: 90,
        height: 60,
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      StringUtils.isNotEmpty(_downEndFloor)?_downEndFloor:"请选择",
                      style: TextStyle(
                          fontSize: 14,
                          color: StringUtils.isNotEmpty(_downEndFloor)
                              ?ThemeRepository.getInstance().getPrimaryColor_1890FF()
                              :Color(0xFFB0B3BF)
                      ),
                    ),
                  ),
                ),
                Image.asset("images/icon_arrow_down.png",width: 11,)
              ],
            ),
            Container(
              width: 90,
              height: 1,
              color: Color(0xFFCFD4D8),
            ),
          ],
        ),
      ),
    );
  }

  ///通知更新
  _notifyChange() {
    _title = _buildingIndexTitleController?.text?.trim();
    setState(() { });
  }

  int getUpStartParam(){
    if("01" == _upOpenFlag && StringUtils.isNotEmpty(_upStartFloor)){
      return int.parse(_upStartFloor.replaceAll("F", ""));
    }
    return null;
  }

  int getUpEndParam(){
    if("01" == _upOpenFlag && StringUtils.isNotEmpty(_upEndFloor)){
      return int.parse(_upEndFloor.replaceAll("F", ""));
    }
    return null;
  }

  int getDownStartParam(){
    if("01" == _downOpenFlag && StringUtils.isNotEmpty(_downStartFloor)){
      return int.parse(_downStartFloor.replaceAll("B", ""));
    }
    return null;
  }

  int getDownEndParam(){
    if("01" == _downOpenFlag && StringUtils.isNotEmpty(_downEndFloor)){
      return int.parse(_downEndFloor.replaceAll("B", ""));
    }
    return null;
  }

  ///创建
  Widget _toCreateWidget(){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: ()async{
            //无标题不能保存
            if(StringUtils.isEmpty(_title)){
              VgToastUtils.toast(context, "请输入指示牌标题");
              return;
            }
            //地上层打开，起止楼层都得有才能保存
            if("01" == _upOpenFlag){
              if(StringUtils.isEmpty(_upStartFloor) || StringUtils.isEmpty(_upEndFloor)){
                VgToastUtils.toast(context, "请完善楼层信息");
                return;
              }
            }
            //地下层打开，起止楼层都得有才能保存
            if("01" == _downOpenFlag){
              if(StringUtils.isEmpty(_downStartFloor) || StringUtils.isEmpty(_downEndFloor)){
                VgToastUtils.toast(context, "请完善楼层信息");
                return;
              }
            }
            if("00" == widget?.createOrEdit){
              //创建
              _viewModel.createBuildingIndex(context, widget?.moduleId, _title,
                  _upOpenFlag, _downOpenFlag, (indexId){
                    RouterUtils.pop(context);
                    //来源 00我制作的详情，模板制作01
                    BuildingIndexDiyFloorSettingsPage.navigatorPush(context, indexId, "01");
                  },
                  upStart: getUpStartParam(), upEnd: getUpEndParam(),
                  downStart: getDownStartParam(), downEnd: getDownEndParam()
              );
            }else{
              //编辑要判断楼层是否有缩减
              int upStart = getUpStartParam();
              int upEnd = getUpEndParam();
              int downStart = getUpEndParam();
              int downEnd = getUpEndParam();
              bool upHint = needUpFloorHint(upStart, upEnd);
              bool downHint = needDownFloorHint(downStart, downEnd);
              if(upHint || downHint){
                bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
                  title: "提示",
                  content: "修改楼层范围后，范围外楼层的企业信息将被删除，确认修改",
                  cancelText: "取消",
                  confirmText: "确认",
                  confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                  cancelBgColor: Colors.white,
                  titleColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  contentColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
                  widgetBgColor: Colors.white,
                );
                if (result ?? false) {
                  _viewModel.editBuildingIndex(context, _comWbrand?.id, _title,
                      _upOpenFlag, _downOpenFlag,
                      upStart: getUpStartParam(), upEnd: getUpEndParam(),
                      downStart: getDownStartParam(), downEnd: getDownEndParam()
                  );
                }
                return;
              }
              _viewModel.editBuildingIndex(context, _comWbrand?.id, _title,
                  _upOpenFlag, _downOpenFlag,
                  upStart: getUpStartParam(), upEnd: getUpEndParam(),
                  downStart: getDownStartParam(), downEnd: getDownEndParam()
              );
            }
          },
          child: Container(
            height: 40,
            alignment: Alignment.center,
            color: !isCanSave()?Color(0xFFCFD4DB):ThemeRepository.getInstance().getPrimaryColor_1890FF(),
            child: Text(
              ("00" == widget?.createOrEdit)?"创建":"保存",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15
              ),
            ),
          ),
        ),
      ),
    );
  }

  //是否需要楼层减少的提示-楼上层
  bool needUpFloorHint(int upStart, int upEnd){
    if("01" == _upOpenFlag){
      //地上楼层打开
      if(widget?.upFloorNumList != null && (widget?.upFloorNumList?.length??0) > 0){
        //现有的最低
        int remainStart = int.parse(widget?.upFloorNumList?.last??"0");
        int remainEnd = int.parse(widget?.upFloorNumList?.first??"0");
        if((upStart??0) > remainStart){
          //如果起始楼层变大了，说明有的楼层被阉割了
          //说明要提示
          return true;
        }
        if((upEnd??0) < remainEnd){
          //如果结束楼层变小了，说明有的楼层被阉割了
          //说明要提示
          return true;
        }
        return false;
      }else{
        //地上楼层为空，此次操作是新增了地上楼层， 则不需要提示
        return false;
      }
    }else{
      //地上楼层关闭
      //如果之前有地上楼层，那么就需要提示
      if(widget?.upFloorNumList != null && (widget?.upFloorNumList?.length??0) > 0){
        return true;
      }else{
        return false;
      }
    }
  }

  bool needDownFloorHint(int downStart, int downEnd){
    if("01" == _downOpenFlag){
      //地下楼层打开
      if(widget?.downFloorNumList != null && (widget?.downFloorNumList?.length??0) > 0){
        //现有的最低
        int remainStart = int.parse(widget?.downFloorNumList?.first??"0");
        int remainEnd = int.parse(widget?.downFloorNumList?.last??"0");
        if((downStart??0) > remainStart){
          //如果起始楼层变大了，说明有的楼层被阉割了
          //说明要提示
          return true;
        }
        if((downEnd??0) < remainEnd){
          //如果结束楼层变小了，说明有的楼层被阉割了
          //说明要提示
          return true;
        }
        return false;
      }else{
        //地上楼层为空，此次操作是新增了地上楼层， 则不需要提示
        return false;
      }
    }else{
      //地上楼层关闭
      //如果之前有地上楼层，那么就需要提示
      if(widget?.downFloorNumList != null && (widget?.downFloorNumList?.length??0) > 0){
        return true;
      }else{
        return false;
      }
    }
  }

  bool isCanSave(){
    if(StringUtils.isEmpty(_title)){
      return false;
    }
    //地上层打开，起止楼层都得有才能保存
    if("01" == _upOpenFlag){
      if(StringUtils.isEmpty(_upStartFloor) || StringUtils.isEmpty(_upEndFloor)){
        return false;
      }
    }
    //地下层打开，起止楼层都得有才能保存
    if("01" == _downOpenFlag){
      if(StringUtils.isEmpty(_downStartFloor) || StringUtils.isEmpty(_downEndFloor)){
        return false;
      }
    }
    return true;
  }

  List<String> getUpFloor(int start, int end){
    List<String> upFloor = new List();
    upFloor.add("1F");
    upFloor.add("2F");
    upFloor.add("3F");
    upFloor.add("4F");
    upFloor.add("5F");
    upFloor.add("6F");
    upFloor.add("7F");
    upFloor.add("8F");
    upFloor.add("9F");
    upFloor.add("10F");


    upFloor.add("11F");
    upFloor.add("12F");
    upFloor.add("13F");
    upFloor.add("14F");
    upFloor.add("15F");
    upFloor.add("16F");
    upFloor.add("17F");
    upFloor.add("18F");
    upFloor.add("19F");
    upFloor.add("20F");

    upFloor.add("21F");
    upFloor.add("22F");
    upFloor.add("23F");
    upFloor.add("24F");
    upFloor.add("25F");
    upFloor.add("26F");
    upFloor.add("27F");
    upFloor.add("28F");
    upFloor.add("29F");
    upFloor.add("30F");

    upFloor.add("31F");
    upFloor.add("32F");
    upFloor.add("33F");
    upFloor.add("34F");
    upFloor.add("35F");
    upFloor.add("36F");
    upFloor.add("37F");
    upFloor.add("38F");
    upFloor.add("39F");
    upFloor.add("40F");

    upFloor.add("41F");
    upFloor.add("42F");
    upFloor.add("43F");
    upFloor.add("44F");
    upFloor.add("45F");
    upFloor.add("46F");
    upFloor.add("47F");
    upFloor.add("48F");
    upFloor.add("49F");
    upFloor.add("50F");

    upFloor.add("51F");
    upFloor.add("52F");
    upFloor.add("53F");
    upFloor.add("54F");
    upFloor.add("55F");
    upFloor.add("56F");
    upFloor.add("57F");
    upFloor.add("58F");
    upFloor.add("59F");
    upFloor.add("50F");

    upFloor.add("61F");
    upFloor.add("62F");
    upFloor.add("63F");
    upFloor.add("64F");
    upFloor.add("65F");
    upFloor.add("66F");
    upFloor.add("67F");
    upFloor.add("68F");
    upFloor.add("69F");
    upFloor.add("70F");

    upFloor.add("71F");
    upFloor.add("72F");
    upFloor.add("73F");
    upFloor.add("74F");
    upFloor.add("75F");
    upFloor.add("76F");
    upFloor.add("77F");
    upFloor.add("78F");
    upFloor.add("79F");
    upFloor.add("80F");

    upFloor.add("81F");
    upFloor.add("82F");
    upFloor.add("83F");
    upFloor.add("84F");
    upFloor.add("85F");
    upFloor.add("86F");
    upFloor.add("87F");
    upFloor.add("88F");
    upFloor.add("89F");
    upFloor.add("90F");

    upFloor.add("91F");
    upFloor.add("92F");
    upFloor.add("93F");
    upFloor.add("94F");
    upFloor.add("95F");
    upFloor.add("96F");
    upFloor.add("97F");
    upFloor.add("98F");
    upFloor.add("99F");
    upFloor.add("100F");


    upFloor.add("101F");
    upFloor.add("102F");
    upFloor.add("103F");
    upFloor.add("104F");
    upFloor.add("105F");
    upFloor.add("106F");
    upFloor.add("107F");
    upFloor.add("108F");
    upFloor.add("109F");
    upFloor.add("110F");

    upFloor.add("111F");
    upFloor.add("112F");
    upFloor.add("113F");
    upFloor.add("114F");
    upFloor.add("115F");
    upFloor.add("116F");
    upFloor.add("117F");
    upFloor.add("118F");
    upFloor.add("119F");
    upFloor.add("120F");
    if(start < 1){
      start = 1;
    }
    if(end < 1){
      end = 1;
    }

    return upFloor.sublist(start - 1, end);
  }


  List<String> getDownFloor(int start, int end){
    List<String> downFloor = new List();
    downFloor.add("B1");
    downFloor.add("B2");
    downFloor.add("B3");
    downFloor.add("B4");
    if(start < 1){
      start = 1;
    }
    if(end < 1){
      end = 1;
    }
    return downFloor.sublist(start - 1, end);
  }
}

/// success : true
/// code : 200
/// msg : "处理成功"
/// data : {"comwid":"a072a49fe04f481baa035bda7493d3f9"}
/// extra : null

class CreateBuildingIndexResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static CreateBuildingIndexResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CreateBuildingIndexResponseBean createBuildingIndexResponseBeanBean = CreateBuildingIndexResponseBean();
    createBuildingIndexResponseBeanBean.success = map['success'];
    createBuildingIndexResponseBeanBean.code = map['code'];
    createBuildingIndexResponseBeanBean.msg = map['msg'];
    createBuildingIndexResponseBeanBean.data = DataBean.fromMap(map['data']);
    createBuildingIndexResponseBeanBean.extra = map['extra'];
    return createBuildingIndexResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// comwid : "a072a49fe04f481baa035bda7493d3f9"

class DataBean {
  String comwid;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.comwid = map['comwid'];
    return dataBean;
  }

  Map toJson() => {
    "comwid": comwid,
  };
}
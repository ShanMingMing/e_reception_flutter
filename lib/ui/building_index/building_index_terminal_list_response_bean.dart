import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"hsnList":[{"terminalName":"终端显示屏12","terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","companyid":"b526266186ac41139f96c302ed8e10d0","screenStatus":"00","bindflg":"01","sideNumber":"ABsx12345678","position":"搜宝的地址","onOff":0,"comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AAC4304","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(搜宝商务中心支行)","screenStatus":"02","bindflg":"01","sideNumber":"AB20210926001","position":"","comwid":null},{"terminalName":"终端显示屏10","terminalPicurl":"","hsn":"151C94B50F591F75594A42E9E18A23F4F2","companyid":"b526266186ac41139f96c302ed8e10d0","address":"山东省济南市莱芜区文化北路83号靠近中国工商银行(城北支行)","screenStatus":"02","bindflg":"01","sideNumber":"AB10000001","position":"搜宝的地址","comwid":null},{"terminalName":"终端显示屏9","terminalPicurl":"","hsn":"15C3130F5F1879EC29C1C7163646D322A8","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","screenStatus":"00","bindflg":"01","sideNumber":"AB20210916373","position":"搜宝呃呃呃","comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACD8E7","companyid":"b526266186ac41139f96c302ed8e10d0","address":"江苏省南京市江宁区天元中路辅路110号靠近江宁开发区总部基地","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"50哦","terminalPicurl":"","hsn":"15CC8E20433E81F2BF2480D51244431133","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近万芳桥","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"终端显示屏2","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACC8BB","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路35号靠近万芳桥","screenStatus":"00","bindflg":"01","sideNumber":"AD10086","position":"搜宝","comwid":null}]}
/// extra : null

class BuildingIndexTerminalListResponseBean {
  bool success;
  String code;
  String msg;
  BuildingIndexTerminalDataBean data;
  dynamic extra;

  static BuildingIndexTerminalListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexTerminalListResponseBean buildingIndexTerminalListResponseBeanBean = BuildingIndexTerminalListResponseBean();
    buildingIndexTerminalListResponseBeanBean.success = map['success'];
    buildingIndexTerminalListResponseBeanBean.code = map['code'];
    buildingIndexTerminalListResponseBeanBean.msg = map['msg'];
    buildingIndexTerminalListResponseBeanBean.data = BuildingIndexTerminalDataBean.fromMap(map['data']);
    buildingIndexTerminalListResponseBeanBean.extra = map['extra'];
    return buildingIndexTerminalListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// hsnList : [{"terminalName":"终端显示屏12","terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","companyid":"b526266186ac41139f96c302ed8e10d0","screenStatus":"00","bindflg":"01","sideNumber":"ABsx12345678","position":"搜宝的地址","onOff":0,"comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AAC4304","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(搜宝商务中心支行)","screenStatus":"02","bindflg":"01","sideNumber":"AB20210926001","position":"","comwid":null},{"terminalName":"终端显示屏10","terminalPicurl":"","hsn":"151C94B50F591F75594A42E9E18A23F4F2","companyid":"b526266186ac41139f96c302ed8e10d0","address":"山东省济南市莱芜区文化北路83号靠近中国工商银行(城北支行)","screenStatus":"02","bindflg":"01","sideNumber":"AB10000001","position":"搜宝的地址","comwid":null},{"terminalName":"终端显示屏9","terminalPicurl":"","hsn":"15C3130F5F1879EC29C1C7163646D322A8","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近中国工商银行(北京搜宝商务中心支行)","screenStatus":"00","bindflg":"01","sideNumber":"AB20210916373","position":"搜宝呃呃呃","comwid":null},{"terminalName":"","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACD8E7","companyid":"b526266186ac41139f96c302ed8e10d0","address":"江苏省南京市江宁区天元中路辅路110号靠近江宁开发区总部基地","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"50哦","terminalPicurl":"","hsn":"15CC8E20433E81F2BF2480D51244431133","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路16-3号楼靠近万芳桥","screenStatus":"02","bindflg":"01","sideNumber":"","position":"","comwid":null},{"terminalName":"终端显示屏2","terminalPicurl":"","hsn":"11140C1F12FEEB2C52DFBEF8EE0AACC8BB","companyid":"b526266186ac41139f96c302ed8e10d0","address":"北京市丰台区南三环西路辅路35号靠近万芳桥","screenStatus":"00","bindflg":"01","sideNumber":"AD10086","position":"搜宝","comwid":null}]

class BuildingIndexTerminalDataBean {
  List<HsnListBean> hsnList;

  static BuildingIndexTerminalDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexTerminalDataBean dataBean = BuildingIndexTerminalDataBean();
    dataBean.hsnList = List()..addAll(
      (map['hsnList'] as List ?? []).map((o) => HsnListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "hsnList": hsnList,
  };
}

/// terminalName : "终端显示屏12"
/// terminalPicurl : ""
/// hsn : "1572BA5FCA24E0E6F66B7A67ED3E636CA0"
/// companyid : "b526266186ac41139f96c302ed8e10d0"
/// screenStatus : "00"
/// bindflg : "01"
/// sideNumber : "ABsx12345678"
/// position : "搜宝的地址"
/// onOff : 0
/// comwid : null

class HsnListBean {
  String terminalName;
  String manager;
  String loginName;
  String terminalPicurl;
  String hsn;
  String companyid;
  String screenStatus;
  String bindflg;
  String sideNumber;
  String position;
  String address;
  String logout;
  int onOff;
  String comwid;
  bool selectStatus = false;

  String getTerminalName(int index){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏${(index ?? 0) + 1}";
  }

  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }

  static HsnListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnListBean hsnListBean = HsnListBean();
    hsnListBean.terminalName = map['terminalName'];
    hsnListBean.terminalPicurl = map['terminalPicurl'];
    hsnListBean.hsn = map['hsn'];
    hsnListBean.companyid = map['companyid'];
    hsnListBean.screenStatus = map['screenStatus'];
    hsnListBean.bindflg = map['bindflg'];
    hsnListBean.sideNumber = map['sideNumber'];
    hsnListBean.position = map['position'];
    hsnListBean.onOff = map['onOff'];
    hsnListBean.comwid = map['comwid'];
    hsnListBean.selectStatus = map['selectStatus'];
    hsnListBean.logout = map['logout'];
    hsnListBean.address = map['address'];
    hsnListBean.manager = map['manager'];
    hsnListBean.loginName = map['loginName'];
    return hsnListBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "hsn": hsn,
    "companyid": companyid,
    "screenStatus": screenStatus,
    "bindflg": bindflg,
    "sideNumber": sideNumber,
    "position": position,
    "onOff": onOff,
    "comwid": comwid,
    "selectStatus": selectStatus,
    "logout": logout,
    "address": address,
    "manager": manager,
    "loginName": loginName,
  };
}
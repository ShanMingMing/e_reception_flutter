/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"wmid":"","watermarkList":[{"wmid":"098382ffb8ff46b29b77366491196417","textColor":"#FFFFFFFF","solidColor":"#1A000000","borderColor":"#FFFFFFFF","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"1e4b2424dd9b4cd88eb3be4f3d02293e","textColor":"#FFFFFFFF","solidColor":"#FF222222","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"2ebdd88d0be845c1ac34d6196e21ad71","textColor":"#FF294F4E","solidColor":"#80FFFFFF","borderColor":"#FF294F4E","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"301240202f074d15901b57cb89ae78fa","textColor":"#FFFFFFFF","solidColor":"#FF294F4E","borderColor":"#FF294F4E","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"357c66fa97fa4e3481aff9e807f21c5b","textColor":"#FFE7CAA6","solidColor":"#66000000","borderColor":"#FFE7CAA6","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"4cf16b035d9f44f5b470aad5c6747630","textColor":"#FF381805","solidColor":"#FFE4C097","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"6866ce575596455ea77838795b1e5122","textColor":"#FFA22F34","solidColor":"#80FFFFFF","borderColor":"#FFA22F34","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"730d35a1a41b4fc28905655446bb874d","textColor":"#FFFFFFFF","solidColor":"#FFA22F34","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"80ae4ff8b59a43a99eacaadca5a1d77a","textColor":"#FF00DDE0","solidColor":"#66000000","borderColor":"#FF00DDE0","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"9758aa7b7c904b3eb47dedcc802a19b2","textColor":"#FFFFFFFF","solidColor":"#FF009D9F","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"a92de6e3caef492694c6a1988b153fec","textColor":"#FFD86700","solidColor":"#80FFFFFF","borderColor":"#FFD86700","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"ab99d851f80f4268b04b958e1ce884d6","textColor":"#FFFFFFFF","solidColor":"#FFE78B0F","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"ebe31da77f874a3ab3e03ccbbcc5ef5c","textColor":"#FFFFC900","solidColor":"#66000000","borderColor":"#FFFFC900","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"eceacd105e484eeb97d0c07cb74974e8","textColor":"#FF2D1400","solidColor":"#FFFFC900","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"}]}
/// extra : null

class SmartHomeWaterListResponseBean {
  bool success;
  String code;
  String msg;
  SmartHomeWaterListDataBean data;
  dynamic extra;

  static SmartHomeWaterListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeWaterListResponseBean smartHomeWaterListResponseBeanBean = SmartHomeWaterListResponseBean();
    smartHomeWaterListResponseBeanBean.success = map['success'];
    smartHomeWaterListResponseBeanBean.code = map['code'];
    smartHomeWaterListResponseBeanBean.msg = map['msg'];
    smartHomeWaterListResponseBeanBean.data = SmartHomeWaterListDataBean.fromMap(map['data']);
    smartHomeWaterListResponseBeanBean.extra = map['extra'];
    return smartHomeWaterListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// wmid : ""
/// watermarkList : [{"wmid":"098382ffb8ff46b29b77366491196417","textColor":"#FFFFFFFF","solidColor":"#1A000000","borderColor":"#FFFFFFFF","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"1e4b2424dd9b4cd88eb3be4f3d02293e","textColor":"#FFFFFFFF","solidColor":"#FF222222","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"2ebdd88d0be845c1ac34d6196e21ad71","textColor":"#FF294F4E","solidColor":"#80FFFFFF","borderColor":"#FF294F4E","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"301240202f074d15901b57cb89ae78fa","textColor":"#FFFFFFFF","solidColor":"#FF294F4E","borderColor":"#FF294F4E","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"357c66fa97fa4e3481aff9e807f21c5b","textColor":"#FFE7CAA6","solidColor":"#66000000","borderColor":"#FFE7CAA6","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"4cf16b035d9f44f5b470aad5c6747630","textColor":"#FF381805","solidColor":"#FFE4C097","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"6866ce575596455ea77838795b1e5122","textColor":"#FFA22F34","solidColor":"#80FFFFFF","borderColor":"#FFA22F34","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"730d35a1a41b4fc28905655446bb874d","textColor":"#FFFFFFFF","solidColor":"#FFA22F34","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"80ae4ff8b59a43a99eacaadca5a1d77a","textColor":"#FF00DDE0","solidColor":"#66000000","borderColor":"#FF00DDE0","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"9758aa7b7c904b3eb47dedcc802a19b2","textColor":"#FFFFFFFF","solidColor":"#FF009D9F","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"a92de6e3caef492694c6a1988b153fec","textColor":"#FFD86700","solidColor":"#80FFFFFF","borderColor":"#FFD86700","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"ab99d851f80f4268b04b958e1ce884d6","textColor":"#FFFFFFFF","solidColor":"#FFE78B0F","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"ebe31da77f874a3ab3e03ccbbcc5ef5c","textColor":"#FFFFC900","solidColor":"#66000000","borderColor":"#FFFFC900","bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"},{"wmid":"eceacd105e484eeb97d0c07cb74974e8","textColor":"#FF2D1400","solidColor":"#FFFFC900","borderColor":null,"bgUrl":"https://etpic.we17.com/Homedecoration/watermarkBG.jpg","delflg":"00"}]

class SmartHomeWaterListDataBean {
  String wmid;
  List<WatermarkListBean> watermarkList;

  static SmartHomeWaterListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SmartHomeWaterListDataBean dataBean = SmartHomeWaterListDataBean();
    dataBean.wmid = map['wmid'];
    dataBean.watermarkList = List()..addAll(
      (map['watermarkList'] as List ?? []).map((o) => WatermarkListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "wmid": wmid,
    "watermarkList": watermarkList,
  };
}

/// wmid : "098382ffb8ff46b29b77366491196417"
/// textColor : "#FFFFFFFF"
/// solidColor : "#1A000000"
/// borderColor : "#FFFFFFFF"
/// bgUrl : "https://etpic.we17.com/Homedecoration/watermarkBG.jpg"
/// delflg : "00"

class WatermarkListBean {
  String wmid;
  String textColor;
  String solidColor;
  String borderColor;
  String bgUrl;
  String delflg;

  static WatermarkListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    WatermarkListBean watermarkListBean = WatermarkListBean();
    watermarkListBean.wmid = map['wmid'];
    watermarkListBean.textColor = map['textColor'];
    watermarkListBean.solidColor = map['solidColor'];
    watermarkListBean.borderColor = map['borderColor'];
    watermarkListBean.bgUrl = map['bgUrl'];
    watermarkListBean.delflg = map['delflg'];
    return watermarkListBean;
  }

  Map toJson() => {
    "wmid": wmid,
    "textColor": textColor,
    "solidColor": solidColor,
    "borderColor": borderColor,
    "bgUrl": bgUrl,
    "delflg": delflg,
  };
}
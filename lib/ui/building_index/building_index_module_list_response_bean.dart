import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"picurl":"https://etpic.we17.com/template/pic/20220110095548.png","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"1","updatetime":"2022-01-10 00:00:00"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class BuildingIndexModuleListResponseBean extends BasePagerBean<BuildingIndexModuleBean>{
  bool success;
  String code;
  String msg;
  DataBean data;

  static BuildingIndexModuleListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexModuleListResponseBean buildingIndexModuleListResponseBeanBean = BuildingIndexModuleListResponseBean();
    buildingIndexModuleListResponseBeanBean.success = map['success'];
    buildingIndexModuleListResponseBeanBean.code = map['code'];
    buildingIndexModuleListResponseBeanBean.msg = map['msg'];
    buildingIndexModuleListResponseBeanBean.data = DataBean.fromMap(map['data']);
    return buildingIndexModuleListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<BuildingIndexModuleBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// page : {"records":[{"picurl":"https://etpic.we17.com/template/pic/20220110095548.png","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"1","updatetime":"2022-01-10 00:00:00"}],"total":1,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"picurl":"https://etpic.we17.com/template/pic/20220110095548.png","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"1","updatetime":"2022-01-10 00:00:00"}]
/// total : 1
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<BuildingIndexModuleBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => BuildingIndexModuleBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// picurl : "https://etpic.we17.com/template/pic/20220110095548.png"
/// htmlurl : "http://etpic.we17.com/test/20220110094429_8649.html"
/// id : "1"
/// updatetime : "2022-01-10 00:00:00"

class BuildingIndexModuleBean {
  String picurl;
  String htmlurl;
  String id;
  String updatetime;

  static BuildingIndexModuleBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexModuleBean recordsBean = BuildingIndexModuleBean();
    recordsBean.picurl = map['picurl'];
    recordsBean.htmlurl = map['htmlurl'];
    recordsBean.id = map['id'];
    recordsBean.updatetime = map['updatetime'];
    return recordsBean;
  }

  Map toJson() => {
    "picurl": picurl,
    "htmlurl": htmlurl,
    "id": id,
    "updatetime": updatetime,
  };
}
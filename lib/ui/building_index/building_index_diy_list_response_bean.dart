import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"哈哈哈","denum":4,"uenum":33,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"c291fbf5f989471c854181eaa5a3dd43","usnum":3},{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"刚果共和国","denum":4,"uenum":20,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"5c63fdb539bb497b904d2873cbc9e114","usnum":1}],"total":2,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}}
/// extra : null

class BuildingIndexDiyListResponseBean extends BasePagerBean<BuildingIndexDiyBean>{
  bool success;
  String code;
  String msg;
  DataBean data;

  static BuildingIndexDiyListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexDiyListResponseBean buildingIndexDiyListResponseBeanBean = BuildingIndexDiyListResponseBean();
    buildingIndexDiyListResponseBeanBean.success = map['success'];
    buildingIndexDiyListResponseBeanBean.code = map['code'];
    buildingIndexDiyListResponseBeanBean.msg = map['msg'];
    buildingIndexDiyListResponseBeanBean.data = DataBean.fromMap(map['data']);
    return buildingIndexDiyListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
  };
  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<BuildingIndexDiyBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";
}

/// page : {"records":[{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"哈哈哈","denum":4,"uenum":33,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"c291fbf5f989471c854181eaa5a3dd43","usnum":3},{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"刚果共和国","denum":4,"uenum":20,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"5c63fdb539bb497b904d2873cbc9e114","usnum":1}],"total":2,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}

class DataBean {
  PageBean page;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
  };
}

/// records : [{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"哈哈哈","denum":4,"uenum":33,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"c291fbf5f989471c854181eaa5a3dd43","usnum":3},{"dsnum":1,"dflg":"01","upflg":"01","wtitle":"刚果共和国","denum":4,"uenum":20,"syswid":"1","htmlurl":"http://etpic.we17.com/test/20220110094429_8649.html","id":"5c63fdb539bb497b904d2873cbc9e114","usnum":1}]
/// total : 2
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<BuildingIndexDiyBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => BuildingIndexDiyBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// dsnum : 1
/// dflg : "01"
/// upflg : "01"
/// wtitle : "哈哈哈"
/// denum : 4
/// uenum : 33
/// syswid : "1"
/// htmlurl : "http://etpic.we17.com/test/20220110094429_8649.html"
/// id : "c291fbf5f989471c854181eaa5a3dd43"
/// usnum : 3

class BuildingIndexDiyBean {
  int dsnum;
  String dflg;
  String upflg;
  String wtitle;
  int denum;
  int uenum;
  String syswid;
  String htmlurl;
  String picurl;
  String id;
  int usnum;
  int hsnnum;//已播放该指示牌的终端数

  static BuildingIndexDiyBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BuildingIndexDiyBean recordsBean = BuildingIndexDiyBean();
    recordsBean.dsnum = map['dsnum'];
    recordsBean.dflg = map['dflg'];
    recordsBean.upflg = map['upflg'];
    recordsBean.wtitle = map['wtitle'];
    recordsBean.denum = map['denum'];
    recordsBean.uenum = map['uenum'];
    recordsBean.syswid = map['syswid'];
    recordsBean.htmlurl = map['htmlurl'];
    recordsBean.picurl = map['picurl'];
    recordsBean.id = map['id'];
    recordsBean.usnum = map['usnum'];
    recordsBean.hsnnum = map['hsnnum'];
    return recordsBean;
  }

  Map toJson() => {
    "dsnum": dsnum,
    "dflg": dflg,
    "upflg": upflg,
    "wtitle": wtitle,
    "denum": denum,
    "uenum": uenum,
    "syswid": syswid,
    "htmlurl": htmlurl,
    "picurl": picurl,
    "id": id,
    "usnum": usnum,
    "hsnnum": hsnnum,
  };
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

/// @author: pengboboer
/// @createDate: 8/20/21

class EditDeletePopMenuWidget extends StatelessWidget {

  final TapUpDetails details;
  final List<String> items;
  final ValueChanged<String> onClickItem;
  final Color bgColor;
  final Color textColor;

  const EditDeletePopMenuWidget({Key key,
    @required this.details,
    @required this.items,
    @required this.onClickItem, this.bgColor, this.textColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          RouterUtils.pop(context);
        },
        child: Container(
          child: Stack(
            children: [
              Positioned(
                  top: details.globalPosition.dy + 15,
                  right: 8,
                  child: Container(
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff000000).withOpacity(0.1),
                              offset: Offset(0, 0),
                              blurRadius: 40
                          )
                        ]
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          child: Image.asset("images/sanjiao_white.png",
                              color: bgColor??Colors.white,
                              width: 11, height: 6),
                          margin: EdgeInsets.only(right: 10),
                        ),
                        Container(
                          // width: 140,
                          padding: EdgeInsets.symmetric(vertical: 10),
                          decoration: BoxDecoration(
                            color: bgColor??Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Column(
                            children: List.generate(items.length, (index) =>
                                ClickAnimateWidget(
                                  scale: 1.02,
                                  onClick: () {
                                    onClickItem(items[index]);
                                  },
                                  child: Container(
                                    width: 120,
                                    height: 45,
                                    alignment: Alignment.center,
                                    child: Text(
                                          items[index],
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: textColor??ThemeRepository.getInstance().getCardBgColor_21263C()),
                                        ),
                                  ),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}





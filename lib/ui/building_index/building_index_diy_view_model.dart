import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'building_index_diy_list_response_bean.dart';
import 'change_to_diy_or_module_page_event.dart';
import 'diy_list_count_event.dart';

///已制作列表viewmodel
class BuildingIndexDiyViewModel extends BasePagerViewModel<
    BuildingIndexDiyBean,
    BuildingIndexDiyListResponseBean> {

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  BuildingIndexDiyViewModel(BaseState<StatefulWidget> state)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();
  }

  @override
  bool isNeedCache() {
    return true;
  }

  @override
  String getCacheKey() {
    return super.getCacheKey() + UserRepository.getInstance().getCompanyId();
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return NetApi.BUILDING_INDEX_DIY_LIST;
  }

  @override
  BuildingIndexDiyListResponseBean parseData(VgHttpResponse resp) {
    BuildingIndexDiyListResponseBean vo = BuildingIndexDiyListResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
      VgEventBus.global.send(new DiyListCountEvent((vo?.data?.page?.total??0)));
    }else{
      VgEventBus.global.send(new ChangeToDiyOrModulePageEvent(1));
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

import 'dart:async';
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_module_detail_page.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_mixin/navigator_page_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'building_index_module_list_response_bean.dart';
import 'building_index_module_view_model.dart';

/// 楼层指示牌模板列表
class BuildingIndexModulePage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "BuildingIndexModulePage";

  @override
  BuildingIndexModulePageState createState() => BuildingIndexModulePageState();

}

class BuildingIndexModulePageState
    extends BasePagerState<BuildingIndexModuleBean, BuildingIndexModulePage>
    with
        AutomaticKeepAliveClientMixin,
        VgPlaceHolderStatusMixin,
        NavigatorPageMixin {

  BuildingIndexModuleViewModel _viewModel;
  StreamSubscription _aiPosterStreamSubscription;


  ///获取state
  static BuildingIndexModulePageState of(BuildContext context) {
    final BuildingIndexModulePageState result =
    context.findAncestorStateOfType<BuildingIndexModulePageState>();
    return result;
  }


  @override
  void initState() {
    super.initState();
    _viewModel = BuildingIndexModuleViewModel(this);
    _viewModel?.refresh();
  }

  @override
  void dispose() {
    _aiPosterStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.white,
      child: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return  MixinPlaceHolderStatusWidget(
        emptyOnClick: () => _viewModel?.refresh(),
        errorOnClick: () => _viewModel.refresh(),
        emptyWidget: _defaultEmptyCustomWidget("暂无内容"),
        errorWidget: _defaultEmptyCustomWidget("网络断线 重新获取"),
        child: VgPullRefreshWidget.bind(
            state: this,
            refreshBgColor: Colors.white,
            viewModel: _viewModel,
            child: _toGridPage())
    );
  }

  Widget _defaultEmptyCustomWidget(String msg) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset(
          "images/icon_empty_white.png",
          width: 120,
          gaplessPlayback: true,
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          msg??"暂无内容",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 13,
          ),
        )
      ],
    );
  }

  Widget _toGridPage(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          bottom: getNavHeightDistance(context)
      ),
      itemCount: data?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 6,
        crossAxisSpacing: 6,
        childAspectRatio: 113 / 193.1,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toGridItemWidget(context,data?.elementAt(index));
      },
    );
  }


  Widget _toGridItemWidget(BuildContext context, BuildingIndexModuleBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        BuildingIndexModelDetailPage.navigatorPush(context, itemBean);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          width: 111,
          decoration: BoxDecoration(
              color: Colors.white
          ),
          child: VgCacheNetWorkImage(itemBean?.picurl?? "",
            fit: BoxFit.fill,
            imageQualityType: ImageQualityType.high,),
        ),
      ),
    );
  }


  @override
  bool get wantKeepAlive => true;
}

import 'dart:async';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/ai_poster_detail_page.dart';
import 'package:e_reception_flutter/ui/ai_poster/ai_poster_diy_view_model.dart';
import 'package:e_reception_flutter/ui/ai_poster/ai_poster_index_response_bean.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/click_back_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_mixin/navigator_page_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// ai海报首页
class AiPosterDiyPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "AiPosterDiyPage";

  @override
  AiPosterDiyPageState createState() => AiPosterDiyPageState();

}

class AiPosterDiyPageState
    extends BasePagerState<AiPosterIndexListItemBean, AiPosterDiyPage>
    with
        AutomaticKeepAliveClientMixin,
        VgPlaceHolderStatusMixin,
        NavigatorPageMixin {

  AiPosterDiyViewModel viewModel;
  StreamSubscription _aiPosterStreamSubscription;

  ///获取state
  static AiPosterDiyPageState of(BuildContext context) {
    final AiPosterDiyPageState result =
    context.findAncestorStateOfType<AiPosterDiyPageState>();
    return result;
  }


  @override
  void initState() {
    super.initState();
    viewModel = AiPosterDiyViewModel(this);
    viewModel?.refresh();
    _aiPosterStreamSubscription =
        VgEventBus.global.on<MonitoringAiPosterIndexClass>()?.listen((event) {
          viewModel?.refresh();
        });
  }

  @override
  void dispose() {
    _aiPosterStreamSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return  MixinPlaceHolderStatusWidget(
        emptyOnClick: () => viewModel?.refresh(),
        errorOnClick: () => viewModel.refresh(),
        // child: child,
        child: VgPullRefreshWidget.bind(
            state: this, viewModel: viewModel, child: _toGridPage())
    );
  }


  Widget _toGridPage(){
    return GridView.builder(
      padding: EdgeInsets.only(
          left: 15,
          right: 15,
          top: 3,
          bottom: getNavHeightDistance(context)
      ),
      itemCount: data?.length ?? 0,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        childAspectRatio: 1 / 1.73,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _toGridItemWidget(context,data?.elementAt(index));
      },
    );
  }


  Widget _toGridItemWidget(BuildContext context, AiPosterIndexListItemBean itemBean) {
    return ClickBackgroundWidget(
      behavior: HitTestBehavior.opaque,
      onTap: (){
        AiPosterDetailPage.navigatorPush(context, itemBean);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: AspectRatio(
          aspectRatio: 1,
          child: Container(
            color: Colors.black,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                VgCacheNetWorkImage(itemBean?.getPicUrl() ?? "",imageQualityType: ImageQualityType.middleDown,),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

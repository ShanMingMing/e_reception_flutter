import 'package:e_reception_flutter/media_library/media_library_index/even/media_library_index_refresh_even.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/diy_poster_count_event.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'ai_poster_index_response_bean.dart';

class AiPosterDiyViewModel extends BasePagerViewModel<
    AiPosterIndexListItemBean,
    AiPosterIndexResponseBean> {

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  AiPosterDiyViewModel(BaseState<StatefulWidget> state)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();
  }

  ///删除diy海报
  void deleteDiyPoster(BuildContext context, String id, {VoidCallback callback}){
    if(StringUtils.isEmpty(id)){
      return;
    }
    VgHudUtils.show(context,"删除中");
    VgHttpUtils.get(NetApi.DELETE_DIY_POSTER,params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "id": id ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, "删除成功");
          VgEventBus.global.send(new MonitoringAiPosterIndexClass());
          if(callback != null){
            callback.call();
          }
        },
        onError: (msg){
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }
    ));
  }



  @override
  bool isNeedCache() {
    return true;
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appGetUserDIYPicList";
  }

  @override
  AiPosterIndexResponseBean parseData(VgHttpResponse resp) {
    AiPosterIndexResponseBean vo = AiPosterIndexResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    VgEventBus.global.send(DiyPosterCount(vo?.data?.page?.total));
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

/// success : true
/// code : 200
/// msg : "处理成功"
/// data : {"id":"d62533afa368428a87fecf7734238650"}
/// extra : null

class EditDiyPosterResponseBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static EditDiyPosterResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EditDiyPosterResponseBean editDiyPosterResponseBeanBean = EditDiyPosterResponseBean();
    editDiyPosterResponseBeanBean.success = map['success'];
    editDiyPosterResponseBeanBean.code = map['code'];
    editDiyPosterResponseBeanBean.msg = map['msg'];
    editDiyPosterResponseBeanBean.data = DataBean.fromMap(map['data']);
    editDiyPosterResponseBeanBean.extra = map['extra'];
    return editDiyPosterResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// id : "d62533afa368428a87fecf7734238650"

class DataBean {
  String id;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.id = map['id'];
    return dataBean;
  }

  Map toJson() => {
    "id": id,
  };
}
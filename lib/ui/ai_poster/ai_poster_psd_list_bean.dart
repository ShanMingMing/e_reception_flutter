import 'dart:convert';

/// cover : "http://etpic.we17.com/test/20210811163924_5952.png"
/// date : [{"left":0,"top":0,"width":1080,"height":1920,"opacity":1,"name":"背景","type":"02","content":"http://etpic.we17.com/test/20210811163919_3608.png"},{"left":81,"top":53,"width":917,"height":919,"opacity":1,"name":"椭圆 1","type":"02","content":"http://etpic.we17.com/test/20210811163921_6998.png"},{"left":48,"top":1450,"width":219,"height":382,"opacity":1,"name":"b98c4e5946b030bc45e2cb978fc60462","type":"02","content":"http://etpic.we17.com/test/20210811163921_3339.png"},{"left":270,"top":1372,"width":1192,"height":596,"opacity":0.5411764705882353,"name":"4f31518f7d3b7018231f8b436d2bdeca","type":"02","content":"http://etpic.we17.com/test/20210811163921_4513.png"},{"left":778,"top":405,"width":229,"height":717,"opacity":1,"name":"76e55f608c6906b1a34818a404f19314","type":"02","content":"http://etpic.we17.com/test/20210811163922_8019.png"},{"left":778,"top":405,"width":229,"height":717,"opacity":1,"name":"76e55f608c6906b1a34818a404f19314 拷贝","type":"02","content":"http://etpic.we17.com/test/20210811163922_7542.png"},{"left":605,"top":1196,"width":429,"height":324,"opacity":0.25098039215686274,"name":"a9a8dc0bc413775a63949b391937c7e3","type":"02","content":"http://etpic.we17.com/test/20210811163922_4635.png"},{"left":22,"top":882,"width":271,"height":373,"opacity":0.25098039215686274,"name":"a9a8dc0bc413775a63949b391937c7e3 拷贝","type":"02","content":"http://etpic.we17.com/test/20210811163923_8817.png"},{"left":373,"top":1319,"width":335,"height":47,"opacity":1,"name":"名称-01-15-哆哆咪培训机构","type":"01","fontFamily":"20210706100220_8983.otf","content":"哆哆咪培训机构","fontSize":50,"alignment":"center","color":"#000000"},{"left":477,"top":1156,"width":126,"height":126,"opacity":1,"name":"logo","type":"02","content":"http://etpic.we17.com/test/20210811163923_3011.png"},{"left":162,"top":1389,"width":756,"height":74,"opacity":1,"name":"Play out the music melody compose the wonderful music chapter","type":"02","content":"http://etpic.we17.com/test/20210811163923_7279.png"}]
/// psd : "http://etpic.we17.com/test/20210811163924_3712.psd"
/// size : "14554"
/// createname : "王凡语"
/// createtime : "2021-08-11 16:39:29"

class AiPosterPsdListBean {
  String cover;
  List<PsdDateBean> data;
  String psd;
  String size;
  String createname;
  String createtime;
  String dataJson;

  static AiPosterPsdListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AiPosterPsdListBean aiPosterPsdListBeanBean = AiPosterPsdListBean();
    aiPosterPsdListBeanBean.cover = map['cover'];
    if(map['data'] != null){
      aiPosterPsdListBeanBean.dataJson = json.encode(map["data"]);
      aiPosterPsdListBeanBean.data = List()..addAll(
          (map['data'] as List ?? []).map((o) => PsdDateBean.fromMap(o))
      );

    }else if(map['date'] != null){
      aiPosterPsdListBeanBean.dataJson = json.encode(map["date"]);
      aiPosterPsdListBeanBean.data = List()..addAll(
          (map['date'] as List ?? []).map((o) => PsdDateBean.fromMap(o))
      );
    }

    aiPosterPsdListBeanBean.psd = map['psd'];
    aiPosterPsdListBeanBean.size = map['size'];
    aiPosterPsdListBeanBean.createname = map['createname'];
    aiPosterPsdListBeanBean.createtime = map['createtime'];
    return aiPosterPsdListBeanBean;
  }

  Map toJson() => {
    "cover": cover,
    "date": data,
    "psd": psd,
    "size": size,
    "createname": createname,
    "dataJson": dataJson,
  };
}

/// left : 0
/// top : 0
/// width : 1080
/// height : 1920
/// opacity : 1
/// name : "背景"
/// type : "02"
/// content : "http://etpic.we17.com/test/20210811163919_3608.png"

class PsdDateBean {
  int left;
  int top;
  int width;
  int height;
  double opacity;
  String name;
  String type;//01文字 02图片
  String content;
  String fontFamily;
  double fontSize;
  String alignment;
  String color;
  String lineHeight;
  String borderRadius;
  String proportion;

  static PsdDateBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PsdDateBean dateBean = PsdDateBean();
    dateBean.left = map['left'];
    dateBean.top = map['top'];
    dateBean.width = map['width'];
    dateBean.height = map['height'];
    if(map['opacity'] is double){
      dateBean.opacity = map['opacity'];
    }else if(map['opacity'] is int){
      int object = map['opacity'];
      dateBean.opacity = double.parse(object.toString());
    }

    dateBean.name = map['name'];
    dateBean.proportion = map['proportion'];
    dateBean.type = map['type'];
    dateBean.content = map['content'];
    dateBean.fontFamily = map['fontFamily'];
    if(map['fontSize'] is double){
      dateBean.fontSize = map['fontSize'];
    }else if(map['fontSize'] is int){
      int object = map['fontSize'];
      dateBean.fontSize = double.parse(object.toString());
    }
    dateBean.alignment = map['alignment'];
    dateBean.color = map['color'];
    if(map['lineHeight'] is int){
      int object = map['lineHeight'];
      dateBean.lineHeight = object.toString();
    }else if(map['lineHeight'] is String){
      dateBean.lineHeight = map['lineHeight'];
    }
    if(map['borderRadius'] is int){
      int object = map['borderRadius'];
      dateBean.borderRadius = object.toString();
    }else if(map['borderRadius'] is String){
      dateBean.borderRadius = map['borderRadius'];
    }
    return dateBean;
  }

  Map toJson() => {
    "left": left,
    "top": top,
    "width": width,
    "height": height,
    "opacity": opacity,
    "name": name,
    "type": type,
    "content": content,
    "fontFamily": fontFamily,
    "fontSize": fontSize,
    "alignment": alignment,
    "color": color,
    "lineHeight": lineHeight,
    "borderRadius": borderRadius,
    "proportion": proportion,
  };
}
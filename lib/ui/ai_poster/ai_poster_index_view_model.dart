import 'dart:io';

import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/ai_poster/diy_poster_count_event.dart';
import 'package:e_reception_flutter/ui/ai_poster/edit_diy_poster_response_bean.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/file_utils.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import '../app_main.dart';
import 'ai_poster_index_response_bean.dart';


class AiPosterIndexViewModel extends BasePagerViewModel<
    AiPosterIndexListItemBean,
    AiPosterIndexResponseBean> {


  ///保存用户DIY海报模板的记录
  static const String SAVE_USER_DIY_PIC =ServerApi.BASE_URL + "app/appSaveUserDiyPicLog";
  ///用户编辑已制作DIY海报模板
  static const String UPDATE_USER_DIY_PIC =ServerApi.BASE_URL + "app/appUpdateDIYPicLog";
  ///DIY海报模板投屏
  static const String PUSH_DIY_PIC =ServerApi.BASE_URL + "app/appPushDiyPicHsn";

  ValueNotifier<int> totalValueNotifier;
  ValueNotifier<PlaceHolderStatusType> statusTypeValueNotifier;

  AiPosterIndexViewModel(BaseState<StatefulWidget> state)
      : super(state){
    statusTypeValueNotifier = ValueNotifier(PlaceHolderStatusType.loading);
    totalValueNotifier = ValueNotifier(null);
  }


  ///保存用户DIY海报模板的记录
  void saveUserDiyPic(BuildContext context, String compicid, String picid,
      String picurl, String psdjson, String firstflg, Function(String pic, String id) pic){
    VgHudUtils.show(context, "保存中");
    FileUtils.getImageInfo(picurl, (picsize){
      print("picsize:" + (picsize??"111"));
      String folderStorage;
      VgMatisseUploadUtils.uploadSingleImage(
          picurl,
          VgBaseCallback(onSuccess: (String netPic) {
            if (StringUtils.isNotEmpty(netPic)) {
              VgHttpUtils.post(SAVE_USER_DIY_PIC, params: {
                "authId":UserRepository.getInstance().authId ?? "",
                "compicid":compicid ?? "",
                "picid":picid ?? "",
                "picurl":netPic ?? "",
                "psdjson":psdjson ?? "",
                "firstflg":firstflg ?? "01",
                "picsize":picsize ?? "1020*1980",
                "picstorage":folderStorage ?? "0",

              },callback: BaseCallback(
                  onSuccess: (val){
                    VgEventBus.global.send(MonitoringAiPosterIndexClass());
                    VgHudUtils.hide(context);
                    EditDiyPosterResponseBean bean =
                    EditDiyPosterResponseBean.fromMap(val);
                    pic.call(netPic, bean?.data?.id);
                  },
                  onError: (msg){
                    loading(false);
                    VgToastUtils.toast(AppMain.context, msg);}
              ));
            } else {
              VgToastUtils.toast(context, "图片上传失败");

            }
          }, onError: (String msg) {
            VgToastUtils.toast(context, msg);
            print("msg:" + msg);
          }),
          onGetNewFileLength: (value){
            folderStorage = (value / 1024).floor().toString();
          }
      );
    });


  }

  ///用户编辑已制作DIY海报模板
  void updateUserDiyPic(BuildContext context, String id,
      String picurl, String psdjson, Function(String pic) pic){
    VgHudUtils.show(context, "保存中");
    FileUtils.getImageInfo(picurl, (picsize){
      print("picsize:" + (picsize??"111"));
      String folderStorage;
      VgMatisseUploadUtils.uploadSingleImage(
          picurl,
          VgBaseCallback(onSuccess: (String netPic) {
            if (StringUtils.isNotEmpty(netPic)) {
              VgHttpUtils.get(UPDATE_USER_DIY_PIC, params: {
                "authId":UserRepository.getInstance().authId ?? "",
                "id":id ?? "",
                "picurl":netPic ?? "",
                "picsize":picsize ?? "1020*1980",
                "picstorage":folderStorage ?? "0",
                "psdjson":psdjson ?? "",
              },callback: BaseCallback(
                  onSuccess: (val){
                    VgEventBus.global.send(MonitoringAiPosterIndexClass());
                    VgHudUtils.hide(context);
                    pic.call(netPic);
                  },
                  onError: (msg){
                    loading(false);
                    VgToastUtils.toast(AppMain.context, msg);}
              ));
            } else {
              VgToastUtils.toast(context, "图片上传失败");

            }
          }, onError: (String msg) {
            VgToastUtils.toast(context, msg);
            print("msg:" + msg);
          }),
          onGetNewFileLength: (value){
            folderStorage = (value / 1024).floor().toString();
          }
      );
    });

  }

  ///DIY海报模板投屏
  void pushDiyPic(BuildContext context, String endday, String hsns,
      String picid, String picurl, String startday, String id, VoidCallback callback){
    VgHudUtils.show(context, "投屏中");
    VgHttpUtils.get(PUSH_DIY_PIC, params: {
      "authId":UserRepository.getInstance().authId ?? "",
      "startday": (StringUtils.isEmpty(hsns))?null:(startday ?? ""),
      "endday": (StringUtils.isEmpty(hsns))?null:(endday ?? ""),
      "hsns":hsns ?? "",
      "picid":picid ?? "",
      "picurl":picurl ?? "",
      "id":id ?? "",
    },callback: BaseCallback(
        onSuccess: (val){
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, "投屏成功");
          callback.call();
        },
        onError: (msg){
          loading(false);
          VgToastUtils.toast(AppMain.context, msg);}
    ));
  }



  @override
  void onDisposed() {
    totalValueNotifier?.dispose();
    statusTypeValueNotifier?.dispose();
    super.onDisposed();
  }


  @override
  bool isNeedCache() {
    return true;
  }


  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appGetDIYPicModelList";
  }

  @override
  AiPosterIndexResponseBean parseData(VgHttpResponse resp) {
    AiPosterIndexResponseBean vo = AiPosterIndexResponseBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    loading(false);
    if((vo?.data?.page?.total??0) > 0){
      statusTypeValueNotifier?.value = null;
    }else{
      statusTypeValueNotifier?.value = PlaceHolderStatusType.empty;
    }
    totalValueNotifier.value = vo?.data?.page?.total;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

  @override
  Stream<String> get onRefreshFailed => Stream.value(null);
}

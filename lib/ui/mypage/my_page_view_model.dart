import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/media_library/media_library_index/service/media_library_index_service.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_dialog_value_bean.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_matisse_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_stream_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'mypage_attendance_ecord_widget/detailed_bean/mypage_proposal_bean.dart';
import 'mypage_attendance_ecord_widget/detailed_bean/mypage_proposal_total_bean.dart';

///我的页面
class MyPageViewModel extends BaseViewModel {
  ///考勤规则数
  VgStreamController<int> punchInCntStreamController;
  ValueNotifier<List<RecordsImprovementListBean>> recordsImprovementListValue;
  ValueNotifier<AllAndUnReadCntNumber> recordsImprovementTotal;

  MyPageViewModel(BaseState<StatefulWidget> state) : super(state) {
    punchInCntStreamController = newAutoReleaseBroadcast();
    recordsImprovementListValue = ValueNotifier<List<RecordsImprovementListBean>>(null);
    recordsImprovementTotal = ValueNotifier<AllAndUnReadCntNumber>(null);
  }

  @override
  void onDisposed() {
    recordsImprovementListValue?.dispose();
    recordsImprovementTotal?.dispose();
    super.onDisposed();
  }

  ///考勤规则数
  void getPunchInRuleCnt() {
    String spPunchInCntKey = KEY_PUNCH_IN_RULE_CNT + UserRepository.getInstance().getCompanyId() ??'';
    SharePreferenceUtil.getInt(spPunchInCntKey).then((value) {
      punchInCntStreamController.add(value);
    });
    String spDepartmentCntKey = KEY_DEPARTMENT_CNT + UserRepository.getInstance().getCompanyId() ??'';
    String spClassCntKey = KEY_CLASS_CNT + UserRepository.getInstance().getCompanyId() ??'';

    VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayFaceSearchInfo",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
        },
        callback: BaseCallback(
            onSuccess: (val) {
              CheckDialogValueBean responseBean =
                  CheckDialogValueBean.fromMap(val);
              if (responseBean.success) {
                //班级加部门数缓存
                print('部门数${responseBean?.data?.MDepartmentCnt??0}');
                SharePreferenceUtil.putInt(spDepartmentCntKey, responseBean?.data?.MDepartmentCnt??0);
                SharePreferenceUtil.putInt(spClassCntKey, responseBean?.data?.MClassCnt??0);

                //考勤规则数
                int cnt = responseBean?.data?.ruleCnt;
                if (cnt != null) {
                  print('考勤规则数更新$cnt');
                  SharePreferenceUtil.putInt(spPunchInCntKey, cnt);
                  punchInCntStreamController.add(cnt);
                }
              }
            },
            onError: (msg) {}));
  }

  ///意见反馈
  void submitProposalData(BuildContext context,String text,String picurl,String video) {
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appSuggestion",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "content": text ?? "",
          "picurl": picurl??"",
          "video" : video??""
        },
        callback: BaseCallback(
            onSuccess: (val) {
              VgEventBus.global.send(SubmitProposalInterfaceEventMonitor());
              VgToastUtils.toast(context, "提交成功");
              RouterUtils.pop(context);
            },
            onError: (msg) {}));
  }

  ///意见反馈列表所有数据
  void getSuggestionListData() {
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appSuggestionList",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "current": 1,
          "size": 1000,
        },
        callback: BaseCallback(
            onSuccess: (val) {
              VgEventBus.global.send(SubmitProposalInterfaceEventMonitor());
              MypageProposalBean myBean = MypageProposalBean.fromMap(val);
              recordsImprovementListValue?.value = myBean?.data?.page?.records;
            },
            onError: (msg) {}));
  }

  ///意见反馈数字
  void getSuggestionTotal() {
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appSuggestionTotal",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "phone": UserRepository.getInstance().getPhone()??"",
        },
        callback: BaseCallback(
            onSuccess: (val) {
              MypageProposalTotalBean myBean = MypageProposalTotalBean.fromMap(val);
              recordsImprovementTotal?.value = myBean?.data?.allAndUnReadCnt;
            },
            onError: (msg) {
              VgToastUtils.toast(AppMain.context, "网络连接不可用");
            }));
  }
}

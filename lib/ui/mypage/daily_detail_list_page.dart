import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/bean/check_in_rec_by_month_response.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/detail/daily_check_in_detail.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/detail/daily_check_in_tabbar_views_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/widget/tab_layout_widget.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_string_util_lib.dart';

///每日打卡记录详情(有考勤规则、无考勤规则)
enum ShowType { NORMAL, ABNORMAL, ABSENCE, NO_RULE }

class DailyDetailListPage extends StatefulWidget {
  final MonthPunchRecordBean data;
  final ShowType type;

  ///身份类型
  final String groupType;
  final String chooseId;

  DailyDetailListPage(this.data, this.type, this.groupType, this.chooseId);

  @override
  DailyDetailListPageState createState() => DailyDetailListPageState();

  static void navigatorPush(BuildContext context, MonthPunchRecordBean data,
      bool hasRule, String groupType, String chooseId,
      {ShowType defaultType = ShowType.NORMAL}) {
    if (hasRule == null || !hasRule) {
      defaultType = ShowType.NO_RULE;
    }
    VgDialogUtils.showCommonBottomDialog(
        context: context,
        child: DailyDetailListPage(data, defaultType, groupType, chooseId));
  }
}

class DailyDetailListPageState extends State<DailyDetailListPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //设置距离顶部位置
      height: ScreenUtils.screenH(context) - 271,

      child: DailyDetailListPageWidget(
          widget.data, widget.type, widget.groupType, widget.chooseId),
    );
  }
}

///弹窗页
class DailyDetailListPageWidget extends StatefulWidget {
  final MonthPunchRecordBean data;
  final ShowType type;

  ///身份类型
  final String groupType;
  final String chooseId;

  DailyDetailListPageWidget(
      this.data, this.type, this.groupType, this.chooseId);

  @override
  _DailyRecordWidgetState createState() => _DailyRecordWidgetState();
}

class _DailyRecordWidgetState extends State<DailyDetailListPageWidget>
    with TickerProviderStateMixin {
  TabController _tabController;

  int _currentTabPosition = 0;

  @override
  void initState() {

    _currentTabPosition = getInitIndex();
    _tabController = TabController(
        length: widget.type == ShowType.NO_RULE ? 0 : 3,
        initialIndex: getInitIndex(),
        vsync: this);
    _tabController.addListener(() {
      _currentTabPosition = _tabController?.index;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          //第一行
          Container(
            height: 50,
            alignment: Alignment.center,
            // padding: const EdgeInsets.only(left: 16,top: 19),
            child: Row(
              children: <Widget>[
                SizedBox(width: 16),
                Text(
                  widget.data.getDateMMddFormat(),
                  style: new TextStyle(color: Color(0xFFFFFFFF), fontSize: 17),
                ),
                SizedBox(
                  width: 3,
                ),
                Text(
                  widget.data.getDateWeek(),
                  style: new TextStyle(color: Color(0xFF808388), fontSize: 12),
                ),
                Spacer(),
                GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 2),
                    child: Icon(
                      Icons.close,
                      size: 20,
                      color: VgColors.INPUT_BG_COLOR,
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
          ),
          //第二行选择点击变化
          Visibility(
            visible: widget?.type != ShowType.NO_RULE,
            child: Container(
              child: Container(
                height: 34,
                  margin: const EdgeInsets.only(left: 18,right: 18,bottom: 10),
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.black26),
                  child: TabLayoutWidget(
                    index: _currentTabPosition,
                    tabCount: 3,
                    tabs: [
                      _tabText("正常", widget.data.normalcnt),
                      _tabText("异常", widget.data.abnormalcnt),
                      _tabText("缺勤", widget.data.absencecnt)
                    ],
                    selectTextStyle:
                        TextStyle(color: Colors.white, fontSize: 12),
                    selectDecoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Color(0xff1890ff),
                        borderRadius: BorderRadius.all(Radius.circular(4))),
                    normalTextStyle:
                        TextStyle(color: Color(0xff808388), fontSize: 12),
                    normalDecoration: null,
                    controller: _tabController,
                    listener: (index) {
                      _tabController.animateTo(index);
                    },
                  )),
            ),
          ),
          Expanded(
            child: Container(
              child: widget?.type == ShowType.NO_RULE
                  ? DailyCheckInDetailPage(
                      widget.data.date,
                      widget.type,
                      weekend: widget.data.isWeekend(),
                      groupType: widget.groupType,
                      chooseId: widget.chooseId,
                    )
                  : DailyCheckInTabBarViewsWidget(
                      dayTime: widget.data.date,
                      tabController: _tabController,
                      weekend: widget.data.isWeekend(),
                      groupType: widget.groupType,
                      chooseId: widget.chooseId,
                    ),
            ),
          ),
        ],
      ),
      //设置一个背景颜色
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        color: Color(0xff21263c),
      ),
    );
  }

  _tabText(String text, int size) {
    return Text(
      text + " " + size.toString(),
    );
  }

  getInitIndex() {
    return widget.type == ShowType.NORMAL
        ? 0
        : widget.type == ShowType.ABNORMAL
            ? 1
            : widget.type == ShowType.ABSENCE ? 2 : 0;
  }
}

/// tab项组件
class _TabItemWidget extends StatelessWidget {
  final String text;

  ///是否选中
  final bool isSelected;

  final VoidCallback onTap;

  const _TabItemWidget({
    Key key,
    this.text = "测试",
    this.isSelected = false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 3, horizontal: 3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: isSelected ? Colors.blue : Colors.transparent,
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(color: Colors.black, fontSize: 12),
          ),
        ),
      ),
    );
  }
}

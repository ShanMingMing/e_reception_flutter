import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditNameDialogWidget extends StatefulWidget {
  final String fullName;

  final String alias;

  const EditNameDialogWidget({Key key, this.fullName, this.alias})
      : super(key: key);

  ///跳转方法
  static Future<Map<String, String>> navigatorPushDialog(
      BuildContext context, String fullName, String alias) {
    return VgDialogUtils.showCommonDialog<Map<String, String>>(
        barrierDismissible: true,
        context: context,
        child: EditNameDialogWidget(
          fullName: fullName,
          alias: alias,
        ));
  }

  @override
  _EditNameDialogWidgetState createState() => _EditNameDialogWidgetState();
}

const String EDIT_NAME_AND_NICK_BY_NAME_KEY = "fullName";
const String EDIT_NAME_AND_NICK_BY_NICK_KEY = "alias";

class _EditNameDialogWidgetState extends State<EditNameDialogWidget> {
  bool isEnableConfrim = true;

  TextEditingController _editingController;
  TextEditingController _editingAliasController;

  @override
  void initState() {
    super.initState();
    _editingController = TextEditingController();
    _editingAliasController = TextEditingController();
    _editingController.text = widget.fullName;
    _editingAliasController.text = widget.alias;
  }

  @override
  void dispose() {
    Future.value(() {
      _editingController?.dispose();
      _editingAliasController.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          RouterUtils.pop(context);
        },
        child: Center(
          child: GestureDetector(onTap: () {}, child: _toMainWidget(context)),
        ),
      ),
    );
  }

  Widget _toMainWidget(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Container(
        width: 290,
        decoration: BoxDecoration(
          color: Color(0xFF21263C),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 25),
            Text(
              "编辑",
              style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 16),
            ),
            SizedBox(height: 13),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: CommonSearchBarWidget(
                controller: _editingController,
                hintText: "姓名",
                onChanged: (String textName) {
                  print("zxx： onSubmitted: $textName");
                  if (textName == null || textName == "") {
                    isEnableConfrim = false;
                  } else {
                    isEnableConfrim = true;
                  }
                  setState(() {});
                },
                onSubmitted: (String textName) {
                  if (textName == null || textName == "") {
                    isEnableConfrim = false;
                  } else {
                    isEnableConfrim = true;
                  }
                  setState(() {});
                },
                customStyleFunc: (Widget textFieldWidget, Widget deleteIcon) {
                  return Container(
                    height: 45,
                    // padding: const EdgeInsets.only(top: 6),
                    decoration: BoxDecoration(
                        color: Color(0xFF191E31),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.5),
                              child: textFieldWidget,
                            ),
                          ),
                          deleteIcon
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: CommonSearchBarWidget(
                controller: _editingAliasController,
                hintText: "英文名/备注名",
                onChanged: (String textName) {
                  print("zxx： onSubmitted: $textName");
                  if (_editingController.text!=null && _editingController.text!="" && textName != null && textName != "") {
                    isEnableConfrim = true;
                  }
                  setState(() {});
                },
                onSubmitted: (String textName) {
                  if (textName != null && textName != "") {
                    isEnableConfrim = true;
                  }
                  setState(() {});
                },
                customStyleFunc: (Widget textFieldWidget, Widget deleteIcon) {
                  return Container(
                    height: 45,
                    // padding: const EdgeInsets.only(top: 6),
                    decoration: BoxDecoration(
                        color: Color(0xFF191E31),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.5),
                              child: textFieldWidget,
                            ),
                          ),
                          deleteIcon
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 25),
            _TabButtonWidget(
              isSelected: isEnableConfrim,
              onConfirm: () {
                if (_editingController?.text == null ||
                    _editingController.text.isEmpty) {
                  return;
                }
                // if (_editingAliasController?.text == null ||
                //     _editingAliasController.text.isEmpty) {
                //   return;
                // }
                Map<String, String> getNameAndAlias = {
                  EDIT_NAME_AND_NICK_BY_NAME_KEY: _editingController.text.trim(),
                  EDIT_NAME_AND_NICK_BY_NICK_KEY: _editingAliasController.text.trim(),
                };
                print("传出数据：：：${getNameAndAlias.toString()}");
                  RouterUtils.pop(context, result: getNameAndAlias);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _TabButtonWidget extends StatelessWidget {
  final String text;
  bool isSelected;
  final VoidCallback onConfirm;

  _TabButtonWidget({Key key, this.text, this.isSelected, this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                RouterUtils.pop(context);
              },
              child: Container(
                color: Color(0xFF191E31),
                child: Center(
                  child: Text(
                    text ?? "取消",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: VgColors.INPUT_BG_COLOR, fontSize: 14, height: 1.2),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: onConfirm,
              child: Container(
                // color: Colors.black,
                color: isSelected ? Color(0xFF1890FF) : Color(0xFF3A3F50),
                child: Center(
                  child: Text(
                    text ?? "确定",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        // color: Colors.amberAccent,
                        color:
                            isSelected ? Color(0xFFFFFFFF) : Color(0xFF5E687C),
                        fontSize: 14,
                        height: 1.2),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

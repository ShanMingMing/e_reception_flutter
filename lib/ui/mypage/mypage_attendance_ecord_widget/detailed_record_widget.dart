import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mypage/mypage_attendance_ecord_widget/detailed_bean/detailed_bean.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 打卡详情-列表

class DetailedRecordWidget extends StatelessWidget {

  final int index;

  final DetailedRecordsListItemBean itemBean;

  final int listLength;

  const DetailedRecordWidget({
    Key key,
    this.index,
    this.itemBean,
    this.listLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toListItemWidget();
  }

  Widget _toListItemWidget() {
    final bool isShowMax = VgToolUtils.isShowMaxTime(itemBean.mintime, itemBean?.maxtime);
    return Container(
      height: 50,
      child: DefaultTextStyle(
        style: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 14,
          fontFamily: "PingFang"),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            _toSplitWidget(),
            SizedBox(
              width: 12,
            ),
            Container(
              width: 70,
              child: Text(
                VgToolUtils.getMonthAndDayStr(itemBean?.mintime) ?? "-",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Text.rich(TextSpan(children: [
              TextSpan(text: "${VgToolUtils.getHourAndMinuteStr(itemBean.mintime)??"-"}"),
              TextSpan(text: isShowMax ? " 至 " : "", style: TextStyle(color: VgColors.INPUT_BG_COLOR)),
              TextSpan(text: "${!isShowMax ? "":VgToolUtils.getHourAndMinuteStr(itemBean.maxtime)??"-"}"),
            ])),
            Spacer(),
            _toDiffstrTimeStr(VgToolUtils.getDifferenceTimeStr(itemBean?.mintime, itemBean?.maxtime)),
            SizedBox(
              width: 15,
            )
          ],
        ),
      ),
    );
  }

  Column _toSplitWidget() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Opacity(
            opacity: index == 0 ? 0 : 1,
            child: Container(
              width: 0.5,
              color: ThemeRepository.getInstance().getLineColor_3A3F50(),
            ),
          ),
        ),
        Container(
          width: 7,
          height: 7,
          margin: const EdgeInsets.symmetric(vertical: 4),
          decoration: ShapeDecoration(
              shape: CircleBorder(),
              color: index == 0
                  ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                  : Color(0xFF3A3F50)),
        ),
        Expanded(
          child: Opacity(
            opacity: index == ((listLength ?? 1) - 1) ? 0 : 1,
            child: Container(
              width: 0.5,
              color: ThemeRepository.getInstance().getLineColor_3A3F50(),
            ),
          ),
        ),
      ],
    );
  }

  Widget _toDiffstrTimeStr(String diffStr) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Offstage(
          offstage: diffStr == null || diffStr == "",
          child: Padding(
            padding: const EdgeInsets.only(top: 0),
            child: Icon(
              Icons.access_time,
              color: VgColors.INPUT_BG_COLOR,
              size: 12,
            ),
          ),
        ),
        SizedBox(
          width: 4,
        ),
        Text(
          "${diffStr ?? ""}",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
              fontSize: 12,
              height: 1.1),
        )
      ],
    );
  }
}

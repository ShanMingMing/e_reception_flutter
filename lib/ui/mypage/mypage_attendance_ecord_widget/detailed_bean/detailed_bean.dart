import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"punchTime":1612746401,"mintime":1612746401,"orderday":"2021-02-08","maxtime":1612777946}],"total":1,"size":10,"current":1,"orders":[],"searchCount":true,"pages":1},"faceUserInfo":{"nick":"","number":"","groupName":"员工","phone":"","putpicurl":"http://etpic.we17.com/test/20210205134954_1594.jpg","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"盖热提江","napicurl":"http://etpic.we17.com/test/20210205134954_1594.jpg"}}
/// extra : null

class DetailedBean extends BasePagerBean<DetailedRecordsListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static DetailedBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DetailedBean detailedBeanBean = DetailedBean();
    detailedBeanBean.success = map['success'];
    detailedBeanBean.code = map['code'];
    detailedBeanBean.msg = map['msg'];
    detailedBeanBean.data = DataBean.fromMap(map['data']);
    detailedBeanBean.extra = map['extra'];
    return detailedBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<DetailedRecordsListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"punchTime":1612746401,"mintime":1612746401,"orderday":"2021-02-08","maxtime":1612777946}],"total":1,"size":10,"current":1,"orders":[],"searchCount":true,"pages":1}
/// faceUserInfo : {"nick":"","number":"","groupName":"员工","phone":"","putpicurl":"http://etpic.we17.com/test/20210205134954_1594.jpg","roleid":"10","groupid":"eadc02553f2a40509f05e407125f15e9","name":"盖热提江","napicurl":"http://etpic.we17.com/test/20210205134954_1594.jpg"}

class DataBean {
  PageBean page;
  FaceUserInfoBean faceUserInfo;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    dataBean.faceUserInfo = FaceUserInfoBean.fromMap(map['faceUserInfo']);
    return dataBean;
  }

  Map toJson() => {
    "page": page,
    "faceUserInfo": faceUserInfo,
  };
}

/// nick : ""
/// number : ""
/// groupName : "员工"
/// phone : ""
/// putpicurl : "http://etpic.we17.com/test/20210205134954_1594.jpg"
/// roleid : "10"
/// groupid : "eadc02553f2a40509f05e407125f15e9"
/// name : "盖热提江"
/// napicurl : "http://etpic.we17.com/test/20210205134954_1594.jpg"

class FaceUserInfoBean {
  String nick;
  String number;
  String groupName;
  String phone;
  String putpicurl;
  String roleid;
  String groupid;
  String name;
  String napicurl;

  static FaceUserInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceUserInfoBean faceUserInfoBean = FaceUserInfoBean();
    faceUserInfoBean.nick = map['nick'];
    faceUserInfoBean.number = map['number'];
    faceUserInfoBean.groupName = map['groupName'];
    faceUserInfoBean.phone = map['phone'];
    faceUserInfoBean.putpicurl = map['putpicurl'];
    faceUserInfoBean.roleid = map['roleid'];
    faceUserInfoBean.groupid = map['groupid'];
    faceUserInfoBean.name = map['name'];
    faceUserInfoBean.napicurl = map['napicurl'];
    return faceUserInfoBean;
  }

  Map toJson() => {
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "phone": phone,
    "putpicurl": putpicurl,
    "roleid": roleid,
    "groupid": groupid,
    "name": name,
    "napicurl": napicurl,
  };
}

/// records : [{"punchTime":1612746401,"mintime":1612746401,"orderday":"2021-02-08","maxtime":1612777946}]
/// total : 1
/// size : 10
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<DetailedRecordsListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => DetailedRecordsListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// punchTime : 1612746401
/// mintime : 1612746401
/// orderday : "2021-02-08"
/// maxtime : 1612777946

class DetailedRecordsListItemBean {
  int punchTime;
  int mintime;
  String orderday;
  int maxtime;

  static DetailedRecordsListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DetailedRecordsListItemBean recordsBean = DetailedRecordsListItemBean();
    recordsBean.punchTime = map['punchTime'];
    recordsBean.mintime = map['mintime'];
    recordsBean.orderday = map['orderday'];
    recordsBean.maxtime = map['maxtime'];
    return recordsBean;
  }

  Map toJson() => {
    "punchTime": punchTime,
    "mintime": mintime,
    "orderday": orderday,
    "maxtime": maxtime,
  };
}
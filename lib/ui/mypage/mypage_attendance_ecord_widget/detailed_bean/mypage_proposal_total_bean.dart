/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"allAndUnReadCnt":{"allCnt":64,"unreadCnt":0}}
/// extra : null

class MypageProposalTotalBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static MypageProposalTotalBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MypageProposalTotalBean mypageProposalTotalBeanBean = MypageProposalTotalBean();
    mypageProposalTotalBeanBean.success = map['success'];
    mypageProposalTotalBeanBean.code = map['code'];
    mypageProposalTotalBeanBean.msg = map['msg'];
    mypageProposalTotalBeanBean.data = DataBean.fromMap(map['data']);
    mypageProposalTotalBeanBean.extra = map['extra'];
    return mypageProposalTotalBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// allAndUnReadCnt : {"allCnt":64,"unreadCnt":0}

class DataBean {
  AllAndUnReadCntNumber allAndUnReadCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.allAndUnReadCnt = AllAndUnReadCntNumber.fromMap(map['allAndUnReadCnt']);
    return dataBean;
  }

  Map toJson() => {
    "allAndUnReadCnt": allAndUnReadCnt,
  };
}

/// allCnt : 64
/// unreadCnt : 0

class AllAndUnReadCntNumber {
  int allCnt;
  int unreadCnt;

  static AllAndUnReadCntNumber fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AllAndUnReadCntNumber allAndUnReadCntBean = AllAndUnReadCntNumber();
    allAndUnReadCntBean.allCnt = map['allCnt'];
    allAndUnReadCntBean.unreadCnt = map['unreadCnt'];
    return allAndUnReadCntBean;
  }

  Map toJson() => {
    "allCnt": allCnt,
    "unreadCnt": unreadCnt,
  };
}
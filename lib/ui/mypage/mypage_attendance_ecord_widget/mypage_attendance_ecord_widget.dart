import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mypage/mypage_attendance_ecord_widget/DetailedRecordViewModel.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'detailed_bean/detailed_bean.dart';
import 'detailed_record_widget.dart';

class FaceBrushRecordPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "FaceBrushRecordPage";

  final String fuid;

  const FaceBrushRecordPage({Key key, this.fuid}) : super(key: key);
  @override
  _FaceBrushRecordPageState createState() => _FaceBrushRecordPageState();

  ///跳转方法
  ///markedInfoBean 提前缓存加载（需要通用修改重构）
  static Future<dynamic> navigatorPush(BuildContext context, String fuid) {
    if(StringUtils.isEmpty(fuid)){
      VgToastUtils.toast(context, "进入详情异常");
      return null;
    }
    return RouterUtils.routeForFutureResult(
      context,
      FaceBrushRecordPage(fuid: fuid,),
      routeName: FaceBrushRecordPage.ROUTER,
    );
  }
}

class _FaceBrushRecordPageState extends BasePagerState<DetailedRecordsListItemBean,FaceBrushRecordPage> with VgPlaceHolderStatusMixin {
  DetailedRecordViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = DetailedRecordViewModel(this,widget?.fuid);
    _viewModel?.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
              child: _toTopBarWidget(),
            ),
            Expanded(
              child:_detailedRecordListWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _detailedRecordListWidget() {
    return Container(
      child: MixinPlaceHolderStatusWidget(
        emptyOnClick: ()=> _viewModel?.refresh(),
        errorOnClick: ()=>_viewModel?.refresh(),
        child: VgPullRefreshWidget.bind(
          viewModel: _viewModel,
          state: this,
          child: ListView.separated(
              itemCount: data?.length ?? 0,
              padding: const EdgeInsets.all(0),
              physics: BouncingScrollPhysics(),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              itemBuilder: (BuildContext context, int index) {
                return DetailedRecordWidget(index: index,itemBean: data?.elementAt(index),listLength: data?.length,);
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 0,
                );
              }),
        ),
      ),

    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "打卡记录" ,
    );
  }
}

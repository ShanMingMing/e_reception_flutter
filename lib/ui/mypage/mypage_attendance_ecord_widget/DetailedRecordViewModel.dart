import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/flutter_global_monitoring_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'detailed_bean/detailed_bean.dart';

class DetailedRecordViewModel
    extends BasePagerViewModel<DetailedRecordsListItemBean, DetailedBean> {
  final String fuid;

  // ValueNotifier<FaceUserInfoBean> infoValueNotifier;

  DetailedRecordViewModel(BaseState<StatefulWidget> state, this.fuid)
      : super(state) {
    // infoValueNotifier = ValueNotifier(null);
  }

  // @override
  // void onDisposed() {
  //   infoValueNotifier?.dispose();
  //   super.onDisposed();
  // }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "fuid": fuid ?? "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.POST;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/userPunchRecordDetails";
  }

  @override
  DetailedBean parseData(VgHttpResponse resp) {
    loading(false);
    DetailedBean vo = DetailedBean.fromMap(resp?.data);
    FlutterGlobalMonitoringUtils?.dataNotifier?.value = vo?.code;
    // infoValueNotifier?.value = vo?.data?.faceUserInfo;
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/vg_menu_widget.dart' as vgMenu;


class DeleteFileDialogUtils {

  static Future<dynamic> showMenuDialog({BuildContext context,
    LongPressStartDetails longPressDetails, Map<String, ValueChanged<String>> itemMap,
    VoidCallback onDelete, VoidCallback onDeleteForever}) {
    assert(itemMap != null, "items is not empty");


    return vgMenu.showMenu(
        elevation: 1,
        color: Color(0x000000),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0)
        ),
        useRootNavigator: true,
        captureInheritedThemes: false,
        context: context,
        position: RelativeRect.fromLTRB(
          longPressDetails.globalPosition.dx,
          longPressDetails.globalPosition.dy,
          longPressDetails.globalPosition.dx,
          longPressDetails.globalPosition.dy,
        ),

        items: itemMap.keys.map((String key){
          return vgMenu.PopupMenuItem<String>(
            value: key,
            child: Column(
              children: [
                Container(
                  width: 180,
                  height: 45,
                  decoration: BoxDecoration(
                    color: Color(0xFF303546),
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: (){
                          onDelete.call();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 89.5,
                          child: Text(
                            "删除",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 14,
                                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                            ),
                          ),
                        ),
                      ),
                      Divider(
                        height: 24,
                        color: Color(0XFF5E687C),
                        thickness: 1,
                      ),
                      GestureDetector(
                        onTap: (){
                          onDeleteForever.call();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 89.5,
                          child: Text(
                            "删除文件",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 14,
                                color: ThemeRepository.getInstance().getTextMainColor_D0E0F7()
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Image.asset(
                  "images/icon_arrow_down_grey.png",
                  width: 14,
                )
              ],
            ),
          );
        }).toList()
    );
  }


}
import 'dart:io';

import 'package:e_reception_flutter/common_widgets/common_bubble_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_red_num_widget/common_red_num_widget.dart';
import 'package:e_reception_flutter/singleton/amap_repository/amap_repository.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/index/bean/index_bind_termainal_info.dart';
import 'package:e_reception_flutter/ui/index/bean/index_top_info_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_page.dart';
import 'package:e_reception_flutter/utils/vg_location_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 首页-管理终端组件
///
/// @author: zengxiangxi
/// @createTime: 1/5/21 5:27 PM
/// @specialDemand:
class IndexManageTerminalBarWidget extends StatelessWidget {
  final IndexBindTermainalInfo topInfo;
  final String gps;
  IndexManageTerminalBarWidget({Key key, this.topInfo, this.gps}) : super(key: key);
  // bool _clickFlag = true;
  @override
  Widget build(BuildContext context) {
    int _bindTerinalList = topInfo?.data?.bindTerminalList?.length ?? 0;
    int _newTerminalCnt = topInfo?.data?.newTerminalCnt ?? 0;
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () async{
            if (_bindTerinalList == 0 && _newTerminalCnt == 0) {
              return;
            }
            // if(!_clickFlag){
            //   return;
            // }
            // _clickFlag = false;
            if(Platform.isAndroid){
              if(StringUtils.isNotEmpty(gps)){
                NewTerminalListPage.navigatorPush(context, gps: gps);
                print("使用首页的gps:" + gps);
                return;
              }
              if (await VgLocation().checkPermission() == false) {
                NewTerminalListPage.navigatorPush(context);
                print("无gps权限");
              }else{
                VgLocation().requestSingleLocation((value) {
                  Future<String> latlng = VgLocationUtils.getCacheLatLngString();
                  latlng.then((value){
                    print("gps:" + value);
                    NewTerminalListPage.navigatorPush(context, gps: value);
                    // _clickFlag = true;
                  });
                });
              }
            }else{
              NewTerminalListPage.navigatorPush(context, gps: gps);
            }
          },
          child: Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: <Widget>[
                Text(
                  // "终端显示屏·${topInfo?.bindTerminalList?.length ?? 0}",
                  "终端显示屏 ",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 17,
                      height: 1.22),
                ),
                Text(
                  "${topInfo?.data?.powerOnCnt??0.toString().toString()}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color:
                      ThemeRepository.getInstance().getHintGreenColor_00C6C4(),
                      fontSize: 17,
                      height: 1.2),
                ),
                Text(
                  "/",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color:
                      ThemeRepository.getInstance().getDefaultGrayColor_FF5E687C(),
                      fontSize: 17,
                      height: 1.2),
                ),
                Text(
                  "${topInfo?.data?.bindTerminalList?.length ?? 0}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 17,
                      height: 1.2),
                ),
                Spacer(),
                _isDeviceManagementText(_bindTerinalList, _newTerminalCnt),
                // _toRedDotWidget()
              ],
            ),
          ),
        ),
        // _bubbleDialog(_newTerminalCnt,_bindTerinalList),

      ],
    );
  }

  Widget _bubbleDialog(int _newTerminalCnt,int _bindTerinalList){
    if(_newTerminalCnt > 0 && _bindTerinalList == 0){
      return Positioned(
        top: 43.0,
        right: 15,
        child: Column(
          children: <Widget>[
            Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 150),
                  child: Image.asset(
                    "images/triangle_bubble_box.png",
                    height: 7,
                    width: 14,
                  ),
                )),
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Container(
                height: 32,
                width: 219,
                color: Color(0xFF1890FF),
                child: Padding(
                  padding: const EdgeInsets.only(left: 12,right: 12,top: 10,bottom: 6),
                  child: Text(
                    "检测到有终端显示屏发起绑定请求",
                    style: TextStyle(color: Color(0xFFFFFFFF), fontSize: 13,height: 1),textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
    return Container();
  }

  Widget _toRedDotWidget() {
    return Row(
      children: [
        Visibility(
            visible: (topInfo?.data?.newTerminalCnt ?? 0)>0,
            child: SizedBox(width: 4,)
        ),
        CommonRedNumWidget(
          topInfo?.data?.newTerminalCnt ?? 0,
          isShowZero: false,
        ),
      ],
    );
  }

  Widget _isDeviceManagementText(int _bindTerinalList, int _newTerminalCnt) {
    print("_bindTerinalList:" + _bindTerinalList.toString());
    print("_newTerminalCnt:" + _newTerminalCnt.toString());
    return Text(
      (_bindTerinalList != null && _bindTerinalList > 0) ? "设备管理" : "",
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style:
      TextStyle(color: Color(0xFFD0E0F7), fontSize: 14, height: 1.22),
    );

    // if (_bindTerinalList <= 0 && _newTerminalCnt <= 0) {
    //   return Text(
    //     _newTerminalCnt == null || _newTerminalCnt <= 0 ? "" : "",
    //     maxLines: 1,
    //     overflow: TextOverflow.ellipsis,
    //     style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14, height: 1.22),
    //   );
    // }
    //
    // return Text(
    //   _newTerminalCnt == null || _newTerminalCnt <= 0 ? "设备管理" : "新终端",
    //   maxLines: 1,
    //   overflow: TextOverflow.ellipsis,
    //   style:
    //   TextStyle(color: Color(0xFFD0E0F7), fontSize: 14, height: 1.22),
    // );
  }
}

// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/utils/screen_utils.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
//
// /// 首页-底部控制播放
// ///
// /// @author: zengxiangxi
// /// @createTime: 1/5/21 5:40 PM
// /// @specialDemand:
// class IndexBottomControlPlayWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final double bottomHeight = ScreenUtils.getBottomBarH(context);
//     return Container(
//       height: 55 + bottomHeight,
//       // alignment: Alignment.center,
//       padding: EdgeInsets.only(bottom: bottomHeight),
//       color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
//       child: _toMainRowWidget(),
//     );
//   }
//
//   Widget _toMainRowWidget() {
//     return Row(
//       children: <Widget>[
//         SizedBox(width: 15),
//         Image.asset("images/index_control_play_ico.png", width: 20),
//         SizedBox(width: 8),
//         Text(
//           "播放控制",
//           maxLines: 1,
//           overflow: TextOverflow.ellipsis,
//           style: TextStyle(color: Color(0xFFBFC2CC), fontSize: 17, height: 1.2),
//         ),
//         Spacer(),
//         _toRichCountWidget(),
//         SizedBox(width: 10,),
//         Image.asset("images/index_arrow_ico.png",width: 6,color: VgColors.INPUT_BG_COLOR,),
//         SizedBox(width: 15,)
//       ],
//     );
//   }
//
//   Widget _toRichCountWidget() {
//     return Text.rich(
//       TextSpan(
//         children: [
//           TextSpan(text: "13",style: TextStyle(color: ThemeRepository.getInstance().getPrimaryColor_1890FF())),
//           TextSpan(text: "视频，"),
//           TextSpan(text: "13",style: TextStyle(color: ThemeRepository.getInstance().getPrimaryColor_1890FF())),
//           TextSpan(text: "图片"),
//         ],
//       ),
//       style: TextStyle(color: Colors.white, fontSize: 13),
//     );
//   }
// }

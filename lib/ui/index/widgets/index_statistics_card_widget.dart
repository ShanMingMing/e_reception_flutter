import 'dart:async';
import 'dart:convert';

import 'package:e_reception_flutter/common_widgets/common_red_num_widget/common_red_num_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/check_in_record_index_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:e_reception_flutter/ui/index/bean/index_top_info_response_bean.dart';
import 'package:e_reception_flutter/ui/index/bean/index_top_punch_info.dart';
import 'package:e_reception_flutter/ui/index/index_view_model.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_sp_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// 首页-统计卡片
///
/// @author: zengxiangxi
/// @createTime: 1/5/21 5:20 PM
/// @specialDemand:
class IndexStatisticsCardWidget extends StatefulWidget {
  // final IndexTopInfoDataBean topInfo;
  final PVCntBean punchInfo;

  const IndexStatisticsCardWidget({Key key, this.punchInfo}) : super(key: key);

  @override
  _IndexStatisticsCardWidgetState createState() => _IndexStatisticsCardWidgetState();
}

class _IndexStatisticsCardWidgetState extends BaseState<IndexStatisticsCardWidget> {

  @override
  Widget build(BuildContext context) {

    // //获取到是否有打卡记录
    // int historyPunchCnt = widget?.punchInfo?.historyPunchCnt ?? 0;
    bool punchcntColorBar = false;
    //如果之前有数据，今日打卡无数据，颜色灰
    // if(historyPunchCnt!=0){
      if(widget?.punchInfo?.punchcnt == 0 && widget?.punchInfo?.visitorcnt == 0){
        punchcntColorBar = false;
        print("卡片：222");
      }else{
        punchcntColorBar = true;
        print("卡片：111");
      }
    // }else{
    //   print("卡片：000");
    //   punchcntColorBar = false;
    // }


    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if(!punchcntColorBar){
          return;
        }
        // CheckInRecordIndexPage.navigatorPush(context);
        CheckInStatisticsPage.navigatorPush(context);
      },
      child: Container(
        height: 70,
        margin: const EdgeInsets.symmetric(horizontal: 15),
        padding: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          color: punchcntColorBar ? ThemeRepository.getInstance().getPrimaryColor_1890FF() :Color(0xFF3A3F50) ,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          children: <Widget>[
            _toColumnWidget(),
            Spacer(),
            // _toRedDotWidget(),
            SizedBox(width: 7),
            Visibility(
              visible: punchcntColorBar,
              child: Image.asset(
                "images/index_arrow_ico.png",
                width: 6,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _toColumnWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "今日打卡：${widget?.punchInfo?.punchcnt ?? 0}人",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          "今日访客：${widget?.punchInfo?.visitorcnt ?? 0}人",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
        ),
      ],
    );
  }
}

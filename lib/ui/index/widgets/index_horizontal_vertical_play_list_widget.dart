import 'dart:async';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/media_detail_page.dart';
import 'package:e_reception_flutter/media_library/media_library_index/pages/poster_detail_page.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_diy_detail_page.dart';
import 'package:e_reception_flutter/ui/building_index/building_index_diy_list_response_bean.dart';
import 'package:e_reception_flutter/ui/index/widgets/terminal_operations_dialog.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/bean/terminal_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/terminal_detail_view_model.dart';
import 'package:e_reception_flutter/ui/new_terminal/terminal_detail/widgets/operation_success_tips_dialog.dart';
import 'package:e_reception_flutter/ui/smart_home/publish/smart_home_store_details_page.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_one_details_page.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_menu_widget/click_back_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 首页-今日播放文件展示
///
class IndexHorizontalVerticalPlayListWidget extends StatefulWidget {
  final List<TerminalDetailListItemBean> list;
  final int direction; //0纵向图片 1横向图片
  final String hsn;
  final String terminalName;
  final bool isOff;
  final double initOffSet;
  final String position;
  final String cbid;

  final ScrollController controller;

  final ValueChanged<TerminalDetailListItemBean> onRepeat;
  final bool showRepeat;

  const IndexHorizontalVerticalPlayListWidget(
      {Key key,
        this.list,
        this.direction,
        this.hsn,
        this.terminalName,
        this.initOffSet,
        this.controller,
        this.onRepeat,
        this.isOff,
        this.position,
        this.cbid,
        this.showRepeat,
      })
      : super(key: key);

  @override
  _IndexHorizontalVerticalPlayListWidgetState createState() =>
      _IndexHorizontalVerticalPlayListWidgetState();
}

class _IndexHorizontalVerticalPlayListWidgetState
    extends BaseState<IndexHorizontalVerticalPlayListWidget> {
  ScrollController _controller;
  Stream<double> _stream;
  double _opacityLevel = 1.0;
  ValueNotifier<double> _opacityLevelValueNotifier;
  TerminalDetailViewModel _viewModel;
  String _hsn;
  double _fixScale = 9 / 16;

  double _containerHeight = 0;
  double _containerWidth = 0;

  @override
  void initState() {
    _hsn = widget?.hsn;
    _viewModel = TerminalDetailViewModel(this, widget.hsn);
    _controller = widget.controller;
    //监听滚动事件，打印滚动位置
    _controller.addListener(() {
      // print('1111111：${_controller.offset}'); //打印滚动位置
      // if (_controller.offset < 1000 ) {
      //   setState(() {
      //     // showToTopBtn = false;
      //   });
      // } else if (_controller.offset >= 1000) {
      //   setState(() {
      //     // showToTopBtn = true;
      //   });
      // }
    });
    super.initState();
    _opacityLevelValueNotifier = new ValueNotifier(1.0);
    _stream = Stream.periodic(Duration(milliseconds: 500), (value) {
      _opacityLevel = _opacityLevel == 0 ? 1.0 : 0.0;
      return _opacityLevel;
    });

    _stream.listen((event) {
      _opacityLevelValueNotifier.value = event;
    });

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _containerWidth = ((ScreenUtils.screenW(context) - 15 - 16) / 2.5);
    _containerHeight =  _containerWidth * 244 / 137;
    _hsn = widget?.hsn;
    return (widget?.list?.length ?? 0) == 0
        ? SizedBox()
        : GridView.builder(
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        itemCount: widget?.list?.length ?? 0,
        physics: ClampingScrollPhysics(),
        controller: _controller,
        scrollDirection:
        widget?.direction == 0 ? Axis.horizontal : Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: widget?.direction == 0 ? 1 : 2,
          mainAxisSpacing: 8,
          crossAxisSpacing: widget?.direction == 0 ? 0 : 8,
          childAspectRatio: 244 / 137,
        ),
        itemBuilder: (BuildContext context, int index) {
          return _toGridItemWidget(context, widget?.list?.elementAt(index));
        });
    // _controller.animateTo(0,);
    // if(item.controller.hasClients){
    //   item.controller.jumpTo(0);
    // }
  }

  Future showRightMenu(BuildContext context, dx, dy, Widget menu) async {
    double sw = MediaQuery.of(context).size.width; //屏幕宽度
    double sh = MediaQuery.of(context).size.height; //屏幕高度
    Border border = dy < sh / 2
        ? //
    Border(top: BorderSide(color: Colors.green[200], width: 2))
        : Border(bottom: BorderSide(color: Colors.green[200], width: 2));
    Size sSize = MediaQuery.of(context).size;

    // PopupMenuItem
    double menuW = 180; //菜单宽度
    double menuH = 52; //菜单高度
    //判断手势位置在屏幕的那个区域以判断最好的弹出方向
    double endL = dx - menuW / 2;
    double endT = dy < sh / 2 ? dy : dy - menuH;
    double endR = dx + menuW / 2;
    double endB = dy < sh / 2 ? dy + menuH : dy;

    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          //由于用了组件放大的动画效果，所以用了SingleChildScrollView包裹
          //否则在组件小的时候会出现菜单超出编辑的错误
          return SingleChildScrollView(child: menu);
        },
        barrierColor: Colors.grey.withOpacity(0),
        //弹窗后的背景遮罩色，调来调去还是透明的顺眼
        barrierDismissible: true,
        barrierLabel: "",
        transitionDuration: Duration(milliseconds: 100),
        //动画时间
        transitionBuilder: (context, anim1, anim2, child) {
          return Stack(
            children: [
              // 有好多种Transition来实现不同的动画效果，可以参考官方API
              PositionedTransition(
                  rect: RelativeRectTween(
                    begin: RelativeRect.fromSize(
                      //动画起始位置与元素大小
                        Rect.fromLTWH(dx, dy, 1, 1),
                        sSize),
                    end: RelativeRect.fromSize(
                      //动画结束位置与元素大小
                        Rect.fromLTRB(endL, endT, endR, endB),
                        sSize),
                  ).animate(
                      CurvedAnimation(parent: anim1, curve: Curves.easeIn)),
                  child: child)
            ],
          );
        });
  }

  Widget _toMenuWidget(TerminalDetailListItemBean itemBean){
    if(itemBean.isSmart()){
      return Column(
        children: [
          Container(
            width: 90,
            height: 45,
            decoration: BoxDecoration(
              color: Color(0xFF303546),
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            alignment: Alignment.center,
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                Navigator.pop(context);
                _onDeleteTap(itemBean);
              },
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  "删除",
                  style: TextStyle(
                    fontSize: 14,
                    decoration: TextDecoration.none,
                    color: ThemeRepository.getInstance()
                        .getTextMainColor_D0E0F7(),
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          Image.asset(
            "images/icon_arrow_down_grey.png",
            width: 14,
            color: Color(0xFF303546),
          )
        ],
      );
    }
    return Column(
      children: [
        Container(
          width: 180,
          height: 45,
          decoration: BoxDecoration(
            color: Color(0xFF303546),
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          alignment: Alignment.center,
          child: Flex(
            direction: Axis.horizontal,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                flex: 1,
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.pop(context);
                    _onDeleteTap(itemBean);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      "取消播放",
                      style: TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.none,
                        color: ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7(),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 24,
                color: Color(0XFF5E687C),
                width: 1,
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.pop(context);
                    _onDeleteForever(itemBean);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      "删除文件",
                      style: TextStyle(
                        fontSize: 14,
                        decoration: TextDecoration.none,
                        color: ThemeRepository.getInstance()
                            .getTextMainColor_D0E0F7(),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Image.asset(
          "images/icon_arrow_down_grey.png",
          width: 14,
          color: Color(0xFF303546),
        )
      ],
    );
  }

  Widget _toGridItemWidget(
      BuildContext context, TerminalDetailListItemBean itemBean) {
    BoxFit boxFit = BoxFit.contain;
    int width = 1;
    int height = 1;
    if (StringUtils.isNotEmpty(itemBean?.getPicSize())) {
      width = int.parse(itemBean?.getPicSize()?.split("*")[0]);
      height = int.parse(itemBean?.getPicSize()?.split("*")[1]);
    }
    double scale = width / height;
    if (_fixScale > scale) {
      boxFit = BoxFit.cover;
    } else {
      boxFit = BoxFit.contain;
    }
    return ClickBackgroundWidget(
      backgroundColor: Colors.transparent,
      selectColor: Colors.transparent,
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (itemBean.isSmart()) {
          // SmartHomeOneDetailsPage.navigatorPush(context, itemBean?.shid, "", itemBean?.name, itemBean?.logo);
          SmartHomeStoreDetailsPage.navigatorPush(context, itemBean?.shid, "", itemBean?.name, itemBean?.logo);
        } else if (itemBean.isIndex()) {
          BuildingIndexDiyBean buildingIndexBean = new BuildingIndexDiyBean();
          buildingIndexBean.id = itemBean?.comwid;
          BuildingIndexDiyDetailPage.navigatorPush(
              context, buildingIndexBean, false);
        } else if (itemBean.isHtml()) {
          PosterDetailPage.navigatorPush(
            context,
            itemBean.htmlurl,
            itemBean.id,
            itemBean.likeflg,
            itemBean.cretype,
            widget?.hsn,
            false,
            itemBean.logoflg,
            itemBean.addressflg,
            itemBean.diyflg,
            "01",
            itemBean.addressdiy,
            itemBean.namediy,
            itemBean.logodiy,
            itemBean.splpicurl,
            itemBean.pictype,
            position: widget?.position ?? "",
            cbid: widget?.cbid ?? "",
          );
        } else {
          MediaDetailPage.navigatorPush(context, itemBean?.picid, "01",
              id: itemBean?.id, branchname: widget?.position ?? "");
        }
        // SingleTerminalMangePage.navigatorPush(context, widget?.hsn, widget?.terminalName);
      },
      onLongPress: (detail) async {
        // Navigator.pop(context);
        if(UserRepository.getInstance().isSmartHomeCompany()){
          return;
        }
        var menuResult = await showRightMenu(
            context,
            detail.globalPosition.dx,
            detail.globalPosition.dy,
            _toMenuWidget(itemBean),
        );
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          decoration: BoxDecoration(color: Colors.transparent),
          child: Stack(
            fit: StackFit.expand,
//          children: <Widget>[
//            terminalGradientColorWidget(itemBean?.picurl, list, context),
//
//          ],
            children: <Widget>[
              (itemBean?.isSmart() ?? false)
                  ? _toSmartHomeWidget(itemBean)
                  : VgCacheNetWorkImage(
                itemBean?.getCoverUrl() ?? "",
                fit: boxFit,
                imageQualityType: ImageQualityType.middleDown,
              ),
              Center(
                child: Offstage(
                    offstage: !(itemBean?.isVideo() ?? false),
                    child: Opacity(
                      opacity: 0.5,
                      child: Image.asset(
                        "images/video_play_ico.png",
                        width: 52,
                      ),
                    )),
              ),
              Visibility(
                visible: itemBean?.isSmart(),
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
//                Color(0xFF191E31),
                        Colors.black.withOpacity(0.0),
                        Colors.black.withOpacity(0.0),
                        Colors.black.withOpacity(0.0),
                        Colors.black.withOpacity(0.0),
                        Colors.black.withOpacity(0.0),
                        Colors.black.withOpacity(0.1),
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.6),
//                Colors.black.withOpacity(0.7),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: itemBean?.isSmart(),
                child:   Container(
                  margin: const EdgeInsets.only(left:10, right: 10, top: 8, bottom: 8),
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    "${itemBean?.name?? ""}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color:
                      ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                    ),
                  ),

                ),
              ),
              Visibility(
                visible: widget?.showRepeat??true,
                child: Positioned(
                  top: 7,
                  left: 7,
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () async {
                      VgEventBus.global.send(new UpdateHomePageEventMonitor());
                      if (widget?.isOff ?? true) {
                        OperationSuccessTipsDialog.navigatorPushDialog(context,
                            title: "设备已关机，无法设置重复播放");
                        return;
                      }
                      if ("00" == itemBean.stopflg) {
                        bool result =
                        await TerminalOperationsDialog.navigatorPushDialog(
                            context,
                            title: "循环播放当前文件？",
                            cancelText: "取消",
                            confirmText: "确定",
                            confirmBgColor: ThemeRepository.getInstance()
                                .getPrimaryColor_1890FF());
                        if (result ?? false) {
                          itemBean.hsn = widget?.hsn;
                          itemBean.editStopFlg =
                          ("01" == itemBean.stopflg) ? "02" : "01";
                          widget?.onRepeat?.call(itemBean);
                        }
                      } else {
                        itemBean.hsn = widget?.hsn;
                        itemBean.editStopFlg =
                        ("01" == itemBean.stopflg) ? "02" : "01";
                        widget?.onRepeat?.call(itemBean);
                      }
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      alignment: Alignment.topLeft,
                      child: _toRepeatWidget(itemBean),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _toSmartHomeWidget(TerminalDetailListItemBean itemBean) {
    if (itemBean?.picList == null || (itemBean?.picList?.length ?? 0) == 0) {
      return Container();
    }
    if ((itemBean?.picList?.length ?? 0) == 1) {
      return _toSingleImageWidget(itemBean);
    } else if ((itemBean?.picList?.length  ?? 0) == 2) {
      return _toTwoImageWidget(itemBean);
    } else if ((itemBean?.picList?.length  ?? 0) == 3) {
      return _toThreeImageWidget(itemBean);
    } else {
      //4个及以上
      return _toFourImageWidget(itemBean);
    }
  }

  ///单图
  Widget _toSingleImageWidget(TerminalDetailListItemBean itemBean){
    BoxFit boxFit = BoxFit.contain;
    int width = 1;
    int height = 1;
    String picsize = itemBean?.picList[0].picsize;
    String picurl = itemBean?.picList[0].getPicUrl();
    if (StringUtils.isNotEmpty(picsize)) {
      width = int.parse(picsize?.split("*")[0]);
      height = int.parse(picsize?.split("*")[1]);
    }
    double scale = width / height;
    if (_fixScale > scale) {
      boxFit = BoxFit.cover;
    } else {
      boxFit = BoxFit.contain;
    }
    return VgCacheNetWorkImage(
      picurl ?? "",
      fit: BoxFit.cover,
      imageQualityType: ImageQualityType.middle,
    );
  }

  ///2图
  Widget _toTwoImageWidget(TerminalDetailListItemBean itemBean){
    String picurl1 = itemBean?.picList[0].getPicUrl();
    String picurl2 = itemBean?.picList[1].getPicUrl();
    return Column(
      children: [
        Expanded(
          child: VgCacheNetWorkImage(
            picurl1 ?? "",
            width: _containerWidth,
            height: (_containerHeight - 2)/2,
            fit: BoxFit.cover,
            imageQualityType: ImageQualityType.middle,
          ),
        ),
        Container(
          height: 2,
          color: Colors.white,
        ),
        Expanded(
          child: VgCacheNetWorkImage(
            picurl2 ?? "",
            width: _containerWidth,
            height: (_containerHeight - 2)/2,
            fit: BoxFit.cover,
            imageQualityType: ImageQualityType.middle,
          ),
        ),
      ],
    );
  }

  ///3图
  Widget _toThreeImageWidget(TerminalDetailListItemBean itemBean){
    String picurl1 = itemBean?.picList[0].getPicUrl();
    String picurl2 = itemBean?.picList[1].getPicUrl();
    String picurl3 = itemBean?.picList[2].getPicUrl();
    return Column(
      children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl1 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
              Container(
                width: 2,
                color: Colors.white,
              ),
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl2 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 2,
          color: Colors.white,
        ),
        Expanded(
          child: VgCacheNetWorkImage(
            picurl3 ?? "",
            width: _containerWidth,
            height: (_containerHeight - 2)/2,
            fit: BoxFit.cover,
            imageQualityType: ImageQualityType.middle,
          ),
        ),
      ],
    );
  }

  ///4图及以上
  Widget _toFourImageWidget(TerminalDetailListItemBean itemBean){
    String picurl1 = itemBean?.picList[0].getPicUrl();
    String picurl2 = itemBean?.picList[1].getPicUrl();
    String picurl3 = itemBean?.picList[2].getPicUrl();
    String picurl4 = itemBean?.picList[3].getPicUrl();
    return Column(
      children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl1 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
              Container(
                width: 2,
                color: Colors.white,
              ),
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl2 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 2,
          color: Colors.white,
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl3 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
              Container(
                width: 2,
                color: Colors.white,
              ),
              Expanded(
                child: VgCacheNetWorkImage(
                  picurl4 ?? "",
                  width: (_containerWidth - 2)/2,
                  height: (_containerHeight - 2)/2,
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middle,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Map<String, ValueChanged> _getMenuMap(
      BuildContext context, TerminalDetailListItemBean itemBean) {
    return {
      "取消播放": (_) {
        _onDeleteTap(itemBean);
      },
      "删除文件": (_) {
        _onDeleteForever(itemBean);
      }
    };
  }

  _onDeleteTap(TerminalDetailListItemBean itemBean) async {
    if (itemBean.isIndex()) {
      bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
          title: "提示",
          content: "仅在当前终端显示屏取消播放，确认不再播放？",
          cancelText: "取消",
          confirmText: "确定",
          confirmBgColor:
          ThemeRepository.getInstance().getMinorRedColor_F95355());
      if (result ?? false) {
        VgEventBus.global.send(new UpdateHomePageEventMonitor());
        _viewModel.deleteTerminalBuildingIndex(
            context, widget?.hsn, itemBean?.buildingIndexId);
      }
      return;
    }
    ///智能家居删除
    if(itemBean.isSmart()){
      bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
          title: "提示",
          content: "仅在当前终端显示屏取消播放，确认不再播放？",
          cancelText: "取消",
          confirmText: "确定",
          confirmBgColor:
          ThemeRepository.getInstance().getMinorRedColor_F95355());
      if (result ?? false) {
        VgEventBus.global.send(new UpdateHomePageEventMonitor());
        _viewModel.deleteSmartHome(context, itemBean?.shid, widget?.hsn);
      }
      return;
    }
    if (StringUtils.isEmpty(itemBean?.picid)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: "仅在当前终端显示屏取消播放，确认不再播放？",
        cancelText: "取消",
        confirmText: "确定",
        confirmBgColor:
        ThemeRepository.getInstance().getMinorRedColor_F95355());
    if (result ?? false) {
      VgEventBus.global.send(new UpdateHomePageEventMonitor());
      if ("09" == (itemBean?.pictype ?? "")) {
        //删除合成海报
        _viewModel.deleteComposePoster(
            context, widget?.hsn, itemBean?.picid, "01", itemBean?.splpicurl);
      } else if ("01" == (itemBean?.uploadflg ?? "")) {
        //删除推送海报
        _viewModel.deleteTodayPushPoster(context, widget?.hsn, itemBean?.id);
      } else {
        _viewModel?.deleteFolder(context, itemBean.picid, currentHsn: _hsn);
      }
    }
  }

  _onDeleteForever(TerminalDetailListItemBean itemBean) async {
    if (itemBean.isIndex()) {
      bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
          title: "提示",
          content: "所有终端显示屏中该文件都会删除，且不可找回，确认继续？",
          cancelText: "取消",
          confirmText: "删除",
          confirmBgColor:
          ThemeRepository.getInstance().getMinorRedColor_F95355());
      if (result ?? false) {
        VgEventBus.global.send(new UpdateHomePageEventMonitor());
        _viewModel.foreverDeleteTerminalBuildingIndex(
            context, itemBean?.comwid);
      }
      return;
    }
    if (StringUtils.isEmpty(itemBean?.picid)) {
      VgToastUtils.toast(context, "资源获取失败");
      return;
    }
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: "所有终端显示屏中该文件都会删除，且不可找回，确认继续？",
        cancelText: "取消",
        confirmText: "删除",
        confirmBgColor:
        ThemeRepository.getInstance().getMinorRedColor_F95355());
    if (result ?? false) {
      VgEventBus.global.send(new UpdateHomePageEventMonitor());
      _viewModel?.deleteForeverFolder(context, itemBean.picid, itemBean?.videoid, itemBean?.getToDeletePicUrl(), (itemBean?.isVideo()??false)?(itemBean?.picurl??""):"");
    }
  }

  ///重复图标布局
  Widget _toRepeatWidget(TerminalDetailListItemBean itemBean) {
    return (("01" == itemBean?.stopflg ?? "") && !(widget?.isOff ?? false))
        ? ValueListenableBuilder(
        valueListenable: _opacityLevelValueNotifier,
        builder: (BuildContext context, double opacityLevel, Widget child) {
          return AnimatedOpacity(
            opacity: opacityLevel,
            duration: Duration(milliseconds: 500),
            curve: Curves.linear,
            child: SizedBox(
              width: 20,
              height: 20,
              child: Image.asset(
                "images/icon_repeat.png",
                width: 20,
                height: 20,
              ),
            ),
          );
        })
        : SizedBox(
      width: 20,
      height: 20,
      child: Image.asset(
        "images/icon_repeat_empty.png",
        width: 20,
        height: 20,
      ),
    );
  }

  Widget terminalGradientColorWidget(
      String terminalPicurl, List list, BuildContext context) {
    return Container(
//          width: list.length <=2
//              ? (ScreenUtils.screenW(context) - 30)/2 - 8
//              :(ScreenUtils.screenW(context) - 15)/2.5 -16,
//          height: list.length <=2
//              ? ((ScreenUtils.screenW(context) - 30)/2 - 8)*244/137
//              :((ScreenUtils.screenW(context) - 15)/2.5 -16)*244/137,
      child: VgCacheNetWorkImage(
        terminalPicurl ?? "",
        fit: BoxFit.cover,
        imageQualityType: ImageQualityType.middleDown,
        errorWidget: Image.asset("images/termial_empty_place_holder_ico.png"),
        placeWidget: Image.asset("images/termial_empty_place_holder_ico.png"),
      ),
    );
  }
}

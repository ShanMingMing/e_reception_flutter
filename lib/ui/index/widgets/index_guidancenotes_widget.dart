import 'dart:collection';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_bubble_dialog.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/index/bean/index_bind_termainal_info.dart';
import 'package:e_reception_flutter/ui/index/bean/index_top_info_response_bean.dart';
import 'package:e_reception_flutter/ui/index/even/location_changed_event.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/terminal_scan_page.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/widgets/terminal_bind_success_dialog.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/rich_text_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import '../index_view_model.dart';
import 'index_grid_terminal_widget.dart';

class IndexGuidanceNotesWidget extends StatefulWidget {
  //判断是否绑定终端
  final IndexBindTermainalInfo topInfo;
  final IndexViewModel viewModel;
  final String gps;
  final bool lightTheme;

  const IndexGuidanceNotesWidget({Key key, this.viewModel, this.topInfo, this.gps, this.lightTheme}) : super(key: key);
  @override
  _IndexGuidanceNotesWidgetState createState() => _IndexGuidanceNotesWidgetState();
}

class _IndexGuidanceNotesWidgetState extends State<IndexGuidanceNotesWidget> {

  String _latestGps;

  @override
  void initState() {
    super.initState();
    _latestGps = widget?.gps;

    VgEventBus.global.streamController.stream.listen((event) {
      if (event is LocationChangedEvent) {
        print("IndexGuidanceNotesWidget 收到gps更新的:" + event?.gps);
        if(StringUtils.isNotEmpty(event?.gps??"")){
          if(event?.gps == _latestGps){
            print("更新的gps与当前一致，不执行刷新");
            return;
          }
          _latestGps = event?.gps;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
         // child: judgeTerminalDisplay(),
        color: (widget?.lightTheme??false)?Colors.white:null,
        child:  judge(),
      ),
    );
  }

  //4个步骤判断
Widget judge(){
    //是否有新终端
  int _newTerminalCnt = widget?.topInfo?.data?.newTerminalCnt ?? 0;
    //终端列表是否有数据
  int _bindTerminalList = widget.topInfo?.data?.bindTerminalList?.length ?? 0;
  // int isThereUser = widget.topInfo?.data?.comPersonCnt ?? 0;
  // return judgeTerminalDisplay(_bindTerminalList);
  //如果终端列表是空则提示步骤
    if(_bindTerminalList==0){
      return judgeTerminalDisplay(_bindTerminalList);
    }
    //如果有新终端提示步骤2
    if(_newTerminalCnt>=0){
      //人脸版和智慧版才展示
      bool showFace = UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition();
      if(showFace??false){
        return  _toBubbleDialog(_bindTerminalList,1);
      }
    }

}
//提示框
  Widget _toBubbleDialog(int _newTerminalCnt,int isThereUser){
    //当终端大于等于1且无用户时提示
    if(isThereUser==1){
      return Container(
        height: 52,
        width: 102,
        child: Bubble(
          direction: BubbleDirection.left,
          color: Colors.blue,
          child: Text(
            "添加用户人脸  启用刷脸功能",
            style: TextStyle(color: Color(0xFFFFFFFF), fontSize: 13,),textAlign: TextAlign.center,
          ),
        ),
      );
    }
    return Container();
  }

  //当无终端时提示步骤1
  Widget judgeTerminalDisplay(_bindTerminalList) {
    String asset  = (widget?.lightTheme??false)?"images/icon_un_bind_light.png":"images/index_unbound_ico.png";
    if(_bindTerminalList==0){
    // if(0==0){
      return ScrollConfiguration(
        behavior: MyBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Container(
                  height: 120,
                  width: 112,
                  // padding: EdgeInsets.only(top: 30),
                  child: Image.asset(asset)),
              SizedBox(
                height: 9,
              ),
              Text(
                "该账号未绑定任何终端显示屏",
                style: TextStyle(color: (widget?.lightTheme??false)?Color(0XFF8B93A5):VgColors.INPUT_BG_COLOR, fontSize: 14),
              ),
              // RichTextWidget(fontSize:14,contentMap: LinkedHashMap.from({"购买使用显示屏事宜请联系":VgColors.INPUT_BG_COLOR,
              //   '010-87562142':ThemeRepository.getInstance().getPrimaryColor_1890FF() }),),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "购买使用显示屏事宜请联系",
                    style: TextStyle(
                        fontSize: 13, color: (widget?.lightTheme??false)?Color(0XFF8B93A5):VgColors.INPUT_BG_COLOR),
                  ),
                  GestureDetector(
                    onTap: (){
                      launch("tel://18665289540");
                    },
                    child: Text(
                      "18665289540",
                      style: TextStyle(
                          fontSize: 14,
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                        height: 1.35
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: (widget?.lightTheme??false)?Color(0XFFF6F7F9):ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
                  border: Border.all(
                      color: (widget?.lightTheme??false)?Color(0XFFE5E5E5):Color(0xFF303546),
                      width: 1),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 15),
                          child: Container(
                            child: Text(
                              "使用须知：",
                              style: TextStyle(
                                  fontSize: 14, color: (widget?.lightTheme??false)?ThemeRepository.getInstance().getCardBgColor_21263C():Color(0xFFFFB714), fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(32),
                            child: Container(
                              height: 20,
                              width: 20,
                              color: (widget?.lightTheme??false)?Color(0XFF8B93A5):Color(0xFFFFB714),
                              child: Text(
                                "1",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: (widget?.lightTheme??false)?Colors.white:Color(0xFF191E31),
                                    height: 1.5),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Container(
                            child: Text(
                              "启动终端显示屏，并确保成功联网",
                              style: TextStyle(
                                  fontSize: 14, color: (widget?.lightTheme??false)?ThemeRepository.getInstance().getCardBgColor_21263C():Color(0xFFFFB714)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(32),
                            child: Container(
                              height: 20,
                              width: 20,
                              color: (widget?.lightTheme??false)?Color(0XFF8B93A5):Color(0xFFFFB714),
                              child: Text(
                                "2",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: (widget?.lightTheme??false)?Colors.white:Color(0xFF191E31),
                                    height: 1.5),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Container(
                            child: Text(
                              "扫描屏幕上的二维码完成绑定确认",
                              style: TextStyle(
                                  fontSize: 14, color: (widget?.lightTheme??false)?ThemeRepository.getInstance().getCardBgColor_21263C():Color(0xFFFFB714)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    GestureDetector(
                      onTap: ()async{
                        bool result = await TerminalScanPage.navigatorPush(context, _latestGps, AppMain.ROUTER);
                        if(result != null){
                          widget.viewModel?.getHomeTerminalInfo(0, gps: _latestGps);
                          // widget.viewModel?.getPunchAndTerminalInfo(0, gps: _latestGps);
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: 160,
                        height: 40,
                        decoration: BoxDecoration(
                          color: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                            "images/icon_scan.png",
                              width: 14,
                            ),
                            SizedBox(width: 8,),
                            Text(
                              "扫一扫",
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
              // SizedBox(height: 5,),
              // Container(
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: <Widget>[
              //       Text(
              //         "注：购买使用显示屏事宜请联系 ",
              //         style: TextStyle(
              //             fontSize: 12, color: Color(0xFF808388)),
              //       ),
              //       FlatButton(
              //         padding: const EdgeInsets.only(right: 7),
              //         onPressed: () => launch("tel://010-87562142"),
              //         child: Text(
              //           "010-87562142",
              //           style: TextStyle(
              //             fontSize: 12,
              //             color: Colors.blue,
              //             // height: 1.5,
              //             decoration: TextDecoration.underline,
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              SizedBox(
                height: NAV_HEIGHT + 30,
              )
            ],
          ),
        ),
      );
    }
    return VgPlaceHolderStatusWidget(
        emptyStatus: widget?.topInfo?.data?.bindTerminalList == null ||
            widget?.topInfo?.data?.bindTerminalList?.isEmpty,
        emptyOnClick: () => widget.viewModel?.getHomeTerminalInfo(0, gps: _latestGps),
        child: IndexGridTerminalWidget(
            list: widget?.topInfo?.data?.bindTerminalList));
  }
}

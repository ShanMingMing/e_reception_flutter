import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/common_terminal_settings_response_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'index_top_info_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"newTerminalCnt":0,"bindTerminalList":[{"terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","createdate":1626764730,"volume":53,"stopflg":"00","terminalName":"终端显示屏2","soundonoff":"00","reportHsnLastTime":"2021-07-20 15:53:44","gpslat":"39.855207","screenStatus":"02","gpslng":"116.368732","juli":"9136533","position":"位置2","autoOnoffTime":"","racid":938,"onOff":0}],"powerOnCnt":0}
/// extra : null

class IndexBindTermainalInfo {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static IndexBindTermainalInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndexBindTermainalInfo indexBindTermainalInfoBean = IndexBindTermainalInfo();
    indexBindTermainalInfoBean.success = map['success'];
    indexBindTermainalInfoBean.code = map['code'];
    indexBindTermainalInfoBean.msg = map['msg'];
    indexBindTermainalInfoBean.data = DataBean.fromMap(map['data']);
    indexBindTermainalInfoBean.extra = map['extra'];
    return indexBindTermainalInfoBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// newTerminalCnt : 0
/// bindTerminalList : [{"terminalPicurl":"","hsn":"1572BA5FCA24E0E6F66B7A67ED3E636CA0","createdate":1626764730,"volume":53,"stopflg":"00","terminalName":"终端显示屏2","soundonoff":"00","reportHsnLastTime":"2021-07-20 15:53:44","gpslat":"39.855207","screenStatus":"02","gpslng":"116.368732","juli":"9136533","position":"位置2","autoOnoffTime":"","racid":938,"onOff":0}]
/// powerOnCnt : 0

class DataBean {
  int newTerminalCnt;
  List<NewBindTerminalListBean> bindTerminalList;
  int powerOnCnt;
  PVCntBean PVCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.newTerminalCnt = map['newTerminalCnt'];
    dataBean.bindTerminalList = List()..addAll(
      (map['bindTerminalList'] as List ?? []).map((o) => NewBindTerminalListBean.fromMap(o))
    );
    dataBean.powerOnCnt = map['powerOnCnt'];
    dataBean.PVCnt = PVCntBean.fromMap(map['PVCnt']);
    return dataBean;
  }

  Map toJson() => {
    "newTerminalCnt": newTerminalCnt,
    "bindTerminalList": bindTerminalList,
    "powerOnCnt": powerOnCnt,
    "PVCnt": PVCnt,
  };
}

/// terminalPicurl : ""
/// hsn : "1572BA5FCA24E0E6F66B7A67ED3E636CA0"
/// createdate : 1626764730
/// volume : 53
/// stopflg : "00"
/// terminalName : "终端显示屏2"
/// soundonoff : "00"
/// reportHsnLastTime : "2021-07-20 15:53:44"
/// gpslat : "39.855207"
/// screenStatus : "02"
/// gpslng : "116.368732"
/// juli : "9136533"
/// position : "位置2"
/// autoOnoffTime : ""
/// racid : 938
/// onOff : 0

class NewBindTerminalListBean {
  String terminalName;
  String terminalPicurl;
  String backup;
  String hsn;
  String position;
  String cbid;
  String stopflg;//00 不重复 01重复
  int onOff;//0关机 1开机
  String screenStatus;//00开 01息屏 02关机
  String logout;//00正常 01登出
  int rcaid;
  String autoOnoffTime;
  String autoOnoffWeek;
  String soundonoff;//00开 01关
  int volume;//音量
  String reportHsnLastTime;
  String sideNumber;
  String address;
  String branchname;
  String autoOnoff;
  ComAutoSwitchBean comAutoSwitch;

  String getTerminalName(){
    String name = "终端显示屏";
    if(StringUtils.isNotEmpty(terminalName)){
      name = terminalName;
    }else if(StringUtils.isNotEmpty(sideNumber)){
      name = sideNumber;
    }else if(StringUtils.isNotEmpty(hsn)){
      name = hsn;
    }
    if(name.length > 12){
      return name.substring(0, 12) + "...";
    }
    return name;
  }


  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus)|| ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }

  SetTerminalStateType getTerminalState(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return SetTerminalStateType.off;
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return SetTerminalStateType.screenOff;
      }else{
        return SetTerminalStateType.open;
      }
    }
    return SetTerminalStateType.off;
  }

  static NewBindTerminalListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    NewBindTerminalListBean bindTerminalListBean = NewBindTerminalListBean();
    bindTerminalListBean.terminalName = map['terminalName'];
    bindTerminalListBean.terminalPicurl = map['terminalPicurl'];
    bindTerminalListBean.backup = map['backup'];
    bindTerminalListBean.hsn = map['hsn'];
    bindTerminalListBean.position = map['position'];
    bindTerminalListBean.stopflg = map['stopflg'];
    bindTerminalListBean.onOff = map['onOff'];
    bindTerminalListBean.screenStatus = map['screenStatus'];
    if(map['rcaid'] != null){
      bindTerminalListBean.rcaid = map['rcaid'];
    }else{
      bindTerminalListBean.rcaid = map['racid'];
    }
    bindTerminalListBean.autoOnoffTime = map['autoOnoffTime'];
    bindTerminalListBean.autoOnoffWeek = map['autoOnoffWeek'];
    bindTerminalListBean.soundonoff = map['soundonoff'];
    bindTerminalListBean.volume = map['volume'];
    bindTerminalListBean.reportHsnLastTime = map['reportHsnLastTime'];
    bindTerminalListBean.logout = map['logout'];
    bindTerminalListBean.sideNumber = map['sideNumber'];
    bindTerminalListBean.address = map['address'];
    bindTerminalListBean.branchname = map['branchname'];
    bindTerminalListBean.autoOnoff = map['autoOnoff'];
    bindTerminalListBean.cbid = map['cbid'];
    bindTerminalListBean.comAutoSwitch = ComAutoSwitchBean.fromMap(map);
    return bindTerminalListBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "backup": backup,
    "hsn":hsn,
    "position":position,
    "stopflg":stopflg,
    "onOff":onOff,
    "screenStatus":screenStatus,
    "rcaid":rcaid,
    "autoOnoffTime":autoOnoffTime,
    "autoOnoffWeek":autoOnoffWeek,
    "soundonoff":soundonoff,
    "volume":volume,
    "reportHsnLastTime":reportHsnLastTime,
    "logout":logout,
    "sideNumber":sideNumber,
    "address":address,
    "branchname":branchname,
    "autoOnoff":autoOnoff,
    "comAutoSwitch":comAutoSwitch,
    "cbid":cbid,
  };
}
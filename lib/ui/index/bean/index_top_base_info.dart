import 'index_top_info_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"companyInfo":{"companyid":"3fb8275ca5584d51a8bdb4172f35c877","comefrom":"00","status":"01","companyname":"","companynick":"0720机构新注册","companylogo":null,"type":"01","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"addrProvince":"北京市","addrCity":"北京市","addrDistrict":"丰台区","addrCode":"110106","enableFaceRecognition":"00","createuid":"5f0980b626d446d2badbfb62c6fc473e","createdate":1626764456,"updatetime":1626764456,"updateuid":"5f0980b626d446d2badbfb62c6fc473e","testflg":"00","deposit":0,"paymoney":0,"roundnum":1,"majorflg":"00","groupList":null,"bjtime":"2021-07-20T15:00:56"},"roleid":"99","manageCnt":4,"noFaceCnt":4,"comPersonCnt":4}
/// extra : null

class IndexTopBaseInfo {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static IndexTopBaseInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndexTopBaseInfo indexTopBaseInfoBean = IndexTopBaseInfo();
    indexTopBaseInfoBean.success = map['success'];
    indexTopBaseInfoBean.code = map['code'];
    indexTopBaseInfoBean.msg = map['msg'];
    indexTopBaseInfoBean.data = DataBean.fromMap(map['data']);
    indexTopBaseInfoBean.extra = map['extra'];
    return indexTopBaseInfoBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// companyInfo : {"companyid":"3fb8275ca5584d51a8bdb4172f35c877","comefrom":"00","status":"01","companyname":"","companynick":"0720机构新注册","companylogo":null,"type":"01","city":"{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}","gps":null,"address":null,"addrProvince":"北京市","addrCity":"北京市","addrDistrict":"丰台区","addrCode":"110106","enableFaceRecognition":"00","createuid":"5f0980b626d446d2badbfb62c6fc473e","createdate":1626764456,"updatetime":1626764456,"updateuid":"5f0980b626d446d2badbfb62c6fc473e","testflg":"00","deposit":0,"paymoney":0,"roundnum":1,"majorflg":"00","groupList":null,"bjtime":"2021-07-20T15:00:56"}
/// roleid : "99"
/// manageCnt : 4
/// noFaceCnt : 4
/// comPersonCnt : 4

class DataBean {
  CompanyTopInfoBean companyInfo;
  String roleid;
  int manageCnt;
  int noFaceCnt;
  int comPersonCnt;
  PVCntBean PVCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.companyInfo = CompanyTopInfoBean.fromMap(map['companyInfo']);
    dataBean.roleid = map['roleid'];
    dataBean.manageCnt = map['manageCnt'];
    dataBean.noFaceCnt = map['noFaceCnt'];
    dataBean.comPersonCnt = map['comPersonCnt'];
    dataBean.PVCnt = PVCntBean.fromMap(map['PVCnt']);
    return dataBean;
  }

  Map toJson() => {
    "companyInfo": companyInfo,
    "roleid": roleid,
    "manageCnt": manageCnt,
    "noFaceCnt": noFaceCnt,
    "comPersonCnt": comPersonCnt,
    "PVCnt": PVCnt,
  };
}

/// companyid : "3fb8275ca5584d51a8bdb4172f35c877"
/// comefrom : "00"
/// status : "01"
/// companyname : ""
/// companynick : "0720机构新注册"
/// companylogo : null
/// type : "01"
/// city : "{\"province\":\"北京市\",\"city\":\"北京市\",\"district\":\"丰台区\",\"code\":\"110106\"}"
/// gps : null
/// address : null
/// addrProvince : "北京市"
/// addrCity : "北京市"
/// addrDistrict : "丰台区"
/// addrCode : "110106"
/// enableFaceRecognition : "00"
/// createuid : "5f0980b626d446d2badbfb62c6fc473e"
/// createdate : 1626764456
/// updatetime : 1626764456
/// updateuid : "5f0980b626d446d2badbfb62c6fc473e"
/// testflg : "00"
/// deposit : 0
/// paymoney : 0
/// roundnum : 1
/// majorflg : "00"
/// groupList : null
/// bjtime : "2021-07-20T15:00:56"

class CompanyTopInfoBean {
  String companyid;
  String status;
  String companyname;
  String companynick;
  String companylogo;
  dynamic type;
  String city;
  dynamic gps;
  dynamic address;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;

  static CompanyTopInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyTopInfoBean companyInfoBean = CompanyTopInfoBean();
    companyInfoBean.companyid = map['companyid'];
    companyInfoBean.status = map['status'];
    companyInfoBean.companyname = map['companyname'];
    companyInfoBean.companynick = map['companynick'];
    companyInfoBean.companylogo = map['companylogo'];
    companyInfoBean.type = map['type'];
    companyInfoBean.city = map['city'];
    companyInfoBean.gps = map['gps'];
    companyInfoBean.address = map['address'];
    companyInfoBean.createuid = map['createuid'];
    companyInfoBean.createdate = map['createdate'];
    companyInfoBean.updatetime = map['updatetime'];
    companyInfoBean.updateuid = map['updateuid'];
    return companyInfoBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "status": status,
    "companyname": companyname,
    "companynick": companynick,
    "companylogo": companylogo,
    "type": type,
    "city": city,
    "gps": gps,
    "address": address,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
  };
}
import 'index_top_info_response_bean.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"PVCnt":{"historyPunchCnt":3,"punchcnt":0,"visitorcnt":0}}
/// extra : null

class IndexTopPunchInfo {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static IndexTopPunchInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndexTopPunchInfo indexTopPunchInfoBean = IndexTopPunchInfo();
    indexTopPunchInfoBean.success = map['success'];
    indexTopPunchInfoBean.code = map['code'];
    indexTopPunchInfoBean.msg = map['msg'];
    indexTopPunchInfoBean.data = DataBean.fromMap(map['data']);
    indexTopPunchInfoBean.extra = map['extra'];
    return indexTopPunchInfoBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// PVCnt : {"historyPunchCnt":3,"punchcnt":0,"visitorcnt":0}

class DataBean {
  PVCntBean PVCnt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.PVCnt = PVCntBean.fromMap(map['PVCnt']);
    return dataBean;
  }

  Map toJson() => {
    "PVCnt": PVCnt,
  };
}

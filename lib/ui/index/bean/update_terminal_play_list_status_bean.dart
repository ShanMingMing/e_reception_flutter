/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"obtainflg":"00"}
/// extra : null

class UpdateTerminalPlayListStatusBean {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static UpdateTerminalPlayListStatusBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    UpdateTerminalPlayListStatusBean updateTerminalPlayListStatusBeanBean = UpdateTerminalPlayListStatusBean();
    updateTerminalPlayListStatusBeanBean.success = map['success'];
    updateTerminalPlayListStatusBeanBean.code = map['code'];
    updateTerminalPlayListStatusBeanBean.msg = map['msg'];
    updateTerminalPlayListStatusBeanBean.data = DataBean.fromMap(map['data']);
    updateTerminalPlayListStatusBeanBean.extra = map['extra'];
    return updateTerminalPlayListStatusBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// obtainflg : "00"

class DataBean {
  String obtainflg;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.obtainflg = map['obtainflg'];
    return dataBean;
  }

  Map toJson() => {
    "obtainflg": obtainflg,
  };
}
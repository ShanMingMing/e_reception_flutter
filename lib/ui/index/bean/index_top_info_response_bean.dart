import 'package:e_reception_flutter/generated/l10n.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/bean/set_terminal_state_type.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"newTerminalCnt":0,"bindTerminalList":[{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼2909前台哈哈哈哈"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼2909前台的嘻嘻嘻嘻嘻嘻嘻嘻哈哈哈哈"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼1606前台哈哈哈哈"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在李小岁的工位对面 哈哈哈哈"},{"terminalName":"嘻嘻BeiJing北京的终端2343545233333333","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"保存呀这个机器安在李小岁的工位这儿 哈哈哈哈嘻嘻嘻嘻嘻嘻嘻嘻"}],"companyInfo":{"companyid":"55a438b79b7c4cd3bf7fec1f603e774a","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技1","companylogo":"http://etpic.we17.com/test/20210115134601_3102.jpg","type":null,"city":"{ \t\"province\":\"北京市\", \t\"city\":\"北京市\", \t\"district\":\"大兴区\", \t\"code\":\"\" }","gps":null,"address":null,"createuid":"bd7ca4fcdfc442049622761636b2f0a4","createdate":1610532915,"updatetime":1610532915,"updateuid":"bd7ca4fcdfc442049622761636b2f0a4"},"roleid":"99","manageCnt":0,"unSignCnt":73,"PVCnt":{"punchcnt":0,"visitorcnt":0}}
/// extra : null

class IndexTopInfoResponseBean {
  bool success;
  String code;
  String msg;
  IndexTopInfoDataBean data;
  dynamic extra;

  static IndexTopInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndexTopInfoResponseBean indexTopInfoResponseBeanBean = IndexTopInfoResponseBean();
    indexTopInfoResponseBeanBean.success = map['success'];
    indexTopInfoResponseBeanBean.code = map['code'];
    indexTopInfoResponseBeanBean.msg = map['msg'];
    indexTopInfoResponseBeanBean.data = IndexTopInfoDataBean.fromMap(map['data']);
    indexTopInfoResponseBeanBean.extra = map['extra'];
    return indexTopInfoResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// newTerminalCnt : 0
/// bindTerminalList : [{"terminalName":"北京搜宝的终端","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼2909前台哈哈哈哈"},{"terminalName":"南京的终端呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼2909前台的嘻嘻嘻嘻嘻嘻嘻嘻哈哈哈哈"},{"terminalName":"南京的终端12321呀呀呀哈哈","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在搜宝3号楼1606前台哈哈哈哈"},{"terminalName":"北京的终端233333333","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"这个机器安在李小岁的工位对面 哈哈哈哈"},{"terminalName":"嘻嘻BeiJing北京的终端2343545233333333","terminalPicurl":"http://etpic.we17.com/test/20210115134601_3102.jpg","backup":"保存呀这个机器安在李小岁的工位这儿 哈哈哈哈嘻嘻嘻嘻嘻嘻嘻嘻"}]
/// companyInfo : {"companyid":"55a438b79b7c4cd3bf7fec1f603e774a","status":"00","companyname":"北京湛腾世纪科技有限公司","companynick":"湛腾科技1","companylogo":"http://etpic.we17.com/test/20210115134601_3102.jpg","type":null,"city":"{ \t\"province\":\"北京市\", \t\"city\":\"北京市\", \t\"district\":\"大兴区\", \t\"code\":\"\" }","gps":null,"address":null,"createuid":"bd7ca4fcdfc442049622761636b2f0a4","createdate":1610532915,"updatetime":1610532915,"updateuid":"bd7ca4fcdfc442049622761636b2f0a4"}
/// roleid : "99"
/// manageCnt : 0
/// unSignCnt : 73
/// PVCnt : {"punchcnt":0,"visitorcnt":0}

class IndexTopInfoDataBean {
  int newTerminalCnt;
  List<BindTerminalListBean> bindTerminalList;
  CompanyInfoBean companyInfo;
  String roleid;
  int manageCnt;
  int unSignCnt;
  PVCntBean PVCnt;
  int comPersonCnt;
  int noFaceCnt;
  int powerOnCnt;//开机数目
  bool isShow;

  static IndexTopInfoDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    IndexTopInfoDataBean dataBean = IndexTopInfoDataBean();
    dataBean.newTerminalCnt = map['newTerminalCnt'];
    dataBean.bindTerminalList = List()..addAll(
      (map['bindTerminalList'] as List ?? []).map((o) => BindTerminalListBean.fromMap(o))
    );
    dataBean.companyInfo = CompanyInfoBean.fromMap(map['companyInfo']);
    dataBean.roleid = map['roleid'];
    dataBean.manageCnt = map['manageCnt'];
    dataBean.unSignCnt = map['unSignCnt'];
    dataBean.PVCnt = PVCntBean.fromMap(map['PVCnt']);
    dataBean.comPersonCnt = map['comPersonCnt'];
    dataBean.noFaceCnt = map['noFaceCnt'];
    dataBean.powerOnCnt = map['powerOnCnt'];
    return dataBean;
  }

  Map toJson() => {
    "newTerminalCnt": newTerminalCnt,
    "bindTerminalList": bindTerminalList,
    "companyInfo": companyInfo,
    "roleid": roleid,
    "manageCnt": manageCnt,
    "unSignCnt": unSignCnt,
    "PVCnt": PVCnt,
    "comPersonCnt": comPersonCnt,
    "noFaceCnt": noFaceCnt,
    "powerOnCnt": powerOnCnt,
  };
}

/// punchcnt : 0
/// visitorcnt : 0

class PVCntBean {
  int punchcnt;
  int visitorcnt;
  int  historyPunchCnt;

  static PVCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PVCntBean pVCntBean = PVCntBean();
    pVCntBean.punchcnt = map['punchcnt'];
    pVCntBean.visitorcnt = map['visitorcnt'];
    pVCntBean.historyPunchCnt = map['historyPunchCnt'];
    return pVCntBean;
  }

  Map toJson() => {
    "punchcnt": punchcnt,
    "visitorcnt": visitorcnt,
    "historyPunchCnt": historyPunchCnt,
  };
}

/// companyid : "55a438b79b7c4cd3bf7fec1f603e774a"
/// status : "00"
/// companyname : "北京湛腾世纪科技有限公司"
/// companynick : "湛腾科技1"
/// companylogo : "http://etpic.we17.com/test/20210115134601_3102.jpg"
/// type : null
/// city : "{ \t\"province\":\"北京市\", \t\"city\":\"北京市\", \t\"district\":\"大兴区\", \t\"code\":\"\" }"
/// gps : null
/// address : null
/// createuid : "bd7ca4fcdfc442049622761636b2f0a4"
/// createdate : 1610532915
/// updatetime : 1610532915
/// updateuid : "bd7ca4fcdfc442049622761636b2f0a4"

class CompanyInfoBean {
  String companyid;
  String status;
  String companyname;
  String companynick;
  String companylogo;
  dynamic type;
  String city;
  dynamic gps;
  dynamic address;
  String createuid;
  int createdate;
  int updatetime;
  String updateuid;

  static CompanyInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CompanyInfoBean companyInfoBean = CompanyInfoBean();
    companyInfoBean.companyid = map['companyid'];
    companyInfoBean.status = map['status'];
    companyInfoBean.companyname = map['companyname'];
    companyInfoBean.companynick = map['companynick'];
    companyInfoBean.companylogo = map['companylogo'];
    companyInfoBean.type = map['type'];
    companyInfoBean.city = map['city'];
    companyInfoBean.gps = map['gps'];
    companyInfoBean.address = map['address'];
    companyInfoBean.createuid = map['createuid'];
    companyInfoBean.createdate = map['createdate'];
    companyInfoBean.updatetime = map['updatetime'];
    companyInfoBean.updateuid = map['updateuid'];
    return companyInfoBean;
  }

  Map toJson() => {
    "companyid": companyid,
    "status": status,
    "companyname": companyname,
    "companynick": companynick,
    "companylogo": companylogo,
    "type": type,
    "city": city,
    "gps": gps,
    "address": address,
    "createuid": createuid,
    "createdate": createdate,
    "updatetime": updatetime,
    "updateuid": updateuid,
  };
}

/// terminalName : "北京搜宝的终端"
/// terminalPicurl : "http://etpic.we17.com/test/20210115134601_3102.jpg"
/// backup : "这个机器安在搜宝3号楼2909前台哈哈哈哈"

class BindTerminalListBean {
  String terminalName;
  String terminalPicurl;
  String backup;
  String hsn;
  String position;
  String stopflg;//00 不重复 01重复
  int onOff;//0关机 1开机
  String screenStatus;//00开 01息屏 02关机
  String logout;
  int rcaid;
  String autoOnoffTime;
  String soundonoff;//00开 01关
  int volume;//音量
  String reportHsnLastTime;


  bool getTerminalIsOff(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return true;
    }
    return false;
  }

  SetTerminalStateType getTerminalState(){
    if((onOff == null || onOff == 0) || ("01" == screenStatus) || ("02" == screenStatus) || ("01" == logout)){
      return SetTerminalStateType.off;
    }
    if(onOff == 1){
      if("01" == screenStatus){
        return SetTerminalStateType.screenOff;
      }else{
        return SetTerminalStateType.open;
      }
    }
    return SetTerminalStateType.off;
  }

  static BindTerminalListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BindTerminalListBean bindTerminalListBean = BindTerminalListBean();
    bindTerminalListBean.terminalName = map['terminalName'];
    bindTerminalListBean.terminalPicurl = map['terminalPicurl'];
    bindTerminalListBean.backup = map['backup'];
    bindTerminalListBean.hsn = map['hsn'];
    bindTerminalListBean.position = map['position'];
    bindTerminalListBean.stopflg = map['stopflg'];
    bindTerminalListBean.onOff = map['onOff'];
    bindTerminalListBean.screenStatus = map['screenStatus'];
    bindTerminalListBean.rcaid = map['rcaid'];
    bindTerminalListBean.autoOnoffTime = map['autoOnoffTime'];
    bindTerminalListBean.soundonoff = map['soundonoff'];
    bindTerminalListBean.volume = map['volume'];
    bindTerminalListBean.reportHsnLastTime = map['reportHsnLastTime'];
    bindTerminalListBean.logout = map['logout'];
    return bindTerminalListBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "terminalPicurl": terminalPicurl,
    "backup": backup,
    "hsn":hsn,
    "position":position,
    "stopflg":stopflg,
    "onOff":onOff,
    "screenStatus":screenStatus,
    "rcaid":rcaid,
    "autoOnoffTime":autoOnoffTime,
    "soundonoff":soundonoff,
    "volume":volume,
    "reportHsnLastTime":reportHsnLastTime,
    "logout":logout,
  };
}
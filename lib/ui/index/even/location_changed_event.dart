import 'package:e_reception_flutter/utils/vg_location_utils.dart';

///定位更新
class LocationChangedEvent{
  final String gps;
  final LatLngBean latLngBean;
  LocationChangedEvent(this.gps, this.latLngBean);
}
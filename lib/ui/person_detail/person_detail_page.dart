import 'dart:async';

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/common_title_edit_with_underline_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/bean/user_response_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/company_choose_terminal_list_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_address_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_branch_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/event/refresh_company_detail_page_event.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/relative_stu/select_relative_stu_list_page.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_info_response_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/bean/edit_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/create_user/edit_user/edit_user_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_course_class/course_class_bean/class_course_bean.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/create_user_address/create_user_address_attend_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/multi_select_class_course_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/multi_select_department_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/personal_details_information_widget/add_user_address_page.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/personal_details_information_widget/personal_class_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/personal_details_information_widget/personal_department_widget.dart';
import 'package:e_reception_flutter/ui/create_user/user_selectsort/update_task_view_model.dart';
import 'package:e_reception_flutter/ui/mypage/toolTipBoxNewPage.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_view_model.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_delete.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/common_select_util.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/vg_place_holder_status_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'widgets/person_detail_group_widget.dart';
import 'widgets/person_detail_info_widget.dart';
import 'widgets/person_detail_list_item_widget.dart';

/// 人员详情
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 4:12 PM
/// @specialDemand:
class PersonDetailPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "PersonDetailPage";

  final String fuid;

  final String fid;

  final String pages;

  //是否是未激活
  final bool isEnActive;

  const PersonDetailPage({Key key, this.fuid, this.fid, this.pages, this.isEnActive})
      : super(key: key);

  @override
  PersonDetailPageState createState() => PersonDetailPageState();

  ///跳转方法
  ///markedInfoBean 提前缓存加载（需要通用修改重构）
  static Future<dynamic> navigatorPush(BuildContext context, String fuid,
      {String fid,bool isEnActive, String pages}) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "进入详情异常");
      return null;
    }
    return RouterUtils.routeForFutureResult(
      context,
      PersonDetailPage(
        fuid: fuid,
        fid: fid,
        pages: pages,
        isEnActive: isEnActive,
      ),
      routeName: PersonDetailPage.ROUTER,
    );
  }
}

class PersonDetailPageState
    extends BasePagerState<PersonDetailListItemBean, PersonDetailPage>
    with VgPlaceHolderStatusMixin {
  PersonDetailViewModel _viewModel;
  StreamSubscription updateChooseSubscription;
  StreamSubscription refreshPersonSubscription;
  // FaceUserInfoBean infoBean;
  List<PersonDetailListItemBean> records;
  FaceUserInfoBean infoDataBean;
  // EditUserInfoDataBean infoDataBean0;
  EditUserUploadBean uploadBean;
  PersonDetailResponseBean personDetailResponseBean;

  List<FuserBranchListBean> fuserBranchList;

  TextEditingController _identityController;
  TextEditingController _terminalController;
  TextEditingController _departmentController;
  TextEditingController _classController;

  String studentNameStr = "";
  String groupidStudent = "";
  List<String> listStudentFuid;

  String classNameStr = "";

  List<String> fuserAddressNameList = new List<String>();
  String fuserAddressNames = "";
  ///是否显示身份和终端
  bool isShowIdentityAndTerminal = false;

  List<CompanyBranchListInfoBean> listBranchBean;

  bool isSchool = UserRepository.getInstance().userData.companyInfo.isSchoolLike();
  List<GroupPersonCntBean>  listGroup = List<GroupPersonCntBean>();
  String _fid = "";
  @override
  void initState() {
    super.initState();
    fuserBranchList = List<FuserBranchListBean>();
    uploadBean = EditUserUploadBean(infoDataBean?.napicurl);
    _viewModel = PersonDetailViewModel(this, widget?.fuid);
    _viewModel.infoValueNotifier.addListener(() async {
      personDetailResponseBean = _viewModel.infoValueNotifier?.value;
      if(StringUtils.isNotEmpty(personDetailResponseBean?.data?.faceUserInfo?.fid)){
        _fid = personDetailResponseBean?.data?.faceUserInfo?.fid;
      }else if(StringUtils.isNotEmpty(widget?.fid)){
        _fid = widget?.fid;
      }
      _addressInfo();
      // _viewModel.getUserInfoHttp(widget.fuid);
      _refreshInfo();
    });
    // _viewModel.userInfoValueNotifier.addListener(_refreshInfo);
    //观察者模式,监听接口是否使用
    updateChooseSubscription =
        VgEventBus.global.on<UpdateChooseInfoRefreshEvent>().listen((event) {
          _viewModel.infoValueNotifier.addListener((){
            personDetailResponseBean = _viewModel.infoValueNotifier?.value;
          });
          setState(() {});
        });
    //观察者模式,监听接口是否使用
    refreshPersonSubscription =
        VgEventBus.global.on<RefreshPersonDetailEvent>().listen((event) {
          _viewModel?.refresh();
          setState(() {});
        });
    _viewModel?.refresh();
    _identityController = TextEditingController(text: '');
    _terminalController = TextEditingController(text: "");
    _departmentController = TextEditingController(text: "");
    _classController = TextEditingController(text: "");
  }

  void _addressInfo(){
    listBranchBean = List<CompanyBranchListInfoBean>();
    if(fuserAddressNameList != null){
      fuserAddressNameList.clear();
    }else{
      fuserAddressNameList = new List<String>();
    }
    fuserBranchList = personDetailResponseBean?.data?.fuserBranchList;
    fuserBranchList?.forEach((element) {
      CompanyBranchListInfoBean addbean = CompanyBranchListInfoBean();
      addbean?.cbid = element?.cbid;
      addbean?.address = element?.address;
      addbean?.selected = true;
      addbean?.addrProvince = element?.addrProvince;
      addbean?.addrCity = element?.addrCity;
      addbean?.addrDistrict = element?.addrDistrict;
      addbean?.branchname = element?.branchname;
      listBranchBean?.add(addbean);
      fuserAddressNameList?.add(element?.getAddressStr());
    });
    // fuserAddressNames = _addressStr(fuserAddressNameList);
    fuserAddressNames = VgStringUtils?.getMultipleStr(fuserAddressNameList);
  }

  void _refreshInfo() {
    listStudentFuid = new List<String>();
    infoDataBean = personDetailResponseBean?.data?.faceUserInfo;
    // infoBean = personDetailResponseBean?.data?.faceUserInfo;
    records = personDetailResponseBean?.data?.page?.records;
    //赋值初始化
    uploadBean
      ..createuid = infoDataBean.createuid
      ..fid = infoDataBean?.fid
      ..fuid = widget.fuid
      ..userid = infoDataBean?.userid
      ..nick = infoDataBean?.nick
      ..name = infoDataBean?.name
      ..phone = infoDataBean?.phone
      ..cityId = infoDataBean?.city
      ..cityName = infoDataBean?.city
      ..projectId = infoDataBean?.projectid
      ..projectName = infoDataBean?.projectname
      ..departmentId = infoDataBean?.departmentid
      ..departmentName = infoDataBean?.department
      ..gradeId = (infoDataBean?.showClassInfo?.map((e) => e?.classid)?.toList()?.join(',')) ?? '' //所在班级id
      ..gradeName = (infoDataBean?.showClassInfo?.map((e) => e?.classname)?.toList()?.join('、')) ?? '' //所在班级名
      ..classList = infoDataBean?.showClassInfo?.map((e) => ClassCourseListItemBean(e?.classname, e?.classid))?.toList() //所在班级
      ..userNumber = infoDataBean?.number
      ..identityType = CompanyAddUserEditInfoIdentityExtension.getIdToTypeStr(
          infoDataBean?.roleid)
      ..identityName = CompanyAddUserEditInfoIdentityExtension?.getIdToTypeStr(infoDataBean?.roleid)?.getTypeToStr()
      ..isManagerTerminal = VgRoleUtils.isSuperAdmin(infoDataBean?.roleid) || VgRoleUtils.isCommonAdmin(infoDataBean?.roleid)
      ..headFaceUrl = VgStringUtils.getFirstNotEmptyStrByList([infoDataBean?.putpicurl, infoDataBean?.napicurl,])
      ..terminalList = personDetailResponseBean?.data?.manageInfo?.MHsn?.map((e) {return CompanyChooseTerminalListItemBean()
        ..hsn = e?.hsn
        ..terminalName = e?.terminalName;})?.toList()
      ..classIdList = personDetailResponseBean?.data?.manageInfo?.MClass?.map((e) => e?.classid)?.toList()
      ..managerClassName =uploadBean?.splitManagerClassName(personDetailResponseBean?.data?.manageInfo?.MClass?.map((e) => e?.classname)?.toList()?.join(','))
      ..managerDepartmentName =uploadBean?.splitDepartment(personDetailResponseBean?.data?.manageInfo?.MDepartment?.map((e) => e?.department)?.toList()?.join(','))
      ..departmentList = personDetailResponseBean?.data?.manageInfo?.MDepartment?.map((e) => e?.departmentid)?.toList()
      ..groupBean = (GroupListBean()
        ..groupType = infoDataBean?.groupType
        ..groupid = infoDataBean?.groupid
        ..groupName = infoDataBean?.groupName);
    Future.delayed(Duration(milliseconds: 0), () {
      setState(() {});
    });
    isShowIdentityAndTerminal = uploadBean.isManagerTerminal ?? false;
    uploadBean.isManagerTerminal = isShowIdentityAndTerminal;

    _identityController.text = uploadBean?.identityName;

    _departmentController.text = uploadBean.isSuperAdminRole()
        ? "全部${uploadBean?.departmentList?.length??0}个部门"
        : uploadBean.managerDepartmentName;

    _classController.text = uploadBean.isSuperAdminRole()
        ? "全部${uploadBean?.classIdList?.length??0}个班级"
        : uploadBean.managerClassName ?? "";
    _terminalController.text = uploadBean.isSuperAdminRole()
        ? "全部${uploadBean?.terminalList?.length??0}台终端显示屏"
        : _getTerminalEditValueStr(uploadBean?.terminalList);
    List<String> listStudentName = new List<String>();
    //获取关联学员信息
    if (infoDataBean?.groupName == "家长") {
      if(UserRepository.getInstance().userData.comUser.roleid == "99"){
        personDetailResponseBean?.data?.studentInfo?.forEach((element) {
          if(element?.nick!=null && element?.nick!=""){
            listStudentName?.add("${element?.name}/${element?.nick}");
          }else{
            listStudentName?.add(element?.name);
          }

          listStudentFuid?.add(element?.fuid);
          groupidStudent = element?.groupid;
        });
      }else{
        personDetailResponseBean?.data?.showStudentInfo?.forEach((element) {
          if(element?.nick!=null && element?.nick!=""){
            listStudentName?.add("${element?.name}/${element?.nick}");
          }else{
            listStudentName?.add(element?.name);
          }

          listStudentFuid?.add(element?.fuid);
          groupidStudent = element?.groupid;
        });
      }

      studentNameStr = VgStringUtils.getSplitStr(listStudentName, symbol: "、");
    }
    List<String> listClassName = new List<String>();
    infoDataBean?.showClassInfo?.forEach((element) {
      listClassName?.add(element?.classname);
    });
    classNameStr = VgStringUtils?.getSplitStr(listClassName, symbol: "、");
  }

  void _notifyValue() {
    print("数据更新了: ${uploadBean.toString()}");
    // String errorMsg = uploadBean.checkAllowConfirmForErrorMsg();
  }

  @override
  void dispose() {
    uploadBean.removeListener(_notifyValue);
    updateChooseSubscription.cancel();
    refreshPersonSubscription?.cancel();
    _terminalController.dispose();
    _classController.dispose();
    _departmentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: _toColumnWidget(),
    );
  }

  Widget _toColumnWidget() {
    return VgPlaceHolderStatusWidget(
      loadingStatus: personDetailResponseBean==null || infoDataBean==null,
      child: Column(
        children: <Widget>[
          _toTopBarWidget(infoDataBean),
          _toColumnInfoWidget(personDetailResponseBean),
          SizedBox(
            height: 8,
          ),
          _toVeryCareWidget(),
          // _toEnActiveWidget(),
          SizedBox(
            height: 8,
          ),
          if (infoDataBean?.roleid != "10" &&
              infoDataBean?.fuid !=
                  UserRepository.getInstance()?.userData?.comUser?.fuid)
            if (!_whetherSuperAdministrator())
              _toManagerTerminalBarWidget(infoDataBean),
          //非管理员展示打卡记录
          if(infoDataBean?.roleid=="10" && infoDataBean?.groupName=="家长" && records?.length != 0)
            Container(
              height: 50,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              alignment: Alignment.centerLeft,
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              child: Text(
                "刷脸记录",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color:
                    ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
              ),
            ),
          _showDifferentWidget(),
        ],
      ),
    );
    // },

    // );
  }

  Widget _showDifferentWidget() {
    return _existAttendanceRecord(personDetailResponseBean) == 0
        ? _getParentRecord()
        : Expanded(child: _toHorizontalBar());
  }

  Widget _getParentRecord() {
    return Expanded(
      child: ListView.separated(
          itemCount: records?.length ?? 0,
          padding: const EdgeInsets.all(0),
          physics: BouncingScrollPhysics(),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          itemBuilder: (BuildContext context, int index) {
            return PersonDetailListItemWidget(
              index: index,
              itemBean: records?.elementAt(index),
              listLength: records?.length,
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: 0,
            );
          }),
    );
  }

  int _existAttendanceRecord(PersonDetailResponseBean data) {
    //如果家长数据为空，提示未激活。有数据则显示数据 == 0
    if (infoDataBean?.groupName == "家长" && records?.length != 0) {
      return 0;
    } else {
      return 1;
    }
    //如果员工/学员为空，提示未激活。有数据则显示下横条
    //如果管理员为空，提示未激活。有数据则显示管理权限和下横条
    // return (data?.data?.page?.records?.length??0)!=0;
  }

  Widget _toHorizontalBar() {
    if(!UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition()){
      return Container();
    }
    if (records?.length == 0 || (widget?.isEnActive??false)) {
      return _toEmptyTips(false);
    } else {
      return Container(
        alignment: Alignment.bottomCenter,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            AttendRecordCalendarPage.navigatorPush(context, widget.fuid);
          },
          child: Container(
            height: 50 + ScreenUtils.getBottomBarH(context),
            padding: EdgeInsets.only(bottom: ScreenUtils.getBottomBarH(context)),
            color: Color(0xFF3A3F50),
            // margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(
                  width: 15,
                ),
                Text("本月出勤",
                    style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 15)),
                Spacer(),
                Text("共${infoDataBean?.monthPunchCnt ?? 0}天",
                    style: TextStyle(color: Color(0xFF1890FF), fontSize: 15)),
                SizedBox(
                  width: 10,
                ),
                Image.asset(
                  "images/index_arrow_ico.png",
                  width: 6,
                  height: 11,
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget _toVeryCareWidget() {
    return Container(
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      height: 50,
      child: Row(
        children: <Widget>[
          SizedBox(width: 15),
          Text(
            "特别关注",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
              fontSize: 14,
              // fontWeight: FontWeight.w600
            ),
          ),
          Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              String special = "00";
              infoDataBean?.special = !(infoDataBean?.special ?? false);
              if((infoDataBean?.special ?? false)){
                special = "01";
              }
              _viewModel?.changeVeryCareStatus(
                  context, infoDataBean?.fuid, special);
              setState(() {});
            },
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Image.asset(
                (infoDataBean?.special ?? false)
                    ? "images/switch_open.png"
                    : "images/switch_close.png",
                width: 36,
                gaplessPlayback: true,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toEnActiveWidget(){
    return Visibility(
      visible: widget?.isEnActive??false,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 12,),
            Text(
              "该用户未激活",
              style: TextStyle(
                fontSize: 13,
                color: ThemeRepository.getInstance().getMinorRedColor_F95355()
              ),
            ),
            Text(
              "预存人脸照片后，需本人在显示屏前进行人脸识别匹配，匹配成功后即可激活该用户",
              style: TextStyle(
                  fontSize: 13,
                  color: ThemeRepository.getInstance().getTextMinorGreyColor_808388()
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _toTopBarWidget(FaceUserInfoBean infoBean) {
    return VgTopBarWidget(
      title: "",
      isShowBack: true,
      //PersonDetailInvitationDialog()
      // rightWidget: _invitationAndDelete(infoBean),
      isHideRightWidget: !UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition(),
      rightWidget: InvitationAndDeleteWidget(
        faceUserInfoBean: infoBean,
      ),
    );
  }

  // Widget _invitationAndDelete(FaceUserInfoBean infoBean){
  //   bool _colorFont = true;
  //   return Builder(
  //     builder: (BuildContext context){
  //       return Row(
  //         mainAxisSize: MainAxisSize.max,
  //         children: <Widget>[
  //           GestureDetector(
  //             behavior: HitTestBehavior.translucent,
  //             onTap: (){
  //               _colorFont = true;
  //               if(infoBean?.napicurl != "" || infoBean?.napicurl != null){
  //                 return VgToastUtils.toast(context, "此用户已添加过人脸");
  //               }
  //               PersonDetailInvitationDialog.navigatorPushDialog(context,infoBean);
  //             },
  //             child: Container(
  //               height: 20,
  //               child: Text(
  //                 "邀请人脸入库",
  //                 style: TextStyle(color: _colorFont ?Color(0xFF1890FF) :Color(0xFF808388), fontSize: 14),
  //               ),),
  //           ),
  //           Offstage(
  //             offstage:!(VgRoleUtils.isHasDeleteForCompanyDetail(infoBean?.userid, infoBean?.roleid)),
  //             child: Text(
  //               " ｜ ",
  //               style: TextStyle(color: Color(0xFF808388), fontSize: 14),
  //             ),
  //           ),
  //           Offstage(
  //             offstage:!(VgRoleUtils.isHasDeleteForCompanyDetail(infoBean?.userid, infoBean?.roleid)),
  //             child: GestureDetector(
  //               onTap: (){
  //                 _colorFont = false;
  //
  //               },
  //               child: Container(
  //                 height: 20,
  //                 child: Text(
  //                   "删除",
  //                   style: TextStyle(color:_colorFont? Color(0xFF808388):Color(0xFF1890FF), fontSize: 14),
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ],
  //       );
  //
  //     },
  //   );
  // }

  @override
  void setListData(List<PersonDetailListItemBean> list) {
    list.removeWhere((element) => element == null);
    super.setListData(list);
  }

  ///空提示
  Widget _toEmptyTips(bool ShowVertical) {
    return Container(
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      alignment: Alignment.topCenter,
      padding: ShowVertical
          ? const EdgeInsets.symmetric(horizontal: 15, vertical: 12)
          : const EdgeInsets.only(left: 15, right: 15, top: 12),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "该用户未激活",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance().getMinorRedColor_F95355(),
                fontSize: 13,
              ),
            ),
          ),
          SizedBox(height: 2),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "预存人脸照片后，需本人在显示屏前进行人脸识别匹配，匹配成功后即可激活该用户",
              maxLines: 10,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance()
                    .getTextMinorGreyColor_808388(),
                fontSize: 13,
              ),
            ),
          )
        ],
      ),
    );
  }

  String splitManagerClassName(String managerClassName){
    if(managerClassName==null || managerClassName=="")return null;
    List<String>  managerClassNames = List();
    if(managerClassName.contains(",")){
      managerClassNames = managerClassName?.split(",");
    }else{
      managerClassNames = managerClassName?.split("、");
    }

    if(managerClassNames?.length ==1){
      return managerClassName;
    }else{
      if(infoDataBean?.roleid == "99"){
        return managerClassName = "${managerClassNames?.length}个班级";
      }
      return managerClassName = "${managerClassNames?.length}个班级";
    }
  }

  String splitDepartment(String managerDepartmentName){
    if(managerDepartmentName!=null && managerDepartmentName!=""){
      List<String>  departmentNames = List();
      if(managerDepartmentName.contains(",")){
        departmentNames = managerDepartmentName?.split(",");
      }else{
        departmentNames = managerDepartmentName?.split("、");
      }

      if(departmentNames?.length ==1){
        return managerDepartmentName;
      }else{
        if(infoDataBean?.roleid == "99"){
          return managerDepartmentName = "全部${departmentNames?.length}个部门";
        }
        return managerDepartmentName = "${departmentNames?.length}个部门";
      }
    }
    return managerDepartmentName;
  }

  Widget _toColumnInfoWidget( PersonDetailResponseBean data) {
    return Stack(
      // fit: StackFit.expand,
      children: [
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () async {
            if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
            if (StringUtils.isEmpty(infoDataBean?.fuid)) {
              return;
            }
            if (StringUtils.isEmpty(infoDataBean?.fid) &&
                infoDataBean?.courseInfo?.length == 0 &&
                infoDataBean?.courseInfo == null) {
              return;
            }
            if (!_isAdmin()) return;
            // 跳转到用户详细编辑界面
            bool result = await EditUserPage.navigatorPush(
                context, infoDataBean?.fuid, infoDataBean?.fid,pages:widget?.pages);
            if (result ?? false) {
              _viewModel?.refreshMultiPage();
            }
          },
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              PersonDetailInfoWidget(infoBean: infoDataBean, departmentSize: uploadBean?.departmentList?.length, valueChanged: (value){
                if(value!=null){
                  uploadBean?.headFaceUrl = value;
                  // _setUserInfo(false);
                  // if
                  _viewModel?.updateUserJurisdictionHttp(context, uploadBean, false);

                }
              },),
              if(infoDataBean?.groupType == "04")
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      // if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                      if (_whetherSuperAdministrator()) return;
                      if (StringUtils.isEmpty(infoDataBean?.fuid)) return;
                      //跳转到对应的信息编辑界面
                      PersonalDepartmentWidget.navigatorPush(context, infoDataBean,"PersonDetailPage")
                          .then((value) {
                        if(value==null){
                          return;
                        }
                        infoDataBean?.department = value?.editText;
                        infoDataBean?.departmentid = value?.value;
                        setState(() { });
                      });
                    },
                    child: PersonDetailGroupWidget(
                      title: "所在部门",
                      content: (splitDepartment(infoDataBean?.department)) ?? "未设置",
                      isEmptyHide: true,
                    )),
              // GestureDetector(
              //     behavior: HitTestBehavior.translucent,
              //     onTap: (){
              //       if(_isSuperAdministrator())return;
              //       if(StringUtils.isEmpty(infoBean?.fuid)){
              //         return;
              //       }
              //       //跳转到对应的信息编辑界面
              //       PersonalProjectWidget.navigatorPush(context, infoBean).then((value){
              //         infoBean?.projectname = value?.editText;
              //       });
              //     },
              //     child: PersonDetailGroupWidget(title: "项目",content: infoBean?.projectname,isEmptyHide: true,)),
              // GestureDetector(
              //     behavior: HitTestBehavior.translucent,
              //     onTap: (){
              //       if(_isSuperAdministrator())return;
              //       if(StringUtils.isEmpty(infoBean?.fuid)){
              //         return;
              //       }
              //       //跳转到对应的信息编辑界面
              //       PersonalCityWidget.navigatorPush(context, infoBean).then((value){
              //         infoBean?.city = value?.editText;
              //       });
              //     },
              //     child: PersonDetailGroupWidget(title: "城市",content: infoBean?.city,isEmptyHide: true,)),
              // if(infoBean?.courseInfo?.length != 0 && infoBean?.courseInfo != null)
              // PersonDetailGroupWidget(title: "课程",content: infoBean?.courseInfo?.length?.toString()+"课程",isEmptyHide: true,),
              // if(infoBean?.classInfo?.length != 0 && infoBean?.classInfo != null)
              if(infoDataBean?.groupType == "02")
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      // if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                      if (_whetherSuperAdministrator()) return;
                      if (StringUtils.isEmpty(infoDataBean?.fuid)) {
                        return;
                      }
                      //跳转到对应的信息编辑界面

                      PersonalClassWidget.navigatorPush(context, infoDataBean,"PersonDetailPage").then((result){
                        if(result==null){
                          return;
                        }
                        List<String> classTextList = new List();
                        List<ClassInfoBean> classInfo = List<ClassInfoBean>();
                        List<ClassCourseListItemBean> classNameList = result;
                        classNameList?.forEach((element) {
                          ClassInfoBean classInfoBean = ClassInfoBean();
                          classTextList?.add(element?.name?.trim());
                          classInfoBean?.classname = element?.name?.trim();
                          classInfoBean?.classid = element?.id;
                          classInfo?.add(classInfoBean);
                        });
                        infoDataBean?.classInfo = classInfo;
                        classNameStr = VgStringUtils?.getSplitStr(classTextList, symbol: "、");
                        setState(() { });
                      });
                    },
                    child: PersonDetailGroupWidget(
                      title: "所在班级",
                      content: splitManagerClassName(classNameStr) ?? "未设置",
                      isEmptyHide: true,
                    )),
              if(infoDataBean?.groupType == "03")
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                      if (_whetherSuperAdministrator()) return;
                      if (StringUtils.isEmpty(infoDataBean?.fuid)) {
                        return;
                      }

                      UserRepository.getInstance().userData?.companyInfo?.groupList?.forEach((element) {
                        if(element?.groupType == "02"){
                          uploadBean?.groupBean?.groupid = element?.groupid;
                          uploadBean?.groupBean?.groupType = element?.groupType;
                        }
                      });
                      //跳转到对应的信息编辑界面
                      SelectRelativeStuListPage.navigatorPush(
                          context, listStudentFuid, uploadBean?.groupBean, infoDataBean?.fuid,studentInfo:personDetailResponseBean?.data?.studentInfo)
                          .then((maps) {
                        if(maps == null){
                          return;
                        }
                        EditTextAndValue editTextAndValue = EditTextAndValue(
                          editText: maps['name'],
                          value: maps['ids'],
                        );
                        studentNameStr=editTextAndValue?.editText;
                        listStudentFuid = editTextAndValue?.value;
                        setState(() {});
                      });
                    },
                    child: PersonDetailGroupWidget(
                      title: "关联学员",
                      content: studentNameStr ?? "未设置",
                      isEmptyHide: true,
                    )),
              if(AppOverallDistinguish.comefromAiOrWeStudy() && UserRepository.getInstance().userData.companyInfo.isShowFaceRecognition())
                GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      // if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                      if (_whetherSuperAdministrator()) return null;
                      if (StringUtils.isEmpty(infoDataBean?.fuid)) return null;
                      return AddUserAddressAttendPage.navigatorPush(
                          context, listBranchBean, widget?.fuid, _fid).then((value){
                        if(value != null && (fuserBranchList?.length ?? 0) !=0 ){
                          List<String> fuserAddressNameList = new List<String>();
                          value?.forEach((element) {
                            fuserAddressNameList?.add(element?.getAddressStr());
                          });
                          // fuserAddressNames = _addressStr(fuserAddressNameList);
                          fuserAddressNames = VgStringUtils?.getMultipleStr(fuserAddressNameList);
                          //VgStringUtils?.getSplitStr(fuserAddressNameList, symbol: ",")

                          setState(() { });
                          //   _addressController?.text = addressUserLocation[0]?.address+"等${value?.length}个地址";
                        }
                      });
                    },
                    child: PersonDetailGroupWidget(
                      title: "刷脸地址",
                      content: fuserAddressNames,
                      isEmptyHide: true,
                    )),
            ],
          ),
        ),
        if(_recognitionDialogShow())
          _recognitionBubbleDialog()
      ],
    );
  }

  // String _addressStr(List<String> fuserAddressNameList){
  //   if((fuserAddressNameList?.length??0)<1)return "未设置";
  //   if(fuserAddressNameList?.length==1){
  //     fuserAddressNames = fuserAddressNameList[0];
  //   }else{
  //     fuserAddressNames = fuserAddressNameList[0]+"等${fuserAddressNameList?.length}个地址";
  //   }
  //   return fuserAddressNames;
  // }

  bool _recognitionDialogShow(){//已启用人脸的企业   没头像的用户加一个这个标签
    // if(infoBean?.groupType=="03")return false;
    // String enableFaceRecognition = UserRepository.getInstance().userData.companyInfo.enableFaceRecognition;//启用人脸识别 00启用 01关闭
    if(AppOverallDistinguish.enableFaceRecognition()){
      if(infoDataBean?.putpicurl == null || infoDataBean?.putpicurl == ""){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  Widget _recognitionBubbleDialog(){
    return Positioned(
      top: 86,
      left: 15,
      child: Column(
        children: <Widget>[
          Container(
              padding: const EdgeInsets.only(right: 68),
              child: Image.asset(
                "images/triangle_bubble_box.png",
                height: 6,
                width: 12,
                color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
              )),
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Container(
              height: 28,
              width: 132,
              color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
              child: Center(
                child: Text(
                  "请上传人脸清晰照片",
                  style: TextStyle(color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(), fontSize: 12),textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool _isAdmin(){
    if (UserRepository.getInstance()?.userData?.comUser?.roleid == "90" || UserRepository.getInstance()?.userData?.comUser?.roleid == "99") {
      return true;
    }
    return false;
  }

  bool _whetherSuperAdministrator() {//多处加此判断，以防后期变动
    //如果登录人是管理员只能查看自己管理的部门员工信息并更改
    if (UserRepository.getInstance()?.userData?.comUser?.roleid == "90") {
      if (infoDataBean?.roleid != "10") {
        if(UserRepository.getInstance()?.userData?.comUser?.fuid == infoDataBean?.fuid){
          return false;
        }else return true;
      } else {
        return false;
      }
    }
    //如果登录人是超级管理员可以更改所有人
    if (UserRepository.getInstance()?.userData?.comUser?.roleid == "99") {
      return false;
    } else {
      return true;
    }
  }

  ///如果是当前账号是普通管理员并且要编辑的这个人没有部门，则普通管理员也能编辑普通管理员的资料
  bool _isNormalAdminAndNoDepart(){
    if(VgRoleUtils.isCommonAdmin(UserRepository.getInstance().getCurrentRoleId()) && (uploadBean?.departmentList?.length??0) == 0){
      return true;
    }
    return false;
  }

  //管理员权限（只有管理员才会展示）
  Widget _toManagerTerminalBarWidget(FaceUserInfoBean infoBean) {
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: Column(
        // mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            height: 50,
            color: ThemeRepository.getInstance().getCardBgColor_21263C(),
            child: Row(
              children: <Widget>[
                SizedBox(width: 15),
                Text(
                  "管理权限",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Color(0xFFD0E0F7),
                    fontSize: 14,
                  ),
                ),
                Spacer(),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                    if (_whetherSuperAdministrator()) return;
                    if (isShowIdentityAndTerminal == true) {
                      resetManagerData();
                    } else {
                      isShowIdentityAndTerminal = !isShowIdentityAndTerminal;
                    }
                    uploadBean.isManagerTerminal = isShowIdentityAndTerminal;
                    setState(() {});
                    VgToolUtils.removeAllFocus(context);
                  },
                  child: Opacity(
                    opacity: AppOverallDistinguish.comefromAiOrWeStudy() ? 1:0,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Image.asset(
                        isShowIdentityAndTerminal
                            ? "images/switch_open.png"
                            : "images/switch_close.png",
                        width: 36,
                        gaplessPlayback: true,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Offstage(
            offstage: !isShowIdentityAndTerminal,
            child: Container(
              color: ThemeRepository.getInstance().getCardBgColor_21263C(),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _toIdentityWidget(),
                  _toCommonAdminTermainlWidget(), //管理终端
                  _toManageDepartmentWidget(), //管理部门
                  _toManageClassWidget() //管理班级
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///管理级别、终端、部门等清空
  void resetManagerData() async {
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示",
        content: "关闭权限后，所有管理对象内容将被清空，确认操作？",
        cancelText: "取消",
        confirmText: "确定",
        confirmBgColor:
        ThemeRepository.getInstance().getMinorRedColor_F95355());
    if (result ?? false) {
      isShowIdentityAndTerminal = false;
      uploadBean.departmentList = null;
      uploadBean?.classIdList = null;
      uploadBean.terminalList = null;
      _terminalController.text = "";
      _departmentController.text = "";
      _classController.text = "";
      _identityController.text = "";
      uploadBean.identityType = CompanyAddUserEditInfoIdentityType.commonUser;
      _setUserInfo(true);
    } else {
      isShowIdentityAndTerminal = true;
    }
  }

  ///管理级别
  Widget _toIdentityWidget() {
    return _EditTextWidget(
      title: "管理级别",
      hintText: "请选择",
      controller: _identityController,
      isShowGoIcon: AppOverallDistinguish.comefromAiOrWeStudy()?true:false,
      isShowRedStar: true,
      initContent: uploadBean?.identityName,
      onTap: (String editText, dynamic value) {
        if(!AppOverallDistinguish.comefromAiOrWeStudy()){return null;}
        if (_whetherSuperAdministrator()) return null;
        return SelectUtil.showListSelectDialog(
          context: context,
          title: "管理级别",
          positionStr: uploadBean.identityName,
          textList: CompanyAddUserEditInfoIdentityExtension.getListStr(),
        );
      },
      onDecodeResult: (dynamic result) {
        if (result is String && StringUtils.isNotEmpty(result)) {
          return EditTextAndValue(
              editText: result,
              value:
              CompanyAddUserEditInfoIdentityExtension.getStrToType(result));
        }
        return null;
      },
      onChanged: (String editText, dynamic value) {
        uploadBean.identityName = editText;
        uploadBean.identityType = value;
        // uploadBean.departmentList = null;
        // uploadBean.classIdList = null;
        // uploadBean.terminalList = null;
        if (value == CompanyAddUserEditInfoIdentityType.superAdmin) {
          //超管时清空终端 部门 班级 （不用上传）

          _terminalController.text = "全部${uploadBean?.terminalList?.length??0}台终端显示屏";
          _departmentController.text = "全部${uploadBean?.departmentList?.length??0}个部门";
          _classController.text = "全部${uploadBean?.classIdList?.length??0}个班级";
        }
        // else {
        //   _terminalController.text = "";
        //   _departmentController.text = "";
        //   _classController.text = "";
        // }
        _setUserInfo(false);
        //如果是超管那么刷新一下班级部门接口
        setState(() {});
      },
    );
  }

  ///管理终端
  Widget _toCommonAdminTermainlWidget() {
    return Column(
      children: <Widget>[
        _toSplitLineWidget(),
        _EditTextWidget(
          title: "管理终端",
          controller: _terminalController,
          hintText: uploadBean.isSuperAdminRole() ? "全部${uploadBean?.terminalList?.length??0}台终端显示屏" : "请选择",
          isShowGoIcon: !uploadBean.isSuperAdminRole(),
          initContent: uploadBean.isSuperAdminRole()
              ? "全部${uploadBean?.terminalList?.length??0}台终端显示屏"
              : _getTerminalEditValueStr(uploadBean?.terminalList),
          isShowRedStar: false,
          onTap: (String editText, dynamic value) {
            if (_whetherSuperAdministrator()) return null;
            if (uploadBean.isSuperAdminRole()) {
              return null;
            }
            return CompanyChooseTerminalListPage.navigatorPush(
                context, uploadBean?.terminalList);
          },
          onDecodeResult: (dynamic result) {
            if (result is! List<CompanyChooseTerminalListItemBean>) {
              return null;
            }
            List<CompanyChooseTerminalListItemBean> resultList = result;

            EditTextAndValue editTextAndValue = EditTextAndValue(
              editText: _getTerminalEditValueStr(resultList),
              value: result,
            );
            return editTextAndValue;
          },
          onChanged: (String editText, dynamic value) {
            uploadBean?.terminalList = value;
            _setUserInfo(false);
          },
        ),
      ],
    );
  }

  ///管理部门
  Widget _toManageDepartmentWidget() {
    return Column(
      children: <Widget>[
        _toSplitLineWidget(),
        _EditTextWidget(
            title: "管理部门",
            controller: _departmentController,
            hintText: uploadBean.isSuperAdminRole() ? "全部${uploadBean?.departmentList?.length??0}个部门" : "请选择",
            initContent: uploadBean.isSuperAdminRole()
                ? "全部${uploadBean?.departmentList?.length??0}个部门"
                : uploadBean.managerDepartmentName,
            isShowGoIcon: !uploadBean.isSuperAdminRole(),
            isShowRedStar: false,
            onTap: (String editText, dynamic value) {
              if (_whetherSuperAdministrator()) return null;
              if (uploadBean.isSuperAdminRole()) {
                return null;
              }
              return MultiSelectDepartmentWidget.navigatorPush(context,
                  selectIds: uploadBean?.departmentList, multiSelect: true);
            },
            onDecodeResult: (dynamic result) {
              if (result is! EditTextAndValue) {
                return null;
              }
              return result;
            },
            onChanged: (String editText, dynamic value) {
              _departmentController.text = uploadBean?.splitDepartment(editText);
              uploadBean.managerDepartmentName = editText;
              uploadBean?.departmentList = value;
              _setUserInfo(false);
            }),
      ],
    );
  }

  ///管理班级
  _toManageClassWidget() {
    return Visibility(
      visible: isSchool,
      child: Column(
        children: <Widget>[
          _toSplitLineWidget(),
          _EditTextWidget(
              title: "管理班级",
              hintText: uploadBean.isSuperAdminRole() ? "全部${uploadBean?.classIdList?.length??0}个班级" : "请选择",
              controller: _classController,
              initContent: uploadBean.isSuperAdminRole()
                  ? "全部${uploadBean?.classIdList?.length??0}个班级"
                  : uploadBean.managerClassName ?? "",
              isShowGoIcon: !uploadBean.isSuperAdminRole()&&AppOverallDistinguish.comefromAiOrWeStudy(),
              isShowRedStar: false,
              onTap: (String editText, dynamic value) {
                if(!AppOverallDistinguish.comefromAiOrWeStudy()){return null;}
                if (_whetherSuperAdministrator()) return null;
                if (uploadBean.isSuperAdminRole()) {
                  return null;
                }
                return MultiSelectClassCourseWidget.navigatorPush(
                    context, uploadBean?.classIdList);
              },
              onDecodeResult: (dynamic result) {
                if (result is! EditTextAndValue) {
                  return null;
                }
                return result;
              },
              onChanged: (String editText, dynamic value) {
                _classController?.text = uploadBean?.splitManagerClassName(editText);
                uploadBean.managerClassName = editText;
                uploadBean?.classIdList = value;
                _setUserInfo(false);
              }),
        ],
      ),
    );
  }

  ///分割线
  Widget _toSplitLineWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 15),
      height: 0.5,
      child: Container(
        color: ThemeRepository.getInstance().getLineColor_3A3F50(),
      ),
    );
  }

  ///获取终端编辑值串
  String _getTerminalEditValueStr(
      List<CompanyChooseTerminalListItemBean> resultList) {
    String editTextStr;
    if (resultList == null || resultList.isEmpty) {
      editTextStr = "";
    } else if (resultList.length == 1) {
      editTextStr = resultList?.elementAt(0)?.terminalName;
      //处理空名字
      if (StringUtils.isEmpty(editTextStr)) {
        editTextStr = "1台终端显示屏";
      }
    } else {
      editTextStr = "${resultList?.length ?? 0}台终端显示屏";
    }
    return editTextStr;
  }

  void pushNameRepeatDialog() async {
    String nick = await ToolTipBoxNewPage.navigatorPushDialog(context);
    if (nick == null || nick.isEmpty) {
      return;
    }
    uploadBean?.nick = nick;
    infoDataBean?.nick = nick;
    _viewModel?.updateUserJurisdictionHttp(context, uploadBean, false);
  }

  void _setUserInfo(bool whetherReturn) {
    //更改部门信息接口保存
    _viewModel?.updateUserJurisdictionHttp(context, uploadBean, whetherReturn);
  }
}

/// 编辑框组件
///
/// @author: zengxiangxi
/// @createTime: 1/18/21 10:55 PM
/// @specialDemand:
class _EditTextWidget extends StatelessWidget {
  final String title;

  final String hintText;

  final CommonTitleEditEditAndValueChangedCallback onChanged;

  ///点击
  final CommonTitleNavigatorPushCallback onTap;

  ///解析跳转返回值
  final CommonTitlePopResultDecodeCallback onDecodeResult;

  final bool isShowRedStar;

  ///中字极限
  final int maxZHCharLimit;

  ///极限字数回调
  final ValueChanged<int> limitCharFunc;

  ///限制
  final List<TextInputFormatter> inputFormatters;

  ///是否显示跳转icon
  final bool isShowGoIcon;

  ///初始化数据
  final String initContent;

  ///初始值
  final dynamic initValue;

  ///只读
  final bool readOnly;

  ///是否去掉红星
  final bool isGoneRedStar;

  ///是否跟随编辑框
  final bool isFollowEdit;

  final TextInputType keyboardType;

  final TextEditingController controller;

  final Function(Widget titleWidget, Widget textFieldWidget, Widget leftWidget,
      Widget rightWidget) customWidget;

  const _EditTextWidget(
      {Key key,
        this.title,
        this.hintText,
        this.onChanged,
        this.isShowRedStar = false,
        this.onTap,
        this.onDecodeResult,
        this.maxZHCharLimit,
        this.limitCharFunc,
        this.inputFormatters,
        this.isShowGoIcon = false,
        this.initContent,
        this.initValue,
        this.readOnly,
        this.isGoneRedStar = false,
        this.isFollowEdit = false,
        this.customWidget,
        this.controller,
        this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommonTitleEditWithUnderlineWidget(
      controller: controller,
      titleTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 14),
      editTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 14),
      title: title,
      titleMarginRight: 15,
      height: 50,
      onChanged: onChanged,
      onDecodeResult: onDecodeResult,
      onTap: onTap,
      minLines: 1,
      maxLines: 1,
      readOnly: readOnly,
      maxZHCharLimit: maxZHCharLimit,
      limitCharFunc: limitCharFunc,
      inputFormatters: inputFormatters,
      customWidget: customWidget,
      keyboardType: keyboardType,
      lineMargin: const EdgeInsets.only(left: 15, right: 0),
      lineUnselectColor: Color(0xFF303546),
      lineHeight: 0,
      initContent: initContent,
      initValue: initValue,
      lineSelectedColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      hintText: hintText,
      autoDispose: false,
      hintTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
          fontSize: 14),
      rightWidget: Offstage(
        offstage: !isShowGoIcon,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Image.asset(
            "images/go_ico.png",
            width: 6,
            color: VgColors.INPUT_BG_COLOR,
          ),
        ),
      ),
      leftWidget: Offstage(
        offstage: isGoneRedStar,
        child: Container(
          width: 15,
          child: Opacity(
            opacity: isShowRedStar ? 1 : 0,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  "*",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getMinorRedColor_F95355(),
                      fontSize: 14,
                      height: 1.1),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class WhetherSup {}

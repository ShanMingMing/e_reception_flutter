/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : "763d9e72ac134913a0d0bdfccfba3107"
/// extra : null

class InviterFaceSendSms {
  bool success;
  String code;
  String msg;
  String data;
  dynamic extra;

  static InviterFaceSendSms fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    InviterFaceSendSms inviterFaceSendSmsBean = InviterFaceSendSms();
    inviterFaceSendSmsBean.success = map['success'];
    inviterFaceSendSmsBean.code = map['code'];
    inviterFaceSendSmsBean.msg = map['msg'];
    inviterFaceSendSmsBean.data = map['data'];
    inviterFaceSendSmsBean.extra = map['extra'];
    return inviterFaceSendSmsBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}
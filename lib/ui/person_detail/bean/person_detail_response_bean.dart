import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/bean/special_attention_staff_list_bean.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"page":{"records":[{"punchTime":1616995085,"mintime":1616995085,"orderday":"2021-03-29","maxtime":1616995085},{"punchTime":1616643222,"mintime":1616643222,"orderday":"2021-03-25","maxtime":1616643222},{"punchTime":1616584243,"mintime":1616584243,"orderday":"2021-03-24","maxtime":1616584243}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1},"faceUserInfo":{"groupType":"00","projectname":"亚华为watches项目南京分布","city":"北京","roleid":"10","groupid":"7e04c15ce92b4c3c96aab009996fdc30","departmentid":"de8ee188ffa043b7931cf305db65419b","napicurl":"http://etpic.we17.com/test/20210324190408_3978.jpg","nick":"","number":"","groupName":"员工","phone":"","putpicurl":"http://etpic.we17.com/test/20210324190408_3978.jpg","name":"亦菲你号码你号码","department":"北京测试开发部门i","projectid":"a174d60aaed440769a2969561f790423"}}
/// extra : null

class PersonDetailResponseBean extends BasePagerBean<PersonDetailListItemBean> {
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static PersonDetailResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PersonDetailResponseBean personDetailResponseBeanBean = PersonDetailResponseBean();
    personDetailResponseBeanBean.success = map['success'];
    personDetailResponseBeanBean.code = map['code'];
    personDetailResponseBeanBean.msg = map['msg'];
    personDetailResponseBeanBean.data = DataBean.fromMap(map['data']);
    personDetailResponseBeanBean.extra = map['extra'];
    return personDetailResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;

  ///得到List列表
  @override
  List<PersonDetailListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }
}

/// page : {"records":[{"punchTime":1616995085,"mintime":1616995085,"orderday":"2021-03-29","maxtime":1616995085},{"punchTime":1616643222,"mintime":1616643222,"orderday":"2021-03-25","maxtime":1616643222},{"punchTime":1616584243,"mintime":1616584243,"orderday":"2021-03-24","maxtime":1616584243}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}
/// faceUserInfo : {"groupType":"00","projectname":"亚华为watches项目南京分布","city":"北京","roleid":"10","groupid":"7e04c15ce92b4c3c96aab009996fdc30","departmentid":"de8ee188ffa043b7931cf305db65419b","napicurl":"http://etpic.we17.com/test/20210324190408_3978.jpg","nick":"","number":"","groupName":"员工","phone":"","putpicurl":"http://etpic.we17.com/test/20210324190408_3978.jpg","name":"亦菲你号码你号码","department":"北京测试开发部门i","projectid":"a174d60aaed440769a2969561f790423"}

class DataBean {
  PageBean page;
  FaceUserInfoBean faceUserInfo;
  ManageInfoBean manageInfo;
  List<StudentInfoBean> studentInfo;
  List<StudentInfoBean> showStudentInfo;
  List<FuserBranchListBean> fuserBranchList;
  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.page = PageBean.fromMap(map['page']);
    dataBean.faceUserInfo = FaceUserInfoBean.fromMap(map['faceUserInfo']);
    dataBean.manageInfo = ManageInfoBean.fromMap(map['manageInfo']);
    dataBean.studentInfo = List()..addAll(
        (map['studentInfo'] as List ?? []).map((o) => StudentInfoBean.fromMap(o))
    );
    dataBean.showStudentInfo = List()..addAll(
        (map['showStudentInfo'] as List ?? []).map((o) => StudentInfoBean.fromMap(o))
    );
    dataBean.fuserBranchList = List()..addAll(
        (map['fuserBranchList'] as List ?? []).map((o) => FuserBranchListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "page": page,
    "faceUserInfo": faceUserInfo,
    "studentInfo": studentInfo,
    "showStudentInfo": showStudentInfo,
  };
}

class ManageInfoBean {
  List<MHsnBean> MHsn;
  List<MDepartmentBean> MDepartment;
  List<MClassBean> MClass;

  static ManageInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ManageInfoBean manageInfoBean = ManageInfoBean();
    manageInfoBean.MHsn = List()..addAll(
        (map['MHsn'] as List ?? []).map((o) => MHsnBean.fromMap(o))
    );
    manageInfoBean.MDepartment = List()..addAll(
        (map['MDepartment'] as List ?? []).map((o) => MDepartmentBean.fromMap(o))
    );
    manageInfoBean.MClass = List()..addAll(
        (map['MClass'] as List ?? []).map((o) => MClassBean.fromMap(o))
    );
    return manageInfoBean;
  }

  Map toJson() => {
    "MHsn": MHsn,
    "MDepartment": MDepartment,
    "MClass": MClass,
  };
}

/// classid : "d057380c60e6445a9fcbe7fc09a19ea7"
/// classname : "新班级0603呀"

class MClassBean {
  String classid;
  String classname;

  static MClassBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MClassBean mClassBean = MClassBean();
    mClassBean.classid = map['classid'];
    mClassBean.classname = map['classname'];
    return mClassBean;
  }

  Map toJson() => {
    "classid": classid,
    "classname": classname,
  };
}

/// departmentid : "d6e48324b86c424ea1d83f048285c347"
/// department : "北京开发欧欧欧欧欧欧欧欧欧欧欧欧春节放假玫瑰玫"

class MDepartmentBean {
  String departmentid;
  String department;

  static MDepartmentBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MDepartmentBean mDepartmentBean = MDepartmentBean();
    mDepartmentBean.departmentid = map['departmentid'];
    mDepartmentBean.department = map['department'];
    return mDepartmentBean;
  }

  Map toJson() => {
    "departmentid": departmentid,
    "department": department,
  };
}

/// terminalName : "小米pad"
/// hsn : "11140C1F12FEEB2C52DFBE67ED3E634AA0"

class MHsnBean {
  String terminalName;
  String hsn;
  String sideNumber;

  static MHsnBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MHsnBean mHsnBean = MHsnBean();
    mHsnBean.terminalName = map['terminalName'];
    mHsnBean.hsn = map['hsn'];
    mHsnBean.sideNumber = map['sideNumber'];
    return mHsnBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
    "sideNumber": sideNumber,
  };
}

/// groupType : "00"
/// projectname : "亚华为watches项目南京分布"
/// city : "北京"
/// roleid : "10"
/// groupid : "7e04c15ce92b4c3c96aab009996fdc30"
/// departmentid : "de8ee188ffa043b7931cf305db65419b"
/// napicurl : "http://etpic.we17.com/test/20210324190408_3978.jpg"
/// nick : ""
/// number : ""
/// groupName : "员工"
/// phone : ""
/// putpicurl : "http://etpic.we17.com/test/20210324190408_3978.jpg"
/// name : "亦菲你号码你号码"
/// department : "北京测试开发部门i"
/// projectid : "a174d60aaed440769a2969561f790423"

class FaceUserInfoBean extends ChangeNotifier{
  String groupType;
  String projectname;
  String city;
  String roleid;
  String groupid;
  String departmentid;
  String napicurl;
  String nick;
  String number;
  String groupName;
  String phone;
  String putpicurl;
  String name;
  String department;
  String projectid;

  String createuid;
  String fuid;
  String address;
  // List<ClassInfoBean> classInfo;
  List<CourseInfoBean> courseInfo;
  // List<HsnInfoBean> hsnInfo;
  // List<DepartmentInfo> MDepartmentInfo;
  // List<ClassInfoBean> MClassInfo;
  List<ClassInfoBean> showClassInfo;//展示使用（为了区分权限展示）
  List<ClassInfoBean> classInfo;//编辑使用
  int monthPunchCnt;

  ///身份名称
  String _identityName;

  ///身份ID
  CompanyAddUserEditInfoIdentityType _identityId;

  String fid;

  bool special;

  String userid;

  bool isSelectAll;

  String get identityName => _identityName;

  set identityName(String value) {
    _identityName = value;
    notifyListeners();
  }

  CompanyAddUserEditInfoIdentityType get identityType => _identityId;

  set identityType(CompanyAddUserEditInfoIdentityType value) {
    _identityId = value;
    notifyListeners();
  }

  static FaceUserInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FaceUserInfoBean faceUserInfoBean = FaceUserInfoBean();
    faceUserInfoBean.monthPunchCnt = map['monthPunchCnt'];
    faceUserInfoBean.groupType = map['groupType'];
    faceUserInfoBean.projectname = map['projectname'];
    faceUserInfoBean.city = map['city'];
    faceUserInfoBean.roleid = map['roleid'];
    faceUserInfoBean.groupid = map['groupid'];
    faceUserInfoBean.departmentid = map['departmentid'];
    faceUserInfoBean.napicurl = map['napicurl'];
    faceUserInfoBean.nick = map['nick'];
    faceUserInfoBean.number = map['number'];
    faceUserInfoBean.groupName = map['groupName'];
    faceUserInfoBean.phone = map['phone'];
    faceUserInfoBean.putpicurl = map['putpicurl'];
    faceUserInfoBean.name = map['name'];
    faceUserInfoBean.department = map['department'];
    faceUserInfoBean.projectid = map['projectid'];
    faceUserInfoBean.createuid = map['createuid'];
    faceUserInfoBean.address = map['address'];
    faceUserInfoBean.fuid = map['fuid'];
    faceUserInfoBean.fid = map['fid'];
    faceUserInfoBean.isSelectAll = map['isSelectAll'];
    faceUserInfoBean.classInfo = List()..addAll(
        (map['classInfo'] as List ?? []).map((o) => ClassInfoBean.fromMap(o))
    );
    faceUserInfoBean.showClassInfo = List()..addAll(
        (map['showClassInfo'] as List ?? []).map((o) => ClassInfoBean.fromMap(o))
    );
    faceUserInfoBean.courseInfo = List()..addAll(
        (map['courseInfo'] as List ?? []).map((o) => CourseInfoBean.fromMap(o))
    );

    // faceUserInfoBean.MClassInfo = List()
    //   ..addAll(
    //       (map['MClassInfo'] as List ?? []).map((o) => ClassInfoBean.fromMap(o)));
    // faceUserInfoBean.hsnInfo = List()
    //   ..addAll(
    //       (map['hsnInfo'] as List ?? []).map((o) => HsnInfoBean.fromMap(o)));
    // faceUserInfoBean.MDepartmentInfo = List()
    //   ..addAll((map['MDepartmentInfo'] as List ?? [])
    //       .map((o) => DepartmentInfo.fromMap(o)));
    faceUserInfoBean.special = map['special'];
    faceUserInfoBean.userid = map['userid'];
    return faceUserInfoBean;
  }

  Map toJson() => {
    // "hsnInfo": hsnInfo,
    // "MClassInfo": MClassInfo,
    "address": address,
    "groupType": groupType,
    "projectname": projectname,
    "city": city,
    "roleid": roleid,
    "groupid": groupid,
    "departmentid": departmentid,
    "napicurl": napicurl,
    "nick": nick,
    "number": number,
    "groupName": groupName,
    "phone": phone,
    "putpicurl": putpicurl,
    "name": name,
    "department": department,
    "projectid": projectid,
    "fuid": fuid,
    "classInfo": classInfo,
    "showClassInfo": showClassInfo,
    // "MDepartmentInfo": MDepartmentInfo,
    "courseInfo": courseInfo,
    "fid": fid,
    "special": special,
    "userid": userid,
    "isSelectAll": isSelectAll,
    "createuid": createuid,
    "monthPunchCnt": monthPunchCnt,
  };
}

/// coursename : "课程1"
/// courseid : "03bcc69dc3d9493382949493fe295ae2"

class CourseInfoBean {
  String coursename;
  String courseid;

  static CourseInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CourseInfoBean courseInfoBean = CourseInfoBean();
    courseInfoBean.coursename = map['coursename'];
    courseInfoBean.courseid = map['courseid'];
    return courseInfoBean;
  }

  Map toJson() => {
    "coursename": coursename,
    "courseid": courseid,
  };
}

class DepartmentInfo {
  String departmentid;
  String department;

  static DepartmentInfo fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentInfo info = DepartmentInfo();
    info.departmentid = map['departmentid'];
    info.department = map['department'];
    return info;
  }
}

/// terminalName : "岁岁的终端"
/// hsn : "a69ab51225adaf8b"

class HsnInfoBean {
  String terminalName;
  String hsn;

  static HsnInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnInfoBean hsnInfoBean = HsnInfoBean();
    hsnInfoBean.terminalName = map['terminalName'];
    hsnInfoBean.hsn = map['hsn'];
    return hsnInfoBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
  };
}

/// classid : "0ffbe482712547618e5ee2edcb26cd89"
/// classname : "班级1"

class ClassInfoBean {
  String classid;
  String classname;

  static ClassInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ClassInfoBean classInfoBean = ClassInfoBean();
    classInfoBean.classid = map['classid'];
    classInfoBean.classname = map['classname'];
    return classInfoBean;
  }

  Map toJson() => {
    "classid": classid,
    "classname": classname,
  };
}


/// records : [{"punchTime":1616995085,"mintime":1616995085,"orderday":"2021-03-29","maxtime":1616995085},{"punchTime":1616643222,"mintime":1616643222,"orderday":"2021-03-25","maxtime":1616643222},{"punchTime":1616584243,"mintime":1616584243,"orderday":"2021-03-24","maxtime":1616584243}]
/// total : 3
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<PersonDetailListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => PersonDetailListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// punchTime : 1616995085
/// mintime : 1616995085
/// orderday : "2021-03-29"
/// maxtime : 1616995085

class PersonDetailListItemBean {
  int punchTime;
  int mintime;
  String orderday;
  int maxtime;

  static PersonDetailListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PersonDetailListItemBean recordsBean = PersonDetailListItemBean();
    recordsBean.punchTime = map['punchTime'];
    recordsBean.mintime = map['mintime'];
    recordsBean.orderday = map['orderday'];
    recordsBean.maxtime = map['maxtime'];
    return recordsBean;
  }

  Map toJson() => {
    "punchTime": punchTime,
    "mintime": mintime,
    "orderday": orderday,
    "maxtime": maxtime,
  };
}

/// nick : ""
/// phone : ""
/// putpicurl : ""
/// name : "李岁"
/// fuid : "c89e6ba455364c938c6bbdb5457846f6"
/// napicurl : ""

class StudentInfoBean {
  String nick;
  String phone;
  String putpicurl;
  String name;
  String fuid;
  String napicurl;
  String groupid;

  static StudentInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    StudentInfoBean studentInfoBean = StudentInfoBean();
    studentInfoBean.nick = map['nick'];
    studentInfoBean.phone = map['phone'];
    studentInfoBean.putpicurl = map['putpicurl'];
    studentInfoBean.name = map['name'];
    studentInfoBean.fuid = map['fuid'];
    studentInfoBean.napicurl = map['napicurl'];
    studentInfoBean.groupid = map['groupid'];
    return studentInfoBean;
  }

  Map toJson() => {
    "nick": nick,
    "phone": phone,
    "putpicurl": putpicurl,
    "name": name,
    "fuid": fuid,
    "napicurl": napicurl,
    "groupid": groupid,
  };
}

/// addrCity : "西安市"
/// gpsaddress : "String gpsadress"
/// address : "西安雁塔软件哦好玩噶我v"
/// caid : "9dc77d7fad1b49399dde589dba8c12e0"
/// addrProvince : "陕西省"
/// addrDistrict : "碑林区"

class FuserBranchListBean {
  String addrCity;
  String gpsaddress;
  String address;
  String cbid;
  String addrProvince;
  String addrDistrict;
  String branchname;

  static FuserBranchListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FuserBranchListBean fuserBranchListBean = FuserBranchListBean();
    fuserBranchListBean.addrCity = map['addrCity'];
    fuserBranchListBean.gpsaddress = map['gpsaddress'];
    fuserBranchListBean.address = map['address'];
    fuserBranchListBean.cbid = map['cbid'];
    fuserBranchListBean.addrProvince = map['addrProvince'];
    fuserBranchListBean.addrDistrict = map['addrDistrict'];
    fuserBranchListBean.branchname = map['branchname'];
    return fuserBranchListBean;
  }

  String getAddressStr(){
    return "${branchname??""}/${address??""}";
  }

  Map toJson() => {
    "addrCity": addrCity,
    "gpsaddress": gpsaddress,
    "address": address,
    "cbid": cbid,
    "addrProvince": addrProvince,
    "addrDistrict": addrDistrict,
    "branchname": branchname,
  };
}
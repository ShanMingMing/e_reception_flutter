import 'dart:ffi';

import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/company_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/unallocated_department_staff_widget.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_attention_widget/special_attention_staff_list_view.dart';
import 'package:e_reception_flutter/ui/company/company_detail_by_type_list/bean/company_detail_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/artificial_attendance_widget.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_widget.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class InvitationAndDeleteWidget extends StatefulWidget {
  final FaceUserInfoBean faceUserInfoBean;

  InvitationAndDeleteWidget({Key key, this.faceUserInfoBean}) : super(key: key);

  @override
  InvitationAndDeleteWidgetState createState() =>
      InvitationAndDeleteWidgetState();
}

class InvitationAndDeleteWidgetState extends BasePagerState<CompanyDetailByTypeListItemBean, InvitationAndDeleteWidget>
   {
  FaceUserInfoBean infoBean;
  bool _colorFont = true;
  ValueNotifier<String> searchPageValueNotifier;
  
  @override
  void initState() {
    super.initState();
    infoBean = widget?.faceUserInfoBean;
  }

  @override
  void didUpdateWidget(InvitationAndDeleteWidget oldWidget) {
    infoBean = widget?.faceUserInfoBean;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return  Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                _colorFont = true;
                setState(() {

                });
                // if (infoBean?.putpicurl != null || infoBean?.putpicurl != "" ||infoBean?.napicurl != null||infoBean?.napicurl != "") {
                //   return VgToastUtils.toast(context, "此用户已添加过人脸");
                // }
                PersonDetailInvitationDialog.navigatorPushDialog(
                    context, infoBean);
              },
              child: Container(
                height: 20,
                child: Text(
                  "邀请人脸入库",
                  style: TextStyle(
                    color:Color(0xFF1890FF),
                      // color: _colorFont ? Color(0xFF1890FF) : Color(0xFF808388),
                      fontSize: 14),
                ),
              ),
            ),
            if(_userPutpicurl())
            Row(
              children: [
                Text(
                  " ｜ ",
                  style: TextStyle(color: Color(0xFF808388).withOpacity(0.5), fontSize: 14),
                ),
                //管理员可见--无头像不展示
                GestureDetector(
                  onTap: () async {
                    if(infoBean?.putpicurl=="" && infoBean?.napicurl=="" ){
                      VgToastUtils.toast(context, "请添加人脸打卡");
                      return;
                    }
                    ArtificialAttendanceWidget.navigatorPush(context,widget?.faceUserInfoBean);
                    // Navigator.push(context, new MaterialPageRoute(builder: (context){
                    //   return new UnallocatedDepartmentStaffWidget();
                    // }));
                    // _colorFont = false;
                    // setState(() { });
                  },
                  child: Container(
                    height: 20,
                    child: Text(
                      "手动打卡",
                      style: TextStyle(
                          color: Color(0xFF1890FF),
                          // color: _colorFont ? Color(0xFF808388) : Color(0xFF1890FF),
                          fontSize: 14),
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
  }


  bool _userPutpicurl(){
    //&& infoBean?.napicurl==null || infoBean?.napicurl==""
    if(infoBean?.putpicurl==null || infoBean?.putpicurl==""){
            return false;
    }else{
      return true;
    }
  }

  bool _isRoleidAdmin(){
    //当选择的是当前登录的超级管理员，则不可删除
    bool roleid = true;
    if(infoBean?.fuid == UserRepository.getInstance()?.userData?.comUser?.fuid){
      if(UserRepository.getInstance()?.userData?.comUser?.roleid == "99"){
        return roleid = false;
      }else{
        return roleid = true;
      }
    }
    return roleid;
  }

}

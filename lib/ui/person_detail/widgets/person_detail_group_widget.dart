import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:flutter/widgets.dart';

/// 人员详情-分组显示
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 4:39 PM
/// @specialDemand:
class PersonDetailGroupWidget extends StatefulWidget {


  final String title;

  String content;
  final bool isEmptyHide;
  final bool showSplit;

  PersonDetailGroupWidget({Key key,this.title, this.content, this.isEmptyHide = false, this.showSplit = true}) : super(key: key);

  @override
  _PersonDetailGroupWidgetState createState() => _PersonDetailGroupWidgetState();
}

class _PersonDetailGroupWidgetState extends State<PersonDetailGroupWidget> {
  @override
  Widget build(BuildContext context) {
    if(widget.isEmptyHide && (widget.content == null || widget.content.isEmpty)){
      if(widget?.title=="所在部门" || widget?.title == "刷脸地址"){
        widget?.content =  "未设置";
      }else{
        return Container();
      }
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Visibility(
          visible: widget?.showSplit??true,
          child: Container(
            height: 0.5,
            margin: const EdgeInsets.only(left: 15, right: 1),
            color: Color(0xFF303546),
          ),
        ),
        Container(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          height: 50,
          child: _toMainRowWidget(),
        ),
      ],
    );
  }

  Widget _toMainRowWidget() {
    bool showGoico = true;
    if(!AppOverallDistinguish.comefromAiOrWeStudy()){
      if(widget?.title=="所在部门" || widget?.title=="所在班级"){//一起学所在部门展示
        showGoico = true;
      }else{
        showGoico = false;
      }
    }else{
      showGoico = true;
    }
    return Row(
      children: <Widget>[
        SizedBox(width: 15,),
         Text(
                   widget.title ?? "",
                   maxLines: 1,
                   overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                     color: VgColors.DEFAULT_FIELD_COLOR,
                       fontSize: 14,
                       ),
                 ),
        SizedBox(width: 15,),
        Expanded(
          child:  Text(
                    "${showOriginEmptyStr(widget.content) ?? "-"}",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                        fontSize: 14,
                        ),
                  ),
        ),
        //只有超级管理员才可以点击查看
        // if(UserRepository.getInstance()?.userData?.comUser?.roleid=="99")
          Opacity(
            opacity: showGoico ? 1 :0,
              child: Image.asset(
            "images/go_ico.png",
            width: 6,
            color: ThemeRepository.getInstance().getHintGreenColor_5E687C(),
          )),
        SizedBox(width:
        15,)
      ],
    );
  }
}


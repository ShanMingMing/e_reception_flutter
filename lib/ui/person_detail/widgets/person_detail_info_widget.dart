import 'package:e_reception_flutter/common_widgets/common_more_menu_dialog/common_more_menu_dialog.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/camera/front_camera_head/front_camera_head_page.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/utils/app_overall_distinguish.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_role_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/clip_image_head_border_utils.dart';
import 'package:e_reception_flutter/vg_widgets/clip_image_head_border/logo_detail_page.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 人员详情-顶部具体信息
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 4:21 PM
/// @specialDemand:
class PersonDetailInfoWidget extends StatefulWidget {
  final FaceUserInfoBean infoBean;
  final int departmentSize;
  final ValueChanged<String> valueChanged;

  const PersonDetailInfoWidget({Key key, this.infoBean, this.departmentSize, this.valueChanged}) : super(key: key);

  @override
  _PersonDetailInfoWidgetState createState() => _PersonDetailInfoWidgetState();
}

class _PersonDetailInfoWidgetState extends State<PersonDetailInfoWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 104,
      child: _toMainRowWidget(context),
    );
  }

  void onClickUserLogo(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    if (StringUtils.isNotEmpty(widget.infoBean?.putpicurl)) {
      LogoDetailPage.navigatorPush(context,
          url: widget.infoBean?.putpicurl,
          selectMode: SelectMode.HeadBorder,
          clipCompleteCallback: (path, cancelLoadingCallback) {
            // widget.infoBean?.putpicurl = path;
            widget.valueChanged.call(path);
            setState(() {});
          }
      );
      return;
    }
    addUserLogo(context);
  }

  void addUserLogo(BuildContext context) {
    CommonMoreMenuDialog.navigatorPushDialog(context, {
      "上传图片": () async {
        String path = await ClipImageHeadBorderUtil.clipOneImage(context,
            scaleY: 1, scaleX: 1, maxAutoFinish: true,
            isShowHeadBorderWidget: true);
        if (path == null || path == "") {
          return;
        }
        // widget.infoBean?.putpicurl = path;
        widget.valueChanged.call(path);
        setState(() {});
      },
      "拍照": () async {
        String path = await FrontCameraHeadPage.navigatorPush(context);
        if (path == null || path.isEmpty) {
          return;
        }
        // widget.infoBean?.putpicurl = path;
        widget.valueChanged.call(path);
        setState(() {});
      }
    });
  }

  Widget _toMainRowWidget(BuildContext context) {
    return Container(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          height: 64,
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 15,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  // if(!AppOverallDistinguish.comefromAiOrWeStudy()){return;}
                    onClickUserLogo(context);
                  // VgPhotoPreview.single(
                  //     AppMain.context,
                  //     showOriginEmptyStr(widget.infoBean?.putpicurl) ??
                  //         (widget.infoBean?.napicurl ?? ""));
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    width: 64,
                    height: 64,
                    child: VgCacheNetWorkImage(
                      showOriginEmptyStr(widget.infoBean?.putpicurl) ??
                          (widget.infoBean?.napicurl ?? ""),
                      imageQualityType: ImageQualityType.middleDown,
                      defaultPlaceType: ImagePlaceType.head,
                      defaultErrorType: ImageErrorType.head,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(height: 64, child: _toColumnWidget()),
              ),
              if(!_whetherSuperAdministrator())
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text("编辑",style: TextStyle(color: Color(0xFF5E687C),fontSize: 12),),
                        SizedBox(width: 9,),
                        // Image.asset(
                        //   "images/go_ico.png",
                        //   width: 6,
                        //   color: Color(0xFF5E687C),
                        // ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text("资料",style: TextStyle(color: Color(0xFF5E687C),fontSize: 12),),
                        SizedBox(width: 9,),
                        // Image.asset(
                        //   "images/go_ico.png",
                        //   width: 6,
                        //   color: Color(0xFF5E687C),
                        // ),
                      ],
                    ),
                  ],
                ),
              if(!_whetherSuperAdministrator())
                Image.asset(
                  "images/go_ico.png",
                  width: 6,
                  color: Color(0xFF5E687C),
                ),
              SizedBox(
                width: 15,
              )
            ],
          ),
        );
  }

  bool _whetherSuperAdministrator(){
    //如果登录人是管理员只能查看自己管理的部门员工信息并更改
    if(!AppOverallDistinguish.comefromAiOrWeStudy()){
      return true;
    }
    if(UserRepository.getInstance()?.userData?.comUser?.roleid == "90"){
      ///2022.05.11改为普通管理员也能互相编辑信息
      // if(widget.infoBean?.roleid != "10"){
      //   return true;
      // }else{
      //   return false;
      // }
      return false;
    }
    //如果登录人是超级管理员可以更改所有人
    if(UserRepository.getInstance()?.userData?.comUser?.roleid=="99"){
      return false;
    }else{
      return true;
    }
  }

  ///如果是当前账号是普通管理员并且要编辑的这个人没有部门，则普通管理员也能编辑普通管理员的资料
  bool _isNormalAdminAndNoDepart(){
    if(VgRoleUtils.isCommonAdmin(UserRepository.getInstance().getCurrentRoleId()) && (widget?.departmentSize??0) ==0){
      return true;
    }
    return false;
  }

  Widget _toColumnWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Row(
            children: <Widget>[
              Text(
                widget.infoBean?.name ?? "-",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(width: 3,),
              if(StringUtils.isNotEmpty(widget.infoBean?.nick))
              Text(
                "/",
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                ),
              ),
              SizedBox(width: 3,),
              Text(
                widget.infoBean?.nick ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
        Text(
          StringUtils.isEmpty(showOriginEmptyStr(widget.infoBean?.phone))
              ? "暂无手机号"
              : (showOriginEmptyStr(widget.infoBean?.phone) ?? ""),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Color(0xFF5E687C),
            fontSize: 12,
          ),
        ),
        Text(
          VgStringUtils.getSplitStr(
                  [widget.infoBean?.groupName?.trim(), widget.infoBean?.number?.trim()],
                  symbol: "・") ??
              "-",
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            color: Color(0xFF5E687C),
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}

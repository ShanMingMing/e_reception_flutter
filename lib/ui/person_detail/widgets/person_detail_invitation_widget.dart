import 'package:e_reception_flutter/common_widgets/common_with_up_or_down_arrows_dialog/common_with_up_or_down_arrows_dialog.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/share_repository/share_repository.dart';
import 'package:e_reception_flutter/singleton/share_repository/vo/share_param_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_tips_dialog/send_sms_invitation_tips_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sharesdk_plugin/sharesdk_defines.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'inviter_sms_view_model.dart';


///微信短信邀请菜单弹窗

class PersonDetailInvitationDialog extends StatefulWidget {
  final FaceUserInfoBean faceUserInfoBean;

  PersonDetailInvitationDialog({
    Key key,this.faceUserInfoBean,
  }) : super(key: key);

  static Future<dynamic> navigatorPushDialog(
      BuildContext context,FaceUserInfoBean faceUserInfoBean) {
    return CommonWithUpOrDownArrowsDialog.navigatorPushForDialog<dynamic>(context,
        dialogMaxHeight: 110,
        arrowsSize: Size(11, 6),
        safeY: 0,
        arrowsOffsetX: 35,
        dialogToBoxOffsetY: 0,
        arrowsColor: Color(0xFF303546),
        child: Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: PersonDetailInvitationDialog(
                // parentContext: context,
                faceUserInfoBean:faceUserInfoBean
            ),
          ),
        )
    );
  }

  @override
  _PersonDetailInvitationDialogState createState() => _PersonDetailInvitationDialogState();

  static bool _isNotEmptyString(String text){
    if(text?.trim() !=null && text?.trim() !=""){
      return true;
    }else{
      return false;
    }
  }
}

class _PersonDetailInvitationDialogState extends BaseState<PersonDetailInvitationDialog> {
  final String H5_WECHAT_SHARE =ServerApi.BASE_URL+"h5/inviteFace.html";
  InviterSmsViewModel viewModel;
  @override
  void initState() {
    super.initState();
    viewModel = InviterSmsViewModel(this);
  }

  @override
  Widget build(BuildContext context) {
    return bubbleWidget(context);
  }

  Widget bubbleWidget(BuildContext context){
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        width: 140,
        // padding: const EdgeInsets.symmetric(vertical: 10),
        color: Color(0xFF303546),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                // PersonDetailInvitationTipsDialog.navigatorPushDialog(context);
                _wechatInvitationShare(context);
               // VgToastUtils.toast(context, "分享成功");
                // RouterUtils.pop(context);
              },
              child: Container(
                height: 50,
                padding: const EdgeInsets.only(left: 28,top: 10,),
                child: Row(
                  children: <Widget>[
                    Image.asset("images/wechat_invitation.png",height: 20,),
                    SizedBox(width: 4,),
                    Text("微信邀请",style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14)),
                  ],
                ),
              ),
            ),
            // Spacer(),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){
                SendSMSInvitationTipsDialog.navigatorPushDialog(context,null,widget.faceUserInfoBean);
              },
              child: Container(
                height: 54,
                padding: const EdgeInsets.only(left: 28,bottom: 14,),
                child: Row(
                  children: <Widget>[
                    Image.asset("images/sms_invitation.png",height: 20,),
                    SizedBox(width: 4,),
                    Text("短信邀请",style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _wechatInvitationShare(BuildContext context){
    String companyShareLogo = "http://etpic.we17.com/logo/ai_logo.png";
    if(StringUtils.isNotEmpty(UserRepository.getInstance()?.userData?.companyInfo?.companylogo)){
      companyShareLogo = UserRepository.getInstance()?.userData?.companyInfo?.companylogo;
    }
    String nameAndNick =widget.faceUserInfoBean?.name??"";
    if(PersonDetailInvitationDialog._isNotEmptyString(widget.faceUserInfoBean?.nick)){
      nameAndNick = widget.faceUserInfoBean?.name + "/${widget.faceUserInfoBean?.nick??""}";
    }
    String userDataId = "";
    viewModel?.userInfoValueNotifier?.addListener(() {
      userDataId = viewModel?.userInfoValueNotifier?.value;
      ShareParamsBean bean = ShareParamsBean(
        title: "人脸识别照片上传邀请",
        text: "请上传人脸正面照片用于考勤识别",
        contentType: SSDKContentTypes.webpage,
        imageUrlAndroid: companyShareLogo,
        images: companyShareLogo,
        url: H5_WECHAT_SHARE + "?" + "id=${userDataId ?? ""}",
      );
      ShareRepository.getInstance()
          .oneKeyShare(context, ShareSDKPlatforms.wechatSession, bean);
      RouterUtils.pop(context);
    });
    viewModel?.appInviterFace(context,widget.faceUserInfoBean?.phone,widget.faceUserInfoBean?.fuid,nameAndNick,"01");
  }
}

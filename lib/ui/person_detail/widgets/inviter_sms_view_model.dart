import 'dart:async';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/inviter_face_send_sms.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_tips_dialog/sms_send_out_success_tips_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_stream_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class InviterSmsViewModel extends BaseViewModel{
 // VgStreamController<InviterFaceSendSms> inviterController;
 //  StreamController<InviterFaceSendSms> sendMsgController = StreamController.broadcast();

  ValueNotifier<String> userInfoValueNotifier;
  InviterSmsViewModel(BaseState<StatefulWidget> state) : super(state) {
    // inviterController = newAutoReleaseBroadcast<InviterFaceSendSms>();
    // sendMsgController = newAutoReleaseBroadcast<bool>();
    userInfoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    userInfoValueNotifier.dispose();
    super.onDisposed();

  }

  ///发送短信
  void sendInviterSms(BuildContext context,String phone,
      String id,String pageName) {
    if (StringUtils.isEmpty(phone)) {
      VgToastUtils.toast(context, "号码错误");
      return;
    }
    // VgHudUtils.show(context, "发送中");
    VgHttpUtils.get(ServerApi.BASE_URL + "app/sendInvite",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "id":id ??"",
          "phone":phone ?? "",
        },
        callback: BaseCallback(onSuccess: (val) async {
          VgHudUtils.hide(context);
          RouterUtils.pop(context);
          bool result =
              await CommonConfirmCancelDialog.navigatorPushDialog(AppMain.context,
              title: "短信发送成功",
              content: "短信服务有延时，对方约在15分钟后收到",
              // cancelText: "取消",
              isShowLeftButton:false,
              confirmText: "我知道了",
              confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF());
          if (result == true) {
            if(pageName == "PersonDetailPage"){
              RouterUtils.popUntil(AppMain.context,"PersonDetailPage");
            }else{
              RouterUtils.popUntil(AppMain.context,"CompanyDetailPage");
            }
          }
          // smsSendOutSuccessTipsDialog.navigatorPushDialog(AppMain.context);
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(AppMain.context, "发送失败");
        }));
  }

  ///邀请人脸入库
  void appInviterFace(BuildContext context,String phone,String fuid,String name,String type,{String pageName}) {
    if (StringUtils.isEmpty(phone)) {
      if(type=="01"){
        phone = " ";
      }else{
        VgToastUtils.toast(context, "请添加手机号");
        return;
      }
    }
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "请暂到个人详情发送短信邀请");
      return;
    }
    VgHttpUtils.post(ServerApi.BASE_URL + "app/appInviterFace",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "companynick":UserRepository.getInstance()?.userData?.companyInfo?.companynick??"",
          "fuid":fuid ??"",
          "name":name ??"",
          "phone": phone ?? "",
          "type":type
        },
        callback: BaseCallback(onSuccess: (val) {
          InviterFaceSendSms inviterFaceSendSms =InviterFaceSendSms.fromMap(val);
          // inviterController.add(inviterFaceSendSms);
          if(type == "02"){
            if(inviterFaceSendSms?.data!=null&&inviterFaceSendSms?.data!=""){
              sendInviterSms(context, phone, inviterFaceSendSms?.data??"",pageName);
              // RouterUtils.popUntil(context,"PersonDetailPage");
            }else{
              VgToastUtils.toast(AppMain.context, "号码已存在");
            }
          }else{
            userInfoValueNotifier.value = inviterFaceSendSms?.data;
            // VgToastUtils.toast(context, "分享成功");
          }
          VgHudUtils.hide(context);
          // VgToastUtils.toast(context, "发送成功");

        }, onError: (msg) {
          VgHudUtils.hide(context);
          // VgToastUtils.toast(context, msg);
        }));
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/share_repository/share_repository.dart';
import 'package:e_reception_flutter/singleton/share_repository/vo/share_param_bean.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_tips_dialog/send_sms_invitation_tips_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sharesdk_plugin/sharesdk_defines.dart';
import 'package:vg_base/vg_arch_lib.dart';

import '../inviter_sms_view_model.dart';

class PersonDetailInvitationTipsDialog extends StatefulWidget {
  final CreateUserUploadBean createUserUploadBean;

  const PersonDetailInvitationTipsDialog({Key key, this.createUserUploadBean})
      : super(key: key);

  ///跳转方法
  static Future<String> navigatorPushDialog(
      BuildContext context, CreateUserUploadBean createUserUploadBean) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: false, //点击空白消失
        context: context,
        child: PersonDetailInvitationTipsDialog(
            createUserUploadBean : createUserUploadBean,
        ));
  }

  @override
  _PersonDetailInvitationTipsDialogState createState() => _PersonDetailInvitationTipsDialogState();
}

class _PersonDetailInvitationTipsDialogState extends BaseState<PersonDetailInvitationTipsDialog> {
  final String H5_WECHAT_SHARE =ServerApi.BASE_URL+"h5/inviteFace.html";
  InviterSmsViewModel viewModel;
  @override
  void initState() {
    super.initState();
    viewModel = InviterSmsViewModel(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: CommonPackUpKeyboardWidget(
          child:_getNoPhotosTips(context)
      ),
    );
  }

  Widget _getNoPhotosTips(BuildContext context) {
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          width: 290,
          color: Color(0xFF303546),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 25,
              ),
              Text(
                "提示",
                style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 16),
              ),
              SizedBox(
                height: 13,
              ),
              //该用户尚无人脸正面照片，你可以：
              Text(
                "该用户尚无人脸正面照片，你可以：",
                style: TextStyle(color: Color(0xFFD0E0F7), fontSize: 14),
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () async{

                  _wechatInvitationShare(context);
                 // await VgToastUtils.toast(context, "分享成功");
                },
                child: Container(
                  width: 230,
                  // color: Color(0xFFD0E0F7),
                  // padding: const EdgeInsets.symmetric(horizontal: 30),
                  height: 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                        color: Color(0xFFD0E0F7).withOpacity(0.3), width: 1),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          "images/wechat_invitation.png",
                          height: 20,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          "发送微信邀请",
                          style:
                              TextStyle(color: Color(0xFFD0E0F7), fontSize: 14),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () async{
                  if(widget.createUserUploadBean?.headFaceUrl != "" && widget.createUserUploadBean?.headFaceUrl != null){
                    return;
                  }
                  RouterUtils.pop(context);
                await SendSMSInvitationTipsDialog.navigatorPushDialog(context,widget.createUserUploadBean,null);
                },
                child: Container(
                  width: 230,
                  // color: Color(0xFFD0E0F7),
                  // padding: const EdgeInsets.symmetric(horizontal: 30),
                  height: 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                        color: Color(0xFFD0E0F7).withOpacity(0.3), width: 1),
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          "images/sms_invitation.png",
                          height: 20,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          "发送短信邀请",
                          style:
                              TextStyle(color: Color(0xFFD0E0F7), fontSize: 14),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  RouterUtils.pop(context);
                },
                child: Container(
                  height: 45,
                  alignment: Alignment.center,
                  child: Text(
                    "以后再说",
                    style: TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 14),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool _wechatInvitationShare(BuildContext context){
    String companyShareLogo = "http://etpic.we17.com/logo/ai_logo.png";
    if(UserRepository.getInstance()?.userData?.companyInfo?.companylogo!=null && UserRepository.getInstance()?.userData?.companyInfo?.companylogo!=null){
      companyShareLogo = UserRepository.getInstance()?.userData?.companyInfo?.companylogo;
    }
      String nameAndNick =widget?.createUserUploadBean?.name??"";
      if(_isNotEmptyString(widget.createUserUploadBean?.nick)){
        nameAndNick = widget?.createUserUploadBean?.name + "/${widget?.createUserUploadBean?.nick??""}";
      }

    String userDataId = "";
    viewModel?.userInfoValueNotifier?.addListener(() {
      userDataId = viewModel?.userInfoValueNotifier?.value;
      ShareParamsBean bean = ShareParamsBean(
        title: "人脸识别照片上传邀请",
        text: "请上传人脸正面照片用于考勤识别",
        contentType: SSDKContentTypes.webpage,
        imageUrlAndroid: companyShareLogo,
        images:"",
        url: H5_WECHAT_SHARE+"?"+"id=${userDataId ?? ""}",
      );
      ShareRepository.getInstance().oneKeyShare(context, ShareSDKPlatforms.wechatSession, bean);
    });
    viewModel?.appInviterFace(context,widget?.createUserUploadBean?.phone,widget?.createUserUploadBean?.fuid,nameAndNick,"01");
    RouterUtils.pop(context);
  }
  static bool _isNotEmptyString(String text){
    if(text?.trim() !=null && text?.trim() !=""){
      return true;
    }else{
      return false;
    }
  }
}

import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/create_user/create_user/bean/create_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/bean/person_detail_response_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/widgets/person_detail_invitation_tips_dialog/sms_send_out_success_tips_dialog.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:e_reception_flutter/utils/vg_reg_exp_utils.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_text_field_widget/vg_text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vg_base/vg_arch_lib.dart';
import '../inviter_sms_view_model.dart';

//发送短信邀请
class SendSMSInvitationTipsDialog extends StatefulWidget {
  final CreateUserUploadBean createUserUploadBean;
  final FaceUserInfoBean faceUserInfoBean;

  SendSMSInvitationTipsDialog({Key key, this.createUserUploadBean, this.faceUserInfoBean}) : super(key: key);


  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context,CreateUserUploadBean createUserUploadBean,FaceUserInfoBean faceUserInfoBean) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: SendSMSInvitationTipsDialog(
            createUserUploadBean:createUserUploadBean,
            faceUserInfoBean:faceUserInfoBean,
        ));
  }

  @override
  _SendSMSInvitationTipsDialogState createState() => _SendSMSInvitationTipsDialogState();
}

class _SendSMSInvitationTipsDialogState extends BaseState<SendSMSInvitationTipsDialog> {
  bool phoneSMSInvitation = false;

  String createUserNameAndPhone;

  String faceUserInfoNameAndPhone;

  // InviterFaceSendSms inviterFaceSendSms;
  //
  // ValueNotifier<InviterFaceSendSms> inviterFaceSendSmsBackValue;
  InviterSmsViewModel viewModel;
  @override
  void initState() {
    super.initState();
    // inviterFaceSendSms = new InviterFaceSendSms();
    viewModel = InviterSmsViewModel(this);
    // viewModel.inviterController.listen((t) {
    //   print(t.data);
    // });
    // viewModel.sendMsgController.stream.listen((event) {
    //   if(event?.success == null){
    //     VgToastUtils.toast(AppMain.context, "发送失败");
    //   }
    //   if(event?.success??false){
    //     smsSendOutSuccessTipsDialog.navigatorPushDialog(AppMain.context);
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    if(widget.createUserUploadBean != null){
      createUserNameAndPhone = widget.createUserUploadBean?.name??"-";
      if(_isNotEmptyString(widget.createUserUploadBean?.phone)){
        phoneSMSInvitation = true;
        createUserNameAndPhone += ", "+ widget.createUserUploadBean?.phone;
      }

    }
    if(widget.faceUserInfoBean != null){
      faceUserInfoNameAndPhone = widget.faceUserInfoBean?.name??"-";
      if(_isNotEmptyString(widget.faceUserInfoBean?.phone)){
        phoneSMSInvitation = true;
        faceUserInfoNameAndPhone += ", "+ widget.faceUserInfoBean?.phone;
      }
    }
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body:phoneSMSInvitation?sendSMSTips(context): sendSMSNoPhoneTips(context),
    );
  }

  Widget sendSMSTips(BuildContext context) {
    String nameAndNick =widget.faceUserInfoBean?.name??widget?.createUserUploadBean?.name??"";
    if(_isNotEmptyString(widget.faceUserInfoBean?.nick)){
      nameAndNick = widget?.faceUserInfoBean?.name + "/${widget?.faceUserInfoBean?.nick??""}";
    }
    if(_isNotEmptyString(widget.createUserUploadBean?.nick)){
      nameAndNick = widget?.createUserUploadBean?.name + "/${widget?.createUserUploadBean?.nick??""}";
    }

    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          width: 290,
          color: Color(0xFF303546),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _toCloseButtonWidget(context),
              // SizedBox(height: 30,),
              Text(
                "${createUserNameAndPhone??faceUserInfoNameAndPhone??""}",
                style:
                TextStyle(color: Color(0xFFD0E0F7), fontSize: 16),
              ),
              SizedBox(height: 20,),
              GestureDetector(
                onTap: () async {
                  if(widget?.faceUserInfoBean!=null){
                    if(!_isNotEmptyString(widget.faceUserInfoBean?.phone)){
                      VgToastUtils.toast(context, "请输入手机号");
                      return;
                    }
                    if(_isNotEmptyString(widget.faceUserInfoBean?.phone)){
                      if(!VgRegExpUtils.isPhone(widget.faceUserInfoBean?.phone)){
                        VgToastUtils.toast(context, "手机号格式不正确");
                        return;
                      }
                      //更新信息
                      viewModel?.appInviterFace(context,widget.faceUserInfoBean?.phone,widget.faceUserInfoBean?.fuid,nameAndNick,"02",pageName: "PersonDetailPage");
                    }
                  }

                  if(widget?.createUserUploadBean!=null){
                    if(!_isNotEmptyString(widget.createUserUploadBean?.phone)){
                      VgToastUtils.toast(context, "请输入手机号");
                      return;
                    }
                    if(_isNotEmptyString(widget.createUserUploadBean?.phone)){
                      if(!VgRegExpUtils.isPhone(widget.createUserUploadBean?.phone)){
                        VgToastUtils.toast(context, "手机号格式不正确");
                        return;
                      }
                      //更新信息
                      viewModel?.appInviterFace(context,widget.createUserUploadBean?.phone,widget.createUserUploadBean?.fuid,nameAndNick,"02",pageName: "CompanyDetailPage");
                    }
                  }

                  // RouterUtils.popUntil(context,"PersonDetailPage");
                 // dynamic result = await smsSendOutSuccessTipsDialog.navigatorPushDialog(context);

                  // RouterUtils.pop(context);
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    width: 230,
                    color: Color(0xFF1890FF),
                    // padding: const EdgeInsets.symmetric(horizontal: 30),
                    height: 45,
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.circular(4),
                    //   border: Border.all(color: Color(0xFFD0E0F7).withOpacity(0.3),
                    //       width: 1),
                    // ),
                    child: Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text("确定发送", style: TextStyle(color: Colors.white,
                              fontSize: 14), textAlign: TextAlign.center,),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 12,),
              // GestureDetector(
              //   behavior: HitTestBehavior.translucent,
              //   onTap: (){
              //     RouterUtils.pop(context);
              //   },
              //   child: Container(
              //     height: 45,
              //     alignment: Alignment.center,
              //     child: Text(
              //       "取消",
              //       style:
              //       TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 14),
              //     ),
              //   ),
              // ),
              SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );
  }

  Widget sendSMSNoPhoneTips(BuildContext context) {
    String _editText = "";
    String nameAndNick =widget.faceUserInfoBean?.name??widget?.createUserUploadBean?.name??"";
    if(_isNotEmptyString(widget.faceUserInfoBean?.nick)){
      nameAndNick =widget.faceUserInfoBean?.name + "/${widget.faceUserInfoBean?.nick??""}";
    }
    if(_isNotEmptyString(widget.createUserUploadBean?.nick)){
      nameAndNick = widget.createUserUploadBean?.name + "/${widget.createUserUploadBean?.nick??""}";
    }
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          width: 290,
          color: Color(0xFF303546),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _toCloseButtonWidget(context),
              // SizedBox(height: 30,),
              Text(
                "${createUserNameAndPhone??faceUserInfoNameAndPhone??""}",
                style:
                TextStyle(color: Color(0xFFD0E0F7), fontSize: 16),
              ),
              SizedBox(height: 15,),
              Container(
                height: 45,
                // width: 230,
                margin: const EdgeInsets.symmetric(horizontal: 30),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Color(0xFF191E31),
                ),

                child: VgTextField(
                  maxLength: 11,
                  maxLines: 1,
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter(RegExp("[0-9+]")),
                    LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT)
                  ],
                  // controller: itemBean.controller,
                  onChanged: (String editPhone){
                    _editText = editPhone;
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    counterText: "",
                    contentPadding: const EdgeInsets.symmetric(horizontal: 14,vertical: 12),
                    hintText: "请输入手机号码",
                    hintStyle: TextStyle(
                        color: ThemeRepository.getInstance()
                            .getTextEditHintColor_3A3F50(),
                        fontSize: 14,height: 1.3),
                  ),
                  style: TextStyle(
                      fontSize: 14,
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),height: 1.3),
                ),
              ),
              SizedBox(height: 12,),
              GestureDetector(
                onTap: () async{//异步
                  if(_editText ==""){
                    VgToastUtils.toast(context, "请输入手机号");
                    return;
                  }
                  if(_editText !=""){
                    if(!VgRegExpUtils.isPhone(_editText)){
                      VgToastUtils.toast(context, "手机号格式不正确");
                      return;
                    }
                    if(widget.faceUserInfoBean!=null){
                      viewModel?.appInviterFace(context,_editText,widget.faceUserInfoBean?.fuid,nameAndNick,"02",pageName: "PersonDetailPage");
                    }
                    if(widget.createUserUploadBean!=null){
                      viewModel?.appInviterFace(context,_editText,widget.createUserUploadBean?.fuid,nameAndNick,"02",pageName: "CompanyDetailPage");
                    }
                  }
                  // await smsSendOutSuccessTipsDialog.navigatorPushDialog(context);

                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    width: 230,
                    color: Color(0xFF1890FF),
                    // padding: const EdgeInsets.symmetric(horizontal: 30),
                    height: 45,
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.circular(4),
                    //   border: Border.all(color: Color(0xFFD0E0F7).withOpacity(0.3),
                    //       width: 1),
                    // ),
                    child: Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text("确定发送", style: TextStyle(color: Colors.white,
                              fontSize: 14), textAlign: TextAlign.center,),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30,),
            ],
          ),
        ),
      ),
    );
  }

  ///关闭按钮
  Widget _toCloseButtonWidget(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          // RouterUtils.popUntil(context,"PersonDetailPage");
          RouterUtils.pop(context);
        },
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Image.asset(
            "images/login_close_ico.png",
            width: 12,
            gaplessPlayback: true,
          ),
        ),
      ),
    );
  }


  static bool _isNotEmptyString(String text){
    if(text?.trim() !=null && text?.trim() !=""){
      return true;
    }else{
      return false;
    }
  }
}


import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_dialog_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class smsSendOutSuccessTipsDialog extends StatelessWidget {
  ///跳转方法
  static Future<String> navigatorPushDialog(BuildContext context) {
    return VgDialogUtils.showCommonDialog<String>(
        barrierDismissible: true,
        context: context,
        child: smsSendOutSuccessTipsDialog());
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Material(
            type: MaterialType.transparency,
            child: smsSendOutSuccessTips(context)));
  }

  Widget smsSendOutSuccessTips(BuildContext context) {
    return Container(
      width: 260,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Color(0xB3000000),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          Text(
            "短信发送成功",
            style: TextStyle(color: Colors.white, fontSize: 16),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 6,
          ),
          Text(
            "短信服务有延时，对方约在15分钟后收到",
            style: TextStyle(color: Colors.white, fontSize: 12),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 18,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              RouterUtils.pop(context);
            },
            child: Container(
              height: 20,
              alignment: Alignment.center,
              child: Text(
                "我知道了",
                style: TextStyle(color: Colors.blue, fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: 21,
          ),
        ],
      ),
    );
  }
}

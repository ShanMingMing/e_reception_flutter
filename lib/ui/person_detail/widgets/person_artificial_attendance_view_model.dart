import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/utils/app_monitoring_class.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class PersonArtificialAttendanceViewModel extends BaseViewModel {
  static const String MANUAL_ADD_USER_API =ServerApi.BASE_URL + "app/manualAddFacePunchInfo";
  PersonArtificialAttendanceViewModel(BaseState<StatefulWidget> state) : super(state);

  //用户手动打卡
  void manualAddFacePunchInfo(BuildContext context,String fuid,String fid,String punchTime,String backup) {
    if (StringUtils.isEmpty(fuid)) {
      VgToastUtils.toast(context, "人员获取信息错误");
      return;
    }
    VgHttpUtils.post(MANUAL_ADD_USER_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fuid": fuid ?? "",
          "fid": fid ?? "",
          "backup": backup ?? "",
          "punchTime": VgDateTimeUtils.getStringToIntMillisecond(punchTime) ?? ""
        },
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, "打卡成功");
          VgEventBus.global.send(new ArtificialAttendanceUpdateEvent());
          RouterUtils.pop(context);
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }
}
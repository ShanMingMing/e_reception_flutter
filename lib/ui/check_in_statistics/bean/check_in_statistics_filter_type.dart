enum CheckInStatisticsFilterType{
  //已刷脸，
  finishFace,
  //未刷脸，
  unfinishFace,
  //无选项
  none,
}
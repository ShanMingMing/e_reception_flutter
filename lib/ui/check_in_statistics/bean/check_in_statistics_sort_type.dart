///排序方式
enum CheckInStatisticsSortType{
  //最早，
  earliest,
  //最晚，
  latest,
  //时长
  duration

}

String parseSortType(CheckInStatisticsSortType type) {
  switch (type.index) {
    case 0:
      return '按到的最早';
    case 1:
      return '按最新刷脸';
    case 2:
      return '按时间最长';
    default:
      return '按到的最早';
  }
}

String parseSortTypeParams(CheckInStatisticsSortType type) {
  //排序类型00最新 01最早 02时长
  switch (type.index) {
    case 0:
      return '01';
    case 1:
      return '00';
    case 2:
      return '02';
    default:
      return '01';
  }
}

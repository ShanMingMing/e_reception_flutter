import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_change_status_type.dart';

class CheckInStatisticsChangeUploadBean{
  final CheckInStatisticsChangeStatusType changeStatusType;

  final String changeBackUp;

  final String startTime;
  final String endTime;

  CheckInStatisticsChangeUploadBean({this.startTime, this.endTime, this.changeStatusType, this.changeBackUp});

}
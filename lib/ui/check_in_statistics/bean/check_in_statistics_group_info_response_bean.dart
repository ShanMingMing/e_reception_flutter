/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"hsnInfo":[{"terminalName":"岁岁的终端","hsn":"a69ab51225adaf8b"},{"terminalName":"小米8SE","hsn":"e57477b75c986715"},{"terminalName":"北001","hsn":"15CC8E20433E81F2BF2480D51244431133"},{"terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0"}],"shouldAttendanceNum":0,"unSignCnt":1,"groupInfo":[{"groupName":"员工","groupType":"00","groupid":"fe49851d6dda41389a7156d386b0e1df","visitor":"01","recorded":"00","cnt":0},{"groupName":"访客","groupid":",b31c8818fb054fbf99af7febee529722","cnt":0,"visitor":"00","recorded":"01"}]}
/// extra : null

class CheckInStatisticsGroupInfoResponseBean {
  bool success;
  String code;
  String msg;
  CheckInStatisticsGroupInfoDataBean data;
  dynamic extra;

  static CheckInStatisticsGroupInfoResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInStatisticsGroupInfoResponseBean checkInStatisticsGroupInfoResponseBeanBean = CheckInStatisticsGroupInfoResponseBean();
    checkInStatisticsGroupInfoResponseBeanBean.success = map['success'];
    checkInStatisticsGroupInfoResponseBeanBean.code = map['code'];
    checkInStatisticsGroupInfoResponseBeanBean.msg = map['msg'];
    checkInStatisticsGroupInfoResponseBeanBean.data = CheckInStatisticsGroupInfoDataBean.fromMap(map['data']);
    checkInStatisticsGroupInfoResponseBeanBean.extra = map['extra'];
    return checkInStatisticsGroupInfoResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// hsnInfo : [{"terminalName":"岁岁的终端","hsn":"a69ab51225adaf8b"},{"terminalName":"小米8SE","hsn":"e57477b75c986715"},{"terminalName":"北001","hsn":"15CC8E20433E81F2BF2480D51244431133"},{"terminalName":"小米pad","hsn":"11140C1F12FEEB2C52DFBE67ED3E634AA0"}]
/// shouldAttendanceNum : 0
/// unSignCnt : 1
/// groupInfo : [{"groupName":"员工","groupType":"00","groupid":"fe49851d6dda41389a7156d386b0e1df","visitor":"01","recorded":"00","cnt":0},{"groupName":"访客","groupid":",b31c8818fb054fbf99af7febee529722","cnt":0,"visitor":"00","recorded":"01"}]

class CheckInStatisticsGroupInfoDataBean {
  List<HsnInfoBean> hsnInfo;
  int shouldAttendanceNum;
  int unSignCnt;
  List<GroupInfoBean> groupInfo;

  static CheckInStatisticsGroupInfoDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInStatisticsGroupInfoDataBean dataBean = CheckInStatisticsGroupInfoDataBean();
    dataBean.hsnInfo = List()..addAll(
      (map['hsnInfo'] as List ?? []).map((o) => HsnInfoBean.fromMap(o))
    );
    dataBean.shouldAttendanceNum = map['shouldAttendanceNum'];
    dataBean.unSignCnt = map['unSignCnt'];
    dataBean.groupInfo = List()..addAll(
      (map['groupInfo'] as List ?? []).map((o) => GroupInfoBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "hsnInfo": hsnInfo,
    "shouldAttendanceNum": shouldAttendanceNum,
    "unSignCnt": unSignCnt,
    "groupInfo": groupInfo,
  };
}

/// groupName : "员工"
/// groupType : "00"
/// groupid : "fe49851d6dda41389a7156d386b0e1df"
/// visitor : "01"
/// recorded : "00"
/// cnt : 0

class GroupInfoBean {
  String groupName;
  String groupType;
  String groupid;
  String visitor;
  String recorded;
  int cnt;

  static GroupInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    GroupInfoBean groupInfoBean = GroupInfoBean();
    groupInfoBean.groupName = map['groupName'];
    groupInfoBean.groupType = map['groupType'];
    groupInfoBean.groupid = map['groupid'];
    groupInfoBean.visitor = map['visitor'];
    groupInfoBean.recorded = map['recorded'];
    groupInfoBean.cnt = map['cnt'];
    return groupInfoBean;
  }

  Map toJson() => {
    "groupName": groupName,
    "groupType": groupType,
    "groupid": groupid,
    "visitor": visitor,
    "recorded": recorded,
    "cnt": cnt,
  };
}

/// terminalName : "岁岁的终端"
/// hsn : "a69ab51225adaf8b"

class HsnInfoBean {
  String terminalName;
  String hsn;

  static HsnInfoBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HsnInfoBean hsnInfoBean = HsnInfoBean();
    hsnInfoBean.terminalName = map['terminalName'];
    hsnInfoBean.hsn = map['hsn'];
    return hsnInfoBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
  };
}
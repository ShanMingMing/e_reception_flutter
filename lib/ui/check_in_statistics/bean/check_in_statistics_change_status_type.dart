import 'package:e_reception_flutter/generated/l10n.dart';

///00未忽略 01已忽略 02外出 03请假 04旷工

enum CheckInStatisticsChangeStatusType {
  UnIgnore, //未忽略
  Ignore, //已忽略
  GoOut, //外出
  Leave, //请假
  Absence, //旷工
}

extension CheckInStatisticsChangeStatusTypeExtension
    on CheckInStatisticsChangeStatusType {
  ///枚举转Id
  String getTypeToId() {
    switch (this) {
      case CheckInStatisticsChangeStatusType.UnIgnore:
        return "00";
      case CheckInStatisticsChangeStatusType.Ignore:
        return "01";
      case CheckInStatisticsChangeStatusType.GoOut:
        return "02";
      case CheckInStatisticsChangeStatusType.Leave:
        return "03";
      case CheckInStatisticsChangeStatusType.Absence:
        return "05";
    }
    return null;
  }

  ///枚举转String
  String getTypeToStr() {
    switch (this) {
      case CheckInStatisticsChangeStatusType.UnIgnore:
        return "忽略";
      case CheckInStatisticsChangeStatusType.Ignore:
        return "已忽略";
      case CheckInStatisticsChangeStatusType.GoOut:
        return "外出";
      case CheckInStatisticsChangeStatusType.Leave:
        return "已请假";
      case CheckInStatisticsChangeStatusType.Absence:
        return "已请假";
    }
    return null;
  }

  ///默认活跃状态
  bool isAlive() {
    switch (this) {
      case CheckInStatisticsChangeStatusType.UnIgnore:
        return true;
      case CheckInStatisticsChangeStatusType.Ignore:
      case CheckInStatisticsChangeStatusType.GoOut:
      case CheckInStatisticsChangeStatusType.Leave:
      case CheckInStatisticsChangeStatusType.Absence:
        return false;
    }
    return null;
  }

  static CheckInStatisticsChangeStatusType getIdToType(String status) {
    if (status == null || status.isEmpty) {
      return null;
    }
    switch (status) {
      case "00":
        return CheckInStatisticsChangeStatusType.UnIgnore;
      case "01":
        return CheckInStatisticsChangeStatusType.Ignore;
      case "02":
        return CheckInStatisticsChangeStatusType.GoOut;
      case "03":
        return CheckInStatisticsChangeStatusType.Leave;
      case "05":
        return CheckInStatisticsChangeStatusType.Absence;
    }
    return null;
  }

  ///获取相反类型
  CheckInStatisticsChangeStatusType getReverseType() {
    if(this == null){
      return CheckInStatisticsChangeStatusType.Ignore;
    }
    switch (this) {
      case CheckInStatisticsChangeStatusType.UnIgnore:
        return CheckInStatisticsChangeStatusType.Ignore;
      // case CheckInStatisticsChangeStatusType.Ignore:
      // case CheckInStatisticsChangeStatusType.GoOut:
      // case CheckInStatisticsChangeStatusType.Leave:
      // case CheckInStatisticsChangeStatusType.Absence:
      //   return CheckInStatisticsChangeStatusType.UnIgnore;
      default:
        return CheckInStatisticsChangeStatusType.UnIgnore;
    }
  }
}

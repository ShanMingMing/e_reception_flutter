import 'package:e_reception_flutter/constants/call_back.dart';
import 'package:e_reception_flutter/net/net_api.dart';
import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_change_status_type.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_dialog_value_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/class_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_detail/bean/department_or_class_list_response.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_page.dart';
import 'package:e_reception_flutter/ui/company/company_detail/class_or_department/class_or_department_viewmodel.dart';
import 'package:e_reception_flutter/utils/vg_date_time_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';

class CheckInStatisicsViewModel extends BaseViewModel {
  ///App单日已未刷脸记录
  static const String _CHANGE_FACE_TYPE_API =
     ServerApi.BASE_URL + "app/appChangeFaceType";

  CheckInStatisticsPageState state;

  ValueNotifier<List<MDepartmentListBean>> DCvalueNotifier;
  ValueNotifier<List<MClassListBean>> classvalueNotifier;

  CheckInStatisicsViewModel(BaseState<StatefulWidget> state) : super(state) {
    if (state!=null&&state is CheckInStatisticsPageState) {
      this.state = state;
    }
    // this.state = state;
    DCvalueNotifier = ValueNotifier(null);
    classvalueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    DCvalueNotifier.dispose();
    classvalueNotifier.dispose();
    super.onDisposed();
  }

  ///改变人脸类型
  void httpChangeFaceType(
    String id,
    CheckInStatisticsChangeStatusType statusType,
    String backUp,String startdate,String enddate,String fuid,
  {VgBaseCallback callback}
  ) {
    if (id == null || id.isEmpty) {
      VgToastUtils.toast(AppMain.context, "获取人信息错误");
      return;
    }
    if (statusType == null) {
      VgToastUtils.toast(AppMain.context, "切换状态错误");
      return;
    }
    VgHttpUtils.get(_CHANGE_FACE_TYPE_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "backup": backUp ?? "",
          "id": id ?? "",
          "type": statusType?.getTypeToId() ?? "",
          "startdate":VgDateTimeUtils.getStringToIntMillisecond(startdate) ?? 0,
          "enddate" : VgDateTimeUtils.getStringToIntMillisecond(enddate) ?? 0,
          "fuid" : fuid??"",
          "coverFlg":"01",
        },
        callback: BaseCallback(
            onSuccess: (val) {
              callback?.onSuccess(val);
            },
            onError: (msg) {
              callback?.onError(msg);
              VgToastUtils.toast(AppMain.context, msg);
            }));
  }

  ///列表数据
  void getData(String type,{String keyword}) {

    //   type(00部门 01班级)
    Map<String, dynamic> params = {
      'authId': UserRepository.getInstance().authId,
      "current": 1,
      "size": '100',
      "type": type ?? "",
      "searchName": keyword ?? ''
    };
    VgHttpUtils.post(NetApi.DEPARTMENT_OR_CLASS_LIST,
        params: params,
        callback: BaseCallback(onSuccess: (jsonMap) {
          // CheckDialogValueBean responseBean =
          // CheckDialogValueBean.fromMap(jsonMap);
          // DCvalueNotifier?.value = responseBean;
          if (jsonMap != null) {
            if (type == "00") {
              List<MDepartmentListBean> depaetmentData = List<MDepartmentListBean>();
              DepartmentOrClassListResponse response =
              DepartmentOrClassListResponse.fromMap(jsonMap, ClassOrDepartmentPage.TYPE_DEPARTMENT);
              response?.data?.page?.records?.forEach((element) {
                MDepartmentListBean beanItem = MDepartmentListBean();
                beanItem?.department = element?.department;
                beanItem?.departmentid = element?.departmentid;
                depaetmentData?.add(beanItem);
              });
              DCvalueNotifier?.value = depaetmentData;
            }

            if (type == "01") {
              DepartmentOrClassListResponse response = DepartmentOrClassListResponse.fromMap(jsonMap, ClassOrDepartmentPage.TYPE_CLASS);
              List<MClassListBean> MClassList = List<MClassListBean>();
              response?.data?.page?.records?.forEach((element) {
                MClassListBean beanItem = MClassListBean();
                beanItem?.classname = element?.classname;
                beanItem?.classid = element?.classid;
                MClassList?.add(beanItem);
              });
              classvalueNotifier?.value = MClassList;
              // List<MClassListBean> classData = responseBean?.data?.MClassList;
            }
          }
        }, onError: (e) {
          VgToastUtils.toast(AppMain.context, e);
        }));
  }

  ///简化列表数据
  void getSimpleData(String type,{String keyword}) {

    //   type(00部门 01班级)
    Map<String, dynamic> params = {
      'authId': UserRepository.getInstance().authId,
      "current": 1,
      "size": '100',
      "type": type ?? "",
      "searchName": keyword ?? ''
    };
    VgHttpUtils.post(NetApi.SELECT_DEPARTMENT_OR_CLASS_LIST,
        params: params,
        callback: BaseCallback(onSuccess: (jsonMap) {
          if (jsonMap != null) {
            if (type == "00") {
              List<MDepartmentListBean> depaetmentData = List<MDepartmentListBean>();
              DepartmentOrClassListResponse response =
              DepartmentOrClassListResponse.fromMap(jsonMap, ClassOrDepartmentPage.TYPE_DEPARTMENT);
              response?.data?.page?.records?.forEach((element) {
                MDepartmentListBean beanItem = MDepartmentListBean();
                beanItem?.department = element?.department;
                beanItem?.departmentid = element?.departmentid;
                depaetmentData?.add(beanItem);
              });
              DCvalueNotifier?.value = depaetmentData;
            }

            if (type == "01") {
              DepartmentOrClassListResponse response = DepartmentOrClassListResponse.fromMap(jsonMap, ClassOrDepartmentPage.TYPE_CLASS);
              List<MClassListBean> MClassList = List<MClassListBean>();
              response?.data?.page?.records?.forEach((element) {
                MClassListBean beanItem = MClassListBean();
                beanItem?.classname = element?.classname;
                beanItem?.classid = element?.classid;
                MClassList?.add(beanItem);
              });
              classvalueNotifier?.value = MClassList;
              // List<MClassListBean> classData = responseBean?.data?.MClassList;
            }
          }
        }, onError: (e) {
          VgToastUtils.toast(AppMain.context, e);
        }));
  }
}



class ChangeFaceTypeRefreshEvent{}

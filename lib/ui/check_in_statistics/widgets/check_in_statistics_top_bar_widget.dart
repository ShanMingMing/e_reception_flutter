import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/visitor_list/visitor_list_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';

/// 打卡记录topbar
///
/// @author: zengxiangxi
/// @createTime: 3/25/21 11:27 AM
/// @specialDemand:
class CheckInStatisticsTopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return VgTopBarWidget(
      isShowBack: true,
      centerWidget: _toCenterWidget(context),
      rightPadding: 0,
      rightWidget: _toRightWidget(context),
    );
  }

  Widget _toCenterWidget(BuildContext context) {
    final DateTime dateTime =
        CheckInStatisticsPageState.of(context).currentTimeByWeekHours;
    double mWidth = 85;
    String dateStr = VgToolUtils.getDateStr(dateTime);
    if(dateStr?.contains(" ")??false){
      mWidth = 115;
    }
    final DateTime nowInitDateTime = _getNowInitDateTime();
    if(nowInitDateTime.year != dateTime.year) {
      mWidth = 125;
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){
            _subDateTime(context);
          },
          child: Container(
              width: 40,
              alignment: Alignment(0.7,0),
              child: Image.asset(
                "images/calendar_previous_ico.png",
                width: 16,
              )),
        ),
        Container(
          width: mWidth,
          child: Center(
            child: Text(
              dateStr ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
                height: 1.2,
              ),
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){
            if(isEquealDateTime(dateTime, _getNowInitDateTime())){
              return;
            }
            _addDateTime(context);
          },
          child: Opacity(
            opacity: isEquealDateTime(dateTime, _getNowInitDateTime()) ? 0.3:1,
            child: Container(
                width: 40,
                alignment: Alignment(-0.7,0),
                child: Image.asset(
                  "images/calendar_next_ico.png",
                  width: 16,
                )),
          ),
        ),
      ],
    );
  }

  ///获取日期串
  String _getDateStr(DateTime currentDateTime){
    if(currentDateTime == null){
      return "-";
    }
    final DateTime nowInitDateTime = _getNowInitDateTime();

    StringBuffer stringBuffer = StringBuffer();

    if(nowInitDateTime.year == currentDateTime.year) {
      if (nowInitDateTime == currentDateTime) {
        stringBuffer.write("今天 ");
      } else if (nowInitDateTime == currentDateTime.add(Duration(days: 1))) {
        stringBuffer.write("昨天 ");
      } else if (nowInitDateTime == currentDateTime.add(Duration(days: 2))) {
        stringBuffer.write("前天 ");
      }
    }else{
      stringBuffer.write("${currentDateTime.year}年");
    }

    stringBuffer.write("${currentDateTime.month}月${currentDateTime.day}日");
    return stringBuffer.toString();
  }

  bool isEquealDateTime(DateTime currentDateTime,DateTime nowInidDateTime){
    if(currentDateTime == null || nowInidDateTime == null){
      return false;
    }
    return currentDateTime.year == nowInidDateTime.year
      &&currentDateTime.month == nowInidDateTime.month
      &&currentDateTime.day == nowInidDateTime.day;
  }

  void _subDateTime(BuildContext context){
    CheckInStatisticsPageState.of(context).changeCurrentDateTime(-1);
  }

  void _addDateTime(BuildContext context){
    CheckInStatisticsPageState.of(context).changeCurrentDateTime(1);
  }

  ///获取现在凌晨时间
  DateTime _getNowInitDateTime(){
    return VgToolUtils.getMaxTimeZoneDateTime();
  }

  Widget _toRightWidget(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: CheckInStatisticsPageState.of(context)?.infoValueNotifier,
      builder: (BuildContext context, CheckInStatisticsListDataBean infoBean, Widget child) {
       return GestureDetector(
         behavior: HitTestBehavior.translucent,
         onTap: (){
           VisitorListPage.navigatorPush(context,CheckInStatisticsPageState.of(context).currentTimeByWeekHours);
         },
         child: Container(
           width: 86,
           height: 44,
           child:  Center(
             child: Text(
               "访客·${infoBean?.dayVisitCnt ?? 0}",
               maxLines: 1,
               overflow: TextOverflow.ellipsis,
               style: TextStyle(
                 color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                 fontSize: 13,
               ),
             ),
           ),
         ),
       );
      },
    );
  }
}

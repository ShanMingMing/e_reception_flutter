import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_filter_type.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_sort_type.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/check_in_sort_pop_window.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/check_in_statistics_classify_button_widget.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_more_pop_window.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_dialog_widget/pop_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 打卡记录-排序弹窗

class CheckInStatisticsFilterWidget extends StatefulWidget {
  final CheckInStatisticsListDataBean checkInStatisticsListDataBean;

  const CheckInStatisticsFilterWidget({Key key, this.checkInStatisticsListDataBean}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CheckInStatisticsFilterState(

    );
  }
}

class CheckInStatisticsFilterState
    extends State<CheckInStatisticsFilterWidget> {
  GlobalKey<CheckInStatisticsFilterState> popKey;

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((val) {
    //   popKey = GlobalKey();
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      color: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      child: _toMainRowWidget(context),
    );
  }

  Widget _toMainRowWidget(
    BuildContext context,
  ) {


    return Row(
      children: <Widget>[
        Expanded(child: _toFilterButtonWidget()),
        _sortButtonWidget(context)
      ],
    );
  }

  Widget _toFilterButtonWidget() {
    return CheckInStatisticsClassifyButtonWidget();
  }

  Widget _sortButtonWidget(context) {
    final CheckInStatisticsPageState parentState =
        CheckInStatisticsPageState.of(context);
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        PopupWindow.showPopWindow(
            context,
            "bottom",
            popKey,
            PopDirection.bottom,
            CheckInSortPopWindow(
              sortType: parentState?.sortValueNotifier?.value,
              onClick: (CheckInStatisticsSortType type) {
                parentState?.sortValueNotifier?.value = type;
                parentState?.refreshMulitiAllListStreamController?.add(null);
              },
            ),
            -70,
            0);
      },
      child: ValueListenableBuilder(
        valueListenable: parentState?.sortValueNotifier,
        builder: (BuildContext context, CheckInStatisticsSortType sortType,
            Widget child) {
          popKey = GlobalKey();
          return Container(
            height: 35,
            key: popKey,
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: 15),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  parseSortType(sortType),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 12,
                  ),
                ),
                Container(
                  height: 35,
                  padding: EdgeInsets.only(top: 2, left: 4),
                  child: Image.asset(
                    'images/icon_arrow_down_gray.png',
                    width: 7,
                    height: 4,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}

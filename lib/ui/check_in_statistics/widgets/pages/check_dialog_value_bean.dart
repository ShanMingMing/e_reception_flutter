import 'package:vg_base/vg_string_util_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"SpecialCnt":1,"MClassList":[{"classid":"7e3fef0bd81741c0a351a4f2194cea40","classname":"初级歌唱班"},{"classid":"6b52fa31241c4ff990787ac7a6d2a1ea","classname":"34"},{"classid":"50e6dbb3cfaf4091924c9acfcd12f3d0","classname":"中级歌唱班"},{"classid":"34fd1fe087b2451a9356219a0bd9e559","classname":"12345678978675653446576564534"},{"classid":"9de2c5e3f5514035b0d38149a8cc59c7","classname":"213454"}],"MDepartmentCnt":5,"MClassCnt":5,"MDepartmentList":[{"departmentid":"d5267fea78d0449cbc6f636f200e7fed","department":"4乳地方"},{"departmentid":"501c2993e96f4baf8d86fff483ce2b68","department":"没有如果"},{"departmentid":"400e275ab1d54f80afa12416ffcc04a4","department":"他说2"},{"departmentid":"282e7ea52b584d3387a9a6422dfc222f","department":"明天你好"},{"departmentid":"f4158cb18af945be8b9fc1a1bc51bafc","department":"江南"}]}
/// extra : null

class CheckDialogValueBean {
  bool success;
  String code;
  String msg;
  DepartmentAndClassDataBean data;
  dynamic extra;

  static CheckDialogValueBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckDialogValueBean checkDialogValueBeanBean = CheckDialogValueBean();
    checkDialogValueBeanBean.success = map['success'];
    checkDialogValueBeanBean.code = map['code'];
    checkDialogValueBeanBean.msg = map['msg'];
    checkDialogValueBeanBean.data = DepartmentAndClassDataBean.fromMap(map['data']);
    checkDialogValueBeanBean.extra = map['extra'];
    return checkDialogValueBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };
}

/// SpecialCnt : 1
/// MClassList : [{"classid":"7e3fef0bd81741c0a351a4f2194cea40","classname":"初级歌唱班"},{"classid":"6b52fa31241c4ff990787ac7a6d2a1ea","classname":"34"},{"classid":"50e6dbb3cfaf4091924c9acfcd12f3d0","classname":"中级歌唱班"},{"classid":"34fd1fe087b2451a9356219a0bd9e559","classname":"12345678978675653446576564534"},{"classid":"9de2c5e3f5514035b0d38149a8cc59c7","classname":"213454"}]
/// MDepartmentCnt : 5
/// MClassCnt : 5
/// MDepartmentList : [{"departmentid":"d5267fea78d0449cbc6f636f200e7fed","department":"4乳地方"},{"departmentid":"501c2993e96f4baf8d86fff483ce2b68","department":"没有如果"},{"departmentid":"400e275ab1d54f80afa12416ffcc04a4","department":"他说2"},{"departmentid":"282e7ea52b584d3387a9a6422dfc222f","department":"明天你好"},{"departmentid":"f4158cb18af945be8b9fc1a1bc51bafc","department":"江南"}]

class DepartmentAndClassDataBean {
  int SpecialCnt;
  int ruleCnt;//考勤规则数
  List<MClassListBean> MClassList;
  int MDepartmentCnt;
  int MClassCnt;
  int MHsnCnt;
  List<MDepartmentListBean> MDepartmentList;
  List<MHsnListBean> MHsnList;

  static DepartmentAndClassDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DepartmentAndClassDataBean dataBean = DepartmentAndClassDataBean();
    dataBean.SpecialCnt = map['SpecialCnt'];
    dataBean.MClassList = List()..addAll(
      (map['MClassList'] as List ?? []).map((o) => MClassListBean.fromMap(o))
    );
    dataBean.MDepartmentCnt = map['MDepartmentCnt'];
    dataBean.MClassCnt = map['MClassCnt'];
    dataBean.MHsnCnt = map['MHsnCnt'];
    dataBean.ruleCnt = map['ruleCnt'];
    dataBean.MDepartmentList = List()..addAll(
      (map['MDepartmentList'] as List ?? []).map((o) => MDepartmentListBean.fromMap(o))
    );
    dataBean.MHsnList = List()..addAll(
        (map['MHsnList'] as List ?? []).map((o) => MHsnListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "SpecialCnt": SpecialCnt,
    "MClassList": MClassList,
    "MDepartmentCnt": MDepartmentCnt,
    "MClassCnt": MClassCnt,
    "MDepartmentList": MDepartmentList,
    "MHsnList": MHsnList,
    "MHsnCnt":MHsnCnt
  };
}

/// departmentid : "d5267fea78d0449cbc6f636f200e7fed"
/// department : "4乳地方"

class MDepartmentListBean {
  String departmentid;
  String department;
  bool selected;

  static MDepartmentListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MDepartmentListBean mDepartmentListBean = MDepartmentListBean();
    mDepartmentListBean.departmentid = map['departmentid'];
    mDepartmentListBean.department = map['department'];
    mDepartmentListBean.selected = map['selected'];
    return mDepartmentListBean;
  }

  Map toJson() => {
    "departmentid": departmentid,
    "department": department,
    "selected": selected,
  };
}

/// classid : "7e3fef0bd81741c0a351a4f2194cea40"
/// classname : "初级歌唱班"

class MClassListBean {
  String classid;
  String classname;
  bool isSelect;
  bool isEdit;

  static MClassListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MClassListBean mClassListBean = MClassListBean();
    mClassListBean.classid = map['classid'];
    mClassListBean.classname = map['classname'];
    mClassListBean.isEdit = map['isEdit'];
    mClassListBean.isSelect = map['isSelect'];
    return mClassListBean;
  }

  Map toJson() => {
    "classid": classid,
    "classname": classname,
    "isSelect": isSelect,
    "isEdit": isEdit,
  };
}

/// terminalName : "小米pad"
/// hsn : "11140C1F12FEEB2C52DFBE67ED3E634AA0"

class MHsnListBean {
  String terminalName;
  String hsn;
  String sideNumber;

  String getTerminalName(){
    if(StringUtils.isNotEmpty(terminalName)){
      return terminalName;
    }
    if(StringUtils.isNotEmpty(sideNumber)){
      return sideNumber;
    }
    if(StringUtils.isNotEmpty(hsn)){
      return hsn;
    }
    return "终端显示屏";
  }

  static MHsnListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MHsnListBean mHsnListBean = MHsnListBean();
    mHsnListBean.terminalName = map['terminalName'];
    mHsnListBean.hsn = map['hsn'];
    mHsnListBean.sideNumber = map['sideNumber'];
    return mHsnListBean;
  }

  Map toJson() => {
    "terminalName": terminalName,
    "hsn": hsn,
    "sideNumber": sideNumber,
  };
}
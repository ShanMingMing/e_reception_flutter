import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_change_status_type.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"eachFaceCnt":{"facecnt":3,"unfacecnt":7},"ruleTime":{"arid":"1a3ba772478840a49e763267f1528f72","offdutytime":1617100200,"ondutytime":1617067800},"page":{"records":[{"firsttime":1617069723,"lasttime":1617069723,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","name":"伟霆","napicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","fuid":"42a06dda6b4b4e18ae01ab91a1e8d22e","id":"369763e99dfaa0fecab46df21e9cf766","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069722,"lasttime":1617069722,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","fuid":"7978070856524c0d9c7b0df9849b987a","id":"b8b50e96fc264e19e24fe9880f026836","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069714,"lasttime":1617069714,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","name":"丽颖","napicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","fuid":"3f84b2240feb403a845c450f0bbfb103","id":"e04d9896747d25e0657e4b0f64eefe46","type":"00","department":null,"projectname":null,"classname":null,"coursename":null}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1},"punchRule":true}
/// extra : null

class CheckInStatisticsListResponseBean extends BasePagerBean<CheckInStatisticsListItemBean> {
  bool success;
  String code;
  String msg;
  CheckInStatisticsListDataBean data;
  dynamic extra;

  static CheckInStatisticsListResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInStatisticsListResponseBean checkInStatisticsListResponseBeanBean = CheckInStatisticsListResponseBean();
    checkInStatisticsListResponseBeanBean.success = map['success'];
    checkInStatisticsListResponseBeanBean.code = map['code'];
    checkInStatisticsListResponseBeanBean.msg = map['msg'];
    checkInStatisticsListResponseBeanBean.data = CheckInStatisticsListDataBean.fromMap(map['data']);
    checkInStatisticsListResponseBeanBean.extra = map['extra'];
    return checkInStatisticsListResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///得到List列表
  @override
  List<CheckInStatisticsListItemBean> getDataList() => data?.page?.records;

  ///最后页码
  @override
  int getMaxPage() => data?.page?.pages == null || data.page.pages <= 0 ? 1:data.page.pages;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.page?.records = list.cast();
  }

  ///当前页
  @override
  int getCurrentPage() => data?.page?.current == null || data.page.current <= 0 ? 1:data.page.current;
}

/// eachFaceCnt : {"facecnt":3,"unfacecnt":7}
/// ruleTime : {"arid":"1a3ba772478840a49e763267f1528f72","offdutytime":1617100200,"ondutytime":1617067800}
/// page : {"records":[{"firsttime":1617069723,"lasttime":1617069723,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","name":"伟霆","napicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","fuid":"42a06dda6b4b4e18ae01ab91a1e8d22e","id":"369763e99dfaa0fecab46df21e9cf766","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069722,"lasttime":1617069722,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","fuid":"7978070856524c0d9c7b0df9849b987a","id":"b8b50e96fc264e19e24fe9880f026836","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069714,"lasttime":1617069714,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","name":"丽颖","napicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","fuid":"3f84b2240feb403a845c450f0bbfb103","id":"e04d9896747d25e0657e4b0f64eefe46","type":"00","department":null,"projectname":null,"classname":null,"coursename":null}],"total":3,"size":20,"current":1,"orders":[],"searchCount":true,"pages":1}
/// punchRule : true

class CheckInStatisticsListDataBean {
  EachFaceCntBean eachFaceCnt;
  List<MClassListBean> MClassList;
  List<MDepartmentListBean> MDepartmentList;
  RuleTimeBean ruleTime;
  PageBean page;
  bool punchRule;
  int dayVisitCnt;

  static CheckInStatisticsListDataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInStatisticsListDataBean dataBean = CheckInStatisticsListDataBean();
    dataBean.eachFaceCnt = EachFaceCntBean.fromMap(map['eachFaceCnt']);
    dataBean.ruleTime = RuleTimeBean.fromMap(map['ruleTime']);
    dataBean.page = PageBean.fromMap(map['page']);
    dataBean.punchRule = map['punchRule'];
    dataBean.dayVisitCnt = map['dayVisitCnt'];
    dataBean.MClassList = List()..addAll(
        (map['MClassList'] as List ?? []).map((o) => MClassListBean.fromMap(o))
    );
    dataBean.MDepartmentList = List()..addAll(
        (map['MDepartmentList'] as List ?? []).map((o) => MDepartmentListBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "eachFaceCnt": eachFaceCnt,
    "ruleTime": ruleTime,
    "page": page,
    "punchRule": punchRule,
    "dayVisitCnt": dayVisitCnt,
    "MDepartmentList": MDepartmentList,
    "MClassList": MClassList,
  };
}

/// records : [{"firsttime":1617069723,"lasttime":1617069723,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","name":"伟霆","napicurl":"http://etpic.we17.com/test/20210324190507_3630.jpg","fuid":"42a06dda6b4b4e18ae01ab91a1e8d22e","id":"369763e99dfaa0fecab46df21e9cf766","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069722,"lasttime":1617069722,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","name":"杨幂","napicurl":"http://etpic.we17.com/test/20210324190520_5390.jpg","fuid":"7978070856524c0d9c7b0df9849b987a","id":"b8b50e96fc264e19e24fe9880f026836","type":"00","department":null,"projectname":null,"classname":null,"coursename":null},{"firsttime":1617069714,"lasttime":1617069714,"city":"","putpicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","name":"丽颖","napicurl":"http://etpic.we17.com/test/20210324190929_2271.jpg","fuid":"3f84b2240feb403a845c450f0bbfb103","id":"e04d9896747d25e0657e4b0f64eefe46","type":"00","department":null,"projectname":null,"classname":null,"coursename":null}]
/// total : 3
/// size : 20
/// current : 1
/// orders : []
/// searchCount : true
/// pages : 1

class PageBean {
  List<CheckInStatisticsListItemBean> records;
  int total;
  int size;
  int current;
  List<dynamic> orders;
  bool searchCount;
  int pages;

  static PageBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    PageBean pageBean = PageBean();
    pageBean.records = List()..addAll(
      (map['records'] as List ?? []).map((o) => CheckInStatisticsListItemBean.fromMap(o))
    );
    pageBean.total = map['total'];
    pageBean.size = map['size'];
    pageBean.current = map['current'];
    pageBean.orders = map['orders'];
    pageBean.searchCount = map['searchCount'];
    pageBean.pages = map['pages'];
    return pageBean;
  }

  Map toJson() => {
    "records": records,
    "total": total,
    "size": size,
    "current": current,
    "orders": orders,
    "searchCount": searchCount,
    "pages": pages,
  };
}

/// firsttime : 1617069723
/// lasttime : 1617069723
/// city : ""
/// putpicurl : "http://etpic.we17.com/test/20210324190507_3630.jpg"
/// name : "伟霆"
/// napicurl : "http://etpic.we17.com/test/20210324190507_3630.jpg"
/// fuid : "42a06dda6b4b4e18ae01ab91a1e8d22e"
/// id : "369763e99dfaa0fecab46df21e9cf766"
/// type : "00"
/// department : null
/// projectname : null
/// classname : null
/// coursename : null

class CheckInStatisticsListItemBean {
  int firsttime;
  int lasttime;
  String city;
  String putpicurl;
  String name;
  String napicurl;
  String fuid;
  String id;
  // String type;
  String department;
  String projectname;
  String classname;
  String coursename;
  String backup;
  //长期请假开始和结束时间
  String startTime;
  String endTime;
  String delflg;
  String temperature;
  String groupType;//groupType  01访客 02学员 03家长 04员工 99普通

  CheckInStatisticsChangeStatusType statusType;

  static CheckInStatisticsListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    CheckInStatisticsListItemBean recordsBean = CheckInStatisticsListItemBean();
    recordsBean.temperature = map['temperature'];
    recordsBean.groupType = map['groupType'];
    recordsBean.firsttime = map['firsttime'];
    recordsBean.lasttime = map['lasttime'];
    recordsBean.city = map['city'];
    recordsBean.putpicurl = map['putpicurl'];
    recordsBean.name = map['name'];
    recordsBean.napicurl = map['napicurl'];
    recordsBean.fuid = map['fuid'];
    recordsBean.id = map['id'];
    // recordsBean.type = map['type'];
    recordsBean.department = map['department'];
    recordsBean.projectname = map['projectname'];
    recordsBean.classname = map['classname'];
    recordsBean.coursename = map['coursename'];
    //自定义字段
    recordsBean.statusType = CheckInStatisticsChangeStatusTypeExtension.getIdToType(map['type']) ?? CheckInStatisticsChangeStatusType.UnIgnore;
    recordsBean.backup = map['backup'];
    recordsBean.startTime = map['startTime'];
    recordsBean.endTime = map['endTime'];
    recordsBean.delflg = map['delflg'];
    return recordsBean;
  }

  Map toJson() => {
    "groupType": groupType,
    "temperature": temperature,
    "firsttime": firsttime,
    "lasttime": lasttime,
    "city": city,
    "putpicurl": putpicurl,
    "name": name,
    "napicurl": napicurl,
    "fuid": fuid,
    "id": id,
    // "type": type,
    "department": department,
    "projectname": projectname,
    "classname": classname,
    "coursename": coursename,
    "backup": backup,
    "startTime": startTime,
    "endTime": endTime,
    "delflg": delflg,
  };
}

/// arid : "1a3ba772478840a49e763267f1528f72"
/// offdutytime : 1617100200
/// ondutytime : 1617067800

class RuleTimeBean {
  String arid;
  int offdutytime;
  int ondutytime;

  static RuleTimeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RuleTimeBean ruleTimeBean = RuleTimeBean();
    ruleTimeBean.arid = map['arid'];
    ruleTimeBean.offdutytime = map['offdutytime'];
    ruleTimeBean.ondutytime = map['ondutytime'];
    return ruleTimeBean;
  }

  Map toJson() => {
    "arid": arid,
    "offdutytime": offdutytime,
    "ondutytime": ondutytime,
  };
}

/// facecnt : 3
/// unfacecnt : 7

class EachFaceCntBean {
  int facecnt;
  int unfacecnt;

  static EachFaceCntBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EachFaceCntBean eachFaceCntBean = EachFaceCntBean();
    eachFaceCntBean.facecnt = map['facecnt'];
    eachFaceCntBean.unfacecnt = map['unfacecnt'];
    return eachFaceCntBean;
  }

  Map toJson() => {
    "facecnt": facecnt,
    "unfacecnt": unfacecnt,
  };
}

/// departmentid : "d5267fea78d0449cbc6f636f200e7fed"
/// department : "4乳地方"

class MDepartmentListBean {
  String departmentid;
  String department;

  static MDepartmentListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MDepartmentListBean mDepartmentListBean = MDepartmentListBean();
    mDepartmentListBean.departmentid = map['departmentid'];
    mDepartmentListBean.department = map['department'];
    return mDepartmentListBean;
  }

  Map toJson() => {
    "departmentid": departmentid,
    "department": department,
  };
}

/// classid : "9de2c5e3f5514035b0d38149a8cc59c7"
/// classname : "213454"

class MClassListBean {
  String classid;
  String classname;

  static MClassListBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MClassListBean mClassListBean = MClassListBean();
    mClassListBean.classid = map['classid'];
    mClassListBean.classname = map['classname'];
    return mClassListBean;
  }

  Map toJson() => {
    "classid": classid,
    "classname": classname,
  };
}
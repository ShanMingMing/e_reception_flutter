import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/check_in_record_index_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_change_status_type.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_change_upload_bean.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_lable_dialog.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/matching_mark_list_page.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/mark_info_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_page.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 打卡记录列表项-学校菜单组件
///
/// @author: zengxiangxi
/// @createTime: 3/25/21 4:34 PM
/// @specialDemand:
class CheckInStatisticsListItemSchoolMenuWidget extends StatelessWidget {
  final CheckInStatisticsListItemBean itemBean;

  final ValueChanged<CheckInStatisticsChangeUploadBean> onChanged;
  final DateTime startTime;

  const CheckInStatisticsListItemSchoolMenuWidget({Key key, this.itemBean, this.onChanged, this.startTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        if (itemBean == null) {
          return;
        }
        if(delflgUser(context)){
          return;
        }
        // if (itemBean.isUnknow()) {
        //   bool result = await MatchingMarkListPage.navigatorPush(
        //       context,
        //       MarkInfoBean(
        //         picurl: itemBean?.napicurl,
        //         fid: itemBean?.fid,
        //       ));
        //   if (result ?? false) {
        //     CheckInRecordIndexPageState.of(context)
        //         .multiRefreshStreamController
        //         ?.add(null);
        //   }
        //   return;
        // }
        // PersonDetailPage.navigatorPush(context, itemBean?.signFuid);
        AttendRecordCalendarPage.navigatorPush(context, itemBean?.fuid,pages: "AttendRecordCalendarPage");
      },
      child: Container(
        height: 60,
        child: _toMainRowWidget(context),
      ),
    );
  }

  Widget _toMainRowWidget(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(width: 15),
        _toHeadPicWidget(context),
        SizedBox(width: 9),
        Expanded(child: _toColumnWidget()),
        SizedBox(width: 10),
        _toIgnoreWidget(context),
        _toMenuWidget(context),
        // Offstage(
        //     offstage: (itemBean?.statusType?.isAlive() ?? false),
        //     child: _toHasStatusWidget(context)),
      ],
    );
  }

  Widget _toHeadPicWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        // VgPhotoPreview.single(
        //     context,
        //     showOriginEmptyStr(itemBean?.putpicurl) ??
        //         showOriginEmptyStr(itemBean?.napicurl ?? ""),
        //     loadingImageQualityType: ImageQualityType.middleDown);
        if(delflgUser(context)){
          return;
        }
        PersonDetailPage.navigatorPush(context, itemBean?.fuid,pages:"CheckInStatisticsPage");
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: VgCacheNetWorkImage(
          showOriginEmptyStr(itemBean?.putpicurl) ??
              (showOriginEmptyStr(itemBean?.napicurl) ?? ""),
          width: 37,
          height: 37,
          imageQualityType: ImageQualityType.middleUp,
          defaultPlaceType: ImagePlaceType.head,
          defaultErrorType: ImageErrorType.head,
        ),
      ),
    );
  }

  Widget _toColumnWidget() {
    return Container(
      height: 37,
      child: Column(
        mainAxisAlignment: (VgStringUtils.checkAllEmpty([
          itemBean?.classname?.trim(),
          itemBean?.department?.trim(),
        ]))
            ? MainAxisAlignment.center
            : MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CommonConstraintMaxWidthWidget(
                maxWidth: VgToolUtils.getScreenWidth() / 2,
                child: Text(
                  itemBean?.name ?? "-",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 15,
                      height: 1.2),
                ),
              ),
              Spacer(),
            ],
          ),
          if (!VgStringUtils.checkAllEmpty([
            itemBean?.classname?.trim(),
            itemBean?.department?.trim(),
          ]))
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                VgStringUtils.getSplitStr([
                  itemBean?.classname?.trim(),
                  itemBean?.department?.trim(),
                ], symbol: "/"),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                  fontSize: 12,
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget _toIgnoreWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        if(delflgUser(context)){
          return;
        }
        if(itemBean?.statusType?.isAlive() ?? false){
          onChanged?.call(CheckInStatisticsChangeUploadBean(
              changeStatusType: itemBean?.statusType?.getReverseType() ??
                  CheckInStatisticsChangeStatusType.Ignore));
        }else{
          String cancel;
          if(itemBean?.statusType?.getTypeToStr() == "外出"){
            cancel = "外出";
          }else if(itemBean?.statusType?.getTypeToStr() == "已请假"){
            cancel = "已请假";
          }
          bool result = await CommonConfirmCancelDialog.navigatorPushDialog(
              context,
              title: "提示",
              content: "确认取消${cancel??"忽略"}操作？",
              cancelText: "取消",
              confirmText: "确定",
              confirmBgColor: ThemeRepository.getInstance().getPrimaryColor_1890FF());
          if (result ?? false) {
            onChanged?.call(CheckInStatisticsChangeUploadBean(
                changeStatusType: itemBean?.statusType?.getReverseType() ??
                    CheckInStatisticsChangeStatusType.Ignore));
          }

        }
      },
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 15, right: 5),
        child: CommonFixedHeightConfirmButtonWidget(
          isAlive: itemBean?.statusType?.isAlive() ?? true,
          width: 60,
          height: 28,
          unSelectBgColor:
          ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: ThemeRepository.getInstance().getTextBFC2CC_BFC2CC(),
            fontSize: 13,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 13,
          ),
          text: itemBean?.statusType?.getTypeToStr() ?? "忽略",
        ),
      ),
    );
  }

  // Widget _toLeaveWidget(BuildContext context) {
  //   return GestureDetector(
  //     behavior: HitTestBehavior.translucent,
  //     onTap: () {
  //       if(delflgUser(context)){
  //         return;
  //       }
  //       onChanged?.call(CheckInStatisticsChangeUploadBean(
  //           changeStatusType: CheckInStatisticsChangeStatusType.Leave));
  //     },
  //     child: Container(
  //       alignment: Alignment.center,
  //       padding: const EdgeInsets.only(left: 5, right: 15),
  //       child: CommonFixedHeightConfirmButtonWidget(
  //         isAlive: true,
  //         width: 60,
  //         height: 28,
  //         unSelectBgColor:
  //             ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
  //         selectedBgColor:
  //             ThemeRepository.getInstance().getPrimaryColor_1890FF(),
  //         unSelectTextStyle: TextStyle(
  //           color: VgColors.INPUT_BG_COLOR,
  //           fontSize: 13,
  //         ),
  //         selectedTextStyle: TextStyle(
  //           color: Colors.white,
  //           fontSize: 13,
  //         ),
  //         text: "请假",
  //         onTap: () async {
  //           if(delflgUser(context)){
  //             return;
  //           }
  //           CheckInStatisticsChangeUploadBean uploadBean =
  //           await CheckInStatisticsLabelPage.navigatorPushDialog(
  //             context,
  //             itemBean?.name,
  //             itemBean?.statusType,
  //             itemBean?.backup,
  //             "CheckInStatisticsListItemSchoolMenuWidget"
  //           );
  //           if (uploadBean != null) {
  //             onChanged?.call(uploadBean);
  //           }
  //         },
  //         textRightSelectedWidget: Container(
  //           padding: const EdgeInsets.only(left: 4),
  //           child: Image(
  //             image: AssetImage("images/triangle_img.png"),
  //             width: 6,
  //             height: 4,
  //           ),
  //         ),
  //       ),
  //     ),
  //   );
  // }

  Widget _toMenuWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        if(delflgUser(context)){
          return;
        }
        CheckInStatisticsChangeUploadBean uploadBean;
        //员工弹窗与学元分开展示groupType  01访客 02学员 03家长 04员工 99普通
        if(itemBean?.groupType == "02"){
          uploadBean =
          await CheckInStatisticsLabelPage.navigatorPushDialog(
              context,
              itemBean?.name,
              itemBean?.statusType,
              itemBean?.backup,
              "CheckInStatisticsListItemSchoolMenuWidget",
            startTime
          );
        }else if(itemBean?.groupType == "04"){
          uploadBean =
          await CheckInStatisticsLabelPage.navigatorPushDialog(
              context,
              itemBean?.name,
              itemBean?.statusType,
              itemBean?.backup,
              "CheckInStatisticsListItemCompanyMenuWidget",
            startTime
          );
        }

        if (uploadBean != null) {
          onChanged?.call(uploadBean);
        }
      },
      child: Container(
        padding: const EdgeInsets.only(left: 10, right: 15),
        alignment: Alignment.center,
        child: Container(
          width: 28,
          height: 28,
          decoration: BoxDecoration(
              color:  (itemBean?.statusType?.isAlive() ?? true) ? Color(0xff1890ff)
                  : ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
              shape: BoxShape.circle),
          child: Center(
            child: Image.asset(
              "images/down_ico.png",
              width: 11,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  // Widget _toHasStatusWidget(BuildContext context) {
  //   return GestureDetector(
  //     behavior: HitTestBehavior.translucent,
  //     onTap: () {
  //       if(delflgUser(context)){
  //         return;
  //       }
  //       onChanged?.call(CheckInStatisticsChangeUploadBean(
  //           changeStatusType: itemBean?.statusType?.getReverseType() ??
  //               CheckInStatisticsChangeStatusType.Ignore));
  //     },
  //     child: Container(
  //       alignment: Alignment.center,
  //       padding: const EdgeInsets.only(left: 15, right: 15),
  //       child: CommonFixedHeightConfirmButtonWidget(
  //         isAlive: itemBean?.statusType?.isAlive() ?? true,
  //         width: 60,
  //         height: 28,
  //         unSelectBgColor:
  //         ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
  //         selectedBgColor:
  //         ThemeRepository.getInstance().getPrimaryColor_1890FF(),
  //         unSelectTextStyle: TextStyle(
  //           color: ThemeRepository.getInstance().getTextBFC2CC_BFC2CC(),
  //           fontSize: 13,
  //         ),
  //         selectedTextStyle: TextStyle(
  //           color: Colors.white,
  //           fontSize: 13,
  //         ),
  //         text: itemBean?.statusType?.getTypeToStr() ?? "忽略",
  //       ),
  //     ),
  //   );
  // }

  bool delflgUser(BuildContext context){
    if(itemBean?.delflg == "01"){
      VgToastUtils.toast(context, "用户已删除");
      return true;
    }else{
      return false;
    }
  }
}

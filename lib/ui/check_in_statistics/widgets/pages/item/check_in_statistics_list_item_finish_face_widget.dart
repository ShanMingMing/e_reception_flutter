
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_constraint_box_widget/common_constraint_max_width_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/attend_record_calendar_page.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/even/attend_record_calendar_refresh_even.dart';
import 'package:e_reception_flutter/ui/attend_record/attend_record_calendar/widgets/modify_personal_clock_record_widget.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_index/check_in_record_index_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/matching_mark_list_page.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/mark_info_bean.dart';
import 'package:e_reception_flutter/ui/person_detail/person_detail_page.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_string_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// 已刷脸列表项组件显示
///
/// @author: zengxiangxi
/// @createTime: 3/25/21 4:02 PM
/// @specialDemand:
class CheckInStatisticsListItemFinishFaceWidget extends StatelessWidget {
  final CheckInStatisticsListItemBean itemBean;

  const CheckInStatisticsListItemFinishFaceWidget({Key key, this.itemBean}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () async{
        if (itemBean == null) {
          return;
        }
        if(delflgUser(context)){
          return;
        }
        // if (itemBean.isUnknow()) {
        //   bool result = await MatchingMarkListPage.navigatorPush(
        //       context,
        //       MarkInfoBean(
        //         picurl: itemBean?.napicurl,
        //         fid: itemBean?.fid,
        //       ));
        //   if (result ?? false) {
        //     CheckInRecordIndexPageState.of(context)
        //         .multiRefreshStreamController
        //         ?.add(null);
        //   }
        //   return;
        // }
        // PersonDetailPage.navigatorPush(context, itemBean?.signFuid);
        //单人整月出勤数据
        // AttendRecordCalendarPage.navigatorPush(context,itemBean?.fuid);
        //单人整日数据列表
        ModifyPersonalClockRecordWidget.navigatorPush(
            context,
            // smallCardValue,
            // smallStatusCardColor,
            getCurrentMonthTimeStramp(),
            itemBean?.fuid,
            DateTime?.parse(DateUtil.getDateTimeByMs(itemBean?.firsttime*1000)?.toString()??DateTime.now().toString()),
                () {
              VgEventBus.global.send(AttendRecordCalendarRefreshEven());
            });
      },
      child: Container(
        height: 60,
        child: _toMainRowWidget(context),
      ),
    );
  }

  int getCurrentMonthTimeStramp() {
    DateTime _dateTime = DateTime?.parse(DateUtil.getDateTimeByMs(itemBean?.firsttime*1000)?.toString() ?? DateTime.now().toString());
    if (_dateTime == null) {
      return null;
    }
    return (DateTime(_dateTime.year, _dateTime.month, _dateTime.day)
        .millisecondsSinceEpoch /
        1000)
        .floor();
  }

  Widget _toMainRowWidget(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(width: 15),
        _toHeadPicWidget(context),
        SizedBox(width: 9),
        Expanded(child: _toColumnWidget()),
        SizedBox(width: 15),
      ],
    );
  }

  Widget _toHeadPicWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        // VgPhotoPreview.single(
        //     context,
        //     showOriginEmptyStr(itemBean?.putpicurl) ??
        //         showOriginEmptyStr(itemBean?.napicurl ?? "") ??
        //         ( ""),
        //     loadingImageQualityType: ImageQualityType.middleDown);
        if(delflgUser(context)){
          return;
        }
        PersonDetailPage.navigatorPush(context, itemBean?.fuid,pages: "CheckInStatisticsPage");
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: VgCacheNetWorkImage(
          showOriginEmptyStr(itemBean?.putpicurl) ??
              (showOriginEmptyStr(itemBean?.napicurl) ?? ""),
          width: 37,
          height: 37,
          imageQualityType: ImageQualityType.middleUp,
          defaultPlaceType: ImagePlaceType.head,
          defaultErrorType: ImageErrorType.head,
        ),
      ),
    );
  }

  ///是否显示最大时间
   bool isShowMaxTime(int minTime, int maxTime) {
    if (minTime == null || minTime <= 0 || maxTime == null || maxTime <= 0) {
      return false;
    }
     int time = (maxTime - minTime).abs();
    if(minTime == maxTime || time<60){
      return false;
    }
    if (minTime > maxTime) {
      //最小与最大对调位置
      int time;
      time = itemBean.firsttime;
      itemBean.firsttime = itemBean?.lasttime;
      itemBean?.lasttime = time;
      return true;
    }
    return true;
  }

  Widget _toColumnWidget() {
    final bool isShowMax = isShowMaxTime(itemBean.firsttime, itemBean?.lasttime);
    String temperature = "0";
    if(itemBean?.temperature!=""&&itemBean?.temperature!=null){
      if(itemBean?.temperature=="0.0"){
        temperature = "0";
      }else{
        temperature = itemBean?.temperature;
      }
    }
    return Container(
      height: 37,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CommonConstraintMaxWidthWidget(
                maxWidth: VgToolUtils.getScreenWidth() / 3,
                child: Text(
                  itemBean?.name ?? "-",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance()
                          .getTextMainColor_D0E0F7(),
                      fontSize: 15,
                      height: 1.2),
                ),
              ),
              Spacer(),
              CommonConstraintMaxWidthWidget(
                maxWidth: VgToolUtils.getScreenWidth() /2,
                child: Text(
                  VgStringUtils.getSplitStr([
                    itemBean?.department?.trim(),
                    // itemBean?.projectname?.trim(),
                    // itemBean?.city?.trim(),
                    itemBean?.classname?.trim(),
                    // itemBean?.coursename?.trim(),
                  ]),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 12,
                  ),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                width: 96,
                alignment: Alignment.centerLeft,
                child: Text.rich(
                  TextSpan(children: [
                    TextSpan(text: "${VgToolUtils.getHourAndMinuteStr(itemBean?.firsttime)??"-"}"),
                    TextSpan(text: isShowMax ? " 至 " : "",style: TextStyle()),
                    TextSpan(text: isShowMax ? "${VgToolUtils.getHourAndMinuteStr(itemBean?.lasttime)??"-"}" : ""),
                  ]),
                  style: TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 12),
                ),
              ),
              if(isShowMax)
              Text(
                "${VgToolUtils.getDifferenceTimeStr(itemBean?.firsttime, itemBean?.lasttime) ?? ""}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: VgColors.INPUT_BG_COLOR, fontSize: 12),
              ),
              Spacer(),
              Text(itemBean?.temperature!=null&&itemBean?.temperature!=""&&itemBean?.temperature!="0.0" ?"${_temperatureStr(itemBean) ?? ''}℃" :
                "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color:(double?.parse(temperature)??0)>37.2 ? ThemeRepository.getInstance().getMinorRedColor_F95355() : VgColors.INPUT_BG_COLOR, fontSize: 12),
              )
            ],
          )
        ],
      ),
    );
  }

  String _temperatureStr(CheckInStatisticsListItemBean itemBean){
    if((itemBean?.temperature?.length??0) > 4){
      return itemBean?.temperature?.substring(0,4);
    }else{
      return itemBean?.temperature;
    }
  }

  bool delflgUser(BuildContext context){
    if(itemBean?.delflg == "01"){
      VgToastUtils.toast(context, "用户已删除");
      return true;
    }else{
      return false;
    }
  }
}

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_dialog_value_bean.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';

import '../../app_main.dart';

class SelectedDepartmentViewModel extends BaseViewModel {
  ValueNotifier<DepartmentAndClassDataBean> infoValueNotifier;

  SelectedDepartmentViewModel(BaseState<StatefulWidget> state) : super(state){
    infoValueNotifier = ValueNotifier(null);
  }

  @override
  void onDisposed() {
    infoValueNotifier.dispose();
    super.onDisposed();
  }


  void getDayFaceRecord() {
    VgHttpUtils.get(ServerApi.BASE_URL + "app/appDayFaceSearchInfo",
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
        },
        callback: BaseCallback(
            onSuccess: (val) {
              CheckDialogValueBean responseBean =
              CheckDialogValueBean.fromMap(val);
              infoValueNotifier?.value = responseBean?.data;
            },
            onError: (msg) {
              VgToastUtils.toast(AppMain.context, "网络连接不可用");
            }));
  }
}

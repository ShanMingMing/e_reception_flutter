// import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
// import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
// import 'package:flutter/material.dart';
// import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
//
// /// 打卡记录-tabbar
// ///
// /// @author: zengxiangxi
// /// @createTime: 3/25/21 11:39 AM
// /// @specialDemand:
// class CheckInStatisticsTabBarWidget extends StatelessWidget {
//
//   CheckInStatisticsPageState state;
//
//   @override
//   Widget build(BuildContext context) {
//     state = CheckInStatisticsPageState.of(context);
//     return Container(
//       height: 40,
//       alignment: Alignment.centerLeft,
//       margin: const EdgeInsets.only(bottom: 5),
//       child: ScrollConfiguration(
//           behavior: MyBehavior(),
//           child: Theme(
//               data: ThemeData(
//                   highlightColor: Colors.transparent,
//                   splashColor: Colors.transparent),
//               child: _toTabBarWidget())),
//     );
//   }
//
//   Widget _toTabBarWidget() {
//     return TabBar(
//       controller: state?.tabController,
//       isScrollable: true,
//       indicatorSize: TabBarIndicatorSize.label,
//       unselectedLabelStyle: TextStyle(
//           color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//           fontSize: 15,
//           height: 1.2),
//       labelStyle: TextStyle(
//           color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
//           fontSize: 15,
//           fontWeight: FontWeight.w600,
//           height: 1.2),
//       indicatorColor: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
//       indicatorPadding: EdgeInsets.only(
//         left: 10,
//         right: 24,
//       ),
//       labelPadding: EdgeInsets.only(left: 15, right: 0),
//       tabs: List.generate(state?.groupInfoDataBean?.groupInfo?.length ?? 0, (index) {
//         return Padding(
//           padding: const EdgeInsets.only(top: 10, bottom: 5),
//           child: Row(
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Text(state?.groupInfoDataBean?.groupInfo?.elementAt(index)?.groupName ?? ""),
//               SizedBox(
//                 width: 3,
//               ),
//               Container(
//                 width: 24,
//                 child: Text(
//                   "${state?.groupInfoDataBean?.groupInfo?.elementAt(index)?.cnt ?? 0}",
//                   maxLines: 1,
//                   overflow: TextOverflow.ellipsis,
//                   style: TextStyle(
//                     fontSize: 12,
//                   ),
//                 ),
//               )
//             ],
//           ),
//         );
//       }),
//     );
//   }
// }

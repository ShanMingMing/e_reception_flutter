import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_sort_type.dart';
import 'package:e_reception_flutter/ui/company/company_detail/widgets/company_more_pop_window.dart';
import 'package:flutter/cupertino.dart';
typedef OnClick=Function(CheckInStatisticsSortType type);
class CheckInSortPopWindow extends StatelessWidget {
  final CheckInStatisticsSortType sortType;
  final OnClick onClick;
  const CheckInSortPopWindow({Key key, this.sortType,this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 2,
        ),
        Stack(
          children: <Widget>[
            Container(
              width: 120,
              height: 6,
              padding: EdgeInsets.only(right: 27),
              child: Align(
                alignment: Alignment.centerRight,
                child: Image.asset(
                  'images/sanjiao.png',
                  width: 11,
                  height: 6,
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
              color: Color(0xff303546)),
          child: Column(
            children: [_itemWidget(context,CheckInStatisticsSortType.earliest),
              _itemWidget(context,CheckInStatisticsSortType.latest),
              _itemWidget(context,CheckInStatisticsSortType.duration)],
          ),
          width: 120,
        ),
      ],
    );
  }

  _itemWidget(BuildContext context,CheckInStatisticsSortType type) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: (){
          onClick(type);
          Navigator.pop(context);
        },
        child: Container(
          height: 45,
          child: Center(
            child: Text(
              parseSortType(type),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: type == sortType
                    ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    : Color(0xFFD0E0F7),
                fontSize: 14,
              ),
            ),
          ),
        ));
  }
}

import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/record_history/check_in_rec_history_page.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_group_info_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 打卡记录-更多按钮
///
/// @author: zengxiangxi
/// @createTime: 3/26/21 10:59 AM
/// @specialDemand:
class CheckInStatisticsMoreButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _moreRecordsBar(context);
  }

  ///更多记录
  Widget _moreRecordsBar(BuildContext context) {
    final CheckInStatisticsGroupInfoDataBean groupInfoBean =
        CheckInStatisticsPageState.of(context)?.groupInfoDataBean;
    // if (groupInfoBean != null && groupInfoBean.groupInfo != null) {
    //   int count = 0;
      // groupInfoBean.groupInfo.forEach((element) {
      //   count += element.cnt ?? 0;
      // });
      // if (count == 0) {
      //   return SizedBox();
      // }
      return GestureDetector(
        child: Container(
          height: 50 + ScreenUtils.getBottomBarH(context),
          padding: EdgeInsets.only(
              right: 15, left: 15, bottom: ScreenUtils.getBottomBarH(context)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('images/lishi.png', width: 18),
              SizedBox(
                width: 8,
              ),
              Text(
                '更多记录',
                style: TextStyle(fontSize: 17, height:1.2,color: Colors.white),
              ),
              Spacer(),
              Image.asset(
                "images/index_arrow_ico.png",
                width: 6,
                color: Colors.white,
              )
            ],
          ),
          decoration: BoxDecoration(
              color: Color(0xFF363c4c)),
        ),
        onTap: () {
          CheckInRecHistoryPage.navigatorPush(context);
        },
      );
    // } else {
    //   return SizedBox();
    // }
  }
}

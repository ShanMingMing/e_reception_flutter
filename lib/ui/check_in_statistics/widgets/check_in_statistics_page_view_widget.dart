// import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_group_info_response_bean.dart';
// import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
// import 'package:e_reception_flutter/ui/check_in_statistics/widgets/pages/check_in_statistics_list_widget.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';
//
// /// 打卡记录-pageview
// ///
// /// @author: zengxiangxi
// /// @createTime: 3/25/21 2:18 PM
// /// @specialDemand: 根据分组列表展示的TabBarView
// class CheckInStatisticsPageViewWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final CheckInStatisticsPageState state = CheckInStatisticsPageState.of(context);
//     return ScrollConfiguration(
//       behavior: MyBehavior(),
//       child: TabBarView(
//         controller: state?.tabController,
//         physics: BouncingScrollPhysics(),
//         children: List.generate(state?.groupInfoDataBean?.groupInfo?.length ?? 0, (index){
//           return _toListWidget(state?.groupInfoDataBean?.groupInfo?.elementAt(index));
//         }),
//       ),
//     );
//   }
//
//   Widget _toListWidget(GroupInfoBean groupItemBean) {
//     return CheckInStatisticsListWidget(groupItemBean: groupItemBean,);
//   }
// }

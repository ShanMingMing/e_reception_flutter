import 'package:e_reception_flutter/ui/check_in_statistics/bean/check_in_statistics_filter_type.dart';
import 'package:e_reception_flutter/ui/check_in_statistics/check_in_statistics_page.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'pages/check_in_statistics_list_widget.dart';

/// 打卡记录-根据已刷脸和未刷脸的PageView
///
/// @author: zengxiangxi
/// @createTime: 3/29/21 9:30 AM
/// @specialDemand:
class CheckInStatisticsTwoPageViewWidget extends StatelessWidget {
  CheckInStatisticsTwoPageViewWidget({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final CheckInStatisticsPageState state =
        CheckInStatisticsPageState.of(context);
    return PreloadPageView(
      controller: state?.pageController,
      preloadPagesCount: 2,
      children: <Widget>[
        CheckInStatisticsListWidget(
            // key: UniqueKey(),
            filterType: CheckInStatisticsFilterType.finishFace),
        CheckInStatisticsListWidget(
            // key: UniqueKey(),
            filterType: CheckInStatisticsFilterType.unfinishFace),
      ],
    );
  }
}

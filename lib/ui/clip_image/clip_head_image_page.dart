import 'dart:io';

import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/material.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_screen_lib.dart';
import 'package:image_editor/image_editor.dart';


/// 裁剪头像图片
///
/// @author: zengxiangxi
/// @createTime: 1/26/21 1:59 PM
/// @specialDemand:
class ClipHeadImagePage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "ClipHeadImagePage";

  const ClipHeadImagePage({Key key, this.localPath, this.aspectRadio}) : super(key: key);

  @override
  _ClipHeadImagePageState createState() => _ClipHeadImagePageState();

  final String localPath;

  final double aspectRadio;

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,String localPath,double aspectRadio ) {
    if(localPath == null || localPath.isEmpty){
      return null;
    }
    if(aspectRadio == null){
      return null;
    }
    print("打印本地图片路径：${localPath}");
    return RouterUtils.routeForFutureResult(
      context,
      ClipHeadImagePage(
        localPath: localPath,
        aspectRadio: aspectRadio,
      ),
      routeName: ClipHeadImagePage.ROUTER,
    );
  }
}

class _ClipHeadImagePageState extends State<ClipHeadImagePage> {
  GlobalKey<ExtendedImageEditorState> editorKey;

  @override
  void initState() {
    super.initState();
    editorKey = GlobalKey<ExtendedImageEditorState>();
  }

  @override
  void dispose() {
    editorKey = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        child: Stack(
          children: <Widget>[
            ExtendedImage.file(
              File(widget?.localPath),
              fit: BoxFit.contain,
              extendedImageEditorKey: editorKey,
              mode: ExtendedImageMode.editor,
              initEditorConfigHandler: (state) {
                return
                    EditorConfig(
                        maxScale: 8.0,
                        cropRectPadding:
                            EdgeInsets.all(20),
                        hitTestSize: 20.0,
                        lineColor:Colors.white,
                        cornerSize: Size(0, 0),
                        lineHeight:2,
                        cropAspectRatio: widget?.aspectRadio,
                        editorMaskColorHandler: (context, bo) {
                          return Color(0x80000000);
                        });
              },
            ),
            Positioned(
              bottom: ScreenUtils.getBottomBarH(context),
              left: 0,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () => Navigator.of(context).pop(),
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child:
                      Text(
                        "取消",
                        style: TextStyle(color: Colors.white, fontSize: 19),
                      ),
                ),
              ),
            ),
            Positioned(
              bottom: ScreenUtils.getBottomBarH(context),
              right: 0,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  managerClipData();
                },
                child: Container(
                  padding: const EdgeInsets.all(20),
                  child:
                      Text(
                        "裁剪",
                        style: TextStyle(color: Colors.white, fontSize: 19),
                      ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

  ///处理裁剪数据
  void managerClipData() async {
    final ExtendedImageEditorState state = editorKey?.currentState;
    if (state == null) {
      print("裁剪失败");
      return;
    }
    final EditActionDetails editAction = state.editAction;

    final Rect cropRect = state.getCropRect();
    final img = state.rawImageData;
    final rotateAngle = editAction.rotateAngle.toInt();
    final flipHorizontal = editAction.flipY;
    final flipVertical = editAction.flipX;
    ImageEditorOption option = ImageEditorOption();

    if (editAction.needCrop) {
      option.addOption(ClipOption.fromRect(cropRect));
    }

    if (editAction.needFlip) {
      option.addOption(
          FlipOption(horizontal: flipHorizontal, vertical: flipVertical));
    }

    if (editAction.hasRotateAngle) {
      option.addOption(RotateOption(rotateAngle));
    }
    final result = await ImageEditor.editImageAndGetFile(
      image: img,
      imageEditorOption: option,
    );
    print("zxx:-----裁剪图片路径：${result?.path}");
    Navigator.of(context).pop(result?.path);
  }
}

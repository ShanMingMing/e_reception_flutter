import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mark/choose_group/bean/choose_group_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/choose_group/choose_group_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'widgets/choose_group_list_widget.dart';

/// 选择分组页面
///
/// @author: zengxiangxi
/// @createTime: 1/19/21 8:29 AM
/// @specialDemand:
class ChooseGroupPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "ChooseGroupPage";

  ///选中Id
  final String selectedId;

  const ChooseGroupPage({Key key, this.selectedId}) : super(key: key);

  @override
  _ChooseGroupPageState createState() => _ChooseGroupPageState();

  ///跳转方法
  static Future<EditTextAndValue> navigatorPush(
      BuildContext context, String selectedId) {
    return RouterUtils.routeForFutureResult<EditTextAndValue>(
      context,
      ChooseGroupPage(
        selectedId: selectedId,
      ),
      routeName: ChooseGroupPage.ROUTER,
    );
  }
}

class _ChooseGroupPageState
    extends BasePagerState<ChooseGroupListItemBean, ChooseGroupPage>
    with VgPlaceHolderStatusMixin {
  ChooseGroupViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = ChooseGroupViewModel(this);
    _viewModel?.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: MixinPlaceHolderStatusWidget(
            child: _toPullRefreshWidget(),
            emptyOnClick: () => _viewModel?.refresh(),
            errorOnClick: () => _viewModel?.refresh(),
          ),
        )
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "选择分组",
      isShowBack: true,
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
    );
  }

  Widget _toPullRefreshWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: VgPullRefreshWidget.bind(
          state: this,
          viewModel: _viewModel,
          enablePullDown: false,
          enablePullUp: false,
          child: ChooseGroupListWidget(
            list: data,
            selectedId: widget?.selectedId,
          )),
    );
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mark/choose_group/bean/choose_group_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// 选择分组-列表
///
/// @author: zengxiangxi
/// @createTime: 1/19/21 8:33 AM
/// @specialDemand:
class ChooseGroupListWidget extends StatelessWidget {
  final List<ChooseGroupListItemBean> list;

  final String selectedId;

  const ChooseGroupListWidget({Key key, this.list, this.selectedId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _toListWidget();
  }

  Widget _toListWidget() {
    return ListView.separated(
        itemCount: list?.length ?? 0,
        padding: const EdgeInsets.all(0),
        physics: NeverScrollableScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(context, list?.elementAt(index));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            color: Color(0xFF303546),
            height: 0.5,
            margin: const EdgeInsets.only(left: 15, right: 1),
          );
        });
  }

  Widget _toListItemWidget(
      BuildContext context, ChooseGroupListItemBean itemBean) {
    if(itemBean == null){
      return Container();
    }
    final bool isSelected =
        selectedId != null && selectedId == itemBean?.groupid;
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        RouterUtils.pop(context,
            result: EditTextAndValue(
                editText: itemBean?.groupName, value: itemBean?.groupid));
      },
      child: Container(
        height: 50,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            AnimatedDefaultTextStyle(
              duration: DEFAULT_ANIM_DURATION,
              style: TextStyle(
                color: isSelected
                    ? ThemeRepository.getInstance().getPrimaryColor_1890FF()
                    : ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
                fontSize: 14,
              ),
              child: Text(
                itemBean?.groupName ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Spacer(),
            Offstage(
              offstage: false,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        itemBean.isNeedRecord() ? "需记录打卡" : "无需记录打卡",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: VgColors.INPUT_BG_COLOR,
                          fontSize: 12,
                        ),
                      ),
                      // SizedBox(
                      //   width: 3,
                      // ),
                      // Transform.scale(
                      //   scale: 2,
                      //   child: Icon(
                      //     Icons.arrow_drop_down,
                      //     size: 8,
                      //     color: VgColors.INPUT_BG_COLOR,
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

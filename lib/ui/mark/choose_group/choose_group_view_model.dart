import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_total_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/choose_group/bean/choose_group_response_bean.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class ChooseGroupViewModel extends BasePagerViewModel<ChooseGroupListItemBean,ChooseGroupResponseBean>{
  ChooseGroupViewModel(BaseState<StatefulWidget> state) : super(state);

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "companyid": UserRepository.getInstance().companyId ?? "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appFindSignGroupInfo";
  }

  @override
  ChooseGroupResponseBean parseData(VgHttpResponse resp) {
    ChooseGroupResponseBean vo = ChooseGroupResponseBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }


}
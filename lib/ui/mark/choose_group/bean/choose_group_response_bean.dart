import 'package:vg_base/vg_pull_to_refresh_lib.dart';

/// success : true
/// code : "200"
/// msg : "处理成功"
/// data : {"groupInfo":[{"groupName":"员工","groupid":"11e2bec33bf743aaae4522b51c022f32"},{"groupName":"访客","groupid":"377750b3c2f14647b1be20bd3cad3ff9"}]}
/// extra : null

class ChooseGroupResponseBean extends BasePagerBean<ChooseGroupListItemBean>{
  bool success;
  String code;
  String msg;
  DataBean data;
  dynamic extra;

  static ChooseGroupResponseBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ChooseGroupResponseBean chooseGroupResponseBeanBean = ChooseGroupResponseBean();
    chooseGroupResponseBeanBean.success = map['success'];
    chooseGroupResponseBeanBean.code = map['code'];
    chooseGroupResponseBeanBean.msg = map['msg'];
    chooseGroupResponseBeanBean.data = DataBean.fromMap(map['data']);
    chooseGroupResponseBeanBean.extra = map['extra'];
    return chooseGroupResponseBeanBean;
  }

  Map toJson() => {
    "success": success,
    "code": code,
    "msg": msg,
    "data": data,
    "extra": extra,
  };

  ///当前页
  @override
  int getCurrentPage() => 1;

  ///得到List列表
  @override
  List<ChooseGroupListItemBean> getDataList() => data?.groupInfo;

  ///最后页码
  @override
  int getMaxPage() => 1;

  ///消息
  @override
  String getMessage() => msg ?? "";

  ///是否成功
  @override
  bool isSucceed() =>
      success != null && success && code != null && code == "200";

  ///多页请求页码赋值
  @override
  void setMultiPageDataList(List<dynamic> list) {
    super.setMultiPageDataList(list);
    data?.groupInfo = list.cast();
  }
}

/// groupInfo : [{"groupName":"员工","groupid":"11e2bec33bf743aaae4522b51c022f32"},{"groupName":"访客","groupid":"377750b3c2f14647b1be20bd3cad3ff9"}]

class DataBean {
  List<ChooseGroupListItemBean> groupInfo;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.groupInfo = List()..addAll(
      (map['groupInfo'] as List ?? []).map((o) => ChooseGroupListItemBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "groupInfo": groupInfo,
  };
}

/// groupName : "员工"
/// groupid : "11e2bec33bf743aaae4522b51c022f32"

class ChooseGroupListItemBean {
  String groupName;
  String groupid;
  ///是否是访客 00是 01不是
  String visitor;

  ///`recorded` '记录打卡 00记录 01不记录 02显示但不记录',
  ///00、02: 需记录打卡
  ///01：无需记录打卡
  String recorded;

  static ChooseGroupListItemBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ChooseGroupListItemBean groupInfoBean = ChooseGroupListItemBean();
    groupInfoBean.groupName = map['groupName'];
    groupInfoBean.groupid = map['groupid'];
    groupInfoBean.recorded = map['recorded'];
    groupInfoBean.visitor = map['visitor'];
    return groupInfoBean;
  }

  Map toJson() => {
    "groupName": groupName,
    "groupid": groupid,
    "visitor": visitor,
    "recorded": recorded,
  };

  ///是否需要记录
  ///`recorded` '记录打卡 00记录 01不记录',
  bool isNeedRecord(){
    return recorded == "00" || recorded == "02";
  }
}
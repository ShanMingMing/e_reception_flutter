import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_confirm_cancel_dialog/common_confirm_cancel_dialog.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/unmarked/check_in_record_brower_for_unmarked_page.dart';
import 'package:e_reception_flutter/ui/mark/merge_mark/merge_mark_view_model.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

import 'widgets/merge_mark_list_widget.dart';

/// 合入已标记
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 9:35 AM
/// @specialDemand:
class MergeMarkPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "MergeMarkPage";

  final CheckInRecordBrowerForTypeListItemBean unMarkedInfoBean;

  const MergeMarkPage({Key key, this.unMarkedInfoBean}) : super(key: key);

  @override
  _MergeMarkPageState createState() => _MergeMarkPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,
      CheckInRecordBrowerForTypeListItemBean unMarkedInfoBean) {
    VgToolUtils.removeAllFocus(context);
    return RouterUtils.routeForFutureResult(
      context,
      MergeMarkPage(
        unMarkedInfoBean: unMarkedInfoBean,
      ),
      routeName: MergeMarkPage.ROUTER,
    );
  }
}

class _MergeMarkPageState extends BasePagerState<
    CheckInRecordBrowerForTypeListItemBean,
    MergeMarkPage> with VgPlaceHolderStatusMixin {
  MergrMarkViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _viewModel = MergrMarkViewModel(this);
    _viewModel?.refresh();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        SizedBox(
          height: 12,
        ),
        CommonSearchBarWidget(
          hintText: "搜索名字",
        ),
        _toTitleBarWidget(),
        Expanded(
          child: MixinPlaceHolderStatusWidget(
              errorOnClick: () => _viewModel?.refresh(),
              emptyOnClick: () => _viewModel?.refresh(),
              child: _toPullToRefreshWidget()),
        )
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "合入已标记",
      isShowBack: true,
    );
  }

  Widget _toTitleBarWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(top: 12, bottom: 4, left: 15, right: 15),
      child: Text(
        "请选择已标记用户以合并：",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 12,
        ),
      ),
    );
  }

  Widget _toPullToRefreshWidget() {
    return VgPullRefreshWidget.bind(
        viewModel: _viewModel,
        state: this,
        child: MergeMarkListWidget(
          list: data,
          onTap: (CheckInRecordBrowerForTypeListItemBean itemBean) {
            if (itemBean == null) {
              return;
            }
            if (widget?.unMarkedInfoBean == null) {
              VgToastUtils.toast(context, "被合并人获取异常");
              return;
            }
            _toConfirmDialog(itemBean);
          },
        ));
  }

  void _toConfirmDialog(CheckInRecordBrowerForTypeListItemBean itemBean) async {
    bool result = await CommonConfirmCancelDialog.navigatorPushDialog(context,
        title: "提示", content: "确认与${itemBean?.name ?? ""}合并标记？");
    if (result ?? false) {
      _viewModel?.mergeFaceHttp(context, itemBean, widget?.unMarkedInfoBean);
    }
  }

  @override
  void setListData(List<CheckInRecordBrowerForTypeListItemBean> list) {
    // if(list != null && list.isNotEmpty){
    //   list = list.toSet().toSet()?.toList();
    // }
    super.setListData(list);
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/bean/check_in_record_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_base/vg_time_util_lib.dart';

/// 合入已标记-列表
///
/// @author: zengxiangxi
/// @createTime: 1/21/21 9:42 AM
/// @specialDemand:
class MergeMarkListWidget extends StatelessWidget {

  final List<CheckInRecordBrowerForTypeListItemBean> list;

  final ValueChanged<CheckInRecordBrowerForTypeListItemBean> onTap;

  const MergeMarkListWidget({Key key, this.list, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return _toListWidget();
  }

  Widget _toListWidget() {
    return ListView.separated(
        itemCount: list?.length ?? 0,
        padding: const EdgeInsets.all(0),
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemBuilder: (BuildContext context, int index) {
          return _toListItemWidget(context,list?.elementAt(index));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            color: Color(0xFF303546),
            height: 0.5,
            margin: const EdgeInsets.only(left: 15, right: 1),
          );
        });
  }
  Widget _toListItemWidget(BuildContext context, CheckInRecordBrowerForTypeListItemBean itemBean) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(onTap != null){
          onTap(itemBean);
        }
      },
      child: Container(
        height: 60,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: Container(
                width: 36,
                height: 36,
                child: VgCacheNetWorkImage(
                  itemBean?.napicurl ?? "",
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middleUp,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 36,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: [
                        Text(
                          itemBean?.name ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: ThemeRepository.getInstance()
                                  .getTextMainColor_D0E0F7(),
                              fontSize: 15,
                              height: 1.2),
                        ),
                        // Text(
                        //   "/",
                        //   maxLines: 1,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(
                        //       color: ThemeRepository.getInstance()
                        //           .getTextMainColor_D0E0F7(),
                        //       fontSize: 15,
                        //       height: 1.2),
                        // ),
                        // Text(
                        //   "Sharon",
                        //   maxLines: 1,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(
                        //       color: ThemeRepository.getInstance()
                        //           .getTextMainColor_D0E0F7(),
                        //       fontSize: 12,
                        //       height: 1.2),
                        // ),
                        Spacer(),
                        Text(
                          _getGroupAndIdStr(itemBean?.groupName,itemBean?.number) ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 12,
                              height: 1.2),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          "${VgToolUtils.getTimestrampStr(itemBean?.punchTime) ??""}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 12,
                              height: 1.2),
                        ),
                        Spacer(),
                        Text(
                          StringUtils.isEmpty(itemBean?.punchName) ? "":"${itemBean.punchName}标记",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: VgColors.INPUT_BG_COLOR,
                              fontSize: 10,
                              height: 1.2),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 15,
            )
          ],
        ),
      ),
    );
  }
  int _transTimeStamp(int time){
    if(time == null){
      return null;
    }
    String str = time?.toString();
    if(str.length ==10){
      return time*1000;
    }
    if(str.length != 13){
      return null;
    }
    return time;
  }


  String _getGroupAndIdStr(String groupName,String number){
    if(StringUtils.isEmpty(groupName) && StringUtils.isEmpty(number)){
      return null;
    }
    if(StringUtils.isNotEmpty(groupName) && StringUtils.isEmpty(number)){
      return groupName;
    }
    if(StringUtils.isEmpty(groupName) && StringUtils.isNotEmpty(number)){
      return number;
    }
    return groupName+ "·" + number;
  }


}


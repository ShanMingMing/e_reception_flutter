import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_refresh_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_total_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class MergrMarkViewModel extends BasePagerViewModel<
    CheckInRecordBrowerForTypeListItemBean,
    CheckInRecordBrowerForTypeResponseBean> {
  ///合入已标记刷脸用户
  static const String MERGE_FACR_API =
     ServerApi.BASE_URL + "app/appMergeSignFaceUser";

  MergrMarkViewModel(BaseState<StatefulWidget> state) : super(state);

  ///获取顶部信息请求
  void mergeFaceHttp(
      BuildContext context, CheckInRecordBrowerForTypeListItemBean markedItemBean, CheckInRecordBrowerForTypeListItemBean unMarkedInfoBean) {
    if (markedItemBean == null ||
        StringUtils.isEmpty(markedItemBean?.fid) ||
        StringUtils.isEmpty(markedItemBean?.groupid) ||
        StringUtils.isEmpty(markedItemBean?.signFuid)) {
      VgToastUtils.toast(context, '合入标记人获取异常');
      return;
    }
    if(unMarkedInfoBean == null ){
      VgToastUtils.toast(context, "被合并人获取异常");
      return;
    }
    VgHttpUtils.post(MERGE_FACR_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fid": unMarkedInfoBean?.fid ?? "",
          "groupid": markedItemBean?.groupid ?? "",
          "signFuid": markedItemBean?.signFuid ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          RouterUtils.pop(context,result: true);
          Future.delayed(Duration(milliseconds: 800),(){
            VgToastUtils.toast(AppMain.context, "合入已标记成功");
          });
        }, onError: (msg) {
          VgToastUtils.toast(context, msg);
        }));
  }

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "companyid": UserRepository.getInstance().companyId ?? "",
      "current": page ?? 1,
      "sign": "01",
      "size": 20,
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appFaceSignList";
  }

  @override
  CheckInRecordBrowerForTypeResponseBean parseData(VgHttpResponse resp) {
    CheckInRecordBrowerForTypeResponseBean vo =
        CheckInRecordBrowerForTypeResponseBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }
}

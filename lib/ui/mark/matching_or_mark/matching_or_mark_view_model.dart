import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_or_mark_upload_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'bean/mark_info_bean.dart';

class MatchingOrMarkViewModel extends BaseViewModel {
  ///合入已标记刷脸用户
  static const String MERGE_FACR_API =
     ServerApi.BASE_URL + "app/appMergeSignFaceUser";
  MatchingOrMarkViewModel(BaseState<StatefulWidget> state) : super(state);

  ///获取顶部信息请求
  void mergeFaceHttp(
      BuildContext context, MatchingInfoBean matchingInfoBean, MarkInfoBean markInfoBean,MatchingOrMarkUploadBean uploadBean) {
    if (matchingInfoBean == null ||
        StringUtils.isEmpty(matchingInfoBean?.groupId) ||
        StringUtils.isEmpty(matchingInfoBean?.signFuid)) {
      VgToastUtils.toast(context, '合入标记人获取异常');
      return;
    }
    if(markInfoBean == null ){
      VgToastUtils.toast(context, "被合并人获取异常");
      return;
    }
    VgHttpUtils.post(MERGE_FACR_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fid": markInfoBean?.fid ?? "",
          "groupid": uploadBean?.groupId ?? "",
          "nick" : uploadBean?.nick ?? "",
          "number" : uploadBean?.id ?? "",
          "napicurl": (showOriginEmptyStr(matchingInfoBean?.picurl) ?? showOriginEmptyStr(markInfoBean?.picurl)) ?? "",
          "signFuid": matchingInfoBean?.signFuid ?? "",
          "phone": globalSubPhone(uploadBean?.phone) ?? "",
        },
        callback: BaseCallback(onSuccess: (val) {
          RouterUtils.pop(context,result: true);
          Future.delayed(Duration(milliseconds: 800),(){
            VgToastUtils.toast(AppMain.context, "合入已标记成功");
          });
        }, onError: (msg) {
          VgToastUtils.toast(context, msg);
        }));
  }
}



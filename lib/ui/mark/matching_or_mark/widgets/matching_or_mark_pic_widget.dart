import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 匹配/标记用户
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 6:05 PM
/// @specialDemand:
class MatchingOrMarkPicWidget extends StatelessWidget {

  final String newPicurl;

  final String oldPicurl;

  const MatchingOrMarkPicWidget({Key key, this.newPicurl, this.oldPicurl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: Row(children: <Widget>[
        Spacer(flex: 53,),
        _toNewPicWidget(context),
        Spacer(flex: 30,),
        _toOldPicWidget(context),
        Spacer(flex: 53,),
      ],),
    );
  }

  Widget _toNewPicWidget(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(StringUtils.isEmpty(newPicurl)){
          return;
        }
        VgPhotoPreview.single(context, newPicurl,loadingImageQualityType: ImageQualityType.middleDown);
      },
      child: Container(
        height: 145,
        width: 120,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "刷脸图片",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                fontSize: 12,
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child:  Container(
                width: 120,
                height: 120,
                child: VgCacheNetWorkImage(
                  newPicurl ?? "",
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middleDown,
                  errorWidget: _toEmptyWidget(),

                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  
  Widget _toOldPicWidget(BuildContext context){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if(StringUtils.isEmpty(oldPicurl)){
          return;
        }
        VgPhotoPreview.single(context, oldPicurl,loadingImageQualityType: ImageQualityType.middleDown);

      },
      child: Container(
        height: 145,
        width: 120,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
             Text(
                       "预存图片",
                       maxLines: 1,
                       overflow: TextOverflow.ellipsis,
                       style: TextStyle(
                           color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
                           fontSize: 12,
                           ),
                     ),
            ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child:  Container(
                width: 120,
                height: 120,
                child: VgCacheNetWorkImage(
                  oldPicurl ?? "",
                  fit: BoxFit.cover,
                  imageQualityType: ImageQualityType.middleDown,
                  errorWidget: _toEmptyWidget(),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  
  Widget _toEmptyWidget(){
    return Container(
      height: 120,
      color: Color(0xFF3A3F50),
      alignment: Alignment.center,
      child: Text(
                "暂无",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: VgColors.INPUT_BG_COLOR,
                    fontSize: 14,
                    ),
              ),
    );
  }
}

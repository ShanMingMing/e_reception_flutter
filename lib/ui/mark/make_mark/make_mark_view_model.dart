import 'dart:convert';

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/bean/com_face_user.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/bean/make_mark_upload_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';

class MakeMarkViewModel extends BaseViewModel {
  static const String _MAKE_MARK_FACE_API =
     ServerApi.BASE_URL + "app/appSignFaceUser";

  MakeMarkViewModel(BaseState<StatefulWidget> state) : super(state);

  ///标记请求
  void makeMarkHttp(
      BuildContext context, MakeMarkUploadBean uploadBean,CheckInRecordBrowerForTypeListItemBean infoBean) {
    if (uploadBean == null || !uploadBean.isAlive()) {
      return;
    }
    VgHudUtils.show(context, "正在提交");
    VgHttpUtils.post(_MAKE_MARK_FACE_API,
        params: {
          "authId" : UserRepository.getInstance().authId ?? "",
          "fid": infoBean?.fid ?? "",
        },
        body: jsonEncode(
          ComFaceUser(
            companyid: UserRepository.getInstance().companyId ?? "",
              groupid: uploadBean?.groupId ?? "",
            name: uploadBean?.name ?? "",
            napicurl: infoBean?.pictureUrl ?? "",
            nick: uploadBean?.nick ?? "",
            number: uploadBean?.id ?? "",
            phone: uploadBean?.phone ?? "",
            // createdate: 0,
            // delflg:"",
            //   createuid:"",
            //   fuid:"",

          )
        ),
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          RouterUtils.pop(context,result: true);
          VgToastUtils.toast(AppMain.context, "保存成功");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }
}



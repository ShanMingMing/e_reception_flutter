import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/common_title_edit_with_underline_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/even/check_refresh_even.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/bean/make_mark_upload_bean.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/make_mark_view_model.dart';
import 'package:e_reception_flutter/ui/mark/merge_mark/merge_mark_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_evnet_bus_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';

import 'widgets/make_mark_edits_widget.dart';

/// 最标记页面
///
/// @author: zengxiangxi
/// @createTime: 1/18/21 7:42 PM
/// @specialDemand:
class MakeMarkPage extends StatefulWidget {

 ///路由名称
  static const String ROUTER = "MakeMarkPage";

  final CheckInRecordBrowerForTypeListItemBean unMarkInfoBean;

  const MakeMarkPage({Key key, this.unMarkInfoBean}) : super(key: key);
  
  @override
  _MakeMarkPageState createState() => _MakeMarkPageState();
  
  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context,CheckInRecordBrowerForTypeListItemBean unMarkInfoBean){
    return RouterUtils.routeForFutureResult<bool>(context,
        MakeMarkPage(
          unMarkInfoBean: unMarkInfoBean,
        ),
        routeName: MakeMarkPage.ROUTER,);
  }
}

class _MakeMarkPageState extends BaseState<MakeMarkPage> {

  MakeMarkViewModel _viewModel;

  ValueNotifier<MakeMarkUploadBean> _editsValueNotifier;
  
  @override
  void initState() {
    super.initState();
    _viewModel = MakeMarkViewModel(this);
    _editsValueNotifier = ValueNotifier(null);
  }
  
  @override
  void dispose() {
    _editsValueNotifier?.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget(),isEnableVerical: false,),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toSingleScrollViewWidget(),
        )
      ],
    );
  }

  Widget _toSingleScrollViewWidget(){
    return ListView(
      physics: BouncingScrollPhysics(),
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 20,),
            _toBigPicWidget(),
            SizedBox(height: 20,),
            _toEditsWidget(),
            SizedBox(
              height: 30,
            ),
            _toConfirmButtonWidget(),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ],
    );
  }

  ///TopBar
  Widget _toTopBarWidget(){
    return VgTopBarWidget(
      title: "标记",
      isShowBack: true,
      rightWidget:  GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: ()async{
          bool result = await MergeMarkPage.navigatorPush(context,widget?.unMarkInfoBean);
          if(result ?? false){
            RouterUtils.pop(context,result: true);
          }
        },
        child: Text(
                  "合入已标记",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ThemeRepository.getInstance().getMinorYellowColor_FFB714(),
                      fontSize: 13,
                      ),
                ),
      ),
    );
  }

  Widget _toBigPicWidget(){
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        VgPhotoPreview.single(context, widget?.unMarkInfoBean?.pictureUrl,loadingImageQualityType: ImageQualityType.middleDown);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
            width: 160,
            height: 160,
            child: VgCacheNetWorkImage(
                widget?.unMarkInfoBean?.pictureUrl ?? "",
              imageQualityType: ImageQualityType.middleDown,
            )),
      ),
    );
  }

  Widget _toEditsWidget(){
    return MakeMarkEditsWidget(
      onChanged: _editsValueNotifier,
    );
  }

  Widget _toConfirmButtonWidget(){
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, MakeMarkUploadBean value,
          Widget child) {
        final bool isAlive = value?.isAlive() ?? false;
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: isAlive,
          height: 40,
          margin: const EdgeInsets.symmetric(horizontal: 25),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          text: "提交",
          onTap: (){
            String msg = value?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              _viewModel.makeMarkHttp(context,value,widget?.unMarkInfoBean);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}


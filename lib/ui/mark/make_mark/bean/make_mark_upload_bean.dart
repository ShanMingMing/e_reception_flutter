import 'package:vg_base/vg_string_util_lib.dart';

class MakeMarkUploadBean {
  String name;

  String phone;

  String id;

  String nick;

  String groupId;

  String groupName;

  MakeMarkUploadBean(
      {this.name,
      this.phone,
      this.id,
      this.nick,
      this.groupId,
      this.groupName});

  @override
  bool operator ==(Object other) => false;

  @override
  int get hashCode => super.hashCode;


  ///是否检查通过
  bool isAlive() {
    //demand  全称非必填
    if (StringUtils.isEmpty(name) || StringUtils.isEmpty(groupId)) {
      return false;
    }
    return true;
  }

  ///检查校验
  //demand  全称非必填
  String checkVerify(){
    if(StringUtils.isEmpty(name)){
      return "姓名不能为空";
    }
    if(StringUtils.isEmpty(groupId)){
      return "分组不能为空";
    }
    return null;
  }

  @override
  String toString() {
    return 'MakeMarkUploadBean{name: $name, phone: $phone, id: $id, nick: $nick, groupId: $groupId, groupName: $groupName}';
  }
}

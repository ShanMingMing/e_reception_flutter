/// companyid : ""
/// createdate : 0
/// createuid : ""
/// delflg : ""
/// fuid : ""
/// groupid : ""
/// name : ""
/// napicurl : ""
/// nick : ""
/// number : ""
/// phone : ""

class ComFaceUser {
  String companyid;
  // int createdate;
  // String createuid;
  // String delflg;
  // String fuid;
  String groupid;
  String name;
  String napicurl;
  String nick;
  String number;
  String phone;
  String roleid;


  ComFaceUser({
      this.companyid,
      // this.createdate,
      // this.createuid,
      // this.delflg,
      // this.fuid,
      this.groupid,
      this.name,
      this.napicurl,
      this.nick,
      this.number,
      this.phone, String roleid});

  static ComFaceUser fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ComFaceUser comFaceUserBean = ComFaceUser();
    comFaceUserBean.companyid = map['companyid'];
    // comFaceUserBean.createdate = map['createdate'];
    // comFaceUserBean.createuid = map['createuid'];
    // comFaceUserBean.delflg = map['delflg'];
    // comFaceUserBean.fuid = map['fuid'];
    comFaceUserBean.groupid = map['groupid'];
    comFaceUserBean.name = map['name'];
    comFaceUserBean.napicurl = map['napicurl'];
    comFaceUserBean.nick = map['nick'];
    comFaceUserBean.number = map['number'];
    comFaceUserBean.phone = map['phone'];
    return comFaceUserBean;
  }

  Map toJson() => {
    "companyid": companyid,
    // "createdate": createdate,
    // "createuid": createuid,
    // "delflg": delflg,
    // "fuid": fuid,
    "groupid": groupid,
    "name": name,
    "napicurl": napicurl,
    "nick": nick,
    "number": number,
    "phone": phone,
  };
}
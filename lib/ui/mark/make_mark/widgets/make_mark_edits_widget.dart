import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/common_title_edit_with_underline_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_title_edit_with_underline_widget/typeof/common_title_edit_with_underline_typeof.dart';
import 'package:e_reception_flutter/constants/constant.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mark/choose_group/choose_group_page.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/bean/make_mark_upload_bean.dart';
import 'package:e_reception_flutter/ui/new_terminal/new_terminal_list/new_terminal_list_page.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

/// 标记编辑组件
///
/// @author: zengxiangxi
/// @createTime: 1/18/21 10:57 PM
/// @specialDemand:
class MakeMarkEditsWidget extends StatelessWidget {
  final ValueNotifier<MakeMarkUploadBean> onChanged;

  MakeMarkUploadBean _uploadBean = MakeMarkUploadBean();

  MakeMarkEditsWidget({Key key, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _EditTextWidget(
          title: "姓名",
          hintText: "请输入",
          isShowRedStar: true,
          maxZHCharLimit: 20,
          limitCharFunc: (int limit) {
            VgToastUtils.toast(context, "姓名最多${limit ?? 0}个字");
          },
          onChanged: (String editText, dynamic value) {
            _uploadBean.name = editText;
            _notifyChange();
          },
        ),
        _EditTextWidget(
          title: "手机",
          hintText: "请输入",
          inputFormatters: [
            WhitelistingTextInputFormatter(RegExp("[0-9+]")),
            LengthLimitingTextInputFormatter(DEFAULT_PHONE_LENGTH_LIMIT),
          ],
          onChanged: (String editText, dynamic value) {
            _uploadBean.phone = editText;
            _notifyChange();
          },
        ),
        _EditTextWidget(
          title: "编号",
          hintText: "员工号/学号",
          inputFormatters: [
            WhitelistingTextInputFormatter(RegExp("[A-Za-z0-9+]")),
          ],
          maxZHCharLimit: 15,
          limitCharFunc: (int limit) {
            VgToastUtils.toast(context, "编号最多${limit ?? 0}个字");
          },
          onChanged: (String editText, dynamic value) {
            _uploadBean.id = editText;
            _notifyChange();
          },
        ),
        _EditTextWidget(
          title: "昵称",
          hintText: "英文名/备注名",
          maxZHCharLimit: 20,
          limitCharFunc: (int limit) {
            VgToastUtils.toast(context, "姓名最多${limit ?? 0}个字");
          },
          onChanged: (String editText, dynamic value) {
            _uploadBean.nick = editText;
            _notifyChange();
          },
        ),
        _EditTextWidget(
          title: "分组",
          hintText: "请选择",
          isShowRedStar: true,
          isShowGoIcon: true,
          onTap: (String editText, dynamic value) {
            return ChooseGroupPage.navigatorPush(context, value);
          },
          onDecodeResult: (dynamic result) {
            if (result is EditTextAndValue) {
              return result;
            }
            return null;
          },
          onChanged: (String editText, dynamic value) {
            _uploadBean.groupName = editText;
            _uploadBean.groupId = value;
            _notifyChange();
          },
        ),
      ],
    );
  }

  ///通知更新
  _notifyChange() {
    print(_uploadBean?.toString());

    if (onChanged == null) {
      return;
    }
    if (_uploadBean == null) {
      return;
    }
    onChanged.value = _uploadBean;
  }
}

/// 编辑框组件
///
/// @author: zengxiangxi
/// @createTime: 1/18/21 10:55 PM
/// @specialDemand:
class _EditTextWidget extends StatelessWidget {
  final String title;

  final String hintText;

  final CommonTitleEditEditAndValueChangedCallback onChanged;

  ///点击
  final CommonTitleNavigatorPushCallback onTap;

  ///解析跳转返回值
  final CommonTitlePopResultDecodeCallback onDecodeResult;

  final bool isShowRedStar;

  ///中字极限
  final int maxZHCharLimit;

  ///极限字数回调
  final ValueChanged<int> limitCharFunc;

  final bool isShowGoIcon;

  ///限制
  final List<TextInputFormatter> inputFormatters;

  const _EditTextWidget(
      {Key key,
      this.title,
      this.hintText,
      this.onChanged,
      this.isShowRedStar = false,
      this.onTap,
      this.onDecodeResult,
      this.maxZHCharLimit,
      this.limitCharFunc,
      this.inputFormatters, this.isShowGoIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CommonTitleEditWithUnderlineWidget(
      titleTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMinorGreyColor_808388(),
          fontSize: 14),
      editTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextMainColor_D0E0F7(),
          fontSize: 14),
      titleWidth: 50,
      title: title,
      height: 50,
      onChanged: onChanged,
      onDecodeResult: onDecodeResult,
      onTap: onTap,
      minLines: 1,
      maxLines: 1,
      maxZHCharLimit: maxZHCharLimit,
      limitCharFunc: limitCharFunc,
      inputFormatters: inputFormatters,
      rightWidget: Opacity(
        opacity: isShowGoIcon ? 1:0,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Image.asset(
            "images/go_ico.png",
            width: 6,
            color: VgColors.INPUT_BG_COLOR,
          ),
        ),
      ),
      lineMargin: const EdgeInsets.only(left: 25, right: 25),
      lineUnselectColor: Color(0xFF303546),
      lineSelectedColor: ThemeRepository.getInstance().getPrimaryColor_1890FF(),
      hintText: hintText,
      hintTextStyle: TextStyle(
          color: ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          fontSize: 14),
      leftWidget: Container(
        width: 23,
        child: Opacity(
          opacity: isShowRedStar ? 1 : 0,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text(
                "*",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color:
                        ThemeRepository.getInstance().getMinorRedColor_F95355(),
                    fontSize: 14,
                    height: 1.1),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

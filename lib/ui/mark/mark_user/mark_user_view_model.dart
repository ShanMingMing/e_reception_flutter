import 'dart:convert';

import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/app_main.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_browser/pages/bean/check_in_record_brower_for_type_response_bean.dart';
import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/bean/com_face_user.dart';
import 'package:e_reception_flutter/ui/mark/mark_user/bean/mark_user_upload_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/mark_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_or_mark_upload_bean.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_http_utils.dart';
import 'package:e_reception_flutter/utils/vg_hud_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_base_callback.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class MarkUserViewModel extends BaseViewModel {
  static const String _MAKE_MARK_FACE_API =
     ServerApi.BASE_URL + "app/appSignFaceUser";

  MarkUserViewModel(BaseState<StatefulWidget> state) : super(state);

  ///标记请求
  void makeMarkHttp(
    BuildContext context,
    MarkInfoBean markInfoBean,
    MarkUserUploadBean uploadBean,
  ) {
    if (uploadBean == null || !uploadBean.isAlive()) {
      return;
    }
    VgHudUtils.show(context, "正在提交");
    VgHttpUtils.post(_MAKE_MARK_FACE_API,
        params: {
          "authId": UserRepository.getInstance().authId ?? "",
          "fid": markInfoBean?.fid ?? "",
          "hsn":uploadBean?.getHsnSplitStr() ?? "",
        },
        body: jsonEncode(ComFaceUser(
          companyid: UserRepository.getInstance().companyId ?? "",
          groupid: uploadBean?.groupId ?? "",
          name: uploadBean?.name ?? "",
          napicurl: markInfoBean?.picurl ?? "",
          nick: uploadBean?.nick ?? "",
          number: uploadBean?.id ?? "",
          phone: uploadBean?.phone ?? "",
          roleid: uploadBean?.identityType?.getTypeToIdStr() ?? "",

          // createdate: 0,
          // delflg:"",
          //   createuid:"",
          //   fuid:"",
        )),
        callback: BaseCallback(onSuccess: (val) {
          VgHudUtils.hide(context);
          RouterUtils.pop(context, result: true);
          VgToastUtils.toast(AppMain.context, "保存成功");
        }, onError: (msg) {
          VgHudUtils.hide(context);
          VgToastUtils.toast(context, msg);
        }));
  }
}

import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mark/mark_user/widgets/mark_user_edit_info_widget.dart';
import 'package:e_reception_flutter/ui/mark/mark_user/widgets/mark_user_pic_widget.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/mark_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_or_mark_upload_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/matching_or_mark_view_model.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/widgets/matching_or_mark_pic_widget.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/mark_user_upload_bean.dart';
import 'mark_user_view_model.dart';

/// 匹配/标记用户
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 4:58 PM
/// @specialDemand:
class MarkUserPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "MatchingOrMarkPage";

  final MarkInfoBean markInfoBean;

  const MarkUserPage({
    Key key,
    this.markInfoBean,
  }) : super(key: key);

  @override
  _MarkUserPageState createState() => _MarkUserPageState();

  ///跳转方法
  static Future<dynamic> navigatorPush(BuildContext context, MarkInfoBean markInfoBean) {

    if (markInfoBean == null) {
      return null;
    }
    return RouterUtils.routeForFutureResult(
      context,
      MarkUserPage(
        markInfoBean: markInfoBean,
      ),
      routeName: MarkUserPage.ROUTER,
    );
  }
}

class _MarkUserPageState extends BaseState<MarkUserPage> {
  MarkUserViewModel _viewModel;

  ValueNotifier<MarkUserUploadBean> _editsValueNotifier;

  @override
  void initState() {
    super.initState();
    _viewModel = MarkUserViewModel(this);
    _editsValueNotifier = ValueNotifier(null);
  }

  @override
  void dispose() {
    _editsValueNotifier?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeRepository.getInstance().getBgOrSplitColor_191E31(),
      body: _toMainColumnWidget(),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        Expanded(
          child: _toScrollViewWidget(),
        )
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      isShowBack: true,
      title: "标记用户",
      rightWidget: _toSaveButtonWidget(),
    );
  }

  Widget _toScrollViewWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: ListView(
        padding: const EdgeInsets.all(0),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        children: <Widget>[
          MarkUserPicWidget(
            picurl: widget?.markInfoBean?.picurl,
          ),
          MatchingOrMarkEditInfoWidget(
            onChanged: _editsValueNotifier,
          )
        ],
      ),
    );
  }

  Widget _toSaveButtonWidget() {
    return ValueListenableBuilder(
      valueListenable: _editsValueNotifier,
      builder: (BuildContext context, MarkUserUploadBean uploadBean,
          Widget child) {
        return CommonFixedHeightConfirmButtonWidget(
          isAlive: uploadBean?.isAlive() ?? false,
          width: 48,
          height: 24,
          unSelectBgColor:
          ThemeRepository.getInstance().getTextEditHintColor_3A3F50(),
          selectedBgColor:
          ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
              color: VgColors.INPUT_BG_COLOR,
              fontSize: 12,
              fontWeight: FontWeight.w600),
          selectedTextStyle: TextStyle(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          text: "保存",
          onTap: (){
            String msg = uploadBean?.checkVerify();
            if(StringUtils.isEmpty(msg)){
              _viewModel?.makeMarkHttp(context, widget?.markInfoBean,uploadBean);
              return;
            }
            VgToastUtils.toast(context, msg);
          },
        );
      },
    );
  }
}

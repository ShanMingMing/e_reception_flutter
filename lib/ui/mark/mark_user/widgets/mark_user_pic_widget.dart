import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:flutter/widgets.dart';

/// 匹配/标记用户
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 6:05 PM
/// @specialDemand:
class MarkUserPicWidget extends StatelessWidget {
  final String picurl;

  const MarkUserPicWidget({Key key, this.picurl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20),
      color: ThemeRepository.getInstance().getCardBgColor_21263C(),
      child: _toNewPicWidget(),
    );
  }

  Widget _toNewPicWidget() {
    return Container(
      alignment: Alignment.center,
      child: Container(
          height: 120,
          width: 120,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: VgCacheNetWorkImage(
              picurl ?? "",
              fit: BoxFit.cover,
              imageQualityType: ImageQualityType.middleDown,
              errorWidget: _toEmptyWidget(),
            ),
          )),
    );
  }

  Widget _toEmptyWidget() {
    return Container(
      color: Color(0xFF3A3F50),
      alignment: Alignment.center,
      child: Text(
        "暂无",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: VgColors.INPUT_BG_COLOR,
          fontSize: 14,
        ),
      ),
    );
  }
}

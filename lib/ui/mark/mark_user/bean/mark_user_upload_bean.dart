import 'package:e_reception_flutter/ui/company/company_add_user_edit_info/bean/company_add_user_edit_info_type.dart';
import 'package:e_reception_flutter/ui/company/company_choose_terminal_list/bean/company_choose_terminal_response_bean.dart';
import 'package:vg_base/vg_string_util_lib.dart';

class MarkUserUploadBean{

  ///用户照片
  String picUrl;

  ///姓名
  String name;

  ///昵称
  String nick;

  ///手机号
  String phone;

  ///编号
  String id;

  ///分组
  String groupId;
  String groupName;

  ///身份
  CompanyAddUserEditInfoIdentityType identityType;
  // String identityName;

  ///终端
  List<dynamic> terminalList;


  @override
  bool operator ==(Object other) => false;

  @override
  int get hashCode => super.hashCode;


  ///是否检查通过
  bool isAlive() {
    // return true;
    //demand  全称非必填
    if (StringUtils.isEmpty(name) || StringUtils.isEmpty(groupId) || identityType == null) {
      return false;
    }
    if(isCommonAdminRole() || isSuperAdminRole()){
      if(StringUtils.isEmpty(phone)){
        return false;
      }
    }
    return true;
  }

  ///检查校验
  //demand  全称非必填
  String checkVerify(){
    if(StringUtils.isEmpty(name)){
      return "姓名不能为空";
    }
    if(StringUtils.isEmpty(groupId)){
      return "分组不能为空";
    }
    if(identityType == null){
      return "身份不能为空";
    }
    if(isCommonAdminRole() || isSuperAdminRole()){
      if(StringUtils.isEmpty(phone)){
        return "手机号不能为空";
      }
    }
    return null;
  }

  @override
  String toString() {
    return 'CompanyAddUserEditInfoUploadBean{picUrl: $picUrl, name: $name, nick: $nick, phone: $phone, id: $id, groupId: $groupId, groupName: $groupName, identityType: ${identityType?.getTypeToIdStr()},  terminalList: $terminalList，hsn: ${getHsnSplitStr() ?? ""}';
  }


  ///是否是超级管理员和管理员
  bool isCommonAdminRole(){
    return identityType == CompanyAddUserEditInfoIdentityType.commonAdmin;
  }

  ///是否是超级管理员
  bool isSuperAdminRole(){
    return identityType == CompanyAddUserEditInfoIdentityType.superAdmin;
  }

  String getHsnSplitStr(){
    if(isSuperAdminRole()){
      return null;
    }
    if(identityType == CompanyAddUserEditInfoIdentityType.commonUser){
      return null;
    }
    if(terminalList == null || terminalList.isEmpty ){
      return null;
    }
    String str = "";
    for(CompanyChooseTerminalListItemBean item in terminalList){
      if(item == null || StringUtils.isEmpty(item?.hsn)){
        continue;
      }
      if(str == null || str == ""){
        str = str + item.hsn;
      }
      else{
        str = str + ","+ item.hsn;
      }
    }
    return str;
  }
}
import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_name_and_nick_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/bean/matching_mark_list_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/matching_or_mark_page.dart';
import 'package:e_reception_flutter/utils/vg_global_func.dart';
import 'package:e_reception_flutter/utils/vg_tool_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_image_widget/vg_cache_net_work_image.dart';
import 'package:e_reception_flutter/vg_widgets/vg_photo_preview/vg_photo_preview.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_string_util_lib.dart';

/// 匹配标记列表-列表项
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 2:25 PM
/// @specialDemand:
class MatchingMarkListItemWidget extends StatelessWidget {


  final MatchingMarkListItemBean itemBean;

  final ValueChanged<MatchingMarkListItemBean> onTap;

  const MatchingMarkListItemWidget({Key key, this.itemBean, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: (){
          onTap?.call(itemBean);
        },
        child: _toListItemWidget(context));
  }

  Widget _toListItemWidget(BuildContext context) {
    return Container(
      height: 64,
      margin: const EdgeInsets.symmetric(horizontal: 15),
      child: _toMainRowWidget(context),
    );
  }

  Widget _toMainRowWidget(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: (){
            VgPhotoPreview.single(context, showOriginEmptyStr(itemBean?.putpicurl) ?? (itemBean?.napicurl ?? ""),loadingImageQualityType: ImageQualityType.middleDown);
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Container(
              width: 36,
              height: 36,
              child: VgCacheNetWorkImage(
                showOriginEmptyStr(itemBean?.putpicurl) ?? (itemBean?.napicurl ?? ""),
                fit: BoxFit.cover,
                imageQualityType: ImageQualityType.middleDown,
                defaultErrorType: ImageErrorType.head,
                defaultPlaceType: ImagePlaceType.head,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: _toColumnWidget(),
        )
      ],
    );
  }

  Widget _toColumnWidget() {
    return DefaultTextStyle(
      style: TextStyle(
          height: 1.2
      ),
      child: Container(
        height: 36,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                CommonNameAndNickWidget(
                  name: itemBean?.name,
                  nick: itemBean?.nick,
                ),
                Offstage(
                  offstage: !(itemBean?.isMark() ?? false),
                  child: Container(
                    height: 16,
                    margin: const EdgeInsets.only(left: 8),
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        color: ThemeRepository.getInstance()
                            .getHintGreenColor_00C6C4()
                            .withOpacity(0.1)),
                    child: Center(
                      child: Text(
                        "已标记",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: ThemeRepository.getInstance()
                              .getHintGreenColor_00C6C4(),
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
                Text(
                  _getGroupAndIdStr(itemBean?.groupName,itemBean?.number) ?? "",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 12,
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Builder(builder: (BuildContext context) {
                  return Text(
                    StringUtils.isNotEmpty(itemBean?.phone) ? "${itemBean.phone}" : "暂无手机号码",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: StringUtils.isNotEmpty(itemBean?.phone) ? Color(0xFF5E687C):ThemeRepository.getInstance().getMinorRedColor_F95355(),
                      fontSize: 12,
                    ),
                  );
                }),
                Spacer(),
                Text(
                  "${_getAddOrMarkUserStr() ?? ""}",
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: VgColors.INPUT_BG_COLOR,
                    fontSize: 10,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  String _getGroupAndIdStr(String groupName,String number){
    if(StringUtils.isEmpty(groupName) && StringUtils.isEmpty(number)){
      return null;
    }
    if(StringUtils.isNotEmpty(groupName) && StringUtils.isEmpty(number)){
      return groupName;
    }
    if(StringUtils.isEmpty(groupName) && StringUtils.isNotEmpty(number)){
      return number;
    }
    return groupName+ "·" + number;
  }

  String _getAddOrMarkUserStr(){
    if(StringUtils.isEmpty(itemBean?.uname)){
      return null;
    }
    StringBuffer stringBuffer = StringBuffer(itemBean?.uname);
    /// tpe  00标记录入 01app手动添加
    if(!(itemBean?.isMark() ?? false)){
      stringBuffer.write("添加");
    }else{
      stringBuffer.write("标记");
    }
    return stringBuffer.toString();
  }
}

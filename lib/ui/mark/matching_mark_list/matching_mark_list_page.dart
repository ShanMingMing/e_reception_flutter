import 'package:e_reception_flutter/colors/colors.dart';
import 'package:e_reception_flutter/common_widgets/common_fixed_height_confirm_button_widget/common_fixed_height_confirm_button_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_pack_up_keyboard_widget/common_pack_up_keyboard_widget.dart';
import 'package:e_reception_flutter/common_widgets/common_search_bar_widget/common_search_bar_widget.dart';
import 'package:e_reception_flutter/singleton/theme_repository/theme_repository.dart';
import 'package:e_reception_flutter/ui/check_in_record/check_in_record_by_type_list/bean/check_in_record_by_type_list_response_bean.dart';
import 'package:e_reception_flutter/ui/mark/make_mark/make_mark_page.dart';
import 'package:e_reception_flutter/ui/mark/mark_user/mark_user_page.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/matching_mark_list_view_model.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/widgets/matching_mark_list_item_widget.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/mark_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/bean/matching_info_bean.dart';
import 'package:e_reception_flutter/ui/mark/matching_or_mark/matching_or_mark_page.dart';
import 'package:e_reception_flutter/utils/router_utils.dart';
import 'package:e_reception_flutter/utils/screen_utils.dart';
import 'package:e_reception_flutter/utils/vg_toast_utils.dart';
import 'package:e_reception_flutter/vg_widgets/vg_place_holder_status_widget/mixin/vg_place_holder_status_mixin.dart';
import 'package:e_reception_flutter/vg_widgets/vg_pull_to_refresh_widget/vg_pull_to_refresh_widget.dart';
import 'package:e_reception_flutter/vg_widgets/vg_top_bar_widget/vg_top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';
import 'package:vg_base/vg_string_util_lib.dart';
import 'package:vg_flutter_base_moudle/vg_moudle_widget_lib.dart';

import 'bean/matching_mark_list_response_bean.dart';

/// 匹配标记列表
///
/// @author: zengxiangxi
/// @createTime: 1/27/21 2:17 PM
/// @specialDemand:
class MatchingMarkListPage extends StatefulWidget {
  ///路由名称
  static const String ROUTER = "MatchingMarkListPage";

  final MarkInfoBean markInfoBean;

  const MatchingMarkListPage({Key key, this.markInfoBean}) : super(key: key);

  @override
  _MatchingMarkListPageState createState() => _MatchingMarkListPageState();

  ///跳转方法
  static Future<bool> navigatorPush(
      BuildContext context, MarkInfoBean markInfoBean) {
    // if(StringUtils.isEmpty(groupId)){
    //   VgToastUtils.toast(context, "分组获取异常");
    //   return null;
    // }
    return RouterUtils.routeForFutureResult<bool>(
      context,
      MatchingMarkListPage(markInfoBean: markInfoBean),
      routeName: MatchingMarkListPage.ROUTER,
    );
  }
}

class _MatchingMarkListPageState
    extends BasePagerState<MatchingMarkListItemBean, MatchingMarkListPage>
    with VgPlaceHolderStatusMixin {
  MatchingMarkListViewModel _viewModel;

  String searchStr;

  @override
  void initState() {
    super.initState();
    _viewModel = MatchingMarkListViewModel(this, () => searchStr);
    _viewModel?.refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: ThemeRepository.getInstance().getCardBgColor_21263C(),
      body: CommonPackUpKeyboardWidget(child: _toMainColumnWidget()),
    );
  }

  Widget _toMainColumnWidget() {
    return Column(
      children: <Widget>[
        _toTopBarWidget(),
        // SizedBox(height: 12),
        CommonSearchBarWidget(
          hintText: "搜索",
          onChanged: (String searchStr) {},
          onSubmitted: (String searchStr) {
            this.searchStr = searchStr;
            _viewModel?.refresh();
          },
        ),
        SizedBox(height: 5),
        Expanded(
          child: MixinPlaceHolderStatusWidget(
              errorOnClick: () => _viewModel.refresh(),
              emptyOnClick: () => _viewModel?.refresh(),
              child: _toPullRefreshWidget()),
        ),
        _toGoMakeMarkButtonWidget(),
      ],
    );
  }

  Widget _toTopBarWidget() {
    return VgTopBarWidget(
      title: "选择匹配用户",
      isShowBack: true,
    );
  }

  Widget _toPullRefreshWidget() {
    return ScrollConfiguration(
      behavior: MyBehavior(),
      child: VgPullRefreshWidget.bind(
        viewModel: _viewModel,
        state: this,
        child: ListView.separated(
            itemCount: data?.length ?? 0,
            padding: const EdgeInsets.all(0),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            itemBuilder: (BuildContext context, int index) {
              return MatchingMarkListItemWidget(
                itemBean: data?.elementAt(index),
                onTap: (MatchingMarkListItemBean itemBean) async {
                  bool result = await MatchingOrMarkPage.navigatorPush(
                      context,
                      MatchingInfoBean(
                          picurl: itemBean?.putpicurl,
                          groupId: itemBean?.groupid,
                          groupName: itemBean?.groupName,
                          signFuid: itemBean?.fuid,
                          nick: itemBean?.nick,
                          name: itemBean?.name,
                          roleid: itemBean?.roleid,
                          phone: itemBean?.phone,
                          number: itemBean?.number),
                      widget?.markInfoBean);
                  if (result != null && result is bool && result ?? false) {
                    RouterUtils.pop(context, result: true);
                  }
                },
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 0,
              );
            }),
      ),
    );
  }

  Widget _toGoMakeMarkButtonWidget() {
    return Container(
      padding: EdgeInsets.only(bottom: ScreenUtils.getBottomBarH(context)),
      decoration: BoxDecoration(
          color: ThemeRepository.getInstance().getCardBgColor_21263C(),
          boxShadow: [
            BoxShadow(color: Colors.black.withOpacity(0.2), blurRadius: 4)
          ]),
      child: Container(
        height: 60,
        alignment: Alignment.center,
        child: CommonFixedHeightConfirmButtonWidget(
          isAlive: true,
          height: 40,
          margin: const EdgeInsets.symmetric(horizontal: 25),
          unSelectBgColor: Color(0xFF3A3F50),
          selectedBgColor:
              ThemeRepository.getInstance().getPrimaryColor_1890FF(),
          unSelectTextStyle: TextStyle(
            color: VgColors.INPUT_BG_COLOR,
            fontSize: 15,
          ),
          selectedTextStyle: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
          text: "没有匹配，直接标记",
          textRightSelectedWidget: Padding(
            padding: const EdgeInsets.only(left: 10, bottom: 2),
            child: Image.asset(
              "images/go_ico.png",
              width: 6,
            ),
          ),
          onTap: () async {
            bool result =
            await MarkUserPage.navigatorPush(context, widget?.markInfoBean);
            if (result != null && result is bool && result ?? false) {
              RouterUtils.pop(context, result: true);
            }
          },
        ),
      ),
    );
  }
}

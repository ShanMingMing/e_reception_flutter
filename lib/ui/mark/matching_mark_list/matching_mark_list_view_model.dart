import 'package:e_reception_flutter/net/server_api.dart';
import 'package:e_reception_flutter/singleton/user_repository/user_repository.dart';
import 'package:e_reception_flutter/ui/mark/matching_mark_list/bean/matching_mark_list_response_bean.dart';
import 'package:flutter/widgets.dart';
import 'package:vg_base/vg_arch_lib.dart';
import 'package:vg_base/vg_http_lib.dart';
import 'package:vg_base/vg_pull_to_refresh_lib.dart';

class MatchingMarkListViewModel extends BasePagerViewModel<MatchingMarkListItemBean,MatchingMarkListResponseBean>{

  final String Function() getSearchFunc;

  MatchingMarkListViewModel(BaseState<StatefulWidget> state,this.getSearchFunc) : super(state);

  @override
  Map<String, dynamic> getQuery(int page) {
    Map<String, dynamic> map = {
      "authId": UserRepository.getInstance().authId ?? "",
      "current": page ?? 1,
      "size": 20,
      "searchName": getSearchFunc?.call()?.trim() ?? "",
    };
    return map;
  }

  @override
  String getRequestMethod() {
    return HttpUtils.GET;
  }

  @override
  String getUrl() {
    return ServerApi.BASE_URL + "app/appChooseMatchingUserList";
  }

  @override
  MatchingMarkListResponseBean parseData(VgHttpResponse resp) {
    MatchingMarkListResponseBean vo = MatchingMarkListResponseBean.fromMap(resp?.data);
    loading(false);
    return vo;
  }

  @override
  String getBodyData(int page) {
    return null;
  }

}
import 'package:e_reception_flutter/constants/branch/main_constant.dart';
import 'package:e_reception_flutter/ui/smart_home/smart_home_style_type_bean.dart';

import '../account_constant.dart';

class BranchTestConstant extends MainConstant{
  @override
  String getAmapIOSKey() {
    return AccountConstant.AMAP_IOS_TEST_KEY;
  }

  @override
  String getAmapAndroidKey() {
    return AccountConstant.AMAP_ANDROID_TEST_KEY;
  }

  @override
  bool isJumpLoginAuthCode() => true;

  @override
  bool isCatchHttpError()  => true;

  @override
  String getOssImageFolder()  => "test";

  @override
  String getOssVideoFolder() => "test";

  @override
  String getOssAudioFolder() => "test";

  @override
  String getIOSDownloadUrl() {
    return "itms-apps://itunes.apple.com/app/id1566814447";
  }

  @override
  String getAndroidPagerUrl() {
    return null;
  }

  @override
  List<String> getCameraStartDistance() {
    List<String> distanceList = new List();
    distanceList.add("0.5米");
    distanceList.add("1米");
    distanceList.add("1.5米");
    distanceList.add("2米");
    distanceList.add("2.5米");
    distanceList.add("3米");
    return distanceList;
  }

  @override
  List<String> getPunchInInterval() {
    List<String> punchInInterval = new List();
    punchInInterval.add("1分钟");
    punchInInterval.add("5分钟");
    punchInInterval.add("10分钟");
    punchInInterval.add("30分钟");
    punchInInterval.add("1小时");
    punchInInterval.add("1.5小时");
    punchInInterval.add("2小时");
    punchInInterval.add("3小时");
    punchInInterval.add("4小时");
    punchInInterval.add("5小时");
    punchInInterval.add("6小时");
    punchInInterval.add("7小时");
    punchInInterval.add("8小时");
    punchInInterval.add("9小时");
    punchInInterval.add("10小时");
    punchInInterval.add("11小时");
    punchInInterval.add("12小时");
    return punchInInterval;
  }


  @override
  bool get isAppStore => false;

  @override
  void initUmeng(){
    print("友盟初始化状态-----");
  }

  @override
  void initBugly(Function runApp) {
    runApp.call();
  }

  @override
  List<String> getMediaChangeInterval() {
    List<String> mediaChangeInterval = new List();
    mediaChangeInterval.add("3秒");
    mediaChangeInterval.add("5秒");
    mediaChangeInterval.add("10秒");


    mediaChangeInterval.add("15秒");
    mediaChangeInterval.add("20秒");

    mediaChangeInterval.add("25秒");
    mediaChangeInterval.add("30秒");

    mediaChangeInterval.add("35秒");
    mediaChangeInterval.add("40秒");

    mediaChangeInterval.add("45秒");
    mediaChangeInterval.add("50秒");

    mediaChangeInterval.add("55秒");
    mediaChangeInterval.add("60秒");

    mediaChangeInterval.add("65秒");
    mediaChangeInterval.add("70秒");

    mediaChangeInterval.add("75秒");
    mediaChangeInterval.add("80秒");

    mediaChangeInterval.add("85秒");
    mediaChangeInterval.add("90秒");

    mediaChangeInterval.add("95秒");
    mediaChangeInterval.add("100秒");

    return mediaChangeInterval;
  }

  @override
  String getStyleNameByStyle(String style) {
    List<SmartHomeStyleTypeBean> styleList = getStyleList();
    String name = "";
    for(int i = 0; i < styleList?.length; i++){
      if(style == styleList[i].style){
        name = styleList[i].name;
        break;
      }
    }
    if("其他" == name){
      return "未知风格";
    }
    return name;
  }
  
  @override
  List<SmartHomeStyleTypeBean> getStyleList() {
    List<SmartHomeStyleTypeBean> styleList = new List();
    styleList.add(new SmartHomeStyleTypeBean("现代简约", "00"));
    styleList.add(new SmartHomeStyleTypeBean("中式现代", "01"));
    styleList.add(new SmartHomeStyleTypeBean("美式田园", "02"));
    styleList.add(new SmartHomeStyleTypeBean("美式经典", "03"));
    styleList.add(new SmartHomeStyleTypeBean("欧式豪华", "04"));
    styleList.add(new SmartHomeStyleTypeBean("北欧极简", "05"));
    styleList.add(new SmartHomeStyleTypeBean("日式", "06"));
    styleList.add(new SmartHomeStyleTypeBean("地中海", "07"));
    styleList.add(new SmartHomeStyleTypeBean("潮流混搭", "08"));
    styleList.add(new SmartHomeStyleTypeBean("轻奢", "09"));
    styleList.add(new SmartHomeStyleTypeBean("其他", "99"));
    return styleList;
  }

  @override
  String getTemplateGroupId() {
    //超清视频组id
    return "38259109e7108bc6586691f49a217c0d";
    //高清视频组id
    // return "8cae871c81c42affda3dd93b8533e729";
  }

  @override
  String getDefinition() {
    //超清 高清
    return "HD,SD";
    //高清
    // return "SD";
  }
}
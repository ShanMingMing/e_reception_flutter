import 'package:e_reception_flutter/ui/smart_home/smart_home_style_type_bean.dart';

abstract class MainConstant{
  // 初始化友盟
  void initUmeng();

  void initBugly(Function runApp);

  //是否开启bugly
  bool get isAppStore;

  //高德IosKey
  String getAmapIOSKey();

  // 高德AndroidKey
  String getAmapAndroidKey();

  //跳过登录验证码
  bool isJumpLoginAuthCode();
  
  //是否显示请求报错
  bool isCatchHttpError();

  //oss上传groupid
  String getTemplateGroupId();

  //oss 分辨率
  String getDefinition();

  //oss视频文件夹
  String getOssVideoFolder();

  //oss图片文件夹
  String getOssImageFolder();

  //oss音频文件夹
  String getOssAudioFolder();
  
  //ios下载
  String getIOSDownloadUrl();

  //Android下载地址
  String getAndroidPagerUrl();

  //摄像头启动距离
  List<String> getCameraStartDistance();

  //打卡间隔
  List<String> getPunchInInterval();

  //图片切换时间
  List<String> getMediaChangeInterval();

  //根据风格标志得到风格名
  String getStyleNameByStyle(String style);

  //风格列表
  List<SmartHomeStyleTypeBean> getStyleList();

}
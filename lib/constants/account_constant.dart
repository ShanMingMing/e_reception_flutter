///账号常量
class AccountConstant{

  //高德IOS测试版KEY
  static const String AMAP_IOS_TEST_KEY = "24d0c2083cad8af553c41c60c6dbbb41";

  //高德IOS正式版KEY
  static const String AMAP_IOS_APPSTORE_KEY = "7f37d38f3af2e53c64778fe11cc1f288";

  // 高德android测试版key
  static const String AMAP_ANDROID_TEST_KEY = "e5f4a01916bfff808036facc6ce8383d";

  // 高德android正式版key
  static const String AMAP_ANDROID_APPSTORE_KEY = "c6acb27e11bc584147590fbcf1b5323e";


  //高德WEB的Key
  static const String AMAP_WEB_KEY = "41266d90cfa5885c0c7b58ea4a95be32";
  

  //友盟IOS
  static const String UMENG_IOS_KEY = "60656ee4de41b946ab397741";

  //友盟Android
  static const String UMENG_ANDROID_KEY = "60656ec1de41b946ab397725";

  ///Bugly Android
  static const String BUGLY_ANDROID_ID = "d5cbff44d6";

  ///Bugly IOS
  static const String BUGLY_IOS_ID = "e88d79812d";

  //友盟渠道名称
  static const String UMENG_CHANNEL_NAME = "Ereception";

  ///微信appId
  // static const String WX_APPID = "wx0f462d791a556895";
  static const String WX_APPID = "wx5effab1c66687e43";

  ///微信secret
  // static const String WX_SECRET = "8c811b17fc6a022262846105a21db602";
  static const String WX_SECRET = "11289b5299bc1e14e3ab59cf7d6c3fab";

  ///universalLink
  static const String WX_UNIVERSAL_LINK =
      "https://1c0b0c18ed4a7dac433dedcee1e88089.share2dlink.com/";
}
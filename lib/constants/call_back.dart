
///成功
typedef OnSuccess<T> = void Function(T val);

///错误
typedef OnError = void Function(String msg);

///额外
typedef OnExtra = void Function(dynamic extra);

///通用成功/错误/额外回调
class VgBaseCallback<T> {
  OnSuccess<T> onSuccess;
  OnError onError;
  OnExtra onHandleExtra;

  VgBaseCallback({this.onSuccess, this.onError,this.onHandleExtra});
}

import 'package:vg_flutter_base_moudle/vg_moudle_constants_lib.dart';

///手机号长度
const int DEFAULT_PHONE_LENGTH_LIMIT = 11;

///默认动画转化时长300ms
const Duration DEFAULT_ANIM_DURATION = Duration(milliseconds: 300);

///默认验证码长度极限
const int DEFAULT_AUTH_LENGTH_LIMIT = 4;

///默认底部卡片弹窗圆角8
const double DEFAULT_BOTTOM_CARD_DIALOG_RADIUS = 12.0;

///导航固定高度
const double NAV_HEIGHT = 49;

///超管
const String ROLE_SUPER_ADMIN = "99";

///普通管理员
const String ROLE_COMMON_ADMIN = "90";

///普通人
const String ROLE_COMMON_USER = "10";

///首页弹窗提示标记
const String KEY_SHOW_HOMEPAGE_DIALOG_FLG = "key_show_homepage_dialog_flg";

///上次添加用户默认选择的类 存选择的groupid
const String KEY_LAST_ADD_USER_TYPE = "key_last_add_user_type";

///今日刷脸缓存日期
const String KEY_FACE_TODAY_RECORD_DATE = "key_face_today_record_date";

///考勤规则数
const String KEY_PUNCH_IN_RULE_CNT = "key_punch_in_rule_cnt";

///部门数
const String KEY_DEPARTMENT_CNT = "key_department_cnt";

///班级数
const String KEY_CLASS_CNT = "key_class_cnt";

///缓存是否是第一次安装
const String SP_FIRST_INSTALL = "sp_first_install";

///缓存是否是勾选过协议
const String SP_CHOICE_AGREEMENT = "sp_choice_agreement";

///缓存企业主业查询记录
const String SP_SEARCH_DETAIL = "sp_search_detail";

///已选部门缓存id
const String KEY_SELECT_DEPARTMENT= "key_select_department";

///已选班级缓存id
const String KEY_SELECT_CLASS= "key_select_class";

const String IGNORE_HSN = "156FC7BC75EE997CF1082C7B8DF44722A8";

///缓存是否告知diy图片相关
const String SP_SAVE_DIY = "sp_save_diy";

///缓存是否显示过切换指示牌模板的标志
const String SP_SAVE_SHOW_SET_MODULE_TYPE_BUBBLE = "sp_save_show_set_module_type_bubble";

///默认加载质量
String DEFAULT_IMAGE_QUALITY = ImageThumbConstant.HIGH;

///所有终端的hsn
const String SP_ALL_TERMINAL_HSNS = "sp_all_terminal_hsns";

const String SP_STORE_COUNT = "sp_store_count";

///缩略图片后缀
const String THUMB_SUFFIX = "?x-oss-process=style/ep_style_list";

///时区
const String TIME_ZONE = 'Asia/Shanghai';



